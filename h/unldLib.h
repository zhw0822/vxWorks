/* unldLib.h - header for unload library */

/*
 * Copyright (c) 1992-2005 Wind River Systems, Inc. 
 *
 * The right to copy, distribute, modify or otherwise make use of this software
 * may be licensed only pursuant to the terms of an applicable Wind River
 * license agreement. 
 */

/*
modification history
--------------------
01l,14apr05,v_r  Removed unnecessary includes and marked obsolete errnos.
01k,28feb05,v_r  Fixed SPR #106542 (unldByGroup not working).
		 Removed unused moduleByIdUnload().
01j,21sep04,jn   Final minor modification to allow sharing w/ host
01i,18sep04,jn   Move towards shared code on host and target
01h,05apr04,jn   Clean up - remove unused declarations and development-related
                 comments
01g,30oct96,elp  Added syncUnldRtn function pointer + UNLD_SYNC option.
01f,22aug93,jmm  added option type to supress breakpoint deletion
01e,16aug93,jmm  added new errno - S_unldLib_TEXT_IN_USE
01d,30oct92,jmm  added prototype for reld() (spr 1716)
01c,22sep92,rrr  added support for c++
01b,18sep92,jcf  added include of moduleLib.h.
01a,14may92,jmm  written.
*/

#ifndef __INCunldLibh
#define __INCunldLibh

#ifdef __cplusplus
extern "C" {
#endif

#include "moduleLib.h"

/* unldLib Status Codes */

#define S_unldLib_MODULE_NOT_FOUND             (M_unldLib | 1) /* obsoleted */
#define S_unldLib_TEXT_IN_USE                  (M_unldLib | 2)

/* User options for the unloader */

#define UNLD_KEEP_BREAKPOINTS	0x1	/* don't delete breakpoints on unload */
#define UNLD_FORCE		0x2	/* Do not test whether text sections
					   are in use */

#define UNLD_CPLUS_XTOR_AUTO    0x20	/* call the C++ destructors */
#define UNLD_CPLUS_XTOR_MANUAL  0x40	/* don't call the C++ destructors */

/* The following options are restricted for internal use only */

#define UNLD_SYNC		0x10000	/* already synchronizing a module */

/* globals */

extern FUNCPTR syncUnldRtn;

/* function declarations */

extern STATUS unldByGroup(UINT16 group, int options);
extern STATUS unldByModuleId(MODULE_ID moduleId, int options);
extern STATUS unldByNameAndPath(char * name, char * path, int options);
extern STATUS unldLibInit (void);

#ifdef __cplusplus
}
#endif

#endif /* __INCunldLibh */
