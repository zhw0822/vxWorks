/* shellInterpLib.h - header file for the interpreters management module */

/* Copyright 2003-2005 Wind River Systems, Inc. */

/*
modification history
--------------------
01i,08feb05,bpn  Added dfltPrompt field to interpreter structure (SPR#106114).
01h,28jul04,bpn  Added shellInterpCtxGet() prototypes.
01g,26feb04,bpn  Added ctxRestart field to SHELL_INTERP.
01f,10feb04,lcs  Add host routine to handle DFW asynchronous events.
01e,06nov03,bpn  Added shellInterpDefaultNameGet() and shellInterpNameGet()
                 prototypes.
01d,24oct03,bpn  Added completion field to SHELL_INTERP structure.
01c,23sep03,bpn  Added prototypes of the initialization functions of the
                 C and command interpreters.
01b,24jun03,bpn  Added history list to each interpreter.
01a,17feb03,bpn  Written.
*/

#ifndef __INCshellInterpLibh
#define __INCshellInterpLibh

#ifdef __cplusplus
extern "C" {
#endif

/* Includes */

#include <ledLib.h>
#include <lstLib.h>

#include <shellLib.h>

/* Defines */

/* The interpreter data structure */

typedef struct shell_interp
    {
    NODE	    node;
    const char *    name;	/* interpreter name */
    FUNCPTR	    ctxInit;	/* interpreter ctx init routine */
    VOIDFUNCPTR	    ctxRestart;	/* interpreter ctx restart routine */
    FUNCPTR	    ctxFinalize;/* interpreter ctx terminate routine */
    FUNCPTR	    parser;	/* interpreter parser routine */
    FUNCPTR	    evaluate;	/* interpreter evaluation routine */
    FUNCPTR	    completion;	/* interpreter input completion routine */
    const char *    prompt;	/* original prompt of the interpreter */
    /* BELOW IS Wind River RESERVED INFO */
    char *	    dfltPrompt;	/* default interpreter prompt (malloc-ed) */
    LED_HIST	    history;	/* initial commands history list */
#ifdef HOST
    FUNCPTR	    asyncEvtDispatch;	/* called when a DFW msg is recvd */
#endif /* HOST */
    } SHELL_INTERP;

typedef struct shell_interp_ctx
    {
    NODE	    node;
    SHELL_INTERP *  pInterp;		/* interpreter used */
    void *	    pInterpParam;	/* internal data of the interpreter */
    char *	    currentPrompt;	/* prompt to use (malloc()-ed) */
    LED_HIST	    history;		/* commands history list */
    void *	    reserved;		/* FOR INTERNAL USE ONLY */
    } SHELL_INTERP_CTX;

/* Function declarations */

extern STATUS		shellInterpRegister (FUNCPTR interpInitRtn);

extern const char *	shellInterpDefaultNameGet (void);
extern const char *	shellInterpNameGet (SHELL_ID shellId);

extern SHELL_INTERP_CTX *	shellInterpCtxGet (SHELL_ID shellId);

/* Functions call at boot time */

extern STATUS	shellInterpCInit (SHELL_INTERP * pInterp);
extern STATUS	shellInterpCmdInit (SHELL_INTERP * pInterp);

#ifdef __cplusplus
}
#endif

#endif /* __INCshellInterpLibh */
