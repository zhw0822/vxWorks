/* semLibCommon.h - common semaphore library header file */

/*
 * Copyright (c) 2003-2005 Wind River Systems, Inc.
 *
 * The right to copy, distribute or otherwise make use of this software
 * may be licensed only pursuant to the terms of an applicable Wind River
 * license agreement. 
*/

/*
modification history
--------------------
01k,03aug05,gls  added read/write semaphore support
01j,05apr05,kk   added OPTIONS_MASK for each type of semaphore
01i,02sep04,bwa  added S_semLib_INVALID_INITIAL_COUNT/COUNT_OVERFLOW errnos.
01h,04may04,cjj  modified SEM_INFO struct. Removed semInfo prototype.
01g,23apr04,dcc  added "context" to semOpen() parameters
01f,11mar04,dcc  added semClose() and semUnlink() prototypes.
01e,30mar04,ans  added SEM_INTERRUPTIBLE semaphore option
01d,05nov03,md   added user level semaphores
01c,25nov03,aeg  added SEM_INFO structure definition.
01b,05sep03,dcc  added SEM_TYPE_OLD to the SEM_TYPE enum.
01a,26aug03,cjj  written based on kernel version 04h of semLib.h
*/

#ifndef __INCsemLibCommonh
#define __INCsemLibCommonh

#ifdef __cplusplus
extern "C" {
#endif

#include <vxWorks.h>
#include <vwModNum.h>
#include <objLib.h>

/* generic status codes */

#define S_semLib_INVALID_STATE			(M_semLib | 101)
#define S_semLib_INVALID_OPTION			(M_semLib | 102)
#define S_semLib_INVALID_QUEUE_TYPE		(M_semLib | 103)
#define S_semLib_INVALID_OPERATION		(M_semLib | 104)
#define S_semLib_INVALID_INITIAL_COUNT  	(M_semLib | 105)
#define S_semLib_COUNT_OVERFLOW                 (M_semLib | 106)

/* semaphore options */

#define SEM_Q_FIFO		 0x00	/* first in first out queue */
#define SEM_Q_PRIORITY		 0x01	/* priority sorted queue */
#define SEM_DELETE_SAFE		 0x04	/* owner delete safe (mutex opt.) */
#define SEM_INVERSION_SAFE	 0x08	/* no priority inversion (mutex opt.) */
#define SEM_EVENTSEND_ERR_NOTIFY 0x10	/* notify when eventRsrcSend fails */
#define SEM_INTERRUPTIBLE        0x20   /* interruptible on RTP signal */

/* option masks for the types of semaphores */

#if !defined (_WRS_KERNEL)

#define SEM_BIN_OPTIONS_MASK	(SEM_Q_FIFO | 			\
				 SEM_Q_PRIORITY | 		\
				 SEM_EVENTSEND_ERR_NOTIFY | 	\
				 SEM_INTERRUPTIBLE | 		\
				 SEM_KUTYPE_MASK)

#else
#define SEM_BIN_OPTIONS_MASK	(SEM_Q_FIFO | 			\
				 SEM_Q_PRIORITY | 		\
				 SEM_EVENTSEND_ERR_NOTIFY | 	\
				 SEM_INTERRUPTIBLE)
#endif /* !_WRS_KERNEL */

#define SEM_CNT_OPTIONS_MASK	SEM_BIN_OPTIONS_MASK

#define SEM_MUT_OPTIONS_MASK	(SEM_BIN_OPTIONS_MASK | 	\
				 SEM_DELETE_SAFE |		\
				 SEM_INVERSION_SAFE)

#ifndef	_ASMLANGUAGE

/* semaphore types */

typedef enum 		/* SEM_TYPE */
    {
    SEM_TYPE_BINARY,         /* 0: binary semaphore */
    SEM_TYPE_MUTEX,          /* 1: mutual exclusion semaphore */
    SEM_TYPE_COUNTING,       /* 2: counting semaphore */
    SEM_TYPE_OLD,	     /* 3: 4.x style semaphore  (not in user space) */
    SEM_TYPE_RW,	     /* 4: read/write semaphore (not in user space) */

    /* 
     * Add new semaphore types above this one.  Only 8 types 
     * are allowed without making major modifications to the
     * kernel 
     */
    SEM_TYPE_MAX = 8	
    } SEM_TYPE;

/* binary semaphore initial state */

typedef enum		/* SEM_B_STATE */
    {
    SEM_EMPTY,			/* 0: semaphore not available */
    SEM_FULL			/* 1: semaphore available */
    } SEM_B_STATE;

/* 
 * Information structure filled by semInfoGet.  The taskIdListMax
 * and taskIdList information is not provided in user land.
 */

typedef struct			/* SEM_INFO */
    {
    UINT	numTasks;	/* OUT: number of blocked tasks */
    SEM_TYPE 	semType;	/* OUT: semaphore type */
    int		options;	/* OUT: options with which sem was created */
    union
	{
	UINT	count;		/* OUT: semaphore count (couting sems) */
	BOOL	full;		/* OUT: binary semaphore FULL? */
	int	owner;		/* OUT: task ID of mutex semaphore owner */
        } state;
#ifdef _WRS_KERNEL
    int		taskIdListMax;	/* IN: max tasks to fill in taskIdList */
    int *	taskIdList;	/* PTR: array of pending task IDs */
#endif /* _WRS_KERNEL */
    } SEM_INFO;


/* function declarations */

extern SEM_ID 	  semMCreate 	(int options);
extern SEM_ID 	  semBCreate 	(int options, SEM_B_STATE initialState);
extern SEM_ID 	  semCCreate 	(int options, int initialCount);
extern STATUS 	  semDelete 	(SEM_ID semId);
extern STATUS 	  semFlush 	(SEM_ID semId);
extern STATUS 	  semGive 	(SEM_ID semId);
extern STATUS 	  semTake 	(SEM_ID semId, int timeout);
extern SEM_ID	  semOpen	(const char * name, SEM_TYPE type, 
				 int initState, int options, int mode,
				 void * context);
extern STATUS	  semInfoGet	(SEM_ID semId, SEM_INFO *pInfo);
extern STATUS 	  semClose	(SEM_ID semId);
extern STATUS 	  semUnlink	(const char * name);

#endif	/* _ASMLANGUAGE */

#ifdef __cplusplus
}
#endif

#endif /* __INCsemLibCommonh */
