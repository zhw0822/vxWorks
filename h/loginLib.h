/* loginLib.h - user password/login subroutine library header file */

/* Copyright 1984-2004 Wind River Systems, Inc. */

/*
modification history
--------------------
01u,26apr04,bpn  Added loginPrompt2() prototype to fix SPR#96149.
01t,12feb04,asr  added #ifdef _WRS_KERNEL sections to allow loginLib.h
                 to be included from user and kernel mode builds. Also
                 added loginVerifyArgs structure for sysctl use.
01s,22sep92,rrr  added support for c++
01r,04jul92,jcf  cleaned up.
01q,26may92,rrr  the tree shuffle
01p,04oct91,rrr  passed through the ansification filter
		  -fixed #else and #endif
		  -changed VOID to void
		  -changed copyright notice
01o,19aug91,rrr  fixed the ansi prototype for loginUserAdd
01d,01may91,jdi  added ifndef for DOC.
01c,21jan91,shl  fixed arguments type in loginUserAdd().
01b,05oct90,shl  added ANSI function prototypes.
                 made #endif ANSI style.
                 added copyright notice.
01a,18apr90,shl  written.
*/

#ifndef __INCloginLibh
#define __INCloginLibh

#ifdef __cplusplus
extern "C" {
#endif

#include "vwModNum.h"

#ifndef DOC
#define MAX_LOGIN_NAME_LEN    80
#define MAX_PASSWORD_LEN      80
#define MAX_LOGIN_ENTRY       80
#endif	/* DOC */

/* status messages */

#define S_loginLib_UNKNOWN_USER			(M_loginLib | 1)
#define S_loginLib_USER_ALREADY_EXISTS		(M_loginLib | 2)
#define S_loginLib_INVALID_PASSWORD		(M_loginLib | 3)

/* structure for passing login user info to sysctl */
struct loginVerifyArgs 
    {
    char name[MAX_LOGIN_NAME_LEN+1];
    char passwd[MAX_PASSWORD_LEN+1];
    };

/* function declarations */

extern void 	loginInit (void);
extern STATUS 	loginUserVerify (char * name, char * passwd);

#ifdef _WRS_KERNEL
extern STATUS 	loginDefaultEncrypt (char * in, char * out);
extern STATUS 	loginPrompt (char * userName);
extern STATUS 	loginUserAdd (char name[MAX_LOGIN_NAME_LEN],
			      char passwd[MAX_PASSWORD_LEN]);
extern STATUS 	loginUserDelete (char * name, char * passwd);
extern void 	loginEncryptInstall (FUNCPTR rtn, int var);
extern void 	loginStringSet (char * newString);
extern void 	loginUserShow (void);

/* Private prototype */

extern STATUS	loginPrompt2 (char * userName, BOOL withTimeout);
#endif /* _WRS_KERNEL */

#ifdef __cplusplus
}
#endif

#endif /* __INCloginLibh */
