/*
**
**  CCI STATUS definitions. 
**
*/
/* cci.h - Main CCI header file				*/

/* 
Copyright (c) 2005 Wind River Systems, Inc. 

The right to copy, distribute, modify or otherwise make use 
of this software may be licensed only pursuant to the terms 
of an applicable Wind River license agreement. 
*/

/*
DESCRIPTION
This file is the main include header for CCI. All relevent headers are to be referenced
from this file.
*/

/*
DESCRIPTION
This module implements CCI macros routines.

INCLUDE: cci.h
*/


/*
modification history
--------------------
01a,26sep05,cdw  Remapped sdLib calls
01Jun22,ralvarez	- Merged multiple headers into single header.
03Apr02,ralvarez	- Initial code checkin.
17Aug05,ralvarez	- Added CCI_AES_XCBC_DIGESTSIZE.

*/
#ifndef CCI_INCLUDED
#define CCI_INCLUDED


#define CCI_MAJOR_VER	3
#define CCI_MINOR_VER	1


#ifdef __cplusplus
extern "C"
{
#endif

#include "stdio.h"								/* Standard I/O header					*/
#include "stdlib.h"								/* Standard library header				*/
#include <stdarg.h>								/* variable argument header				*/
#include <string.h>								/* string processing header				*/
										
/*
** --- Non-VxWorks operating system headers (BSD, WIN32, etc...)
*/	
#ifndef WIN32
#ifndef VXWORKS
#define VXWORKS
#endif
#endif

#ifndef VXWORKS
#include <sys/timeb.h>							/* time structures						*/
#include <windows.h>							/* Standard Windows header(s)			*/
#include <io.h>									/* Windows I/O definitions				*/
#include <process.h>							/* Windows task/thread definitions		*/
#include <time.h>								
												
#define CCI_LITTLE_ENDIAN						
												
/*
** --- VxWorks headers
*/	
#else											
#include "vxWorks.h"							/* Standard vxWorks header(s)			*/
#include "semLib.h"								/* vxWorks semaphore header				*/
#include "time.h"								/* time structures						*/
#include "taskLib.h"							/* task/thread definitions				*/

#ifndef _BYTE_ORDER
#error  '_BYTE_ORDER' macro not defined!!!
#endif

/*
** --- For VxWorks, Check endianness of platform
*/ 
#if _BYTE_ORDER == _BIG_ENDIAN
#undef CCI_LITTLE_ENDIAN
#else
#define CCI_LITTLE_ENDIAN
#endif
#endif

#ifdef	_WRS_VXWORKS_MAJOR				/* Building in kernel for 6.x and above				*/
#define CCI_SHAREDMEM_REGION			/* Running in RTP/KERNEL share mode					*/
#define CCI_ENVIRONMENT_KERNEL			/* Build for kernel instance						*/
#else 	
#ifdef  _WRS_KERNEL
#define CCI_ENVIRONMENT_KERNEL			/* By default, we're always running in kernel mode	*/
#else						/* RTP		*/
#define CCI_SHAREDMEM_REGION			/* Running in RTP/KERNEL share mode					*/
#define CCI_ENVIRONMENT_PROCESS			/* Build for RTP instance							*/
#endif
#endif
/*
** --- CCI Data types
*/
typedef void			*cci_g;					/* Opague binary byte stream object.	*/
typedef cci_g			CCISessionCtx;			/* CCI Session context					*/
typedef unsigned long	cci_l;					/* Unsigned long data type				*/
typedef unsigned int	cci_t;					/* Unsigned integer data type			*/
typedef unsigned char	cci_b;					/* Unsigned byte data type				*/
typedef unsigned short	cci_w;					/* Unsigned short data type				*/
typedef unsigned char	*cci_z;					/* NULL-terminated ASCII string object	*/
typedef unsigned int	CCI_CLASS_ID;			/* CCI CLASS object type				*/
typedef unsigned int	CCI_ALGORITHM_ID;		/* CCI Algorithm object type			*/
typedef unsigned int	CCI_ATTRIBUTE_ID;		/* CCI attribute object type			*/
typedef unsigned int	CCI_PROVIDER_ID;		/* CCI provider object type				*/
typedef unsigned int	cci_bool;				/* Boolean TRUE=1/FALSE=0 data type		*/
typedef unsigned char	CCISecurityTag;			/* CCI security tag						*/
												
												
#ifndef VXWORKS
typedef HANDLE			CCIMutex;				/* non-vxworks Mutex object				*/
typedef HANDLE			CCISemaphore;			/* non-vxworks Semaphire object			*/
typedef HANDLE			CCIEvent;				/* non-vxworks Trigger/event object		*/
typedef unsigned long	CCITaskId;				/* non-vxworks Thread/task object		*/
#else											
typedef SEM_ID			CCIMutex;				/* VxWorks Mutex object					*/
typedef SEM_ID			CCISemaphore;			/* VxWorks Semaphire object				*/
typedef SEM_ID			CCIEvent;				/* VxWorks Trigger/event object			*/
typedef int				CCITaskId;				/* VxWorks Thread/task object			*/
#endif

#ifdef CCI_LIBRARY_INIT
#define _EXTERN 
#else
#define _EXTERN extern
#endif

_EXTERN CCI_PROVIDER_ID			CCI_APP_PROVIDER_ID;				/* Default application provider			*/
_EXTERN CCI_PROVIDER_ID			CCI_DEF_PROVIDER_ID;				/* Default CCI provider					*/
_EXTERN int						(*cci_default_rng_seed_functptr)();	/* User's RNG seeding function			*/


#define CCI_SUCCESS	S_cciLib_SUCCESS
#define CCI_FAILURE	S_cciLib_FAILURE

#ifndef __max									/* not implemented in VxWorks 					*/
#define __max(a,b)  (((a) > (b)) ? (a) : (b))
#endif
#ifndef __min									/* not implemented in VxWorks 					*/
#define __min(a,b)  (((a) < (b)) ? (a) : (b))
#endif


/*
** --- GMP GNU mpz macros
*/
typedef unsigned char							mpz_bool;
#define CCI_ASYNC_IS_SUPPORTED

#define CCI_ATTR_BIT_SET( bits, attr )			(bits[attr/32] |= (1<<(attr%32)))
#define CCI_ATTR_BIT_CLR( bits, attr )			(bits[attr/32] &=~(1<<(attr%32)))
#define CCI_IS_ATTR_BIT_SET( bits, attr )		(bits[attr/32] & (1<<(attr%32)))

/*
** --- Control register bit masks
*/
#define CCI_CTRL_FINAL_MSK						0x00000001		/* Finalalize context		*/	
#define CCI_CTRL_INIT_MSK						0x00000002		/* Initialize context		*/
#define CCI_CTRL_ALGID_MSK						0x000003FC		/* Algorithm ID				*/
#define CCI_CTRL_CCLASS_MSK						0x00003C00		/* Crypto Class ID			*/
#define CCI_CTRL_PROVID_MSK						0x0003C000		/* Provider ID				*/
#define CCI_CTRL_MISSATT_MSK					0x03FC0000		/* Missing attribute		*/
#define CCI_CTRL_RTP_REQ_MSK					0x04000000		/* RTP request				*/
#define CCI_CTRL_KRNL_MEM_MSK					0x08000000		/* RTP w/kernel memory		*/
#define CCI_CTRL_MESSAGE_MSK					0x10000000		/* RTP message to kernel	*/
#define CCI_CTRL_UNUSED_MSK						0xE0000000		/* Unused bits				*/

#define CCI_CTRL_FINAL_SET( ctx )				((ctx)->control |= CCI_CTRL_FINAL_MSK)
#define CCI_CTRL_FINAL_CLR( ctx )				((ctx)->control &=~CCI_CTRL_FINAL_MSK)
#define CCI_IS_CTRL_FINAL_SET( ctx )			((ctx)->control & CCI_CTRL_FINAL_MSK)
#define CCI_CTRL_INIT_SET( ctx )				((ctx)->control |= CCI_CTRL_INIT_MSK)
#define CCI_CTRL_INIT_CLR( ctx )				((ctx)->control &=~CCI_CTRL_INIT_MSK)
#define CCI_IS_CTRL_INIT_SET( ctx )				((ctx)->control & CCI_CTRL_INIT_MSK)

#define CCI_CTRL_MESSAGE_SET( ctx )				((ctx)->control |= CCI_CTRL_MESSAGE_MSK)
#define CCI_CTRL_MESSAGE_CLR( ctx )				((ctx)->control &=~CCI_CTRL_MESSAGE_MSK)
#define CCI_IS_CTRL_MESSAGE_SET( ctx )			((ctx)->control & CCI_CTRL_MESSAGE_MSK)

#define CCI_CTRL_RTP_REQ_SET( ctx )				((ctx)->control |= CCI_CTRL_RTP_REQ_MSK)
#define CCI_CTRL_RTP_REQ_CLR( ctx )				((ctx)->control &=~CCI_CTRL_RTP_REQ_MSK)
#define CCI_IS_CTRL_RTP_REQ_SET( ctx )			((ctx)->control & CCI_CTRL_RTP_REQ_MSK)

#define CCI_CTRL_KRNL_MEM_SET( ctx )			((ctx)->control |= CCI_CTRL_KRNL_MEM_MSK)
#define CCI_CTRL_KRNL_MEM_CLR( ctx )			((ctx)->control &=~CCI_CTRL_KRNL_MEM_MSK)
#define CCI_IS_CTRL_KRNL_MEM_SET( ctx )			((ctx)->control & CCI_CTRL_KRNL_MEM_MSK)


#define CCI_CTRL_ALGID_SET( ctx, algId )		((ctx)->control |= (CCI_CTRL_ALGID_MSK & ((int)algId<<2)))
#define CCI_CTRL_CCLASS_SET( ctx, classId )		((ctx)->control |= (CCI_CTRL_CCLASS_MSK & ((int)classId<<10)))
#define CCI_CTRL_PROVID_SET( ctx, provId )		((ctx)->control |= (CCI_CTRL_PROVID_MSK & ((int)provId<<14)))
#define CCI_CTRL_MISSATT_SET( ctx, missAttr )	((ctx)->control |= (CCI_CTRL_MISSATT_MSK & ((int)missAttr<<18)))

#define CCI_CTRL_ALGID( ctx )					((((ctx)->control & CCI_CTRL_ALGID_MSK) >> 2))
#define CCI_CTRL_CCLASS( ctx )					((((ctx)->control & CCI_CTRL_CCLASS_MSK) >> 10))
#define CCI_CTRL_PROVID( ctx )					((((ctx)->control & CCI_CTRL_PROVID_MSK) >> 14))
#define CCI_CTRL_MISSATT( ctx )					((((ctx)->control & CCI_CTRL_MISSATT_MSK) >> 18))


#define CCI_DEFAULT_PROVIDER					ccip_default_provider
#define CCI_RNG_PROVIDER_ID( pid )				(cciIsAlgorithmSet( pid, CCI_CLASS_RNG, CCI_RNG_GENERIC ) ? pid : CCI_DEF_PROVIDER_ID)
#define CCI_IS_ATTRIBUTE_SET(ctx,attr)			(CCI_IS_ATTR_BIT_SET((ctx)->attribMask, attr))



#define __iterator( a, b )						for( a = 0; a < b; a++ )
#define _CLASS( macro )							CCI_CLASS_##macro
#define _MACRO_IN_RANGE( m, max )				( (((int)m >= 0) && (m < max)) ? TRUE : FALSE )
#define _TIME_SET( a )							( a = ccip_time_ms() )
#define _TIME_DELTA( a, b )						( ((double)((double)(b - a) / (double)(1000))) )
#define _ALGORITHM( c, a )						( provider->algorithms[c][a] )
#define _CLASS_ALGORITHM_MAX( c )				( cryptoClassTable[c].algorithmMax )
#define _CLASS_ATTRIBUTE_MAX( c )				( cryptoClassTable[c].attributeMax )
#define _CLASS_ATTRIBUTE_NAME(c,a)				( cryptoClassTable[c].attributeTable[a].name )
#define _CLASS_ATTRIBUTE_DESC(c,a)				( cryptoClassTable[c].attributeTable[a].desc )
#define _IS_PROVIDER_VALID( p ) \
		( ((p < _ginstanceCtx->providerMax) && _ginstanceCtx->providerList[p]) ? TRUE : FALSE )
#define _IS_CLASS_VALID(c)						( _MACRO_IN_RANGE( c, _CLASS( MAX ) ) )
#define _IS_ALGORITHM_VALID(c, alg)				( _MACRO_IN_RANGE( alg, _CLASS_ALGORITHM_MAX( c ) ) )
#define CCISUCCESS( statusCode )				( statusCode == CCI_SUCCESS ? TRUE : FALSE )
#define CCI_PROVIDER_NAME( p )					( cciProviderName(p) )
#define CCI_DEFAULT_PROVIDER_NAME				"DEFAULT_PROVIDER"
#define CCI_SHOW_BUFFER( title, buffer, length )( ccip_show_buffer( title, buffer, length, 16 ));
#define CCI_IS_READY							( _ginstanceCtx!=NULL ? TRUE : FALSE )
#define CCI_PARAM_NOT_USED(a)					(a=a)


#ifndef VXWORKS
#define	ccip_mutex_create( mtx )				((*mtx) = ( CCIMutex )CreateMutex( 0, FALSE, 0 ))
#define ccip_mutex_lock( mtx )					(WaitForSingleObject( mtx, INFINITE ))
#define ccip_mutex_unlock( mtx )				(ReleaseMutex( mtx ))
#define ccip_mutex_destroy( mtx )				(CloseHandle( mtx ))
#define ccip_sem_create( sem, ic, max )			((*sem) = (CCISemaphore)CreateSemaphore( 0, ic, max, NULL ))
#define ccip_sem_wait( sem )					(WaitForSingleObject(sem,INFINITE))
#define ccip_sem_release( sem )					(ReleaseSemaphore( sem, 1, NULL ))
#define ccip_sem_destroy( sem )					(CloseHandle( sem ))
#define ccip_event_create( event )				(ccip_sem_create( event, 0, 1 ))
#define ccip_event_wait( event )				(ccip_sem_wait( event ))
#define ccip_event_release( event )				(ccip_sem_release( event ))
#define ccip_event_destroy( event )				(ccip_sem_destroy( event ))
#define ccip_task_create( tid, name, pri, funct, args )	((*tid) = ( CCITaskId )_beginthread( funct, 0, args ))
#define ccip_task_exists( name )				(0)
#define ccip_task_delete( taskId )				
#else
#define	ccip_mutex_create( mtx )				((*mtx) = ( CCIMutex )semBCreate( SEM_Q_FIFO, SEM_FULL ))
#define ccip_mutex_lock( mtx )					(semTake(mtx,WAIT_FOREVER ))
#define ccip_mutex_unlock( mtx )				(semGive( mtx ))
#define ccip_mutex_destroy( mtx )				(semDelete( mtx ))
#define ccip_sem_create( sem, ic, max )			((*sem) = (CCISemaphore)semCCreate( 0, ic ))
#define ccip_sem_wait( sem )					(semTake( sem, WAIT_FOREVER ))
#define ccip_sem_release( sem )					(semGive( sem ))
#define ccip_sem_destroy( sem )					(semDelete( sem ))
#define ccip_event_create( event )				((*event) = ( CCIEvent )semBCreate( SEM_Q_FIFO, SEM_EMPTY ))
#define ccip_event_wait( event )				(semTake( event, WAIT_FOREVER ))
#define ccip_event_release( event )				(semGive( event ))
#define ccip_event_destroy( event )				(semDelete( event ))
#define ccip_task_create( tid, name, pri, funct, args )	((*tid) = ( CCITaskId )taskSpawn( name, pri, 0, 20000, (FUNCPTR) funct, (int)args, (int)NULL,(int)NULL,(int)NULL,(int)NULL,(int)NULL,(int)NULL,(int)NULL,(int)NULL,(int)NULL ))
#define ccip_task_exists( name )				((taskNameToId( name ) == ERROR) ? 0:1 )
#define ccip_task_delete( taskId )				(taskDelete(taskId))
#endif

#define ccip_zero( memblock, size )				(memset( memblock, 0, size ))


/*
** --- Public interface macros
*/

/*******************************************************************************
*
* cciCtxAlgorithmId - Get algorithm type (macro).
*
* Returns algorithm identifier (CCI_CIPHER_DES, CCI_HASH_SHA1, etc... ) that 
* is associated with a CCIContext object.
*
* SYNOPSIS: 
* \cs
* CCI_ALGORITHM_ID cciCtxAlgorithmId
*	(
*	CCIContext ctx   // CCIContext object 
*	)
* \ce
* 
* Parameters:
* \is
* \i <CCIContext cciContext> 
* CCIContext object that was created with cciCtxCreate().
*
* \ie
*
* RETURNS: Returns identifier of algorithm. Identifier returned depends on crypto 
* class of the CCIContext.
*
* ERRNO: N/A
*
*
* EXAMPLE:
* \cs
* //
* // --- Show how to determine algorithm type from a CCIContext.
* //
* void some_function( CCIContext ctx )
* {
* 	CCI_ALGORITHM_ID algorithmId;
* 
*	//
*	// --- Make decision based on algorithm type...
*	//
*	algorithmId = cciCtxAlgorithmId( ctx );
*	if ( algorithmId == CCI_CIPHER_3DES )
*	{
*		......
*	}
* }
* \ce
* SEE ALSO: cciCtxProviderId(), cciCtxClassId()
*
*/
#define cciCtxAlgorithmId( ctx )				(CCI_CTRL_ALGID( ctx ))


/*******************************************************************************
*
* cciCtxClassId - Get crypto class (macro).
*
* Returns crypto class identifier (CCI_CLASS_CIPHER, CCI_CLASS_HASH, etc... ) 
* that is associated with a CCIContext object.
*
* SYNOPSIS: 
* \cs
* CCI_CLASS_ID cciCtxClassId
*	(
*	CCIContext ctx   // CCIContext object 
*	)
* \ce
*
* Parameters:
* \is
* \i <CCIContext cciContext> 
* CCIContext object that was created with cciCtxCreate().
*
* \ie
*
* RETURNS: Returns CCI crypto class identifier. Possible values are 
* CCI_CLASS_CIPHER, CCI_CLASS_HASH, CCI_CLASS_HMAC, CCI_CLASS_PUBLICKEY, 
* CCI_CLASS_RNG and CCI_CLASS_COMPRESSION.
*
* ERRNO: N/A
*
* EXAMPLE:
* \cs
* //
* // --- Show how to determine the CCI_CLASS of a CCIContext.
* //
* void some_function( CCIContext ctx )
* {
*	CCI_CLASS_ID classId;
*
*	//
*	// --- Check ''class'' of request...
*	//
*	classId = cciCtxClassId( ctx );
*	if ( classId == CCI_CLASS_HMAC )
*	{
*	    ......
*	}
* }
* \ce
* SEE ALSO: cciCtxProviderId(), cciCtxAlgorithmId()
*
*/
#define cciCtxClassId( ctx )					(CCI_CTRL_CCLASS( ctx ))


/*******************************************************************************
*
* cciCtxProviderId - Get provider Id (macro).
*
* Returns crypto provider identifier that is associated with a CCIContext. By 
* default, there are at least two possible values that cciCtxProviderId() 
* may return:
*
*	CCI_DEF_PROVIDER_ID -- Default software provider module.
*	CCI_APP_PROVIDER_ID -- Provider IDE selected by application
*
* Other values may be returned if three or more providers are loaded 
* simultaneously.
*
* SYNOPSIS: 
* \cs
* CCI_PROVIDER_ID cciCtxProviderId
*	(
*	CCIContext ctx   // CCIContext object 
*	)
* \ce
*
* Parameters:
* \is
* \i <CCIContext cciContext> 
* CCIContext object that was created with cciCtxCreate().
*
* \ie
*
* RETURNS: Returns identifier of provider. 
*
* ERRNO: N/A
*
* EXAMPLE:
* \cs
* //
* // --- Show how to get the provider Id from a CCIContext.
* //
* void some_function( CCIContext ctx )
* {
*	CCI_PROVIDER_ID providerId;
*
*	providerId = cciCtxProviderId( ctx );
*	if ( providerId == CCI_DEF_PROVIDER_ID )
*	{
*		......
*	}
* }
* \ce
* SEE ALSO: cciCtxAlgorithmId(), cciCtxClassId()
*
*/
#define cciCtxProviderId( ctx )					(CCI_CTRL_PROVID( ctx ))

/*******************************************************************************
*
* cciCtxIsAttrSet - Check attribute status (macro).
*
* Returns boolean indication if specified atttribute has been set in 
* CCIContext object.
*
* SYNOPSIS: 
* \cs
* BOOL cciCtxIsAttrSet
*	(
*	CCIContext ctx,   // CCIContext object 
*       CCI_ATTRIBUTE_ID id // attribute identifier
*	)
* \ce
*
* Parameters:
* \is
* \i <CCIContext cciContext> 
* CCIContext object that was created with cciCtxCreate().
*
* \i <CCI_ATTRIBUTE_ID attributeId> 
* Identifier of attribute that is to be checked. The attribute id value is
* dependant on the type of CCIContext object.
*
* \ie
*
* RETURNS: Returns TRUE if attribute is set, Otherwise, FALSE. 
*
* ERRNO: N/A
*
* EXAMPLE:
* \cs
* //
* // --- Show how to check if attribute is set in a CCIContext.
* //
* void some_function( CCIContext ctx )
* {
* 	if (cciCtxIsAttrSet( ctx, CCI_CIPHER_BLOCK_SIZE ))
* 		cciCtxAttrClr( ctx, CCI_CIPHER_BLOCK_SIZE );
* }
* \ce
* SEE ALSO: cciCtxAttrClr(), cciCtxAttrGet(), cciCtxAttrSet()
*
*/
#define cciCtxIsAttrSet(ctx,attr)				(CCI_IS_ATTRIBUTE_SET(ctx,attr))

/*******************************************************************************
*
* cciCtxAttrClr - Clear attribute (macro).
*
* If set, clears specified attribute in CCIContext object.
*
* SYNOPSIS: 
* \cs
* void cciCtxAttrClr
*	(
*	CCIContext ctx,   // CCIContext object 
*       CCI_ATTRIBUTE_ID id // attribute identifier
*	)
* \ce
*
* Parameters:
* \is
* \i <CCIContext cciContext> 
* CCIContext object that was created with cciCtxCreate().
*
* \i <CCI_ATTRIBUTE_ID attributeId> 
* Identifier of attribute.
*
* \ie
*
* RETURNS: N/A 
*
* ERRNO: N/A
*
* EXAMPLE:
* \cs
* //
* // --- Show how to clear attribute flag in CCIContext.
* //
* void some_function( CCIContext ctx )
* {
* 	if (cciCtxIsAttrSet( ctx, CCI_CIPHER_BLOCK_SIZE ))
* 		cciCtxAttrClr( ctx, CCI_CIPHER_BLOCK_SIZE );
* }
* \ce
* SEE ALSO: cciCtxIsAttrSet(), cciCtxAttrGet(), cciCtxAttrSet()
*
*/
#define cciCtxAttrClr( ctx, attr )				{(ctx)->attributes[attr] = (void *)NULL;(CCI_ATTR_BIT_CLR((ctx)->attribMask, attr));}

/*******************************************************************************
*
* cciCtxAttrGet - Get attribute value (macro).
*
* Retrieve attribute value from CCIContext. The value parameter MUST be the 
* address of a variable.
*
* SYNOPSIS: 
*
* \cs
* cci_st cciCtxAttrGet
*	(
*	CCIContext ctx,   // CCIContext object 
*       CCI_ATTRIBUTE_ID id, // attribute identifier
*	void *value	     // output variable
*	)
* \ce
*
* Parameters:
* \is
* \i <CCIContext cciContext> 
* CCIContext object that was created with cciCtxCreate().
*
* \i <CCI_ATTRIBUTE_ID attributeId> 
* Identifier of attribute.
*
* \i <void *value> 
* Address of variable where value is to be set into.
*
* \ie
*
* RETURNS: CCI_SUCCESS if attribute value was successfully retrieved. 
* Otherwise, CCI_FAILURE. 
*
* ERRNO: N/A
*
* EXAMPLE:
* \cs
* //
* // --- Show how to get attribute from CCIContext object.
* //
* void some_function( CCIContext ctx )
* {
* 	CCI_CLASS_ID classId;
* 	cci_b *cipherKey;
* 	cci_t cipherKeyLength, cryptoMode;
* 
*	//
* 	// --- Check 'class' of request...
* 	//
* 	classId = cciCtxClassId( ctx );
* 	if ( classId == CCI_CLASS_CIPHER )
* 	{
* 		cciCtxAttrGet( ctx, CCI_CIPHER_KEY, &cipherKey );
* 		cciCtxAttrGet( ctx, CCI_CIPHER_KEY_LENGTH, &cipherKeyLength );
* 		cciCtxAttrGet( ctx, CCI_CIPHER_MODE, &cryptoMode );
* 	}
* }
* \ce
* SEE ALSO: cciCtxIsAttrSet(), cciCtxAttrClr(), cciCtxAttrSet()
*
*/
#define cciCtxAttrGet( ctx, attr, value )		(ccip_ctx_get_attr( ctx, attr, (void *)value ))

/*******************************************************************************
*
* cciCtxAttrSet - Set attribute value (macro).
*
* Set attribute value into CCIContext. All attribute values MUST be passed 
* in by value.
*
* SYNOPSIS:
* \cs
* void cciCtxAttrSet
*	(
*	CCIContext ctx,   // CCIContext object 
*       CCI_ATTRIBUTE_ID id, // attribute identifier
*	cci_t value	     // attribute value to set
*	)
* \ce
*
* Parameters:
* \is
* \i <CCIContext cciContext> 
* CCIContext object that was created with cciCtxCreate().
*
* \i <CCI_ATTRIBUTE_ID attributeId> 
* Identifier of attribute.
*
* \i <'type-specific' value> 
* Variable that is associated with attribute id.
*
* \ie
*
* RETURNS: N/A 
*
* ERRNO: N/A
*
* EXAMPLE:
* \cs
* //
* // --- Show how to set attribute value in CCIContext object.
* //
* void some_function( CCIContext ctx )
* {
* 	CCI_CLASS_ID classId;
* 	cci_b plaintText[100];
* 	cci_t plaintTextLength = sizeof(plaintText);
* 
*	//
* 	// --- Check 'class' of request...
* 	//
* 	classId = cciCtxClassId( ctx );
* 	if ( classId == CCI_CLASS_CIPHER )
* 	{
* 		cciCtxAttrSet( ctx, CCI_INPUT_BUFFER, plaintText );
* 		cciCtxAttrSet( ctx, CCI_INPUT_LENGTH, plaintTextLength );
* 	}
* }
* \ce
* SEE ALSO: cciCtxAttrGet(), cciCtxAttrClr(), cciCtxIsAttrSet()
*
*/
#define cciCtxAttrSet( ctx, attr, val)			{(ctx)->attributes[attr] = (void *)val;(CCI_ATTR_BIT_SET((ctx)->attribMask, attr));}


/*******************************************************************************
*
* cciCtxInputGet - Get input buffer data and length (macro).
*
* Retrieve input buffer address and length information from CCIContext. 
* Addresses of both 'buffer' and 'bufferLength' variables MUST be passed.
* 
* Using cciCtxInputGet() has the same effect as using cciCtxAttrGet() to get
* the input buffer and lenth attribute values.
*
* SYNOPSIS:
* \cs
* void cciCtxInputGet
*	(
*	CCIContext ctx,   // CCIContext object 
*       void **buffer,   // Address of pointer variable
*	cci_t *bufferLength	     // Address of length variable
*	)
* \ce
*
* \cs
* void some_function( CCIContext ctx )
* {
* 	cci_b *plaintText;
* 	cci_t plaintTextLength;
* 
* 	cciCtxAttrGet( ctx, CCI_INPUT_BUFFER, &plaintText );
* 	cciCtxAttrGet( ctx, CCI_INPUT_LENGTH, &plaintTextLength );  
* }
* \ce
* Parameters:
* \is
* \i <CCIContext cciContext> 
* CCIContext object that was created with cciCtxCreate().
*
* \i <void **buffer> 
* Address of pointer variable.
*
* \i <cci_t *bufferLength> 
* Address of length variable.
*
* \ie
*
* RETURNS: N/A 
*
* ERRNO: N/A
*
* EXAMPLE:
* \cs
* //
* // --- Show how to determine length of message digest.
* //
* void some_function( CCIContext ctx )
* {
* 	CCI_CLASS_ID classId;
* 	cci_b *plaintText;
* 	cci_t plaintTextLength;
* 
*	//
* 	// --- Check class of request...
* 	//
* 	classId = cciCtxClassId( ctx );
* 	if ( classId == CCI_CLASS_CIPHER )
* 	{
* 		cciCtxInputGet( ctx, &plaintText, &plaintTextLength );
* 
*		//
* 		// --- Process request using plaintText and plaintTextLength information
* 		//
* 		....
* 	}
* }
* \ce
* SEE ALSO: cciCtxAttrGet(), cciCtxOutputGet()
*
*/
#define cciCtxInputGet( ctx, buffer, bufferLength )	{cciCtxAttrGet( ctx, CCI_INPUT_BUFFER, buffer );cciCtxAttrGet( ctx, CCI_INPUT_LENGTH, bufferLength );}


/*******************************************************************************
*
* cciCtxOutputGet - Get output buffer data and length (macro).
*
* Retrieve output buffer and buffer length data from CCIContext. Addresses of 
* both 'buffer' and 'bufferLength' variables MUST be passed.
* 
* Using cciCtxOutputGet() has the same effect as using cciCtxAttrGet() to get
* the output buffer and lenth attribute values.
* \cs
* void some_function( CCIContext ctx )
* {
* 	cci_b *cipherText;
* 	cci_t cipherTextLength;
* 
* 	cciCtxAttrGet( ctx, CCI_OUTPUT_BUFFER, &cipherText );
* 	cciCtxAttrGet( ctx, CCI_OUTPUT_LENGTH, &cipherTextLength );  
* }
* \ce
* 
* SYNOPSIS:
* \cs
* void cciCtxOutputGet
*	(
*	CCIContext ctx,   // CCIContext object 
*       void **buffer,   // Address of pointer variable
*	cci_t *bufferLength	     // Address of length variable
*	)
* \ce
*
* Parameters:
* \is
* \i <CCIContext cciContext> 
* CCIContext object that was created with cciCtxCreate().
*
* \i <void **buffer> 
* Address of pointer variable.
*
* \i <cci_t *bufferLength> 
* Address of length variable.
*
* \ie
* RETURNS: N/A 
*
* ERRNO: N/A
*
* EXAMPLE:
* \cs
* //
* // --- Show how extract output buffer data from CCIContext object.
* //
* void some_function( CCIContext ctx )
* {
* 	CCI_CLASS_ID classId;
* 	cci_b *cipherText;
* 	cci_t cipherTextLength;
* 
*	//
* 	// --- Check class of request...
* 	//
* 	classId = cciCtxClassId( ctx );
* 	if ( classId == CCI_CLASS_CIPHER )
* 	{
* 		//
* 		// --- Get processed cipher text....
* 		//
* 		cciCtxOutputGet( ctx, &cipherText, &cipherTextLength );
* 	}
* }
* \ce
* SEE ALSO: cciCtxAttrGet(), cciCtxOutputGet()
*
*/
#define cciCtxOutputGet( ctx, buffer, bufferLength )		{cciCtxAttrGet( ctx, CCI_OUTPUT_BUFFER, buffer );cciCtxAttrGet( ctx, CCI_OUTPUT_LENGTH, bufferLength );}



/*******************************************************************************
*
* cciCtxMissingAttrSet - Set missing attribute identifier (macro).
*
* Set missing attribute identifier in CCIContext. This function is used by a 
* CPI provider module to inform the application that a required attribute was 
* not set in the CCIContext crypto request. In order for the application to know 
* to use this function, the CPI module must return 
* an 'S_cciLib_MISSING_ATTRIBUTE' error.
*
* SYNOPSIS:
* \cs
* void cciCtxMissingAttrSet
*	(
*	CCIContext ctx,   // CCIContext object 
*       CCI_ATTRIBUTE_ID attributeId   // Identifier of attribute
*	)
* \ce
*
* Parameters:
* \is
* \i <CCIContext cciContext> 
* CCIContext object that was created with cciCtxCreate().
*
* \i <CCI_ATTRIBUTE_ID attributeId> 
* Identifier of attribute.
*
* \ie
*
* RETURNS: N/A 
*
* ERRNO: N/A
*
* EXAMPLE:
* \cs
* //
* // --- Show how to set missing attribute in CCIContext object.
* //
* cci_st some_function( CCIContext ctx )
* {
* 	CCI_CLASS_ID classId;
* 	cci_b *cipherKey;
* 	cci_t cipherKeyLength, cryptoMode;
* 	cci_st cciStatus;
* 
*	classId = cciCtxClassId( ctx );
* 	if ( classId == CCI_CLASS_CIPHER )
* 	{
* 		//
* 		// --- Make sure CCI_CIPHER_KEY attribute is set.
* 		//
* 		cciStatus = cciCtxAttrGet( ctx, CCI_CIPHER_KEY, &cipherKey );
* 		if ( cciStatus != CCI_SUCCESS )
* 		{
* 			//
* 			// --- Notify application that CCI_CIPHER_KEY attribute was not set.
* 			//
* 			cciCtxMissingAttrSet( ctx, CCI_CIPHER_KEY );
* 			return( S_cciLib_MISSING_ATTRIBUTE );
* 		}
* 	}
* }
* \ce
* SEE ALSO: cciCtxMissingAttrGet()
*
*/
#define cciCtxMissingAttrSet( ctx, attrId ) (CCI_CTRL_MISSATT_SET( ctx, attrId ))


/*******************************************************************************
*
* cciCtxMissingAttrGet - Get missing attribute identifier (macro).
*
* Get missing attribute from CCIContext. When a crypto request returns 
* a 'S_cciLib_MISSING_ATTRIBUTE', use the 'cciCtxMissingAttrGet' to retrieve 
* the ordinal value of the missing attribute.
*
* SYNOPSIS:
* \cs
* CCI_ATTRIBUTE_ID cciCtxMissingAttrGet
*	(
*	CCIContext ctx   // CCIContext object 
*	)
* \ce

* Parameters:
* \is
* \i <CCIContext cciContext> 
* CCIContext object that was created with cciCtxCreate().
*
* \ie
* RETURNS: Identifier of missing attribute.
*
* ERRNO: N/A
*
* EXAMPLE:
* \cs
* //
* // --- Show how to get missing attribute from CCIContext object.
* //
* cci_st some_function( CCIContext ctx )
* {
* 	CCI_ATTRIBUTE_ID attrId;
* 	cci_st cciStatus;
* 
*	cciStatus = cciCipher( ctx, ... );
* 	if ( cciStatus == S_cciLib_MISSING_ATTRIBUTE )
* 	{
* 		//
* 		// --- Oooops, forget to set required attribute.
* 		//
* 		attrId = cciCtxMissingAttrGet( ctx );
* 		printf("Missing '%u' attribute in cipher request\n", attrId ); 
* 		return( CCI_FAILURE );
* 	}
* }
* \ce
* SEE ALSO: cciCtxMissingAttrSet()
*
*/
#define cciCtxMissingAttrGet( ctx )				(CCI_CTRL_MISSATT( ctx ))


/*******************************************************************************
*
* cciProviderAttrSet - Set provider parameter (macro).
*
* Set provider-specific parameter into CCIProvider. Possible values include:
*
*	CCI_PVR_SOFTWARE_DEFAULT				-- Use default s/w algorithms
*	CCI_PVR_SECURITY_PUBLIC_KEY				-- Provider verification private key
*	CCI_PVR_SECURITY_SIGNATURE				-- Provider signature used for verification
*	CCI_PVR_SECURITY_SIGNATURE_LEN			-- Provider signature length
*	CCI_PVR_SECURITY_TAG_TABLE				-- Security tag table
*
* SYNOPSIS:
* \cs
* void cciProviderAttrSet
*	(
*	CCIProvider provider,   // CCIProvider object
*	CCI_PROVIDER_PARAM attributeId, // Identifier of parameter
*	cci_t value		// Parameter value to be set
*	)
* \ce
*
* Parameters:
* \is
* \i <CCIProvider provider> 
* CCIProvider object.
*
* \i <CCI_PROVIDER_PARAM attributeId> 
* Identifier of parameter.
*
* \i <'type-specific' value> 
* Variable that is associated with parameter id.
*
* \ie
*
* RETURNS: N/A 
*
* ERRNO: N/A
*
* EXAMPLE:
* \cs
* //
* // --- Create Provider Export Module. 
* //
* CCIProvider Mpc190ExportCCIProvider( void )
* {
* 	CCIProvider		Mpc190Provider = ( CCIProvider ) NULL;
* 	cci_st cciStatus;
* 
*	//
* 	// --- Create CCIProvider object for MPC190 Security Processor
* 	//
* 	cciStatus = cciProviderCreate( &Mpc190Provider, MPC190_PROVIDER_NAME, 
*                                mpc190ProviderInit, mpc190ProviderUninit );
* 	if( CCISUCCESS( cciStatus ) )
* 	{
* 
*		//
* 		// --- Create public key that reference RSA public key.
* 		//
* 		cciPKIKeyCreate( CCI_DEF_PROVIDER_ID, CCI_RSA_PUBLIC_KEY, &publicKey );
* 		cciPKIKeyCompSet( publicKey, CCI_RSA_PUBLIC_EXPONENT,  cci_public_key_e, 
*                                           sizeof(cci_public_key_e));
* 		cciPKIKeyCompSet( publicKey, CCI_RSA_MODULAS,  cci_public_key_n, 
*                                           sizeof(cci_public_key_n));
* 
*		//
* 		// --- Register CPI Digital Signature (provider by WRS)
* 		//
* 		cciProviderAttrSet( Mpc190Provider, CCI_PVR_SECURITY_SIGNATURE, 
*                                                   cci_provider_signature );
* 		cciProviderAttrSet( Mpc190Provider, CCI_PVR_SECURITY_SIGNATURE_LEN, 256 );
* 		cciProviderAttrSet( Mpc190Provider, CCI_PVR_SECURITY_PUBLIC_KEY, 
*                                                   publicKey );
* 		cciProviderAttrSet( Mpc190Provider, CCI_PVR_SECURITY_TAG_TABLE, 
*                                                   security_tag_table );
* 
*		// 
* 		// --- Create CCIAlgorithm object, set required attributes and default 
*       //     values, set processing handler and register with CCIProvider 
*       //     object.
* 		//
* 		cciStatus = cciAlgorithmCreate( &algorithm, CCI_CLASS_CIPHER, 
*                                                       CCI_CIPHER_DES );
* 		if( CCISUCCESS( cciStatus ) )
* 		{
* 			// 
* 			// --- Set required attributes and default values.
* 			//
* 			cciAlgorithmAttrSet( algorithm, CCI_ASYNC_SUPPORT, TRUE );
* 			cciAlgorithmAttrSet( algorithm, CCI_CIPHER_OPERATION, NULL );
* 			cciAlgorithmAttrSet( algorithm, CCI_CIPHER_MODE, CCI_MODE_CBC );
* 			cciAlgorithmAttrSet( algorithm, CCI_CIPHER_KEY, NULL );
* 			cciAlgorithmAttrSet( algorithm, CCI_CIPHER_KEY_LENGTH, 8 );
* 			cciAlgorithmAttrSet( algorithm, CCI_CIPHER_IV, NULL );
* 			cciAlgorithmAttrSet( algorithm, CCI_CIPHER_IV_LENGTH, 8 );
* 
*			// 
* 			// --- Set processing handler...
* 			//
* 			cciAlgorithmHandlerSet( algorithm, CCI_HANDLER_SESSION, 
*                                                       mpc190_handler_des );
* 
*			// 
* 			// --- Add CCIAlgorithm object to CCIProvider object.
* 			//
* 			cciProviderAlgorithmSet( Mpc190Provider, algorithm );
* 		}
* 	}
* 
*	return( Mpc190Provider );
* }
* \ce
* SEE ALSO: cciProviderAttrGet()
*
*/
#define cciProviderAttrSet(provider,attribute,value)		(ccip_pvr_set_param(provider, attribute, (cci_g)value))

/*******************************************************************************
*
* cciProviderAttrGet - Get parameter value (macro).
*
* Get value for parameter set in CCIProvider object. 
*
* SYNOPSIS:
* \cs
* void cciProviderAttrGet
*	(
*	CCIProvider provider,   // CCIProvider object
*	CCI_PROVIDER_PARAM attributeId, // Identifier of parameter
*	void *value		// Parameter value to be set
*	)
* \ce
*
* Parameters:
* \is
* \i <CCIProvider provider> 
* CCIProvider object.
*
* \i <CCI_PROVIDER_PARAM attributeId> 
* Identifier of parameter.
*
* \i <'type-specific' *value> 
* Address of variable that is associated with parameter id.
*
* \ie
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* EXAMPLE:
* \cs
* //
* // --- Show how to extract attribute information from CCIProvider object.
* //
* cci_st some_function( CCIContext ctx )
* {
* 	CCIProvider		upkiExportCCIProvider( void );
* 	CCIProvider		upkiProvider = ( CCIProvider ) NULL;
* 	cci_st			cciStatus;
* 	BOOL			useSoftwareDefault;
* 
*	upkiProvider = upkiExportCCIProvider();
* 
*	//
* 	// --- Check is provider uses default algorithms.
* 	//
* 	cciStatus = cciProviderAttrGet( upkiProvider, 
*                       CCI_PVR_SOFTWARE_DEFAULT, &useSoftwareDefault );
* 	if ( useSoftwareDefault == TRUE )
* 		printf("Provider is using default software algorithms\n");
* }
* \ce
* SEE ALSO: cciProviderAttrSet()
*
*/
#define cciProviderAttrGet(provider,attribute,value)		(ccip_pvr_get_param(provider, attribute, (cci_g *)value))


/*******************************************************************************
*
* cciAlgorithmAttrSet - Set algorithm attribute (macro).
*
* Set algorithm-specific attribute. Setting of attributes in a CCIAlgorithm
* object define what attributes will be needed by the CCIContext object for 
* that algorithm. In other words, attributes defined in the CCIAlgorithm object
* in the Provider Export Module determine what attributes will need to be set 
* by the application for that particular crypto request.Values passed in will 
* be inherited by the CCIContext object when it's created.
*
* Algorithm identifier values are determined by the CCI_CLASS used in 
* cciAlgorithmCreate().
*
*	CCI_CLASS_CIPHER Attributes
*	--------------------------------
*	CCI_CIPHER_OPERATION -- Direction of operation (CCI_ENCRYPT or CCI_DECRYPT).
*	CCI_CIPHER_KEY -- Key used en/decryption operation.
*	CCI_CIPHER_KEY_LENGTH -- Length of en/decryption key.
*	CCI_CIPHER_MODE	-- Cipher mode (CCI_MODE_ECB, CCI_MODE_CBC, etc...)
*	CCI_CIPHER_IV -- Initialization Vector (IV)
*	CCI_CIPHER_IV_LENGTH -- IV Length.
*	CCI_CIPHER_BLOCK_SIZE -- Cipher Block Size.
*
*
*	CCI_CLASS_HASH  Attributes
*	--------------------------------
*	CCI_HASH_DIGEST	-- Buffer where message digest will be placed.
*	CCI_HASH_DIGEST_LENGTH --- Length of allocated buffer. Value passed by reference.
*
*
*	CCI_CLASS_HMAC  Attributes
*	--------------------------------
*	CCI_HMAC_KEY -- MAC Key. 
*	CCI_HMAC_KEY_LENGTH -- Length of MAC key.
*	CCI_HMAC_TRUNCATE -- Truncate output digest to this length.
*	CCI_HMAC_DIGEST -- Buffer where message digest will be placed.
*	CCI_HMAC_DIGEST_LENGTH -- Length of allocated buffer. Value passed by reference.
*	CCI_HMAC_BUFFER_PADDED -- Boolean value indicating if input buffer is padded or not.
*
*
*	CCI_CLASS_RNG  Attributes
*	--------------------------------
*	CCI_RNG_BUFFER -- Buffer where random bytes will be placed.
*	CCI_RNG_NUM_BYTES --- Number of random bytes to add to buffer.
*
*
* SYNOPSIS:
* \cs
* cci_st cciProviderAttrSet
*	(
*	CCIAlgorithm algorithm,   // CCIAlgorithm object
*	CCI_ATTRIBUTE_ID attributeId, // Identifier of attribute
*	cci_t value		// Attribute value to be set
*	)
* \ce
*
* Parameters:
* \is
* \i <CCIAlgorithm algorithm> 
* CCIAlgorithm object.
*
* \i <CCI_ATTRIBUTE_ID attributeId> 
* Identifier of attribute.
*
* \i <'type-specific' value> 
* variable that is associated with attribute id.
*
* \ie
* RETURNS: CCI_SUCCESS, if successful, Otherwise, CCI_FAILURE. 
*
* ERRNO: N/A
*
* EXAMPLE:
* \cs
* // 
* // --- This code shows how an CCIAlgorithm object is created and is added 
* //     to a CCIProvider object.
* //
* CCIProvider Mpc190ExportCCIProvider( void )
* {
* 	CCIProvider		Mpc190Provider = ( CCIProvider ) NULL;
* 	CCIAlgorithm	algorithm;
* 
*	// 
* 	// --- Create CCIAlgorithm object, set required attributes and 
*   //     default values, set processing handler and register with 
*   //     CCIProvider object.
* 	//
* 	cciStatus = cciAlgorithmCreate( &algorithm, CCI_CLASS_CIPHER, 
*                                                       CCI_CIPHER_DES );
* 	if( CCISUCCESS( cciStatus ) )
* 	{
* 		// 
* 		// --- Set required attributes and default values.
* 		//
* 		cciAlgorithmAttrSet( algorithm, CCI_ASYNC_SUPPORT, TRUE );
* 		cciAlgorithmAttrSet( algorithm, CCI_CIPHER_OPERATION, NULL );
* 		cciAlgorithmAttrSet( algorithm, CCI_CIPHER_MODE, CCI_MODE_CBC );
* 		cciAlgorithmAttrSet( algorithm, CCI_CIPHER_KEY, NULL );
* 		cciAlgorithmAttrSet( algorithm, CCI_CIPHER_KEY_LENGTH, 8 );
* 		cciAlgorithmAttrSet( algorithm, CCI_CIPHER_IV, NULL );
* 		cciAlgorithmAttrSet( algorithm, CCI_CIPHER_IV_LENGTH, 8 );
* 
*		// 
* 		// --- Set processing handler...
* 		//
* 		cciAlgorithmHandlerSet( algorithm, CCI_HANDLER_SESSION, 
*                                                   mpc190_handler_des );
* 
*		// 
* 		// --- Add algorithm object to provider object.
* 		//
* 		cciProviderAlgorithmSet( Mpc190Provider, algorithm );
* 	}
* }
* \ce
* SEE ALSO: cciAlgorithmCreate(), cciAlgorithmHandlerSet(), cciProviderAlgorithmSet() 
*
*/
#define cciAlgorithmAttrSet( algorithmObject, attribute, value)	(ccip_algorithm_set_attr( algorithmObject, attribute, (void *)value ))


/*******************************************************************************
*
* cciCtxIsAsyncReq - Check processing mode.
*
* Use this function in a CPI session handler to check if a CCIContext object 
* is being submitted asynchronously or synchronously. Asynchronous requests 
* do not block the calling application while synchronous requests do. If the 
* device does not support non-blocking (asynchronous) operations this function 
* need not be used --- in which case, all requests should be treated as 
* blocking (synchronous) operations.
*
* SYNOPSIS:
* \cs
* BOOL cciCtxIsAsyncReq
*	(
*	CCIContext cciContext   // CCIContext object
*	)
* \ce
*
* Parameters:
* \is
* \i <CCIContext cciContext> 
* CCIContext object that was created with cciCtxCreate().
*
* \ie
*
* RETURNS: TRUE, if request is non-blocking, Otherwise, FALSE if request 
* is blocking.
*
* ERRNO: N/A
*
* EXAMPLE:
*
* \cs
* //
* // --- DES and 3DES session handler
* //
* cci_st myDesSessionHandler( CCIContext ctx )
* {
* 	DeviceStatus devStatus;
* 
*	//
* 	// --- Extract attribute value data here...
* 	//
* 	cciCtxAttrGet( ctx, ... );
* 	cciCtxAttrGet( ctx, ... );
* 	cciCtxAttrGet( ctx, ... );
* 
*	//
* 	// --- Submit crypto request to device. Since device accepts asynchronous
* 	//     requests (by use of callback functions), the DeviceCipherOp() call 
* 	//     will NOT block. When processing is complete, the device will execute 
* 	//     the 'myDesCompletionCallback' callback.
* 	//
* 	if ( cciCtxIsAsyncReq( ctx ) )
* 		devStatus = DeviceCipherOp(..., myDesCompletionCallback, ctx );
* 	//
* 	// --- Otherwise, submit synchronous (blocking) request.
* 	//
* 	else
* 		devStatus = DeviceCipherOp(..., NULL, NULL );
* }
* \ce
*
* SEE ALSO: cciCtxAsyncStatus()
*
*/
#define cciCtxIsAsyncReq(ctx)	(CCI_IS_ATTRIBUTE_SET(ctx,CCI_ASYNC_REQUEST) && CCI_IS_ATTRIBUTE_SET(ctx,CCI_ASYNC_SUPPORT))

/*******************************************************************************
*
* cciCtxSessionCtxSet - Set user-defined state information (macro).
*
* Use this function in a CPI session handler to set user-specific state 
* information into a CCIContext object. 
*
* SYNOPSIS:
* \cs
* void  cciCtxSessionCtxSet
*	(
*	CCIContext cciContext,   // CCIContext object
*	void *stateCtx		//  Address of user-defined object that is to be stored in CCIContext object
*	)
* \ce
*
* Parameters:
* \is
* \i <CCIContext cciContext> 
* CCIContext object that was created with cciCtxCreate().
*
* \i <void *stateCtx> 
* Address of user-defined object that is to be stored in CCIContext object.
*
* \ie
*
* RETURNS: N/A
*
* ERRNO: N/A
*
*
* SEE ALSO: cciCtxSessionCtxGet()
*
*/
#define cciCtxSessionCtxSet( ctx, stateCtx ) ( (ctx)->attributes[CCI_SESSION_CTX] = (cci_b *) stateCtx )

/*******************************************************************************
*
* cciCtxSessionCtxGet - Get user-defined state information.
*
* Use this function in a CPI session handler to get user-definedstate 
* information from a CCIContext object. 
*
* SYNOPSIS:
* \cs
* void * cciCtxSessionCtxSet
*	(
*	CCIContext cciContext   // CCIContext object
*	)
* \ce
*
* Parameters:
* \is
* \i <CCIContext cciContext> 
* CCIContext object that was created with cciCtxCreate().
*
* \ie
*
* RETURNS: Pointer to state information object set with 'cciCtxSessionCtxSet'().
*
* ERRNO: N/A
*
*
* SEE ALSO: cciCtxSessionCtxSet()
*
*/
#define cciCtxSessionCtxGet( ctx )	((ctx)->attributes[CCI_SESSION_CTX])



/*******************************************************************************
*
* cciPoolCreate - Create resource pool.
*
* Use this function to create a resource pool of user-defined elements. The 
* resource pool routines 'cciPoolCreate()', 'cciPoolAlloc()', 'cciPoolRelease()' 
* and 'cciPoolDestroy()' are used to pre-allocate a fixed-number of data 
* structures that could be retrieved ad hoc by the application. Since only a 
* fixed number of pool elements are available for use at any given time, the 
* application can be ensured that excessive memory allocations are not done. The 
* memory for each data structure, or pool element, is pre-allocated at 
* initialization time with a call to cciPoolCreatet(). At runtime, rather than 
* having to allocate a new memory-block via a malloc() or calloc() call, the 
* application can just allocate a pool element with the cciPoolAlloc() routine, 
* use it, and then return it to the free pool with a call to cciPoolRelease(). 
* When the resource pool is no longer needed, the application should free it
* with a call to cciPoolDestroy().
* 
* SYNOPSIS:
* \cs
* cci_st  cciPoolCreate
*	(
*	CCIPool *pool,     // Address to an uninstantiated CCIPool object
*	cci_t elementSize, // Size of each element in the pool
*	cci_t poolSize,	   // Number of initial elements in the pool
*	cci_t poolMax      // Maximum number of elements in the pool 
*	)
* \ce
* 
*
* Parameters:
* \is
* \i <CCIPool *pool> 
* Address to an uninstantiated CCIPool object.
*
* \i <cci_t elementSize> 
* The size of each element in the pool.
*
* \i <cci_t poolSize> 
* The number of initial elements in the pool.
*
* \i <cci_t poolMax> 
* The maximum number of elements that the pool can contain. If the number of
* pool elements is increased beyond the initial pool size, the pool will be 
* automatically reduced back to the initial size when the number of free 
* elements equals 'poolMax'.
*
* \ie
*
* RETURNS: CCI_SUCCESS, if successful, Otherwise, CCI_FAILURE. 
*
* ERRNO: N/A
*
*
* SEE ALSO: cciPoolAlloc(), cciPoolRelease(), cciPoolDestroy()
*
*/
#define cciPoolCreate( pool,eSize,size,max)		(cciPoolCreateReg(pool,eSize,size,max,NULL,NULL))


/*******************************************************************************
*
* cciHmacUpdate - Update message digest for HMAC CCIContext.
* 
* Use this function to update the message digest associated with a HMAC 
* CCIContext
*
* SYNOPSIS:
* \cs
* cci_st  cciHmacUpdate
*	(
*	CCIContext cciContext,     // CCIContext object
*	cci_b *input, 	           // Input buffer pointer
*	cci_t inputLength	   // Length of input buffer
*	)
* \ce
*
* Parameters:
* \is
* \i <CCIContext cciContext> 
* The CCIContext object created with 'cciHmacInit()'.
*
* \i <cci_b *input> 
* An address to the message buffer to be used in the update operation.
*
* \i <cci_t inputLength> 
* The length of the message buffer.
*
* \ie
* 
* RETURNS: CCI_SUCCESS, if successful, Otherwise, CCI_FAILURE. 
* 
* ERRNO: N/A
*
* SEE ALSO: cciHmacInit(), cciHmacFinal(), cciHmacBlock()
*
*/
#define cciHmacUpdate( ctx, in, len )			(ccip_ctx_update( ctx, in, len ))


/*******************************************************************************
*
* cciHmacFinal - Extract message digest from CCIContext of type CCI_HMAC.
* 
* Use this function to extract the message digest associated with a HMAC 
* CCIContext
*
* SYNOPSIS:
* \cs
* cci_st  cciHmacFinal
*	(
*	CCIContext cciContext,     // CCIContext object
*	cci_b *digest, 	           // Output buffer pointer
*	cci_t *digestLength	   // Length pointer
*	)
* \ce
*
* Parameters:
* \is
* \i <CCIContext cciContext> 
* The CCIContext object created with 'cciHmacInit()'.
*
* \i <cci_b *digest> 
* The address of a buffer where the digest will be copied to.
*
* \i <cci_t *digestLength> 
* The address of a variable that contains the length of the digest buffer. 
* The variable that this pointer points to will be updated with the  number of 
* bytes that were copied into the digest buffer.
*
* \ie
* 
* RETURNS: CCI_SUCCESS, if successful, Otherwise, CCI_FAILURE. 
* 
* ERRNO: N/A
*
* SEE ALSO: cciHmacInit(), cciHmacUpdate(), cciHmacBlock()
*
*/
#define cciHmacFinal( ctx, dig, digLen )		(ccip_ctx_digest_get( ctx, dig, digLen ))

/*******************************************************************************
*
* cciHashInit - Initialize HASH CCIContext.
* 
* Initialize a HASH crypto context that will be used with 'cciHashUpdate()' and
* 'cciHashFinal()' operations.
*
* SYNOPSIS:
* \cs
* cci_st  cciHashInit
*	(
*	CCIContext *cciContext,            // CCIContext object
*	CCI_PROVIDER_ID providerId,        // Provider to use
*	CCI_ALGORITHM_ID algorithmId	   // Type of CCI_HASH algorithm to use
*	)
* \ce
*
* Parameters:
* \is
* \i <CCIContext *cciContext> 
* The address of an un-initialized CCIContext object.
*
* \i <CCI_PROVIDER_ID providerId> 
* Identifier of the CCI Provider the this object will be created for.
*
* \i <CCI_ALGORITHM_ID algorithmId> 
* The type of CCI_HASH algorithm that will be used.
*
*
* \ie
* 
* RETURNS: CCI_SUCCESS, if successful, Otherwise, CCI_FAILURE. 
* 
* ERRNO: N/A
*
* SEE ALSO: cciHashUpdate(), cciHashFinal(), cciHashBlock()
*
*/
#define cciHashInit( ctx, pvr, algId )			(cciCtxCreate( ctx, pvr, CCI_CLASS_HASH, algId ))


/*******************************************************************************
*
* cciHashUpdate - Update message digest for HASH CCIContext.
* 
* Use this function to update the message digest associated with a HASH 
* CCIContext
*
* SYNOPSIS:
* \cs
* cci_st  cciHashUpdate
*	(
*	CCIContext cciContext,     // CCIContext object
*	cci_b *input, 	           // Input buffer pointer
*	cci_t inputLength	   // Length of input buffer
*	)
* \ce
* Parameters:
* \is
* \i <CCIContext cciContext> 
* The CCIContext object created with 'cciHashInit()'.
*
* \i <cci_b *input> 
* An address to the message buffer to be used in the update operation.
*
* \i <cci_t inputLength> 
* The length of the message buffer.
*
* \ie
* 
* RETURNS: CCI_SUCCESS, if successful, Otherwise, CCI_FAILURE. 
* 
* ERRNO: N/A
*
* SEE ALSO: cciHashInit(), cciHashFinal(), cciHashBlock()
*
*/
#define cciHashUpdate( ctx, in, len )			(ccip_ctx_update( ctx, in, len ))


/*******************************************************************************
*
* cciHashFinal - Extract message digest from CCIContext of type CCI_HASH.
* 
* Use this function to extract the message digest associated with a HASH 
* CCIContext
*
* SYNOPSIS:
* \cs
* cci_st  cciHashFinal
*	(
*	CCIContext cciContext,     // CCIContext object
*	cci_b *digest, 	           // Output buffer pointer
*	cci_t *digestLength	   // Length pointer
*	)
* \ce
* Parameters:
* \is
* \i <CCIContext cciContext> 
* The CCIContext object created with 'cciHashInit()'.
*
* \i <cci_b *digest> 
* The address of a buffer where the digest will be copied to.
*
* \i <cci_t *digestLength> 
* The address of a variable that contains the length of the digest buffer. 
* The variable that this pointer points to will be updated with the  number of 
* bytes that were copied into the digest buffer.
*
* \ie
* 
* RETURNS: CCI_SUCCESS, if successful, Otherwise, CCI_FAILURE. 
* 
* ERRNO: N/A
*
* SEE ALSO: cciHashInit(), cciHashUpdate(), cciHashBlock()
*
*/
#define cciHashFinal( ctx, dig, digLen )		(ccip_ctx_digest_get( ctx, dig, digLen ))


/*
** --- Misc macro routines
*/
#define cciCtxUserObjectSet( ctx, obj )			((ctx)->attributes[CCI_USER_ASSIGNED] = (cci_b *) obj)
#define cciCtxUserObjectGet( ctx )				((void *)(ctx)->attributes[CCI_USER_ASSIGNED])
#define cciCtxPoolCreate( pool, size, max )		( cciPoolCreate( pool, sizeof(CCIContextRec), size, max ))
#define cciCtxPoolDestroy( pool )				( cciPoolDestroy( pool ))

#define CCI_SECURITY_NUM_TAG_DIGITS					20

#define CCI_RSA_BIT_MULTIPLE						16
#define CCI_RSA_MIN_MODULAS_SIZE					46

#define CCI_AES_BLOCKSIZE							16
#define CCI_DES_BLOCKSIZE							8

#define CCI_AESK_IV_LENGTH						8
#define CCI_AES_IV_LENGTH							CCI_AES_BLOCKSIZE		/* AES Init Vector length					*/
#define CCI_DES_IV_LENGTH							CCI_DES_BLOCKSIZE		/* DES Init Vector length					*/
#define CCI_MAX_IV_LENGTH							(__max(CCI_AES_IV_LENGTH, CCI_DES_IV_LENGTH))


#define CCI_AES_XCBC_DIGESTSIZE						16		/* AES-XCBC Digest size						*/
#define CCI_MD2_DIGESTSIZE							16		/* MD2 Digest size							*/
#define CCI_MD2_BLOCKSIZE							16		/* MD2 Block size							*/
#define CCI_MD4_DIGESTSIZE							16		/* MD4 Digest size							*/
#define CCI_MD4_BLOCKSIZE							64		/* MD4 Block size							*/
#define CCI_MD5_DIGESTSIZE							16		/* MD5 Digest size							*/
#define CCI_MD5_BLOCKSIZE							64		/* MD5 Block size							*/
#define CCI_SHA1_DIGESTSIZE							20		/* SHA1 Digest size							*/
#define CCI_SHA1_BLOCKSIZE							64		/* SHA1 Block size							*/
#define CCI_SHA256_BLOCKSIZE						64		/* SHA256 Block size						*/
#define CCI_SHA384_BLOCKSIZE						128		/* SHA384 Block size						*/
#define CCI_SHA512_BLOCKSIZE						128		/* SHA512 Block size						*/
#define CCI_SHA256_DIGESTSIZE						32		/* SHA256 Digest size						*/
#define CCI_SHA384_DIGESTSIZE						48		/* SHA384 Digest size						*/
#define CCI_SHA512_DIGESTSIZE						64		/* SHA512 Digest size						*/
#define CCI_RMD160_DIGESTSIZE						20		/* RMD160 Digest size						*/
#define CCI_RMD160_BLOCKSIZE						64		/* RMD160 Block size						*/
#define CCI_RMD128_DIGESTSIZE						16		/* RMD160 Digest size						*/
#define CCI_RMD128_BLOCKSIZE						64		/* RMD160 Block size						*/
#define CCI_HASH_OPENSSL_DIGESTSIZE					36		/* Special OpenSSL MD5+SHA1 Hash size */

/*
** --- Algorithm handler types...
*/
typedef enum _CCI_SESSION_HANDLER
{
	CCI_HANDLER_SESSION_OPEN,								/* Algorithm open session handler				*/
	CCI_HANDLER_SESSION_INIT,								/* Algorithm initialize session handler			*/
	CCI_HANDLER_SESSION_UPDATE,								/* Algorithm update session handler				*/
	CCI_HANDLER_SESSION_FINAL,								/* Algorithm final session handler				*/
	CCI_HANDLER_SESSION,									/* Algorithm process session handler			*/
	CCI_HANDLER_SESSION_CLOSE								/* Algorithm close session handler				*/
} CCI_SESSION_HANDLER;


/*
** --- IPC Communication types
*/
typedef enum _CCI_NOTIFY_TYPE
{
	CCI_NOTIFY_CALLBACK,									/* Notification type: Callback					*/
	CCI_NOTIFY_MSGQ,										/* Notification type: Message queue				*/
	CCI_NOTIFY_SOCKET,										/* Notification type: Socket					*/
	CCI_NOTIFY_NAMEDPIPE,									/* Notification type: Named Pipe				*/
	CCI_NOTIFY_SYNC_SEMAPHORE								/* Notification type: Binary/sync semaphore		*/
} CCI_NOTIFY_TYPE;


/*
** --- CCI resource pool flags
*/
typedef enum _CCI_POOL_FLAG
{
	CCI_P_NOWAIT = FALSE,									/* Don't wait for resource						*/
	CCI_P_WAIT = TRUE										/* Wait for next available resource				*/
} CCI_POOL_FLAG;



/*
**  --- Priority levels
*/
typedef enum _CCI_PRIORITY_LEVEL
{
	CCI_PRIORITY_1,											/* Highest priority								*/
	CCI_PRIORITY_2,											/* 2nd highest priority							*/
	CCI_PRIORITY_3,											/* Medium priority								*/
	CCI_PRIORITY_4,											/* 2nd lowest priority							*/
	CCI_PRIORITY_5,											/* Lowest priority								*/
	CCI_PRIORITY_MAX										/* Max num of priority levels					*/
} CCI_PRIORITY_LEVEL;


/*
** --- Internal attributes
*/
typedef enum	_CCI_INTERNAL_ATTRIBUTES
{
	CCI_CONTEXT_MSGID,										/* Internal attribute: Context msg identifier.	*/
	CCI_NOTIFICATION_TYPE,									/* Internal attribute: IPC mechanism is use		*/
	CCI_NOTIFICATION_IPC,									/* Internal attribute: IPC Object to be used	*/
	CCI_NOTIFICATION_ARG,									/* Internal attribute: Optional IPC arguments	*/
	CCI_REQUEST_STATUS,										/* Internal attribute: Request status			*/
	CCI_USER_ASSIGNED,										/* Internal attribute: User specified attribute	*/
	CCI_HASH_BLOCK,											/* Internal attribute: HASH Single Block		*/	
	CCI_ASYNC_SUPPORT,										/* Internal attribute: Module has async support	*/	
	CCI_ASYNC_REQUEST,										/* Internal attribute: This is an async request	*/	
	CCI_INPUT_BUFFER,										/* Internal attribute: Input buffer address		*/	
	CCI_INPUT_LENGTH,										/* Internal attribute: Input buffer length		*/	
	CCI_OUTPUT_BUFFER,										/* Internal attribute: Output buffer address	*/	
	CCI_OUTPUT_LENGTH,										/* Internal attribute: Output buffer length		*/	
	CCI_SESSION_CTX,										/* Internal attribute: Session context object	*/	
	CCI_PRIORITY,											/* Internal attribute: Request priority			*/	

#ifdef CCI_SHAREDMEM_REGION									/* Intenral attributes for 6.x and above		*/	
	CCI_VIRTUAL_BASE_ADDR,									/* Internal attribute: Share region virt addr	*/	
	CCI_PHYSICAL_BASE_ADDR,									/* Internal attribute: Share region phys addr	*/	
	CCI_SHARE_PROCESS_PID,									/* Internal attribute: RTP process ID			*/
	CCI_SHARE_RTP_REGION_ID,								/* Internal attribute: RTP region identifier	*/
	CCI_SHARE_RTP_REGION_SIZE,								/* Internal attribute: RTP region size			*/
	CCI_SHARE_INTEGER_SUBTYPE,								/* Internal attribute: RTP integer subtype		*/
	CCI_SHARE_INTEGER_PARAMS,								/* Internal attribute: RTP integer parameters	*/
	CCI_SHARE_PROVIDER_ID,									/* Internal attribute: KERNEL mapping of APP	*/
	CCI_CONV_VIRTUAL_ADDRESS,								/* Convert virtual addr to physical addr		*/
	CCI_SHARE_SYNC_SEM_ID,									/* Synchronization semaphore					*/
	CCI_UNUSED_ATTRIBUTE_001,								/* Internal attribute: future attr place holder */
	CCI_UNUSED_ATTRIBUTE_002,								/* Internal attribute: future attr place holder */
	CCI_UNUSED_ATTRIBUTE_003,								/* Internal attribute: future attr place holder */
	CCI_UNUSED_ATTRIBUTE_004,								/* Internal attribute: future attr place holder */
	CCI_UNUSED_ATTRIBUTE_005,								/* Internal attribute: future attr place holder */
	CCI_UNUSED_ATTRIBUTE_006,								/* Internal attribute: future attr place holder */
#endif
	CCI_ATTRIBUTE_BASE										/* Starting attribute for base classes			*/
} CCI_INTERNAL_ATTRIBUTES;


#define CCI_PUBLIC_KEY_BASE			500						/* Base value for PKI private/public keys		*/
#define CCI_PUBLIC_KEY_COMP_BASE	600						/* Base value for PKI components				*/

typedef enum	_CCI_PUBLIC_KEY_TYPE
{
	CCI_RSA_PUBLIC_KEY			= CCI_PUBLIC_KEY_BASE,		/* RSA Public key							*/
	CCI_RSA_PRIVATE_KEY,									/* RSA Private key							*/
	CCI_DSA_PUBLIC_KEY,										/* DSA Public key							*/
	CCI_DSA_PRIVATE_KEY,									/* DSA Private key							*/
	CCI_DH_PUBLIC_KEY,										/* DH Public key							*/
	CCI_DH_PRIVATE_KEY										/* DH Private key							*/
} CCI_PUBLIC_KEY_TYPE;

typedef enum	_CCI_PUBLIC_KEY_COMPONENT
{
	CCI_RSA_MODULAS				= CCI_PUBLIC_KEY_COMP_BASE, /* RSA n = (P)*(Q)							*/
	CCI_RSA_PUBLIC_EXPONENT,								/* RSA e = d(exp-1)mod((P-1)(Q-1))			*/
	CCI_RSA_PRIVATE_EXPONENT,								/* RSA d = e(exp-1)mod((P-1)(Q-1))			*/
	CCI_RSA_PRIME_FACTOR_P,									/* RSA P Prime factor						*/
	CCI_RSA_PRIME_FACTOR_Q,									/* RSA Q Prime factor						*/
	CCI_RSA_EXPONENT_dP,									/* RSA dP = (d)mod(P-1)						*/
	CCI_RSA_EXPONENT_dQ,									/* RSA dQ = (d)mod(Q-1)						*/
	CCI_RSA_CRT_PINV,										/* RSA PINV = P(exp-1)mod(Q)				*/
	CCI_RSA_CRT_QINV,										/* RSA QINV = Q(exp-1)mod(P)				*/
	CCI_RSA_PHI,											/* RSA PHI									*/
	CCI_RSA_CQ,												/* RSA CRT - Component CQ					*/
	CCI_RSA_CP,												/* RSA CRT - Component CP					*/

	CCI_DSA_MODULAS,										/* DSA P Prime factor						*/
	CCI_DSA_SUBPRIME_Q										/* DSA Q Subprime factor					*/
} CCI_PUBLIC_KEY_COMPONENT;

typedef enum	_CCI_PROVIDER_PARAM
{
	CCI_PVR_SOFTWARE_DEFAULT,								/* Use default s/w algorithms				*/
	CCI_PVR_SECURITY_PUBLIC_KEY,							/* Provider verification private key		*/
	CCI_PVR_SECURITY_SIGNATURE,								/* Provider signature used for verification	*/
	CCI_PVR_SECURITY_SIGNATURE_LEN,							/* Provider signature length				*/
	CCI_PVR_SECURITY_TAG_TABLE,								/* Security tag table						*/
	CCI_PVR_MAX
} CCI_PROVIDER_PARAM;


/*
**  --- CCI STATUS definitions. 
*/
typedef enum	_cci_st
{
	S_cciLib_SUCCESS						= 0,		/* Successful completion						*/
	S_cciLib_FAILURE						= 1,		/* Failure completion							*/
	S_cciLib_LIB_ALREADY_INITIALIZED		= 2,		/* CCI library is already been initialized		*/
	S_cciLib_LIB_NOT_INITIALIZED			= 3,		/* CCI library is NOT initialized				*/
	S_cciLib_CONTEXT_IS_INVALID				= 4,		/* Request context is NULL						*/
	S_cciLib_UNKNOWN_ATTRIBUTE				= 5,		/* Unknown attribute 							*/
	S_cciLib_UNEXPECTED_ATTRIBUTE			= 6,		/* Unexpected attribute received				*/
	S_cciLib_UNEXPECTED_ATTRIBUTE_TYPE		= 7,		/* Unexpected attribute type 					*/
	S_cciLib_UNKNOWN_ATTRIBUTE_DATATYPE		= 8,		/* Unknown attribute data type					*/
	S_cciLib_ATTRIBUTE_NOT_FOUND			= 9,		/* Attribute not found							*/
	S_cciLib_INVALID_ATTRIBUTE_VALUE		= 10,		/* Invalid attribute value						*/
	S_cciLib_MISSING_ATTRIBUTE				= 11,		/* Missing attribute from context				*/
	S_cciLib_MEMALLOC_FAILED				= 12,		/* Memory allocation failed						*/
	S_cciLib_MORE_DATA						= 13,		/* More data is available						*/
	S_cciLib_NO_MORE_DATA					= 14,		/* No more data is available					*/
	S_cciLib_ALGORITHM_NOT_SUPPORTED		= 15,		/* Provider does not support algorithm			*/
	S_cciLib_PROVIDER_NOT_REGISTED			= 16,		/* Provider is not registed						*/
	S_cciLib_INVALID_ALGORITHM_ID			= 17,		/* Invalid algorithm identifier					*/
	S_cciLib_INVALID_CRYPTO_CLASS_ID		= 18,		/* Invalid crypto class identifier				*/
	S_cciLib_INVALID_PROVIDER_ID			= 19,		/* Invalid provider identifier					*/
	S_cciLib_PROVIDER_NULL_FROM_LOADPROC	= 20,		/* Provider returned from procedure was NULL	*/
	S_cciLib_PROVIDER_INIT_FAILED			= 21,		/* Provider initialization failed				*/
	S_cciLib_PROVIDER_DEFINED_STATUS		= 22,		/* Provider defined status code					*/
	S_cciLib_INVALID_IDENTIFIER				= 23,		/* Identifier value is invalid					*/
	S_cciLib_PROVIDER_UNKNOWN_STATUS		= 24,		/* Provider returned an unknown status code		*/
	S_cciLib_INPUT_BUFFER_NOT_BYTE_ALIGNED	= 25,		/* Input buffer is not byte aligned				*/
	S_cciLib_CIPHER_MODE_NOT_SUPPORTED		= 26,		/* Cipher mode is not supported	by provider		*/
	S_cciLib_INVALID_KEY_LENGTH				= 27,		/* Invalid key length							*/
	S_cciLib_PROVIDER_ALREADY_LOADED		= 28,		/* Provider is already loaded					*/
	S_cciLib_OUTPUT_BUFFER_TOO_SMALL		= 29,		/* Output buffer is to small processing			*/
	S_cciLib_INVALID_IV_LENGTH				= 30,		/* Invalid initialization vector size			*/
	S_cciLib_PARAMETER_STRING_TOO_LONG		= 31,		/* Parameter string too long					*/
	S_cciLib_DECODING_ERROR					= 32,		/* Decoding error								*/
	S_cciLib_MESSAGE_TOO_LONG				= 33,		/* Message too long								*/
	S_cciLib_INTENDED_MESSAGE_TOO_SHORT		= 34,		/* Intended message is too short				*/
	S_cciLib_INVALID_HASH_FUNCTION			= 35,		/* Invalid Hash function						*/
	S_cciLib_INVALID_ENCODING_FORMAT		= 36,		/* Invalid Encoding format						*/
	S_cciLib_MODULAS_TOO_SMALL				= 37,		/* Modulas is too small							*/
	S_cciLib_INVALID_SIGNATURE				= 38,		/* Invalid signature							*/
	S_cciLib_SESSION_OPEN_ERROR				= 39,		/* Session open exception						*/
	S_cciLib_VALIDATION_TEST_NOT_AVAIL		= 40,		/* Validation test not available				*/
	S_cciLib_MAX_MTU_LENGTH_EXCEEDED		= 41,		/* Max MTU length exceeded						*/
	S_cciLib_DIGEST_BUFFER_TOO_SMALL		= 42,		/* Digest buffer is too small					*/
	S_cciLib_PROVIDER_SIGNATURE_INVALID		= 43,		/* Invalid provider signature					*/
	S_cciLib_CONTEXT_POOL_EMPTY				= 44,		/* Context Pool is empty						*/
	S_cciLib_CHANNEL_NOT_AVAILABLE			= 45,		/* Channel not available message from provider	*/
	S_cciLib_INVALID_PADDING_ALG			= 46,		/* Invalid padding type							*/
	S_cciLib_MESSAGE_TOO_SHORT				= 47,		/* Message too short							*/
	S_cciLib_INVALID_MESSAGE_LENGTH			= 48,		/* Message message length not valid				*/
	S_cciLib_INVALID_BUFFER_ALIGNMENT		= 49,		/* Input buffer not properly aligned			*/
	S_cciLib_AUTHENTICATION_ERROR			= 50,		/* Authentication error							*/
	S_cciLib_ST_MAX										/* Max value									*/
} cci_st;



/*
**
**  --- CCI CIPHER definitions
**
**  All macro definitions with this enumeration must be 
**  SEQUENTIAL and the first enum must begin with ZERO.
**
*/
typedef enum	_CCI_CIPHER
{
	CCI_CIPHER_DES,											/* 56bit key DES block cipher				*/
	CCI_CIPHER_2DES,										/* 112bit key DES block cipher              */
	CCI_CIPHER_3DES,										/* 168bit key TRIPLE-DES block cipher       */
	CCI_CIPHER_IDEA,										/* IDEA block cipher						*/
	CCI_CIPHER_DESX,										/* 56bit key DESX block cipher				*/
	CCI_CIPHER_RC2,											/* RC2 block cipher                         */
	CCI_CIPHER_RC5,											/* RC5 block cipher                         */
	CCI_CIPHER_BLOWFISH,									/* BLOWFISH block cipher                    */
	CCI_CIPHER_SKIPJACK,									/* SKIPJACK block cipher                    */
	CCI_CIPHER_CAST128,										/* CAST-128 block cipher                    */
	CCI_CIPHER_AES,											/* AES block cipher                         */
	CCI_CIPHER_RC4,											/* RC4 stream cipher                        */
	CCI_CIPHER_A5,											/* A5 stream cipher							*/
	CCI_CIPHER_LEVIATHON,									/* LEVIATHON stream cipher					*/
	CCI_CIPHER_SOBER,										/* SOBER stream cipher                      */
	CCI_CIPHER_SEAL,										/* SEAL stream cipher						*/
	CCI_CIPHER_FEAL,										/* FEAL block cipher						*/
	CCI_CIPHER_ICE,											/* ICE 64-bit block cipher					*/
	CCI_CIPHER_KASUMI,										/* KASUMI 64-bit block cipher				*/
	CCI_CIPHER_MISTY,										/* MISTY 64-bit block cipher				*/
	CCI_CIPHER_SAFER,										/* SAFER 64-bit block cipher				*/
	CCI_CIPHER_SHARK,										/* SHARK 64-bit block cipher				*/
	CCI_CIPHER_CAMELLIA,									/* CAMELLIA 128-bit block cipher			*/
	CCI_CIPHER_SEED,										/* SEED 128-bit block cipher				*/
	CCI_CIPHER_SQUARE,										/* SQUARE 128-bit block cipher				*/
	CCI_CIPHER_UNICORN,										/* UNICORN 128-bit block cipher				*/
	CCI_CIPHER_MERCY,										/* MERCY Disk Sector block cipher			*/
	CCI_CIPHER_PANAMA,										/* PANAMA cipher							*/
	CCI_CIPHER_NULL,										/* NULL encryption cipher					*/
	CCI_CIPHER_AESKW,										/* AES Key Wrap Algorithm					*/
	CCI_CIPHER_RC4TKIP,										/* TKIP using RC4 algorithm					*/
	CCI_CIPHER_MAX											/* Max value								*/
} CCI_CIPHER;

/*
**  --- CIPHER attribute definitions
**
*/
typedef enum _CCI_CIPHER_ATTRIBUTE
{
	CCI_CIPHER_OPERATION	= CCI_ATTRIBUTE_BASE,			/* Cipher operation: encrypt/decrypt		*/
	CCI_CIPHER_KEY,											/* Symmetric cipher key						*/
	CCI_CIPHER_KEY_LENGTH,									/* Symmetric cipher key length   			*/
	CCI_CIPHER_MODE,										/* Symmetric cipher mode   					*/
	CCI_CIPHER_IV,											/* Cipher initialization vector				*/
	CCI_CIPHER_IV_LENGTH,									/* Cipher initialization vector	length		*/
	CCI_CIPHER_BLOCK_SIZE,									/* Cipher block size						*/
	CCI_CIPHER_KEY_OFFSET,									/* Cipher KEY offset						*/
	CCI_CIPHER_ATTRIBUTE_MAX								/* Max value								*/
}															
CCI_CIPHER_ATTRIBUTE;										
															
typedef enum _CCI_CIPHER_CONSTANTS							
{															
	CCI_ENCRYPT,											/* Encryption flag							*/
	CCI_DECRYPT												/* Decryption flag							*/
} CCI_CIPHER_CONSTANTS;										
															
typedef enum _CCI_MODE_CONSTANTS							
{															
	CCI_MODE_NONE,											/* No CIPHER mode needed					*/
	CCI_MODE_BC,											/* Block Chaining Mode						*/
	CCI_MODE_PCBC,											/* Propagating Cipher Block Chaining Mode	*/
	CCI_MODE_CBCC,											/* Cipher Block Chaining Checksum Mode		*/
	CCI_MODE_OFBNLF,										/* Output-Feedback Nonlinear Fuction Mode	*/
	CCI_MODE_PBC,											/* Plainttext Block Chaining Mode			*/
	CCI_MODE_CBC,											/* Cipher Block Chaining Mode				*/
	CCI_MODE_ECB,											/* Electronic Codebook Mode					*/
	CCI_MODE_PFB,											/* Plaintext-Feedback Mode					*/
	CCI_MODE_CFB,											/* Cipher-Feedback Mode						*/
	CCI_MODE_CFB128,										/* Cipher-Feedback 128-bit mode				*/
	CCI_MODE_CFB8,											/* Cipher-Feedback 8-bit mode				*/
	CCI_MODE_CFB1,											/* Cipher-Feedback 1-bit mode				*/
	CCI_MODE_CTR,											/* AES counter Mode							*/
	CCI_MODE_OFB,											/* Output-Feedback  Mode					*/
	CCI_MODE_MAX											/* Maximum cipher modes						*/
} CCI_MODE_CONSTANTS;





/*
**
**  --- CCI HASH definitions
**
**  All macro definitions with this enumeration must be 
**  SEQUENTIAL and the first enum must begin with ZERO.
**
*/
typedef enum	_CCI_HASH
{
	CCI_HASH_SHA1,											/* SHAI hash algorithm						*/
	CCI_HASH_SHA256,										/* SHA 256 hash algorithm					*/
	CCI_HASH_SHA384,										/* SHA 384 hash algorithm					*/
	CCI_HASH_SHA512,										/* SHA 512 hash algorithm					*/
	CCI_HASH_MD2,											/* MD2 hash algorithm RFC1319				*/
	CCI_HASH_MD4,											/* MD4 hash algorithm						*/
	CCI_HASH_MD5,											/* MD5 hash algorithm RFC1321				*/
	CCI_HASH_RIPEMD160,										/* RIPEMD160 hash algorithm					*/
	CCI_HASH_RIPEMD128,										/* RIPEMD128 hash algorithm					*/
	CCI_HASH_TIGER,											/* TIGER hash algorithm						*/
	CCI_HASH_PANAMA,										/* PANAMA hash algorithm					*/
	CCI_HASH_CRC32,											/* CRC hash algorithm						*/
	CCI_HASH_ADLER32,										/* ADLER32 hash algorithm					*/
	CCI_HASH_MAX											/* Max value								*/
} CCI_HASH;													
															
/*															
**  --- HASH attribute definitions							
**															
*/															
typedef enum _CCI_HASH_ATTRIBUTE							
{															
	CCI_HASH_DIGEST	= CCI_ATTRIBUTE_BASE,					/* Digest to authenticate against			*/
	CCI_HASH_DIGEST_LENGTH,									/* Digest length							*/
	CCI_HASH_ATTRIBUTE_MAX									/* Max value								*/
}
_CCI_HASH_ATTRIBUTE;


/*
**
**  --- CCI MAC definitions
**
**  All macro definitions with this enumeration must be 
**  SEQUENTIAL and the first enum must begin with ZERO.
**
*/
typedef enum	_CCI_HMAC
{
	CCI_HMAC_SHA1,											/* SHA1 160 MAC algorithm					*/
	CCI_HMAC_SHA256,										/* SHA 256 MAC algorithm					*/
	CCI_HMAC_SHA384,										/* SHA 384 MAC algorithm					*/
	CCI_HMAC_SHA512,										/* SHA 512 MAC algorithm					*/
	CCI_HMAC_MD4,											/* MD5 MAC algorithm						*/
	CCI_HMAC_MD5,											/* MD5 MAC algorithm						*/
	CCI_HMAC_RIPEMD160,										/* RIPEMD160 MAC algorithm					*/
	CCI_HMAC_RIPEMD128,										/* RIPEMD128 MAC algorithm					*/
	CCI_HMAC_AES_XCBC,										/* AES-MAC Cipher Block Chaining Mode		*/
	CCI_HMAC_NONE,											/* NO authentication required				*/
	CCI_HMAC_MAX											/* Max value								*/
} CCI_HMAC;													
															
/*															
**  --- MAC attribute definitions							
**															
*/															
typedef enum _CCI_HMAC_ATTRIBUTE							
{															
	CCI_HMAC_KEY = CCI_ATTRIBUTE_BASE,						/* HMAC secret key							*/
	CCI_HMAC_KEY_LENGTH,									/* HMAC secret key length in bytes			*/
	CCI_HMAC_TRUNCATE,										/* Truncate output digest					*/
	CCI_HMAC_DIGEST,										/* Digest to authenticate against			*/
	CCI_HMAC_DIGEST_LENGTH,									/* Digest length							*/
	CCI_HMAC_BUFFER_PADDED,									/* Input buffer is padded					*/
	CCI_HMAC_ATTRIBUTE_MAX									/* Max value								*/
} _CCI_HMAC_ATTRIBUTE;


/*
**
**  --- CCI PUBLICKEY definitions
**
**  All PUBLICKEYro definitions with this enumeration must be 
**  SEQUENTIAL and the first enum must begin with ZERO.
**
*/
typedef enum	_CCI_PUBLICKEY
{
	CCI_PUBLICKEY_RSA,										/* RSA PUBLICKEY algorithm					*/
	CCI_PUBLICKEY_DSA,										/* DSA PUBLICKEY algorithm					*/
	CCI_PUBLICKEY_ECC,										/* Ellipic Curve algorithm					*/
	CCI_PUBLICKEY_ECDSA,									/* ECDSA PUBLICKEY algorithm				*/
	CCI_PUBLICKEY_DH,										/* DH PUBLICKEY algorithm					*/
	CCI_PUBLICKEY_ECDH,										/* ECDH PUBLICKEY algorithm					*/
	CCI_PUBLICKEY_KEA,										/* KEA PUBLICKEY algorithm (RFC2528)		*/
	CCI_PUBLICKEY_ELGAMAL,									/* ELGMAL PUBLICKEY algorithm				*/
	CCI_PUBLICKEY_RABIN_WILLIAMS,							/* RABIN-WILLIAMS PUBLICKEY algorithm		*/
	CCI_PUBLICKEY_BLUM_GOLDWASSER,							/* BLUM GOLDWASSER algorithm				*/
	CCI_PUBLICKEY_MQV,										/* MQV public key agreement  algorithm		*/
	CCI_PUBLICKEY_MAX										/* Max value								*/
} CCI_PUBLICKEY;

/*
**  --- PUBLICKEY attribute definitions
**
*/
typedef enum _CCI_PUBLICKEY_ATTRIBUTE
{
	CCI_PUBLICKEY_OPERATION			= CCI_ATTRIBUTE_BASE,	/* Public key operation						*/
	CCI_PUBLICKEY_LENGTH_BITS,								/* Public/private key length in bits		*/
	CCI_PUBLICKEY_PRIVATE,									/* Private key								*/
	CCI_PUBLICKEY_PUBLIC,									/* Public key								*/
	CCI_PUBLICKEY_ENCRYPT_FORMAT,							/* Encryption/decryption format				*/
	CCI_PUBLICKEY_SIGN_FORMAT,								/* Signature/verification format			*/
	CCI_PUBLICKEY_HASH_FORMAT,								/* Format of HASH							*/
	CCI_PUBLICKEY_PLAINTEXT,								/* Plainetext data							*/
	CCI_PUBLICKEY_PLAINTEXT_LENGTH,							/* Length of plaintext data					*/
	CCI_PUBLICKEY_SIGNATURE,								/* Signature of signed data					*/
	CCI_PUBLICKEY_SIGNATURE_LENGTH,							/* Signaure length							*/
	CCI_PUBLICKEY_HASH_DIGEST,								/* HASH to sign								*/
	CCI_PUBLICKEY_CIPHER_TEXT,								/* Signature of signed data					*/
	CCI_PUBLICKEY_CIPHER_TEXT_LENGTH,						/* Signaure length							*/
	CCI_PUBLICKEY_KEY_TYPE,									/* PKI Key type								*/
	CCI_PUBLICKEY_KEY,										/* PKI key object							*/
	CCI_PUBLICKEY_KEY_SIZE,									/* PKI key size								*/
	CCI_PUBLICKEY_KEY_COMP_TYPE,							/* PKI Key component type					*/
	CCI_PUBLICKEY_KEY_COMP,									/* PKI key component object					*/
	CCI_PUBLICKEY_KEY_COMP_LENGTH,							/* PKI key component object	length			*/
	CCI_PUBLICKEY_BLINDING,									/* TRUE/FALSE state for Blinding			*/

	CCI_PUBLICKEY_ATTRIBUTE_MAX								/* Max value								*/
}
_CCI_PUBLICKEY_ATTRIBUTE;

typedef enum	_CCI_PUBLICKEY_CONSTANTS
{
	CCI_PUBLICKEY_OP_CREATE_KEY,							/* Generate public key pair					*/
	CCI_PUBLICKEY_OP_DESTROY_KEY,							/* Destroy public key pair					*/
	CCI_PUBLICKEY_OP_SET_KEY_COMP,							/* Set key component						*/
	CCI_PUBLICKEY_OP_GET_KEY_COMP,							/* Get key component						*/
	CCI_PUBLICKEY_OP_GEN_KEYPAIR,							/* Generate public key pair					*/
	CCI_PUBLICKEY_OP_SIGN,									/* Perform public key signing operation		*/
	CCI_PUBLICKEY_OP_VERIFY,								/* Perform public key verify operation		*/
	CCI_PUBLICKEY_OP_ENCRYPT,								/* Perform public key encryption operation	*/
	CCI_PUBLICKEY_OP_DECRYPT,								/* Perform public key decryption operation	*/
	CCI_PUBLICKEY_OP_SET_BLINDING,							/* Set Enable/Disable Blinding operations	*/
	CCI_PUBLICKEY_OP_GET_BLINDING,							/* Get Enable/Disable Blinding operations	*/
	CCI_PUBLICKEY_OP_KEY_SIZE,								/* PKI keysize operations					*/
	CCI_PUBLICKEY_HASH_SHA1,								/* SHA1 HASH digest							*/
	CCI_PUBLICKEY_HASH_MD5,									/* MD5 HASH digest							*/
	CCI_PUBLICKEY_HASH_MD4,									/* MD4 HASH digest							*/
	CCI_PUBLICKEY_HASH_MD2,									/* MD2 HASH digest							*/
	CCI_PUBLICKEY_HASH_RIP160,								/* RIPEMD160 HASH digest					*/
	CCI_PUBLICKEY_OAEP,										/* Optimal asymmetric encryption padding	*/
	CCI_PUBLICKEY_PKCS1_V1_5,								/* PKCS1 V1.5 format						*/
	CCI_PUBLICKEY_NO_PADDING,								/* No padding of messages required			*/
	CCI_PUBLICKEY_SSLV23,									/* PKCS1 V1.5 padding with SSL modification	*/
	CCI_PUBLICKEY_HASH_OPENSSL								/* Special openSSL signing method			*/
} CCI_PUBLICKEY_CONSTANTS;


/*
**
**  --- CCI RANDOM NUMBER definitions
**
**  All RANDOMNUMro definitions with this enumeration must be 
**  SEQUENTIAL and the first enum must begin with ZERO.
**
*/
typedef enum	_CCI_RNG
{
	CCI_RNG_GENERIC,										/* Generic RANDOMNUM algorithm				*/
	CCI_RNG_FIPS186_SHA1,									/* FIPS186_SHA1 RANDOMNUM algorithm			*/
	CCI_RNG_FIPS186_DES,									/* FIPS186_DES RANDOMNUM algorithm			*/
	CCI_RNG_YARROW_DES,										/* YARROW_DES RANDOMNUM algorithm			*/
	CCI_RNG_ISSAC,											/* ISSAC RANDOMNUM algorithm				*/
	CCI_RNG_SEED,											/* ISSAC RANDOMNUM algorithm				*/
	CCI_RNG_MAX												/* Max value								*/
} CCI_RNG;

/*
**  --- RANDOM NUMBER attribute definitions
**
*/
typedef enum _CCI_RNG_ATTRIBUTE
{
	CCI_RNG_NUM_BYTES		= CCI_ATTRIBUTE_BASE,			/* RNG operation 							*/
	CCI_RNG_BUFFER,											/* List of random numbers generated			*/
	CCI_RNG_ATTRIBUTE_MAX									/* Max value								*/
} CCI_RNG_ATTRIBUTE;										
															
typedef enum	_CCI_RNG_CONSTANTS							
{															
	CCI_SECURE_NUMBERS,										/* Generate crypto secure random numbers	*/
	CCI_RAW_NUMBERS											/* Generate un-processed random numbers		*/
} CCI_RNG_CONSTANTS;


/*
**  --- CCI compression algorithms
**
**  All COMPRESSIONro definitions with this enumeration must be 
**  SEQUENTIAL and the first enum must begin with ZERO.
**
*/
typedef enum	CCI_COMPRESSION
{
	CCI_COMPRESSION_LZS,									/* STAC Lempel-Ziv standard compression			*/
	CCI_COMPRESSION_LZS_STRIP0,								/* STAC Lempel-Ziv STRIP0  standard compression	*/
	CCI_COMPRESSION_MPPC,									/* Microsoft Point-to-Point Compression			*/
	CCI_COMPRESSION_MAX										/* Max value									*/
} CCI_COMPRESSION;

/*
**
**  --- COMPRESSION attribute definitions
**
*/
typedef enum _CCI_COMPRESSION_ATTRIBUTE
{
	CCI_COMPRESSION_OPERATION		= CCI_ATTRIBUTE_BASE,	/* Compression/Decompression operation			*/
	CCI_COMPRESSION_ATTRIBUTE_MAX							/* Max value									*/
}
_CCI_COMPRESSION_ATTRIBUTE;

typedef enum	_CCI_COMPRESSION_CONSTANTS
{
	CCI_COMPRESS,											/* Perform data compression operation			*/
	CCI_DECOMPRESS											/* Perform data decompression operation			*/
} CCI_COMPRESSION_CONSTANTS;

/* 
** 
**  --- CCI IPSEC definitions 
** 
**  All macro definitions with this enumeration must be 
**  SEQUENTIAL and the first enum must begin with ZERO. 
** 
*/ 
typedef enum _CCI_IPSEC 
{ 
	CCI_IPSEC_SINGLEPASS,									/* Single-pass operation						*/ 
	CCI_IPSEC_MAX											/* Max value									*/ 
} CCI_IPSEC; 

/* 
**  --- IPSEC attribute definitions 
** 
*/ 
typedef enum _CCI_IPSEC_ATTRIBUTE 
{ 
	CCI_IPSEC_OPERATION = CCI_ATTRIBUTE_BASE,				/* Cipher operation: encrypt/decrypt			*/
	CCI_IPSEC_MODE,											/* Mode: TUNNEL or TRANSPORT					*/ 
	CCI_IPSEC_PROTOCOL,										/* Mode: AH or ESP Protocol						*/ 
	CCI_IPSEC_CIPHER,										/* Cipher algorithm used						*/
	CCI_IPSEC_CIPHER_MODE,									/* Cipher cipher mode							*/ 
	CCI_IPSEC_CIPHER_KEY,									/* Cipher cipher key							*/ 
	CCI_IPSEC_CIPHER_KEY_LENGTH,							/* Cipher cipher key length						*/ 
	CCI_IPSEC_CIPHER_IV,									/* Cipher initialization vector					*/ 
	CCI_IPSEC_CIPHER_IV_LENGTH,								/* Cipher initialization vector length			*/ 
	CCI_IPSEC_CIPHER_INPUT_BUFFER,							/* Cipher input buffer							*/ 
	CCI_IPSEC_CIPHER_INPUT_LENGTH,							/* Cipher input buffer  length					*/ 
	CCI_IPSEC_CIPHER_OUTPUT_BUFFER,							/* Cipher output buffer							*/ 
	CCI_IPSEC_CIPHER_OUTPUT_LENGTH,							/* Cipher output buffer length					*/ 
	CCI_IPSEC_CIPHER_BLOCK_SIZE,							/* Cipher Block Size							*/ 
	CCI_IPSEC_HMAC,											/* Hmac algorithm used							*/ 
	CCI_IPSEC_HMAC_KEY,										/* Hmac key										*/
	CCI_IPSEC_HMAC_KEY_LENGTH,								/* Hmac key length								*/ 
	CCI_IPSEC_HMAC_INPUT_BUFFER,							/* Hmac input buffer							*/ 
	CCI_IPSEC_HMAC_BUFFER_LENGTH,							/* Hmac input buffer    length					*/ 
	CCI_IPSEC_HMAC_TRUNCATE_LENGTH,							/* Truncate output digest size					*/
	CCI_IPSEC_HMAC_DIGEST,									/* Digest to authenticate against				*/ 
	CCI_IPSEC_HMAC_DIGEST_LENGTH,							/* Digest length								*/
	CCI_IPSEC_ATTRIBUTE_MAX									/* Max value									*/
} CCI_IPSEC_ATTRIBUTE;                                   


/*
**
**  --- Library algorithm classes
**
**  All macro definitions with this enumeration must be  SEQUENTIAL and 
**  the first enum must begin with ZERO. The order of the class entries in 
**  the cryptoClassTable table MUST match exactly the order of the CCI_CLASS 
**  enumeration.
**
*/
typedef enum CCI_CLASS
{
	_CLASS( CIPHER ),										/* Base value							*/
	_CLASS( HASH ),											/* Hash/Signature algorithms			*/
	_CLASS( HMAC ),											/* MAC algorithms						*/
	_CLASS( PUBLICKEY ),									/* Public Key Algorithms				*/
	_CLASS( RNG ),											/* Random number generation algorithms	*/
	_CLASS( COMPRESSION ),									/* Data compression algorithms			*/
	_CLASS( IPSEC ),										/* IPSEC packet processing algorithms	*/
	_CLASS( MAX )											/* Max value							*/
} CCI_CLASS;


typedef cci_g	( *mpi_CreateProc ) ();
typedef void	( *mpi_SetWordProc ) ( cci_g op, cci_t initValue );
typedef cci_g	( *mpi_CreateDecProc ) ( cci_z decString );
typedef cci_g	( *mpi_CreateHexProc ) ( cci_z hexString );
typedef cci_g	( *mpi_CreateBinProc ) ( cci_b * bytes, cci_t numbytes );
typedef cci_st	( *mpi_DestroyProc ) ( cci_g op );
typedef cci_t	( *mpi_BinaryProc ) ( cci_g op, cci_b **byteArray );
typedef cci_t	( *mpi_NumBitsProc ) ( cci_g op );
typedef void	( *mpi_CopyProc ) ( cci_g op, cci_g op2 );
typedef cci_z	( *mpi_HexStrProc ) ( cci_g op );
typedef cci_z	( *mpi_DecStrProc ) ( cci_g op );
typedef void	( *mpi_AddProc ) ( cci_g rop, cci_g op1, cci_g op2 );
typedef void	( *mpi_SubtractProc ) ( cci_g rop, cci_g op1, cci_g op2 );
typedef void	( *mpi_MultiplyProc ) ( cci_g rop, cci_g op1, cci_g op2 );
typedef void	( *mpi_DivideProc ) ( cci_g rop, cci_g op1, cci_g op2, cci_g rem );
typedef void	( *mpi_ExponentProc ) ( cci_g rop, cci_g op1, cci_g exponent );
typedef void	( *mpi_ModProc ) ( cci_g rop, cci_g op1, cci_g numberM );
typedef void	( *mpi_ModExpProc ) ( cci_g rop, cci_g op1, cci_g exp, cci_g numberM );
typedef void	( *mpi_ModInverseProc ) ( cci_g rop, cci_g op, cci_g numberM );
typedef void	( *mpi_ShiftRightProc ) ( cci_g rop, cci_g op, cci_t shiftAmount );
typedef void	( *mpi_ShiftLeftProc )  ( cci_g rop, cci_g op, cci_t shiftAmount );
typedef void	( *mpi_GCDProc ) ( cci_g rop, cci_g op, cci_g op2 );
typedef int		( *mpi_CompareProc ) ( cci_g op1, cci_g op2 );
typedef int		( *mpi_CmpWordProc ) ( cci_g op, cci_t value );
typedef cci_bool ( *mpi_IsPosProc ) ( cci_g op );
typedef cci_bool ( *mpi_IsNegProc ) ( cci_g op );
typedef void	( *mpi_PosProc ) ( cci_g op );
typedef void	( *mpi_NegProc ) ( cci_g op );
typedef cci_bool ( *mpi_TstBitProc ) ( cci_g op, cci_t bit );
typedef cci_bool ( *mpi_SetBitProc ) ( cci_g op, cci_t bit );
typedef cci_bool ( *mpi_ClrBitProc ) ( cci_g op, cci_t bit );
typedef cci_bool ( *mpi_MaskBitsProc ) ( cci_g op, cci_t value );
typedef void ( *mpi_RandomProc ) ( cci_g op, cci_t numBits );
typedef cci_bool ( *mpi_IsPrimeProc ) ( cci_g op );
typedef void ( *mpi_PrimeProc ) ( cci_g op, cci_t numBits );


typedef struct	_CCIIntegerTableRec
{
	mpi_SetWordProc				numSetWordProc;
	mpi_CreateProc				numCreateProc;
	mpi_CreateDecProc			numCreateDecProc;
	mpi_CreateHexProc			numCreateHexProc;
	mpi_CreateBinProc			numCreateBinProc;
	mpi_DestroyProc				numDestroyProc;
	mpi_BinaryProc				numBinaryProc;
	mpi_NumBitsProc				numNumBitsProc;
	mpi_CopyProc				numCopyProc;
	mpi_HexStrProc				numHexStrProc;
	mpi_DecStrProc				numDecStrProc;
	mpi_AddProc					numAddProc;
	mpi_SubtractProc			numSubtractProc;
	mpi_MultiplyProc			numMultiplyProc;
	mpi_DivideProc				numDivideProc;
	mpi_ExponentProc			numExponentProc;
	mpi_ModProc					numModProc;
	mpi_ModExpProc				numModExpProc;
	mpi_ModInverseProc			numModInverseProc;
	mpi_ShiftRightProc			numShiftRightProc;
	mpi_ShiftLeftProc			numShiftLeftProc;
	mpi_CompareProc				numCompareProc;
	mpi_IsPosProc				numIsPosProc;
	mpi_IsNegProc				numIsNegProc;
	mpi_PosProc					numPosProc;
	mpi_NegProc					numNegProc;
	mpi_TstBitProc				numTstBitProc;
	mpi_SetBitProc				numSetBitProc;
	mpi_ClrBitProc				numClrBitProc;
	mpi_CmpWordProc				numCmpWordProc;
	mpi_MaskBitsProc			numMaskBitsProc;
	mpi_GCDProc					numGCDProc;
	mpi_RandomProc				numRandomProc;
	mpi_IsPrimeProc				numIsPrimeProc;
	mpi_PrimeProc				numPrimeProc;
} CCIIntegerTableRec, CCIIntegerTable;



/* make sure alloc is defined */
#ifdef __GNUC__
#ifndef alloca
#define alloca __builtin_alloca
#endif
#endif

#ifndef VXWORKS
#ifndef alloca
#define alloca _alloca
#endif
#endif


/*
** --- Structure definitions
*/
typedef struct _CCIContextRec		*CCIContext;			/* Request context object				*/
typedef struct _CCIProviderRec		*CCIProvider;			/* Provider object						*/
typedef struct _CCIPoolRec			*CCIPool;				/* Request pool object					*/
typedef struct _CCIAlgorithmRec		*CCIAlgorithm;			/* CPI algorithm object					*/
typedef struct _CCIPublicKeyRec		*CCIPublicKey;			/* PKI public/private key object		*/
typedef struct _CCIIntegerRec		*cci_m;					/* Large integer object					*/

typedef void * ( *CCI_REALLOC_FUNCPTR ) (void *memblock, size_t size);
typedef void * ( *CCI_MALLOC_FUNCPTR ) ( size_t size );
typedef void ( *CCI_FREE_FUNCPTR ) (void *memblock);
typedef void ( *FUNCTION_PTR )();


/*
**  --- Request context object definition
*/
#define CCI_NATIVE_ALGORITHM( alg )	{cci_st alg##_native( CCIProvider provider );cciSetNativeAlgorithm( alg##_native );}
#define CCI_CLASS_ATTRIBUTE_MAX	__max((int) CCI_IPSEC_ATTRIBUTE_MAX,__max((int) CCI_COMPRESSION_ATTRIBUTE_MAX,__max((int)CCI_RNG_ATTRIBUTE_MAX,__max((int)CCI_PUBLICKEY_ATTRIBUTE_MAX,__max((int)CCI_HMAC_ATTRIBUTE_MAX,__max((int)CCI_CIPHER_ATTRIBUTE_MAX,(int)CCI_HASH_ATTRIBUTE_MAX))))))
typedef void ( *CCI_CALLBACK_FUNCPTR ) (const CCIContext cciContext, const cci_g callbackParams, const cci_st status);
typedef struct	_CCIContextRec
{
	cci_t					control;									/* Context control register		*/
	CCIAlgorithm			algorithm;									/* Algorithm object/structure	*/
	cci_b					*attributes[CCI_CLASS_ATTRIBUTE_MAX];		/* Request attributes			*/
	cci_t					attribMask[(CCI_CLASS_ATTRIBUTE_MAX/32)+1];	/* Attribute set flags			*/
	CCIContext				next;										/* Pointer to next context		*/
} CCIContextRec;




/*
** --- Function signatures
*/
typedef cci_st ( *CCI_NATIVE_ALG_FUNCPTR ) (CCIProvider provider);
typedef cci_bool ( *CCI_LISTSEARCH_FUNCPTR ) (const void *source, const void *entry);
typedef void ( *CCI_THREAD_FUNCPTR ) (const void *entry);
typedef void ( *CCI_RANDNUM_FUNCPTR ) ( cci_b * randBuffer, cci_t numBits );
typedef CCIProvider ( *CCIProviderLoadProc ) ();
typedef void ( *CCI_POOLELEM_FUNCPTR ) (void *elementObj, cci_t elemSize);
typedef cci_st ( *CCI_PROVIDER_IDINIT_FUNCPTR ) ( CCIProvider provider );
typedef cci_st ( *CCI_PROVIDER_IDUNINIT_FUNCPTR ) ( CCIProvider provider );
typedef cci_st ( *CCI_SESSION_FUNCPTR ) (const CCIContext cciContext);
typedef cci_st ( *CCI_SESSION_OPEN_FUNCPTR ) (const CCIContext cciContext);
typedef cci_st ( *CCI_SESSION_INIT_FUNCPTR ) (const CCIContext cciContext);
typedef cci_st ( *CCI_SESSION_UPDATE_FUNCPTR ) (const CCIContext cciContext);
typedef cci_st ( *CCI_SESSION_FINAL_FUNCPTR ) (const CCIContext cciContext);
typedef cci_st ( *CCI_SESSION_CLOSE_FUNCPTR ) (const CCIContext cciContext);
typedef void * ( *CCI_SD_OPEN_FUNCPTR ) (char *name, BOOL create, UINT32 size, void ** pVirtAddress);
typedef void ( *CCI_SD_UNMAP_DELETE_FUNCPTR ) (void * pShareRegionId);
typedef void * ( *CCI_VMTRANSLATE_FUNCPTR ) (void * virtualAddress);

/*
** --- private functions used in public macros. (Used only for backward compatibility)
*/
void		ccip_show_buffer( char *title, void *buffer, cci_t length, cci_t lineLen );
cci_st		ccip_ctx_get_attr( const CCIContext cciContext, const CCI_ATTRIBUTE_ID attributeId, cci_g *value );
cci_st		ccip_ctx_update( const CCIContext cciContext, const cci_b *buffer, const cci_t bufferLength );
cci_st		ccip_ctx_digest_get( const CCIContext cciContext, const cci_b *digestBuffer, cci_t *digestLength );
cci_st		ccip_algorithm_set_attr( const CCIAlgorithm algorithm, const CCI_ATTRIBUTE_ID attributeId, cci_g value );
cci_bool	ccip_pvr_set_param( const CCIProvider provider, cci_t paramater, cci_g value );
CCIProvider ccip_default_provider( void );
#define		ccip_ctx_output_buffer( ctx )			( (ctx)->attributes[CCI_OUTPUT_BUFFER] )
#define		ccip_ctx_output_buffer_length( ctx )	( (cci_t *)(ctx)->attributes[CCI_OUTPUT_LENGTH] )
#define		cciDebug
#define		CCI_VERBOSE_TRACE	0x001
#define		CCI_VERBOSE_ERROR	0x002
#define		CCI_VERBOSE_DETAIL	0x003
#define		CCI_VERBOSE_DEBUG	0x004
#define		CCI_VERBOSE_FATAL	0x005


/*
**
** --- Public Prototypes...
**
*/
cci_st	cciSharedRegionSet( cci_t sharedSegmentSize );
cci_st	cciTaskPrioritySet( cci_t taskPrority );
void	cciShowVersion( void );
cci_st	cciSetNativeAlgorithm( CCI_NATIVE_ALG_FUNCPTR nativeFunctPtr );

cci_st	cciCipher( const CCI_PROVIDER_ID providerId, CCI_ALGORITHM_ID algorithmId, const cci_t direction,
								const CCI_MODE_CONSTANTS cipherMode, const cci_b *key, const cci_t keyLength, const cci_b *inputBuffer,
								const cci_t inputLength, cci_b *outputBuffer, cci_t *outputLength, const cci_g initVector,
								const cci_t initVectorLength );
cci_st	cciHashBlock( const CCI_PROVIDER_ID providerId, const CCI_ALGORITHM_ID algorithmId, 
								const cci_b *input, const cci_t inputLength, 
								cci_b *digest, cci_t *digestLength );
cci_st	cciHmacInit( CCIContext *cciContext, const CCI_PROVIDER_ID providerId,
								const CCI_ALGORITHM_ID algorithmId, const cci_b *key, const cci_t keyLength );
cci_st	cciHmacBlock( const CCI_PROVIDER_ID providerId, const CCI_ALGORITHM_ID algorithmId, 
								const cci_b *key, const cci_t keyLength, 
								const cci_b *input, const cci_t inputLength, 
								cci_b *digest, cci_t *digestLength );
cci_st	cciLibInit( void );
cci_st	cciLibUnInit( void );


cci_st	cciProviderCompare( const CCI_PROVIDER_ID providerId );
cci_st	cciProviderVerify( const CCI_PROVIDER_ID providerId, const CCI_CLASS_ID cciClass, CCI_ALGORITHM_ID );
cci_st	cciProviderValidate( const CCI_PROVIDER_ID providerId );
cci_st	cciProviderBenchmark( const CCI_PROVIDER_ID providerId );

cci_st	cciRand( const CCI_PROVIDER_ID providerId, cci_b *buffer, const cci_t numBytes );
cci_st 	cciRandSeed(const CCI_PROVIDER_ID providerId, cci_b *buffer, const cci_t numBytes );

cci_st	cciProviderLoad( const CCIProviderLoadProc providerLoadProc, CCI_PROVIDER_ID *providerId );
cci_st	cciProviderUnload( const CCI_PROVIDER_ID providerId );
cci_st	cciProviderInherit( const CCI_PROVIDER_ID providerId );

cci_st	cciPoolCreateReg( CCIPool *pool, cci_t elementSize, cci_t poolSize, cci_t poolMax, CCI_POOLELEM_FUNCPTR init, CCI_POOLELEM_FUNCPTR uninit );
cci_st	cciPoolDestroy( CCIPool pool );
cci_st	cciPoolAlloc( CCIPool pool, void **object, CCI_POOL_FLAG waitFlag );
cci_st	cciPoolRelease( CCIPool pool, void *object );


/*
**
** --- CCI Allocation functions
**
*/
void	*cciAlloc( const size_t blocksize );
void	cciMemFunctions(const CCI_REALLOC_FUNCPTR reallocProc, const CCI_MALLOC_FUNCPTR mallocProc,const CCI_FREE_FUNCPTR freeProc );
void	cciFree( void *memblock );
void	*cciRealloc( void *memblock, const size_t blocksize );
cci_st  cciSetSharedRegionSize( cci_t sharedSegmentSize );
void    cciStats();

/*
**
** --- CCI Shared Memory functions
**
*/
void cciSharedMemFunctions
    (
    const CCI_SD_OPEN_FUNCPTR sdOpenProc,
    const CCI_SD_UNMAP_DELETE_FUNCPTR sdUnmapDeleteProc,
    const CCI_VMTRANSLATE_FUNCPTR vmTranslateProc
    );

/*
**
** --- CCI Application API context processing functions
**
*/
cci_st		cciCtxCreate( CCIContext *cciContext, const CCI_PROVIDER_ID providerId, const CCI_CLASS cryptoClass,
									const CCI_ALGORITHM_ID algorithmId );
cci_st		cciCtxExecute( const CCIContext cciContext );
cci_st		cciCtxDigestLenGet( const CCIContext cciContext );
cci_st		cciCtxIvLenGet( const CCIContext cciContext );
cci_st		cciCtxDestroy( CCIContext cciContext );
cci_st		cciCtxInit( CCIContext cciContext, const CCI_PROVIDER_ID providerId, const CCI_CLASS cryptoClass,
					   const CCI_ALGORITHM_ID algorithmId );
cci_st		cciCtxClear( CCIContext cciContext );
cci_st		cciCtxPoolAlloc( CCIPool pool, CCIContext *cciContext, 
							const CCI_PROVIDER_ID providerId, const CCI_CLASS cryptoClass,
							const CCI_ALGORITHM_ID algorithmId, CCI_POOL_FLAG waitFlag );
cci_st		cciCtxPoolRelease( CCIPool pool, CCIContext cciContext );
cci_st		cciCtxAsyncStatus( CCIContext cciContext, cci_st cciStatus );
cci_st		cciCtxCipher( const CCIContext cciContext, const cci_b *inputBuffer, const cci_t inputLength,
									const cci_b *outputBuffer, cci_t *outputLength );
cci_st		cciCtxCompress( const CCIContext cciContext, const cci_b *inputBuffer, const cci_t inputLength,
										 const cci_b *ouputBuffer, cci_t *outputLength );
cci_bool	cciIsAlgorithmSet( const CCI_PROVIDER_ID providerId, const CCI_CLASS cryptoClass, const CCI_ALGORITHM_ID algorithmId );
cci_bool	cciIsNative( const CCI_PROVIDER_ID providerId, const CCI_CLASS cryptoClass, const CCI_ALGORITHM_ID algorithmId );
cci_st		cciCtxHashCopy( CCIContext destCtx, CCIContext srcCtx );


/*
**
** --- CCI Provider definition functions
**
*/
cci_st	cciProviderCreate( CCIProvider *provider, const cci_z providerName,
									const CCI_PROVIDER_IDINIT_FUNCPTR initHandler,
									const CCI_PROVIDER_IDUNINIT_FUNCPTR uninitHandler );
cci_b   *cciProviderName( const CCI_PROVIDER_ID providerId );
cci_st	cciProviderAlgorithmSet( const CCIProvider provider, const CCIAlgorithm algorithm );
void	cciProviderIntegerTableSet( const CCIProvider provider, const CCIIntegerTable *integerTable );
cci_st	cciProviderDestroy( const CCIProvider provider );

cci_st	cciAlgorithmCreate( CCIAlgorithm *algorithm, const CCI_CLASS cryptoClass, const CCI_ALGORITHM_ID algorithmId );
cci_st	cciAlgorithmHandlerSet( const CCIAlgorithm algorithm, const CCI_SESSION_HANDLER sessionHandler,
							 CCI_SESSION_FUNCPTR handler );
cci_st	cciAlgorithmDestroy( const CCIAlgorithm algorithm );

/*
**
** --- CCI large Integer functions
**
*/
#define		cciNumBytes( op )			((cciNumBits( op ) + 7)/8)
#define		cciIsOdd( op )				(cciTestBit( op, 0 ))
#define		cciIsEven( op )				(!cciIsOdd( op ))
#define		cciSetZero( op )			(cciSetWord(op,0)) 
#define		cciInc( op )				(cciAddWord( op, op, 1 ))
#define		cciDec( op )				(cciSubWord( op, op, 1 ))
#define		cciIsWord( op, w )			(!cciCmpWord( op, w )?1:0)
#define		cciIsZero( op )				(!cciCmpWord( op, 0 )?1:0)
#define		cciSqr( rop, op )			(cciExpWord( rop, op, 2 ))
#define		cciModAdd( rop, op1, op2, n){cci_m t=cciIntegerDup( rop );cciAdd( t, op1, op2);cciMod( rop, t, n);cciIntegerFree(t);}
#define		cciModSub( rop, op1, op2, n){cci_m t=cciIntegerDup( rop );cciSub( t, op1, op2);cciMod( rop, t, n);cciIntegerFree(t);}
#define		cciModMul( rop, op1, op2, n){cci_m t=cciIntegerDup( rop );cciMul( t, op1, op2);cciMod( rop, t, n);cciIntegerFree(t);}
#define		cciModSqr( rop, op1, n)		{cci_m t=cciIntegerDup( rop );cciSqr( t, op1 );cciMod( rop, t, n);cciIntegerFree(t);}


void		cciGcd( cci_m rop, cci_m op1, cci_m op2 );
cci_m		cciInteger( CCI_PROVIDER_ID providerId );
cci_st		cciIntegerFree( cci_m op1 );

cci_m		cciIntegerDec( CCI_PROVIDER_ID providerId, cci_z decValue );
cci_m		cciIntegerHex( CCI_PROVIDER_ID providerId, cci_z hexValue );
cci_m		cciIntegerBin( CCI_PROVIDER_ID providerId, cci_b *binvalue, cci_t binLength );
cci_m		cciIntegerDup( cci_m op );

void		cciRandomInt( cci_m rop, cci_t numBits );
void		cciPrime( cci_m rop, cci_t numBits );
cci_bool	cciIsPrime( cci_m op1 );

void		cciImportBin( cci_m op, cci_b *byteArray, cci_t binLength );
cci_t		cciExportBin( cci_m op, cci_b **byteArray );
void 		cciCopy( cci_m op1, cci_m op2 );

void		cciSetWord( cci_m op1,  cci_t wordValue );
void		cciSetDec( cci_m op, cci_z decValue );
void		cciSetHex( cci_m op, cci_z hexValue );

void		cciAddWord( cci_m rop, cci_m op, cci_t value );
void		cciSubWord( cci_m rop, cci_m op, cci_t value );
void		cciMulWord( cci_m rop, cci_m op, cci_t value );
void		cciExpWord( cci_m rop, cci_m op, cci_t value );
void		cciModWord( cci_m rop, cci_m op, cci_t modulas );

cci_b		*cciHexStr( cci_m op1 );
cci_b		*cciDecStr( cci_m op1 );

void		cciAdd( cci_m rop, cci_m op1, cci_m op2 );
void		cciSub( cci_m rop, cci_m op1, cci_m op2 );
void		cciMul( cci_m rop, cci_m op1, cci_m op2 );
void		cciDiv( cci_m rop, cci_m op1, cci_m op2, cci_m remainder );
void		cciExp( cci_m rop, cci_m op1, cci_m exponent );
void		cciMod( cci_m rop, cci_m op1, cci_m modulas );
void		cciModExp( cci_m, cci_m, cci_m, cci_m );
void		cciModInverse( cci_m rop, cci_m op1, cci_m modulas );

int			cciCmp( cci_m op1, cci_m op2 );
int			cciCmpWord( cci_m op, cci_t value );
int			cciCmpDec( cci_m op, cci_z decValue );
int			cciCmpHex( cci_m op, cci_z hexValue );

void		cciLShift( cci_m rop, cci_m op1, cci_t shift );
void		cciRShift( cci_m rop, cci_m op1, cci_t shift );

cci_bool	cciIsPos( cci_m op1 );
cci_bool	cciIsNeg( cci_m op1 );
void		cciPos( cci_m op1 );
void		cciNeg( cci_m op1 );

cci_t		cciNumBits( cci_m op1 );
cci_bool	cciTestBit( cci_m op, cci_t bit );
cci_bool	cciSetBit( cci_m op, cci_t bit );
cci_bool	cciClearBit( cci_m op, cci_t bit );
cci_bool	cciMaskBits( cci_m op, cci_t numBits );

cci_b		*cciHexToBin( cci_z hexStr, cci_t *retBytes );
cci_z		cciBinToHex( cci_b *numBuffer, cci_t numBytes, cci_b *buffer, cci_t bufferLength,
									 cci_bool lsbFirst );

/*
** --- Obsolete Large Integer routines. Other routines whose interfaces can't be touched
**     include cciIntegerBin(), cciIntegerFree() and cciExportBin()
*/
cci_m		cciModExpObsolete( cci_m op, cci_m exponent, cci_m modulas );
#define cci_mp_initBin						cciIntegerBin			
#define cci_mp_destroy						cciIntegerFree			
#define cci_mp_export_bin					cciExportBin			
#define cci_mp_modExp						cciModExpObsolete				




/*
**
** --- CCI PUBLIC KEY functions
**
*/
cci_st	cciPKIKeyCreate( const CCI_PROVIDER_ID providerId, const CCI_PUBLIC_KEY_TYPE pkType, 
								CCIPublicKey *publicKey );
cci_st	cciPKIKeyDestroy( const CCIPublicKey publicKey );
CCI_PUBLICKEY cciPKIAlgorithm( CCIPublicKey publicKey );
cci_st	cciPKIKeyCompSet( const CCIPublicKey publicKey, const CCI_PUBLIC_KEY_COMPONENT compType,
										   const cci_b *component, const cci_t compLength );
cci_st	cciPKIKeyCompGet( const CCIPublicKey publicKey, const CCI_PUBLIC_KEY_COMPONENT compType,
										   cci_b **component, cci_t *compLength );
cci_st cciPKIGenerateKeys( CCI_PROVIDER_ID providerId, CCI_PUBLICKEY pkiAlgorithmType,
						CCIPublicKey *privateKey, CCIPublicKey *publicKey, cci_t keyLength );
cci_st cciPKISignMessage( CCIPublicKey privateKey, cci_t signType,
						 cci_b *message, cci_t messageLength, 
						 cci_b **signature, cci_t *signatureLength );
cci_st cciPKIVerifyMessage( CCIPublicKey publicKey, cci_t signType,
						cci_b *message, cci_t messageLength, 
						cci_b *signature, cci_t signatureLength );
cci_st cciPKISignDigest( CCIPublicKey privateKey, cci_t signType, 
									cci_b *digest,
									cci_b **signature, cci_t *signatureLength );
cci_st cciPKIVerifyDigest( CCIPublicKey publicKey, cci_t signType, 
								 cci_b *digest, 
								 cci_b *signature, cci_t signatureLength );
cci_st cciPKIEncrypt( CCIPublicKey publicKey, cci_t paddingFormat, cci_t hashingFormat,
						cci_b *plainText, cci_t plainTextLength,
						cci_b **cipherText, cci_t *cipherTextLength);
cci_st cciPKIDecrypt( CCIPublicKey privateKey, cci_t paddingFormat, cci_t hashingFormat, 
						cci_b *cipherText, cci_t cipherTextLength,
						cci_b **plainText, cci_t *plaintTextLength );
cci_st cciPKIBlindingSet( CCIPublicKey publicKey, BOOL blindEnable );
cci_st cciPKIBlindingGet( CCIPublicKey publicKey, BOOL *blindState );
cci_t cciPKISize( CCIPublicKey publicPrivateKey );


cci_st	cciCtxPKIKeyGen( const CCIContext cciContext );
cci_st	cciCtxPKISign( const CCIContext cciContext );
cci_st	cciCtxPKIVerify( const CCIContext cciContext );
cci_st	cciCtxPKIEncrypt( const CCIContext cciContext );
cci_st	cciCtxPKIDecrypt( const CCIContext cciContext );


/* 
** --- For backward compatibility, keep older status codes around for a while...
**/
#define CCI_SUCCESS							S_cciLib_SUCCESS							
#define CCI_FAILURE							S_cciLib_FAILURE							
#define CCI_LIB_ALREADY_INITIALIZED			S_cciLib_LIB_ALREADY_INITIALIZED			
#define CCI_LIB_NOT_INITIALIZED				S_cciLib_LIB_NOT_INITIALIZED				
#define CCI_CONTEXT_IS_INVALID				S_cciLib_CONTEXT_IS_INVALID				
#define CCI_UNKNOWN_ATTRIBUTE				S_cciLib_UNKNOWN_ATTRIBUTE				
#define CCI_UNEXPECTED_ATTRIBUTE			S_cciLib_UNEXPECTED_ATTRIBUTE			
#define CCI_UNEXPECTED_ATTRIBUTE_TYPE		S_cciLib_UNEXPECTED_ATTRIBUTE_TYPE		
#define CCI_UNKNOWN_ATTRIBUTE_DATATYPE		S_cciLib_UNKNOWN_ATTRIBUTE_DATATYPE		
#define CCI_ATTRIBUTE_NOT_FOUND				S_cciLib_ATTRIBUTE_NOT_FOUND				
#define CCI_INVALID_ATTRIBUTE_VALUE			S_cciLib_INVALID_ATTRIBUTE_VALUE			
#define CCI_MISSING_ATTRIBUTE				S_cciLib_MISSING_ATTRIBUTE				
#define CCI_MEMALLOC_FAILED					S_cciLib_MEMALLOC_FAILED					
#define CCI_MORE_DATA						S_cciLib_MORE_DATA						
#define CCI_NO_MORE_DATA					S_cciLib_NO_MORE_DATA					
#define CCI_ALGORITHM_NOT_SUPPORTED			S_cciLib_ALGORITHM_NOT_SUPPORTED			
#define CCI_PROVIDER_NOT_REGISTED			S_cciLib_PROVIDER_NOT_REGISTED			
#define CCI_INVALID_ALGORITHM_ID			S_cciLib_INVALID_ALGORITHM_ID			
#define CCI_INVALID_CRYPTO_CLASS_ID			S_cciLib_INVALID_CRYPTO_CLASS_ID			
#define CCI_INVALID_PROVIDER_ID				S_cciLib_INVALID_PROVIDER_ID				
#define CCI_PROVIDER_NULL_FROM_LOADPROC		S_cciLib_PROVIDER_NULL_FROM_LOADPROC		
#define CCI_PROVIDER_INIT_FAILED			S_cciLib_PROVIDER_INIT_FAILED			
#define CCI_PROVIDER_DEFINED_STATUS			S_cciLib_PROVIDER_DEFINED_STATUS			
#define CCI_INVALID_IDENTIFIER				S_cciLib_INVALID_IDENTIFIER				
#define CCI_PROVIDER_UNKNOWN_STATUS			S_cciLib_PROVIDER_UNKNOWN_STATUS			
#define CCI_INPUT_BUFFER_NOT_BYTE_ALIGNED	S_cciLib_INPUT_BUFFER_NOT_BYTE_ALIGNED	
#define CCI_CIPHER_MODE_NOT_SUPPORTED		S_cciLib_CIPHER_MODE_NOT_SUPPORTED		
#define CCI_INVALID_KEY_LENGTH				S_cciLib_INVALID_KEY_LENGTH				
#define CCI_PROVIDER_ALREADY_LOADED			S_cciLib_PROVIDER_ALREADY_LOADED			
#define CCI_OUTPUT_BUFFER_TOO_SMALL			S_cciLib_OUTPUT_BUFFER_TOO_SMALL			
#define CCI_INVALID_IV_LENGTH				S_cciLib_INVALID_IV_LENGTH				
#define CCI_PARAMETER_STRING_TOO_LONG		S_cciLib_PARAMETER_STRING_TOO_LONG		
#define CCI_DECODING_ERROR					S_cciLib_DECODING_ERROR					
#define CCI_MESSAGE_TOO_LONG				S_cciLib_MESSAGE_TOO_LONG				
#define CCI_INTENDED_MESSAGE_TOO_SHORT		S_cciLib_INTENDED_MESSAGE_TOO_SHORT		
#define CCI_INVALID_HASH_FUNCTION			S_cciLib_INVALID_HASH_FUNCTION			
#define CCI_INVALID_ENCODING_FORMAT			S_cciLib_INVALID_ENCODING_FORMAT			
#define CCI_MODULAS_TOO_SMALL				S_cciLib_MODULAS_TOO_SMALL				
#define CCI_INVALID_SIGNATURE				S_cciLib_INVALID_SIGNATURE				
#define CCI_SESSION_OPEN_ERROR				S_cciLib_SESSION_OPEN_ERROR				
#define CCI_VALIDATION_TEST_NOT_AVAIL		S_cciLib_VALIDATION_TEST_NOT_AVAIL		
#define CCI_MAX_MTU_LENGTH_EXCEEDED			S_cciLib_MAX_MTU_LENGTH_EXCEEDED			
#define CCI_DIGEST_BUFFER_TOO_SMALL			S_cciLib_DIGEST_BUFFER_TOO_SMALL			
#define CCI_PROVIDER_SIGNATURE_INVALID		S_cciLib_PROVIDER_SIGNATURE_INVALID		




/* 
** --- For backward compatibility, reference previous function names...
*/
#define cci_cipher							cciCipher				
#define cci_hmac_block						cciHmacBlock			
#define cci_hmac_init						cciHmacInit				
#define cci_hmac_update						cciHmacUpdate			
#define cci_hmac_final						cciHmacFinal			
#define cci_hash_block	       	 			cciHashBlock			
#define cci_hash_init						cciHashInit				
#define cci_hash_update						cciHashUpdate			
#define cci_hash_final						cciHashFinal			
#define cci_init							cciLibInit				
#define cci_done							cciLibUnInit			
#define cci_rand_num						cciRand					
#define cci_rand_seed						cciRandSeed				
#define cci_pvr_define						cciProviderCreate		
#define cci_pvr_set_param					cciProviderAttrSet		
#define cci_pvr_get_param					cciProviderAttrGet		
#define cci_pvr_add_algorithm				cciProviderAlgorithmSet	
#define cci_pvr_destroy						cciProviderDestroy		
#define cci_pvr_compare						cciProviderCompare		
#define cci_validate_algorithm				cciProviderVerify		
#define cci_pvr_validate					cciProviderValidate		
#define cci_pvr_performance					cciProviderBenchmark	
#define cci_load							cciProviderLoad			
#define cci_unload							cciProviderUnload		
#define cci_use_software_default			cciProviderInherit		
#define cci_alg_define						cciAlgorithmCreate		
#define cci_alg_set_handler					cciAlgorithmHandlerSet	
#define cci_alg_destroy						cciAlgorithmDestroy		
#define cci_is_algorithm_avail				cciIsAlgorithmSet		
#define cci_activity_cb						cciActivityCbSet		
#define cci_ctx_pool_create					cciCtxPoolCreate		
#define cci_ctx_pool_alloc					cciCtxPoolAlloc			
#define cci_ctx_pool_release				cciCtxPoolRelease		
#define cci_ctx_pool_destroy				cciCtxPoolDestroy		
#define cci_pool_create						cciPoolCreate			
#define cci_pool_alloc						cciPoolAlloc			
#define cci_pool_release					cciPoolRelease			
#define cci_pool_destroy					cciPoolDestroy			
#define cci_hex_to_bin						cciHexToBin				
#define cci_bin_to_hex						cciBinToHex				
#define cci_ctx_create						cciCtxCreate			
#define cci_ctx_init						cciCtxInit				
#define cci_ctx_destroy						cciCtxDestroy			
#define cci_ctx_update						cciCtxUpdate			
#define cci_ctx_set_attr					cciCtxAttrSet			
#define cci_ctx_get_attr					cciCtxAttrGet			
#define cci_ctx_clr_attr					cciCtxAttrClr			
#define cci_ctx_cipher						cciCtxCipher			
#define cci_ctx_digest_length				cciCtxDigestLenGet		
#define cci_ctx_iv_length					cciCtxIvLenGet			
#define cci_ctx_compression					cciCtxCompress			
#define cci_notification					cciCtxAsyncStatus		
#define cci_ctx_async_req					cciCtxIsAsyncReq		
#define cci_ctx_clear						cciCtxClear				
#define cci_ctx_provider					cciCtxProviderId		
#define cci_ctx_algorithm					cciCtxAlgorithmId		
#define cci_ctx_class						cciCtxClassId			
#define cci_ctx_set_missing_attr			cciCtxMissingAttrSet	
#define cci_ctx_missing_attr				cciCtxMissingAttrGet	
#define cci_ctx_set_user_object				cciCtxUserObjectSet		
#define cci_ctx_get_user_object				cciCtxUserObjectGet		
#define cci_ctx_get_data					cciCtxInputGet			
#define cci_ctx_get_session_ctx				cciCtxSessionCtxGet		
#define cci_ctx_set_session_ctx				cciCtxSessionCtxSet		
#define cci_ctx_publickey_sign				cciCtxPKISign			
#define cci_ctx_publickey_verify			cciCtxPKIVerify			
#define cci_ctx_publickey_encrypt			cciCtxPKIEncrypt		
#define cci_ctx_publickey_decrypt			cciCtxPKIDecrypt		
#define cci_ctx_publickey_gen				cciCtxPKIKeyGen			
#define cci_pki_create(a,b,c)				cciPKIKeyCreate(b,c,a)			
#define cci_pki_destroy						cciPKIKeyDestroy		
#define cci_pki_set_component				cciPKIKeyCompSet		
#define cci_pki_get_component				cciPKIKeyCompGet		
#define cci_alloc							cciAlloc				
#define cci_free							cciFree					
#define cci_realloc							cciRealloc				
#define cci_debug							cciDebug				

#define cci_hnd_get_attr					cciCtxAttrGet
#define cci_hnd_get_data					cciCtxInputGet
#define cci_hnd_algorithm					cciCtxAlgorithmId
#define cci_hnd_set_missing_attr    

#define cci_r								unsigned char *				
#define cci_alg_set_attr(a,b,c,d)			(cciAlgorithmAttrSet(a,b,c))
#define cci_ctx_get_outputbuffer( ctx )		(ctx->attributes[CCI_OUTPUT_BUFFER])
#define cci_ctx_get_outputbuffer_length( ctx ) ((cci_t *)ctx->attributes[CCI_OUTPUT_LENGTH])
#define cciPKIBlinding						cciPKIBlindingSet

#ifdef __cplusplus
}
#endif

#endif
