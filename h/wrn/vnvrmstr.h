/*
 * $Log:: /Tornado/target/h/NB/vnvrmstr.h                                                       $
 * 
 * 1     2/09/00 4:42p Admin
 * 
 * 2     8/26/99 7:42p Alex
 * 
 * 1     8/25/99 7:01p Alex
 * 
 * 1     5/26/99 9:53a Alex
 * 
 * 1     4/14/99 5:12p Rajive
 * Initial vesrion of Next Gen RouterWare Source code
 * 
 * 1     1/18/99 5:10p Mahesh
 * 
 * 1     1/18/99 4:24p Mahesh
 * 
 * 6     11/05/98 12:15p Alex
 * 
 * 5     10/29/98 1:38p Alex
 * Added new function prototype for NVRAM module.
 * 
 * 4     4/30/98 1:16p Rajive
 * Overwrote with INCLUDE v4.2.1
 * 
 * 3     4/27/98 11:42a Release Engineer
 * Additions into include for L2TP and RTM modules.
 * INCLUDE v4.2.1
 * 
 * 2     3/27/96 7:08p Ross
 * Initial check-in with source safe.
*/
/*	$Modname: vnvrmstr.h$  $version: 1.7$      $date: 02/11/94$   */
/*
* 	$lgb$
1.0 10/21/93 ross
1.1 10/21/93 ross
1.2 10/21/93 ross
1.3 11/01/93 ross ipx certification version
1.4 12/01/93 ross lsl version 3 additions.
1.5 01/08/94 ross More LSL version 3 changes
1.6 02/02/94 ross fixed lslsnap bugs, and added ipx protocol detection, support for dlsx and nlsp
1.7 02/11/94 ross added stuff for dlsw and nlsp.
* 	$lge$
*/
/************************************************************************/
/*	Copyright (C) 1989 - 1998 RouterWare, Inc.									*/
/*	Unpublished - rights reserved under the Copyright Laws of the			*/
/*	United States.  Use, duplication, or disclosure by the 					*/
/*	Government is subject to restrictions as set forth in 					*/
/*	subparagraph (c)(1)(ii) of the Rights in Technical Data and 			*/
/*	Computer Software clause at 252.227-7013.										*/
/*	RouterWare, Inc., 3961 MacArthur Blvd. Suite 212, Newport Beach Ca   */
/************************************************************************/
#if !defined (_VNMRMSTR_H_)
#define _VNMRMSTR_H_

typedef	struct	CONFIGURATION_FUNCTION
{
	void 	(*fptr_parameter_function) (char *cptr_start_of_configuration_string,ULONG parameter_1,
		ULONG ulptr_parameter_2,ULONG parameter_3);
	BOOLEAN *eptr_enable_string;
	char *cptr_parameter_string;
	ULONG	parameter_1;
	ULONG	ulptr_parameter_2;
	ULONG	parameter_3;
	ULONG	minimum_value;
	ULONG	maximum_value;
} CONFIGURATION_FUNCTION;

typedef	struct	CONFIGURATION_TABLE
{
	BOOLEAN 				valid;

	CONFIGURATION_FUNCTION	function[200];

} CONFIGURATION_TABLE;	

typedef	struct	NVRAM_CLASS
{
	BOOLEAN 			display_configuration_file;

	BYTE 						current_configuration_section_index;

	CONFIGURATION_TABLE	*sptr_current_configuration_table;

	BYTE 						value[12];
	BYTE 						number_of_configurations;
} NVRAM_CLASS;

void set_variable_port_and_enable (char *cptr_start_of_configuration_string,ULONG vptr_parameter_1,
	ULONG ulptr_parameter_2,ULONG size_of_parameter);
void set_variable_port_less_one_and_enable (char *cptr_start_of_configuration_string,ULONG vptr_parameter_1,
	ULONG ulptr_parameter_2,ULONG size_of_parameter);
void set_variable_port_and_ushort_decimal_value (char *cptr_start_of_configuration_string,ULONG vptr_parameter_1,
	ULONG ulptr_parameter_2,ULONG size_of_parameter);
void set_variable_port_and_ushort_hex_value (char *cptr_value_string,ULONG vptr_parameter_1,
	ULONG ulptr_parameter_2,ULONG size_of_parameter);
void set_variable_port_and_swapped_ushort_hex_value (char *cptr_value_string,ULONG vptr_parameter_1,
	ULONG ulptr_parameter_2,ULONG size_of_parameter);
void set_ushort_swapped_hex_value (char *cptr_start_of_configuration_string,ULONG offset,ULONG ulptr_base);
void set_ushort_decimal_value (char *cptr_start_of_configuration_string,ULONG offset,ULONG ulptr_base);
void set_byte_decimal_value (char *cptr_start_of_configuration_string,ULONG offset,ULONG ulptr_base);
void set_variable_string (char *cptr_start_of_configuration_string,ULONG offset,ULONG ulptr_base);
void set_variable_port_and_string (char *cptr_value_string,ULONG offset,ULONG ulptr_base,ULONG size_of_entry);
void set_ulong_swapped_hex_value (char *cptr_start_of_configuration_string,ULONG offset,ULONG ulptr_base);
void nvram_memcpy (void *vptr_destination,void *vptr_source,USHORT number_of_bytes);
BOOLEAN is_parameter_enabled (char *cptr_parameter_string);
USHORT get_port_number_and_string (char *cptr_port_number_and_value,char *return_string);
ULONG get_port_number_and_value (char *cptr_port_number_and_value,USHORT *usptr_port_number);
ULONG get_port_number_and_hex_value (char *cptr_port_number_and_value,USHORT *usptr_port_number);
ULONG get_ulong_hex_value (char *cptr_port_number_and_value);
ULONG get_decimal_value (char *cptr_value);
void set_variable_port_and_ulong_decimal_value (char *cptr_value_string,ULONG vptr_parameter_1,
	ULONG ulptr_parameter_2,ULONG size_of_parameter);
void set_ulong_decimal_value (char *cptr_value_string,ULONG offset,ULONG ulptr_base);
void set_ulong_value (char *cptr_value_string,ULONG offset,ULONG ulptr_base);
void set_variable_port_and_byte_decimal_value (char *cptr_value_string,ULONG vptr_parameter_1,
	ULONG ulptr_parameter_2,ULONG size_of_parameter);
void set_variable_port_and_swapped_ulong_hex_value (char *cptr_value_string,ULONG vptr_parameter_1,
	ULONG ulptr_parameter_2,ULONG size_of_parameter);
void set_variable_port_and_mac_address (char *cptr_ethernet_string,ULONG vptr_parameter_1,
	ULONG ulptr_parameter_2,ULONG size_of_parameter);
void set_mac_address (char *cptr_mac_address_string,ULONG offset,ULONG ulptr_base);
ULONG convert_4_bytes_to_ulong (BYTE byte_1,BYTE byte_2,BYTE byte_3,BYTE byte_4);
void add_section_name_and_configuration_table_pointer (char *cptr_section_name,
	CONFIGURATION_TABLE *sptr_configuration_table_to_add);
void set_variable_port_and_ip_address (char *cptr_ip_address_string,ULONG vptr_parameter_1,
	ULONG ulptr_parameter_2,ULONG size_of_parameter);
void set_ip_address (char *cptr_ip_address_string,ULONG offset,ULONG ulptr_base);
void set_variable_port_and_enum_enable (char *cptr_start_of_configuration_string,ULONG vptr_parameter_1,
	ULONG ulptr_parameter_2,ULONG size_of_parameter);
void set_uint_decimal_value (char *cptr_value_string,ULONG offset,ULONG ulptr_base);
void set_enum_enable (char *cptr_value_string,ULONG offset,ULONG ulptr_base);
void set_variable_port_and_ulong_enable (char *cptr_start_of_configuration_string,ULONG vptr_parameter_1,
	ULONG ulptr_parameter_2,ULONG size_of_parameter);


#endif /*_VNMRMSTR_H_ */
