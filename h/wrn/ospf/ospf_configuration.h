/* ospf_configuration.h */

/* Copyright 2000-2004 Wind River Systems, Inc. */

/*
modification history
-------------------
02d,30apr04,mwv SPR#91272, added support for area_summary configuration in the area 
                configuration.
02c,29jan04,ram NBMA, PTMP, & Unnumbered modifications
02b,22jan04,ram Merge fixes
02a,29may03,agi Modified RWOS calls to OSPF calls as part of RWOS removal
01x,20feb03,kc  Fixed SPR#86319 - rename inject_default_route_if_stub_area
                to inject_summary_lsa_into_stub_area. Rename staic keyword
                "OSPF Area Inject Default Route Into Stub Area" to
                "OSPF Area Inject Summary LSA Into Stub Area".
01w,10apr02,bt  added "OSPF Port NBMA or PTMP Neighbor Address ="
01v,24jul01,jkw Removed WINROUTER preproc
01u,23jul01,jkw Added new configuration variables for ignoring tos and demand circuit
01t,26sep00,res Added WindRiver CopyRight
01s,25sep00,res RFC-1587 implementation for OSPF NSSA Option, also tested against ANVL.
01r,20jul00,res Unix compatibility related changes.
01q,06jul00,res Removed unnecessary header files and defines.
01p,04apr00,res Added some MIB support (Read only).
                Passed all important ANVL OSPF tests.
01o,23feb00,res Changes for ospf mib
01n,23dec99,res Compatibility with VxWorks-IP and VxWorks RTM-interface
01m,03jun99,jac Added missing config parameter inject_default_route_if_stub_area in
                stub area case.
01l,17may99,jac Added new include file ospf_patricia_32_bits_key_prototypes.h
01k,28dec98,jac Compiled and added some comments
01j,13nov98,jac Changes related to introducing queuing in OSPF to RTM interface and bug
                fix on the external route additions path (to RTM)
01i,11nov98,jac Config changes, linted and big endian changes
01h,30oct98,jac Incorporate changes for compilation on Vxworks
01g,23aug98,jac ANVL tested OSPF with PATRICIA tree route table and no recursion
01f,10aug98,jac PATRICIA Route Table Based OSPF Code Base
01e,04jun98,jac Integration with RTM and BGP
01d,10jul97,cin Pre-release v1.52b
01c,10feb97,cin Release Version 1.52
01b,22oct97,cin Release Version 1.50
01a,05jun96,cin First Beta Release
*/

#if !defined (_OSPF_CONFIGURATION_H_)
#define _OSPF_CONFIGURATION_H_
#if defined (__OSPF_VIRTUAL_STACK__)
#include "ospf_vs_lib.h"
#endif /* __OSPF_VIRTUAL_STACK__ */

/********************************************************************************************************************************/
#if !defined (__OSPF_VIRTUAL_STACK__)
OSPF_CONFIGURATION_TABLE ospf_configuration_table =
{
	1,
	{
/* _____________________________________________________________________________________________ */
	{
		(void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
		ospf_set_enum_enable,
		NULL,
		"OSPF =",
		(ULONG) offsetof (OSPF_CLASS,protocol_enabled),
		(ULONG ) &ospf,
		(ULONG) NULL
	},

/* _____________________________________________________________________________________________ */
	{
		(void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
		ospf_set_default_values,
		NULL,
		"OSPF Use Default Values ="
	},

/* _____________________________________________________________________________________________ */
	{
		(void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
		ospf_set_ip_address,
		NULL,
		"OSPF Router ID =",
		(ULONG) offsetof (OSPF_CLASS,router_id),
		(ULONG) &ospf,
		(ULONG) NULL
	},

/* _____________________________________________________________________________________________ */
	{
		(void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
		ospf_set_enum_enable,
		NULL,
		"OSPF Type of Service Capability =",
		(ULONG) offsetof (OSPF_CLASS,type_of_service_capability),
		(ULONG ) &ospf,
		(ULONG) NULL
	},

/* _____________________________________________________________________________________________ */
	{
		(void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
		ospf_set_enum_enable,
		NULL,
		"OSPF Autonomous System Border Router =",
		(ULONG) offsetof (OSPF_CLASS,autonomous_system_border_router),
		(ULONG ) &ospf,
		(ULONG) NULL
	},

/* _____________________________________________________________________________________________ */
	{
		(void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
		ospf_set_enum_enable,
		NULL,
		"OSPF IP Multicast =",
		(ULONG) offsetof (OSPF_CLASS,ip_multicast),
		(ULONG ) &ospf,
		(ULONG) NULL
	},

/* _____________________________________________________________________________________________ */
	{
		(void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
		ospf_set_ulong_decimal_value,
		NULL,
	"OSPF Number of Areas =",
		(ULONG) offsetof (OSPF_CLASS, number_of_areas),
		(ULONG) &ospf,
		(ULONG) NULL
	},


/* _____________________________________________________________________________________________ */
	{
		(void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
		ospf_set_variable_port_and_ip_address,
		NULL,
	"OSPF Area ID =",
		(ULONG) offsetof (OSPF_CLASS, area[0].config.area_id),
		(ULONG) &ospf,
		sizeof (OSPF_AREA_CLASS)
	},

/* RFC 2178 G.3 */
/* _____________________________________________________________________________________________ */
	{
		(void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
		ospf_set_ulong_decimal_value,
		NULL,
	"OSPF Total Number of Area Address Ranges =",
		(ULONG) offsetof (OSPF_CLASS, number_of_area_address_ranges),
		(ULONG) &ospf,
		(ULONG) NULL
	},

/* RFC 2178 G.3 Multiple Area Address ranges*/
/* _____________________________________________________________________________________________ */
	{
		(void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
		ospf_set_variable_port_and_ip_address,
		NULL,
	"OSPF Area Address Range Address =",
		(ULONG) offsetof (OSPF_CLASS, area_address_range[0].config.network),
		(ULONG) &ospf,
		sizeof (OSPF_AREA_ADDRESS_RANGE_CLASS)
	},
/* _____________________________________________________________________________________________ */
	{
		(void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
		ospf_set_variable_port_and_ip_address,
		NULL,
	"OSPF Area Address Range Mask =",
		(ULONG) offsetof (OSPF_CLASS, area_address_range[0].config.mask),
		(ULONG) &ospf,
		sizeof (OSPF_AREA_ADDRESS_RANGE_CLASS)
	},

/* _____________________________________________________________________________________________ */
	{
		(void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
		ospf_set_variable_port_and_enable,
		NULL,
	"OSPF Area Address Range Advertise =",
		(ULONG) offsetof (OSPF_CLASS, area_address_range[0].config.advertise),
		(ULONG) &ospf,
		sizeof (OSPF_AREA_ADDRESS_RANGE_CLASS)
	},


/* _____________________________________________________________________________________________ */
	{
		(void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
		ospf_set_variable_port_and_ip_address,
		NULL,
	"OSPF Area Address Range Area ID =",
		(ULONG) offsetof (OSPF_CLASS, area_address_range[0].config.area_id),
		(ULONG) &ospf,
		sizeof (OSPF_AREA_ADDRESS_RANGE_CLASS)
	},


/* _____________________________________________________________________________________________ */
	{
		(void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
		ospf_set_variable_port_and_enable,
		NULL,
	"OSPF Area External Routing Capability =",
		(ULONG) offsetof (OSPF_CLASS, area[0].config.external_routing_capability_enabled),
		(ULONG) &ospf,
		sizeof (OSPF_AREA_CLASS)
	},
/* _____________________________________________________________________________________________ */
	{
		(void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
		ospf_set_variable_port_and_enable,
		NULL,
	"OSPF Area NSSA option =",
		(ULONG) offsetof (OSPF_CLASS, area[0].config.nssa_enabled),
		(ULONG) &ospf,
		sizeof (OSPF_AREA_CLASS)
	},
/* _____________________________________________________________________________________________ */
	{
		(void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
		ospf_set_variable_port_and_enable,
		NULL,
    "OSPF Area Inject Default Route Into Stub Area =",
		(ULONG) offsetof (OSPF_CLASS, area[0].config.inject_default_route_if_stub_area),
		(ULONG) &ospf,
		sizeof (OSPF_AREA_CLASS)
	},

/* _____________________________________________________________________________________________ */
    /* SPR#91272 -begin */
	{
		(void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
		ospf_set_variable_port_and_enable,
		NULL,
    "OSPF Area Area Summary =",
		(ULONG) offsetof (OSPF_CLASS, area[0].config.area_summary),
		(ULONG) &ospf,
		sizeof (OSPF_AREA_CLASS)
	},
    /* SPR#91272 -end */

/* _____________________________________________________________________________________________ */
	{
		(void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
		ospf_set_variable_port_and_ulong_decimal_value,
		NULL,
	"OSPF Area Stub Default Cost =",
		(ULONG) offsetof (OSPF_CLASS, area[0].config.stub_default_cost),
		(ULONG) &ospf,
		sizeof (OSPF_AREA_CLASS)
	},


/* _____________________________________________________________________________________________ */
	{
		(void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
		ospf_set_ulong_decimal_value,
		NULL,
	"OSPF Number of Ports =",
		(ULONG) offsetof (OSPF_CLASS, number_of_ports),
		(ULONG) &ospf,
		(ULONG) NULL
	},

/* _____________________________________________________________________________________________ */
    {
        (void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
        ospf_set_port,
        NULL,
        "OSPF Port =",
        (ULONG) offsetof (OSPF_CLASS, port[0].config.port_enabled),
        (ULONG) &ospf,
        sizeof (OSPF_PORT_CLASS)
    },

/* _____________________________________________________________________________________________ */
	{
		(void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
		ospf_set_variable_port_and_ip_address,
		NULL,
	"OSPF Port Address =",
		(ULONG) offsetof (OSPF_CLASS, port[0].config.ip_address),
		(ULONG) &ospf,
		sizeof (OSPF_PORT_CLASS)
	},

/* __________________________________________________________ unnumbered dest ip */
	{
		(void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
		ospf_set_variable_port_and_ip_address,
		NULL,
	"OSPF Port Unnumbered Dest IP =",
		(ULONG) offsetof (OSPF_CLASS, port[0].config.unnumbered_dest_ip),
		(ULONG) &ospf,
		sizeof (OSPF_PORT_CLASS)
	},

/* __________________________________________________________ */
	{
		(void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
		ospf_set_variable_port_and_ip_address,
		NULL,
	"OSPF Port NBMA or PTMP Neighbor Address =",
		(ULONG) offsetof (OSPF_CLASS, port[0].config.nbma_ptmp_neighbor_address),	/* __NBMA_PTMP__ */
		(ULONG) &ospf,
		sizeof (OSPF_PORT_CLASS)
	},

/* _____________________________________________________________________________________________ */
	{
		(void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
		ospf_set_variable_port_and_ip_address,
		NULL,
	"OSPF Port Address Mask =",
		(ULONG) offsetof (OSPF_CLASS, port[0].config.subnetmask),
		(ULONG) &ospf,
		sizeof (OSPF_PORT_CLASS)
	},

/* _____________________________________________________________________________________________ */
	{
		(void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
		ospf_set_variable_port_and_ip_address,
		NULL,
	"OSPF Port Area ID =",
		(ULONG) offsetof (OSPF_CLASS, port[0].config.area_id),
		(ULONG) &ospf,
		sizeof (OSPF_PORT_CLASS)
	},

/* _____________________________________________________________________________________________ */
	{
		(void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
		ospf_set_variable_port_and_ushort_decimal_value,
		NULL,
	"OSPF Port Authentication Type =",
		(ULONG) offsetof (OSPF_CLASS, port[0].config.authentication_type),
		(ULONG) &ospf,
		sizeof (OSPF_PORT_CLASS)
	},

/* _____________________________________________________________________________________________ */

	{
		(void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
		ospf_parse_port_plain_text_or_md5_password,
		NULL,
	"OSPF Port Plain Text Password =",
	},
/* _____________________________________________________________________________________________ */
	{
		(void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
		ospf_parse_port_plain_text_or_md5_password,
		NULL,
	"OSPF Port MD5 Authentication Secret Key =",
	},
/* _____________________________________________________________________________________________ */
	{
		(void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
		ospf_parse_port_plain_text_or_md5_password,
		NULL,
	"OSPF Port Authentication Secret =",
	},
/* _____________________________________________________________________________________________ */
	{
		(void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
		ospf_set_variable_port_and_ulong_decimal_value,
		NULL,
	"OSPF Port Type =",
		(ULONG) offsetof (OSPF_CLASS, port[0].config.type),
		(ULONG) &ospf,
		sizeof (OSPF_PORT_CLASS)
	},

/* _____________________________________________________________________________________________ */
	{
		(void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
		ospf_set_variable_port_and_ip_address,
		NULL,
	"OSPF Port Transit Area ID =",
		(ULONG) offsetof (OSPF_CLASS, port[0].config.transit_area_id),
		(ULONG) &ospf,
		sizeof (OSPF_PORT_CLASS)
	},

/* _____________________________________________________________________________________________ */
	{
		(void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
		ospf_set_variable_port_and_ip_address,
		NULL,
	"OSPF Port Source Area ID =",
		(ULONG) offsetof (OSPF_CLASS, port[0].config.source_area_id_for_virtual_link),
		(ULONG) &ospf,
		sizeof (OSPF_PORT_CLASS)
	},

/* _____________________________________________________________________________________________ */
	{
		(void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
		ospf_set_variable_port_and_ulong_decimal_value,
		NULL,
	"OSPF Port Router Dead Interval =",
		(ULONG) offsetof (OSPF_CLASS, port[0].config.router_dead_interval),
		(ULONG) &ospf,
		sizeof (OSPF_PORT_CLASS)
	},

/* _____________________________________________________________________________________________ */
	{
		(void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
		ospf_set_variable_port_and_ulong_decimal_value,
		NULL,
	"OSPF Port Transmit Delay =",
		(ULONG) offsetof (OSPF_CLASS, port[0].config.transmit_delay),
		(ULONG) &ospf,
		sizeof (OSPF_PORT_CLASS)
	},

/* _____________________________________________________________________________________________ */
	{
		(void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
		ospf_set_variable_port_and_ushort_decimal_value,
		NULL,
	"OSPF Port Priority =",
		(ULONG) offsetof (OSPF_CLASS, port[0].config.priority),
		(ULONG) &ospf,
		sizeof (OSPF_PORT_CLASS)
	},

/* _____________________________________________________________________________________________ */
	{
		(void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
		ospf_set_variable_port_and_ushort_decimal_value,
		NULL,
	"OSPF Port Cost =",
		(ULONG) offsetof (OSPF_CLASS, port[0].config.cost),
		(ULONG) &ospf,
		sizeof (OSPF_PORT_CLASS)
	},

/* _____________________________________________________________________________________________ */
	{
		(void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
		ospf_set_variable_port_and_ulong_decimal_value,
		NULL,
	"OSPF Port Hello Interval =",
		(ULONG) offsetof (OSPF_CLASS, port[0].config.hello_interval),
		(ULONG) &ospf,
		sizeof (OSPF_PORT_CLASS)
	},

/* _____________________________________________________________________________________________ */
	{
		(void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
		ospf_set_variable_port_and_ip_address,
		NULL,
	"OSPF Port Virtual Neighbor Router ID =",  /* Will be deprecated eventually */
		(ULONG) offsetof (OSPF_CLASS, port[0].config.virtual_neighbor_rid),
		(ULONG) &ospf,
		sizeof (OSPF_PORT_CLASS)
	},
/* _____________________________________________________________________________________________ */
{
		(void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
		ospf_set_variable_port_and_ip_address,
		NULL,
	"OSPF Port Neighbor Router ID =",
		(ULONG) offsetof (OSPF_CLASS, port[0].config.virtual_neighbor_rid),
		(ULONG) &ospf,
		sizeof (OSPF_PORT_CLASS)
	},

/* _____________________________________________________________________________________________ */
{
		(void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
		ospf_set_variable_port_and_ulong_decimal_value,
		NULL,
	"OSPF Port Poll Interval =",
		(ULONG) offsetof (OSPF_CLASS, port[0].config.poll_interval),
		(ULONG) &ospf,
		sizeof (OSPF_PORT_CLASS)
	},

/* _____________________________________________________________________________________________ */
{
		(void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
		ospf_set_variable_port_and_ulong_decimal_value,
		NULL,
	"OSPF Port Retransmit Interval =",
		(ULONG) offsetof (OSPF_CLASS, port[0].config.retransmit_interval),
		(ULONG) &ospf,
		sizeof (OSPF_PORT_CLASS)
	},

/* _____________________________________________________________________________________________ */
	{
		(void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
		ospf_set_enum_enable,
		NULL,
		"OSPF Initialization Breakpoint =",
		(ULONG) offsetof (OSPF_CLASS, initialization_breakpoint),
		(ULONG) &ospf,
		(ULONG) NULL
	},

/* _____________________________________________________________________________________________ */
	{
		(void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
		ospf_set_enum_enable,
		NULL,
		"OSPF Configuration Breakpoint =",
		(ULONG) offsetof (OSPF_CLASS, configuration_breakpoint),
		(ULONG) &ospf,
		(ULONG) NULL
	},

/* _____________________________________________________________________________________________ */
	{
		(void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
		ospf_set_enum_enable,
		NULL,
		"OSPF Run Time Breakpoint =",
		(ULONG) offsetof (OSPF_CLASS, runtime_breakpoint),
		(ULONG) &ospf,
		(ULONG) NULL
	},

/* _____________________________________________________________________________________________ */
	{
		(void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
		ospf_set_enum_enable,
		NULL,
		"OSPF Printf =",
		(ULONG) offsetof (OSPF_CLASS, printing_enabled),
		(ULONG) &ospf,
		(ULONG) NULL
	},

/* _____________________________________________________________________________________________ */
	{
		(void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
		ospf_set_enum_enable,
		NULL,
		"OSPF Printf INTERFACE =",
		(ULONG) offsetof (OSPF_CLASS, interface_printing_enabled),
		(ULONG) &ospf,
		(ULONG) NULL
	},

/* _____________________________________________________________________________________________ */
	{
		(void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
		ospf_set_enum_enable,
		NULL,
		"OSPF Printf NEIGHBOR =",
		(ULONG) offsetof (OSPF_CLASS, neighbor_printing_enabled),
		(ULONG) &ospf,
		(ULONG) NULL
	},

/* _____________________________________________________________________________________________ */
	{
		(void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
		ospf_set_enum_enable,
		NULL,
		"OSPF Printf Memory =",
		(ULONG) offsetof (OSPF_CLASS, memory_printing_enabled),
		(ULONG) &ospf,
		(ULONG) NULL
	},

/* _____________________________________________________________________________________________ */
	{
		(void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
		ospf_set_enum_enable,
		NULL,
		"OSPF Printf Alarm =",
		(ULONG) offsetof (OSPF_CLASS, alarm_printing_enabled),
		(ULONG) &ospf,
		(ULONG) NULL
	},

/* _____________________________________________________________________________________________ */
	{
		(void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
		ospf_set_enum_enable,
		NULL,
		"OSPF Printf SNMP =",
		(ULONG) offsetof (OSPF_CLASS, snmp_printing_enabled),
		(ULONG) &ospf,
		(ULONG) NULL
	},

/* _____________________________________________________________________________________________ */
	{
		(void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
		ospf_set_enum_enable,
		NULL,
		"OSPF Printf Packets =",
		(ULONG) offsetof (OSPF_CLASS, packet_printing_enabled),
		(ULONG) &ospf,
		(ULONG) NULL
	},

/* _____________________________________________________________________________________________ */
	{
		(void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
		ospf_set_enum_enable,
		NULL,
		"OSPF Printf Routing Table =",
		(ULONG) offsetof (OSPF_CLASS, routing_table_printing_enabled),
		(ULONG) &ospf,
		(ULONG) NULL
	},

/* _____________________________________________________________________________________________ */
	{
		(void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
		ospf_set_enum_enable,
		NULL,
		"OSPF Printf Debug =",
		(ULONG) offsetof (OSPF_CLASS, debug_printing_enabled),
		(ULONG) &ospf,
		(ULONG) NULL
	},

/* _____________________________________________________________________________________________ */
	{
		(void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
		ospf_set_enum_enable,
		NULL,
		"OSPF Printf Rtm =",
		(ULONG) offsetof (OSPF_CLASS, rtm_printing_enabled),
		(ULONG) &ospf,
		(ULONG) NULL
	},

/* _____________________________________________________________________________________________ */
	{
		(void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
		ospf_set_enum_enable,
		NULL,
		"OSPF Redistribute BGP =",
		(ULONG) offsetof (OSPF_CLASS, ospf_redistribution_configuration.redistribute_all_bgp),
		(ULONG ) &ospf,
		sizeof (OSPF_REDISTRIBUTION_CONFIGURATION)
	},
/* _____________________________________________________________________________________________ */
	{
		(void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
		ospf_set_enum_enable,
		NULL,
		"OSPF Redistribute RIP =",
		(ULONG) offsetof (OSPF_CLASS, ospf_redistribution_configuration.redistribute_all_rip),
		(ULONG ) &ospf,
		sizeof (OSPF_REDISTRIBUTION_CONFIGURATION)
	},
/* _____________________________________________________________________________________________ */
	{
		(void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
		ospf_set_enum_enable,
		NULL,
		"OSPF Redistribute Static =",
		(ULONG) offsetof (OSPF_CLASS, ospf_redistribution_configuration.redistribute_all_static),
		(ULONG ) &ospf,
		sizeof (OSPF_REDISTRIBUTION_CONFIGURATION)
	},
/* _____________________________________________________________________________________________ */
	{
		(void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
		ospf_set_enum_enable,
		NULL,
		"OSPF Redistribute Default =",
		(ULONG) offsetof (OSPF_CLASS, ospf_redistribution_configuration.redistribute_ip_default_route),
		(ULONG ) &ospf,
		sizeof (OSPF_REDISTRIBUTION_CONFIGURATION)
	},
/* _____________________________________________________________________________________________ */
	{
		(void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
		ospf_set_enum_enable,
		NULL,
		"OSPF Allow BGP Default =",
		(ULONG) offsetof (OSPF_CLASS, ospf_redistribution_configuration.allow_bgp_default),
		(ULONG ) &ospf,
		sizeof (OSPF_REDISTRIBUTION_CONFIGURATION)
	},
/* _____________________________________________________________________________________________ */
	{
		(void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
		ospf_set_bgp_subnets_to_redistribute,
		NULL,
	"OSPF Redistribute BGP Subnets =",
	},
/* _____________________________________________________________________________________________ */
	{
		(void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
		ospf_set_static_subnets_to_redistribute,
		NULL,
	"OSPF Redistribute Static Subnets =",
	},
/* _____________________________________________________________________________________________ */
	{
		(void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
		ospf_set_rip_subnets_to_redistribute,
		NULL,
	"OSPF Redistribute RIP Subnets =",
	},
/* _____________________________________________________________________________________________ */
    {
        (void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
        ospf_set_ip_route_policy,
        NULL,
        "OSPF IP Route Policy =",
    },
/* _____________________________________________________________________________________________ */
	{
		(void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
		ospf_set_neighbor_as_not_allowed,
		NULL,
	"OSPF Do Not Allow AS Neighbor =",
	},
/* _____________________________________________________________________________________________ */
	{
		(void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
		ospf_set_origin_as_not_allowed,
		NULL,
	"OSPF Do Not Allow AS Origin =",
	},
/* _____________________________________________________________________________________________ */
	{
		(void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
		ospf_set_ulong_decimal_value,
		NULL,
	"OSPF Route Queue Process Interval =",
		(ULONG) offsetof (OSPF_CLASS, ospf_export_route_queue_process_interval),
		(ULONG) &ospf,
		(ULONG) NULL
	},
/* _____________________________________________________________________________________________ */
	{
		(void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
		ospf_set_ulong_decimal_value,
		NULL,
	"OSPF Number of Routes Dequeued Per Run =",
		(ULONG) offsetof (OSPF_CLASS, ospf_routes_dequeued_per_run),
		(ULONG) &ospf,
		(ULONG) NULL
	},

/*opaque lsa configuration jkw*/
/* _____________________________________________________________________________________________ */
	{
		(void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
		ospf_set_enum_enable,
		NULL,
		"OSPF Opaque Capability =",
		(ULONG) offsetof (OSPF_CLASS,opaque_capability),
		(ULONG ) &ospf,
		(ULONG) NULL
	},

/* RFC 2178 G.7 */
/* _____________________________________________________________________________________________ */
	{
		(void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
		ospf_set_enum_enable,
		NULL,
	"OSPF RFC1583 Compatibility =",
		(ULONG) offsetof (OSPF_CLASS,ospf_rfc1583_compatibility),
		(ULONG ) &ospf,
		(ULONG) NULL
	},


/* RFC 2178 G.6 */
/* _____________________________________________________________________________________________ */
	{
		(void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
		ospf_set_ulong_decimal_value,
		NULL,
	"OSPF Point to Point Router Lsa Option =",
		(ULONG) offsetof (OSPF_CLASS, ospf_point_to_point_router_lsa_option),
		(ULONG) &ospf,
		(ULONG)NULL
	},

/* _____________________________________________________________________________________________ */
/* RFC 1765 */
#if defined (__OSPF_DB_OVERFLOW_SUPPORT__)

	{
		(void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
		ospf_set_ulong_decimal_value,
		NULL,
		"OSPF External LSDB Limit =",
		(ULONG) offsetof (OSPF_CLASS, ospf_external_lsdb_limit),
		(ULONG) &ospf,
		(ULONG) NULL
	},
/*____________________________________________________________________________________________________*/

	{
		(void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
		ospf_set_ulong_decimal_value,
		NULL,
		"OSPF Exit Overflow Interval =",
		(ULONG) offsetof (OSPF_CLASS, ospf_exit_overflow_interval),
		(ULONG) &ospf,
		(ULONG) NULL
	},

#endif /* (__OSPF_DB_OVERFLOW_SUPPORT__)*/

/* _____________________________________________________________________________________________ */
	{
		(void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
		ospf_set_port_passive,
		NULL,
		"OSPF Port Passive-Interface ="
	},

/* _____________________________________________________________________________________________ */
	{
		(void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
		ospf_set_variable_port_and_enable,
		NULL,
	"OSPF Port Passive =",
		(ULONG) offsetof (OSPF_CLASS, port[0].config.passive),
		(ULONG) &ospf,
		sizeof (OSPF_PORT_CLASS)
	},

/*_____________________________________________________________________________________________________*/
{
		(void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
		ospf_set_enum_enable,
		NULL,
		"OSPF Printf DB Overflow =",
		(ULONG) offsetof (OSPF_CLASS, db_overflow_printing_enabled),
		(ULONG) &ospf,
		(ULONG) NULL
	},

/*_____________________________________________________________________________________________________*/
{
		(void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
		ospf_set_enum_enable,
		NULL,
		"OSPF Printf Search =",
		(ULONG) offsetof (OSPF_CLASS, search_printing_enabled),
		(ULONG) &ospf,
		(ULONG) NULL
	},

/*_____________________________________________________________________________________________________*/
{
		(void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
		ospf_set_enum_enable,
		NULL,
		"OSPF Printf Prologue =",
		(ULONG) offsetof (OSPF_CLASS, prologue_printing_enabled),
		(ULONG) &ospf,
		(ULONG) NULL
	},

/* _____________________________________________________________________________________________ */
	{
		(void (*) (char *cptr_start_of_configuration_string,ULONG parameter_1,ULONG ulptr_parameter_2,ULONG parameter_3))
		ospf_set_ulong_decimal_value,
		NULL,
	"OSPF Number of Maximum age LSAs Deleted Per Second =",
		(ULONG) offsetof (OSPF_CLASS, ospf_maxaged_lsas_removed_per_tick),
		(ULONG) &ospf,
		(ULONG) NULL
	},
/*_____________________________________________________________________________________________ */



	{
		NULL,
		NULL,
	"",
		(ULONG) NULL,
		(ULONG) NULL,
		(ULONG) NULL
	}

	}
};
#endif /* __OSPF_VIRTUAL_STACK__ */
#endif /* _OSPF_CONFIGURATION_H_ */
