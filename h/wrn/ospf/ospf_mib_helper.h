/* ospf_mib_helper.h - header file for OSPF-MIB Helper Module */

/* Copyright 1998-2003 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/* 
modification history
--------------------
03d,15dec03,agi propogated latest MIB API fixes
03c,08dec03,agi merged fixes from OSPF 2.1 patch branch
03b,01dec03,agi fixed compile issue
03a,12aug03,agi updated include file list, updated copyright
02m,28may03,kc  All defines should appear after the enumerations.
02l,09may03,kc  Fixed SPR#88389 - added ospfExtLsdbProtoId to mApiOspfExtLsdb_t.
02k,07feb03,kc  Fixed SPR#86144 - removed the mApiReqType_t argument from all the
                ospf_mApi_xxxHelperCreate() prototypes.
02j,19nov02,mwv Merge TMS code SPR 84284
02i,12nov02,kc  Added ospf_mApi_nbrHelperDeregister() prototype.
02h,14apr02,kc  Renamed EmapiOspfAuthType_simplePasswd mApiOspfAuthType_t to
                EmApiOspfAuthType_simplePasswd.
02g,11apr02,kc  Added ospf_mApi_ifHelperDelete() and ospf_mApi_areaHelperRegisterIf()
                prototypes.
02f,10apr02,kc  Added ospf_mApi_avlTreeWalk() prototype and AVL_WALK_FUNC typdedef.
02e,06apr02,kc  Added sysIfName, sysIfIndex and sysIfFlags members to mApiOspfIf_t
                structure.
02d,28mar02,kc  Added oldStatus member to mApiOspfClient_t structure. Also added
                numIfAttached, numVirtIfAttached and numHostAttached members to
                mApiOspfArea_t structure.
02c,27mar02,kc  Changed the createdAs variable type from mApiOspfRowStatus_t to
                rsAction_t.
02b,22mar02,kc  Removed ospf_mApi_initBackbone() prototype.
02a,09jan02,kc  Added host netmask define.
01y,12dec01,kc  Added defaultStubMetric to mApiOspfArea_t and defined WindNet OSPF
                specific DEFVAL_mApiOspfMetric.
01x,03dec01,kc  Redefined ospfVirtNbrTable OID instance length.
01w,29nov01,kc  For consistency, renamed OSPF_AREA_AGGREGATE_TABLE_LEN define to 
                OSPF_AREA_AGGREGATE_INSTANCE_LEN.
01v,28nov01,kc  Changed ospfLsdbAdvertisement and ospfExtLsdbAdvertisement from
                uchar_t to char.
01u,23oct01,kc  Changed ospfLsdbAge, ospfLsdbChecksum, ospfExtLsdbAge, and
                ospfExtLsdbChecksum from ushort_t to ulong_t.
01t,21oct01,kc  Added ospf_mApi_configurationGet() prototype.
01s,20oct01,kc  Added Area Aggregation related prototypes and mApiOspfClient_t.
01r,18oct01,kc  Modified arguments for ospfMapiOidCompare() prototype.
01q,14oct01,kc  Added createdAs member to mApiOspfArea_t, mApiOspfStub_t, mApiOspfIf_t,
                mApiOspfIfMetric_t, mApiOspfVirtIf_t and mApiOspfNbr_t to remove the 
                chicken and egg problem.
01p,14oct01,kc  Added ospf_mApi_nbrHelperRegister() prototype.
01o,13oct01,kc  Added defaultMtu, defaultNetmask and defaultMetric to mApiOspfHost_t.
01n,13oct01,kc  Added pointer to Type-10 Opaque LSA Area to mApiOspfArea_t.
01m,11oct01,kc  Added mApiOspfClientType_t and mApiOspfClient_t (moved from 
                ospf_mib_api.h).
01l,11oct01,kc  Changed isTransitArea in mApiOspfArea_t from BOOL to TrueValue type.
01k,10oct01,kc  Added ospf_mApi_nbrHelperSet() prototype.
01j,08oct01,kc  Added ospfMapiOidCompare() prototype.
01i,06oct01,kc  Added netmask, mtu and default metric to mApiOspfIf_t structure.
01h,05oct01,kc  Added ospf_mApi_hostHelperCreate prototype.
01g,04oct01,kc  Added ospf_mApi_xxxHelperCreate() prototypes for area, stub area,
                interface and virtual interface.
01f,18sep01,kc  Changed ospfVirtIfRetransInterval, ospfNbrPriority to ulong_t.
01e,14sep01,kc  Added default mtu, netmask and metric to mApiOspfIf_t structure.
01d,12sep01,kc  Added listIfAttached to mApiOspfArea_t structure. Also added 
                listNbrAttached to mApiOspfIf_t structure.
01c,28aug01,kc  Added prototypes for TEST/SET routines.
01b,22aug01,kc  Added prototypes for GET routines.
01a,16aug01,kc  Initial file creation.
*/

/*
DESCRIPTION:

This file defines the RFC1850 MIB specific Manamgenet Interface Local ID enumerations, 
the MIB object enumerations, the MIB object sizes and some MIB API helper function
prototypes.

*/
 
#ifndef __INCospf_mib_helperh
#define __INCospf_mib_helperh

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include <net/if.h>

/****************************************************************************************
* Management Interface 1-based local ID enumerations for RFC1850 MIB.
*/
typedef enum {
    mApiOspfRouterId = 1,
    mApiOspfAdminStat,
    mApiOspfVersionNumber,         
    mApiOspfAreaBdrRtrStatus,     
    mApiOspfASBdrRtrStatus,
    mApiOspfExternLsaCount,         
    mApiOspfExternLsaCksumSum,       
    mApiOspfTOSSupport,
    mApiOspfOriginateNewLsas,       
    mApiOspfRxNewLsas, 
    mApiOspfExtLsdbLimit,
    mApiOspfMulticastExtensions,
    mApiOspfExitOverflowInterval,
    mApiOspfDemandExtensions,
    mApiOspfAreaId,                        /* mApiOspf_t enumeration value = 15 */
    mApiOspfAuthType,
    mApiOspfImportAsExtern,
    mApiOspfSpfRuns,
    mApiOspfAreaBdrRtrCount,
    mApiOspfAsBdrRtrCount,
    mApiOspfAreaLsaCount,
    mApiOspfAreaLsaCksumSum,
    mApiOspfAreaSummary,
    mApiOspfAreaStatus,
    mApiOspfStubAreaId,                    /* mApiOspf_t enumeration value = 25 */
    mApiOspfStubTOS,
    mApiOspfStubMetric,
    mApiOspfStubStatus,
    mApiOspfStubMetricType,
    mApiOspfLsdbAreaId,                    /* mApiOspf_t enumeration value = 30 */
    mApiOspfLsdbType,
    mApiOspfLsdbLsid,
    mApiOspfLsdbRouterId,
    mApiOspfLsdbSequence,
    mApiOspfLsdbAge,
    mApiOspfLsdbChecksum,
    mApiOspfLsdbAdvertisement,
    mApiOspfAreaRangeAreaId,               /* mApiOspf_t enumeration value = 38 */
    mApiOspfAreaRangeNet,
    mApiOspfAreaRangeMask,
    mApiOspfAreaRangeStatus,
    mApiOspfAreaRangeEffect,
    mApiOspfHostIpAddress,                 /* mApiOspf_t enumeration value = 43 */
    mApiOspfHostTOS,
    mApiOspfHostMetric,
    mApiOspfHostStatus,
    mApiOspfHostAreaID,
    mApiOspfIfIpAddress,                   /* mApiOspf_t enumeration value = 48 */
    mApiOspfAddressLessIf,
    mApiOspfIfAreaId,
    mApiOspfIfType,
    mApiOspfIfAdminStat,
    mApiOspfIfRtrPriority,
    mApiOspfIfTransitDelay,
    mApiOspfIfRetransInterval,
    mApiOspfIfHelloInterval,
    mApiOspfIfRtrDeadInterval,
    mApiOspfIfPollInterval,
    mApiOspfIfState,
    mApiOspfIfDesignatedRouter,
    mApiOspfIfBackupDesignatedRouter,
    mApiOspfIfEvents,
    mApiOspfIfAuthKey,
    mApiOspfIfStatus,
    mApiOspfIfMulticastForwarding,
    mApiOspfIfDemand,
    mApiOspfIfAuthType,
    mApiOspfIfMetricIpAddress,             /* mApiOspf_t enumeration value =68 */
    mApiOspfIfMetricAddressLessIf,
    mApiOspfIfMetricTOS,
    mApiOspfIfMetricValue,
    mApiOspfIfMetricStatus,
    mApiOspfVirtIfAreaId,                  /* mApiOspf_t enumeration value = 73 */
    mApiOspfVirtIfNeighbor,
    mApiOspfVirtIfTransitDelay,
    mApiOspfVirtIfRetransInterval,
    mApiOspfVirtIfHelloInterval,
    mApiOspfVirtIfRtrDeadInterval,
    mApiOspfVirtIfState,
    mApiOspfVirtIfEvents,
    mApiOspfVirtIfAuthKey,
    mApiOspfVirtIfStatus,
    mApiOspfVirtIfAuthType,
    mApiOspfNbrIpAddr,                     /* mApiOspf_t enumeration value = 84 */
    mApiOspfNbrAddressLessIndex,
    mApiOspfNbrRtrId,
    mApiOspfNbrOptions,
    mApiOspfNbrPriority,
    mApiOspfNbrState,
    mApiOspfNbrEvents,
    mApiOspfNbrLsRetransQLen,
    mApiOspfNbmaNbrStatus,
    mApiOspfNbmaNbrPermanence,
    mApiOspfNbrHelloSuppressed,
    mApiOspfVirtNbrArea,                   /* mApiOspf_t enumeration value = 95 */
    mApiOspfVirtNbrRtrId,
    mApiOspfVirtNbrIpAddr,
    mApiOspfVirtNbrOptions,
    mApiOspfVirtNbrState,
    mApiOspfVirtNbrEvents,
    mApiOspfVirtNbrLsRetransQLen,
    mApiOspfVirtNbrHelloSuppressed,
    mApiOspfExtLsdbType,                   /* mApiOspf_t enumeration value = 103 */
    mApiOspfExtLsdbLsid,
    mApiOspfExtLsdbRouterId,
    mApiOspfExtLsdbSequence,
    mApiOspfExtLsdbAge,
    mApiOspfExtLsdbChecksum,
    mApiOspfExtLsdbAdvertisement,
    mApiOspfAreaAggregateAreaID,           /* mApiOspf_t enumeration value = 110 */
    mApiOspfAreaAggregateLsdbType,
    mApiOspfAreaAggregateNet,
    mApiOspfAreaAggregateMask,
    mApiOspfAreaAggregateStatus,
    mApiOspfAreaAggregateEffect,
    mApiRfc1850MaxLocalIds  /* 115 - Number of objects in rfc1850 */
} mApiOspf_t;

/****************************************************************************************
* Manamgenet Interface RFC1850 MIB object enumerations
*/
typedef enum {
    EmApiOspf_true = 1,
    EmApiOspf_false = 2
} mApiOspfTrueValue_t;

typedef enum {
    EmApiOspf_enabled = 1,
    EmApiOspf_disabled = 2
} mApiOspfStatus_t;

/* Row Status enumerations. This is the same for all tables with the rowStatus object */
typedef enum {
    EmApiOspfRowStatus_active = 1,
    EmApiOspfRowStatus_notInService = 2,
    EmApiOspfRowStatus_notReady = 3,
    EmApiOspfRowStatus_createAndGo = 4,
    EmApiOspfRowStatus_createAndWait = 5,
    EmApiOspfRowStatus_destroy = 6
} mApiOspfRowStatus_t;

typedef enum {
    EmApiOspfImportAsExtern_importExternal = 1,
    EmApiOspfImportAsExtern_importNoExternal = 2,
    EmApiOspfImportAsExtern_importNssa = 3
} mApiOspfAreaImportAsType_t;

typedef enum {
    EmApiOspfAreaSummary_noAreaSummary = 1,
    EmApiOspfAreaSummary_sendAreaSummary = 2
} mApiOspfAreaSummary_t;

typedef enum {
    EmApiOspfStubMetricType_mApiOspfMetric = 1,
    EmApiOspfStubMetricType_comparableCost = 2,
    EmApiOspfStubMetricType_nonComparable = 3
} mApiOspfStubMetricType_t;

typedef enum {
    EmApiOspfIfState_down = 1,
    EmApiOspfIfState_loopback = 2,
    EmApiOspfIfState_waiting = 3,
    EmApiOspfIfState_pointToPoint = 4,
    EmApiOspfIfState_designatedRouter = 5,
    EmApiOspfIfState_backupDesignatedRouter = 6,
    EmApiOspfIfState_otherDesignatedRouter = 7
} mApiOspfIfState_t;

typedef enum {
    EmApiOspfIfType_broadcast = 1,
    EmApiOspfIfType_nbma = 2,
    EmApiOspfIfType_pointToPoint = 3,
    EmApiOspfIfType_pointToMultipoint = 5
} mApiOspfIfType_t;

typedef enum {
    EmApiOspfIfMulticastForwarding_blocked = 1,
    EmApiOspfIfMulticastForwarding_multicast = 2,
    EmApiOspfIfMulticastForwarding_unicast = 3
} mApiOspfIfMcastType_t;

typedef enum {
    EmApiOspfVirtIfState_down = 1,
    EmApiOspfVirtIfState_pointToPoint = 4
} mApiOspfVirtIfState_t;

typedef enum {
    EmApiOspfLsdbType_routerLink = 1,
    EmApiOspfLsdbType_networkLink = 2,
    EmApiOspfLsdbType_summaryLink = 3,
    EmApiOspfLsdbType_asSummaryLink = 4,
    EmApiOspfLsdbType_asExternalLink = 5,
    EmApiOspfLsdbType_multicastLink = 6,
    EmApiOspfLsdbType_nssaExternalLink = 7,
    EmApiOspfLsdbType_type9 = 9,
    EmApiOspfLsdbType_type10 = 10,
    EmApiOspfLsdbType_type11 = 11
} mApiOspfLsdbType_t;

typedef enum {
    EmApiOspfExtLsdbType_asExternalLink = 5
} mApiOspfExtLsdbType_t;

typedef enum {
    EmApiOspfNbrState_down = 1,
    EmApiOspfNbrState_attempt = 2,
    EmApiOspfNbrState_init = 3,
    EmApiOspfNbrState_twoWay = 4,
    EmApiOspfNbrState_exchangeStart = 5,
    EmApiOspfNbrState_exchange = 6,
    EmApiOspfNbrState_loading = 7,
    EmApiOspfNbrState_full = 8
} mApiOspfNbrState_t;

typedef enum {
    EmApiOspfNbmaNbrPermanence_dynamic = 1,
    EmApiOspfNbmaNbrPermanence_permanent = 2
} mApiOspfNbmaNbrPermanence_t;

/* supported Authentication Type */
typedef enum {
    EmApiOspfAuthType_none = 0,
    EmApiOspfAuthType_simplePasswd = 1,
    EmApiOspfAuthType_md5 = 2
} mApiOspfAuthType_t;

typedef enum {
    EmApiOspfAggregateLsdbType_summaryLink = 3,
    EmApiOspfAggregateLsdbType_nssaExternalLink = 7
} mApiOspfAggregateLsdbType_t;

typedef enum {
    EmApiOspfAggregateEffect_advertiseMatching = 1,
    EmApiOspfAggregateEffect_doNotAdvertiseMatching = 2
} mApiOspfAggregateEffect_t;


/* mApiOspfClientType_t enumeration type is used to identify the type of client
 * that attaches to the mApiOspfArea_t or mApiOspfIf_t. It is used when
 * creating an mApiOspfClient_t node
 */
typedef enum {
    EmApiOspfClientType_intf = 1,   /* client is physical interface */
    EmApiOspfClientType_vintf = 2,  /* client is virtual interface */
    EmApiOspfClientType_host = 3,   /* client is host interface */
    EmApiOspfClientType_nbr = 4,    /* client is neighbor */
    EmApiOspfClientType_ag          /* client is area aggregation */

} mApiOspfClientType_t;


/***************************************************************************************
 * Management Interface RFC1850 MIB Definitions
 */

/* OSPF Version Number */
#define OSPF_VERSION             2

/* OSPF Area Backbone, ospfAreaId = 0.0.0.0 */
#define OSPF_BACKBONE_AREA       00000000L

/* NOTE: This is an internal define for Virtual Interface, shall not be used by 
 *       application utilizing the MIB API
 */
#define OSPF_VIRTUAL_IF_TYPE      100

 
#define MIN_OSPF_EXT_LSDB_LIMIT               -1L        
#define MAX_OSPF_EXT_LSDB_LIMIT               2147483647L
#define MAX_OSPF_EXIT_OVERFLOW_INTERVAL       2147483647L
#define MAX_OSPF_STUB_METRIC                  16777215L
#define MIN_OSPF_LSDB_SIZE                    1L
#define MAX_OSPF_LSDB_SIZE                    65535L
#define MAX_OSPF_PRIORITY                     255L
#define MAX_OSPF_POLL_INTERVAL                2147483647L
#define MAX_OSPF_AGE                          3600L
#define MIN_OSPF_HELLO_INTERVAL               1L           
#define MAX_OSPF_HELLO_INTERVAL               65535L       
#define MAX_OSPF_DEAD_INTERVAL                2147483647L 
#define MAX_OSPF_AUTH_KEY                     256L         
#define MAX_OSPF_METRIC_VALUE                 65535L
#define MAX_EXTERNAL_LSDB_SIZE                36L

#define OSPF_AUTH_SIMPLE_SIZE                  8
#define OSPF_AUTH_MD5_SIZE                     16

/* the following defines provides the number of sub-identifier (the instance length)
 * of each table in RFC1850 MIB  
 */
#define OSPF_AREA_INSTANCE_LEN                  4
#define OSPF_STUB_INSTANCE_LEN                  5
#define OSPF_LSDB_INSTANCE_LEN                  13
#define OSPF_HOST_INSTANCE_LEN                  5
#define OSPF_INTF_INSTANCE_LEN                  5
#define OSPF_METRIC_INSTANCE_LEN                6
#define OSPF_VIRT_INTF_INSTANCE_LEN             8
#define OSPF_NBR_INSTANCE_LEN                   5
#define OSPF_VIRT_NBR_INSTANCE_LEN              8
#define OSPF_EXT_LSDB_INSTANCE_LEN              9
#define OSPF_AREA_AGGREGATE_INSTANCE_LEN        13

/* RFC1850 MIB default value */
#define DEFVAL_mApiOspfExtLsdbLimit            -1L
#define DEFVAL_mApiOspfExitOverflowInterval    0

#define DEFVAL_mApiOspfIfRtrPriority           1
#define DEFVAL_mApiOspfIfTransitDelay          1
#define DEFVAL_mApiOspfIfRetransInterval       5
#define DEFVAL_mApiOspfIfHelloInterval         10
#define DEFVAL_mApiOspfIfRtrDeadInterval       40
#define DEFVAL_mApiOspfIfPollInterval          120

#define DEFVAL_mApiOspfVirtIfTransitDelay      1
#define DEFVAL_mApiOspfVirtIfRetransInterval   5
#define DEFVAL_mApiOspfVirtIfHelloInterval     10
#define DEFVAL_mApiOspfVirtIfRtrDeadInterval   60
            
#define DEFVAL_mApiHostNetMask                 0xFFFFFFFF

/* WindNet OSPF default value */
#define DEFVAL_mApiOspfIfType                 EmApiOspfIfType_broadcast
#define DEFVAL_mApiOspfMetric                  1


/****************************************************************************************
 * Mamagement Interface RFC1850 Data Structure.
 */

/* mApiOspfClient_t is used by mApiOspfArea_t to track instances of interface, 
 * virtual interface and host interface that are attached to this area. It is also
 * used by mApiOspfIf_t to track instances of neighbor that are associated
 * with the interface. The <clientOldState> mApiOspfRowStatus_t enumeration type is
 * only used by mApiOspfArea_t to remember the existing rowStatus of the clients
 * before it forces the client to make the rowStatus state transition due to the
 * rowStatus changes occur in the mApiOspfArea_t itself
 */
typedef struct mApiOspfClient
{
    NODE                 node;          /* linked list node */
    void                 *pClient;      /* pointer to client data structure */
    int                  oldStatus;     /* previous row stauts of the attached client */
    mApiOspfClientType_t clientType;    /* the type of client */
} mApiOspfClient_t;


/* OSPF General Group variables
 * NOTE: 
 * - RFC2178 only supports TOS 0. Thus the ospfTOSSupport scalar object is implemented
 *  as read-only and its value is always set to TRUE
 * - ospfExitOverflowInterval and ospfExtLsdbLimit scalar objects are implemented as
 *   read-write if and only if the __DBASE_OVERFLOW_SUPPORT__ compiler preprocessor
 *   is turned on. Otherwise, these scalars will be implemented as read-only
 */
typedef struct mApiOspfGenGroup
{
    ulong_t               ospfRouterId;             /* read-write */
    mApiOspfStatus_t      ospfAdminStat;            /* read-write */
    ulong_t               ospfVersionNumber;        /* read-only */
    mApiOspfTrueValue_t   ospfAreaBdrRtrStatus;     /* read-only */ 
    mApiOspfTrueValue_t   ospfASBdrRtrStatus;       /* read-write */
    ulong_t               ospfExternLsaCount;       /* read-only */
    ulong_t               ospfExternLsaCksumSum;    /* read-only */
    mApiOspfTrueValue_t   ospfTOSSupport;      /* read-write, implemented as read-only */
    ulong_t               ospfOriginateNewLsas;     /* read-only */
    ulong_t               ospfRxNewLsas;            /* read-only */
    long                  ospfExtLsdbLimit;         /* read-write */
    ulong_t               ospfExitOverflowInterval; /* read-write */

    /* the following read-write objects are implemented as read-only */
    ulong_t           ospfMulticastExtensions;
    mApiOspfTrueValue_t  ospfDemandExtensions;
} mApiOspfGenGroup_t;

/* an instance of mApiOspfAreaClass_t object will always be created each time a new
 * OSPF Area is added to the Management Database.
 */
typedef struct mApiOspfArea
{
    AVL_NODE      node;               /* AVL Tree node */
    void          *pSpfStub;          /* pointer to the stub associate with this area */
    LIST          listIfAttached;     /* If clients attached, unsorted */
    LIST          listAreaAggregate;  /* Area Aggregate instances */

    ulong_t       defaultStubMetric;  /* default stub metric */
    ushort_t      numActiveIf;        /* number of active interface attached */
    ushort_t      numActiveVirtIf;    /* number of active virtual interface attached */
    ushort_t      numPassiveIf;       /* numbe of passive interfaces */
    ushort_t      numActiveHost;      /* number of active host interface attached */
    ushort_t      numIfAttached;      /* number of interface instances attached */
    ushort_t      numVirtIfAttached;  /* number of virtual interface instances attached */
    ushort_t      numHostAttached;    /* number of host interface instances attached */
    ushort_t      numAgAttached;      /* number of area aggregate instances attached */
    rsAction_t    createdAs;          /* the method this row is created */
    mApiOspfTrueValue_t  isTransitArea;      /* TRUE if this is a transit area */
    void                 *pOpaqueArea;       /* Type-10 Opaque LSA Area */
    
    /* objects associate with the ospfAreaTable */
    /* NOTE: ospfAuthType has been obsolete, thus not implemented */
    ulong_t                     ospfAreaId;            /* index object, read-only */
    ulong_t                     ospfSpfRuns;           /* read-only */
    ulong_t                     ospfAreaBdrRtrCount;   /* read-only */
    ulong_t                     ospfAsBdrRtrCount;     /* read-only */
    ulong_t                     ospfAreaLsaCount;      /* read-only */
    ulong_t                     ospfAreaLsaCksumSum;   /* read-only */
    mApiOspfAreaImportAsType_t  ospfImportAsExtern;    /* read-create */
    mApiOspfAreaSummary_t       ospfAreaSummary;       /* read-create */
    mApiOspfRowStatus_t         ospfAreaStatus;        /* read-create */
} mApiOspfArea_t;

/* objects associate with the ospfStubTable */
typedef struct mApiOspfStub
{
    AVL_NODE               node;       /* AVL Tree node */
    void                   *pArea;     /* pointer to area associated with this stub */
    rsAction_t             createdAs;  /* the method this row is created */

    ulong_t                  ospfStubAreaId;      /* index object, read-only */
    ulong_t                  ospfStubTOS;         /* index object, read-only */
    ulong_t                  ospfStubMetric;      /* read-create */
    mApiOspfRowStatus_t      ospfStubStatus;      /* read-create */
    mApiOspfStubMetricType_t ospfStubMetricType;  /* read-create */
} mApiOspfStub_t;

/* objects associate with the ospfLsdbTable */
typedef struct mApiOspfLsdb
{
    AVL_NODE            node;                     /* AVL Tree node */
    ulong_t             ospfLsdbAreaId;           /* index object, read-only */
    mApiOspfLsdbType_t  ospfLsdbType;             /* index object, read-only */
    ulong_t             ospfLsdbLsid;             /* index object, read-only */
    ulong_t             ospfLsdbRouterId;         /* index object, read-only */
    long                ospfLsdbSequence;         /* read-only */
    ulong_t             ospfLsdbAge;              /* read-only */
    ulong_t             ospfLsdbChecksum;         /* read-only */
    char                *ospfLsdbAdvertisement;   /* read-only */
    ushort_t            lsdbLen;           /* advertisement length, not a MIB object */
} mApiOspfLsdb_t;

/* objects associate with the ospfHostTable */
typedef struct mApiOspfHost
{
    AVL_NODE             node;            /* AVL Tree node */
    ulong_t              defaultMtu;      /* default mtu from in_ifaddr structure */
    ulong_t              defaultNetmask;  /* default netmask from in_ifaddr structure */
    ulong_t              defaultMetric;   /* default metric from in_ifaddr structure */
    rsAction_t           createdAs;       /* the method this row is created */
    void                 *pArea;          /* area associated with this interface */

    ulong_t              ospfHostIpAddress;     /* index object, read-only */
    ulong_t              ospfHostTOS;           /* index object, read-only */
    ulong_t              ospfHostMetric;        /* read-create */
    mApiOspfRowStatus_t  ospfHostStatus;        /* read-create */
    ulong_t              ospfHostAreaId;        /* read-only */
} mApiOspfHost_t;

/* objects associate with the ospfIfMetricTable */
typedef struct mApiOspfIfMetric
{
    AVL_NODE             node;            /* AVL Tree node */
    void                 *pIf;            /* interface associated with this metric */
    rsAction_t           createdAs;       /* the method this row is created */

    ulong_t              ospfIfMetricIpAddress;        /* index object, read-only */
    ulong_t              ospfIfMetricAddressLessIf;    /* index object, read-only */
    ulong_t              ospfIfMetricTOS;              /* index object, read-only */
    ulong_t              ospfIfMetricValue;            /* read-create */
    mApiOspfRowStatus_t  ospfIfMetricStatus;           /* read-create */
} mApiOspfIfMetric_t;

typedef struct mApiOspfCryptKey
{
    uchar_t       authKeyId;
    uchar_t       authKey[OSPF_AUTH_MD5_SIZE+1];
    uchar_t       authLen;  /* md5 authKey length, not including the key id */
} mApiOspfCryptKey_t;


/* objects associate with the ospfIfTable */
typedef struct mApiOspfIf
{
    AVL_NODE      node;              /* AVL Tree node */
    LIST          listNbrAttached;   /* neighbors attached to this interface, unsorted */
    ushort_t      numNbrAttached;    /* number of neighbors attached to this interface */
    ulong_t       defaultMtu;        /* default mtu from in_ifaddr structure */
    ulong_t       defaultNetmask;    /* default netmask from in_ifaddr structure */
    ulong_t       defaultMetric;     /* default metric from in_ifaddr structure */
    rsAction_t    createdAs;         /* the method this row is created */

    void          *pIfMetric;        /* metric associate with this interface */
    void          *pArea;            /* pointer to area this interface connects */
    char          sysIfName[IFNAMSIZ];  /* interface name, e.g. `en'' or ``lo'' */
    ushort_t      sysIfIndex;        /* numeric abbreviation for this interface */
    int           sysIfFlags;        /* interface flags */

    ulong_t              ospfIfIpAddress;               /* index object, read-only */
    ulong_t              ospfAddressLessIf;             /* index object, read-only */
    ulong_t              ospfIfAreaId;                  /* read-create */
    mApiOspfIfType_t     ospfIfType;                    /* read-create */
    mApiOspfStatus_t     ospfIfAdminStat;               /* read-create */
    ulong_t              ospfIfRtrPriority;             /* read-create */
    ulong_t              ospfIfTransitDelay;            /* read-create */
    ulong_t              ospfIfRetransInterval;         /* read-create */
    ulong_t              ospfIfHelloInterval;           /* read-create */
    ulong_t              ospfIfRtrDeadInterval;         /* read-create */
    ulong_t              ospfIfPollInterval;            /* read-create */
    mApiOspfIfState_t    ospfIfState;                   /* read-only */
    ulong_t              ospfIfDesignatedRouter;        /* read-only */
    ulong_t              ospfIfBackupDesignatedRouter;  /* read-only */
    ulong_t              ospfIfEvents;                  /* read-only */
    mApiOspfAuthType_t   ospfIfAuthType;                /* read-create */
    mApiOspfRowStatus_t  ospfIfStatus;                  /* read-create */
    mApiOspfTrueValue_t  wrnOspfLocalIfPassive;

    /* read-create ospfIfAuthKey */
    uchar_t              ospfIfSimpleAuthKey[OSPF_AUTH_SIMPLE_SIZE +1]; 
    mApiOspfCryptKey_t   ospfIfMd5AuthKey;
    ulong_t              authKeyLen;  /* simple authKey length, not a MIB object */

    /* the following read-create objects are implemented as read-only */
    mApiOspfIfMcastType_t  ospfIfMulticastForwarding;
    mApiOspfTrueValue_t    ospfIfDemand;      
} mApiOspfIf_t;

/* objects associate with the ospfVirtIfTable */
typedef struct mApiOspfVirtIf
{
    AVL_NODE               node;                 /* AVL Tree node */
    void                   *pArea;               /* pointer to transit area */
    rsAction_t             createdAs;            /* the method this row is created */

    ulong_t                ospfVirtIfAreaId;            /* index object, read-only */
    ulong_t                ospfVirtIfNeighbor;          /* index object, read-only */
    ulong_t                ospfVirtIfTransitDelay;      /* read-create */
    ulong_t                ospfVirtIfRetransInterval;   /* read-create */
    ulong_t                ospfVirtIfHelloInterval;     /* read-create */
    ulong_t                ospfVirtIfRtrDeadInterval;   /* read-create */
    mApiOspfVirtIfState_t  ospfVirtIfState;             /* read-only */
    ulong_t                ospfVirtIfEvents;            /* read-only */
    mApiOspfAuthType_t     ospfVirtIfAuthType;          /* read-create */
    mApiOspfRowStatus_t    ospfVirtIfStatus;            /* read-create */

    /* read-create ospfVirtIfAuthKey */
    uchar_t                ospfVirtIfSimpleAuthKey[OSPF_AUTH_SIMPLE_SIZE+1]; 
    mApiOspfCryptKey_t     ospfVirtIfMd5AuthKey;
    ulong_t                authKeyLen;  /* simple authKey length, not a MIB object */
} mApiOspfVirtIf_t;


/* objects associate with the ospfNbrTable */
typedef struct mApiOspfNbr
{
    AVL_NODE               node;            /* AVL Tree node */
    void                   *pIf;            /* interface associated with */
    rsAction_t             createdAs;       /* the method this row is created */

    ulong_t                ospfNbrIpAddr;            /* index object, read-only */
    ulong_t                ospfNbrAddressLessIndex;  /* index object, read-only */
    ulong_t                ospfNbrRtrId;             /* read-only */
    ulong_t                ospfNbrOptions;           /* read-only */
    ulong_t                ospfNbrPriority;          /* read-create */
    mApiOspfNbrState_t     ospfNbrState;             /* read-only */
    ulong_t                ospfNbrEvents;            /* read-only */
    ulong_t                ospfNbrLsRetransQLen;     /* read-only */
    mApiOspfRowStatus_t    ospfNbmaNbrStatus;        /* read-create */
    mApiOspfNbmaNbrPermanence_t  ospfNbmaNbrPermanence;    /* read-only */
    mApiOspfTrueValue_t     ospfNbrHelloSuppressed;   /* read-only */
} mApiOspfNbr_t;

/* objects associate with the ospfVirtNbrTable */
typedef struct mApiOspfVirtNbr
{
    AVL_NODE             node;                         /* AVL Tree node */
    ulong_t              ospfVirtNbrArea;              /* index object, read-only */
    ulong_t              ospfVirtNbrRtrId;             /* index object, read-only */
    ulong_t              ospfVirtNbrIpAddr;            /* read-only */
    ulong_t              ospfVirtNbrOptions;           /* read-only */
    mApiOspfNbrState_t   ospfVirtNbrState;             /* read-only */
    ulong_t              ospfVirtNbrEvents;            /* read-only */
    ulong_t              ospfVirtNbrLsRetransQLen;     /* read-only */
    mApiOspfTrueValue_t  ospfVirtNbrHelloSuppressed;   /* read-only */
} mApiOspfVirtNbr_t;

/* objects associate with the ospfExtLsdbTable */
typedef struct mApiOspfExtLsdb
{
    AVL_NODE               node;                        /* AVL Tree node */
    mApiOspfExtLsdbType_t  ospfExtLsdbType;             /* index object, read-only */
    ulong_t                ospfExtLsdbLsid;             /* index object, read-only */
    ulong_t                ospfExtLsdbRouterId;         /* index object, read-only */
    ulong_t                ospfExtLsdbSequence;         /* read-only */
    ulong_t                ospfExtLsdbAge;              /* read-only */
    ulong_t                ospfExtLsdbChecksum;         /* read-only */
    ulong_t                ospfExtLsdbProtoId;          /* route protocol ID, interanl use */
    char                   ospfExtLsdbAdvertisement[MAX_EXTERNAL_LSDB_SIZE];   /* read-only */
    ushort_t               lsdbLen;   /* length of ospfExtLsdbAdvertisement */
} mApiOspfExtLsdb_t;

/* objects associate with the ospfAreaAggregateTable */
typedef struct mApiOspfAreaAggregate
{
    AVL_NODE                     node;            /* AVL Tree node */
    void                         *pArea;          /* area associated with */
    rsAction_t                   createdAs;       /* the method this row is created */

    ulong_t                      ospfAreaAggregateAreaID;   /* index object, read-only */
    mApiOspfAggregateLsdbType_t  ospfAreaAggregateLsdbType; /* index object, read-only */
    ulong_t                      ospfAreaAggregateNet;      /* index object, read-only */
    ulong_t                      ospfAreaAggregateMask;     /* index object, read-only */
    mApiOspfRowStatus_t          ospfAreaAggregateStatus;   /* read-create */
    mApiOspfAggregateEffect_t    ospfAreaAggregateEffect;   /* read-create */
} mApiOspfAreaAggregate_t;


/* function pointer requires by ospf_mApi_avlTreeWalk() routine */
typedef STATUS (* AVL_WALK_FUNC)(void *pNode, void *arg);

/***************************************************************************************
 * Public Function Prototypes for Management Interface
 */

/* ospfGeneralGroup helper routines */
IMPORT STATUS ospf_mApi_globalParmGet(mApiOspfGenGroup_t *thisGenGroup, 
                                       mApiRequest_t *pRequest, mApiObject_t *pObject);
IMPORT STATUS ospf_mApi_globalParmSet(mApiRequest_t *pRequest, mApiObject_t *pObject,
                                       mApiReqType_t reqType);

/* ospfAreaTable helper routines */
IMPORT STATUS ospf_mApi_areaHelperGet( void *pRow, mApiRequest_t *pRequest,
                                       mApiObject_t *pObject );
IMPORT STATUS ospf_mApi_areaHelperSet(mApiRequest_t *pRequest, rsRequest_t rsReqType );
IMPORT STATUS ospf_mApi_areaHelperRegisterIf( mApiOspfIf_t *pIf, int oldStatus );

/* ospfStubTable helper routines */
IMPORT STATUS ospf_mApi_stubHelperGet( void *pRow, mApiRequest_t *pRequest,
                                       mApiObject_t *pObject );
IMPORT STATUS ospf_mApi_stubHelperSet( mApiRequest_t *pRequest, rsRequest_t rsReqType );

/* ospfLasbTable helper routine */
IMPORT STATUS ospf_mApi_lsdbHelperGet( void *pRow, mApiRequest_t *pRequest,
                                       mApiObject_t *pObject );

/* ospfHostTable helper functions */
IMPORT STATUS ospf_mApi_hostHelperGet( void *pRow, mApiRequest_t *pRequest,
                                       mApiObject_t *pObject );
IMPORT STATUS ospf_mApi_hostHelperSet( mApiRequest_t *pRequest, rsRequest_t rsReqType );

/* ospfIfTable helper routines */
IMPORT STATUS ospf_mApi_ifHelperGet( void *pRow, mApiRequest_t *pRequest,
                                     mApiObject_t *pObject );
IMPORT STATUS ospf_mApi_ifHelperSet(mApiRequest_t *pRequest, rsRequest_t rsReqType );
IMPORT void ospf_mApi_ifHelperDelete( ulong_t ospfIfIpAddr, ulong_t ospfAddrLessIf );

/* ospfIfMetricTable helper functions */
IMPORT STATUS ospf_mApi_ifMetricHelperGet( void *pRow, mApiRequest_t *pRequest,
                                           mApiObject_t *pObject );
IMPORT STATUS ospf_mApi_ifMetricHelperSet( mApiRequest_t *pRequest, rsRequest_t rsReqType );

/* ospfVirtIfTable helper routines */
IMPORT STATUS ospf_mApi_virtIfHelperGet( void *pRow, mApiRequest_t *pRequest,
                                         mApiObject_t *pObject );
IMPORT STATUS ospf_mApi_virtIfHelperSet( mApiRequest_t *pRequest, rsRequest_t rsReqType );

/* ospfNbrTable helper routines */
IMPORT STATUS ospf_mApi_nbrHelperGet( void *pRow, mApiRequest_t *pRequest,
                                      mApiObject_t *pObject );
IMPORT STATUS ospf_mApi_nbrHelperSet( mApiRequest_t *pRequest, rsRequest_t rsReqType );
IMPORT STATUS ospf_mApi_nbrHelperRegister( mApiOspfNbr_t *pNbr, ulong_t ifIpAddress, 
                                           ulong_t ifIndex );
IMPORT void ospf_mApi_nbrHelperDeregister( mApiOspfNbr_t *pNbr, ulong_t ifIpAddress,
                                           ulong_t ifIndex );

/* ospfVirtNbrTable helper routines */
IMPORT STATUS ospf_mApi_virtNbrHelperGet( void *pRow, mApiRequest_t *pRequest,
                                          mApiObject_t *pObject );

/* ospfExtLsdbTable helper routines */
IMPORT STATUS ospf_mApi_extLsdbHelperGet(void *pRow, mApiRequest_t *pRequest, 
                                         mApiObject_t *pObject);

/* ospfAreaAggregateTable helper routines */
IMPORT STATUS ospf_mApi_areaAggregateHelperGet( void *pRow, mApiRequest_t *pRequest,
                                                mApiObject_t *pObject );
IMPORT STATUS ospf_mApi_areaAggregateHelperSet( mApiRequest_t *pRequest, 
                                                rsRequest_t rsReqType );

/* common AVL compare routine */
IMPORT int ospfMapiOidCompare( int oidLength, ulong_t *pOid1, ulong_t *pOid2 );

/* misc AVL routines */
IMPORT STATUS ospf_mApi_avlTreeWalk( AVL_TREE * pRoot, AVL_WALK_FUNC ospfMapiTreeWalk,
                                     void *arg );

/* rfc1850-specific helper initialization routine */
IMPORT STATUS ospf_mApi_configurationGet( void );
IMPORT void ospf_mApi_reset( void );
IMPORT void ospf_mApi_initGeneralGroup( BOOL resetAllToDefault );
IMPORT STATUS ospf_mApi_initRsLib(void);
IMPORT STATUS ospf_mApi_initAvlTree(void);

/***************************************************************************************
 * Function Prototypes for Management Interface for Public Routines that will
 * eventually be obsoleted. Third party applications that needs to integrate to 
 * MIB API must not use the following routines.
 */
IMPORT void ospf_mApi_areaHelperCreate( mApiOspfArea_t *pStaticArea );
IMPORT void ospf_mApi_stubHelperCreate( mApiOspfStub_t *pStaticStub );
IMPORT void ospf_mApi_hostHelperCreate( mApiOspfHost_t *pStaticHost );
IMPORT void ospf_mApi_ifHelperCreate( mApiOspfIf_t *pStaticIf );
IMPORT void ospf_mApi_ifMetricHelperCreate( mApiOspfIfMetric_t *pStaticIfm );
IMPORT void ospf_mApi_virtIfHelperCreate( mApiOspfVirtIf_t *pStaticVirtIf ); 
IMPORT void ospf_mApi_areaAggregateHelperCreate( mApiOspfAreaAggregate_t *pStaticAreaAg );

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __INCospf_mib_helperh */
