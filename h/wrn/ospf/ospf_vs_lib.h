/* vsOspfLib.h - virtual stack routines */

/* Copyright 2000 Wind River Systems, Inc. */

/*
modification history
--------------------
02,17sep01,kc   Added ospfConfigurationTableInit prototype.
01,07mar01,jkw  written

*/

#ifndef __INCospf_vs_libh
#define __INCospf_vs_libh

#include "vxWorks.h"
#include "ospf_vs_data.h"

/* This is used for conditional compilation during development. */

#ifndef __OSPF_VIRTUAL_STACK__
#define __OSPF_VIRTUAL_STACK__
#endif

#define OSPF_VSID_MAX 32
#define OSPF_VS_MGMT_STACK 0

typedef OSPF_GLOBAL_DATA* OSPF_VSID;

IMPORT SEM_ID ospfVsTblLock;
IMPORT OSPF_GLOBAL_DATA* ospfVsTbl[];
IMPORT int myStackNum;
IMPORT OSPF_VSID mgmtStackId;


#ifdef __cplusplus
extern "C" {
#endif

IMPORT STATUS ospfVirtualStackLibInit();
IMPORT STATUS ospfVirtualStackCreate(OSPF_VSID* pOSPF_VSID);
IMPORT STATUS ospfVirtualStackInit(OSPF_VSID ospfVsId);
IMPORT STATUS ospfVirtualStackDelete(OSPF_VSID ospfVsId);
IMPORT STATUS ospfVirtualStackIdGet(char* pName, OSPF_VSID* pOSPF_VSID);
IMPORT void ospfVirtualStackInfo(OSPF_VSID ospfVsId);
IMPORT void ospfVirtualStackShow(void);
IMPORT void ospfConfigurationTableInit(CONFIGURATION_TABLE *configuration_table);
#ifdef __cplusplus
}
#endif

#endif /* __INCospf_vs_libh */
