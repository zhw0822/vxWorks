/* ospf.h - main OSPF header file */

/* Copyright 2001-2003 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*
modification history
--------------------
03i,16feb05,xli include m2Lib.h 
03h,29nov04,tlu OSPF stack decoupling 
03g,22apr04,ram AVL & Memory migration to shared library
03f,19apr04,zhr compile with the IP dual stack CD
03e,07apr04,ram OSPF v2/v3 coexistance compile
03d,12mar04,zhr changed #include mdLib.h to ospf_mdLib.h
03c,01dec03,agi added #include <mdLib.h> - merged from 20dev branch
03b,29sep03,agi removed #include "ospf_container.h"
03a,05aug03,agi ported to Accordion stack
02e,02jun03,agi removed #include "rwproto.h"
02d,02jun03,agi changed #include "rwos.h" to include "ospf_rwos.h"
02c,29may03,agi removed unused includes, added new includes
02c,08may03,asr Changes to make OSPF virtual stack compatible
02b,09may03,agi added #include <kernelLib.h>, removed #include <v8022str.h>
02a,17feb02,ram SPR 81808 Added OSPF memory partition support
*/

#if !defined (_OSPF_H_)
#define _OSPF_H_

#if !defined (__VxWORKS__)
#define __VxWORKS__
#endif /* __VxWORKS__ */

#if !defined (__RTM__)
#define __RTM__
#endif /* __RTM__ */

#if !defined (__BIG_ENUMS__)
#define __BIG_ENUMS__
#endif /* __BIG_ENUMS__ */

#include <vxWorks.h>
#include <selectLib.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <stdio.h>
#include <stddef.h>
#include <sysLib.h>
#include <unistd.h>
#include <kernelLib.h>
#include <wdLib.h>
#include <time.h>
#include <net/if.h>
#include <net/if_var.h>
#include <netinet/in.h>
#include <netinet/in_var.h>
#include <netLib.h>
#include <sys/socket.h>
#include <net/route.h>
#include <netinet/ip_icmp.h>
#include <net/socketvar.h>
#include <tickLib.h>
#include <errnoLib.h>
#include <taskLib.h>/* Added for Task Spawn for the "tOspfRecv" task*/
/* #include <routeEnhLib.h> */
#include <m2Lib.h> 
#include <ifLib.h>
#include <sys/sysctl.h>
#include <md5.h>
#include <ospf_enums.h>
#include <avlLib.h>

#if defined (__OSPF_FTP__)
#include <ftpLib.h>
#include <ramDrv.h>
#include <dosFsLib.h>
#endif /*__OSPF_FTP__*/

#include "ospf_constants.h"
#include <ospfCommonLib.h>
/* #include <routeEnhLib.h> */
#include "ospf_ip_structures.h"
#include "ospf_structures.h"
#include "ospf_globals.h"
#include "ospf_prototypes.h"
#include "ospf_invar.h"
#include "ospf_vnvram.h"
#include "ospf_memory.h"

#if defined (__OSPF_MIB__)
#include   "ospfCommonMibApi.h"
#include   "ospf_mib_helper_update.h"
#endif /*__OSPF_MIB__*/

#if defined (VIRTUAL_STACK)
    #include <netinet/vsIp.h>
    #include <netinet/vsLib.h>
    #include <vs/vsIf.h>
#endif /* VIRTUAL_STACK */

#endif /* _OSPF_H_ */
