/* ospf_vs.h - ospf virtual stack data */

/* Copyright 2001-2003 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*
modification history
--------------------
02a,22aug03,agi ported to Accordion, updated copyright
01c,12may03,asr Changes to make OSPF virtual stack compatible.
01b,13aug01,kc  Removed FP_OSPF_RECEIVE, FP_OSPF_SEND and OSPF_CALLBACK_IF.
01a,7mar01,jkw  written
*/

#ifndef __INCospf_vsh
#define __INCospf_vsh


/* includes */

#include "vxWorks.h"
#include "ospf.h"

/* defines */

/* typedefs */
/* definitions from ospf_init.c */
#if defined (__OSPF_VIRTUAL_STACK__)

typedef struct ospfVs
    {
	/* definitions from ospf_init.c */
	OSPF_CALLBACK_IF _OspfRegistrationInfo;

	/* definitions from ospf_system.c */
	char _ospf_configuration_text[RWOS_MAXIMUM_FILE_SIZE + 1];

	/* definitions from ospf_timer.c */
	UINT _ten_seconds_counter;

	/* definitions from configuration.h */
	CONFIGURATION_TABLE _ospf_configuration_table;

	/* definitions from ospf_globals.h */
	OSPF_CLASS _ospf;
	RWOS_MUTEX _rwos_ospf_mutex;
	int _ospfRawInputTaskId;
	ULONG _ospf_db_overflow_timer;

    } OSPF_VS;

/* macros */

#define OspfRegistrationInfo  ospfVsTbl[myStackNum]->ospfGlobals._OspfRegistrationInfo
#define	ospf_configuration_text ospfVsTbl[myStackNum]->ospfGlobals._ospf_configuration_text
#define ten_seconds_counter ospfVsTbl[myStackNum]->ospfGlobals._ten_seconds_counter
#define ospf_configuration_table ospfVsTbl[myStackNum]->ospfGlobals._ospf_configuration_table
#define ospf ospfVsTbl[myStackNum]->ospfGlobals._ospf


#define ospf_vs_id ospf.ospf_vs_id

#define rwos_ospf_mutex ospfVsTbl[myStackNum]->ospfGlobals._rwos_ospf_mutex
#define ospfRawInputTaskId ospfVsTbl[myStackNum]->ospfGlobals._ospfRawInputTaskId
#define ospf_db_overflow_timer ospfVsTbl[myStackNum]->ospfGlobals._ospf_db_overflow_timer


#endif /* __OSPF_VIRTUAL_STACK__ */


#endif /* __INCospf_vsh */

