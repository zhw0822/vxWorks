/* vnvram.h */

/* Copyright 2003 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*
modification history
--------------------
01e,06apr04,ram OSPF v2/v3 coexistance changes
01d,06aug03,agi fixed compiler error
01c,14aug03,kkz Changed nvram to ospf_nvram to prevent conflicts
01b,12jun03,agi added copyright notice, removed unused tables
01a,29may03,agi created (ported from RWOS)
*/

#ifndef __INCospfv3Vnvramh
#define __INCospfv3Vnvramh

#ifdef __cplusplus
extern "C" {
#endif

OSPF_NVRAM_CLASS ospf_nvram;

void nvram_printf (const char *cptr_format, ...);

void ospf_set_ulong_decimal_value
    (char *cptr_value_string,
     ULONG offset,
     ULONG ulptr_base);

void ospf_set_variable_port_and_enable
    (char *cptr_start_of_configuration_string,
     ULONG vptr_parameter_1,
     ULONG ulptr_parameter_2,
     ULONG size_of_parameter);

void ospf_set_enum_enable
    (char *cptr_value_string,
     ULONG offset,
     ULONG ulptr_base);

void ospf_set_variable_port_and_ushort_decimal_value
    (char *cptr_start_of_configuration_string,
     ULONG vptr_parameter_1,
     ULONG ulptr_parameter_2,
     ULONG size_of_parameter);

void ospf_set_variable_port_and_ulong_decimal_value
    (char *cptr_value_string,
     ULONG vptr_parameter_1,
     ULONG ulptr_parameter_2,
     ULONG size_of_parameter);

void ospf_set_variable_port_and_ip_address
    (char *cptr_ip_address_string,
     ULONG vptr_parameter_1,
     ULONG ulptr_parameter_2,
     ULONG size_of_parameter);

void ospf_set_ip_address
    (char *cptr_ip_address_string,
     ULONG offset,
     ULONG ulptr_base);

#ifdef __cplusplus
}
#endif

#endif /*__INCospfv3Vnvramh*/
