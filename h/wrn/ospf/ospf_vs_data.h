/* ospf_vs_data.h - ospf virtual stack data */

/* Copyright 2000 Wind River Systems, Inc. */

/*
modification history
--------------------
01a,07mar01,jkw  written

*/

#ifndef __INCospf_vs_datah
#define __INCospf_vs_datah

#include "vxWorks.h"
#include "ospf_vs.h"

/* defines */

#define OSPF_VS_NAME_MAX     32


typedef struct ospf_global_data
{    
	/* OSPF VS Layer.  Things the OSPF VS system needs. */
    /* Name of this virtual ospf stack. */
    char name[OSPF_VS_NAME_MAX + 1];
    char *pName;

	OSPF_VS ospfGlobals;
} OSPF_GLOBAL_DATA;

/* macros */


#endif /* __INCospf_vs_datah */
