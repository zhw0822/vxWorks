/* ospf_snmp.h */

/* Copyright 2000 Wind River Systems, Inc. */

/*modification history
___________________
 18,26september00,reshma		Added WindRiver CopyRight
 17,25september00,reshma		RFC-1587 implementation for OSPF NSSA Option, also tested against ANVL.
 16,20july00,reshma				Unix compatibility related changes.
 15,06july00,reshma				Removed unnecessary header files and defines.
 14,04april00,reshma			Added some MIB support (Read only).
								Passed all important ANVL OSPF tests.
 13,23december99,reshma			Compatibility with VxWorks-IP and VxWorks RTM-interface
 12,19may99,jack				redefined OSPF_PATRICIA_32_BITS_KEY_MAX_BIT_POSITION as per fixes in
								patricia
 11,28december98,jack			Compiled and added some comments
 10,11november98,jack			Config changes, linted and big endian changes
 09,30october98,jack			Incorporate changes for compilation on Vxworks
 08,23august98,jack				ANVL tested OSPF with PATRICIA tree route table and no recursion
 07,10august98,jack				PATRICIA Route Table Based OSPF Code Base
 06,08july98,jack				Patricia RT table related changes - need to be tested
 05,04june98,jack				Integration with RTM and BGP
 04,10july97,cindy				Pre-release v1.52b
 03,10february97,cindy			Release Version 1.52
 02,22october97,cindy			Release Version 1.50
 01,05june96,cindy				First Beta Release
*/

#if !defined (_OSPF_SNMP_H_)
#define _OSPF_SNMP_H_


SNMP_TABLE_ENTRY_PARAMETERS	ospf_snmp_table_parameters[] =
{
	{(ULONG) &ospf.ospfGeneralGroupEntry,                           /* address of structure containing mib variable */
		FALSE														/* is a table entry */
	},

	{(ULONG) NULL,
		TRUE,
		0x01,                                               /* number of indices */
		{{
			SNMP_IP_ADDRESS_TYPE,                           /* index type */
			sizeof (ULONG),                                 /* size of index */
			offsetof (OSPF_AREA_ENTRY, area_id),                 /* offset to index */
			FALSE,                                          /* need to increment */
			NULL,                                           /* vptr_index_value */
			FALSE,                                          /* need to swap */
			NULL,                                           /* vptr_maximum_value_of_index */
			FALSE													/* should be used to get pointer to next array element */
		}},
		MIB_TABLE_AS_A_LINKED_LIST,                         /* table type */
		&ospf.sptr_area_list									/* vptr_first_row_in_mib_table */
	},

	{(ULONG) NULL,
		TRUE,
		0x02,
		{{
			SNMP_IP_ADDRESS_TYPE,
			sizeof (ULONG),
			offsetof (OSPF_AREA_ENTRY, area_id),
			FALSE,
			NULL,
			FALSE,
			NULL,
			FALSE
		},{
			SNMP_SCALAR_TYPE,
			sizeof (ULONG),
			offsetof (OSPF_AREA_ENTRY, mib_stub_type_of_service),
			FALSE,
			NULL,
			FALSE,
			NULL,
			FALSE
		}},
		MIB_TABLE_AS_A_LINKED_LIST,
		&ospf.sptr_stub_area_list

 	},

	{(ULONG) NULL,
		TRUE,
		0x04,
		{{
			SNMP_IP_ADDRESS_TYPE,
			sizeof (ULONG),
			offsetof (OSPF_MIB_LS_DATABASE_ENTRY, area_id),
			FALSE,
			NULL,
			FALSE,
			NULL,
			FALSE
		},{
			SNMP_SCALAR_TYPE,
			sizeof (BYTE),
			offsetof (OSPF_MIB_LS_DATABASE_ENTRY, link_state_type),
			FALSE,
			NULL,
			FALSE,
			NULL,
			FALSE
		},{
			SNMP_IP_ADDRESS_TYPE,
			sizeof (ULONG),
			offsetof (OSPF_MIB_LS_DATABASE_ENTRY, link_state_id),
			FALSE,
			NULL,
			FALSE,
			NULL,
			FALSE
		},{
			SNMP_IP_ADDRESS_TYPE,
			sizeof (ULONG),
			offsetof (OSPF_MIB_LS_DATABASE_ENTRY, router_id),
			FALSE,
			NULL,
			FALSE,
			NULL,
			FALSE
		}},
		MIB_TABLE_AS_A_LINKED_LIST,
		&ospf.sptr_mib_ls_database_list
	},

	{(ULONG) NULL,
		TRUE,
		0x02,
		{{
			SNMP_IP_ADDRESS_TYPE,
			sizeof (ULONG),
			offsetof (OSPF_HOSTS, interface_address),
			FALSE,
			NULL,
			FALSE,
			NULL,
			FALSE
		},{
			SNMP_SCALAR_TYPE,
			sizeof (ULONG),
			offsetof (OSPF_HOSTS, type_of_service),
			FALSE,
			NULL,
			FALSE,
			NULL,
			FALSE
		}},
		MIB_TABLE_AS_A_LINKED_LIST,
		&ospf.sptr_host_list
	},

	{(ULONG) NULL,
		TRUE,
		0x02,
		{{
			SNMP_IP_ADDRESS_TYPE,
			sizeof (ULONG),
			offsetof (OSPF_INTERFACE, address),
			FALSE,
			NULL,
			FALSE,
			NULL,
			FALSE
		},{
			SNMP_SCALAR_TYPE,
			sizeof (ULONG),
			offsetof (OSPF_INTERFACE, mib_address_less_if),
			TRUE,
			NULL,
			FALSE,
			NULL,
			TRUE
		}},
		MIB_TABLE_AS_AN_ARRAY,
		&ospf.port[0].sptr_interface,
		sizeof (OSPF_PORT_CLASS),
		NUMBER_OF_OSPF_PORTS
	},

	{(ULONG) NULL,
		TRUE,
		0x03,
		{{
			SNMP_IP_ADDRESS_TYPE,
			sizeof (ULONG),
			offsetof (OSPF_INTERFACE, address),
			FALSE,
			NULL,
			FALSE,
			NULL,
			FALSE
		},{
			SNMP_SCALAR_TYPE,
			sizeof (ULONG),
			offsetof (OSPF_INTERFACE, mib_address_less_if),
			TRUE,
			NULL,
			FALSE,
			NULL,
			TRUE
		},{
			SNMP_SCALAR_TYPE,
			sizeof (ULONG),
			offsetof (OSPF_INTERFACE, metric_type_of_service),
			TRUE,
			NULL,
			FALSE,
			NULL,
			TRUE
		}},
		MIB_TABLE_AS_AN_ARRAY,
		&ospf.port[0].sptr_interface,
		sizeof (OSPF_PORT_CLASS),
		NUMBER_OF_OSPF_PORTS
	},

	{(ULONG) NULL,
		TRUE,
		0x02,
		{{
			SNMP_IP_ADDRESS_TYPE,
			sizeof (ULONG),
			offsetof (OSPF_NEIGHBOR, address),
			FALSE,
			NULL,
			FALSE,
			NULL,
			FALSE
		},{
			SNMP_SCALAR_TYPE,
			sizeof (ULONG),
			offsetof (OSPF_NEIGHBOR, mib_address_less_index),
			FALSE,
			NULL,
			FALSE,
			NULL,
			FALSE
		}},
		MIB_TABLE_AS_A_LINKED_LIST,
		&ospf.sptr_neighbor_list
	},

	{(ULONG) NULL,
		TRUE,
		0x02,
		{{
			SNMP_IP_ADDRESS_TYPE,
			sizeof (ULONG),
			offsetof (OSPF_NEIGHBOR, mib_area_id),
			FALSE,
			NULL,
			FALSE,
			NULL,
			FALSE
		},{
			SNMP_IP_ADDRESS_TYPE,
			sizeof (ULONG),
			offsetof (OSPF_NEIGHBOR, id),
			FALSE,
			NULL,
			FALSE,
			NULL,
			FALSE
		}},
		MIB_TABLE_AS_A_LINKED_LIST,
		&ospf.sptr_virtual_neighbor_list
	},

	{(ULONG) NULL,
		TRUE,
		0x03,
		{{
			SNMP_SCALAR_TYPE,
			sizeof (BYTE),
			offsetof (OSPF_ADVERTISEMENT_NODE, mib_ls_header.type),
			FALSE,
			NULL,
			FALSE,
			NULL,
			FALSE
		},{
			SNMP_IP_ADDRESS_TYPE,
			sizeof (ULONG),
			offsetof (OSPF_ADVERTISEMENT_NODE, mib_ls_header.id),
			FALSE,
			NULL,
			FALSE,
			NULL,
			FALSE
		},{
			SNMP_IP_ADDRESS_TYPE,
			sizeof (ULONG),
			offsetof (OSPF_ADVERTISEMENT_NODE, mib_ls_header.advertising_router),
			FALSE,
			NULL,
			FALSE,
			NULL,
			FALSE
		}},
		MIB_TABLE_AS_A_LINKED_LIST,
		&ospf.sptr_external_advertisement_list_head
	}
};

SNMP_TABLE_ENTRY	ospf_snmp_table[] =
{
	{"ospfRouterId",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,                                                                  /* need to increment mib value for get operation */
		FALSE,                                                                  /* need to swap */
		(ULONG) offsetof (OSPF_MIB_GENERAL_ENTRY,ospfRouterId),
		&ospf_snmp_table_parameters[0]
		},

	{"ospfAdminStat",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_boolean_plus_one,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_MIB_GENERAL_ENTRY,ospfAdminStat),
		&ospf_snmp_table_parameters[0]
		},

	{"ospfVersionNumber",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_MIB_GENERAL_ENTRY,ospfVersionNumber),
		&ospf_snmp_table_parameters[0]
		},

	{"ospfAreaBdrRtrStatus",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_boolean_plus_one,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_MIB_GENERAL_ENTRY,ospfAreaBdrRtrStatus),
		&ospf_snmp_table_parameters[0]
		},

	{"ospfASBdrRtrStatus",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_boolean_plus_one,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_MIB_GENERAL_ENTRY,ospfASBdrRtrStatus),
		&ospf_snmp_table_parameters[0]
		},

	{"ospfExternLsaCount",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_MIB_GENERAL_ENTRY,ospfExternLsaCount),
		&ospf_snmp_table_parameters[0]
		},

	{"ospfExternLsaCksumSum",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_MIB_GENERAL_ENTRY,ospfExternLsaCksumSum),
		&ospf_snmp_table_parameters[0]
		},

	{"ospfTOSSupport",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_boolean_plus_one,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_MIB_GENERAL_ENTRY,ospfTOSSupport),
		&ospf_snmp_table_parameters[0]
		},

	{"ospfOriginateNewLsas",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_MIB_GENERAL_ENTRY,ospfOriginateNewLsas),
		&ospf_snmp_table_parameters[0]
		},

	{"ospfRxNewLsas",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_MIB_GENERAL_ENTRY,ospfRxNewLsas),
		&ospf_snmp_table_parameters[0]
		},

	{"ospfExtLsdbLimit",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_MIB_GENERAL_ENTRY,ospfExtLsdbLimit),
		&ospf_snmp_table_parameters[0]
		},

	{"ospfMulticastExtensions",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_MIB_GENERAL_ENTRY,ospfMulticastExtensions),
		&ospf_snmp_table_parameters[0]
		},

	{"ospfExitOverflowInterval",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_MIB_GENERAL_ENTRY,ospfExitOverflowInterval),
		&ospf_snmp_table_parameters[0]
		},

	{"ospfDemandExtensions",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_boolean_plus_one,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_MIB_GENERAL_ENTRY,ospfDemandExtensions),
		&ospf_snmp_table_parameters[0]
		},

	{"ospfAreaId",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_AREA_ENTRY,area_id),
		&ospf_snmp_table_parameters[1]
		},

	{"ospfImportAsExtern",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_AREA_ENTRY,mib_import_as_external),
		&ospf_snmp_table_parameters[1]
		},

	{"ospfSpfRuns",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_AREA_ENTRY,shortest_path_first_run_count),
		&ospf_snmp_table_parameters[1]
		},

	{"ospfAreaBdrRtrCount",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_AREA_ENTRY,number_of_area_border_routers),
		&ospf_snmp_table_parameters[1]
		},

	{"ospfAsBdrRtrCount",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_AREA_ENTRY,number_of_autonomous_system_boundary_routers),
		&ospf_snmp_table_parameters[1]
		},

	{"ospfAreaLsaCount",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_AREA_ENTRY,mib_number_of_link_state_advertisements),
		&ospf_snmp_table_parameters[1]
		},

	{"ospfAreaLsaCksumSum",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_AREA_ENTRY,mib_checksum_sum),
		&ospf_snmp_table_parameters[1]
		},

	{"ospfAreaSummary",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_AREA_ENTRY,mib_area_summary),
		&ospf_snmp_table_parameters[1]
		},

	{"ospfAreaStatus",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_boolean_plus_one,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_AREA_ENTRY,mib_area_status),
		&ospf_snmp_table_parameters[1]
		},

	{"ospfStubAreaId",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_AREA_ENTRY,area_id),
		&ospf_snmp_table_parameters[2]
		},

	{"ospfStubTOS",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_AREA_ENTRY,mib_stub_type_of_service),
		&ospf_snmp_table_parameters[2]
		},

	{"ospfStubMetric",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_AREA_ENTRY,stub_default_cost),
		&ospf_snmp_table_parameters[2]
		},

	{"ospfStubStatus",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_boolean_plus_one,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_AREA_ENTRY,mib_area_status),
		&ospf_snmp_table_parameters[2]
		},

	{"ospfStubMetricType",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_AREA_ENTRY,mib_stub_metric_type),
		&ospf_snmp_table_parameters[2]
		},

	{"ospfLsdbAreaId",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_MIB_LS_DATABASE_ENTRY,area_id),
		&ospf_snmp_table_parameters[3]
		},

	{"ospfLsdbType",sizeof (BYTE),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_byte,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_MIB_LS_DATABASE_ENTRY,link_state_type),
		&ospf_snmp_table_parameters[3]
		},

	{"ospfLsdbLsid",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_MIB_LS_DATABASE_ENTRY,link_state_id),
		&ospf_snmp_table_parameters[3]
		},

	{"ospfLsdbRouterId",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_MIB_LS_DATABASE_ENTRY,router_id),
		&ospf_snmp_table_parameters[3]
		},

	{"ospfLsdbSequence",sizeof (long),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_8_byte_object,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_MIB_LS_DATABASE_ENTRY,sequence),
		&ospf_snmp_table_parameters[3]
		},

	{"ospfLsdbAge",sizeof (long),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_MIB_LS_DATABASE_ENTRY,age),
		&ospf_snmp_table_parameters[3]
		},

	{"ospfLsdbChecksum",sizeof (long),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_MIB_LS_DATABASE_ENTRY,checksum),
		&ospf_snmp_table_parameters[3]
		},

	{"ospfLsdbAdvertisement", MAXIMUM_ETHERNET_DATA_SIZE,
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_string,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_MIB_LS_DATABASE_ENTRY,advertisement),
		&ospf_snmp_table_parameters[3]
		},

	{"ospfAreaRangeAreaId", sizeof (long),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_AREA_ENTRY,area_id),
		&ospf_snmp_table_parameters[2]
		},

/*	{"ospfAreaRangeNet", sizeof (long),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_AREA_ENTRY,mib_area_range_net),
		&ospf_snmp_table_parameters[1]
		},

	{"ospfAreaRangeMask", sizeof (long),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_AREA_ENTRY,mib_area_range_mask),
		&ospf_snmp_table_parameters[1]
		},

	{"ospfAreaRangeStatus", sizeof (long),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_AREA_ENTRY,mib_area_range_status),
		&ospf_snmp_table_parameters[1]
		},
*/
	{"ospfHostIpAddress",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_HOSTS,interface_address),
		&ospf_snmp_table_parameters[4]
		},

	{"ospfHostTOS",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_HOSTS,type_of_service),
		&ospf_snmp_table_parameters[4]
		},

	{"ospfHostMetric",sizeof (USHORT),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ushort,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_HOSTS,cost),
		&ospf_snmp_table_parameters[4]
		},

	{"ospfHostStatus",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_boolean_plus_one,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_HOSTS,status),
		&ospf_snmp_table_parameters[4]
		},

	{"ospfHostAreaID",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_HOSTS,area_id),
		&ospf_snmp_table_parameters[4]
		},

	{"ospfIfIpAddress",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_INTERFACE,address),
		&ospf_snmp_table_parameters[5]
		},

	{"ospfAddressLessIf",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_INTERFACE,mib_address_less_if),
		&ospf_snmp_table_parameters[5]
		},

	{"ospfIfAreaId",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_INTERFACE,mib_area_id),
		&ospf_snmp_table_parameters[5]
		},

	{"ospfIfType",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_INTERFACE,type),
		&ospf_snmp_table_parameters[5]
		},

	{"ospfIfAdminStat",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_boolean_plus_one,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_INTERFACE,mib_admin_status),
		&ospf_snmp_table_parameters[5]
		},

	{"ospfIfRtrPriority",sizeof (USHORT),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ushort,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_INTERFACE,priority),
		&ospf_snmp_table_parameters[5]
		},

	{"ospfIfTransitDelay",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_INTERFACE,transmit_delay),
		&ospf_snmp_table_parameters[5]
		},

	{"ospfIfRetransInterval",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_INTERFACE,retransmit_interval),
		&ospf_snmp_table_parameters[5]
		},

	{"ospfIfHelloInterval",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_INTERFACE,hello_interval),
		&ospf_snmp_table_parameters[5]
		},

	{"ospfIfRtrDeadInterval",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_INTERFACE,router_dead_interval),
		&ospf_snmp_table_parameters[5]
		},

	{"ospfIfPollInterval",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_INTERFACE,poll_interval),
		&ospf_snmp_table_parameters[5]
		},

	{"ospfIfState",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_INTERFACE,state),
		&ospf_snmp_table_parameters[5]
		},

	{"ospfIfDesignatedRouter",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_INTERFACE,designated_router.id),
		&ospf_snmp_table_parameters[5]
		},

	{"ospfIfBackupDesignatedRouter",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_INTERFACE,backup_designated_router.id),
		&ospf_snmp_table_parameters[5]
		},

	{"ospfIfEvents",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_INTERFACE,events),
		&ospf_snmp_table_parameters[5]
		},

	{"ospfIfAuthKey",sizeof (BYTE),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_byte,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_INTERFACE,authentication.key_or_plain_text_passwd ),
		&ospf_snmp_table_parameters[5]
		},

	{"ospfIfStatus",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_boolean_plus_one,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_INTERFACE,mib_status),
		&ospf_snmp_table_parameters[5]
		},

	{"ospfIfMulticastForwarding",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_INTERFACE,mib_multicast_forwarding),
		&ospf_snmp_table_parameters[5]
		},

	{"ospfIfDemand",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_boolean_plus_one,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_INTERFACE,mib_demand),
		&ospf_snmp_table_parameters[5]
		},

	{"ospfIfAuthType",sizeof (USHORT),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ushort,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_INTERFACE,authentication.type),
		&ospf_snmp_table_parameters[5]
		},

	{"ospfIfMetricIpAddress",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_INTERFACE,address),
		&ospf_snmp_table_parameters[6]
		},

	{"ospfIfMetricAddressLessIf",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_INTERFACE,mib_address_less_if),
		&ospf_snmp_table_parameters[6]
		},

	{"ospfIfMetricTOS",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_INTERFACE,metric_type_of_service),
		&ospf_snmp_table_parameters[6]
		},

	{"ospfIfMetricValue",sizeof (USHORT),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ushort,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_INTERFACE,cost),
		&ospf_snmp_table_parameters[6]
		},

	{"ospfIfMetricStatus",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_boolean_plus_one,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_INTERFACE,metric_status),
		&ospf_snmp_table_parameters[6]
		},

	{"ospfVirtIfAreaId",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_INTERFACE,mib_area_id),
		&ospf_snmp_table_parameters[7]
		},

	{"ospfVirtIfNeighbor",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_INTERFACE,virtual_neighbor_rid),
		&ospf_snmp_table_parameters[7]
		},

	{"ospfVirtIfTransitDelay",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_INTERFACE,transmit_delay),
		&ospf_snmp_table_parameters[7]
		},

	{"ospfVirtIfRetransInterval",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_INTERFACE,retransmit_interval),
		&ospf_snmp_table_parameters[7]
		},

	{"ospfVirtIfHelloInterval",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_INTERFACE,hello_interval),
		&ospf_snmp_table_parameters[7]
		},

	{"ospfVirtIfRtrDeadInterval",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_INTERFACE,router_dead_interval),
		&ospf_snmp_table_parameters[7]
		},

	{"ospfVirtIfState",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_INTERFACE,state),
		&ospf_snmp_table_parameters[7]
		},

	{"ospfVirtIfEvents",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_INTERFACE,events),
		&ospf_snmp_table_parameters[7]
		},

	{"ospfVirtIfAuthType",sizeof (USHORT),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ushort,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_INTERFACE,authentication.type),
		&ospf_snmp_table_parameters[7]
		},

	{"ospfVirtIfAuthKey",sizeof (BYTE),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_byte,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_INTERFACE,authentication.key_or_plain_text_passwd),
		&ospf_snmp_table_parameters[7]
		},

	{"ospfVirtIfStatus",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_INTERFACE,mib_status),
		&ospf_snmp_table_parameters[7]
		},

	{"ospfNbrIpAddr",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_NEIGHBOR,address),
		&ospf_snmp_table_parameters[8]
		},

	{"ospfNbrAddressLessIndex",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_NEIGHBOR,mib_address_less_index),
		&ospf_snmp_table_parameters[8]
		},

	{"ospfNbrRtrId",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_NEIGHBOR,id),
		&ospf_snmp_table_parameters[8]
		},

	{"ospfNbrOptions",sizeof (BYTE),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_byte,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_NEIGHBOR,options),
		&ospf_snmp_table_parameters[8]
		},

	{"ospfNbrPriority",sizeof (USHORT),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ushort,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_NEIGHBOR,priority),
		&ospf_snmp_table_parameters[8]
		},

	{"ospfNbrState",sizeof (BYTE),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_byte,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_NEIGHBOR,state),
		&ospf_snmp_table_parameters[8]
		},

	{"ospfNbrEvents",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_NEIGHBOR,events),
		&ospf_snmp_table_parameters[8]
		},

	{"ospfNbrLsRetransQLen",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_NEIGHBOR,retransmit_queue_count),
		&ospf_snmp_table_parameters[8]
		},

	{"ospfNbmaNbrStatus",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_boolean_plus_one,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_NEIGHBOR,mib_nbma_status),
		&ospf_snmp_table_parameters[8]
		},

	{"ospfNbmaNbrPermanence",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_boolean_plus_one,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_NEIGHBOR,mib_nbma_permanence),
		&ospf_snmp_table_parameters[8]
		},

	{"ospfNbrHelloSuppressed",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_boolean_plus_one,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_NEIGHBOR,mib_hello_suppressed),
		&ospf_snmp_table_parameters[8]
		},

	{"ospfVirtNbrArea",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_NEIGHBOR,mib_area_id),
		&ospf_snmp_table_parameters[9]
		},

	{"ospfVirtNbrRtrId",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_NEIGHBOR,id),
		&ospf_snmp_table_parameters[9]
		},

	{"ospfVirtNbrIpAddr",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_NEIGHBOR,address),
		&ospf_snmp_table_parameters[9]
		},

	{"ospfVirtNbrOptions",sizeof (BYTE),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_byte,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_NEIGHBOR,options),
		&ospf_snmp_table_parameters[9]
		},

	{"ospfVirtNbrState",sizeof (BYTE),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_byte,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_NEIGHBOR,state),
		&ospf_snmp_table_parameters[9]
		},

	{"ospfVirtNbrEvents",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_NEIGHBOR,events),
		&ospf_snmp_table_parameters[9]
		},

	{"ospfVirtNbrLsRetransQLen",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_NEIGHBOR,retransmit_queue_count),
		&ospf_snmp_table_parameters[9]
		},

	{"ospfVirtNbrHelloSuppressed",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_boolean_plus_one,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_NEIGHBOR,mib_hello_suppressed),
		&ospf_snmp_table_parameters[9]
		},

	{"ospfExtLsdbType",sizeof (BYTE),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_byte,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_ADVERTISEMENT_NODE,mib_ls_header.type),
		&ospf_snmp_table_parameters[10]
		},

	{"ospfExtLsdbLsid",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_ADVERTISEMENT_NODE,mib_ls_header.id),
		&ospf_snmp_table_parameters[10]
		},

	{"ospfExtLsdbRouterId",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_ADVERTISEMENT_NODE,mib_ls_header.advertising_router),
		&ospf_snmp_table_parameters[10]
		},

	{"ospfExtLsdbSequence",sizeof (long),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_8_byte_object,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_ADVERTISEMENT_NODE,mib_ls_header.sequence_number),
		&ospf_snmp_table_parameters[10]
		},

	{"ospfExtLsdbAge",sizeof (long),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_ADVERTISEMENT_NODE,mib_ls_header.age),
		&ospf_snmp_table_parameters[10]
		},

	{"ospfExtLsdbChecksum",sizeof (long),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_ADVERTISEMENT_NODE,mib_ls_header.checksum),
		&ospf_snmp_table_parameters[10]
		},

	{"ospfExtLsdbAdvertisement", MAXIMUM_ETHERNET_DATA_SIZE,
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_string,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_ADVERTISEMENT_NODE,mib_advertisement),
		&ospf_snmp_table_parameters[10]
		},

    /*opaque lsa new snmp opaque variables jkw*/
	{"ospfLsa9AreaId",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_MIB_LSA9_DATABASE_ENTRY,area_id),
		&ospf_snmp_table_parameters[3]
		},

	{"ospfLsa9Type",sizeof (BYTE),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_byte,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_MIB_LSA9_DATABASE_ENTRY,link_state_type),
		&ospf_snmp_table_parameters[3]
		},

	{"ospfLsa9Lsid",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_MIB_LSA9_DATABASE_ENTRY,link_state_id),
		&ospf_snmp_table_parameters[3]
		},

	{"ospfLsa9RouterId",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_MIB_LSA9_DATABASE_ENTRY,router_id),
		&ospf_snmp_table_parameters[3]
		},

	{"ospfLsa9Sequence",sizeof (long),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_8_byte_object,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_MIB_LSA9_DATABASE_ENTRY,sequence),
		&ospf_snmp_table_parameters[3]
		},

	{"ospfLsa9Age",sizeof (long),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_MIB_LSA9_DATABASE_ENTRY,age),
		&ospf_snmp_table_parameters[3]
		},

	{"ospfLsa9Checksum",sizeof (long),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_MIB_LSA9_DATABASE_ENTRY,checksum),
		&ospf_snmp_table_parameters[3]
		},

	{"ospfLsa9Advertisement", MAXIMUM_ETHERNET_DATA_SIZE,
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_string,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_MIB_LSA9_DATABASE_ENTRY,advertisement),
		&ospf_snmp_table_parameters[3]
		},

	{"ospfType9LsaCount",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_MIB_GENERAL_ENTRY,ospfType9LsaCount),
		&ospf_snmp_table_parameters[0]
		},

	{"ospfType9LsaCksumSum",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_MIB_GENERAL_ENTRY,ospfType9LsaCksumSum),
		&ospf_snmp_table_parameters[0]
		},

	{"ospfLsa11Type",sizeof (BYTE),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_byte,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_MIB_LSA11_DATABASE_ENTRY,link_state_type),
		&ospf_snmp_table_parameters[3]
		},

	{"ospfLsa11Lsid",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_MIB_LSA11_DATABASE_ENTRY,link_state_id),
		&ospf_snmp_table_parameters[3]
		},

	{"ospfLsa11RouterId",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_MIB_LSA11_DATABASE_ENTRY,router_id),
		&ospf_snmp_table_parameters[3]
		},

	{"ospfLsa11Checksum",sizeof (long),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_MIB_LSA11_DATABASE_ENTRY,checksum),
		&ospf_snmp_table_parameters[3]
		},

	{"ospfLsa11Advertisement", MAXIMUM_ETHERNET_DATA_SIZE,
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_string,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_MIB_LSA11_DATABASE_ENTRY,advertisement),
		&ospf_snmp_table_parameters[3]
		},

	{"ospfType11LsaCount",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_MIB_GENERAL_ENTRY,ospfType11LsaCount),
		&ospf_snmp_table_parameters[0]
		},

	{"ospfType11LsaCksumSum",sizeof (ULONG),
		(enum TEST (*) (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset_into_class, ULONG class_address,
		ULONG size_of_port_class, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained))
		snmp_get_or_set_ulong,
		FALSE,
		FALSE,
		(ULONG) offsetof (OSPF_MIB_GENERAL_ENTRY,ospfType11LsaCksumSum),
		&ospf_snmp_table_parameters[0]
		},



	{"",0x0000,NULL,
			(ULONG) NULL,
		(ULONG) NULL,
		(ULONG) NULL
		}
};

#endif /* _OSPF_SNMP_H_ */
GLOBAL
