/* ospf_constants.h - ospf constants*/

/* Copyright 2000-2004 Wind River Systems, Inc. */

/*
modification history
--------------------
04n,19oct05,xli doc build warnings clean up
04m,13may05,xli cleanup compiler warnings
02u,04may05,xli update OSPF_REV from 3.0 to 3.1
02t,19apr05,xli cleanup gnu compiler warnings
02s,02sep04,ram Increase OSPF thread stack size
02r,29jul04,dsk update OSPF_REV from 2.4 to 3.0
02q,10jun04,mwv update OSPF_REV 
02p,12apr04,ram Cleanup unused macros
02o,12mar04,ram SPR#94944 OSPF performance modifications
02n,22aug03,agi Ported to Accordion
02m,03jul03,ram Backed out SPR#88600 workaround
02l,11jun03,ram SPR#88965 Separate route table and LSDB hash parameters and
                increase LSDB hash parameters.
02k,28may03,agi Addedd code inspection changes
02g,26may03,agi Added OSPF_MAXIMUM_FILE_SIZE, OSPF_RAMDRIVE_VOLUME_NAME
                OSPF_CONFIGURATION_FILE_NAME
02i,26may03,htm Stampede issue# 24 - Adding OSPF_MAX_PKT_SIZE to be used in
                ospf_vx_ip_output() instead of the magic number 1000.
02h,26may03,dsk SPR 88600 fix for stuck in EXCHANGE_START (timer added
                to restart if stuck in exchange start state too long)
02g,09may03,agi added ospf task related constants
02f,10apr03,mwv SPR 85906 update define for OSPF_MAXIMUM_IP_HEADER_SIZE
02e,21mar03,kc  Fixed SPR#87023 - changed OSPF_INPUT_TASK_STACK_SIZE
                from 6K to 8K. Removed unused OSPF_MAPI_TASK_STACK_SIZE
                define.
02d,17feb03,mwv SPR 85906 define max ip header size
02c,14jan03,smr SPR 78250 Need to correct hash table size to accomodate 255.
02b,06jan03,ram SPR 85432 Changes to allow more OSPF external route processing
02a,09dec02,ram SPR 83418 Added support for OSPF external type 1 & 2
01a,05jun96,cin First Beta Release
*/

#if !defined (_OSPF_CONSTANTS_H_)
#define _OSPF_CONSTANTS_H_

#define OSPF_PROTOCOL_ID_VALUE  89
#define OSPF_VERSION 2

#define OSPF_REV             "3.1"
#define OSPF_REV_TYPE        "1"
#define OSPF_BUILD           __DATE__

#if !defined (NUMBER_OF_IP_PORTS)
    #define NUMBER_OF_IP_PORTS 16
#endif /* NUMBER_OF_IP_PORTS */

#define MAXIMUM_IP_OPTION_LENGTH                                40              /* Largest option field, bytes */
#define NO_SUCH_PORT                                                0xffff

#if !defined (PRINT_BUFFER_SIZE)
    #define PRINT_BUFFER_SIZE                                       150
#endif /* PRINT_BUFFER_SIZE */

/* SPR 85906 -- BEGIN */
#define OSPF_MAXIMUM_IP_HEADER_SIZE ((ULONG) 60)    /* largest size for IP header */

#define RFC1745_EGP_ORIGEGP_ROUTE_TAG 0x9000            /* BGP routes reached via OSPF, ext adv originated by some other ASBR  */      /* 4.3.2 RFC 1745 */
#define RFC1745_IGP_TO_OSPF_ROUTE_TAG 0xc000        /* IGP dynamic routes etc. */           /* IGP, local_AS */                /* 4.3.4 RFC 1745 */
#define RFC1745_EGP_ORIGIGP_ROUTE_TAG_PER_AS 0xd000 /* BGP routes with orig IGP, next_hop AS */ /* IGP, Local_AS, Next_hop_as */ /* 4.3.5 RFC 1745 DO not use for now */
#define RFC1745_BGP_TO_OSPF_ROUTE_TAG 0xe000        /* BGP origin */
#define OSPF_INTERNAL 0xffff

#define     NUMBER_OF_OSPF_PORTS        NUMBER_OF_IP_PORTS
#define     NUMBER_OF_OSPF_AREAS        NUMBER_OF_IP_PORTS
#define     NUMBER_OF_OSPF_AREA_ADDRESS_RANGES  16

#define OSPF_NUMBER_OF_INTERFACE_STATES 7
#define OSPF_NUMBER_OF_INTERFACE_EVENTS 7

#define OSPF_NUMBER_OF_NEIGHBOR_STATES  8
#define OSPF_NUMBER_OF_NEIGHBOR_EVENTS  13

#define OSPF_BACKBONE   0x00000000L

#define OSPF_NBMA_DEFAULT_HELLO                 30
#define OSPF_BROADCAST_DEFAULT_HELLO            10
#define OSPF_POINT_TO_POINT_DEFAULT_HELLO       30
#define OSPF_VIRTUAL_DEFAULT_HELLO              60

/* SPR#88965 */
/* OSPF LSDB hash paramters */
#define OSPF_HASH_TABLE_SIZE    512L
#define OSPF_HASH_MASK          0xFF800000
#define OSPF_GET_HASH_INDEX(x)  (((x) & OSPF_HASH_MASK) >> 23)

/* SPR#88965 */
/* OSPF routing table hash parameters
 * These values cannot be changed*/
#define OSPF_RT_HASH_TABLE_SIZE 256L
#define OSPF_RT_HASH_MASK       0xFF000000
#define OSPF_GET_RT_HASH_INDEX(x)  (((x) & OSPF_RT_HASH_MASK) >> 24)


#define OSPF_LSRefreshTime      (1800)
#define OSPF_MinLSArrival       ((ULONG) 0x00000001)
#define OSPF_MinLSInterval      ((ULONG) 0x00000005)
#define OSPF_LSInfinity         0xFFFFFF

#define  OSPF_EXTERNAL_LSA_QUEUE_PROCESS_INTERVAL 10 /* Routerware Inc. Specific */

#define OSPF_CheckAge                       (300)

#define OSPF_MAXIMUM_AGE_DIFFERENCE ((USHORT) 900)
#define OSPF_MAXIMUM_AGE            ((USHORT) 3600)

#define OSPF_ALL_UP_NEIGHBORS           0x00000001
#define OSPF_ALL_ELIGIBLE_NEIGHBORS     0x00000002
#define OSPF_ALL_EXCHANGE_NEIGHBORS     0x00000003
#define OSPF_DESIGNATED_ROUTER_AND_BACKUP_DESIGNATED_ROUTER 0x00000004


#define OSPF_ADDR_ALLSPF    0xe0000005  /* 224.0.0.5 */
#define OSPF_ADDR_ALLDR 0xe0000006  /* 224.0.0.6 */

#define OSPF_IP_TTL_FOR_OSPF_PACKET 0x01      /* RFC 1583 Appendix A.1 */
#define OSPF_IP_TTL_FOR_UNI_CAST_VLINK_OSPF_PACKET 0x80   /* VLINK: Virtual Link*/

#define OSPF_DefaultDestination 0x00000000L
#define OSPF_DefaultMask        0x00000000L

#define OSPF_DEFAULT_TRANSMIT_DELAY 1
#define OSPF_DEFAULT_COST       1
#define OSPF_DEFAULT_ROUTER_DEAD_INTERVAL   40
#define OSPF_DEFAULT_PRIORITY   1
#define OSPF_DEFAULT_RETRANSMIT_INTERVAL    5   /* was 5  */

#define OSPF_DEFAULT_POLL_INTERVAL         120

#define OSPF_ACKNOWLEDGEMENT_INTERVAL       2

#define OSPF_ROUTING_TABLE_BUILD_INTERVAL   2  /* WAS 10 */

#define OSPF_MAXIMUM_PACKET_SIZE_FOR_VIRTUAL_LINK   512 /* does not include the IP header */

#define OSPF_PACKET_SIZE ((ULONG) 24)
#define OSPF_HELLO_HEADER_SIZE ((ULONG) 20)
#define OSPF_DATABASE_HEADER_SIZE   ((ULONG) 8)
#define OSPF_LS_HEADER_SIZE ((ULONG) 20)
#define OSPF_LS_UPDATE_HEADER_SIZE ((ULONG) 4)

#define OSPF_ROUTER_LINK_ADVERTISEMENT_HEADER_SIZE ((ULONG) 24)
#define OSPF_NETWORK_LINK_ADVERTISEMENT_HEADER_SIZE ((ULONG) 24)
#define OSPF_SUMMARY_LINK_ADVERTISEMENT_HEADER_SIZE ((ULONG) 28)
#define OSPF_EXTERNAL_LINK_ADVERTISEMENT_HEADER_SIZE ((ULONG) 36)

/*opaque lsa header size jkw*/
#define OSPF_TYPE_9_LINK_ADVERTISEMENT_HEADER_SIZE ((ULONG) 20)
#define OSPF_TYPE_10_LINK_ADVERTISEMENT_HEADER_SIZE ((ULONG) 20)
#define OSPF_TYPE_11_LINK_ADVERTISEMENT_HEADER_SIZE ((ULONG) 20)

#define OSPF_DB_PIECE_SIZE  ((ULONG) 20)
#define OSPF_LS_REQUESTED_ADVERTISEMENT_SIZE ((ULONG) 12)

#define OSPF_ROUTER_LINK_PIECE_SIZE  ((ULONG) 12)
#define OSPF_NETWORK_LINK_PIECE_SIZE ((ULONG) 4)

#define OSPF_EXTERNAL_LINK_METRIC_PIECE_SIZE ((ULONG) 12)
#define OSPF_ROUTER_LINK_METRIC_PIECE_SIZE ((ULONG) 4)

#define OSPF_HOST_NET_MASK              0xFFFFFFFF

#define OSPF_ASE_bit_E  0x80000000

#define OSPF_AUTHENTICATION_NONE            0   /* No authentication */
#define OSPF_AUTHENTICATION_SIMPLE      1   /* Simple password */
#define OSPF_AUTHENTICATION_MD5         2   /* MD5 crypto checksum */

#define OSPF_AUTHENTICATION_SIMPLE_SIZE 8
#define OSPF_AUTHENTICATION_MD5_SIZE    16
#define OSPF_AUTHENTICATION_SIZE        16

#define OSPF_MD5_A_INIT 0x67452301
#define OSPF_MD5_B_INIT 0xefcdab89
#define OSPF_MD5_C_INIT 0x98badcfe
#define OSPF_MD5_D_INIT 0x10325476

#define OSPF_IF_MULTICAST_ALLSPF        0x100000    /* Joined All SPF group */
#define OSPF_IF_MULTICAST_ALLDR         0x200000    /* Joined All DR group */

#define OSPF_NUMBER_OF_INLINE_COMPUTATIONS      8
#define OSPF_MASK_FOR_UNEVEN_BITS                   (OSPF_NUMBER_OF_INLINE_COMPUTATIONS - 1)
#define OSPF_INLINED_SHIFT                          3
#define OSPF_FINAL_CHECKSUM_SHIFT                   8
#define OSPF_NUMBER_OF_ITERATIONS_BEFORE_MOD    4096
#define OSPF_LOG2_OF_NUMBER_OF_ITERATIONS       12
#define OSPF_MOD_MASK                                   (OSPF_NUMBER_OF_ITERATIONS_BEFORE_MOD - 1)
#define OSPF_NUMBER_OF_INLINE_ITERATIONS        (OSPF_NUMBER_OF_ITERATIONS_BEFORE_MOD/OSPF_NUMBER_OF_INLINE_COMPUTATIONS)
#define OSPF_MODULUS                                    255

#define OSPF_ENABLE_EXTERNAL_ROUTING 0x02
#define OSPF_NOT_ENABLE_EXTERNAL_ROUTING 0x00

#define OSPF_STARTING_AREA_ID_FOR_VIRTUAL_INTERFACE 0xff000000

#define OSPF_CONVERT_IP_ADDRESS_TO_DOT_FORMAT_FOR_DEBUG ospf_convert_ip_address_to_dot_format

#if defined __OSPF_DEBUG__
    #define OSPF_PRINTF_PACKET ospf_printf
    #define OSPF_PRINTF_INTERFACE ospf_printf
    #define OSPF_PRINTF_NEIGHBOR ospf_printf
    #define OSPF_PRINTF_MEMORY  ospf_printf
    #define OSPF_PRINTF_DEBUG   ospf_printf
    #define OSPF_PRINTF_ROUTING_TABLE ospf_printf
    #define OSPF_PRINTF_SNMP ospf_printf
    #define OSPF_PRINTF_RTM ospf_printf
    #define OSPF_PRINTF_DB_OVERFLOW ospf_printf
    #define OSPF_PRINTF_PROLOGUE ospf_printf
#else
    #define OSPF_PRINTF_PACKET(...)
    #define OSPF_PRINTF_INTERFACE(...)
    #define OSPF_PRINTF_NEIGHBOR(...)
    #define OSPF_PRINTF_MEMORY(...)
    #define OSPF_PRINTF_DEBUG(...)
    #define OSPF_PRINTF_ROUTING_TABLE(...)
    #define OSPF_PRINTF_SNMP(...)
    #define OSPF_PRINTF_RTM(...)
    #define OSPF_PRINTF_DB_OVERFLOW(...)
    #define OSPF_PRINTF_PROLOGUE(...)
#endif /*__OSPF_DEBUG__*/

#define OSPF_PRINTF_ALARM ospf_printf

#define OSPF_CONVERT_IP_ADDRESS_TO_DOT_FORMAT_FOR_ALARM ospf_convert_ip_address_to_dot_format
#define OSPF_CONVERT_IP_ADDRESS_TO_DOT_FORMAT_FOR_ROUTING_TABLE ospf_convert_ip_address_to_dot_format

#define ROUTE_MATCH_WITH_ADDRESS_RANGE  1
#define ROUTE_SUBSUMED_BY_ADDRESS_RANGE 2
#define RANGE_STATUS_DO_NOT_ADVERTISE   3


#if !defined (__BIT_FIELDS_CAN_BE_ENUMS__)
    #if defined (_MSC_VER)
        #define BIT_FIELD(enum_,enum_name) unsigned char
    #else
        #define BIT_FIELD(enum_,enum_name) UINT
    #endif
#endif

#if defined BIT_FIELD
#undef BIT_FIELD
    #define BIT_FIELD(enum_,enum_name) unsigned char
#endif

#define MAXIMUM_IP_OPTION_LENGTH                                40              /* Largest option field, bytes */
#define PRINT_BUFFER_SIZE   150

#define NUMBER_OF_IP_PORTS      16  /* Legacy Constant from old IP*/
#define MAXIMUM_ETHERNET_DATA_SIZE 1500     /* Legacy Constant from old v8022str.h */

#define OSPF_RTM_HANDLE ULONG

#if (_BYTE_ORDER == _LITTLE_ENDIAN ) /*#$-NOTE:note36-$#*/
    #define ospf_min min
#endif

/*raw socket maximum receive ospf packet jkw*/
#define MAX_BUFSIZE  8192
/*raw socket end of change*/

#define MAXSIZE_OSPF_AUTH_KEY    256L
/* opaque lsa maximum opaque type lsas jkw */
#define MAX_OPAQUE_NUM 3
/* opaque lsa redefined for array index jkw */
#define TYPE_9_LSA 1
#define TYPE_10_LSA 2
#define TYPE_11_LSA 3
#define OSPF_OPAQUE_ALIGNMENT 4
#define TYPE_7_LSA 7
#define LS_EXTERNAL_LSA 1
#define LS_TYPE_11_LSA 1


/*RFC 1765*/
#define OSPF_DEFAULT_EXTERNAL_LSDB_LIMIT -1;
#define OSPF_DEFAULT_EXIT_OVERFLOW_TIMER 0;

/* Traffic Engineering Extension Constants */
#define OSPF_TE_ROUTER_ADDRESS_TLV_TYPE 1
#define OSPF_TE_LINK_TLV_TYPE 2
#define OSPF_TE_SUB_LINK_TYPE 1
#define OSPF_TE_SUB_LINK_ID 2
#define OSPF_TE_SUB_LOCAL_ADDRESS 3
#define OSPF_TE_SUB_REMOTE_ADDRESS 4
#define OSPF_TE_SUB_METRIC 5
#define OSPF_TE_SUB_MAXIMUM_BANDWIDTH 6
#define OSPF_TE_SUB_MAXIMUM_RESERVABLE_BANDWIDTH 7
#define OSPF_TE_SUB_UNRESERVED_BANDWIDTH 8
#define OSPF_TE_SUB_RESOURCE_CLASS_COLOR 9
#define OSPF_TE_LINK_TYPE_PADDING 3

#define OSPF_1K_BLOCKS 1024

#define OSPF_INPUT_TASK_STACK_SIZE (16 * OSPF_1K_BLOCKS)
#define OSPF_TIMER_TASK_STACK_SIZE (12 * OSPF_1K_BLOCKS)
#define OSPF_HELLO_TIMER_TASK_STACK_SIZE (8 * OSPF_1K_BLOCKS)
#define OSPF_RTM_TASK_STACK_SIZE   (12 * OSPF_1K_BLOCKS)
#if defined __OSPF_MIB__
#define OSPF_MAPI_TASK_STACK_SIZE  (8 * OSPF_1K_BLOCKS)
#endif 

#define OSPF_TIMER_TASK_PRIORITY        100
#define OSPF_HELLO_TIMER_TASK_PRIORITY  90
#define OSPF_INPUT_TASK_PRIORITY        100
#define OSPF_RTM_TASK_PRIORITY          90
#if defined __OSPF_MIB__
#define OSPF_MAPI_TASK_PRIORITY         150
#endif 

#define OSPFV3_TASK_NAME_SIZE   11 /* 10 chars + null terminator */

/* SPR 83418 -- Begin */
#define OSPF_EXTERNAL_METRIC_TYPE_1 1
#define OSPF_EXTERNAL_METRIC_TYPE_2 2
#define OSPF_EXTERNAL_DEFAULT_METRIC 20
/* SPR 83418 -- End */

/* SPR 85432 -- Begin */
#define OSPF_CLEANUP_INTERVAL  (60 * 30)    /* 30 minutes */
#define MAX_EXTERNAL_ROUTES_PROCESSED 250
/* SPR 85432 -- End */

/* Size of maximum ospf pkt to be sent by ospf_vx_ip_output().
   If you changed this const make sure the calling task has enough stack
   size. */
#define OSPF_MAX_PKT_SIZE           MAXIMUM_ETHERNET_DATA_SIZE

#ifdef INCLUDE_TMS_MODS
#define OSPF_MAXIMUM_FILE_SIZE          512
#else
#define OSPF_MAXIMUM_FILE_SIZE          4000
#endif /* INCLUDE_TMS_MODS */

#define OSPF_RAMDRIVE_VOLUME_NAME       "DEV1:"
#define OSPF_CONFIGURATION_FILE_NAME    "config"

/* raw socket receive buffer size */
#ifndef OSPF_RS_RCV_BUFFER
#define OSPF_RS_RCV_BUFFER              (8 * 1024)
#endif

/* routing socket receive buffer size */
#ifndef OSPF_RAW_RCV_BUFFER
#define OSPF_RAW_RCV_BUFFER             (64 * 1024)
#endif

#define RTM_OSPF_ROUTE_WEIGHT 110

#define OSPF_TASK_NAME_SIZE   11 /* 10 chars + null terminator */

#endif /* _OSPF_CONSTANTS_H_ */

