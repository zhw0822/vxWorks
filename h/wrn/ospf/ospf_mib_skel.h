/******************************************************************************
 ******************************************************************************
 **** This file was automatically generated by Epilogue Technology's
 **** Emissary SNMP MIB Compiler, version 9.1.
 **** This file was generated using the -skel.h switch.
 **** 
 **** This file contains declarations of stub functions to aid in building
 **** the agent MIB interface.  It declares all the MIB get, set, test and
 **** next method routines that you will need to supply.  You'll probably
 **** want to FORCE-INCLUDE this file when using the -mib.c output mode.
 **** 
 **** To generate the skeletal stub functions themselves, run Emissary
 **** with the -skel switch.
 **** 
 **** YOU MAY MODIFY THIS FILE BUT BEWARE ACCIDENTALLY OVERWRITING IT
 **** BY REGENERATING IT WITH THE MIB COMPILER.
 **** 
 **** Last build date: Tue Oct 16 11:22:24 2001
 **** from files:
 ****  rfc1213.mib, rfc1850.mi2, rfc1850.ctl
 ******************************************************************************
 ******************************************************************************
 */

void ospfGeneralGroup_get(OIDC_T, int, OIDC_T*, SNMP_PKT_T*, VB_T*);
void ospfGeneralGroup_set(OIDC_T, int, OIDC_T*, SNMP_PKT_T*, VB_T*);
void ospfGeneralGroup_test(OIDC_T, int, OIDC_T*, SNMP_PKT_T*, VB_T*);
void ospfGeneralGroup_next(OIDC_T, int, OIDC_T*, SNMP_PKT_T*, VB_T*);
void null_test_async(OIDC_T, int, OIDC_T*, SNMP_PKT_T*, VB_T*);
void ospfAreaEntry_get(OIDC_T, int, OIDC_T*, SNMP_PKT_T*, VB_T*);
void ospfAreaEntry_next(OIDC_T, int, OIDC_T*, SNMP_PKT_T*, VB_T*);
void ospfAreaEntry_set(OIDC_T, int, OIDC_T*, SNMP_PKT_T*, VB_T*);
void ospfAreaEntry_test(OIDC_T, int, OIDC_T*, SNMP_PKT_T*, VB_T*);
void ospfStubAreaEntry_get(OIDC_T, int, OIDC_T*, SNMP_PKT_T*, VB_T*);
void ospfStubAreaEntry_next(OIDC_T, int, OIDC_T*, SNMP_PKT_T*, VB_T*);
void ospfStubAreaEntry_set(OIDC_T, int, OIDC_T*, SNMP_PKT_T*, VB_T*);
void ospfStubAreaEntry_test(OIDC_T, int, OIDC_T*, SNMP_PKT_T*, VB_T*);
void ospfLsdbEntry_get(OIDC_T, int, OIDC_T*, SNMP_PKT_T*, VB_T*);
void ospfLsdbEntry_next(OIDC_T, int, OIDC_T*, SNMP_PKT_T*, VB_T*);
void ospfHostEntry_get(OIDC_T, int, OIDC_T*, SNMP_PKT_T*, VB_T*);
void ospfHostEntry_next(OIDC_T, int, OIDC_T*, SNMP_PKT_T*, VB_T*);
void ospfHostEntry_set(OIDC_T, int, OIDC_T*, SNMP_PKT_T*, VB_T*);
void ospfHostEntry_test(OIDC_T, int, OIDC_T*, SNMP_PKT_T*, VB_T*);
void ospfIfEntry_get(OIDC_T, int, OIDC_T*, SNMP_PKT_T*, VB_T*);
void ospfIfEntry_next(OIDC_T, int, OIDC_T*, SNMP_PKT_T*, VB_T*);
void ospfIfEntry_set(OIDC_T, int, OIDC_T*, SNMP_PKT_T*, VB_T*);
void ospfIfEntry_test(OIDC_T, int, OIDC_T*, SNMP_PKT_T*, VB_T*);
void ospfIfMetricEntry_get(OIDC_T, int, OIDC_T*, SNMP_PKT_T*, VB_T*);
void ospfIfMetricEntry_next(OIDC_T, int, OIDC_T*, SNMP_PKT_T*, VB_T*);
void ospfIfMetricEntry_set(OIDC_T, int, OIDC_T*, SNMP_PKT_T*, VB_T*);
void ospfIfMetricEntry_test(OIDC_T, int, OIDC_T*, SNMP_PKT_T*, VB_T*);
void ospfVirtIfEntry_get(OIDC_T, int, OIDC_T*, SNMP_PKT_T*, VB_T*);
void ospfVirtIfEntry_next(OIDC_T, int, OIDC_T*, SNMP_PKT_T*, VB_T*);
void ospfVirtIfEntry_set(OIDC_T, int, OIDC_T*, SNMP_PKT_T*, VB_T*);
void ospfVirtIfEntry_test(OIDC_T, int, OIDC_T*, SNMP_PKT_T*, VB_T*);
void ospfNbrEntry_get(OIDC_T, int, OIDC_T*, SNMP_PKT_T*, VB_T*);
void ospfNbrEntry_next(OIDC_T, int, OIDC_T*, SNMP_PKT_T*, VB_T*);
void ospfNbrEntry_set(OIDC_T, int, OIDC_T*, SNMP_PKT_T*, VB_T*);
void ospfNbrEntry_test(OIDC_T, int, OIDC_T*, SNMP_PKT_T*, VB_T*);
void ospfVirtNbrEntry_get(OIDC_T, int, OIDC_T*, SNMP_PKT_T*, VB_T*);
void ospfVirtNbrEntry_next(OIDC_T, int, OIDC_T*, SNMP_PKT_T*, VB_T*);
void ospfExtLsdbEntry_get(OIDC_T, int, OIDC_T*, SNMP_PKT_T*, VB_T*);
void ospfExtLsdbEntry_next(OIDC_T, int, OIDC_T*, SNMP_PKT_T*, VB_T*);
void ospfAreaAggregateEntry_get(OIDC_T, int, OIDC_T*, SNMP_PKT_T*, VB_T*);
void ospfAreaAggregateEntry_next(OIDC_T, int, OIDC_T*, SNMP_PKT_T*, VB_T*);
void ospfAreaAggregateEntry_set(OIDC_T, int, OIDC_T*, SNMP_PKT_T*, VB_T*);
void ospfAreaAggregateEntry_test(OIDC_T, int, OIDC_T*, SNMP_PKT_T*, VB_T*);
