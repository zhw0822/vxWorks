/* ospf_mib_wrn_helper.h - header file for WindNet OSPF Enterprise MIB  */

/* Copyright 1998-2003 Wind River, Inc. */
#include "copyright_wrs.h"

/*
modification history
--------------------
03l,07jan05,xli porting external route redistribution feature from ospf2.3.1
01v,29jan04,ram NBMA, PTMP, & Unnumbered modifications
01u,08dec03,agi merged fixes from OSPF 2.1 patch branch, updated copyright

01t,28may03,kc  Fixed SPR#88863 - Added wrnOspfRedistributionTable support.
01s,27may03,kc  Fixed wrnOspf2Mapi_if_create() prototype. All defines should 
                appear after the enumerations.
01r,27may03,kc  Fixed SPR#88309 - added asBdrStatusChanged argument to the
                mApi2Ospf_configRedistributeOpts() prototype.
01q,19nov02,mwv Merge TMS code SPR 84284
01p,09aug02,kc  Added createdAs member to mApiWrnOspfIf_t structure.
01o,08may03,kc  Fixed SPR#88309 - added asBdrStatusChanged argument to the
                mApi2Ospf_configRedistributeOpts() prototype.
01n,18apr02,kc  Fixed SPR #74432 - Added mApi2Ospf_configRedistributeOpts() prototype.
01m,15feb02,kc  Merged changes for wrnOspfIfTable support for unnumbered interface. 
01l,04feb02,kc  Added wrnOspfRedistributeDefaultRoutes, wrnOspfRedistributeStaticRoutes,
                wrnOspfRedistributeRIPRoutes and wrnOspfRedistributeBGPRoutes scalar
                objects enumeration values and definitions.
01k,16jan02,kc  Added prototypes for wrnOspf2Mapi_lsdb_create(),
                wrnOspf2Mapi_localLsdb_create() and wrnOspf2Mapi_extLsdb_create().
01j,17dec01,kc  Added wrnOspf_mApi_flushAllLsas() prototype.
01i,14dec01,kc  Added wrnOspf_mApi_areaDelete() prototype.
01h,14dec01,kc  Added resetAllToDefault boolean flag argument to 
                wrnOspf_mApi_initGeneralGroup().
01g,28nov01,kc  Changed wrnOspfLsdbAdvertisement, wrnOspfLocalLsdbAdvertisement and
                wrnOspfExtLsdbAdvertisement from uchar_t to char. 
01f,23oct01,kc  Changed wrnOspfLsdbAge, wrnOspfLsdbChecksum, wrnOspfLocalLsdbAge,
                wrnOspfLocalLsdbChecksum, wrnOspfExtLsdbAge and wrnOspfExtLsdbChecksum 
                from ushort_t to ulong_t.
01e,21oct01,kc  Added mApi2Ospf_configWrnGenGroup() prototype.
01d,16oct01,kc  Added the missing AVL_NODE member to each mApiWrnOspfXXX_t.
01c,13oct01,kc  Added various wrnOspf2Mapi_xxx_delete() prototypes.
01b,13oct01,kc  Added various wrnOspf2Mapi_xxx_update() prototypes.
01a,15aug01,kc  Initial file creation.
*/

/*
DESCRIPTION
This file defines the WindNet OSPF Enterprise MIB specific Management Interface
Local ID enumerations, the MIB object enumerations, the MIB object sizes and 
some function prototypes.

INCLUDE FILES: N/A
*/

#ifndef __INCospf_mib_wrn_helperh
#define __INCospf_mib_wrn_helperh

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/* Defines */

/*****************************************************************************
* Management Interface Local ID enumerations for WindNet OSPF Enterprise MIB.
*/
typedef enum {
    mApiWrnOspfRFC1583Compatibility = 1,
    mApiWrnOspfOpaqueLsaSupport,
    mApiWrnOspfOriginateNewOpaqueLsas,
    mApiWrnOspfRxNewOpaqueLsas,
    mApiWrnOspfType9LsaCount,
    mApiWrnOspfType9LsaCksumSum,
    mApiWrnOspfType11LsaCount,
    mApiWrnOspfType11LsaCksumSum,
    mApiWrnOspfRedistributeDefaultRoutes,
    mApiWrnOspfRedistributeStaticRoutes,
    mApiWrnOspfRedistributeRIPRoutes,
    mApiWrnOspfRedistributeBGPRoutes,
    mApiWrnOspfRedistributeLocalRoutes,
    mApiWrnOspfPtpRouterLsaOption,
    mApiWrnOspfAreaId,                  /* mApiWrnOspf_t enumeration value = 15 */
    mApiWrnOspfAreaType10LsaCount,
    mApiWrnOspfAreaType10LsaCksumSum,
    mApiWrnOspfLsdbAreaId,              /* mApiWrnOspf_t enumeration value = 18 */
    mApiWrnOspfLsdbType,
    mApiWrnOspfLsdbLsid,
    mApiWrnOspfLsdbRouterId,
    mApiWrnOspfLsdbSequence,
    mApiWrnOspfLsdbAge,
    mApiWrnOspfLsdbChecksum,
    mApiWrnOspfLsdbAdvertisement,
    mApiWrnOspfLocalLsdbAreaId,         /* mApiWrnOspf_t enumeration value = 26 */
    mApiWrnOspfLocalLsdbIpAddress,
    mApiWrnOspfLocalLsdbType,
    mApiWrnOspfLocalLsdbLsid,
    mApiWrnOspfLocalLsdbRouterId,
    mApiWrnOspfLocalLsdbSequence,
    mApiWrnOspfLocalLsdbAge,
    mApiWrnOspfLocalLsdbChecksum,
    mApiWrnOspfLocalLsdbAdvertisement,
    mApiWrnOspfExtLsdbType,             /* mApiWrnOspf_t enumeration value = 35 */
    mApiWrnOspfExtLsdbLsid,
    mApiWrnOspfExtLsdbRouterId,
    mApiWrnOspfExtLsdbSequence,
    mApiWrnOspfExtLsdbAge,
    mApiWrnOspfExtLsdbChecksum,
    mApiWrnOspfExtLsdbAdvertisement,
    mApiWrnOspfIfDstIpAddress,          /* mApiWrnOspf_t enumeration value = 42 */ 
    mApiWrnOspfIfIndex,                                 
    mApiWrnOspfIfStatus,                              
    mApiWrnOspfRedistributionRouteType, /* mApiWrnOSPF_t enumeration value = 45 */
    mApiWrnOspfRedistributionSubnet,
    mApiWrnOspfRedistributionMask,
    mApiWrnOspfRedistributionType,
    mApiWrnOspfRedistributionMetric,
    mApiWrnOspfRedistributionStatus,
    mApiWrnOspfLocalIfPassive,          /* mApiWrnOSPF_t enumeration value = 51 */
    mApiWrnOspfToIpRouteSubnet,         /* mApiWrnOspf_t enumberation value = 52 */
    mApiWrnOspfToIpRouteMask,
    mApiWrnOspfToIpRouteWeight,
    mApiWrnOspfToIpRouteStatus,
    mApiWrnOspfMaxLocalIds              /* 56 - Number of objects in wrnOspf */
} mApiWrnOspf_t;

/****************************************************************************
* Management Interface WindNet OSPF Enterprise MIB object enumerations.
*/

typedef enum {
    EmApiWrnOspfLsdbType_areaOpaqueLink = 10,
    EmApiWrnOspfLocalLsdbType_localOpaqueLink = 9,
    EmApiWrnOspfExtLsdbType_asOpaqueLink = 11
} mApiWrnOspfOpaqueType_t;

typedef enum {
    EmApiWrnOspfPtpRouterLsaOption_1 = 1,
    EmApiWrnOspfPtpRouterLsaOption_2 = 2

} mApiWrnOspfPtpRouterLsaOption_t;

typedef enum {
    EwrnOspfRedistribRouteType_default = 1,
    EwrnOspfRedistribRouteType_static = 2,
    EwrnOspfRedistribRouteType_rip = 3,
    EwrnOspfRedistribRouteType_bgp     = 4,
    EwrnOspfRedistribRouteType_local   = 5
} mApiWrnOspfRedistribRouteType_t;

typedef enum {
    EwrnOspfRedistribType_externalType1 = 1,
    EwrnOspfRedistribType_externalType2 = 2
} mApiWrnOspfRedistribType_t;

/****************************************************************************
* Management Interface WindNet OSPF Enterprise MIB Definitions.
*/
/* the following defines provides the number of sub-identifier (the instance length)
 * of each table in the WRN OSPF Enterprise MIB.
 */
#define MAX_OSPF_WEIGHT_VALUE                     255L
#define OSPF_LOCAL_LSDB_INSTANCE_LEN              17
#define OSPF_WRN_INTF_INSTANCE_LEN                4
#define OSPF_WRN_REDISTRIBUTION_INSTANCE_LEN      9

#define OSPF_WRN_TO_IP_ROUTE_INSTACE_LEN          8
/* WRN-OSPF Enterprise MIB default values */
#define DEFVAL_mApiWrnOspfRedistribType           EwrnOspfRedistribType_externalType2
#define DEFVAL_mApiWrnOspfRedistribMetric         20
#define DEFVAL_mApiWrnOspfToIpRouteWeight         110

/****************************************************************************
* Management Interface WindNet OSPF Enterprise MIB Data Structure.
*/
typedef struct mApiWrnOspfGenGroup
{
    mApiOspfTrueValue_t  wrnOspfRFC1583Compatibility;       /* read-write */
    mApiOspfTrueValue_t  wrnOspfOpaqueLsaSupport;           /* read-write */
    ulong_t              wrnOspfOriginateNewOpaqueLsas;     /* read-only */
    ulong_t              wrnOspfRxNewOpaqueLsas;            /* read-only */
    ulong_t              wrnOspfType9LsaCount;              /* read-only */
    ulong_t              wrnOspfType9LsaCksumSum;           /* read-only */
    ulong_t              wrnOspfType11LsaCount;             /* read-only */
    ulong_t              wrnOspfType11LsaCksumSum;          /* read-only */
    mApiOspfTrueValue_t  wrnOspfRedistributeDefaultRoutes;  /* read-write */
    mApiOspfTrueValue_t  wrnOspfRedistributeStaticRoutes;   /* read-write */
    mApiOspfTrueValue_t  wrnOspfRedistributeRIPRoutes;      /* read-write */
    mApiOspfTrueValue_t  wrnOspfRedistributeBGPRoutes;      /* read-write */
    mApiOspfTrueValue_t  wrnOspfRedistributeLocalRoutes;    /* read-write */
    ushort_t             wrnOspfPtpRouterLsaOption;         /* read-write */
} mApiWrnOspfGenGroup_t;

/* Type-10 Opaque LSA cumulative statistics contained in an Area */
typedef struct mApiWrnOspfArea
{
    AVL_NODE   node;                             /* AVL Tree node */
    ulong_t    wrnOspfAreaId;                    /* index object, read-only */
    ulong_t    wrnOspfAreaType10LsaCount;        /* read-only */
    ulong_t    wrnOspfAreaType10LsaCksumSum;     /* read-only */
} mApiWrnOspfArea_t;

/* Type-10 Opaque LSA, have an area-local scope */
typedef struct mApiWrnOspfLsdb
{
    AVL_NODE                 node;                        /* AVL Tree node */
    ulong_t                  wrnOspfLsdbAreaId;           /* index object, read-only */
    mApiWrnOspfOpaqueType_t  wrnOspfLsdbType;             /* index object, read-only */
    ulong_t                  wrnOspfLsdbLsid;             /* index object, read-only */
    ulong_t                  wrnOspfLsdbRouterId;         /* index object, read-only */
    long                     wrnOspfLsdbSequence;         /* read-only */
    ulong_t                  wrnOspfLsdbAge;              /* read-only */
    ulong_t                  wrnOspfLsdbChecksum;         /* read-only */
    char                     *wrnOspfLsdbAdvertisement;   /* read-only */
    ushort_t                 lsdbLen;    /* length of wrnOspfLsdbAdvertisement */
    
} mApiWrnOspfLsdb_t;

/* Type-9 Opaque LSA, have a link-local scope */
typedef struct mApiWrnOspfLocalLsdb
{
    AVL_NODE                  node;                        /* AVL Tree node */
    ulong_t                   wrnOspfLocalLsdbAreaId;      /* index object, read-only */
    ulong_t                   wrnOspfLocalLsdbIpAddress;   /* index object, read-only */
    mApiWrnOspfOpaqueType_t   wrnOspfLocalLsdbType;        /* index object, read-only */
    ulong_t                   wrnOspfLocalLsdbLsid;        /* index object, read-only */
    ulong_t                   wrnOspfLocalLsdbRouterId;    /* index object, read-only */
    long                      wrnOspfLocalLsdbSequence;    /* read-only */
    ulong_t                   wrnOspfLocalLsdbAge;         /* read-only */
    ulong_t                   wrnOspfLocalLsdbChecksum;    /* read-only */
    char                      *wrnOspfLocalLsdbAdvertisement;  /* read-only */
    ushort_t                  lsdbLen;    /* length of wrnOspfLocalLsdbAdvertisement */
} mApiWrnOspfLocalLsdb_t;

/* Type-11 Opaque LSA, have an Autonomous system-wide scope */
typedef struct mApiWrnOspfExtLsdb
{
    AVL_NODE                  node;                         /* AVL Tree node */
    mApiWrnOspfOpaqueType_t   wrnOspfExtLsdbType;           /* index object, read-only */
    ulong_t                   wrnOspfExtLsdbLsid;           /* index object, read-only */
    ulong_t                   wrnOspfExtLsdbRouterId;       /* index object, read-only */
    long                      wrnOspfExtLsdbSequence;       /* read-only */
    ulong_t                   wrnOspfExtLsdbAge;            /* read-only */
    ulong_t                   wrnOspfExtLsdbChecksum;       /* read-only */
    char                      *wrnOspfExtLsdbAdvertisement; /* read-only */
    ushort_t                  lsdbLen;      /* length of wrnOspfExtLsdbAdvertisement */
} mApiWrnOspfExtLsdb_t;

/* objects associated with wrnOspfIfTable */
typedef struct mApiWrnOspfIf
{
    AVL_NODE               node;                      /* AVL Tree node */
    rsAction_t             createdAs;               /* the method this row is created */
    ulong_t                wrnOspfIfDstIpAddress;     /* index object, read-only */
    ulong_t                wrnOspfIfIndex;            /* read-only */
    mApiOspfRowStatus_t    wrnOspfIfStatus;           /* read-create */
} mApiWrnOspfIf_t;

/* objects associated with wrnOspfRedistributionTable */
typedef struct mApiWrnOspfRedistribution
{
    AVL_NODE               node;                      /* AVL Tree node */
    rsAction_t             createdAs;               /* the method this row is created */
    mApiWrnOspfRedistribRouteType_t wrnOspfRedistribRouteType;  /* index object, read-only */
    ulong_t                wrnOspfRedistribSubnet;  /* index object, read-only */
    ulong_t                wrnOspfRedistribMask;    /* index object, read-only */
    mApiWrnOspfRedistribType_t wrnOspfRedistribType; /* read-create */
    ulong_t                wrnOspfRedistribMetric;  /* read-create */
    mApiOspfRowStatus_t    wrnOspfRedistribStatus;  /* read-create */
} mApiWrnOspfRedistrib_t;
typedef struct mApiWrnOspf2IpRoute
{
    AVL_NODE               node;                      /* AVL Tree node */
    rsAction_t             createdAs;               /* the method this row is created */
    ulong_t                wrnOspfToIpRouteSubnet;  /* index object, read-only */
    ulong_t                wrnOspfToIpRouteMask;    /* index object, read-only */
    ulong_t                wrnOspfToIpRouteWeight;  /* read-create */
    mApiOspfRowStatus_t    wrnOspfToIpRouteStatus;  /* read-create */
    BOOL                   searchIpRouteTbl;  /* if set, search IP Routing Table */
} mApiWrnOspf2IpRoute_t;

/****************************************************************************
* Management Interface WindNet OSPF Enterprise MIB Function Prototypes.
*/

/* wrnOspfGeneralGroup helper routines */
IMPORT STATUS wrnOspf_mApi_globalParmGet( mApiWrnOspfGenGroup_t *thisGenGroup, 
                                          mApiRequest_t *pRequest,  
                                          mApiObject_t *pObject );
IMPORT STATUS wrnOspf_mApi_globalParmSet( mApiRequest_t *pRequest, mApiObject_t *pObject,
                                           mApiReqType_t reqType );
IMPORT STATUS mApi2Ospf_configWrnGenGroup( void *pGenParams );
IMPORT void mApi2Ospf_configRedistributeOpts( void *pGenParams, BOOL asBdrStatusChanged );

/* wrnOspfAreaTable helper routines */
IMPORT STATUS wrnOspf_mApi_areaHelperGet( void *pRow, mApiRequest_t *pRequest, 
                                          mApiObject_t *pObject );
IMPORT void wrnOspf2Mapi_area_update( void *pOspfReqBuf );

IMPORT void wrnOspf_mApi_areaDelete( ulong_t wrnOspfAreaId );

/* clean up routines for wrn-ospf enterprise mib */
IMPORT void wrnOspf_mApi_flushAllLsas( BOOL resetAllToDefault );

/* wrnOspfLasbTable helper routine */
IMPORT STATUS wrnOspf_mApi_lsdbHelperGet( void *pRow, mApiRequest_t *pRequest, 
                                          mApiObject_t *pObject );
IMPORT void wrnOspf2Mapi_lsdb_update( void *pOspf2MapiReqBuf );
IMPORT void wrnOspf2Mapi_lsdb_create( void *pOspf2MapiReqBuf );
IMPORT void wrnOspf2Mapi_lsdb_delete( void *pOspf2MapiReqBuf );

/* wrnOspfLocalLasbTable helper routine */
IMPORT STATUS wrnOspf_mApi_localLsdbHelperGet( void *pRow, mApiRequest_t *pRequest, 
                                               mApiObject_t *pObject );
IMPORT void wrnOspf2Mapi_localLsdb_update( void *pOspf2MapiReqBuf );
IMPORT void wrnOspf2Mapi_localLsdb_create( void *pOspf2MapiReqBuf );
IMPORT void wrnOspf2Mapi_localLsdb_delete( void *pOspf2MapiReqBuf );

/* wrnOspfExtLsdbTable helper routines */
IMPORT STATUS wrnOspf_mApi_extLsdbHelperGet( void *pRow, mApiRequest_t *pRequest,
                                             mApiObject_t *pObject );
IMPORT void wrnOspf2Mapi_extLsdb_update( void *pOspf2MapiReqBuf );
IMPORT void wrnOspf2Mapi_extLsdb_create( void *pOspf2MapiReqBuf );
IMPORT void wrnOspf2Mapi_extLsdb_delete( void *pOspf2MapiReqBuf );

/* wrnOspfIfTable helper routines */
IMPORT STATUS wrnOspf_mApi_ifHelperGet( void *pRow, mApiRequest_t *pRequest, 
                                        mApiObject_t *pObject );        
IMPORT STATUS wrnOspf_mApi_ifHelperSet( mApiRequest_t *pRequest,        
                                        rsRequest_t rsReqType );        
IMPORT void wrnOspf2Mapi_if_create( void *pOspf2MapiReqBuf );

/* wrnOspfRedistributionTable helper routines */
IMPORT STATUS wrnOspf_mApi_redistribHelperGet( void *pRow, mApiRequest_t *pRequest, 
                                               mApiObject_t *pObject );
IMPORT STATUS wrnOspf_mApi_redistribHelperSet( mApiRequest_t *pRequest, 
                                               rsRequest_t rsReqType );
IMPORT void wrnOspf2Mapi_redistrib_create( void *pOspf2MapiReqBuf );

IMPORT STATUS wrnOspf_mApi_ospf2IpRouteHelperGet( void *pRow, mApiRequest_t *pRequest, 
                                                  mApiObject_t *pObject );
IMPORT STATUS wrnOspf_mApi_ospf2IpRouteHelperSet( mApiRequest_t *pRequest, 
                                                  rsRequest_t rsReqType );
IMPORT void wrnOspf2Mapi_ospf2IpRoute_create( void *pOspf2MapiReqBuf );
/* wrn-ospf mib specific initialization routines */
IMPORT STATUS wrnOspf_mApi_initAvlTree( void );
IMPORT void wrnOspf_mApi_initGeneralGroup( BOOL resetAllToDefault );
IMPORT STATUS wrnOspf_mApi_initRsLib( void );
IMPORT STATUS wrnOspf_mApi_configurationGet( void );
IMPORT STATUS wrnOspf_mApi_localIfHelperSet ( 
    mApiRequest_t *pRequest, rsRequest_t rsReqType );
IMPORT STATUS wrnOspf_mApi_localIfHelperGet ( 
    void *pRow, mApiRequest_t *pRequest, mApiObject_t *pObject );

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __INCospf_mib_wrn_helperh */
