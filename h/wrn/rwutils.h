/*
 * $Log:: /Tornado/target/h/wrn/rwutils.h                                                       $
 *
 * 6     9/20/00 4:31p Rob
 * Created stristr (case-insensitive sub-string compare) function.
 * Removed prototype for non-existent x86_normalize function.
 *
 * 3     3/23/00 6:06p Rob
 * Now uses Tornado _BYTE_ORDER predefined macro for endian-specific code.
 *
 * 2     3/13/00 7:55p Rob
 * Now defines big-endian empty macros for swap, swap_long, etc.
 * Now declares strlwr() always (no longer dependant on __VxWORKS__).
 *
 * 1     2/09/00 4:42p Admin
 *
 * 2     8/26/99 7:42p Alex
 *
 * 1     8/25/99 7:01p Alex
 *
 * 1     5/26/99 9:53a Alex
 *
 * 1     4/14/99 5:10p Rajive
 * Initial vesrion of Next Gen RouterWare Source code
 *
 * 1     1/18/99 5:10p Mahesh
 *
 * 1     1/18/99 4:24p Mahesh
 *
 * 11    12/28/98 9:03a Nishit
 * Removed functions (de)serialize_ulong/short
 *
 * 10    12/15/98 6:04p Mahesh
 * Added prototypes/definitions for new serialize/deserialize byte functions.
 *
 * 9     12/03/98 6:35p Rajive
 * Changed __SNMP__ to __SNMP_FWD_DECL__
 *
 * 7     4/30/98 1:16p Rajive
 * Overwrote with INCLUDE v4.2.1
 *
 * 3     4/27/98 11:42a Release Engineer
 * Additions into include for L2TP and RTM modules.
 * INCLUDE v4.2.1
 *
 * 1     2/13/98 12:18a Release Engineer
 * code cleanup, code style changes, linted, system level test
 * INCLUDE v4.2.0
 *
 * 2     3/27/96 7:08p Ross
 * Initial check-in with source safe.
*/
/************************************************************************/
/*	Copyright (C) 1989 - 1998 RouterWare, Inc										*/
/*	Unpublished - rights reserved under the Copyright Laws of the			*/
/*	United States.  Use, duplication, or disclosure by the 					*/
/*	Government is subject to restrictions as set forth in 					*/
/*	subparagraph (c)(1)(ii) of the Rights in Technical Data and 			*/
/*	Computer Software clause at 252.227-7013.										*/
/*	RouterWare, Inc., 3961 MacArthur Blvd. Suite 212, Newport Beach Ca   */
/************************************************************************/
#ifndef _ROUTILS_H_
#define _ROUTILS_H_

#define USE_RWOS_MESSAGE_DIGEST

#ifdef USE_RWOS_MESSAGE_DIGEST
enum MD_ALGORITHM
{
	MD4 = 0x04,
	MD5 = 0x05
};
#endif

/* convutls.c */

#if (_BYTE_ORDER == _LITTLE_ENDIAN)
	USHORT host_to_net_short (USHORT host_order_short_value);
	USHORT net_to_host_short (USHORT net_order_short_value);
	ULONG host_to_net_long (ULONG host_order_long_value);
	ULONG net_to_host_long (ULONG net_order_long_value);
	USHORT swap (USHORT ushort_to_swap_bytes_with);
	ULONG swap_long (ULONG ulong_to_swap_bytes_with);
#else
	#define	BIG_ENDIAN_CONSTANT(ushort) ushort
	#define host_to_net_short(a) (a)
	#define net_to_host_short(a) (a)
	#define host_to_net_long(a) (a)
	#define net_to_host_long(a) (a)
	#define	swap(a) (a)
	#define	swap_long(a) (a)
#endif

/* miscutls.c */

enum SORT_RETURN compare_mac_addresses (void *sptr_address_1,void *sptr_address_2);

char *convert_ip_address_to_dot_format (char *cptr_array_to_store_dot_format_address, ULONG ip_address);

char *strlwr (char *cptr_string_to_lower_case);

char *stristr (char *str, char *find);

/* listutls.c */

void add_entry_to_list (LINK *sptr_link, LINK *sptr_link_to_add);
void *get_entry_from_list (LINK *sptr_link);
void delete_entry_from_list (LINK *sptr_list_link, LINK *sptr_link_to_delete);
void add_entry_to_front_of_list (LINK *sptr_link, LINK *sptr_link_to_add);
void insert_entry_in_list (LINK *sptr_link, LINK *sptr_link_to_add, LINK *sptr_link_after_which_to_add);
void *get_pointer_to_first_entry_in_list (LINK *sptr_link);
void *get_pointer_to_next_entry_in_list (LINK *sptr_current_entry);
void *get_pointer_to_last_entry_in_list (LINK *sptr_link);
void *get_pointer_to_previous_entry_in_list (LINK *sptr_current_entry);
void free_list (LINK *sptr_list, void (*fptr_free) (void *vptr_data_to_free));
void *get_entry_from_list_using_index (LINK *sptr_list,USHORT element_index_to_find);
/*
 * The following functions have not been implemented yet
 *
 *	void *get_last_entry_from_list (LINK *sptr_link);
 *
 *	void add_entry_to_single_list (LINK *sptr_link,SINGLE_LINK *sptr_link_to_add);
 *	void add_entry_to_front_of_single_list (LINK *sptr_link,SINGLE_LINK *sptr_link_to_add);
 *	void *get_entry_from_single_list (LINK *sptr_link);
 *	void delete_entry_from_single_list (LINK *sptr_list_link,SINGLE_LINK *sptr_link_to_delete);
 *	void *get_last_entry_from_single_list (LINK *sptr_link);
 */


/* sortlist.c */

enum TEST add_entry_to_sorted_linked_list (LINKED_LIST_SORT_PARAMETERS *sptr_linked_list_sort_parameters);

/* snmpglbl.c */
#if defined (__SNMP_FWD_DECL__)
	enum TEST process_mib_variable (enum MIB_OPERATION mib_operation, char *cptr_mib_string,
		BYTE *bptr_variable_value, USHORT *usptr_size_of_variable_value, USHORT *usptr_size_of_table_indices,
		ULONG *ulptr_table_indices,  BOOLEAN *eptr_end_of_table, char **ptr_to_cptr_next_variable_name,
		SNMP_TABLE_ENTRY *sptr_first_table_entry);
#else
	enum TEST process_mib_variable (enum MIB_OPERATION mib_operation, char *cptr_mib_string,
		BYTE *bptr_variable_value, USHORT *usptr_size_of_variable_value, USHORT *usptr_size_of_table_indices,
		ULONG *ulptr_table_indices,  BOOLEAN *eptr_end_of_table, char **ptr_to_cptr_next_variable_name,
		void *sptr_first_table_entry);
#endif

/* accesnmp.c */

enum TEST snmp_get_or_set_ulong (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset, ULONG _class, ULONG size,
	void *vptr_mib_value_obtained, USHORT *usptr_length_of_mib_obtained);
enum TEST snmp_get_or_set_ushort (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset, ULONG _class, ULONG size,
	void *vptr_mib_value_obtained, USHORT *usptr_length_of_mib_obtained);
enum TEST snmp_get_or_set_boolean_plus_one (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset, ULONG _class,
	ULONG size, void *vptr_mib_value_obtained, USHORT *usptr_length_of_mib_obtained);
enum TEST snmp_get_or_set_byte (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset, ULONG _class, ULONG size,
	void *vptr_mib_value_obtained, USHORT *usptr_length_of_mib_obtained);
enum TEST snmp_get_or_set_string (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset, ULONG _class, ULONG size,
	void *vptr_mib_value_obtained, USHORT *usptr_length_of_mib_obtained);
enum TEST snmp_get_or_set_mac_address (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset, ULONG _class,
	ULONG size, void *vptr_mib_value_obtained, USHORT *usptr_length_of_mib_obtained);
enum TEST snmp_get_or_set_8_byte_object (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset, ULONG _class,
	ULONG size, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained);
enum TEST snmp_get_or_set_2_byte_object (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset, ULONG _class,
	ULONG size, void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained);
enum TEST snmp_get_port_number (USHORT port_number, enum MIB_OPERATION mib_operation, ULONG offset, ULONG _class, ULONG size,
	void *vptr_mib_value, USHORT *usptr_length_of_mib_obtained);

/* snmpncfg.c */

void *get_mib_access_function (enum TEST (*fptr_mib_access_function) (USHORT port_number, enum MIB_OPERATION mib_operation,
	 ULONG offset_into_class, ULONG class_address, ULONG size_of_port_class, void *vptr_mib_value,
	 USHORT *usptr_length_of_mib_obtained));

/* serialization_utilities.c */

UINT deserialize_byte_contents (BYTE **ptr_to_bptr_location);
UINT deserialize_ushort_contents (USHORT **ptr_to_usptr_location);
ULONG deserialize_ulong_contents (ULONG **ptr_to_ulptr_location);

void deserialize_bit_field (UINT size, UINT *uiptr_start, ...);
void deserialize_short_bit_field (UINT size, USHORT *usptr_start, ...);

void serialize_byte_contents (UINT source, BYTE **ptr_to_bptr_destination);
void serialize_ushort_contents (UINT source, USHORT **ptr_to_usptr_destination);
void serialize_ulong_contents (ULONG source, ULONG **ptr_to_ulptr_destination);

enum TEST serialize_long_bit_field (UINT size, ULONG *ulptr_destination, ...);
enum TEST serialize_short_bit_field (UINT size, USHORT *usptr_destination, ...);
enum TEST serialize_byte_bit_field (UINT size, BYTE *bptr_destination, ...);

/* message_digest.c */

#ifdef USE_RWOS_MESSAGE_DIGEST
void MD_string (BYTE *cptr_string, ULONG length, BYTE *bptr_digest, enum MD_ALGORITHM md_algorithm);
#endif

#endif /* _ROUTILS_H_ */
