/************************************************************************/
/*	Copyright (C) 1999 RouterWare, Inc.     							*/
/*	Unpublished - rights reserved under the Copyright Laws of the		*/
/*	United States.  Use, duplication, or disclosure by the 				*/
/*	Government is subject to restrictions as set forth in 				*/
/*	subparagraph (c)(1)(ii) of the Rights in Technical Data and 		*/
/*	Computer Software clause at 252.227-7013.							*/
/*	RouterWare, Inc., 3961 MacArthur Blvd. Suite 212, Newport Beach, CA	*/
/************************************************************************/ 
#if !defined (__ARP_IF_H__)
#define __ARP_IF_H__
/* ************************************************************************************************************************ */
/* ARP Interface */ 
/* ************************************************************************************************************************ */
typedef void (*FP_ARP_USER_ON_SEND_ARP_PACKET)			(ARP_USER_HANDLE, RW_DEVICE_MESSAGE* p_rw_device_message);

typedef void (*FP_ARP_USER_ON_ADDRESS_RESOLUTION)		(ARP_USER_HANDLE, ARP_RESULT arp_result, IP_ADDRESS, 
																			BYTE* p_mac_address, UINT length);

typedef struct ARP_IF
{
	bool (*fp_arp_if_register)							(struct ARP_IF*, ARP_USER_HANDLE, FP_ARP_USER_ON_ADDRESS_RESOLUTION, 
																	FP_ARP_USER_ON_SEND_ARP_PACKET);
	
	enum ARP_RESULT (*fp_arp_if_get_mac_address)	(struct ARP_IF*, IP_ADDRESS, BYTE* p_buffer, UINT buffer_size);

	bool (*fp_arp_if_receive)							(struct ARP_IF*, RW_DEVICE_MESSAGE*);
	
	bool (*fp_arp_if_add_entry)						(struct ARP_IF* p_arp_if, IP_ADDRESS sender_ip_address, 
																	BYTE* p_sender_mac_address, UINT mac_address_length);
	
	bool (*fp_arp_if_remove_entry)					(struct ARP_IF* p_arp_if, IP_ADDRESS sender_ip_address);								
} ARP_IF;
/* ************************************************************************************************************************ */
#endif /* __ARP_IF_H__ */