/* ospfv3Prototypes.h - OSPFv3 prototypes header file */

/* Copyright 2004 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*
modification history
--------------------
02v,10dec04,tlu OSPF stack decoupling (interface)
02u,06may04,xli ospfv3_init has no params for non FTP
02t,06may04,ram Fix for AVL memory corruption
02s,29apr04,ram Fix compile warnings
02r,22apr04,ram AVL & Memory migration to shared library
02q,14apr04,ram Performance optimization changes
02p,13apr04,ram OSPFv3 Routing Table Hash Modifications
02o,13apr04,ram LSDB AVL modifications
02n,12apr04,ram Memory and AVL modifications
02m,08apr04,ram OSPF v2/v3 coexistance compile
02l,31mar04,dsk Modifications for OSPFv3 ANVL conformance
02k,11mar04,dsk Modifications for OSPFv3 ANVL conformance
02j,16dec03,ram Mods per type 3 & 4 testing
02i,04dec03,ram Mods for LSA type 3 & 4, general modification, and show commands
02h,27nov03,dsk Mods according to integration testing
02g,21nov03,dsk Mods for type 9 LSAs
02f,18nov03,agi fixed compiler warning
02e,18nov03,agi added ospfv3_get_prefix_length_in_bytes() prototype
02d,17nov03,dsk Mods according to integr testing
02c,08nov03,dsk Mods according to integr testing
02b,06nov03,ram Modifications for external routes
02a,06nov03,dsk Mods for type 5 lsas with var length address prefix
01z,04nov03,dsk Modifications for type 8 and type 9 LSAs
01y,31oct03,ram Modifications for mapi, traffic, & show
01x,28oct03,agi updated ospfv3LsdbShow.c prototypes
01w,21oct03,ram Modifications for adjacency establishment
01v,17oct03,ram Modifications for memory corruption, traffic, & debug output
01u,16oct03,dsk added prototypes for new OSPFv3 LSAs origination
01t,10oct03,ram Modifications for LSDB, dynamic config, & NSSA cleanup
01s,07oct03,agi changed ospfv3OriginateASingleSummaryLsa() to
                ospfv3OriginateSummaryLsaForASingleRoutingTableEntry()
01r,07oct03,agi changed ospfv3_get_next_prefix() to
                ospfv3_get_next_prefix_in_prefix_list()
01p,25sep03,ram Second general modifications submission
01o,16sep03,dsk added ospfv3_print_prefix
01n,29jul03,ram Added sockets, processing, and external route changes
01n,29jul03,dsk added prototypes for LSA aggregation
01m,16jul03,dsk added function ospfv3_get_lsdb_index to map hash tables to lsa types
01l,03jun03,agi updated parameter changes in database entry related functions
01k,29may03,dsk added prototypes for new functions:
                for comparing LSAs, routes, prefixes, prefix lists, prefixes, subnets,
                for walking LSA mixed aggregate, for building prefix list of routing table entry,
                for utilities related to routing table computation and lookup,
                for prioritized hello processing (processing of backlog queue)
                for LSA early retransmission filter mechanism (SPR 88619)
                for lookup of IPv6 address within range of prefix,
                for replacing rwos get elapsed time function with vxWorks functions,
                for route updates transmission OSPF to RS via socket
                for generating unique link IDs of LSAs in aggregates,
                cleanup (removed unused functions)
01j,07mar03,dsk added prototypes for find and show functions, also for routing table prototype
01i,21feb03,dsk byte_order changes (store in host byte order)
01h,18feb03,dsk ported from OSPFv2 to OSPFv3 structures
01g,28jan03,agi updated ospfv3_set_intervening_router() prototype
01i,27jan03,agi added ospfv3IPv6AddrToDotFormat() prototype
01h,24jan03,htm added changes for IPv6
01g,23jan03,agi fixed previous submission of older file version
01f,16jan03,agi updated more prototypes,
                removed ospfv3_find_network_LSA() prototype
01e,14jan03,agi changed ospfv3Utils.c prototypes
01d,13jan03,agi propogated SPR#85432, 83418 fixes from OSPFv2
01c,23dec02,agi propogated TMS code merge for SPR#84284
                added ospf_find_router_or_network_routing_table_node()
                added prototypes for ospf_notify_areas_for_abr_change()
                ospf_flush_network_link_advertisement() SPR#84478, 84485, 84486
01b,14nov02,agi first pass at clean-up
01a,23oct02,agi written
*/

#ifndef __INCospfv3Prototypesh
#define __INCospfv3Prototypesh

#ifdef __cplusplus
extern "C" {
#endif

/* includes */

#include        <in.h>
#include        <net/if.h>


/* function prototypes */

/**********************************/
/* ospfv3AcknowledgementReceive.c */
/**********************************/

enum OSPFV3_PACKET_STATE ospfv3_ls_acknowledgement_packet_received
    ( OSPFV3_LS_ACKNOWLEDGEMENT_HEADER *sptr_acknowledgement_packet,
      OSPFV3_NEIGHBOR *sptr_neighbor,
      OSPFV3_INTERFACE *sptr_interface,
      ULONG size_of_packet );



/*******************/
/* ospfv3_checksum.c */
/*******************/

USHORT ospfv3_generate_LS_checksum
    ( void * vptr_data,
      USHORT length_of_data,
      BYTE * bptr_checksum);

STATUS ospfv3_verify_LS_checksum
    ( void * vptr_data,
      USHORT length_of_data /* in network order */,
      USHORT checksum);


/************************/
/*      ospfv3Cfg.c     */
/************************/

void ospfv3_cfg (void);


/************************/
/* ospfv3CfgRoutines.c  */
/************************/

#if defined (__OSPFV3_PASSIVE_INTERFACE__)

STATUS ospfv3SetPassiveMode ( char *commands );

STATUS ospfv3SetPassivePort
    ( char *cptr_ip_address_string, enum OSPFV3_MODE mode );
#endif /* (__OSPFV3_PASSIVE_INTERFACE__) */

STATUS ospfv3SetTotalAreaAddressRange ( ULONG total_number_area_ranges );

void ospfv3_cfg_routines_stub ();


/**************************/
/* ospfv3_configuration.c */
/*************************/

STATUS ospfv3_set_default_values (char *cptr_value_string);

STATUS ospfv3_set_router_id (char *cptr_value_string);

STATUS ospfv3_set_tos_capability (char *cptr_value_string);

STATUS ospfv3_set_asbr (char *cptr_value_string);

STATUS ospfv3_set_multicast (char *cptr_value_string);

STATUS ospfv3_set_number_of_areas (char *cptr_value_string);

STATUS ospfv3_set_area_id (char *cptr_value_string);

STATUS ospfv3_set_number_of_area_address_range (char *cptr_value_string);

STATUS ospfv3_set_area_address_range_address (char *cptr_value_string);

STATUS ospfv3_set_area_address_range_length (char *cptr_value_string);

STATUS ospfv3_set_area_address_range_enable (char *cptr_value_string);

STATUS ospfv3_set_area_address_range_area_id (char *cptr_value_string);

STATUS ospfv3_set_area_external_routing_capability (char *cptr_value_string);

STATUS ospfv3_set_area_stub_inject_default_route (char *cptr_value_string);

STATUS ospfv3_set_area_stub_cost (char *cptr_value_string);

STATUS ospfv3_set_number_of_ports (char *cptr_value_string);

STATUS ospfv3_set_port_enable (char *cptr_value_string);

STATUS ospfv3_set_port_index (char *cptr_value_string);

STATUS ospfv3_set_port_nbma_neighbor_address (char *cptr_value_string);

STATUS ospfv3_set_port_nbma_neighbor_id (char *cptr_value_string);

STATUS ospfv3_set_port_area_id (char *cptr_value_string);

STATUS ospfv3_set_port_type (char *cptr_value_string);

STATUS ospfv3_set_port_transit_area_id (char *cptr_value_string);

STATUS ospfv3_set_port_source_area_id (char *cptr_value_string);

STATUS ospfv3_set_port_router_dead_interval (char *cptr_value_string);

STATUS ospfv3_set_port_transmit_delay (char *cptr_value_string);

STATUS ospfv3_set_port_priority (char *cptr_value_string);

STATUS ospfv3_set_port_cost (char *cptr_value_string);

STATUS ospfv3_set_port_hello_interval (char *cptr_value_string);

STATUS ospfv3_set_port_neighbor_router_id (char *cptr_value_string);

STATUS ospfv3_set_port_poll_interval (char *cptr_value_string);

STATUS ospfv3_set_port_retxmt_interval (char *cptr_value_string);

STATUS ospfv3_set_port_passive (char *cptr_value_string);

STATUS ospfv3_set_port_passive_all (char *cptr_value_string);

STATUS ospfv3_set_printf (char *cptr_value_string);

STATUS ospfv3_set_printf_interface (char *cptr_value_string);

STATUS ospfv3_set_printf_neighbor (char *cptr_value_string);

STATUS ospfv3_set_printf_memory (char *cptr_value_string);

STATUS ospfv3_set_printf_alarm (char *cptr_value_string);

STATUS ospfv3_set_printf_snmp (char *cptr_value_string);

STATUS ospfv3_set_printf_packet (char *cptr_value_string);

STATUS ospfv3_set_printf_hello_packet (char *cptr_value_string);

STATUS ospfv3_set_printf_dd_packet (char *cptr_value_string);

STATUS ospfv3_set_printf_ls_request_packet (char *cptr_value_string);

STATUS ospfv3_set_printf_ls_update_packet (char *cptr_value_string);

STATUS ospfv3_set_printf_ls_ack_packet (char *cptr_value_string);

STATUS ospfv3_set_printf_routing_table (char *cptr_value_string);

STATUS ospfv3_set_printf_debug (char *cptr_value_string);

STATUS ospfv3_set_printf_rtm (char *cptr_value_string);

STATUS ospfv3_set_printf_prologue (char *cptr_value_string);

STATUS ospfv3_set_printf_db_overflow (char *cptr_value_string);

STATUS ospfv3_set_bgp_redistribute_all (char *cptr_value_string);

STATUS ospfv3_set_rip_redistribute_all (char *cptr_value_string);

STATUS ospfv3_set_static_redistribute_all (char *cptr_value_string);

STATUS ospfv3_set_default_redistribute (char *cptr_value_string);

STATUS ospfv3_set_bgp_subnets_to_redistribute (char *cptr_value_string);

STATUS ospfv3_set_rip_subnets_to_redistribute (char *cptr_value_string);

STATUS ospfv3_set_static_subnets_to_redistribute (char *cptr_value_string);

STATUS ospfv3_set_point_to_point_lsa_option (char *cptr_value_string);

#ifdef __OSPFV3_DB_OVERFLOW_SUPPORT__
STATUS ospfv3_set_external_lsdb_limit (char *cptr_value_string);

STATUS ospfv3_set_exit_overflow_interval (char *cptr_value_string);
#endif /*__OSPFV3_DB_OVERFLOW_SUPPORT__*/

STATUS ospfv3_set_max_lsa_deleted (char *cptr_value_string);

#ifdef __UNNUMBERED_LINK__  /* -bt- */
STATUS ospfv3_set_unnumbered_dest_ip(char *cptr_value_string);
#endif  /* __UNNUMBERED_LINK__ -bt- */


/******************/
/* ospfv3_control.c */
/******************/

void ospfv3_halt_ospfv3_router (void);

STATUS ospfv3_control
    ( enum PROTOCOL_CONTROL_OPERATION command,
      ULONG parameter_0,ULONG parameter_1 );

void ospfv3_shut_down_ospfv3_router (void);


/***************************/
/* ospfv3_database_summary.c */
/***************************/

enum OSPFV3_RETURN_FLAGS ospfv3_build_ls_database_summary
    ( OSPFV3_INTERFACE *sptr_interface,OSPFV3_NEIGHBOR *sptr_neighbor);

OSPFV3_LS_DATABASE_SUMMARY *ospfv3_allocate_ls_database_summary
    (OSPFV3_INTERFACE *sptr_interface);

void ospfv3_free_ls_database_summary
    (OSPFV3_LS_DATABASE_SUMMARY *sptr_database_summary);


/*************************************/
/* ospfv3_designated_router_election.c */
/*************************************/

void ospfv3_run_designated_router_election (OSPFV3_INTERFACE *sptr_interface);

/*******************/
/* ospfv3_dijkstra.c */
/*******************/

void ospfv3_run_dijkstra (OSPFV3_AREA_ENTRY *sptr_area);

void ospfv3_bring_up_virtual_links_if_necessary (OSPFV3_AREA_ENTRY *sptr_area);

void ospfv3_set_intervening_router
    ( OSPFV3_SHORTEST_PATH_NODE *sptr_vertex_V,
      OSPFV3_SHORTEST_PATH_NODE *sptr_vertex_W,
      OSPFV3_AREA_ENTRY* sptr_area );


/***************************/
/* ospfv3EntryInitialize.c */
/***************************/

STATUS ospfv3_task_initialize (const char *p_configuration_file);


/*************************************/
/* ospfv3_external_routes.c           */
/*************************************/

void ospfv3_process_external_route_to_ospf ();

void ospfv3_add_exported_route_to_ospf
    ( struct in6_addr *  destination_network,
      ULONG network_mask,
      ULONG metric,
      struct in6_addr *  forwarding_address,
      ULONG tag,
      ULONG proto);

void ospfv3_delete_exported_route_from_ospf
    ( struct in6_addr *destination_network,
      ULONG network_mask,
      ULONG metric,
      struct in6_addr *forwarding_address,
      ULONG tag,
      ULONG proto);

/***********************************/
/* ospfv3ExternalRoutesCalculate.c */
/***********************************/

void ospfv3_calculate_routes_to_external_destinations (void);

void ospfv3_calculate_routes_to_a_single_external_destination
    ( UNION_OSPFV3_LINK_STATE_ADVERTISEMENT *advertisement,
       enum BOOLEAN on_new_external_lsa_received_path );

void ospfv3_invalidate_external_route_entry
    ( OSPFV3_EXTERNAL_LINK_ADVERTISEMENT_HEADER *sptr_external );

/*****************/
/* ospfv3Flood.c */
/*****************/

enum BOOLEAN ospfv3_flood_advertisement_out_some_subset_of_the_routers_interfaces
    (OSPFV3_ROUTER_LINK_ADVERTISEMENT_HEADER *sptr_advertisement,
     OSPFV3_AREA_ENTRY *sptr_area,
     OSPFV3_INTERFACE *sptr_interface,
     OSPFV3_INTERFACE *sptr_interface_advertisement_received_on,
     OSPFV3_NEIGHBOR *sptr_neighbor_advertisement_received_from,
     enum BOOLEAN advertisement_installed );

enum BOOLEAN ospfv3_update_ls_request_list_for_this_adjacency(
    OSPFV3_ROUTER_LINK_ADVERTISEMENT_HEADER *sptr_advertisement,
    OSPFV3_NEIGHBOR *sptr_neighbor);

/*****************/
/* ospfv3Hello.c */
/*****************/

void ospfv3_send_hello
    ( OSPFV3_INTERFACE *sptr_interface,
      OSPFV3_NEIGHBOR *sptr_neighbor,
      enum BOOLEAN goingdown );

enum OSPFV3_PACKET_STATE ospfv3_hello_packet_received
    ( OSPFV3_HELLO_HEADER *sptr_hello,
      OSPFV3_NEIGHBOR *sptr_new_neighbor,
      OSPFV3_INTERFACE *sptr_interface,
      struct in6_addr *source_address,
      ULONG router_id,
      ULONG size_of_packet,
      ULONG cryptographic_sequence_number);

void ospfv3_set_neighbor_fields
    ( OSPFV3_NEIGHBOR *sptr_neighbor,
      ULONG router_id,
      struct in6_addr *address,
      ULONG cryptographic_sequence_number,
      OSPFV3_INTERFACE *sptr_interface,
      USHORT priority,
      UNION_OSPFV3_OPTIONS options,
      enum OSPFV3_NEIGHBOR_STATE state,
      ULONG neighbor_intf_id);


/**************************/
/* ospfv3Initialize.c */
/**************************/

STATUS ospfv3_initialize_router ( ULONG clock_ticks_per_second );

void ospfv3_put_an_interface_on_areas_interface_list
    ( OSPFV3_AREA_ENTRY *sptr_area, OSPFV3_INTERFACE *sptr_interface );

void ospfv3_multicast_group_request
    ( int ifIndex, struct in6_addr *multicastAddr, int request );

STATUS ospfv3_add_entry_to_hosts_list
    ( OSPFV3_INTERFACE *sptr_interface, OSPFV3_IPV6_ADDR_PREFIX_LINKS *sptr_prefix, BOOL dynamic );

STATUS ospfv3_add_entry_to_virtual_link_list
    ( OSPFV3_INTERFACE *sptr_interface, BOOL dynamic );

OSPFV3_AREA_ENTRY* ospfv3_create_pseudo_area_for_virtual_link
    ( OSPFV3_INTERFACE *sptr_interface );

void ospfv3Shutdown ();
void ospfv3Restart ();
void ospfShowOspfTid ();

void ospfv3_initialize_interfaces (void);

/***********************************/
/* ospfv3InterAreaRouteCalculate.c */
/***********************************/

void ospfv3_calculate_inter_area_routes (OSPFV3_AREA_ENTRY *sptr_area);

enum BOOLEAN ospfv3_calculate_inter_area_routes_for_a_single_summary_advertisement
    (OSPFV3_ADVERTISEMENT_NODE *sptr_advertisement_node,
     OSPFV3_AREA_ENTRY *sptr_area,
     enum BOOLEAN new_summary_lsa_received);

void ospfv3_invalidate_inter_area_route_entry
    ( OSPFV3_INTER_AREA_PREFIX_LSA_HEADER *sptr_advertisement_node,
      OSPFV3_AREA_ENTRY *sptr_area );


/********************/
/* ospfv3Interface.c */
/********************/

void ospfv3_bring_up_interface (OSPFV3_INTERFACE *sptr_interface);

void ospfv3_initialize_interface
    ( OSPFV3_INTERFACE *sptr_interface, BOOL dynamic );

STATUS ospfv3_check_if_status ( unsigned short if_index );
STATUS ospfv3_is_if_unnumbered_up ( OSPFV3_INTERFACE *sptr_interface );


/***************************/
/* ospfv3InterfaceEvents.c */
/***************************/

void ospfv3_process_interface_up_event ( OSPFV3_INTERFACE *sptr_interface );

void ospfv3_process_interface_wait_timer_event
    ( OSPFV3_INTERFACE *sptr_interface );

void ospfv3_process_interface_backup_seen_event
    ( OSPFV3_INTERFACE *sptr_interface );

void ospfv3_process_interface_neighbor_change_event
    ( OSPFV3_INTERFACE *sptr_interface );

void ospfv3_process_interface_loop_indication_event
    ( OSPFV3_INTERFACE *sptr_interface );

void ospfv3_process_interface_unloop_indication_event
    ( OSPFV3_INTERFACE *sptr_interface );

void ospfv3_process_interface_down_event (OSPFV3_INTERFACE *sptr_interface);

void ospfv3_reset_interface_variables_and_timers_and_destroy_all_associated_neighbor_connections
    ( OSPFV3_INTERFACE *sptr_interface,
      enum OSPFV3_INTERFACE_EVENT event,
      enum OSPFV3_INTERFACE_STATE new_state );

void ospfv3_interface_address_delete(USHORT if_index, struct in6_addr *if_addr,
                                     BOOL linkLocal);
void ospfv3_interface_address_add(USHORT if_index, struct in6_addr *if_addr,
                                  ULONG if_mask, BOOL linkLocal);
void ospfv3_interface_flag_change(USHORT if_index, ULONG if_flags);

/**********************************/
/* ospfv3InterfaceStateMachine.c */
/**********************************/

void ospfv3_execute_interface_state_machine
    ( enum OSPFV3_INTERFACE_EVENT event,
      enum OSPFV3_INTERFACE_STATE state,
      OSPFV3_INTERFACE *sptr_interface );


/***********************************/
/* ospfv3LinkStateAdvertisements.c */
/***********************************/

void ospfv3_originate_new_instance_of_the_link_state_advertisement
    (OSPFV3_LS_DATABASE_ENTRY *sptr_database_entry,
     OSPFV3_AREA_ENTRY *sptr_area,
     OSPFV3_INTERFACE *sptr_interface,
     enum BOOLEAN first_pass_external);
void ospfv3OriginateRouterLsa (OSPFV3_AREA_ENTRY *sptr_area);
STATUS ospfv3OriginateNetworkLsa (OSPFV3_INTERFACE *sptr_interface);
void ospfv3OriginateSummaryLsa (OSPFV3_AREA_ENTRY *sptr_area);
void ospfv3OriginateSummaryLsaForASingleRoutingTableEntry (
    OSPFV3_ROUTING_TABLE_ENTRY * sptr_routing_table_entry,
    OSPFV3_AREA_ENTRY * sptr_area,
    enum BOOLEAN prematurely_age_advertisement);
void ospfv3OriginateDefaultSummaryLinkIntoArea (OSPFV3_AREA_ENTRY *sptr_area);
void ospfv3OriginateExternalLsa (OSPFV3_EXTERNAL_ROUTE *sptr_external_route);
OSPFV3_ROUTER_LSA_LINK * ospfv3RetrieveRouterLsaInternal (
    enum OSPFV3_SEARCH_TYPE key, ULONG key_value, ULONG area_id);
STATUS ospfv3OriginateLinkLsa (OSPFV3_INTERFACE *  sptr_interface);
STATUS ospfv3OriginateIntraAreaPrefixLsa (OSPFV3_INTERFACE *  sptr_interface);
BOOLEAN ospfv3_verify_if_type9_already_originated(
            OSPFV3_INTRA_AREA_PREFIX_LSA_HEADER *   sptr_type9_lsa,
            OSPFV3_AREA_ENTRY                       *sptr_area,
            OSPFV3_INTERFACE                        *sptr_interface);
BOOLEAN ospfv3_compare_prefixes(UNION_OSPFV3_ADVERTISEMENT_HEADER *   sptr_lsa1,
                                UNION_OSPFV3_ADVERTISEMENT_HEADER *   sptr_lsa2);



/*****************************/
/* ospfv3LinkStateDatabase.c */
/*****************************/

OSPFV3_LS_DATABASE_ENTRY *ospfv3_install_a_new_advertisement_in_the_link_state_database
    (OSPFV3_LS_DATABASE_ENTRY *sptr_old_instance,
     UNION_OSPFV3_LINK_STATE_ADVERTISEMENT *sptr_advertisement,
     OSPFV3_AREA_ENTRY * sptr_area,
     OSPFV3_INTERFACE *sptr_interface,
     STATUS* different_lsa);

OSPFV3_LS_DATABASE_ENTRY * ospfv3_find_originated_lsa
    (
    OSPFV3_AREA_ENTRY *sptr_area,
    OSPFV3_INTERFACE *sptr_interface,
    ULONG id,
    ULONG destination_router_id,
    struct in6_addr *sptr_prefix,
    ULONG prefix_length,
    BYTE_ENUM (OSPFV3_LS_TYPE) type);

OSPFV3_LS_DATABASE_ENTRY * ospfv3_find_lsa
    (OSPFV3_AREA_ENTRY *sptr_area,
     OSPFV3_INTERFACE *sptr_interface,
     ULONG id, ULONG advertising_router,
     BYTE_ENUM(OSPFV3_LS_TYPE) type);

void ospfv3_free_database_entry (OSPFV3_LS_DATABASE_ENTRY *sptr_database_entry);

STATUS ospfv3_check_if_previous_instance_exists
    (OSPFV3_LS_DATABASE_ENTRY *sptr_old_instance);

STATUS ospfv3_check_if_new_advertisement_is_different_from_its_previous_instance (
    UNION_OSPFV3_LINK_STATE_ADVERTISEMENT *sptr_advertisement,
    OSPFV3_LS_DATABASE_ENTRY *sptr_old_instance);

void ospfv3_flush_link_scope_link_state_database (void);

void ospfv3_flush_as_scope_link_state_database (void);

void ospfv3_flush_area_scope_link_state_database (void);

void ospfv3_flush_the_link_state_database_of_self_originated_external_lsa ();

OSPFV3_LS_DATABASE_HEAD * ospfv3FindLsaDatabaseHead (
    OSPFV3_ROUTER_LINK_ADVERTISEMENT_HEADER *   sptr_advertisement,
    OSPFV3_AREA_ENTRY * sptr_area,
    OSPFV3_INTERFACE *sptr_interface);

void ospfv3AggregateLsa (
    OSPFV3_LS_DATABASE_ENTRY *  sptr_database_entry);

void ospfv3DeaggregateLsa (
    OSPFV3_LS_DATABASE_ENTRY *  sptr_database_entry);


OSPFV3_LS_DATABASE_ENTRY * ospfv3_get_first_router_lsa_of_aggregate (
    OSPFV3_AREA_ENTRY *sptr_area, ULONG advertising_router);

OSPFV3_LS_DATABASE_ENTRY * ospfv3_get_first_intra_area_prefix_lsa_of_aggregate (
    OSPFV3_LS_DATABASE_ENTRY *sptr_net_or_rtr_lsa,
    OSPFV3_AREA_ENTRY * sptr_area);

OSPFV3_LS_DATABASE_ENTRY * ospfv3_find_first_intra_area_prefix_lsa(
    OSPFV3_AREA_ENTRY *sptr_area,
    USHORT referenced_ls_type,
    ULONG  referenced_link_state_id,
    ULONG  referenced_adv_router);

OSPFV3_LS_DATABASE_ENTRY * ospfv3_find_first_router_lsa(
    OSPFV3_AREA_ENTRY *sptr_area, ULONG advertising_router);

OSPFV3_LS_DATABASE_ENTRY * ospfv3_find_next_intra_area_prefix_lsa(
    OSPFV3_LS_DATABASE_ENTRY * sptr_database_entry);

OSPFV3_LS_DATABASE_ENTRY * ospfv3_find_next_router_lsa(
    OSPFV3_LS_DATABASE_ENTRY * sptr_lsa);


OSPFV3_LS_DATABASE_ENTRY* ospfv3_get_next_lsa_of_aggregate (
    OSPFV3_LS_DATABASE_ENTRY *sptr_lsa);

OSPFV3_LS_DATABASE_ENTRY * ospfv3_get_first_lsa_of_aggregate(
    OSPFV3_LS_DATABASE_ENTRY *sptr_lsa);

void ospfv3_verify_aggregate(OSPFV3_LS_DATABASE_ENTRY* sptr_lsa);

OSPFV3_LS_DATABASE_ENTRY * ospfv3_find_next_intra_area_prefix_lsa(
    OSPFV3_LS_DATABASE_ENTRY * sptr_lsa);

OSPFV3_LS_DATABASE_ENTRY * ospfv3_find_next_router_lsa(
    OSPFV3_LS_DATABASE_ENTRY * sptr_lsa);

int ospfv3AvlLsdbCompare(AVL_NODE *node,void* key);

/**************************/
/* ospfv3_neighbor_events.c */
/**************************/

void ospfv3_process_neighbor_hello_received_event
    (OSPFV3_INTERFACE *sptr_interface,
     OSPFV3_NEIGHBOR *sptr_neighbor,
     enum OSPFV3_NEIGHBOR_EVENT event);

void ospfv3_process_neighbor_start_event
    (OSPFV3_INTERFACE *sptr_interface,
     OSPFV3_NEIGHBOR *sptr_neighbor,
     enum OSPFV3_NEIGHBOR_EVENT event);

void ospfv3_process_neighbor_2_way_received_event (OSPFV3_INTERFACE *sptr_interface,OSPFV3_NEIGHBOR *sptr_neighbor,enum OSPFV3_NEIGHBOR_EVENT event);
void ospfv3_process_neighbor_negotiation_done_event (OSPFV3_INTERFACE *sptr_interface,OSPFV3_NEIGHBOR *sptr_neighbor,enum OSPFV3_NEIGHBOR_EVENT event);
void ospfv3_process_neighbor_exchange_done_event (OSPFV3_INTERFACE *sptr_interface,OSPFV3_NEIGHBOR *sptr_neighbor,enum OSPFV3_NEIGHBOR_EVENT event);
void ospfv3_process_neighbor_loading_done_event (OSPFV3_INTERFACE *sptr_interface,OSPFV3_NEIGHBOR *sptr_neighbor,enum OSPFV3_NEIGHBOR_EVENT event);
void ospfv3_start_forming_an_adjacency_with_the_neighbor (OSPFV3_INTERFACE *sptr_interface,OSPFV3_NEIGHBOR *sptr_neighbor,enum OSPFV3_NEIGHBOR_EVENT event);
void ospfv3_maintain_or_destroy_existing_adjacency (OSPFV3_INTERFACE *sptr_interface,OSPFV3_NEIGHBOR *sptr_neighbor,enum OSPFV3_NEIGHBOR_EVENT event);
void ospfv3_tear_down_and_reestablish_adjacency (OSPFV3_INTERFACE *sptr_interface,OSPFV3_NEIGHBOR *sptr_neighbor,enum OSPFV3_NEIGHBOR_EVENT event);
void ospfv3_process_neighbor_1_way_received_event (OSPFV3_INTERFACE *sptr_interface,OSPFV3_NEIGHBOR *sptr_neighbor,enum OSPFV3_NEIGHBOR_EVENT event);
void ospfv3_process_neighbor_down_event (OSPFV3_INTERFACE *sptr_interface,OSPFV3_NEIGHBOR *sptr_neighbor,enum OSPFV3_NEIGHBOR_EVENT event);
void ospfv3_clear_advertisements_from_lists (OSPFV3_INTERFACE *sptr_interface,OSPFV3_NEIGHBOR *sptr_neighbor);


/*********************************/
/* ospfv3_neighbor_state_machine.c */
/*********************************/

void ospfv3_execute_neighbor_state_machine (enum OSPFV3_NEIGHBOR_EVENT event,enum OSPFV3_NEIGHBOR_STATE state,OSPFV3_INTERFACE *sptr_interface,
    OSPFV3_NEIGHBOR *sptr_neighbor);

/*************************/
/* ospfv3_list_utilities.c */
/*************************/

OSPFV3_GENERIC_NODE * ospfv3_find_first_node_in_list (
    OSPFV3_GENERIC_NODE * sptr_node);

void ospfv3_insert_node_in_list(
     OSPFV3_GENERIC_NODE * sptr_node,
     OSPFV3_GENERIC_NODE * sptr_previous_node);

void ospfv3_add_node_to_end_of_list(
     OSPFV3_GENERIC_NODE * sptr_node,
     OSPFV3_GENERIC_NODE * sptr_first_node_in_list);

void ospfv3_free_entire_list (OSPFV3_GENERIC_NODE *sptr_first_node_in_list);

void ospfv3_remove_node_from_list
    (OSPFV3_GENERIC_NODE ** ptr_sptr_first_node,
     OSPFV3_GENERIC_NODE * sptr_node);

void ospfv3_add_neighbor
    (OSPFV3_INTERFACE *sptr_interface,OSPFV3_NEIGHBOR *sptr_neighbor_to_add);
OSPFV3_LS_DATABASE_NODE *ospfv3_find_advertisement_on_neighbors_retransmit_list
    (OSPFV3_NEIGHBOR *sptr_neighbor,
     OSPFV3_LS_DATABASE_ENTRY *sptr_advertisement);

void ospfv3_remove_node_from_neighbors_retransmit_queue
    (OSPFV3_NEIGHBOR *sptr_neighbor,
     OSPFV3_LS_DATABASE_NODE *sptr_retransmission_node);

void ospfv3_remove_neighbor_from_advertisements_retransmit_list
    (OSPFV3_LS_DATABASE_ENTRY *sptr_advertisement,
     OSPFV3_NEIGHBOR *sptr_neighbor);

void ospfv3_add_database_entry_to_neighbor_retransmit_list
    (OSPFV3_NEIGHBOR *sptr_neighbor,
     OSPFV3_LS_DATABASE_ENTRY *sptr_database_entry);

void ospfv3_add_neighbor_to_database_retransmit_list
    (OSPFV3_LS_DATABASE_ENTRY *sptr_database_entry,
     OSPFV3_NEIGHBOR *sptr_neighbor);

void ospfv3_clean_up_retransmit_lists_affiliated_with_this_advertisement
    (OSPFV3_LS_DATABASE_ENTRY *sptr_advertisement);

void ospfv3_remove_all_database_pointers_from_neighbor_retransmit_list
    (OSPFV3_NEIGHBOR *sptr_neighbor);

void ospfv3_free_neighbor_database_summary_list
    (OSPFV3_NEIGHBOR *sptr_neighbor);

OSPFV3_LS_REQUEST * ospfv3_find_advertisement_on_neighbors_ls_request_list
    (OSPFV3_NEIGHBOR *sptr_neighbor,
     OSPFV3_LS_DATABASE_ENTRY *sptr_advertisement);

void ospfv3_free_neighbor_ls_request_list (OSPFV3_NEIGHBOR *sptr_neighbor);

void ospfv3_free_interface_acknowledgement_list
    (OSPFV3_INTERFACE *sptr_interface,
     enum BOOLEAN check_number_of_neighbors_in_init_state);

void ospfv3_remove_current_database_copy_from_all_neighbors_retransmission_lists
    (OSPFV3_LS_DATABASE_ENTRY *sptr_database_entry,
     OSPFV3_INTERFACE *sptr_interface);

void ospfv3_free_ospfv3_classes_area_lists (void);

void ospfv3_free_all_the_associated_area_pointers(OSPFV3_AREA_ENTRY* sptr_area);

void ospfv3_free_routing_table_nodes
    (OSPFV3_ROUTING_TABLE_NODE *sptr_first_discarded_or_valid_routing_table_node);

void ospfv3_free_areas_shortest_path_tree_nodes
    (OSPFV3_SHORTEST_PATH_NODE* sptr_first_shortest_path_candidate);

void ospfv3_free_areas_link_state_database (OSPFV3_AREA_ENTRY *sptr_area);

void ospfv3_free_interfaces();

void ospfv3_free_neighbor (OSPFV3_NEIGHBOR *sptr_neighbor);

STATUS ospfv3_check_if_out_going_interface_belongs_to_the_passed_area
    (ULONG out_going_interfaces_address, OSPFV3_AREA_ENTRY* sptr_area);


/*****************************/
/* ospfv3OldIpDependancies.c */
/*****************************/

USHORT ospfv3_calculate_ip_checksum
    ( PSEUDO_IP_PARAMETERS *sptr_pseudo_header,
      BYTE *bptr_start_from, USHORT length );



/***************************/
/* ospfv3_receive_database.c */
/***************************/

enum OSPFV3_PACKET_STATE ospfv3_database_packet_received (OSPFV3_DATABASE_HEADER *sptr_database_packet_header,OSPFV3_NEIGHBOR *sptr_neighbor,
    OSPFV3_INTERFACE *sptr_interface,ULONG size_of_packet);


/*******************/
/* ospfv3Receive.c */
/*******************/

void ospfv3_router_rx_packet
    ( OSPFV3_INTERFACE *sptr_interface,
      OSPFV3_PACKET *sptr_ospfv3_packet,
      USHORT packet_size,
      struct in6_addr *source_ip_address,
      struct in6_addr *destination_ip_address );

void ospfv3_build_pseudo_ip_params(PSEUDO_IP_PARAMETERS *sptr_pseudo_params,
                                    USHORT              packet_size,
                                    struct in6_addr     *   sptr_src_addr,
                                    struct in6_addr     *   sptr_dst_addr);

/**************************/
/* ospfv3_receive_request.c */
/**************************/

enum OSPFV3_PACKET_STATE ospfv3_ls_request_packet_received (OSPFV3_LS_REQUEST_HEADER *sptr_ls_request_header,OSPFV3_NEIGHBOR *sptr_neighbor,
    OSPFV3_INTERFACE *sptr_interface,ULONG size_of_packet);

/*************************/
/* ospfv3_receive_update.c */
/*************************/

enum OSPFV3_PACKET_STATE ospfv3_ls_update_packet_received  (OSPFV3_LS_UPDATE_HEADER *sptr_ls_update_header,OSPFV3_NEIGHBOR *sptr_neighbor,
    OSPFV3_INTERFACE *sptr_interface_packet_received_on,struct in6_addr *source_address,struct in6_addr *destination_address);

/************************/
/* ospfv3RoutingTable.c */
/************************/

void ospfv3_schedule_routing_table_build (void);

void ospfv3_build_routing_table (void);

void ospfv3_examine_a_single_destination_for_a_better_path
    (OSPFV3_ADVERTISEMENT_NODE *sptr_advertisement_node,
     OSPFV3_AREA_ENTRY *sptr_area);

enum BOOLEAN ospfv3_add_new_path_to_routing_table_entry_and_update_rtm
    (OSPFV3_ROUTING_TABLE_NODE *sptr_routing_table_entry_for_N,
     ULONG next_hop_router,
     OSPFV3_ROUTING_TABLE_ENTRY *sptr_routing_table_entry);

enum BOOLEAN ospfv3_next_hop_block_changed
    (OSPFV3_ROUTING_TABLE_NODE *sptr_old_routing_table_node,
     OSPFV3_ROUTING_TABLE_NODE *sptr_routing_table_node);

enum BOOLEAN ospfv3_get_new_next_hop_blocks_and_mark_ospfv3_rt_node_new
    (OSPFV3_ROUTING_TABLE_NODE *sptr_routing_table_node,
     OSPFV3_NEXT_HOP_BLOCK *sptr_new_next_hop_block,
     OSPFV3_NEXT_HOP_BLOCK **ptr_sptr_new_next_hops_copy);

BOOLEAN ospfv3_verify_if_route_is_old
    (OSPFV3_ROUTING_TABLE_ENTRY *sptr_routing_table_entry);

BOOLEAN ospfv3_same_prefixes(OSPFV3_ROUTING_TABLE_ENTRY *sptr_lsdb_entry1,
                             OSPFV3_ROUTING_TABLE_ENTRY *sptr_lsdb_entry2);

BOOLEAN ospfv3_same_route(   OSPFV3_ROUTING_TABLE_ENTRY *sptr_routing_table_entry1,
                             OSPFV3_ROUTING_TABLE_ENTRY *sptr_routing_table_entry2);

int ospfv3_uncompressed_subnet_compare(BYTE *sptr_prefix1,
                                       BYTE length1,
                                       BYTE *sptr_prefix2,
                                       BYTE length2);

int ospfv3_compare_rt_prefix_subnets(OSPFV3_ROUTING_TABLE_ADDR_PREFIX* sptr_prefix1,
                             OSPFV3_ROUTING_TABLE_ADDR_PREFIX* sptr_prefix2);


STATUS ospfv3_build_prefix_list_for_routing_table_entry(OSPFV3_ROUTING_TABLE_ENTRY *sptr_routing_table_entry);

STATUS ospfv3_export_changed_route_to_other_protocols (
            OSPFV3_ROUTING_TABLE_ENTRY *sptr_new_routing_table_entry,   /* new route */
            OSPFV3_ROUTING_TABLE_ENTRY *sptr_old_routing_table_entry    /* old route */ );
OSPFV3_ROUTING_TABLE_ENTRY* ospfv3_find_routing_table_entry_for_ABR(ULONG destintation_id,
                                                                    ULONG area_id );
STATUS ospfv3_build_prefix_list_for_rte(OSPFV3_ROUTING_TABLE_ENTRY *sptr_routing_table_entry);

STATUS ospfv3_include_prefixes(OSPFV3_LS_DATABASE_ENTRY *sptr_lsa, OSPFV3_ROUTING_TABLE_ENTRY *sptr_routing_table_entry);

/********************************/
/* ospfv3_routing_table_lookups.c */
/********************************/

OSPFV3_ROUTING_TABLE_NODE *ospfv3_find_routing_table_node (
    struct in6_addr *destination_address, /* can be NULL if only router ID given (second param) */
    ULONG destination_id_to_look,         /* can be 0 if only destination IPv6 address is given (first param) */
    enum OSPFV3_ROUTE_DESTINATION_TYPE destination_type,
    enum OSPFV3_ROUTE_PATH_TYPE path_type,OSPFV3_AREA_ENTRY *sptr_area);  /* NEWRT LOOKUP */

OSPFV3_ROUTING_TABLE_NODE * ospfv3_find_router_or_network_routing_table_node(
    struct in6_addr *destination_address, /* can be NULL if only router ID given (second param) */
    ULONG destination_id_to_look,         /* can be 0 if only destination IPv6 address is given (first param) */
    enum OSPFV3_LS_TYPE lsa_header_type,
    enum OSPFV3_ROUTE_PATH_TYPE path_type,OSPFV3_AREA_ENTRY *sptr_area);

OSPFV3_ROUTING_TABLE_ENTRY *ospfv3_find_routing_table_entry (
    struct in6_addr *destination_address, /* can be NULL if only router ID given (second param) */
    ULONG destination_id_to_look,         /* can be 0 if only destination IPv6 address is given (first param) */
    enum OSPFV3_ROUTE_DESTINATION_TYPE destination_type,
    enum OSPFV3_ROUTE_PATH_TYPE path_type,OSPFV3_AREA_ENTRY *sptr_area);

void ospfv3_select_complete_set_of_matching_entries_from_the_routing_table (struct in6_addr * destination_ip_address,
    OSPFV3_ROUTING_TABLE_LOOKUP_TEMP_NODE **ptr_to_sptr_first_matching_node,USHORT *usptr_number_of_entries);


/********************************/
/* ospfv3RoutingTableUpdates.c  */
/********************************/

void ospfv3_update_routing_table_due_to_receipt_of_a_inter_area_prefix_lsa
    (OSPFV3_INTER_AREA_PREFIX_LSA_HEADER *sptr_inter_area_prefix,
     OSPFV3_AREA_ENTRY * sptr_area_A);

void ospfv3_update_routing_table_due_to_receipt_of_a_inter_area_router_lsa
    (OSPFV3_INTER_AREA_ROUTER_LSA_HEADER *sptr_inter_area_router,
     OSPFV3_AREA_ENTRY * sptr_area_A);

void ospfv3_invalidate_routing_table_entry
    (OSPFV3_ROUTING_TABLE_NODE *sptr_routing_table_node_for_N,ULONG network_mask,
     struct in6_addr * destination_N,OSPFV3_AREA_ENTRY *sptr_area_A);


/************************************/
/* ospfv3_shortest_path_calculation.c */
/************************************/

void ospfv3_calculate_shortest_path_tree (OSPFV3_AREA_ENTRY *sptr_area);

STATUS ospfv3_check_if_link_exists (UNION_OSPFV3_LINK_STATE_ADVERTISEMENT *sptr_advertisement,ULONG vertex,OSPFV3_AREA_ENTRY *sptr_area);

OSPFV3_NEXT_HOP_BLOCK *ospfv3_calculate_the_set_of_next_hops (OSPFV3_SHORTEST_PATH_NODE *sptr_destination,OSPFV3_SHORTEST_PATH_NODE *sptr_parent,
    OSPFV3_ROUTER_LINK_PIECE *sptr_link, OSPFV3_AREA_ENTRY *sptr_area);

OSPFV3_NEXT_HOP_BLOCK *ospfv3_inherit_the_set_of_next_hops_from_node_X (OSPFV3_NEXT_HOP_BLOCK *sptr_node_X_next_hops);

void ospfv3_schedule_shortest_path_first_job (OSPFV3_AREA_ENTRY *sptr_area);

/***************/
/* ospfv3_snmp.c */
/***************/

/*
STATUS ospfv3_get_mib_variable (enum MIB_OPERATION mib_operation, char *cptr_mib_string,
    BYTE* bptr_variable_value , USHORT *usptr_size_of_variable_value, USHORT *usptr_size_of_table_indices,
    ULONG *ulptr_table_indices, enum BOOLEAN *eptr_end_of_table, char **ptr_to_cptr_next_variable_name);
*/

/******************/
/* ospfv3System.c */
/******************/

void ospfv3_init_stub();

#if defined (__OSPFV3_FTP__)
STATUS ospfv3_init
    (const char* p_configuration_file,
     const char* p_ftp_server_address,
     const char* p_directory, const char* p_user, const char* p_password);
#else
STATUS ospfv3_init (void);
#endif

STATUS ospfv3_startup( void );

STATUS ospfv3PartMemInitialize (void);

void ospfv3_objects_reclamation(void);

STATUS  ospfv3_init_proto ();
void ospfv3_spf(void);

/*****************/
/* ospfv3Timer.c */
/*****************/

void ospfv3_router_timer (void);

/*******************/
/* ospfv3_send.c */
/*******************/

void ospfv3_tx_packet (OSPFV3_HEADER *sptr_packet,OSPFV3_INTERFACE *sptr_interface,enum OSPFV3_UNION_PACKET_TYPES type,ULONG length,
    ULONG destination_type, enum BOOLEAN free_packet, OSPFV3_NEIGHBOR *sptr_neighbor);

void ospfv3_send_packet_to_all_neighbors_in_up_state(OSPFV3_HEADER *sptr_packet,ULONG length /* in host order */,
    OSPFV3_INTERFACE *sptr_interface,enum BOOLEAN free_packet);


void ospfv3_send_completion_packet (USHORT port_number,void *vptr_txed_packet);


/***********************************/
/* ospfv3_transmit_acknowledgement.c */
/***********************************/

enum OSPFV3_ACKNOWLEDGEMENT_RETURN_TYPE ospfv3_send_acknowledgement (OSPFV3_INTERFACE *sptr_interface, OSPFV3_NEIGHBOR *sptr_neighbor,
    OSPFV3_LS_HEADER_QUEUE **ptr_to_sptr_header_queue);

/****************************/
/* ospfv3_transmit_database.c */
/****************************/

void ospfv3_send_an_empty_database_description_packet (OSPFV3_INTERFACE *sptr_interface,OSPFV3_NEIGHBOR *sptr_neighbor);

void ospfv3_send_database_summary (OSPFV3_INTERFACE *sptr_interface,OSPFV3_NEIGHBOR *sptr_neighbor);

/***************************/
/* ospfv3_transmit_request.c */
/***************************/

void ospfv3_send_ls_request_packet (OSPFV3_INTERFACE *sptr_interface,OSPFV3_NEIGHBOR *sptr_neighbor);

/**************************/
/* ospfv3_transmit_update.c */
/**************************/

void ospfv3_send_ls_update (OSPFV3_LS_DATABASE_NODE *sptr_database_list,OSPFV3_NEIGHBOR *sptr_neighbor,OSPFV3_INTERFACE *sptr_interface,
    enum BOOLEAN retransmit_flag, enum BOOLEAN advertisement_installed);
void ospfv3_free_timestamps_for_lsdb_entry(OSPFV3_LS_DATABASE_ENTRY *sptr_database_entry);

/******************/
/* ospfv3Utils.c */
/*****************/

struct ifnet* ospfv3_dstaddr_to_ifp (struct in_addr *addr); /* TBD: should this be replaced by OSPFV3_IA6_TO_IFP */

const char *ospfv3_convert_ipv6_address_to_dot_format(char *cptr_array_to_store_dot_format_address, struct in6_addr *ip_address);

void  ospfv3_ipv6address_and_mask(struct in6_addr *subnet, struct in6_addr *address, ULONG mask_length);

BOOLEAN ospfv3_is_same_ipv6_subnet(struct in6_addr *address1, ULONG mask_length1, struct in6_addr *address2, ULONG mask_length2);

BOOLEAN ospfv3_is_same_ipv6_address(struct in6_addr *address1, struct in6_addr *address2);
/* use IPADDR1_ARE_SAME instead of this */

BOOLEAN ospfv3_is_same_prefix(OSPFV3_IPV6_ADDR_PREFIX* prefix1, OSPFV3_IPV6_ADDR_PREFIX* prefix2);

/* TODO: make this macro or find and use macro from RS for this */
BOOLEAN ospfv3_is_ipv6_address_null(struct in6_addr *address1);

/* TODO: convert to macro after debug */
void ospfv3_set_metric(ULONG* metric_in_its_ulong,  USHORT metric_value);

/* TODO: convert to macro after debug */
ULONG ospfv3_get_number_of_links_in_router_lsa(OSPFV3_ROUTER_LINK_ADVERTISEMENT_HEADER *sptr_router);

/* TODO: convert to macro after debug */
ULONG ospfv3_get_number_of_routers_in_network_lsa(OSPFV3_NETWORK_LINK_ADVERTISEMENT_HEADER *sptr_network);

/* TODO: convert to macro after debug */
USHORT ospfv3_get_metric(ULONG* metric_in_its_ulong);

ULONG ospfv3_get_prefix_length_in_ulongs(BYTE prefix_length);

void ospfv3_ntoh_ipv6address(struct in6_addr *sptr_host_order_ipv6_address,
                             struct in6_addr *sptr_net_order_ipv6_address);

void ospfv3_hton_ipv6address(struct in6_addr *sptr_host_order_ipv6_address,
                             struct in6_addr *sptr_net_order_ipv6_address);

/* hton or ntoh for an OSPFv3 PDU: */
void ospfv3_byte_order_pdu( OSPFV3_HEADER *sptr_pdu, BOOLEAN net_to_host);

const struct in6_addr * ospfv3_get_mask_for_mask_length(ULONG mask_length);

ULONG ospfv3_get_mask_length_for_mask(const struct in6_addr *mask);

STATUS ospfv3_check_if_interface_in_area
    (ULONG interfaces_address, OSPFV3_AREA_ENTRY * sptr_area);

enum BOOLEAN ospfv3_check_for_transit_areas (void);

void ospfv3_generate_network_and_router_link_state_advertisements
    (OSPFV3_INTERFACE *sptr_interface);


STATUS ospfv3_get_interface_info (OSPFV3_INTERFACE *sptr_interface);

USHORT ospfv3_get_interface_mtu (OSPFV3_INTERFACE *sptr_interface);

enum BOOLEAN ospfv3_validate_interface(ULONG interface_id);

enum BOOLEAN ospfv3_check_if_area_border_router (void);

STATUS ospfv3_check_if_more_recent
    (OSPFV3_LS_HEADER *sptr_new_ls_header,
     OSPFV3_LS_HEADER *sptr_current_ls_header,ULONG elapsed_time);

STATUS ospfv3_check_if_same_instance
    (OSPFV3_LS_HEADER *sptr_new_ls_header,
     OSPFV3_LS_HEADER *sptr_current_ls_header,ULONG elapsed_time);

seq_t ospfv3_increment_sequence_number (seq_t sequence_number);

void ospfv3_add_entry_to_interfaces_delayed_acknowledgement_list
    (OSPFV3_INTERFACE *sptr_interface,
     OSPFV3_LS_DATABASE_ENTRY *sptr_database_entry);

STATUS ospfv3_check_for_valid_ls_type (OSPFV3_INTERFACE *sptr_interface,OSPFV3_LS_HEADER *sptr_ls_header, enum OSPFV3_UNION_PACKET_TYPES packet_type);

OSPFV3_HEADER *ospfv3_new_link_state_update (UNION_OSPFV3_ADVERTISEMENT_HEADER **ptr_to_uptr_acknowledgement_header,OSPFV3_INTERFACE *sptr_interface);

STATUS ospfv3_check_if_ip_destination_falls_into_address_range (
    struct in6_addr * destination_ip_address,struct in6_addr *destination_id,ULONG address_mask);

STATUS ospfv3_check_if_ip_destination_falls_into_address_range_of_prefixes (struct in6_addr *destination_ip_address,
    OSPFV3_ROUTING_TABLE_ENTRY *sptr_routing_table_entry,
    BYTE *most_specific_mask,
    OSPFV3_ROUTING_TABLE_ADDR_PREFIX **sptr_most_specific_prefix);
void ospfv3_transfer_fields_from_ls_request_structure_to_ls_header_structure (OSPFV3_LS_REQUEST *sptr_ls_request,OSPFV3_LS_HEADER *sptr_ls_header);

void ospfv3_notify_areas_for_abr_change( ulong_t area_id_to_skip );

void ospfv3_flush_network_link_advertisement( OSPFV3_INTERFACE *sptr_interface );

OSPFV3_AREA_ENTRY* ospfv3_find_area_from_area_id(ULONG area_id);

OSPFV3_INTERFACE*  ospfv3_find_interface_based_on_id_and_neighbor(OSPFV3_NEIGHBOR *sptr_neighbor, ULONG link_id);

OSPFV3_NEIGHBOR *  ospfv3_find_neighbor_based_on_id_and_interface(OSPFV3_INTERFACE *sptr_interface, ULONG id);

UINT get_system_elapsed_time_in_seconds (void);

STATUS ospfv3_export_route_to_other_protocols
    (enum OSPFV3_ROUTING_TABLE_UPDATE_ACTION action,
     OSPFV3_ROUTING_TABLE_ENTRY *sptr_ospfv3_route_entry_to_export);

STATUS ospfv3_export_route_prefix_to_other_protocols (
    enum OSPFV3_ROUTING_TABLE_UPDATE_ACTION action,
    OSPFV3_ROUTING_TABLE_ENTRY              *sptr_ospfv3_route_entry_to_export,
    OSPFV3_ROUTING_TABLE_ADDR_PREFIX        *sptr_prefix);

ULONG ospfv3_get_lsdb_index (BYTE_ENUM (OSPFV3_LS_TYPE) type);

ULONG ospfv3_get_ls_request_index(BYTE_ENUM(OSPFV3_LS_TYPE) type, BYTE_ENUM(OSPFV3_LS_FLOODING_SCOPE) flood);

ULONG ospfv3_get_full_type_with_scope (BYTE_ENUM (OSPFV3_LS_TYPE) type);

OSPFV3_ROUTING_TABLE_ADDR_PREFIX * ospfv3_get_next_prefix_in_prefix_list (
    OSPFV3_ROUTING_TABLE_ADDR_PREFIX * sptr_prefix);

ULONG ospfv3_get_next_link_id_for_self_originated_lsa (
    enum OSPFV3_LS_TYPE lsa_type);

OSPFV3_IPV6_ADDR_PREFIX *ospfv3_get_next_var_length_prefix (
    OSPFV3_IPV6_ADDR_PREFIX *);

struct in6_addr * ospfv3_get_opt_forwarding_address_ptr (
    OSPFV3_EXTERNAL_LINK_ADVERTISEMENT_HEADER *sptr_lsa);

ULONG* ospfv3_get_opt_forwarding_ext_route_tag_ptr (
    OSPFV3_EXTERNAL_LINK_ADVERTISEMENT_HEADER *sptr_lsa);

ULONG* ospfv3_get_opt_ref_link_id_ptr (
    OSPFV3_EXTERNAL_LINK_ADVERTISEMENT_HEADER *sptr_lsa);

ULONG ospfv3_get_prefix_length_in_bytes (
    BYTE prefix_length);

/********************/
/* ospfv3ToRtm.c    */
/********************/

void ospfv3_receive_rtm_socket_input (void);

STATUS ospfv3_routing_table_walk (enum BOOLEAN asCapable, enum BOOLEAN redistStatic,
                                  enum BOOLEAN redistRIP, enum BOOLEAN redistBGP,
                                  enum BOOLEAN redistDefault, enum BOOLEAN reachable);

void ospfv3_add_route (OSPFV3_IP6_ROUTE_ENTRY *sptr_route_entry);

void ospfv3_delete_route (OSPFV3_IP6_ROUTE_ENTRY *sptr_route_entry);

void ospfv3_change_route (OSPFV3_IP6_ROUTE_ENTRY *sptr_route_entry);

BOOLEAN ospfv3_is_protocol_redistributed (OSPFV3_IP6_ROUTE_ENTRY *sptr_ip_route);

/********************/
/* ospfv3LsdbShow.c */
/********************/

void ospfv3_show_stub ();

void ospfv3ShowLsdbSummary ();

void ospfv3ShowLsdb ();

void ospfv3ShowRouterLsdb ();

void ospfv3ShowNetworkLsdb ();

void ospfv3ShowSummaryLsdb ();

void ospfv3ShowExternalLsdb ();

void ospfv3ShowInterAreaRouterLsdb(void);

void ospfv3ShowInterAreaPrefixLsdb(void);

void ospfv3ShowLinkLsdb(void);

void ospfShowIntraAreaPrefixLsdb(void);

/****************************/
/* ospfv3RoutingTableShow.c */
/****************************/
void ospfv3_show_routing_stub();

STATUS ospfv3RoutingTableShow();
void ospfv3_print_destination_addresses(OSPFV3_ROUTING_TABLE_ENTRY *sptr_routing_table_entry, BOOLEAN print_always);
void ospfv3_print_prefix(OSPFV3_IPV6_ADDR_PREFIX *sptr_address_prefix);
void ospfv3_print_routing_table_entry(OSPFV3_ROUTING_TABLE_ENTRY *sptr_routing_table_entry);


/*****************************/
/* ospfv3DynamicConfig.c     */
/*****************************/

enum BOOLEAN ospfv3_dynamic_validate_area( ULONG area_id );

void ospfv3_dynamic_tear_down_neighbors(OSPFV3_INTERFACE *sptr_interface);

void ospfv3_dynamic_destroy_neighbor( OSPFV3_INTERFACE *sptr_interface,
                                    OSPFV3_NEIGHBOR *sptr_neighbor );

STATUS ospfv3_dynamic_config_metric( OSPFV3_INTERFACE *sptr_interface );

void ospfv3_dynamic_step_intfSm( enum OSPFV3_INTERFACE_EVENT event,
                               enum OSPFV3_INTERFACE_STATE state,
                               OSPFV3_INTERFACE *sptr_interface );

STATUS ospfv3_dynamic_create_area( OSPFV3_AREA_ENTRY *sptr_area );

STATUS ospfv3_dynamic_reinit_area( OSPFV3_AREA_ENTRY *sptr_area, enum BOOLEAN delete_stub );

STATUS ospfv3_dynamic_destroy_area( OSPFV3_AREA_ENTRY *sptr_area );

void ospfv3_dynamic_reset_interface( OSPFV3_INTERFACE *sptr_interface );

STATUS ospfv3_dynamic_init_unnumbered_interface( OSPFV3_INTERFACE *sptr_interface );

STATUS ospfv3_dynamic_reinit_interface( OSPFV3_AREA_ENTRY *sptr_new_area,
                                        OSPFV3_INTERFACE *sptr_interface,
                                      enum BOOLEAN  reset_interface );

STATUS ospfv3_dynamic_create_interface( OSPFV3_INTERFACE *sptr_interface );

STATUS ospfv3_dynamic_destroy_interface(OSPFV3_INTERFACE *sptr_interface,
                                      OSPFV3_AREA_ENTRY  *sptr_area );

STATUS ospfv3_dynamic_create_virtIf( OSPFV3_INTERFACE *sptr_interface,
                                   OSPFV3_AREA_ENTRY *sptr_area );

STATUS ospfv3_dynamic_destory_virtIf( OSPFV3_INTERFACE *sptr_interface );

void ospfv3_dynamic_calculate_spf( OSPFV3_AREA_ENTRY *sptr_area );

void ospfv3_dynamic_create_area_range( OSPFV3_AREA_ENTRY *sptr_area,
                                     OSPFV3_ADDRESS_RANGE_LIST_ENTRY *addr_ranges );

void ospfv3_dynamic_destroy_area_range( OSPFV3_AREA_ENTRY *sptr_area,
                                      OSPFV3_ADDRESS_RANGE_LIST_ENTRY *addr_range );

enum BOOLEAN ospfv3_dynamic_config_overflow( ULONG new_extLsdb_limit,
                                           ULONG new_overflow_interval );

void ospfv3_dynamic_flush_opaque_lsa( OSPFV3_AREA_ENTRY *sptr_area );

void ospfv3_dynamic_flush_external_routes( void );

void ospfv3_dynamic_export_external_routes( void );

/************************/
/* ospfv3Show.c */
/************************/

STATUS ospfv3EnableDebug ( char * );

STATUS ospfv3DisableDebug ( char * );

STATUS ospfv3ShowDebug ();

void ospfv3ShowVersion ();

void ospfv3_show_routines_stub ();

void ospfv3_display_packet (OSPFV3_HEADER *sptr_packet,enum BOOLEAN receive);

void ospfv3_display_ospfv3_header (OSPFV3_HEADER *sptr_packet,enum BOOLEAN receive);

void ospfv3_print_memory_error_message_and_free_buffer_if_necessary (void *vptr_buffer,const char *cptr_string);

void ospfv3_printf (enum OSPFV3_PRINTF_GROUPS printf_group,const char *cptr_format, ...);

void ospfv3_printf_stub(enum OSPFV3_PRINTF_GROUPS printf_group,const char *cptr_format, ...);/*added for the case when the debug option is turned off*/

void ospfv3_display_routing_table (void);

void ospfv3_print_next_hops (OSPFV3_NEXT_HOP_BLOCK* sptr_next_hop);

void ospfv3_display_hello_packet (OSPFV3_HELLO_HEADER *sptr_hello_packet,USHORT length_of_packet);

void ospfv3_display_database_description_packet (OSPFV3_DATABASE_HEADER *sptr_database_packet,USHORT length_of_packet);

void ospfv3_display_ls_request_packet (OSPFV3_LS_REQUEST_HEADER *sptr_ls_request_packet,USHORT length_of_packet);

void ospfv3_display_ls_update_packet (OSPFV3_LS_UPDATE_HEADER *sptr_ls_update_packet);

void ospfv3_display_ls_header (OSPFV3_LS_HEADER *sptr_ls_header);

void ospfv3_display_router_links_advertisement (OSPFV3_ROUTER_LINK_ADVERTISEMENT_HEADER *sptr_router_links_advertisement_header);

void ospfv3_display_network_links_advertisement (OSPFV3_NETWORK_LINK_ADVERTISEMENT_HEADER *sptr_network_links_advertisement_header);

void ospfv3_display_inter_area_prefix_links_advertisement (OSPFV3_INTER_AREA_PREFIX_LSA_HEADER *sptr_inter_area_prefix_lsa);

void ospfv3_display_inter_area_router_links_advertisement (OSPFV3_INTER_AREA_ROUTER_LSA_HEADER *sptr_inter_area_router_lsa);

void ospfv3_display_links_lsa(OSPFV3_LINK_LSA_HEADER *sptr_links_lsa);

void ospfv3_display_intra_area_prefix_lsa(OSPFV3_INTRA_AREA_PREFIX_LSA_HEADER *sptr_intra_area_prefix_lsa);

void ospfv3_display_external_links_advertisement (OSPFV3_EXTERNAL_LINK_ADVERTISEMENT_HEADER *sptr_external_links_advertisement_header);

void ospfv3_display_ls_acknowledgement_packet (OSPFV3_LS_ACKNOWLEDGEMENT_HEADER *sptr_ls_acknowledgement_packet,USHORT length_of_packet);

void ospfv3ShowInterface ();

void ospfv3ShowNeighbor ();

void ospfv3ShowInterfaceSummary ();

void ospfv3ShowNeighborSummary ();

void ospfv3ShowSummary ();

void ospfv3ShowAreas();

STATUS ospfv3ShowMemPartition(int option);

/*********************/
/* ospfv3SendUtils.c */
/*********************/

OSPFV3_HEADER *ospfv3_get_an_ospfv3_send_packet (ULONG size_of_packet);

void ospfv3_free_an_ospfv3_send_packet (OSPFV3_HEADER *sptr_tx_packet);

ULONG ospfv3_determine_packet_destination (OSPFV3_INTERFACE *sptr_interface,OSPFV3_NEIGHBOR *sptr_neighbor);


/****************************/
/* ospfv3ToIp.c */
/****************************/

void ospfv3_receive_ip_socket_input (void);

void ospfv3_ip_output
    ( OSPFV3_HEADER *sptr_packet,
      struct in6_addr *source_address,
      unsigned int src_if_ndx,
      struct in6_addr *destination_address,
      ULONG length,
      enum BOOLEAN free_packet,
      enum BOOLEAN convert);

STATUS ospfv3_ip_register(void);

/**************************/
/* ospfv3Nvram.c */
/**************************/

enum TEST ospfv3_initialize_nvram (char *cptr_file_name,ULONG location_of_ini_file,char *cptr_section_name,
                                   void *vptr_location_of_configuration_table);


/**************************/
/* ospfv3BacklogProcess.c */
/**************************/

STATUS ospfv3_backlog_queue_create(void);

STATUS ospfv3_backlog_queue_delete(void);

void ospfv3_backlog_queue_process(void);

int ospfv3_get_backlog_queue_size(void);

STATUS ospfv3_backlog_queue_write(
    OSPFV3_INTERFACE *sptr_interface,
    OSPFV3_PACKET    *sptr_ospfv3_packet,
    USHORT           packet_size,
    struct in6_addr  *source_ip_address,
    struct in6_addr  *destination_ip_address);

STATUS ospfv3_backlog_queue_read(OSPFV3_BACKLOG_NON_HELLO_PDU *sptr_first_queued_pdu);

/**************************************/
/* ospfv3PrototypeRoutingTableBuild.c */
/**************************************/

void ospfv3InitializeForRTPrototype(void);

int  ospfv3RoutingTablePrototype(void);

void ospfv3ShowRoutingTable(void);
#ifdef __cplusplus
}
#endif

#endif /* __INCospfv3Prototypesh */
