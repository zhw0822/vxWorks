/* ospfv3Structures.h - OSPFv3 structures header file */

/* Copyright 2004 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*
modification history
--------------------
02k,10dec04,tlu Clean up compile warning on #endif (line 1751)
02j,29nov04,tlu OSPF stack decoupling
02i,31jul04,dsk code insepction fixes
02h,28apr04,dsk Interop with dual stack routing table
02g,14apr04,ram Performance optimization changes
02f,13apr04,ram OSPFv3 Routing Table Hash Modifications
02e,13apr04,ram LSDB AVL modifications
02d,07apr04,ram OSPF v2/v3 coexistance compile
02c,31mar04,dsk Modifications for OSPFv3 ANVL conformance
02b,31mar04,dsk Modifications for OSPFv3 ANVL conformance
02a,11mar04,dsk Modifications for OSPFv3 ANVL conformance
01z,16dec03,ram Mods per type 3 & 4 testing
01y,27nov03,dsk Mods according to integration testing
01x,06nov03,ram Modifications for external routes
01w,06nov03,dsk mods to type 5 LSAs due to var length address prefix
01v,22oct03,ram Modifications for LS request & separating performance
01u,21oct03,ram Modifications for adjacency establishment
01t,17oct03,ram Modifications for memory corruption, traffic, & debug output
01s,10oct03,ram Modifications for LSDB, dynamic config, & NSSA cleanup
01r,07oct03,agi added inter_area_prefix_lsa_id, inter_area_router_lsa_id to
                OSPFV3_CLASS
01q,30sep03,ram OSPF datapath changes
01p,25sep03,ram Second general modifications submission
01r,05aug03,agi code clean-up
01p,29jul03,ram Added sockets, processing, and external route changes
01o,23jul03,agi added RWOS removal changes
                updated copyright
01n,03jun03,agi fixed structure declaration order for diab compilation, added
                number_of_prefixes to the interface structures
01m,22may03,dsk added definition for surrogate hello processing
                definitions for prioritized hello processing
                definitions for routing table entry prefix list
                removed redundant fields of routing table and of LSDB
                changes for routing table lookup due to multiple prefixes per entry
                added LSA counts needed by MIB
                added advertising router for unique identification of SPF nodes
                added definitions for replacing rwos objects with vxWorks objects
                (removed rwos dispatchers, added task descriptors, timer and semaphores)
                definitions for generating unique IDs for LSAs in aggregates
                fix early LSA retransmission filter (SPR 88619)
01l,04feb02,dsk added list of address prefixes for interface and hash table changes
01k,18feb03,dsk ported from OSPFv2 to OSPFv3 structures
01j,04feb02,dsk added usage of in6_addr (from IPv6 router stack) for IPv6 address
01i,23jan03,htm added changes for IPv6
01h,21jan03,agi updated LSA datastructures
01g,10jan03,agi propogated SPR#85432, 83418 fixes from OSPFv2
01f,10jan03,agi initial changes to the LSA datastructures
                removed unused TE datastructures
01e,06jan03,agi propogated SPR#75194, ANVL 27.15 fix from OSPFv2
                fixed compilation
01d,06jan03,hme changed all OSPFv3 data structures as per RFC 2740
01c,23dec02,agi propogated TMS code merge for SPR#84284
                propogated SPR#75796 ANVL 37.1 fix from OSPFv2
                propogated fix for SPR#81628 ANVL 13.6 to pass on MIPS board
01b,25oct02,agi added initial structures
                first pass at clean-up
01a,23oct02,agi written
*/

#ifndef __INCospfv3Structuresh
#define __INCospfv3Structuresh

#ifdef __cplusplus
extern "C" {
#endif

#include        <inetLib.h>
#include        <net/if.h>
/*#include        <netinet/ipv6.h>   */ /* for struct ip6_hdr, TODO: TBD include path */
#include        <sys/socket.h>
#include        <net/socketvar.h>

#define OSPFV3_SURROGATE_HELLO_ENABLED

enum PROTOCOL_STACK_TYPE
{
    IP_PROTOCOL_STACK,
    IPX_PROTOCOL_STACK,
    SPANNING_TREE_STACK,
    SOURCE_ROUTING_STACK,
    NLSP_PROTOCOL_STACK,
    DLSW_PROTOCOL_STACK,
    OSPFV3_PROTOCOL_STACK,
    APPLETALK_PROTOCOL_STACK,
    NETBIOS_PROTOCOL_STACK,
    VINES_PROTOCOL_STACK,
    CONSOLE_PROTOCOL_STACK,
    L2TP_PROTOCOL_STACK,
    RTM_PROTOCOL_STACK,
    PPTP_DATA_TUNNEL,
    PPP_OVER_ETHERNET_PROTOCOL_STACK,
    VPN_INTERFACE_VIRTUAL_PROTOCOL_STACK
};

enum PROTOCOL_CONTROL_OPERATION
{
    OPEN_PROTOCOL_STACK,
    CLOSE_PROTOCOL_STACK,
    GET_IP_ADDRESS_FOR_PORT,

    IS_PROTOCOL_STACK_ENABLED = 0x0100,
    GET_NUMBER_OF_PROTOCOL_STACKS_VIRTUAL_PORTS,
    GET_PROTOCOL_STACK_TYPE,
    GET_PROTOCOL_STACK_PROTOCOL_ID,
    GET_PROTOCOL_STACK_SSAP,
    GET_PROTOCOL_STACK_DSAP,
    GET_PROTOCOL_STACK_VIRTUAL_PORT_PACKET_TYPE,
    GET_PROTOCOL_STACK_REAL_PORT_NUMBER_USING_VIRTUAL_PORT_NUMBER,

    INITIALIZE_SOCKET_PROTOCOL_STACK_INTERFACE,

    IS_TOKEN_RING_FRAME_SOURCE_ROUTED,
    IS_TOKEN_RING_FRAME_A_MAC,
    GET_LENGTH_OF_TOKEN_RING_RIF,
    CLEAR_SOURCE_ROUTED_BIT,
    MOVE_RIF_INTO_PACKET,
    IS_MAC_ADDRESS_SOURCE_ROUTED,
    REGISTER_TO_IP_FROM_UPPER_LAYER,

    INITIALIZE_STACK_FUNCTION_POINTERS,
    INITIALIZE_PROTOCOL_FUNCTION_POINTERS,
    GET_PROTOCOL_ADDRESS,
    CLEAR_CACHED_ROUTE,
    REINITIALIZE_PROTOCOL_STACK,
    PROTOCOL_STACK_ROUTERWARE_CONTROL,
    UNINITIALIZE_PROTOCOL_STACK,
    PROTOCOL_ROUTE_ADDED,
    PROTOCOL_ROUTE_DELETED,
    PROTOCOL_ROUTE_CHANGED,
    LLC_PROTOCOL_STACK,
    REGISTER_NAT_FILTER = 200,
    DEREGISTER_NAT_FILTER,
    OPEN_PROTOCOL_STACK_PORT,
    CLOSE_PROTOCOL_STACK_PORT,
    ADD_IP_ADDRESS_AND_NETWORK_MASK_FOR_PORT,
    ADD_REMOTE_IP_ADDRESS_FOR_PORT,

    OPEN_VIRTUAL_PORT,
    CLOSE_VIRTUAL_PORT,
    REINITIALIZE_VIRTUAL_PORT,
    UNINITIALIZE_VIRTUAL_PORT
};

typedef struct OSPFV3_RETURN_TYPE
{
    enum OSPFV3_PACKET_STATE  packet_state;
    BYTE_ENUM (BOOLEAN)     exit_routine;
} OSPFV3_RETURN_TYPE;

/**************************************************************************************/

/* Definition of the generic node structure for database containers like LSDB and routing table.
Links LSAs into their container (DLL or AVL),
links routing table entries into their container (DLL or AVL) */
/* used for pointer manipulation when inserting and removing nodes in a list */
typedef struct OSPFV3_GENERIC_NODE
{
    struct  OSPFV3_GENERIC_NODE   *sptr_forward_link;
    struct  OSPFV3_GENERIC_NODE   *sptr_backward_link;
} OSPFV3_GENERIC_NODE;


/**************************************************************************************/

/* RFC 2740 definition of options, annex A.2 */
#if (_BYTE_ORDER == _LITTLE_ENDIAN )
typedef _struct OSPFV3_OPTIONS
    {
    BIT_FIELD_8 (enum,BOOLEAN)  V6:1;
    BIT_FIELD_8 (enum,BOOLEAN)  externals:1;
    BIT_FIELD_8 (enum,BOOLEAN)  multicast:1;
    BIT_FIELD_8 (enum,BOOLEAN)  nssa:1;
    BIT_FIELD_8 (enum,BOOLEAN)  R:1;
    BIT_FIELD_8 (enum,BOOLEAN)  demand_circuit:1; /* not supported, RFC 1793 */
    BIT_FIELD_8 (enum,BOOLEAN)  not_used_1:2;     /* should be set to zero */
    BIT_FIELD_8 (enum,BOOLEAN)  not_used_2:8;     /* should be set to zero */
    BIT_FIELD_8 (enum,BOOLEAN)  not_used_3:8;     /* should be set to zero */
    } _pack OSPFV3_OPTIONS;
#else /* _BYTE_ORDER == _BIG_ENDIAN */
typedef _struct OSPFV3_OPTIONS
    {
    BIT_FIELD_8 (enum,BOOLEAN)  not_used_3:8;     /* should be set to zero */
    BIT_FIELD_8 (enum,BOOLEAN)  not_used_2:8;     /* should be set to zero */
    BIT_FIELD_8 (enum,BOOLEAN)  not_used_1:2;     /* should be set to zero */
    BIT_FIELD_8 (enum,BOOLEAN)  demand_circuit:1; /* not supported */
    BIT_FIELD_8 (enum,BOOLEAN)  R:1;
    BIT_FIELD_8 (enum,BOOLEAN)  nssa:1;
    BIT_FIELD_8 (enum,BOOLEAN)  multicast:1;
    BIT_FIELD_8 (enum,BOOLEAN)  externals:1;
    BIT_FIELD_8 (enum,BOOLEAN)  V6:1;
    } _pack OSPFV3_OPTIONS;
#endif /* _BYTE_ORDER == _LITTLE_ENDIAN */

typedef _union  UNION_OSPFV3_OPTIONS
    {
    OSPFV3_OPTIONS  _bit;
    /*BYTE            _byte[3];*/
    } _pack UNION_OSPFV3_OPTIONS;

/**************************************************************************************/

/* DD packets flags */
#if (_BYTE_ORDER == _LITTLE_ENDIAN )
typedef _struct OSPFV3_FLAGS
{
    BIT_FIELD (enum,BOOLEAN)    master:1;
    BIT_FIELD (enum,BOOLEAN)    more:1;
    BIT_FIELD (enum,BOOLEAN)    initialize:1;
    BIT_FIELD (enum,BOOLEAN)    not_used:5;
} _pack OSPFV3_FLAGS;
#else /* _BYTE_ORDER == _BIG_ENDIAN */
typedef _struct OSPFV3_FLAGS
{
    BIT_FIELD (enum,BOOLEAN)    not_used:5;
    BIT_FIELD (enum,BOOLEAN)    initialize:1;
    BIT_FIELD (enum,BOOLEAN)    more:1;
    BIT_FIELD (enum,BOOLEAN)    master:1;
} _pack OSPFV3_FLAGS;
#endif /* _BYTE_ORDER == _LITTLE_ENDIAN */

typedef _union  UNION_OSPFV3_FLAGS
{
    OSPFV3_FLAGS  _bit;
    BYTE          _byte;
} _pack UNION_OSPFV3_FLAGS;


/**************************************************************************************/

/* RFC 2740 definition of LS_type, RFC 2740, annex A.4.2.1, page 59
This field combines flooding scope and type. Is to be used ONLY by ntoh at LSA receival
 and by hton at LSA sending.
 For all computation purpose, please use the distinct fields flood and type defined in
 structure OSPFV3_LS_HEADER below,
 Reason: type preserves its index properties that it had in OSPFv2 (contiguous range, start at 0) so
 type can be and is extensively used as index in LSDB.
 Presenting the 2 fields flood and type combined into one field only affect order of endianness,
 should only be taken in consideration at receiving / sending the LSA */

#if (_BYTE_ORDER == _LITTLE_ENDIAN )
typedef _struct OSPFV3_LS_TYPE_AND_FLOOD_FIELD
    {
    BIT_FIELD_8 (enum,BOOLEAN)  U:1;
    BIT_FIELD_8 (enum,BOOLEAN)  S1:1;
    BIT_FIELD_8 (enum,BOOLEAN)  S2:1;
    BIT_FIELD_8 (enum,BOOLEAN)  unused:5;
    BIT_FIELD_8 (enum,BOOLEAN)  lsa_function_code:8;
    } _pack OSPFV3_LS_TYPE_AND_FLOOD_FIELD;
#else /* _BYTE_ORDER == _BIG_ENDIAN */
typedef _struct OSPFV3_LS_TYPE_AND_FLOOD_FIELD
    {
    BIT_FIELD_8 (enum,BOOLEAN)  lsa_function_code:8;
    BIT_FIELD_8 (enum,BOOLEAN)  unused:5;
    BIT_FIELD_8 (enum,BOOLEAN)  S2:1;
    BIT_FIELD_8 (enum,BOOLEAN)  S1:1;
    BIT_FIELD_8 (enum,BOOLEAN)  U:1;
    } _pack OSPFV3_LS_TYPE_AND_FLOOD_FIELD;
#endif /* _BYTE_ORDER == _LITTLE_ENDIAN */

typedef _union UNION_OSPFV3_LS_TYPE
{
    OSPFV3_LS_TYPE_AND_FLOOD_FIELD  _bit;
    USHORT                          _ushort;
} _pack UNION_OSPFV3_LS_TYPE;


/**************************************************************************************/

/* definition of LSA header, as per RFC 2740, annex A.4.2, page 58 */
typedef _struct OSPFV3_LS_HEADER
{
    USHORT                                  age;
    BYTE_ENUM (OSPFV3_LS_FLOODING_SCOPE)    flood;   /* OSPFv3 flooding scope,
                                                        first byte of LS type, RFC 2740 section A.4.2 */
    BYTE_ENUM (OSPFV3_LS_TYPE)              type;    /* Type of advertisement */
    ULONG                                   id;      /* Link State Id */
    ULONG                                   advertising_router;
    seq_t                                   sequence_number;
    USHORT                                  checksum;
    USHORT                                  length;   /* length of advertisement*/
} _pack OSPFV3_LS_HEADER;

/**************************************************************************************/

/* definition of OSPF HELLO PDU, as per RFC 2740, annex A.3.2, page 50 */
typedef _struct OSPFV3_HELLO_HEADER
{
    ULONG                   interface_id;
    BYTE                    router_priority;   /* if set to 0, this router is not eligible to
                                              become designated router */
    UNION_OSPFV3_OPTIONS    options;
    USHORT                  hello_interval;  /* number of seconds between this router's Hello packets */
    USHORT                  router_dead_interval;   /* number of seconds before declaring a silent router down */

    ULONG                   designated_router;   /* identity of the designated router for
                                                this network */
    ULONG                   backup_designated_router;  /* identity of the backup designated
                                                      router for this network */
    ULONG                   neighbor;    /* IDs of each router from whom valid Hello packets
                                        have been seen recently on this network */
} _pack OSPFV3_HELLO_HEADER;


/**************************************************************************************/

/* definition of OSPF Database Description PDU, as per RFC 2740, annex A.3.3, page 51 */
typedef _struct OSPFV3_DATABASE_HEADER
{
    /* RFC 2178 G.9 */

    BYTE                    zero_field_1;
    UNION_OSPFV3_OPTIONS    options;
    USHORT                  interface_mtu;
    BYTE                    zero_field_2;
    UNION_OSPFV3_FLAGS      flags;
    ULONG                   sequence;
    OSPFV3_LS_HEADER        link_state_advertisement_header;
} _pack OSPFV3_DATABASE_HEADER;

/**************************************************************************************/

/* definition of OSPF Link State Request PDU, as per RFC 2740, annex A.3.4, page 53 */
typedef _struct OSPFV3_LS_REQUESTED_ADVERTISEMENT
{
    BYTE                        reserved_bytes[2];
    BYTE_ENUM (OSPFV3_LS_FLOODING_SCOPE)    flood;
    BYTE_ENUM(OSPFV3_LS_TYPE)   type; /* type of advertisement */
    ULONG                       id;        /* Link State Id */
    ULONG                       advertising_router;
} _pack OSPFV3_LS_REQUESTED_ADVERTISEMENT;

typedef _struct OSPFV3_LS_REQUEST_HEADER
{
    OSPFV3_LS_REQUESTED_ADVERTISEMENT requested_advertisement;
} _pack OSPFV3_LS_REQUEST_HEADER;

/* Definition of OSPFv3 Link State Request PDU, as defined by RFC 2740, annex A.3.4, page 53 */

typedef struct  OSPFV3_LS_REQUEST
{
    struct OSPFV3_LS_REQUEST   *sptr_forward_link;
    struct OSPFV3_LS_REQUEST   *sptr_backward_link;
    OSPFV3_LS_REQUESTED_ADVERTISEMENT   lsr; /* link state requst PDU contents */
    seq_t                               sequence_number;
    USHORT                              checksum;
    USHORT                              age;

} OSPFV3_LS_REQUEST;


/**************************************************************************************/

/* definition of OSPF Type 1 Lsa = Router LSA, as per RFC 2740, annex A.4.3, page 60 */
#if (_BYTE_ORDER == _LITTLE_ENDIAN )
typedef _struct OSPFV3_ROUTER_LINK_BIT_OPTIONS
{
    BIT_FIELD (enum,BOOLEAN)   bit_B:1;
    BIT_FIELD (enum,BOOLEAN)   bit_E:1;
    BIT_FIELD (enum,BOOLEAN)   bit_V:1;
    BIT_FIELD (enum,BOOLEAN)   bit_W:1;
    BIT_FIELD (enum,BOOLEAN)   not_used:4;
} _pack OSPFV3_ROUTER_LINK_BIT_OPTIONS;
#else /* _BYTE_ORDER == _BIG_ENDIAN */
typedef _struct OSPFV3_ROUTER_LINK_BIT_OPTIONS
{
    BIT_FIELD (enum,BOOLEAN)   not_used:4;
    BIT_FIELD (enum,BOOLEAN)   bit_W:1;
    BIT_FIELD (enum,BOOLEAN)   bit_V:1;
    BIT_FIELD (enum,BOOLEAN)   bit_E:1;
    BIT_FIELD (enum,BOOLEAN)   bit_B:1;
} _pack OSPFV3_ROUTER_LINK_BIT_OPTIONS;
#endif /* _BYTE_ORDER == _LITTLE_ENDIAN */

typedef _union  UNION_OSPFV3_ROUTER_LINK_BIT_OPTIONS
{
        OSPFV3_ROUTER_LINK_BIT_OPTIONS  _bit;
        BYTE                            _byte; /* was _ushort in OSPFv2 */
} _pack UNION_OSPFV3_ROUTER_LINK_BIT_OPTIONS;


typedef _struct OSPFV3_ROUTER_LINK_PIECE
{
    BYTE_ENUM (OSPFV3_ROUTER_LINK_TYPE) type;
    BYTE                                zero_filed;
    USHORT                              metric;
    ULONG                               interface_id;
    ULONG                               neighbor_interface_id;
    ULONG                               neighbor_router_id;
} _pack OSPFV3_ROUTER_LINK_PIECE;


typedef _struct OSPFV3_ROUTER_LINK_ADVERTISEMENT_HEADER
{
    OSPFV3_LS_HEADER                      ls_header;
    UNION_OSPFV3_ROUTER_LINK_BIT_OPTIONS  bit_options;
    UNION_OSPFV3_OPTIONS                  options;
    OSPFV3_ROUTER_LINK_PIECE              link;
} _pack OSPFV3_ROUTER_LINK_ADVERTISEMENT_HEADER;

typedef _struct OSPFV3_ROUTER_LSA_LINK
{
    OSPFV3_GENERIC_NODE                     links;
    OSPFV3_ROUTER_LINK_ADVERTISEMENT_HEADER lsa;
} _pack OSPFV3_ROUTER_LSA_LINK;


/**************************************************************************************/

/* definition of OSPF type 2 LSA = Network LSAs, as per RFC 2740, annex A.4.4, page 63 */
typedef _struct OSPFV3_NETWORK_LINK_PIECE
{
    ULONG   link_id;
} _pack OSPFV3_NETWORK_LINK_PIECE;

typedef _struct OSPFV3_NETWORK_LINK_ADVERTISEMENT_HEADER
{
    OSPFV3_LS_HEADER           ls_header;
    BYTE                        zero_field;
    UNION_OSPFV3_OPTIONS        options;   /* three bytes */
    OSPFV3_NETWORK_LINK_PIECE  attached_router;
} _pack OSPFV3_NETWORK_LINK_ADVERTISEMENT_HEADER;

typedef _struct OSPFV3_NETWORK_LSA_LINK
{
    OSPFV3_GENERIC_NODE                         links;
    OSPFV3_NETWORK_LINK_ADVERTISEMENT_HEADER    lsa;

} _pack OSPFV3_NETWORK_LSA_LINK;


/**************************************************************************************/

/* definition of OSPFv3 address prefix, as defined by RFC 2740, annex A.4.1, page 57,
for all LSAs including address prefix lists */

#if (_BYTE_ORDER == _LITTLE_ENDIAN )
typedef _struct OSPFV3_IPV6_ADDR_PREFIX_BIT_OPTIONS
    {
    BIT_FIELD_8 (enum,BOOLEAN)   NU:1;
    BIT_FIELD_8 (enum,BOOLEAN)   LA:1;
    BIT_FIELD_8 (enum,BOOLEAN)   MC:1;
    BIT_FIELD_8 (enum,BOOLEAN)   P:1;
    BIT_FIELD_8 (enum,BOOLEAN)   not_used:4;
    } _pack OSPFV3_IPV6_ADDR_PREFIX_BIT_OPTIONS;
#else /* _BYTE_ORDER == _BIG_ENDIAN */
typedef _struct PREFIX_BIT_OPTIONS
    {
    BIT_FIELD_8 (enum,BOOLEAN)   not_used:4;
    BIT_FIELD_8 (enum,BOOLEAN)   P:1;
    BIT_FIELD_8 (enum,BOOLEAN)   MC:1;
    BIT_FIELD_8 (enum,BOOLEAN)   LA:1;
    BIT_FIELD_8 (enum,BOOLEAN)   NU:1;
    } _pack OSPFV3_IPV6_ADDR_PREFIX_BIT_OPTIONS;
#endif /* _BYTE_ORDER == _LITTLE_ENDIAN */


typedef _union  UNION_OSPFV3_IPV6_ADDR_PREFIX_BIT_OPTIONS
    {
    OSPFV3_IPV6_ADDR_PREFIX_BIT_OPTIONS    _bit;
    BYTE                  _byte;
    } _pack UNION_OSPFV3_IPV6_ADDR_PREFIX_BIT_OPTIONS;

typedef _struct OSPFV3_IPV6_ADDR_PREFIX
    {
    BYTE                                       prefix_length;
    UNION_OSPFV3_IPV6_ADDR_PREFIX_BIT_OPTIONS  prefix_options;
    USHORT                                     null_or_referenced_type_or_metric; /*  this
    2 byte field always exists
    between prefix_options and address_prefix in all prefix LSAs. Its value is:
        null, unused  in inter-area prefix LSAs and link-lsas,
        metric for intra-area prefix LSAs,
        referenced_ls_type for external LSAs */

    struct in6_addr                            address_prefix;  /* dsk changed to use definition of IPv6 address
                                                                   from target/h/netinet6/in6.h */
    } _pack OSPFV3_IPV6_ADDR_PREFIX;

typedef _struct OSPFV3_IPV6_ADDR_PREFIX_LINKS
{
    OSPFV3_GENERIC_NODE                         links;
    OSPFV3_IPV6_ADDR_PREFIX                     prefix;
} _pack OSPFV3_IPV6_ADDR_PREFIX_LINKS;

/* RFC 2740, section A.4.1:
Within OSPF, IPv6 address prefixes are always represented by a
   combination of three fields: PrefixLength, PrefixOptions, and Address
   Prefix. PrefixLength is the length in bits of the prefix.
   PrefixOptions is an 8-bit field describing various capabilities
   associated with the prefix (see Section A.4.2). Address Prefix is an
   encoding of the prefix itself as an even multiple of 32-bit words,
   padding with zero bits as necessary; this encoding consumes
   (PrefixLength + 31) / 32) 32-bit words.

*/

typedef _struct OSPFV3_IPV6_ADDR_PREFIX_RECEIVED
    {
    BYTE                                       prefix_length;
    UNION_OSPFV3_IPV6_ADDR_PREFIX_BIT_OPTIONS  prefix_options;
    USHORT                                     null_or_referencedLSType_or_metric; /*  this 2 byte field always exists
    between prefix_options and address_prefix in all prefix LSAs. Its value is:
        null, unused  in inter-area prefix LSAs and link-lsas,
        metric for intra-area prefix LSAs,
        referenced_ls_type for external LSAs */
    ULONG                                      address_prefix[OSPFV3_SIZE_OF_IPV6_ADDRESS_32BITWORDS];
        /* address prefix received is an even multiple of
            32 bit words = 0, 2 or 4 LWORDs */
    } _pack OSPFV3_IPV6_ADDR_PREFIX_RECEIVED;


/* these are used for storing prefixes associated with a routing table entry */
typedef _struct OSPFV3_ROUTING_TABLE_ADDR_PREFIX
    {
    BYTE                                       prefix_length; /* mask length */
    UNION_OSPFV3_IPV6_ADDR_PREFIX_BIT_OPTIONS  prefix_options;
    USHORT                                     prefix_metric;
    BYTE                                       prefix_address;  /* at least 1 byte in compressed format */
    } _pack OSPFV3_ROUTING_TABLE_ADDR_PREFIX;

typedef _struct OSPFV3_ROUTING_TABLE_PREFIXES_AREA
    {
    short                                       length;
    short                                       num_prefixes;
    OSPFV3_ROUTING_TABLE_ADDR_PREFIX            prefixes;
    } _pack OSPFV3_ROUTING_TABLE_PREFIXES_AREA;

/**************************************************************************************/

    /* Definition of OPSFv3 type 3 LSA = Inter-area Prefix LSA, as defined by
    RFC 2740, annex A.4.5, page 64 */

typedef _struct OSPFV3_INTER_AREA_PREFIX_LSA_HEADER
    {
        OSPFV3_LS_HEADER                            ls_header;
        ULONG                                       metric;
        OSPFV3_IPV6_ADDR_PREFIX                     address_prefix;
    } _pack OSPFV3_INTER_AREA_PREFIX_LSA_HEADER;


typedef _struct OSPFV3_INTER_AREA_PREFIX_LSA_LINK
    {
        OSPFV3_GENERIC_NODE                         links;
        OSPFV3_INTER_AREA_PREFIX_LSA_HEADER         lsa;
    } _pack OSPFV3_INTER_AREA_PREFIX_LSA_LINK;

/**************************************************************************************/

    /* Definition of OSPFv3 type 4 LSAs = Inter-Area Router LSAs, as defined by
    RFC 2740, annex A.4.6, page 65 */

typedef _struct OSPFV3_INTER_AREA_ROUTER_LSA_HEADER
    {
        OSPFV3_LS_HEADER                            ls_header;
        BYTE                                        zero_field;
        UNION_OSPFV3_OPTIONS                        options;
        ULONG                                       metric;
        ULONG                                       destination_router_id;
    } _pack OSPFV3_INTER_AREA_ROUTER_LSA_HEADER;

typedef _struct OSPFV3_INTER_AREA_ROUTER_LSA_LINK
    {
        OSPFV3_GENERIC_NODE                         links;
        OSPFV3_INTER_AREA_ROUTER_LSA_HEADER         lsa;
    } _pack OSPFV3_INTER_AREA_ROUTER_LSA_LINK;


/**************************************************************************************/

/*  Definition of OSPFv3 type 5 LSAs = AS-external LSAs, as defined by
    RFC 2740, annex A.4.7, page 66 */

typedef _struct OSPF_EXTERNAL_LINK_METRIC_PIECE
    {
    USHORT  tos;
    USHORT  metric;
    ULONG   forwarding_address;
    ULONG   external_route_tag;
    } _pack OSPFV3_EXTERNAL_LSA_LINK_METRIC_PIECE;


#if (_BYTE_ORDER == _LITTLE_ENDIAN )
typedef _struct OSPFV3_AS_EXTERNAL_LSA_FLAGS_AND_METRIC
{
    BIT_FIELD_32 (enum,BOOLEAN)  metric:24;
    BIT_FIELD_8 (enum,BOOLEAN)   T:1;
    BIT_FIELD_8 (enum,BOOLEAN)   F:1;
    BIT_FIELD_8 (enum,BOOLEAN)   E:1;
    BIT_FIELD_8 (enum,BOOLEAN)   not_used:5;
} _pack OSPFV3_AS_EXTERNAL_LSA_FLAGS_AND_METRIC;
#else /* _BYTE_ORDER == _BIG_ENDIAN */
typedef _struct OSPFV3_AS_EXTERNAL_LSA_FLAGS_AND_METRIC
{
    BIT_FIELD_8 (enum,BOOLEAN)   not_used:5;
    BIT_FIELD_8 (enum,BOOLEAN)   E:1;
    BIT_FIELD_8 (enum,BOOLEAN)   F:1;
    BIT_FIELD_8 (enum,BOOLEAN)   T:1;
    BIT_FIELD_32 (enum,BOOLEAN)  metric:24;
} _pack OSPFV3_AS_EXTERNAL_LSA_FLAGS_AND_METRIC;
#endif /* _BYTE_ORDER == _LITTLE_ENDIAN */

typedef _union UNION_OSPFV3_AS_EXTERNAL_LSA_FLAGS_AND_METRIC
{
    OSPFV3_AS_EXTERNAL_LSA_FLAGS_AND_METRIC     _bit;
    ULONG                                       _ulong;
} _pack UNION_OSPFV3_AS_EXTERNAL_LSA_FLAGS_AND_METRIC;


typedef _struct OSPFV3_EXTERNAL_LINK_ADVERTISEMENT_HEADER {
    OSPFV3_LS_HEADER                                ls_header;
    UNION_OSPFV3_AS_EXTERNAL_LSA_FLAGS_AND_METRIC   union_external_flags_metric;
    OSPFV3_IPV6_ADDR_PREFIX                         address_prefix;
    struct in6_addr                                 template_forwarding_address;
    ULONG                                           template_external_route_tag;
    ULONG                                           template_referenced_link_state_id;
} _pack OSPFV3_EXTERNAL_LINK_ADVERTISEMENT_HEADER;


typedef _struct OSPFV3_EXTERNAL_LSA_LINK
{
        OSPFV3_GENERIC_NODE                         links;
        OSPFV3_EXTERNAL_LINK_ADVERTISEMENT_HEADER   lsa;
} _pack OSPFV3_EXTERNAL_LSA_LINK;


/**************************************************************************************/

/* Definition of OSPFv3 type 7 LSAs = NSSA (Not-so-stubby-area) LSAs  */

/* dsk: port type 7 (NSSA) to IPv6 when specified (not part of RFC 2740,
it only specifies they are supported, not their format) OSPFV3_TYPE_7_LINK_ADVERTISEMENT_HEADER
*/
#define   OSPFV3_TYPE_7_LINK_ADVERTISEMENT_HEADER   OSPFV3_EXTERNAL_LINK_ADVERTISEMENT_HEADER

typedef _struct OSPFV3_TYPE_7_LSA_LINK
{
    OSPFV3_GENERIC_NODE                             links;
    OSPFV3_TYPE_7_LINK_ADVERTISEMENT_HEADER         lsa;
} _pack OSPFV3_TYPE_7_LSA_LINK;


/**************************************************************************************/

/* Definition of OSPFv3 type 8 LSA = Link LSA, as defined by RFC 2740, annex A.4.8, page 68 */

typedef _struct OSPFV3_LINK_LSA_HEADER
    {
        OSPFV3_LS_HEADER                        ls_header;
        BYTE                                    rtr_priority;
        UNION_OSPFV3_OPTIONS                    options;
        struct in6_addr                         link_local_interface_address;
        ULONG                                   number_prefixes;
        OSPFV3_IPV6_ADDR_PREFIX                 address_prefix;
    } _pack OSPFV3_LINK_LSA_HEADER;

typedef _struct OSPFV3_LINK_LSA_LINK
    {
        OSPFV3_GENERIC_NODE                     links;
        OSPFV3_LINK_LSA_HEADER                  lsa;
    } _pack OSPFV3_LINK_LSA_LINK;


/**************************************************************************************/

/*  Definition of OPSFv3 type 9 LSA = intra-area prefix LSA, as defined by
    RFC 2740, annex A.4.9, page 70 */

typedef _struct OSPFV3_INTRA_AREA_PREFIX_LSA_HEADER
    {
    OSPFV3_LS_HEADER                            ls_header;
    USHORT                                      number_prefixes;
    USHORT                                      referenced_ls_type;
    ULONG                                       referenced_link_state_id;
    ULONG                                       referenced_adv_router;
    OSPFV3_IPV6_ADDR_PREFIX                     address_prefix;
    } _pack OSPFV3_INTRA_AREA_PREFIX_LSA_HEADER;

typedef _struct OSPFV3_INTRA_AREA_PREFIX_LSA_LINK
    {
    OSPFV3_GENERIC_NODE                         links;
    OSPFV3_INTRA_AREA_PREFIX_LSA_HEADER         lsa;
    } _pack OSPFV3_INTRA_AREA_PREFIX_LSA_LINK;


/**************************************************************************************/

/* Definition of union of all LSAs

UNION_OSPFV3_ADVERTISEMENT_HEADER is union of LSAs of all types, used for definition of
    link state update
UNION_OSPFV3_LINK_STATE_ADVERTISEMENT is union of pointers to LSAs of all types,
    used for LSDB traversal
 */

typedef _union  UNION_OSPFV3_ADVERTISEMENT_HEADER
{
    OSPFV3_ROUTER_LINK_ADVERTISEMENT_HEADER             router;
    OSPFV3_NETWORK_LINK_ADVERTISEMENT_HEADER            network;
    OSPFV3_INTER_AREA_PREFIX_LSA_HEADER                 inter_area_prefix;
    OSPFV3_INTER_AREA_ROUTER_LSA_HEADER                 inter_area_router;
    OSPFV3_EXTERNAL_LINK_ADVERTISEMENT_HEADER           external;
    OSPFV3_EXTERNAL_LINK_ADVERTISEMENT_HEADER           type_7;

    OSPFV3_LINK_LSA_HEADER                              link_lsa;
    OSPFV3_INTRA_AREA_PREFIX_LSA_HEADER                 intra_area_prefix;
} _pack UNION_OSPFV3_ADVERTISEMENT_HEADER;


/* Definition of union of LSA pointers for all LSA types */

typedef union UNION_OSPFV3_LINK_STATE_ADVERTISEMENT
    {
    struct OSPFV3_ROUTER_LINK_ADVERTISEMENT_HEADER      *sptr_router;
    struct OSPFV3_NETWORK_LINK_ADVERTISEMENT_HEADER     *sptr_network;
    struct OSPFV3_INTER_AREA_PREFIX_LSA_HEADER          *sptr_inter_area_prefix;
    struct OSPFV3_INTER_AREA_ROUTER_LSA_HEADER          *sptr_inter_area_router;
    struct OSPFV3_EXTERNAL_LINK_ADVERTISEMENT_HEADER    *sptr_external;
    struct OSPFV3_TYPE_7_LINK_ADVERTISEMENT_HEADER      *sptr_type_7;
    struct OSPFV3_LINK_LSA_HEADER                       *sptr_link_lsa;
    struct OSPFV3_INTRA_AREA_PREFIX_LSA_HEADER          *sptr_intra_area_prefix;

} UNION_OSPFV3_LINK_STATE_ADVERTISEMENT;

/**************************************************************************************/

/* Definition of OSPFv3 Link State Update PDU, as defined by RFC 2740, annex A.3.5, page 54 */

typedef _struct OSPFV3_LS_UPDATE_HEADER
{
    ULONG                               number_of_advertisements;
    UNION_OSPFV3_ADVERTISEMENT_HEADER   advertisement_header;
} _pack OSPFV3_LS_UPDATE_HEADER;


/**************************************************************************************/

/* Definition of OSPFv3 Link State Acknowledge PDU, as defined by RFC 2740, annex A.3.6, page 55 */

typedef _struct OSPFV3_LS_ACKNOWLEDGEMENT_HEADER
{
    OSPFV3_LS_HEADER ls_advertisement_header;
} _pack OSPFV3_LS_ACKNOWLEDGEMENT_HEADER;


/**************************************************************************************/

/* Definition of union of OSPFv3 PDUs */

typedef _union  UNION_OSPFV3_PACKET_TYPES
{
    OSPFV3_HELLO_HEADER               hello;
    OSPFV3_DATABASE_HEADER            database;
    OSPFV3_LS_REQUEST_HEADER          ls_request;
    OSPFV3_LS_UPDATE_HEADER           ls_update;
    OSPFV3_LS_ACKNOWLEDGEMENT_HEADER  ls_acknowledgement;
} _pack UNION_OSPFV3_PACKET_TYPES;


/**************************************************************************************/

/* Definition of OSPFv3 PDU header as defined by RFC 2740, annex A.3.1, page 48 */

typedef _struct OSPFV3_HEADER     /* the standard 24 byte header for OSPF (page 178) */
{
    BYTE                                    version;    /* set to 2 for this implementation */
    BYTE_ENUM(OSPFV3_UNION_PACKET_TYPES)    type;
    USHORT                                  length;     /* length of entire OSPF protocol packet
                                                           in bytes, including the standard OSPF header */

    ULONG                                   router_id;  /* identity of the router originating the packet */
    ULONG                                   area_id;    /* a 32 bit number identifying the area
                                                           this packet belongs to */


    USHORT                                  checksum;   /* checksum of the entire contents of the packet
                                                           starting with the OSPF packet header
                                                           but excluding the 64 bit authentication field */
    BYTE                                    instance_id;
    BYTE                                    zero_field;
    UNION_OSPFV3_PACKET_TYPES               rest_of_packet;
} _pack OSPFV3_HEADER;

/**************************************************************************************/

/* Definition of OSPFv3 PDU including IPv6 header, ip6_hdr, defined in wrn/coreip/protos/ipv6/ipv6.h */

typedef _struct OSPFV3_PACKET
{
    OSPFV3_HEADER                           header;
} _pack OSPFV3_PACKET;

/**************************************************************************************/

typedef struct  OSPFV3_LS_DATABASE_SUMMARY
{

    struct OSPFV3_GENERIC_NODE              links;
    OSPFV3_HEADER                           *sptr_ospfv3_header;
    USHORT                                  size_of_packet; /* length of this packet including ospf header size */
    USHORT                                  number_of_database_entries;
} OSPFV3_LS_DATABASE_SUMMARY;

/**************************************************************************************/



/**************************************************************************************/
/****************************/
/* Routing Table Structures */
/****************************/

typedef struct OSPFV3_NEXT_HOP_BLOCK
{
        struct OSPFV3_NEXT_HOP_BLOCK      *sptr_forward_link;
        struct OSPFV3_NEXT_HOP_BLOCK      *sptr_backward_link;
        ULONG                                   outgoing_router_interface;
        ULONG                                   next_hop_router;
        enum OSPFV3_ROUTE_STATUS route_status;

} OSPFV3_NEXT_HOP_BLOCK;

/**************************************************************************************/
/* SPR 88619 start */
typedef struct OSPFV3_LSA_TIMESTAMP_PER_INTERFACE_LIST
{
    struct OSPFV3_LSA_TIMESTAMP_PER_INTERFACE_LIST   *sptr_forward_link;
    struct OSPFV3_LSA_TIMESTAMP_PER_INTERFACE_LIST   *sptr_backward_link;
    ULONG   if_index;
    ULONG   neighbor_id;
    ULONG   timestamp;
} OSPFV3_LSA_TIMESTAMP_PER_INTERFACE_LIST;


typedef struct  OSPFV3_LS_DATABASE_ENTRY
    {
    OSPFV3_GENERIC_NODE                     links;
#ifdef OSPFV3_AGGREGATE_OPTIMIZE
    OSPFV3_GENERIC_NODE                     links_lsa_aggregate; /* links the chain LSAs that
                                                are pieces of an aggregate LSA together:
                                                Type 1 LSA: chain multiple router LSAs of same router
                                                            chain last type 1 LSA of aggregate to
                                                            type 9 LSA describing the router
                                                Type 2 LSA: chain to type 9 LSA describing the link
                                                Type 9 LSA: chain multiple type 9 LSAs describing
                                                            same link (for type 9 LSA describing
                                                            a router, only 1 LSA per router,
                                                            RFC 2740, page 32) */
#endif
    UNION_OSPFV3_LINK_STATE_ADVERTISEMENT   advertisement;

    struct OSPFV3_AREA_ENTRY                *sptr_ls_database_area;                                                         /* for keeping count of db's in each area */

    struct OSPFV3_INTERFACE                 *sptr_ls_database_interface;  /* This is necessary for local scope lsas to associate
                                                                             them to the interface received or originated on.*/

    ULONG                                   ls_database_time_stamp; /* for keeping age - stamped when arrived */

    struct OSPFV3_NEIGHBOR_LIST             *sptr_ls_database_retrans;                                              /* neighbors pointing to this ls_database */
    enum OSPFV3_SEQUENCE_STATE              ls_database_sequence_state;

/* SPR 88619 start */
    OSPFV3_LSA_TIMESTAMP_PER_INTERFACE_LIST *   sptr_lsa_retransmit_timestamps_per_neighbor;
/* SPR 88619 end */

    enum OSPFV3_LS_TYPE                     ls_database_type;

    ULONG                                   lsa_age_after_maxage;   /* counts age of LSA after maxage */

} OSPFV3_LS_DATABASE_ENTRY;


typedef struct OSPFV3_ROUTING_TABLE_ENTRY
{

        enum OSPFV3_ROUTE_DESTINATION_TYPE      destination_type;
        ULONG                                   destination_id;
        OSPFV3_ROUTING_TABLE_PREFIXES_AREA      *sptr_prefixes; /* area with all prefixes defined for this network entity (router or network link) */
        BYTE_ENUM (BOOLEAN)                     type_5_route_originated;
        UNION_OSPFV3_OPTIONS                    optional_capabilities;
        struct OSPFV3_AREA_ENTRY                *sptr_area;
        enum OSPFV3_ROUTE_PATH_TYPE             path_type;
        ULONG                                   path_cost;
        ULONG                                   type2_cost;
        ULONG                                   link_state_id;
        BYTE_ENUM (OSPFV3_LS_TYPE)              link_type;
        ULONG                                   advertising_router;
        OSPFV3_LS_DATABASE_ENTRY                *sptr_link_state_origin;
        OSPFV3_NEXT_HOP_BLOCK                   *sptr_next_hop;
        struct in6_addr                         forwarding_address;

} OSPFV3_ROUTING_TABLE_ENTRY;

typedef struct  OSPFV3_ROUTING_TABLE_NODE
{
        struct OSPFV3_ROUTING_TABLE_NODE    *sptr_forward_link;
        struct OSPFV3_ROUTING_TABLE_NODE    *sptr_backward_link;
        BYTE_ENUM (BOOLEAN)                 active_areas_discarded_entry; /* discard_entry */
        enum OSPFV3_ROUTE_STATUS            route_node_status;
        OSPFV3_ROUTING_TABLE_ENTRY          *sptr_routing_table_entry;
} OSPFV3_ROUTING_TABLE_NODE;

/* this is used during routing table lookup, to avoid 2 passes through the
list of prefixes of a routing table entry,
one to find a match, one to find best match,
maintain best match from first pass.
This has exactly same fields of OSPFV3_ROUTING_TABLE_NODE plus the most specific mask */
typedef struct  OSPFV3_ROUTING_TABLE_LOOKUP_TEMP_NODE
{
        struct OSPFV3_ROUTING_TABLE_LOOKUP_TEMP_NODE    *sptr_forward_link;
        struct OSPFV3_ROUTING_TABLE_LOOKUP_TEMP_NODE    *sptr_backward_link;
        BYTE_ENUM (BOOLEAN)                 active_areas_discarded_entry; /* discard_entry */
        enum OSPFV3_ROUTE_STATUS            route_node_status;
        OSPFV3_ROUTING_TABLE_ENTRY          *sptr_routing_table_entry;
        BYTE                                most_specific_mask;
} OSPFV3_ROUTING_TABLE_LOOKUP_TEMP_NODE;

/**************************************************************************************/

typedef union UNION_OSPFV3_VIRTUAL_ROUTE
{
        OSPFV3_ROUTING_TABLE_ENTRY          *sptr_ospfv3_route;
} UNION_OSPFV3_VIRTUAL_ROUTE;


typedef struct OSPFV3_LS_DATABASE_NODE
{
        struct OSPFV3_LS_DATABASE_NODE    *sptr_forward_link;
        struct OSPFV3_LS_DATABASE_NODE    *sptr_backward_link;

        OSPFV3_LS_DATABASE_ENTRY          *sptr_ls_database_entry;
        ULONG                             periodic_retransmit_time_counter;
        BYTE_ENUM (BOOLEAN)               flood;                                                                  /* true if flooding this one */
} OSPFV3_LS_DATABASE_NODE;

typedef struct OSPFV3_DATABASE_INFORMATION_STRUCTURE                                                              /* used in ospfv3_flood.c */
{
        BYTE_ENUM (BOOLEAN)                       found_link_state_advertisement_in_database;
        OSPFV3_LS_DATABASE_ENTRY                  *sptr_database_entry;
        struct OSPFV3_INTERFACE                   *sptr_interface;
        struct OSPFV3_NEIGHBOR                    *sptr_neighbor;
        UNION_OSPFV3_LINK_STATE_ADVERTISEMENT     new_advertisement;
        ULONG                                     time_difference;
        struct in6_addr                           source_address;
        struct in6_addr                           destination_address;
} OSPFV3_DATABASE_INFORMATION_STRUCTURE;


/**************************************************************************************/

/* Definition of OSPFV3_NEIGHBOR */

typedef struct  OSPFV3_NEIGHBOR
{
        struct OSPFV3_NEIGHBOR      *sptr_forward_link;
        struct OSPFV3_NEIGHBOR      *sptr_backward_link;
        struct OSPFV3_INTERFACE     *sptr_interface;
        enum OSPFV3_NEIGHBOR_STATE  state;

        ULONG                       events;  /* Number of state changes */
        ULONG                       last_hello;     /* Time last hello was received from this neighbor */
        ULONG                       periodic_inactivity_time_counter;
        BYTE_ENUM (BOOLEAN)         inactivity_timer_enabled;
        enum OSPFV3_NEIGHBOR_MODE   mode;    /* Master or slave mode */
        UNION_OSPFV3_FLAGS          flags;   /* For passing initialize, more and master/slave bits */
        /* ANVL 43.2 */
        UNION_OSPFV3_FLAGS          last_dd_flags;   /* For recording values received in last DD packet from neighbor 
                                                    for initialize, more and master/slave bits */
        UNION_OSPFV3_OPTIONS        options;
        seq_t                       database_description_sequence_number;
        seq_t                       last_received_dd_sequence_number;
        ULONG                       last_exchange;    /* Time last exchange was received from this neighbor - hold timer */
        ULONG                       periodic_slave_hold_time_counter;
        USHORT                      priority;  /* 0 means not eligible to become the Designated Router */
        ULONG                       id;  /* neighbor ID = its router ID */

        /* Neighbor's Interface ID. RFC 2740, section 3.1.3
           The interface ID that the neighbor advertises in its Hello packets
        */
        ULONG                       neighbor_interface_id;

        /* Neighbor IP address. RFC 2740, section 3.1.3 */
        struct in6_addr             address;

        /* In OSPFv3 Neighbor's designated and backup designated routers are encoded as
           Router IDs, instead of IP addresses. See RFC 2740, section 3.1.3
           HME kept the structures as ULONG
        */
        ULONG                       designated_router;
        ULONG                       backup_designated_router;

        OSPFV3_LS_DATABASE_NODE     *sptr_retransmit;        /* Link state advertisements waiting for acknowledgements */
        OSPFV3_LS_DATABASE_SUMMARY  *sptr_database_summary; /* Database summary packets that make up area database */

        OSPFV3_LS_REQUEST           *sptr_ls_request[OSPFV3_NUM_LSA_REQUEST_INDEX];

        USHORT                      retransmit_queue_count;
        USHORT                      database_summary_queue_count;
        USHORT                      ls_request_queue_count;

        BYTE_ENUM (BOOLEAN)         ls_request_list_has_changed;

        ULONG                       mib_address_less_index;
        BYTE_ENUM (BOOLEAN)         mib_nbma_status;
        BYTE_ENUM (BOOLEAN)         mib_nbma_permanence;
        BYTE_ENUM (BOOLEAN)         mib_hello_suppressed;
        ULONG                       mib_area_id;

} OSPFV3_NEIGHBOR;

/**************************************************************************************/

typedef struct  OSPFV3_DESIGNATED_ROUTER_NODE
{
        ULONG                       id;
        struct in6_addr             address;
        enum OSPFV3_NEIGHBOR_STATE  state;
        USHORT                      priority; /* 0 means not eligible to become the Designated Router */
} OSPFV3_DESIGNATED_ROUTER_NODE;

/**************************************************************************************/

/* Definition of OSPFv3 interface */

#if (_BYTE_ORDER == _LITTLE_ENDIAN )
typedef _struct OSPFV3_INTERFACE_FLAGS
{
        BIT_FIELD (enum, BOOLEAN)       network_scheduled:1;            /* When designated router: semaphore for generating link state advertisements */
        BIT_FIELD (enum, BOOLEAN)       enable:1;                                       /* Interface is enabled */
        BIT_FIELD (enum, BOOLEAN)       build_network:1;                        /* Flag to build_net_lsa */
        BIT_FIELD (enum, BOOLEAN)       neighbor_change:1;                      /* Schedule neighbor change */
        BIT_FIELD (enum, BOOLEAN)       multicast:1;                            /* Interface is multicast capable */
        BIT_FIELD (enum, BOOLEAN)       cost_set:1;                                     /* Cost was manually configured */
        BIT_FIELD (enum, BOOLEAN)       unused:1;     /* Secondary authentication key */
        BIT_FIELD (enum,BOOLEAN)        build_type9:1;
} _pack OSPFV3_INTERFACE_FLAGS;
#else /* _BYTE_ORDER == _BIG_ENDIAN */
typedef _struct OSPFV3_INTERFACE_FLAGS
{
        BIT_FIELD (enum, BOOLEAN)       build_type9:1;     /* Type 8 LSA received, must build type 9 LSA if designated router */
        BIT_FIELD (enum, BOOLEAN)       unused:1;     /* Secondary authentication key */
        BIT_FIELD (enum, BOOLEAN)       cost_set:1;                                     /* Cost was manually configured */
        BIT_FIELD (enum, BOOLEAN)       multicast:1;                            /* Interface is multicast capable */
        BIT_FIELD (enum, BOOLEAN)       neighbor_change:1;                      /* Schedule neighbor change */
        BIT_FIELD (enum, BOOLEAN)       build_network:1;                        /* Flag to build_net_lsa */
        BIT_FIELD (enum, BOOLEAN)       enable:1;                                       /* Interface is enabled */
        BIT_FIELD (enum, BOOLEAN)       network_scheduled:1;            /* When designated router: semaphore for generating link state advertisements */
} _pack OSPFV3_INTERFACE_FLAGS;
#endif /* _BYTE_ORDER == _LITTLE_ENDIAN */

typedef _union UNION_OSPFV3_INTERFACE_FLAGS
{
        OSPFV3_INTERFACE_FLAGS          _bit;
        BYTE                            _byte;
} _pack UNION_OSPFV3_INTERFACE_FLAGS;


typedef struct OSPFV3_LS_HEADER_QUEUE
{
    struct OSPFV3_LS_HEADER_QUEUE       *sptr_forward_link;
    struct OSPFV3_LS_HEADER_QUEUE       *sptr_backward_link;
    OSPFV3_LS_HEADER                    ls_header;
} OSPFV3_LS_HEADER_QUEUE;


typedef struct OSPFV3_LS_DATABASE_HEAD
    {
    OSPFV3_LS_DATABASE_ENTRY    *sptr_database_entry;
    AVL_NODE *                  sptr_avl_database_entry;  /* AVL LSA tree */
    } OSPFV3_LS_DATABASE_HEAD;


/* OSPFv3 interface datastructure */

typedef struct OSPFV3_INTERFACE
    {
    struct OSPFV3_INTERFACE     *sptr_forward_link;
    struct OSPFV3_INTERFACE     *sptr_backward_link;
    struct ifnet                *vx_ospfv3_interface;
    /* HME add new structures for OSPFv3 as per RFC 2740, section 3.1.2. */

    ULONG                       interface_id;

    /*  Instance ID enables multiple instances of OSPF to be run over a single link. */
    BYTE                        instance_id;

    /* A new data structure for LSAs with Link-Local flooding scope.
       link_local_database_hash_table[1] contains all LSAs with UNKNOWN LS types and Link-Local flooding scope.
       link_local_database_hash_table[0] contains all LSAs with KNOWN   LS types and Link-Local flooding scope
    */
    OSPFV3_LS_DATABASE_HEAD     ls_database_hash_table[OSPFV3_NUM_INTERFACE_LSA_HASHES][OSPFV3_HASH_TABLE_SIZE];
    /* number of interface link LSAs in the interface LSDB */
    ULONG                       interface_lsa_count;
    /* sum of checksums of all link LSAs for fast comparison of LSDB of 2 routers */
    ULONG                       interface_lsa_checksum;

    /* The list of IPv6 prefixes configured for the attached link*/
    OSPFV3_IPV6_ADDR_PREFIX_LINKS *sptr_list_of_prefixes;

    /* IP interface address. */
    struct in6_addr             address;   /* a link_local address */
    BYTE                        netmask;  /* link local address length */
    USHORT                      ifnet_flags;
    ULONG                       area_id;
    BYTE_ENUM (BOOLEAN)         port_enabled;

    enum OSPFV3_INTERFACE_TYPE    type;
    enum OSPFV3_INTERFACE_STATE   state;
    ULONG                       events;  /* Number of state changes */
    ULONG                       mtu;
    struct OSPFV3_AREA_ENTRY    *sptr_area;  /* The area this interface is in */
    struct OSPFV3_AREA_ENTRY    *sptr_transit_area;
    ULONG                       source_area_id_for_virtual_link;

    ULONG                       router_dead_interval;  /* Number of seconds before the router's sptr_neighbors will declare it down */
    ULONG                       transmit_delay;       /* Estimated number of seconds it takes to transmit a link state update packet over this interface */
    USHORT                      priority;   /* If set to 0, this router is not eligible to become designated router on the attached network */

    OSPFV3_NEIGHBOR             *sptr_neighbor;
    OSPFV3_NEIGHBOR             potential_neighbor;
    ULONG                       virtual_neighbor_rid;
    ULONG                       virtual_neighbor_intf_id;

    USHORT                      number_of_neighbors_in_init_state;
    USHORT                      number_of_neighbors_in_exchange_state;
    USHORT                      number_of_neighbors_in_full_state;

    OSPFV3_DESIGNATED_ROUTER_NODE designated_router;
    OSPFV3_DESIGNATED_ROUTER_NODE backup_designated_router;
    USHORT                      cost;   /* One for each type of service */

    ULONG                       hello_interval;     /* Length of time, in seconds, between Hello packets sent on this interface */
    ULONG                       poll_interval;      /* the default poll interval is 4 * the hello interval */
    ULONG                       periodic_hello_time_counter;
    BYTE_ENUM (BOOLEAN)         hello_timer_enabled;
    ULONG                       last_hello_time;
    ULONG                       retransmit_interval;
    ULONG                       periodic_retransmit_time_counter;
    BYTE_ENUM (BOOLEAN)         retransmit_timer_enabled;
    ULONG                       periodic_wait_time_counter;  /* single shot timer */
    BYTE_ENUM (BOOLEAN)         wait_timer_enabled;

    ULONG                       periodic_delayed_acknowledgement_time_counter;  /* single shot timer - sort of */
    BYTE_ENUM (BOOLEAN)         delayed_acknowledgement_timer_enabled;

    UNION_OSPFV3_INTERFACE_FLAGS  flags;
    OSPFV3_LS_HEADER_QUEUE        *sptr_head_of_delayed_acknowledgement_list;
    ULONG                       number_of_acknowledgements_on_delayed_acknowledgement_list;

    ULONG                       lock_time;
    ULONG                       up_time;  /* Time interface came up */

    BYTE_ENUM (BOOLEAN)         point_timer_enabled;
    ULONG                       port_number; /* Unnumbered link support jkw */

    BYTE_ENUM (BOOLEAN)         passive;
    /* TBD: for unnumbered interfaces, should this interface be of type in6_addr or ULONG */
    ULONG                       unnumbered_dest_ip;    /* unnumbered link dest ip */
    ULONG                       unnumbered_router_id;  /* unnumbered link router id */
    ULONG                       nbma_ptmp_neighbor_id;  /* __NBMA_PTMP__ */
    struct in6_addr             nbma_ptmp_neighbor_address;  /* __NBMA_PTMP__ */
    ULONG                       nbma_ptmp_neighbor_intf_id;
    ULONG                       number_of_prefixes;
    OSPFV3_IPV6_ADDR_PREFIX_LINKS * sptr_site_local_or_global_addresses;
    } OSPFV3_INTERFACE;


typedef struct OSPFV3_INTERFACE_NODE
{
    struct OSPFV3_INTERFACE_NODE    *sptr_forward_link;
    struct OSPFV3_INTERFACE_NODE    *sptr_backward_link;
    OSPFV3_INTERFACE                *sptr_interface;
} OSPFV3_INTERFACE_NODE ;

/**************************************************************************************/

typedef struct OSPFV3_HOSTS
{
    struct OSPFV3_HOSTS         *sptr_forward_link;
    struct OSPFV3_HOSTS         *sptr_backward_link;
    struct in6_addr             interface_address;
    ULONG                       type_of_service;
    USHORT                      cost;
    BYTE_ENUM (BOOLEAN)         status;
    ULONG                       area_id;
} OSPFV3_HOSTS;


/**************************************************************************************/

typedef struct OSPFV3_SHORTEST_PATH_NODE
{
        struct OSPFV3_SHORTEST_PATH_NODE    *sptr_forward_link;
        struct OSPFV3_SHORTEST_PATH_NODE    *sptr_backward_link;
/* unique identification for a vertex (or any entity) in OSPFv3:
 ID and advertising router, not just ID */
        /*ULONG                               vertex;*/ /* temporary
        comment out vertex to locate via compiler all places where
        identification via vertex
        should be replaced with identification via (vertex and adv. router)  */
        ULONG                               vertexv3;
        ULONG                               adv_router;

        BYTE_ENUM (OSPFV3_LS_TYPE)          vertex_type;
        OSPFV3_LS_DATABASE_ENTRY            *sptr_database_entry;
        ULONG                               cost;
        ULONG                               intervening_router;
        OSPFV3_NEXT_HOP_BLOCK               *sptr_next_hop;
} OSPFV3_SHORTEST_PATH_NODE;

/**************************************************************************************/

/* definition of Area and address range */

#if (_BYTE_ORDER == _LITTLE_ENDIAN )
typedef _struct   OSPFV3_AREA_FLAGS
{
        BIT_FIELD (enum, BOOLEAN)       transit:1;                      /* This is a transit area */
        BIT_FIELD (enum, BOOLEAN)       virtual_up:1;           /* One or more virtual links in this area are up */
        BIT_FIELD (enum, BOOLEAN)       stub:1;                         /* This is a  area (no external routes) */
        BIT_FIELD (enum, BOOLEAN)       nssa:1;                         /* This is a not so stubby area */
        BIT_FIELD (enum, BOOLEAN)       stub_default:1;         /* Inject default into this stub area */
        BIT_FIELD (enum,BOOLEAN)        not_used:3;
} _pack OSPFV3_AREA_FLAGS;
#else /* _BYTE_ORDER == _BIG_ENDIAN */
typedef _struct OSPFV3_AREA_FLAGS
{
        BIT_FIELD (enum,BOOLEAN)        not_used:3;
        BIT_FIELD (enum, BOOLEAN)       stub_default:1;         /* Inject default into this stub area */
        BIT_FIELD (enum, BOOLEAN)       nssa:1;                         /* This is a not so stubby area */
        BIT_FIELD (enum, BOOLEAN)       stub:1;                         /* This is a  area (no external routes) */
        BIT_FIELD (enum, BOOLEAN)       virtual_up:1;           /* One or more virtual links in this area are up */
        BIT_FIELD (enum, BOOLEAN)       transit:1;                      /* This is a transit area */
} _pack OSPFV3_AREA_FLAGS;
#endif /* _BYTE_ORDER == _LITTLE_ENDIAN */

typedef _union  UNION_OSPFV3_AREA_FLAGS
{
        OSPFV3_AREA_FLAGS _bit;
        BYTE                    _byte;
} _pack UNION_OSPFV3_AREA_FLAGS;


typedef struct OSPFV3_ADDRESS_RANGE_LIST_ENTRY    /* list of networks associated with an area (section 6 of OSPF Version 2 specification, dated June 1995) */
{
        struct OSPFV3_ADDRESS_RANGE_LIST_ENTRY  *sptr_forward_link;
        struct OSPFV3_ADDRESS_RANGE_LIST_ENTRY  *sptr_backward_link;
    ULONG                                       area_id;

        struct in6_addr                         network;
        ULONG                                   mask;
        BYTE_ENUM (BOOLEAN)                     advertise;
        BYTE_ENUM (BOOLEAN)                     active;
        ULONG                                   cost;
        struct OSPFV3_ROUTING_TABLE_NODE        *sptr_discard_entries;
} OSPFV3_ADDRESS_RANGE_LIST_ENTRY;


/* OSPFv3 area datastructure */

typedef struct OSPFV3_AREA_ENTRY
    {
    struct OSPFV3_AREA_ENTRY        *sptr_forward_link;
    struct OSPFV3_AREA_ENTRY        *sptr_backward_link;
    ULONG                           area_id;

    /* area address ranges */
    ULONG                           number_of_address_ranges;   /* for this area */
    OSPFV3_ADDRESS_RANGE_LIST_ENTRY *sptr_address_ranges;

    /* associated router interfaces */
    OSPFV3_INTERFACE                *sptr_interfaces;                                               /* this is setup at configuration time */

    /* summary links advertisements */
    /*TODO!!!!!!: convert this to OSPFV3_LS_DATABASE_NODE or move it to ls_database_hash_table*/
    struct OSPFV3_ADVERTISEMENT_NODE *sptr_summary_advertisement_list_head;

    UNION_OSPFV3_AREA_FLAGS         flags;                                                                  /* Various state bits */
    ULONG                           stub_default_cost;

    /* shortest-path tree */
    OSPFV3_SHORTEST_PATH_NODE       shortest_path_first_tree;                               /* area's shortest path first tree; head is this router */
    ULONG                           shortest_path_first_run_count;                  /* # times spf has been run for this area */
    BYTE_ENUM (BOOLEAN)             run_shortest_path_calculation;
    ULONG                           shortest_path_calculation_time_counter;
    OSPFV3_SHORTEST_PATH_NODE       *sptr_candidate;                                                /* area's candidate list for dijkstra */

    OSPFV3_LS_DATABASE_HEAD         ls_database_hash_table[OSPFV3_NUM_AREA_LSA_HASHES][OSPFV3_HASH_TABLE_SIZE];

    /* number of LSAs in the area LSDB */
    ULONG                           area_lsa_count;
    /* sum of checksums of all LSAs of an area for fast comparison of LSDB of 2 routers */
    ULONG                           area_lsa_checksum;

    USHORT                          number_of_autonomous_system_boundary_routers;

    USHORT                          number_of_area_border_routers;

    USHORT                          number_of_neighbors_in_init_state;
    USHORT                          number_of_neighbors_in_exchange_state;
    USHORT                          number_of_neighbors_in_full_state;

    USHORT                          number_of_interfaces_in_up_state;
    ULONG                           lock_time;
    BYTE                            build_router;
    BYTE_ENUM (BOOLEAN)             bring_up_virtual_links;
    ULONG                           periodic_MinLSInterval_time_counter;
    BYTE_ENUM (BOOLEAN)             MinLSInterval_timer_enabled;
    BYTE_ENUM (BOOLEAN)             inject_default_route_if_stub_area;

    USHORT                          mib_number_of_link_state_advertisements;
    ULONG                           mib_checksum_sum;

    ULONG                           mib_stub_metric_type;   /* 1 = ospf metric, 2 = comparable cost, 3 = non comparable */

    USHORT                          mib_number_of_type10_lsa;
    ULONG                           mib_type10_checksum_sum;
    ULONG                           nssa_abr_to_convert_type_7_to_type_5;
    /* RFC 2740 page 19, a routing table for wach area holding
    intermediate results during routing table build for area.
    It contains:
        one entry for every router in the area
        one entry for every transit link traversing the area */
    OSPFV3_ROUTING_TABLE_NODE       *sptr_routing_table_for_area;

    } OSPFV3_AREA_ENTRY;

typedef struct OSPFV3_STUB_AREA_ENTRY
    {
    struct OSPFV3_STUB_AREA_ENTRY   *sptr_forward_link;
    struct OSPFV3_STUB_AREA_ENTRY   *sptr_backward_link;
    struct OSPFV3_AREA_ENTRY        *sptr_area;
    } OSPFV3_STUB_AREA_ENTRY;


typedef struct OSPFV3_TRANSIT_AREA_ENTRY
{
        struct OSPFV3_TRANSIT_AREA_ENTRY  *sptr_forward_link;
        struct OSPFV3_TRANSIT_AREA_ENTRY  *sptr_backward_link;
        struct OSPFV3_AREA_ENTRY          *sptr_area;
} OSPFV3_TRANSIT_AREA_ENTRY;


/**************************************************************************************/

/**********************************/
/* Link State Database Structures */
/**********************************/

typedef struct OSPFV3_ADVERTISEMENT_NODE
{
        struct OSPFV3_ADVERTISEMENT_NODE        *sptr_forward_link;
        struct OSPFV3_ADVERTISEMENT_NODE        *sptr_backward_link;
        UNION_OSPFV3_LINK_STATE_ADVERTISEMENT    advertisement;
} OSPFV3_ADVERTISEMENT_NODE;

/**************************************************************************************/

typedef struct OSPFV3_NEIGHBOR_LIST
{
    struct OSPFV3_NEIGHBOR_LIST   *sptr_forward_link;
    struct OSPFV3_NEIGHBOR_LIST   *sptr_backward_link;
    struct OSPFV3_NEIGHBOR        *sptr_neighbor;
} OSPFV3_NEIGHBOR_LIST;

/**************************************************************************************/

/* definition of port structures */

typedef struct OSPFV3_PORT_CONFIGURATION_CLASS
{
        BYTE_ENUM (BOOLEAN)                     port_enabled;
        USHORT                                  virtual_port_number;
        ULONG                                   if_index;
        ULONG                                   area_id;
        ULONG                                   transit_area_id;
        ULONG                                   source_area_id_for_virtual_link; /* The area to which VLink belongs */
        ULONG                                   type;
        ULONG                                   router_dead_interval;
        ULONG                                   transmit_delay;
        USHORT                                  priority;
        USHORT                                  cost;
        ULONG                                   hello_interval;
        ULONG                                   virtual_neighbor_rid;   /* Router ID */
        ULONG                                   virtual_neighbor_intf_id;
        BYTE_ENUM (BOOLEAN)                     passive;

/* dsk: TODO: figure out functionality of unnumbered interfaces (unnumbered = do not have IPv6 address, only some ID, keep it at ULONG size */
        ULONG                                   unnumbered_dest_ip;                                                     /* unnumbered dest ip*/

        ULONG                                   poll_interval;
        ULONG                                   retransmit_interval;
        struct in6_addr                         nbma_ptmp_neighbor_address;   /* __NBMA_PTMP__ */
        ULONG                                   nbma_ptmp_neighbor_id;  /* __NBMA_PTMP__ */
        ULONG                                   nbma_ptmp_neighbor_intf_id;

} OSPFV3_PORT_CONFIGURATION_CLASS;

typedef struct OSPFV3_PORT_CLASS
{
        OSPFV3_PORT_CONFIGURATION_CLASS          config;                                 /* this portion is configured by NVRAM */

} OSPFV3_PORT_CLASS;

/**************************************************************************************/

typedef struct OSPFV3_AREA_CONFIGURATION_CLASS
{
        ULONG                                   area_id;
        BYTE_ENUM (BOOLEAN)                     advertise_address_range;
        ULONG                                   address_range_cost;
        BYTE_ENUM (BOOLEAN)                     external_routing_capability_enabled;
        BYTE_ENUM (BOOLEAN)                     inject_default_route_if_stub_area;
        ULONG                                   stub_default_cost;
        ULONG                                   number_of_area_address_ranges;                                                                          /* number of areas, 0 will always be the backbone */

} OSPFV3_AREA_CONFIGURATION_CLASS;

typedef struct OSPFV3_AREA_CLASS
{
        OSPFV3_AREA_CONFIGURATION_CLASS         config;    /* this portion is configured by NVRAM */
} OSPFV3_AREA_CLASS;

/**************************************************************************************/
/* RFC 2178 G.3 */
typedef struct OSPFV3_AREA_ADDRESS_RANGE_CONFIGURATION_CLASS
{


        struct in6_addr                         network;
        ULONG                                   mask_length;
        BYTE_ENUM (BOOLEAN)                     advertise;
        BYTE_ENUM (BOOLEAN)                     active;
        ULONG                                   cost;
        ULONG                                   area_id;
} OSPFV3_AREA_ADDRESS_RANGE_CONFIGURATION_CLASS;

/* RFC 2178 G.3 */
typedef struct OSPFV3_AREA_ADDRESS_RANGE_CLASS
{
        OSPFV3_AREA_ADDRESS_RANGE_CONFIGURATION_CLASS config;
} OSPFV3_AREA_ADDRESS_RANGE_CLASS;

/**************************************************************************************/

typedef struct OSPFV3_EXTERNAL_ROUTE
{
        struct OSPFV3_EXTERNAL_ROUTE            *sptr_forward_link;
        struct OSPFV3_EXTERNAL_ROUTE            *sptr_backward_link;
        ULONG                                   destination_id;
        struct in6_addr                         destination_network;
        ULONG                                   destination_mask;
        ULONG                                   metric;

        struct in6_addr                         forwarding_address;
        ULONG                                   tag;
        ULONG                                   time_stamp;
        ULONG                                   advertising_router;
        /* SPR 85050 */
        ULONG                                   route_protocol;

} OSPFV3_EXTERNAL_ROUTE;

/**************************************************************************************/

typedef struct OSPFV3_MIB_GENERAL_ENTRY
{
        ULONG                                   ospfv3VersionNumber;
        BYTE_ENUM (BOOLEAN)     ospfv3AreaBdrRtrStatus;
        ULONG                                   ospfv3ExternLsaCount;
        ULONG                                   ospfv3ExternLsaCksumSum;
        ULONG                                   ospfv3OriginateNewLsas;
        ULONG                                   ospfv3RxNewLsas;
} OSPFV3_MIB_GENERAL_ENTRY;



typedef struct WRN_OSPFV3_MIB_GENERAL_ENTRY
{
    int dummy;
} WRN_OSPFV3_MIB_GENERAL_ENTRY;



typedef struct OSPFV3_ADDRESS_MASK_PAIRS
{
        struct OSPFV3_ADDRESS_MASK_PAIRS    *sptr_forward_link;
        struct OSPFV3_ADDRESS_MASK_PAIRS    *sptr_backward_link;

        struct in6_addr                     network_address;
        ULONG                               network_mask;
        ULONG                               metric_type;
        ULONG                               metric_value;

} OSPFV3_ADDRESS_MASK_PAIRS;

typedef struct OSPFV3_REDISTRIBUTION_CONFIGURATION
{
        enum BOOLEAN            redistribute_all_bgp;
        enum BOOLEAN            redistribute_all_rip;
        enum BOOLEAN            redistribute_all_static;
        enum BOOLEAN            redistribute_ip_default_route;

        OSPFV3_GENERIC_NODE     ospfv3_imported_rip_subnets;
        OSPFV3_GENERIC_NODE     ospfv3_imported_static_subnets;
        OSPFV3_GENERIC_NODE     ospfv3_imported_bgp_subnets;

} OSPFV3_REDISTRIBUTION_CONFIGURATION;

/**************************************************************************************/

typedef struct OSPFV3_IP6_ROUTE_ENTRY
{
    struct in6_addr                         target; /* network address */
    ULONG                                   mask;   /* network address prefix length */
    struct in6_addr                         forwarding_address; /* forwarding address when redistributing routes from other protocols into OSPF: local attached subnet, or point-to-point link */
    ULONG                                   gateway;/* next hop router for redistributing OSPF routes to other protocols. 0: local attached subnet, or point-to-point link */
    ULONG                                   metric;                             /* primary metric */
    ULONG                                   metric_2;                           /* alternative metric 2 */
    ULONG                                   metric_3;                           /* alternative metric 3 */
    ULONG                                   metric_4;                           /* alternative metric 4 */
    ULONG                                   metric_5;                           /* alternative metric 5 */
    ULONG                                   flags;
    BYTE                                    weight;
    ULONG                                   protocol;
    USHORT                                  route_tag;
    ULONG                                   advertising_router;
    void*                                   p_ifnet;
} OSPFV3_IP6_ROUTE_ENTRY;


/**************************************************************************************/


#define OSPFV3_TASK_NAME_SIZE   11 /* 10 chars + null terminator */
typedef struct OSPFV3_TASK_DESCRIPTOR
{
    int                 task_id;
    int                 task_priority;
    int                 task_stack_size;
    FUNCPTR             task_start_func;
    OSPFV3_TASK_STATES  task_state;
    char                task_name[OSPFV3_TASK_NAME_SIZE];

} OSPFV3_TASK_DESCRIPTOR;

/* hello prioritized handling start */

typedef struct OSPFV3_BACKLOG_NON_HELLO_PDU
{
    /* 50 bytes overhead per queued PDU, up to OSPFV3_BACKLOG_PDU_MAX */
    OSPFV3_INTERFACE    *sptr_interface;
    OSPFV3_PACKET       *sptr_ospfv3_packet;
    USHORT              packet_size;
    struct in6_addr     source_ip_address;
    struct in6_addr     destination_ip_address;

} OSPFV3_BACKLOG_NON_HELLO_PDU;

/* OSPFV3_BACKLOG_NON_HELLO_PDU_QUEUE Used to be (without LIST from lstLib.h):
typedef struct OSPFV3_BACKLOG_NON_HELLO_PDU_QUEUE
{
    ULONG                           number_of_pdus_queued;
    OSPFV3_BACKLOG_NON_HELLO_PDU    *sptr_non_hello_pdu_queue;
    SEM_ID                          backlog_queue_semaphore_id;

} OSPFV3_BACKLOG_NON_HELLO_PDU_QUEUE;
*/
typedef struct OSPFV3_BACKLOG_NON_HELLO_PDU_QUEUE
{
    int                             read_index;
    int                             write_index;
    int                             num_pdus_in_queue;
    OSPFV3_BACKLOG_NON_HELLO_PDU    *sptr_pdu;
    SEM_ID                          pdu_queue_sem_id; /* semaphore guarding concurrent LIST operations from multiple tasks */

} OSPFV3_BACKLOG_NON_HELLO_PDU_QUEUE;
/* hello prioritized handling end */

/* Definition of main OSPF structure */
/* OSPFv3 meta-structure */
typedef struct OSPFV3_CLASS
    {
    enum BOOLEAN    protocol_enabled;
    /*ULONG           tid;*/          /* ospf task ID */
    ULONG           stack_id;
    ULONG           router_id;    /* uniquely identifies this router in the Autonomous System */
    enum BOOLEAN    type_of_service_capability;     /* TRUE means this router will calculate separate routes based on type of service */
    enum BOOLEAN    autonomous_system_border_router;
    enum BOOLEAN    ip_multicast;

    OSPFV3_PORT_CLASS port[NUMBER_OF_OSPFV3_PORTS];

    /* make sptr_interface linked list visible here. Was in OSPFV3_PORT_CLASS */
    OSPFV3_INTERFACE   *sptr_interface_list;

    ULONG            number_of_ports;
    ULONG            starting_port_number;

    ULONG            number_of_interfaces;

    /* areas */
    ULONG            number_of_areas;    /* number of areas */
    ULONG            number_of_area_address_ranges;  /* total number of area address ranges for all areas */

    ULONG            number_of_stub_areas;

    /* NOTE: areat[] shall only be used during startup for processing configuration
     *       read from ospfv3_cfg()
     */
    OSPFV3_AREA_CLASS         area[NUMBER_OF_OSPFV3_AREAS];
    struct OSPFV3_AREA_ENTRY  *sptr_area_list;    /* an array of areas connected to this router which will be
                                                     * allocated at initialization time, area 0 is the backbone */
    /* RFC 2178 G.3 */
    OSPFV3_AREA_ADDRESS_RANGE_CLASS area_address_range[NUMBER_OF_OSPFV3_AREA_ADDRESS_RANGES]; /* area address ranges for a particular area */

    struct OSPFV3_AREA_ENTRY          *sptr_backbone_area;
    struct OSPFV3_STUB_AREA_ENTRY     *sptr_stub_area_list;

    /* virtual links */
    ULONG             number_of_virtual_links;
    ULONG             number_of_virtual_links_in_Up_state;
    OSPFV3_INTERFACE_NODE     *sptr_configured_virtual_links;

    OSPFV3_EXTERNAL_ROUTE     *sptr_external_route_queue;
    ULONG                     external_lsa_queue_process_interval;
    ULONG                     ospfv3_rtm_sequence;
    ULONG                     ospfv3_rtm_pid;

    /* ip and routing socket */
    int                       ospfv3_ip_socket;
    int                       ospfv3_rtm_socket;

    /* neighbors */
    ULONG                  number_of_neighbors;       /* number of neighbors known to this router */
    ULONG                  number_of_neighbors_in_init_state;
    ULONG                  number_of_neighbors_in_exchange_state;
    ULONG                  number_of_neighbors_in_full_state;

    OSPFV3_ROUTING_TABLE_NODE  *sptr_routing_table_head[OSPFV3_RT_HASH_TABLE_SIZE];

    enum BOOLEAN           build_routing_table;
    ULONG                  routing_table_build_time_counter;

    OSPFV3_LS_DATABASE_ENTRY *sptr_free_database_entry_list;  /* list of LSAs to be freed */

    enum BOOLEAN           timer_enabled;
    ULONG                  _1_second_counter;
    ULONG                  clock_ticks_per_second;

    OSPFV3_MIB_GENERAL_ENTRY ospfv3GeneralGroupEntry;

    /* only contains RFC 1583 compatibility and opaque LSA support, obsoleted by RFC 2740 */
    WRN_OSPFV3_MIB_GENERAL_ENTRY wrnOspfv3GeneralGroupEntry;
    OSPFV3_HOSTS             *sptr_host_list;

    /* various debug flags */
    enum BOOLEAN           printing_enabled;
    enum BOOLEAN           interface_printing_enabled;
    enum BOOLEAN           neighbor_printing_enabled;
    enum BOOLEAN           memory_printing_enabled;
    enum BOOLEAN           alarm_printing_enabled;
    enum BOOLEAN           snmp_printing_enabled;
    enum BOOLEAN           packet_printing_enabled;
    enum BOOLEAN           hello_packet_printing_enabled;
    enum BOOLEAN           dd_packet_printing_enabled;
    enum BOOLEAN           ls_request_packet_printing_enabled;
    enum BOOLEAN           ls_update_packet_printing_enabled;
    enum BOOLEAN           ls_ack_packet_printing_enabled;
    enum BOOLEAN           routing_table_printing_enabled;
    enum BOOLEAN           debug_printing_enabled;
    enum BOOLEAN           rtm_printing_enabled; /* ### RTM */
    enum BOOLEAN           db_overflow_printing_enabled;
    enum BOOLEAN           search_printing_enabled;
    enum BOOLEAN           prologue_printing_enabled;


    ULONG                  routing_table_revision_number;
    ULONG                  ospfv3_maxaged_lsas_removed_per_tick;
    ULONG                  ospfv3_maxaged_lsas_removed_this_tick;

    OSPFV3_REDISTRIBUTION_CONFIGURATION ospfv3_redistribution_configuration;

    int                     ospfv3_multicast_extensions;
    enum BOOLEAN            ospfv3_demand_extensions;
    ULONG                   ospfv3_point_to_point_router_lsa_option;
    /* RFC 1765 */
    ULONG                   ospfv3_external_lsdb_limit;
    ULONG                   ospfv3_exit_overflow_interval;
    ULONG                   ospfv3_external_lsa_count;
    enum BOOLEAN            in_overflow_state;
    enum BOOLEAN            force_out_of_overflow_state;
    enum BOOLEAN            reset_ospfv3_exit_overflow_interval;


#if defined (__OSPFV3_FTP__)
    const char *    ftp_configuration_file;
    const char *    ftp_server_address;
    const char *    ftp_dir;
    const char *    ftp_user;
    const char *    ftp_pwd;
#endif

    OSPFV3_LS_DATABASE_HEAD   external_database_hash_table[OSPFV3_NUM_AS_LSA_HASHES][OSPFV3_HASH_TABLE_SIZE];
        /* HME add new structure for LSAs with unknown LS types and AS flooding scope. RFC 2740, section 3.1*/
    /* number of LSAs in the AS LSDB */
    ULONG                       as_lsa_count;
    /* sum of checksums of all LSAs in main structure for fast comparison of LSDB of 2 routers */
    ULONG                       as_lsa_checksum;

    /*
     * Pointer to old routing table, valid during SPF computation, NULL
     * otherwise, Inter-area SPF computation can be invoked: from receive
     * task at reception of type 3 or type 4 LSA from timer task (or SPF
     * task) during regular SPF computation.  This "sptr_old_routing_table"
     * is used during inter-area SPF to prevent flooding of type 3 and 4
     * LSAs from receive task and enable flooding of type 3 LSAs from timer
     * task depending on result of SPF computation.  Reason: RFC 2328
     * specifies that type 3 LSAs should only be issued based on routing
     * table entries.
    */
    OSPFV3_ROUTING_TABLE_NODE * sptr_old_routing_table[OSPFV3_RT_HASH_TABLE_SIZE];

    /* watchdog timer: runs every second in the system timer interrupt.
    All it does is give the semaphore timer_semaphore_id.
    OSPF timer task is blocked on this semaphore and wakes up executing
    the 1 second OSPF timer activities at task level when awaken by the watchdog */
    WDOG_ID  watchdog_timer;

    /* semaphore to trigger 1 second timer interrupt activities:
    taken by timer task, given by watchdog timer */
    SEM_ID timer_semaphore_id;

    /* semaphore to synchronize startup of OSPF tasks. Taken by every OSPF task
    after finishing own initialization. Given synchronously via semFlush to all ospf
    tasks pending on it when task state of all tasks is OSPFV3_TASK_STATE_INIT_COMPLETE */
    SEM_ID startup_sem_id;

    OSPFV3_TASK_DESCRIPTOR ospfv3_tasks[OSPFV3_NUM_TASKS];

    /* hello prioritized handling start */
    OSPFV3_BACKLOG_NON_HELLO_PDU_QUEUE     backlog_pdus_queue;
    ULONG                                  num_discarded_pdus;
#ifdef OSPFV3_SURROGATE_HELLO_ENABLED
    BOOLEAN                                surrogate_hello_processing_enabled;
#endif 

    /* hello prioritized handling end */

    /* these are used by the algorithm to generate unique link state IDs for individual
    LSAs that participate in an aggregate LSA originated by this router */
    ULONG   router_lsa_id;
    ULONG   inter_area_prefix_lsa_id;
    ULONG   inter_area_router_lsa_id;
    ULONG   intra_area_prefix_lsa_id;
    ULONG   external_lsa_id;

    } OSPFV3_CLASS;

/* OSPFv3 AVL LSDB node */
typedef struct OSPFV3_AVL_LSDB_NODE
{
    AVL_NODE                  node;
    OSPFV3_LS_DATABASE_ENTRY  *sptr_database_entry;
} OSPFV3_AVL_LSDB_NODE;

typedef struct OSPFV3_AVL_LSDB_KEY
{
    ULONG   id;
    ULONG   router;
    BYTE_ENUM (OSPF_LS_TYPE) type;
} OSPFV3_AVL_LSDB_KEY;


/**************************************************************************************/


#ifdef __cplusplus
}
#endif

#endif /* __INCospfv3Structuresh */
