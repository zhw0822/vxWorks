/* ospfv3NeighborStateMachine.h - OSPFv3 neighbor state machine  header file */

/* Copyright 2004 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*
modification history
--------------------
01b,14nov02,agi first pass at clean-up
01a,23oct02,agi  written
*/

#ifndef __INCospfv3NeighborStateMachineh
#define __INCospfv3NeighborStateMachineh

#ifdef __cplusplus
extern "C" {
#endif

/* includes */

#include <wrn/ospfv3/ospfv3Prototypes.h>

/* typedefs */

typedef void (*NEIGHBOR_TRANSITION_FUNCTION) 
    (
    OSPFV3_INTERFACE *sptr_interface,
    OSPFV3_NEIGHBOR *sptr_neighbor,
    enum OSPFV3_NEIGHBOR_EVENT event
    );

typedef struct
{
    NEIGHBOR_TRANSITION_FUNCTION fptr_neighbor_transition_function;
} OSPFV3_NEIGHBOR_STATE_MACHINE;

/* OSPFv3 neighbor state machine */

OSPFV3_NEIGHBOR_STATE_MACHINE ospfv3_neighbor_event_processing_table
    [OSPFV3_NUMBER_OF_NEIGHBOR_EVENTS][OSPFV3_NUMBER_OF_NEIGHBOR_STATES] =
{
/* -------------------------------------------------------------------------- */
/*                        OSPFV3_HELLO_RECEIVED 0                             */
/* -------------------------------------------------------------------------- */
{
/* OSPFV3_NEIGHBOR_DOWN		 		*/		
{(NEIGHBOR_TRANSITION_FUNCTION) ospfv3_process_neighbor_hello_received_event},
/* OSPFV3_NEIGHBOR_ATTEMPT	 		*/		
{(NEIGHBOR_TRANSITION_FUNCTION) ospfv3_process_neighbor_hello_received_event},
/* OSPFV3_NEIGHBOR_INITIALIZING	*/		
{(NEIGHBOR_TRANSITION_FUNCTION) ospfv3_process_neighbor_hello_received_event},
/* OSPFV3_NEIGHBOR_2_WAY				*/		
{(NEIGHBOR_TRANSITION_FUNCTION) ospfv3_process_neighbor_hello_received_event},
/* OSPFV3_NEIGHBOR_EXCHANGE_START	*/		
{(NEIGHBOR_TRANSITION_FUNCTION) ospfv3_process_neighbor_hello_received_event},
/* OSPFV3_NEIGHBOR_EXCHANGE			*/		
{(NEIGHBOR_TRANSITION_FUNCTION) ospfv3_process_neighbor_hello_received_event},
/* OSPFV3_NEIGHBOR_LOADING			*/		
{(NEIGHBOR_TRANSITION_FUNCTION) ospfv3_process_neighbor_hello_received_event},
/* OSPFV3_NEIGHBOR_FULL				*/		
{(NEIGHBOR_TRANSITION_FUNCTION) ospfv3_process_neighbor_hello_received_event}
},

/* -------------------------------------------------------------------------- */
/*                            OSPFV3_START 1                                  */
/* -------------------------------------------------------------------------- */
{
/* OSPFV3_NEIGHBOR_DOWN	 		*/		
{(NEIGHBOR_TRANSITION_FUNCTION) ospfv3_process_neighbor_start_event},
/* OSPFV3_NEIGHBOR_ATTEMPT 		*/		
{NULL},
/* OSPFV3_NEIGHBOR_INITIALIZING		*/		
{NULL},
/* OSPFV3_NEIGHBOR_2_WAY		*/		
{NULL},
/* OSPFV3_NEIGHBOR_EXCHANGE_START	*/		
{NULL},
/* OSPFV3_NEIGHBOR_EXCHANGE		*/
{NULL},
/* OSPFV3_NEIGHBOR_LOADING		*/
{NULL},
/* OSPFV3_NEIGHBOR_FULL			*/
{NULL}
},

/* -------------------------------------------------------------------------- */
/*                          OSPFV3_TWO_WAY_RECEIVED 2                         */
/* -------------------------------------------------------------------------- */
{
/* OSPFV3_NEIGHBOR_DOWN		*/		
{NULL},
/* OSPFV3_NEIGHBOR_ATTEMPT	*/		
{NULL},
/* OSPFV3_NEIGHBOR_INITIALIZING	*/
{(NEIGHBOR_TRANSITION_FUNCTION) ospfv3_process_neighbor_2_way_received_event},
/* OSPFV3_NEIGHBOR_2_WAY				*/	{(NEIGHBOR_TRANSITION_FUNCTION) ospfv3_process_neighbor_2_way_received_event} /* ### */,
/* OSPFV3_NEIGHBOR_EXCHANGE_START	*/		{NULL},
/* OSPFV3_NEIGHBOR_EXCHANGE			*/		{NULL},
/* OSPFV3_NEIGHBOR_LOADING			*/		{NULL},
/* OSPFV3_NEIGHBOR_FULL				*/		{NULL}
},
/* ------------------------------------------------------------------------------------------------------------------ */
/*										     					OSPFV3_NEGOTIATION_DONE          3       			 			 						 */
/* ------------------------------------------------------------------------------------------------------------------ */
{
/* OSPFV3_NEIGHBOR_DOWN		 		*/		{NULL},
/* OSPFV3_NEIGHBOR_ATTEMPT	 		*/		{NULL},
/* OSPFV3_NEIGHBOR_INITIALIZING	*/		{NULL},
/* OSPFV3_NEIGHBOR_2_WAY				*/		{NULL},
/* OSPFV3_NEIGHBOR_EXCHANGE_START	*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospfv3_process_neighbor_negotiation_done_event},
/* OSPFV3_NEIGHBOR_EXCHANGE			*/		{NULL},
/* OSPFV3_NEIGHBOR_LOADING			*/		{NULL},
/* OSPFV3_NEIGHBOR_FULL				*/		{NULL}
},
/* ------------------------------------------------------------------------------------------------------------------ */
/*										     					  OSPFV3_EXCHANGE_DONE          4       			 			 						 */
/* ------------------------------------------------------------------------------------------------------------------ */
{
/* OSPFV3_NEIGHBOR_DOWN		 		*/		{NULL},
/* OSPFV3_NEIGHBOR_ATTEMPT	 		*/		{NULL},
/* OSPFV3_NEIGHBOR_INITIALIZING	*/		{NULL},
/* OSPFV3_NEIGHBOR_2_WAY				*/		{NULL},
/* OSPFV3_NEIGHBOR_EXCHANGE_START	*/		{NULL},
/* OSPFV3_NEIGHBOR_EXCHANGE			*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospfv3_process_neighbor_exchange_done_event},
/* OSPFV3_NEIGHBOR_LOADING			*/		{NULL},
/* OSPFV3_NEIGHBOR_FULL				*/		{NULL}
},
/* ------------------------------------------------------------------------------------------------------------------ */
/*							     					    OSPFV3_BAD_LINK_STATE_REQUEST          5         			 			 					 */
/* ------------------------------------------------------------------------------------------------------------------ */
{
/* OSPFV3_NEIGHBOR_DOWN		 		*/		{NULL},
/* OSPFV3_NEIGHBOR_ATTEMPT	 		*/		{NULL},
/* OSPFV3_NEIGHBOR_INITIALIZING	*/		{NULL},
/* OSPFV3_NEIGHBOR_2_WAY				*/		{NULL},
/* OSPFV3_NEIGHBOR_EXCHANGE_START	*/		{NULL},
/* OSPFV3_NEIGHBOR_EXCHANGE			*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospfv3_tear_down_and_reestablish_adjacency},
/* OSPFV3_NEIGHBOR_LOADING			*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospfv3_tear_down_and_reestablish_adjacency},
/* OSPFV3_NEIGHBOR_FULL				*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospfv3_tear_down_and_reestablish_adjacency}
},
/* ------------------------------------------------------------------------------------------------------------------ */
/*									     					    OSPFV3_LOADING_DONE          6         			 			 						 */
/* ------------------------------------------------------------------------------------------------------------------ */
{
/* OSPFV3_NEIGHBOR_DOWN		 		*/		{NULL},
/* OSPFV3_NEIGHBOR_ATTEMPT	 		*/		{NULL},
/* OSPFV3_NEIGHBOR_INITIALIZING	*/		{NULL},
/* OSPFV3_NEIGHBOR_2_WAY				*/		{NULL},
/* OSPFV3_NEIGHBOR_EXCHANGE_START	*/		{NULL},
/* OSPFV3_NEIGHBOR_EXCHANGE			*/		{NULL},
/* OSPFV3_NEIGHBOR_LOADING			*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospfv3_process_neighbor_loading_done_event},
/* OSPFV3_NEIGHBOR_FULL				*/		{NULL}
},
/* ------------------------------------------------------------------------------------------------------------------ */
/*										     					  OSPFV3_ADJACENCY_OK          7          			 			 						 */
/* ------------------------------------------------------------------------------------------------------------------ */
{
/* OSPFV3_NEIGHBOR_DOWN		 		*/		{NULL},
/* OSPFV3_NEIGHBOR_ATTEMPT	 		*/		{NULL},
/* OSPFV3_NEIGHBOR_INITIALIZING	*/		{NULL},
/* OSPFV3_NEIGHBOR_2_WAY				*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospfv3_start_forming_an_adjacency_with_the_neighbor},
/* OSPFV3_NEIGHBOR_EXCHANGE_START	*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospfv3_maintain_or_destroy_existing_adjacency},
/* OSPFV3_NEIGHBOR_EXCHANGE			*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospfv3_maintain_or_destroy_existing_adjacency},
/* OSPFV3_NEIGHBOR_LOADING			*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospfv3_maintain_or_destroy_existing_adjacency},
/* OSPFV3_NEIGHBOR_FULL				*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospfv3_maintain_or_destroy_existing_adjacency}
},
/* ------------------------------------------------------------------------------------------------------------------ */
/*	  						     					    OSPFV3_SEQUENCE_NUMBER_MISMATCH          8        			 			 					 */
/* ------------------------------------------------------------------------------------------------------------------ */
{
/* OSPFV3_NEIGHBOR_DOWN		 		*/		{NULL},
/* OSPFV3_NEIGHBOR_ATTEMPT	 		*/		{NULL},
/* OSPFV3_NEIGHBOR_INITIALIZING	*/		{NULL},
/* OSPFV3_NEIGHBOR_2_WAY				*/		{NULL},
/* OSPFV3_NEIGHBOR_EXCHANGE_START	*/		{NULL},
/* OSPFV3_NEIGHBOR_EXCHANGE			*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospfv3_tear_down_and_reestablish_adjacency},
/* OSPFV3_NEIGHBOR_LOADING			*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospfv3_tear_down_and_reestablish_adjacency},
/* OSPFV3_NEIGHBOR_FULL				*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospfv3_tear_down_and_reestablish_adjacency}
},
/* ------------------------------------------------------------------------------------------------------------------ */
/*									     					       OSPFV3_ONE_WAY          9         			 			 						 	 */
/* ------------------------------------------------------------------------------------------------------------------ */
{
/* OSPFV3_NEIGHBOR_DOWN		 		*/		{NULL},
/* OSPFV3_NEIGHBOR_ATTEMPT	 		*/		{NULL},
/* OSPFV3_NEIGHBOR_INITIALIZING	*/		{NULL},
/* OSPFV3_NEIGHBOR_2_WAY				*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospfv3_process_neighbor_1_way_received_event},
/* OSPFV3_NEIGHBOR_EXCHANGE_START	*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospfv3_process_neighbor_1_way_received_event},
/* OSPFV3_NEIGHBOR_EXCHANGE			*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospfv3_process_neighbor_1_way_received_event},
/* OSPFV3_NEIGHBOR_LOADING			*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospfv3_process_neighbor_1_way_received_event},
/* OSPFV3_NEIGHBOR_FULL				*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospfv3_process_neighbor_1_way_received_event}
},
/* ------------------------------------------------------------------------------------------------------------------ */
/*									     					    OSPFV3_KILL_NEIGHBOR          10         			 			 						 */
/* -------------------------------------------------------------------------- */
{
/* OSPFV3_NEIGHBOR_DOWN		 		*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospfv3_process_neighbor_down_event},
/* OSPFV3_NEIGHBOR_ATTEMPT	 		*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospfv3_process_neighbor_down_event},
/* OSPFV3_NEIGHBOR_INITIALIZING	*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospfv3_process_neighbor_down_event},
/* OSPFV3_NEIGHBOR_2_WAY				*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospfv3_process_neighbor_down_event},
/* OSPFV3_NEIGHBOR_EXCHANGE_START	*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospfv3_process_neighbor_down_event},
/* OSPFV3_NEIGHBOR_EXCHANGE			*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospfv3_process_neighbor_down_event},
/* OSPFV3_NEIGHBOR_LOADING			*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospfv3_process_neighbor_down_event},
/* OSPFV3_NEIGHBOR_FULL				*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospfv3_process_neighbor_down_event}
},
/* ------------------------------------------------------------------------------------------------------------------ */
/*										     			OSPFV3_INACTIVITY_TIMER          11         			 			 						 	 */
/* ------------------------------------------------------------------------------------------------------------------ */
{
/* OSPFV3_NEIGHBOR_DOWN		 		*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospfv3_process_neighbor_down_event},
/* OSPFV3_NEIGHBOR_ATTEMPT	 		*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospfv3_process_neighbor_down_event},
/* OSPFV3_NEIGHBOR_INITIALIZING	*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospfv3_process_neighbor_down_event},
/* OSPFV3_NEIGHBOR_2_WAY				*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospfv3_process_neighbor_down_event},
/* OSPFV3_NEIGHBOR_EXCHANGE_START	*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospfv3_process_neighbor_down_event},
/* OSPFV3_NEIGHBOR_EXCHANGE			*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospfv3_process_neighbor_down_event},
/* OSPFV3_NEIGHBOR_LOADING			*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospfv3_process_neighbor_down_event},
/* OSPFV3_NEIGHBOR_FULL				*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospfv3_process_neighbor_down_event}
},
/* ------------------------------------------------------------------------------------------------------------------ */
/*								OSPFV3_DOWN_INDICATION_FROM_LOWER_LEVEL_PROTOCOLS_RECEIVED         12      		 						    */
/* ------------------------------------------------------------------------------------------------------------------ */
{
/* OSPFV3_NEIGHBOR_DOWN		 		*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospfv3_process_neighbor_down_event},
/* OSPFV3_NEIGHBOR_ATTEMPT	 		*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospfv3_process_neighbor_down_event},
/* OSPFV3_NEIGHBOR_INITIALIZING	*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospfv3_process_neighbor_down_event},
/* OSPFV3_NEIGHBOR_2_WAY				*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospfv3_process_neighbor_down_event},
/* OSPFV3_NEIGHBOR_EXCHANGE_START	*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospfv3_process_neighbor_down_event},
/* OSPFV3_NEIGHBOR_EXCHANGE			*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospfv3_process_neighbor_down_event},
/* OSPFV3_NEIGHBOR_LOADING			*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospfv3_process_neighbor_down_event},
/* OSPFV3_NEIGHBOR_FULL				*/		{(NEIGHBOR_TRANSITION_FUNCTION) ospfv3_process_neighbor_down_event}
}
};

#ifdef __cplusplus
}
#endif

#endif /* __INCospfv3NeighborStateMachineh */
