/* ospfv3Constants.h - OSPFv3 constants */

/* Copyright 2004 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*
modification history
--------------------
02f,19oct05,xli updated version number to 3.1 from 3.1.1
02e,29jul04,dsk updated version number to 3.0 from 1.0
02d,14apr04,ram Performance optimization changes
02c,13apr04,ram OSPFv3 Routing Table Hash Modifications
02b,15mar04,dsk Mods according to OSPFv3 ANVL conformance testing
02a,16dec03,ram Mods per type 3 & 4 testing
01z,06nov03,agi fixed merge error
01y,06nov03,ram Modifications for external routes
01x,03nov03,agi fixed #ifndef to #ifdef around OSPFV3TOMAPI_TASK_PRIORITY
01w,28oct03,ram Modifications for traffic, aging, LSAs, & cleanup
01v,22oct03,ram Modifications for LS request & separating performance
01u,21oct03,ram Modifications for adjacency establishment
01t,17oct03,ram Modifications for memory corruption, traffic, & debug output
01s,10oct03,ram Modifications for LSDB, dynamic config, & NSSA cleanup
01r,07oct03,agi changed OSPFV3_INTER_AREA_PREFIX_LINK_ADVERTISEMENT_HEADER_SIZE
                to OSPFV3_INTER_AREA_PREFIX_LSA_HEADER_SIZE
01p,07oct03,agi removed conditional flag for debug output
01o,30sep03,ram OSPF datapath changes
01n,25sep03,ram Second general modifications submission
01m,29jul03,ram Added sockets, processing, and external route changes
01i,29jul03,dsk added defines for LSAaggregate offest link, removed opaque defs
01l,29may03,dsk definitions for prioritized hello processing, for
                link availability PDU processing,
                definitions for task creation via vxWorks instead of rwos
                definitions for prefixes and multiple prefixes per link
01k,07mar03,dsk hash table indexes defined as enum
01j,18feb03,dsk byte_order changes (store in host byte order)
01i,18feb03,dsk ported from OSPFv2 to OSPFv3 structures
01h,24jan03,htm added OSPFV3_ADDR6_ALLSPF,OSPFV3_ADDR6_ALLDR
01g,23jan03,agi propogated SPR#78250 fix from OSPFv2
01f,10jan03,agi propogated SPR#85432, 83418 fixes from OSPFv2
                changed definition of BIT_FIELD_16 from int to unsigned short
01e,09jan03,agi added BIT_FIELD_8, BIT_FIELD_16 and BIT_FIELD_32
01d,06jan03,hme added new constants for OSPFv3 External Flag
01c,18dec02,agi corrected value of OSPFV3_VERSION from 2 to 3
01b,14nov02,agi first pass at clean-up
01a,23oct02,agi written
*/

#ifndef __INCospfv3Constantsh
#define __INCospfv3Constantsh

#ifdef __cplusplus
extern "C" {
#endif

#define OSPFV3_LINK_AVAILABLE_PDU
#ifdef OSPFV3_LINK_AVAILABLE_PDU
/* number of link availability PDUs per second */
#define OSPFV3_NUM_LINK_AVAIL_PDU_PER_SECOND    10 /* upto 60 per second,
        as we can only get 60 ticks per second */
#define OSPFV3_CLOCK_TICKS_PER_SECOND   OSPFV3_NUM_LINK_AVAIL_PDU_PER_SECOND
#else
#define OSPFV3_CLOCK_TICKS_PER_SECOND   1
#endif

/* hello prioritized handling start */
#define OSPFV3_BACKLOG_PDU_MAX                  100  /* number of non-hello PDUs queued for handling */
#define OSPFV3_BACKLOG_PDU_CONGESTION_THRESHOLD  40   /* number of non-hello PDUs queued beyond which
                                                        we are in congested mode and apply
                                                        surrogate hello treatment to prevent
                                                        timing out adjacencies */
/* hello prioritized handling end */


/* TODO: replace this with the define from RS*/
#define OSPFV3_SIZE_OF_IPV6_ADDRESS            16
#define OSPFV3_SIZE_OF_IPV6_ADDRESS_32BITWORDS  4

#define OSPFV3_OPTIONS_SIZE_BYTES 3 /* sizeof options field in OSPF PDUs  */
#define OSPFV3_PROTOCOL_ID_VALUE    89
#define OSPFV3_VERSION 3

#define OSPFV3_REV           "3.1"
#define OSPFV3_REV_TYPE        "1"
#define OSPFV3_BUILD             __DATE__

#if !defined (NUMBER_OF_IP_PORTS)
    #define NUMBER_OF_IP_PORTS 16
#endif /* NUMBER_OF_IP_PORTS */

#define MAXIMUM_IP_OPTION_LENGTH                                40              /* Largest option field, bytes */
#define NO_SUCH_PORT                                                0xffff

#if !defined (PRINT_BUFFER_SIZE)
    #define PRINT_BUFFER_SIZE                                       150
#endif /* PRINT_BUFFER_SIZE */


#define RFC1745_EGP_ORIGEGP_ROUTE_TAG 0x9000            /* BGP routes reached via OSPF, ext adv originated by some other ASBR  */      /* 4.3.2 RFC 1745 */
#define RFC1745_IGP_TO_OSPFV3_ROUTE_TAG 0xc000      /* IGP dynamic routes etc. */           /* IGP, local_AS */                /* 4.3.4 RFC 1745 */
#define RFC1745_EGP_ORIGIGP_ROUTE_TAG_PER_AS 0xd000 /* BGP routes with orig IGP, next_hop AS */ /* IGP, Local_AS, Next_hop_as */ /* 4.3.5 RFC 1745 DO not use for now */
#define RFC1745_BGP_TO_OSPFV3_ROUTE_TAG 0xe000      /* BGP origin */
#define OSPFV3_INTERNAL 0xffff

#define     NUMBER_OF_OSPFV3_PORTS      NUMBER_OF_IP_PORTS
#define     NUMBER_OF_OSPFV3_AREAS      NUMBER_OF_IP_PORTS
#define     NUMBER_OF_OSPFV3_AREA_ADDRESS_RANGES    16

#define OSPFV3_NUMBER_OF_INTERFACE_STATES   7
#define OSPFV3_NUMBER_OF_INTERFACE_EVENTS   7

#define OSPFV3_NUMBER_OF_NEIGHBOR_STATES    8
#define OSPFV3_NUMBER_OF_NEIGHBOR_EVENTS    13

#define OSPFV3_BACKBONE 0x00000000L

#define OSPFV3_NBMA_DEFAULT_HELLO                   30
#define OSPFV3_BROADCAST_DEFAULT_HELLO          10
#define OSPFV3_POINT_TO_POINT_DEFAULT_HELLO     30
#define OSPFV3_VIRTUAL_DEFAULT_HELLO                60

/* LSDB hash constants */
#define OSPFV3_HASH_TABLE_SIZE  512
#define OSPFV3_HASH_MASK        511

/* Routing table hash constants */
#define OSPFV3_RT_HASH_TABLE_SIZE 256
#define OSPFV3_RT_HASH_MASK       255

#define OSPFV3_LSRefreshTime        (1800)
#define OSPFV3_MinLSArrival     ((ULONG) 0x00000001)
#define OSPFV3_MinLSInterval        ((ULONG) 0x00000005)
#define OSPFV3_LSInfinity           0xFFFF/* was 0xFFFFFF
dsk:TODO: TBD metrics are either 3 bytes or 2 bytes depending on LSA types,
all must be comparable, so all of them should be short  */

#define OSPFV3_AS_EXTERNAL_INFINITY 0x00FFFFFF

#define NET_TO_HOST TRUE    /* to be used as argument for ospfv3_byte_order_lsa */
#define HOST_TO_NET FALSE   /* to be used as argument for ospfv3_byte_order_lsa */

#define  OSPFV3_EXTERNAL_LSA_QUEUE_PROCESS_INTERVAL 10 /* Routerware Inc. Specific */

#define OSPFV3_CheckAge                     (300)

#define OSPFV3_MAXIMUM_AGE_DIFFERENCE   ((USHORT) 900)
#define OSPFV3_MAXIMUM_AGE          ((USHORT) 3600)

#define OSPFV3_ALL_UP_NEIGHBORS         0x00000001
#define OSPFV3_ALL_ELIGIBLE_NEIGHBORS       0x00000002
#define OSPFV3_ALL_EXCHANGE_NEIGHBORS       0x00000003
#define OSPFV3_DESIGNATED_ROUTER_AND_BACKUP_DESIGNATED_ROUTER   0x00000004
#define OSPFV3_UNNUMBERED                   0x00000005
#define OSPFV3_ADDR_ALLSPF                  0x00000006
#define OSPFV3_ADDR_ALLDR                   0x00000007


/* AllSPFRouters, and AllDRRouters, as per RFC 2740, section A1, page 46,
in address string format and in in6_addr format (to avoid repetitive conversion) */
#define OSPFV3_ADDR6_ALLSPF "FF02::5"
#define OSPFV3_ADDR6_ALLDR  "FF02::6"
#define OSPFV3_ADDR6_ALLSPF_BYTES   ospfv3_addr6_allspf    /* "FF02::5" in in6_addr format */
#define OSPFV3_ADDR6_ALLDR_BYTES    ospfv3_addr6_alldr     /* "FF02::6" in in6_addr format */


/* TODO: determine what are the IPv6 equivalent addresses of multicast allspf and alldr,
their definitions in IPv4 was: ALL_SPF = bit 20 set, ALL_DR = bit 21 set:
#define OSPF_IF_MULTICAST_ALLSPF        0x100000
#define OSPF_IF_MULTICAST_ALLDR         0x200000
For now, define them same as the general ALLSPF and ALLDR IPv6 addresses
 */
#define OSPFV3_IF_MULTICAST_ALLSPF              OSPFV3_ADDR6_ALLSPF /* Joined All SPF group */
#define OSPFV3_IF_MULTICAST_ALLDR               OSPFV3_ADDR6_ALLDR  /* Joined All DR group */
#define OSPFV3_IF_MULTICAST_ALLSPF_BYTES        OSPFV3_ADDR6_ALLSPF_BYTES   /* Joined All SPF group */
#define OSPFV3_IF_MULTICAST_ALLDR_BYTES         OSPFV3_ADDR6_ALLDR_BYTES    /* Joined All DR group */

#define OSPFV3_IP_TTL_FOR_OSPFV3_PACKET 0x01      /* RFC 1583 Appendix A.1 */
#define OSPFV3_IP_TTL_FOR_UNI_CAST_VLINK_OSPFV3_PACKET 0x80   /* VLINK: Virtual Link*/

#define OSPFV3_DefaultDestination 0x00000000L
#define OSPFV3_DefaultMask        0x00000000L

#define OSPFV3_DEFAULT_TRANSMIT_DELAY   1
#define OSPFV3_DEFAULT_COST     1
#define OSPFV3_DEFAULT_ROUTER_DEAD_INTERVAL 40
#define OSPFV3_DEFAULT_PRIORITY 1
#define OSPFV3_DEFAULT_RETRANSMIT_INTERVAL  5   /* was 5  */

#define OSPFV3_DEFAULT_POLL_INTERVAL         120

#define OSPFV3_ACKNOWLEDGEMENT_INTERVAL     2

#define OSPFV3_ROUTING_TABLE_BUILD_INTERVAL 2  /* WAS 10 */

#define OSPFV3_MAXIMUM_PACKET_SIZE_FOR_VIRTUAL_LINK     512 /* does not include the IP header */

#define OSPFV3_PSEUDO_HEADER_SIZE   40          /* size of upper layer protocol IPv6 pseudo-header,
                                                    as per RFC 2740, page 49 and
                                                    as per RFC 2640 "IPv6 Specification", chapter 8.1 */
#define OSPFV3_ID_SIZE ((ULONG) 4)              /* size of an ID: router ID, interface ID, etc, in OSPFv2 = 4 bytes */
#define OSPFV3_PACKET_SIZE ((ULONG) 16)         /* sizeof OSPF packet header (RFC 2740, annex A.3.1, page 48) = 16 bytes */
#define OSPFV3_HELLO_HEADER_SIZE ((ULONG) 20)   /* sizeof OSPF hello packet between PDU header and variable list part = 20 bytes*/
#define OSPFV3_DATABASE_HEADER_SIZE ((ULONG) 12)/* sizeof OSPF DD packet, between PDU header and variable list part = 12 bytes */
#define OSPFV3_LS_HEADER_SIZE ((ULONG) 20)      /* sizeof OSPF LSA header, RFC 2740, annex A.4.2, page 58 = 20 bytes */
#define OSPFV3_LS_UPDATE_HEADER_SIZE ((ULONG) 4)/* sizeof OSPF LSU packet, between PDU header and variable list part = 4 bytes */

#define OSPFV3_ROUTER_LINK_ADVERTISEMENT_HEADER_SIZE ((ULONG) 24)
#define OSPFV3_NETWORK_LINK_ADVERTISEMENT_HEADER_SIZE ((ULONG) 24)
#define OSPFV3_SUMMARY_LINK_ADVERTISEMENT_HEADER_SIZE ((ULONG) 28)
#define OSPFV3_INTER_AREA_PREFIX_LSA_HEADER_SIZE ((ULONG) 44)
#define OSPFV3_INTER_AREA_PREFIX_LSA_FIXED_HEADER_SIZE ((ULONG) 28)
#define OSPFV3_INTER_AREA_ROUTER_LSA_HEADER_SIZE ((ULONG) 32)
#define OSPFV3_EXTERNAL_LINK_ADVERTISEMENT_FIXED_HEADER_SIZE ((ULONG) 28)
#define OSPFV3_EXTERNAL_LINK_ADVERTISEMENT_HEADER_SIZE ((ULONG) 68)
#define OSPFV3_LOCAL_LINK_ADVERTISEMENT_FIXED_HEADER_SIZE ((ULONG) 44)
#define OSPFV3_INTRA_AREA_LINK_ADVERTISEMENT_FIXED_HEADER_SIZE ((ULONG) 32)

#define OSPFV3_DB_PIECE_SIZE    ((ULONG) 20)
#define OSPFV3_LS_REQUESTED_ADVERTISEMENT_SIZE ((ULONG) 12)

#define OSPFV3_ROUTER_LINK_PIECE_SIZE  ((ULONG) 16) /* was 12 in OSPFv2 */
#define OSPFV3_NETWORK_LINK_PIECE_SIZE ((ULONG) 4)

#define OSPFV3_EXTERNAL_LINK_METRIC_PIECE_SIZE ((ULONG) 12)
#define OSPFV3_ROUTER_LINK_METRIC_PIECE_SIZE ((ULONG) 4)

#define OSPFV3_HOST_NET_MASK                0xFFFFFFFFL
#define OSPFV3_HOST_NET_MASK_LENGTH         128
#define OSPFV3_PREFIX_METRIC_MASK           0x0000FFFFL

#define OSPFV3_ASE_bit_E    0x04000000
#define OSPFV3_ASE_bit_F    0x02000000
#define OSPFV3_ASE_bit_T    0x01000000

#define OSPFV3_NUMBER_OF_INLINE_COMPUTATIONS        8
#define OSPFV3_MASK_FOR_UNEVEN_BITS                 (OSPFV3_NUMBER_OF_INLINE_COMPUTATIONS - 1)
#define OSPFV3_INLINED_SHIFT                            3
#define OSPFV3_FINAL_CHECKSUM_SHIFT                 8
#define OSPFV3_NUMBER_OF_ITERATIONS_BEFORE_MOD  4096
#define OSPFV3_LOG2_OF_NUMBER_OF_ITERATIONS     12
#define OSPFV3_MOD_MASK                                 (OSPFV3_NUMBER_OF_ITERATIONS_BEFORE_MOD - 1)
#define OSPFV3_NUMBER_OF_INLINE_ITERATIONS      (OSPFV3_NUMBER_OF_ITERATIONS_BEFORE_MOD/OSPFV3_NUMBER_OF_INLINE_COMPUTATIONS)
#define OSPFV3_MODULUS                                  255

/* masks for Options bits as defined in Annex A.2, RFC 2740 */
#define OSPFV3_ENABLE_EXTERNAL_ROUTING 0x02
#define OSPFV3_NOT_ENABLE_EXTERNAL_ROUTING 0x00

#define OSPFV3_STARTING_AREA_ID_FOR_VIRTUAL_INTERFACE 0xff000000


#define OSPFV3_PRINTF_PACKET ospfv3_printf
#define OSPFV3_PRINTF_INTERFACE ospfv3_printf
#define OSPFV3_PRINTF_NEIGHBOR ospfv3_printf
#define OSPFV3_PRINTF_MEMORY    ospfv3_printf
#define OSPFV3_PRINTF_DEBUG ospfv3_printf
#define OSPFV3_PRINTF_ROUTING_TABLE ospfv3_printf
#define OSPFV3_PRINTF_ALARM ospfv3_printf
#define OSPFV3_PRINTF_SNMP ospfv3_printf
#define OSPFV3_PRINTF_RTM ospfv3_printf
#define OSPFV3_PRINTF_DB_OVERFLOW ospfv3_printf
#define OSPFV3_PRINTF_PROLOGUE ospfv3_printf


/* TODO: obsolete rwos function convert_ip_address_to_dot_format not ported to IPv6 and dupplicated by router stack's inet_ntop */
    /* for printing 32 bit IDs in IPv4 dot format, use convert_ip_address_to_dot_format */
#define OSPFV3_CONVERT_IP_ADDRESS_TO_DOT_FORMAT_FOR_DEBUG ospfv3_convert_ipv6_address_to_dot_format
#define OSPFV3_CONVERT_IP_ADDRESS_TO_DOT_FORMAT_FOR_ALARM ospfv3_convert_ipv6_address_to_dot_format
#define OSPFV3_CONVERT_IP_ADDRESS_TO_DOT_FORMAT_FOR_ROUTING_TABLE ospfv3_convert_ipv6_address_to_dot_format

#define ROUTE_MATCH_WITH_ADDRESS_RANGE  1
#define ROUTE_SUBSUMED_BY_ADDRESS_RANGE 2
#define RANGE_STATUS_DO_NOT_ADVERTISE   3


/* XXX - BIT_FIELD is now obsolete and should be removed */
#define BIT_FIELD(enum_,enum_name) unsigned char

#define BIT_FIELD_8(enum_,enum_name) unsigned char
#define BIT_FIELD_16(enum_,enum_name) unsigned short
#define BIT_FIELD_32(enum_,enum_name) unsigned int


#define MAXIMUM_IP_OPTION_LENGTH                                40              /* Largest option field, bytes */

#define NUMBER_OF_IP_PORTS      16  /* Legacy Constant from old IP*/
#define MAXIMUM_ETHERNET_DATA_SIZE 1500     /* Legacy Constant from old v8022str.h */

#define RTM_HANDLE ULONG



#if (_BYTE_ORDER == _LITTLE_ENDIAN )
    #define ospfv3_min min
#else
    #define ospfv3_min(arg1,arg2)  (arg1 < arg2 ? arg1 : arg2)
#endif

/*raw socket maximum receive ospf packet jkw*/
#define MAX_BUFSIZE  8192
/*raw socket end of change*/

/*RFC 1765*/
#define OSPFV3_DEFAULT_EXTERNAL_LSDB_LIMIT -1;
#define OSPFV3_DEFAULT_EXIT_OVERFLOW_TIMER 0;

#define OSPFV3_1K_BLOCKS 1024

#define OSPFV3_TIMER_TASK_STACK_SIZE (8 * OSPFV3_1K_BLOCKS)
#define OSPFV3_SPF_TASK_STACK_SIZE   (10 * OSPFV3_1K_BLOCKS)
#define OSPFV3_INPUT_TASK_STACK_SIZE (12 * OSPFV3_1K_BLOCKS)
#define OSPFV3_RTM_TASK_STACK_SIZE   (6 * OSPFV3_1K_BLOCKS)
#define OSPFV3_MAPI_TASK_STACK_SIZE  (4 * OSPFV3_1K_BLOCKS)

#define OSPFV3_TIMER_TASK_PRIORITY   100
#define OSPFV3_SPF_TASK_PRIORITY     110 /* lower priority than timer task */
#define OSPFV3_INPUT_TASK_PRIORITY    90 /* high priority, almost same as tNetTask (50), just drain socket, no processing */
#define OSPFV3_RTM_TASK_PRIORITY      90 /* high priority, almost same as tNetTask (50), just drain socket, no processing */
#define OSPFV3TOMAPI_TASK_PRIORITY   150 /* 150, lower priority, human speed not machine speed */

#define OSPFV3_AS_EXTERNAL_FLAG_T 0x01
#define OSPFV3_AS_EXTERNAL_FLAG_F 0x02
#define OSPFV3_AS_EXTERNAL_FLAG_E 0x04

#define OSPFV3_EXTERNAL_METRIC_TYPE_1 1
#define OSPFV3_EXTERNAL_METRIC_TYPE_2 2
#define OSPFV3_EXTERNAL_DEFAULT_METRIC 20

#define MAX_EXTERNAL_ROUTES_PROCESSED 250

    /* used to be RWOS_MAXIMUM_FILE_SIZE */
#ifdef INCLUDE_TMS_MODS
#define OSPFV3_MAXIMUM_FILE_SIZE                    512
#else
#define OSPFV3_MAXIMUM_FILE_SIZE                    4000
#endif /* INCLUDE_TMS_MODS */

/* maximum number of PDUs handled by SPF task
    before we verify if SPF needs to be built */
#define OSPFV3_MAX_NUM_PDUS_PROCESSED_ONCE          20
#define OSPFV3_RT_PREFIX_BLOCK_SIZE        256 /* routing table prefix block size,
            space for at least 15 prefixes per link, 4 + 20 bytes per uncompressed prefix */
#define OSPFV3_RT_MAX_NUM_PREFIXES_PER_LINK     10
#define OSPFV3_PREFIX_INFO_SIZE                 4 /*1 byte prefix length, 1 byte prefix options, 2 bytes metric */
#define OSPFV3_ANY_LINK_ID              0xFFFFFFFFL /* used for LSA link ID search, means any link ID */


#define OSPFV3_LS_TYPE_MASK             0x00FF /* ushort mask to extract LSA type
                                                  excluding flooding scope bits from LSA type field */

#define OSPFV3_U_BIT_MASK 0x80

#define OSPFV3_AGGREGATE_LINKS_OFFSET   8 /* sizeof(OSPFV3_GENERIC_NODE)
        This constant defines offset of "links_lsa_aggregate" field in
        "OSPFV3_LS_DATABASE_ENTRY" structure */

/* OSPFv3 route weight in routing table */
#define OSPFV3_ROUTE_WEIGHT               110

/* Raw & routing socket receive buffer size */
#ifndef OSPFV3_RS_RCV_BUFFER
#define OSPFV3_RS_RCV_BUFFER              (8 * 1024)
#endif

#ifndef OSPFV3_RAW_RCV_BUFFER
#define OSPFV3_RAW_RCV_BUFFER             (8 * 1024)
#endif

/* OSPFv3 Performance enhancing code - not test/needed for early release*/
#undef OSPFV3_LINK_AVAILABLE_PDU
#undef OSPFV3_PRIORITY_QUEUE_PROCESSING
#undef OSPFV3_SURROGATE_HELLO_ENABLED

#define OSPFV3_CLEANUP_INTERVAL         (30 * 60) /* 30 minutes*/


#ifdef __cplusplus
}
#endif




#endif /* __INCospfv3Constantsh */
