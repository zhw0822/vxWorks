/* ospfv3MapiHelper.h - header file for OSPFv3-MIB Helper Module */

/* Copyright 2004 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/* 
 * Modification history
 * --------------------
01f,27oct04,xli add ospf multicast extension struct
01e,25jun04,xli change the commits for oid enum 
01d,30may03,xli change the ospfv3MulticastExtension data type from 
                integer to char*
01c,29may03,dsk included Xinzhi's changes from integration with Xinzhi 
01b,10jan03,xli Add OSPFv3 specific enum value, data structures
                and method routines 
01a,16nov02,xli Initial file creation.
 */

/*
DESCRIPTION:

This file defines the draft-ietf-ospf-ospfv3-mib-05 MIB specific 
Manamgenet Interface Local ID enumerations, the MIB object enumerations, 
the MIB object sizes and some MIB API helper function prototypes.
*/
 
#ifndef __INCospfv3MapiHelper_h
#define __INCospfv3MapiHelper_h

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/***********************************************************************
 * Management Interface draft-ietf-ospf-ospfv3-mib-05 Definitions
 */

/* OSPFv3 Version Number */
#define OSPFV3_VERSION         3 

/* OSPFv3 Area Backbone, ospfv3AreaId = 0.0.0.0 */
#define OSPFV3_BACKBONE_AREA   00000000L

#define MIN_OSPFV3_EXT_LSDB_LIMIT               -1L        
#define MAX_OSPFV3_EXT_LSDB_LIMIT               2147483647L
#define MAX_OSPFV3_EXIT_OVERFLOW_INTERVAL       2147483647L
#define MAX_OSPFV3_PRIORITY                     255L
#define MAX_OSPFV3_POLL_INTERVAL                2147483647L
#define MAX_OSPFV3_AGE                          3600L
#define MIN_OSPFV3_HELLO_INTERVAL               1L           
#define MAX_OSPFV3_HELLO_INTERVAL               65535L       
#define MAX_OSPFV3_DEAD_INTERVAL                2147483647L 
#define MAX_EXTERNAL_LSDB_SIZE                  36L

/* draft-ietf-ospf-ospfv3-mib-05 MIB default value */
#define DEFVAL_mApiOspfv3ExtAreaLsdbLimit        -1L
#define DEFVAL_mApiOspfv3ExitOverflowInterval     0

#define DEFVAL_mApiOspfv3ImportAsExtern                      1
#define DEFVAL_mApiOspfv3AreaSummary                         1
#define DEFVAL_mApiOspfv3AreaNssaTranslatorRole              2
#define DEFVAL_mApiOspfv3AreaNssaTranslatorStabilityInterval 40
#define DEFVAL_mApiOspfv3IfAreaId                            00000000L
#define DEFVAL_mApiOspfv3IfAdminStat                         1
#define DEFVAL_mApiOspfv3IfRtrPriority                       1
#define DEFVAL_mApiOspfv3IfTransitDelay                      1
#define DEFVAL_mApiOspfv3IfRetransInterval                   5
#define DEFVAL_mApiOspfv3IfHelloInterval                     10
#define DEFVAL_mApiOspfv3IfRtrDeadInterval                   40
#define DEFVAL_mApiOspfv3IfPollInterval                      120
#define DEFVAL_mApiOspfv3IfMulticastForwarding               1
#define DEFVAL_mApiOspfv3IfDemand                            2
#define DEFVAL_mApiOspfv3VirtIfTransitDelay                  1
#define DEFVAL_mApiOspfv3VirtIfRetransInterval               5
#define DEFVAL_mApiOspfv3VirtIfHelloInterval                 10
#define DEFVAL_mApiOspfv3VirtIfHelloInterval                 10
#define DEFVAL_mApiOspfv3VirtIfRtrDeadInterval               60
#define DEFVAL_mApiOspfv3NbmaNbrPriority                     1
#define DEFVAL_mApiOspfv3NbmaNbaStorageType                  3

#define DEFVAL_mApiHostNetMask                 0xFFFFFFFF

/* WindNet OSPFv3 default value */
#define DEFVAL_mApiOspfv3IfType  EmApiOspfv3IfType_broadcast


/* the following defines provides the number of sub-identifier 
 *(the instance length) of each table in draft-ietf-ospf-ospfv3-mib-05 MIB  
 */
#define OSPFV3_AREA_INSTANCE_LEN                  4
#define OSPFV3_AS_LSDB_INSTANCE_LEN               9
#define OSPFV3_AREA_LSDB_INSTANCE_LEN             13
#define OSPFV3_LINK_LSDB_INSTANCE_LEN             10
#define OSPFV3_HOST_INSTANCE_LEN                  17
#define OSPFV3_INTF_INSTANCE_LEN                  1 
#define OSPFV3_VIRT_INTF_INSTANCE_LEN             8
#define OSPFV3_NBR_INSTANCE_LEN                   5
#define OSPFV3_NBMA_NBR_INSTANCE_LEN              18     
#define OSPFV3_VIRT_NBR_INSTANCE_LEN              8
#define OSPFV3_AREA_AGGREGATE_INSTANCE_LEN        23                                                                                                 

/*************************************************************************
* Management Interface 1-based local ID enumerations for 
* draft-ietf-ospf-ospfv3-mib-05 MIB.
*/
typedef enum {

    mApiOspfv3RouterId = 1,
    mApiOspfv3AdminStat,
    mApiOspfv3VersionNumber,  
    mApiOspfv3AreaBdrRtrStatus, 
    mApiOspfv3ASBdrRtrStatus, 
    mApiOspfv3AsScopeLsaCount, 
    mApiOspfv3AsScopeLsaCksumSum,
    mApiOspfv3OriginateNewLsas, 
    mApiOspfv3RxNewLsas,    
    mApiOspfv3ExtAreaLsdbLimit,  
    mApiOspfv3MulticastExtensions,  
    mApiOspfv3ExitOverflowInterval,  
    mApiOspfv3DemandExtensions,  
    mApiOspfv3TrafficEngineeringSupport,

    mApiOspfv3AreaId,  /* mApiOspfv3_t enumeration value = 15 */ 
    mApiOspfv3ImportAsExtern,        
    mApiOspfv3SpfRuns,              
    mApiOspfv3AreaBdrRtrCount,     
    mApiOspfv3AsBdrRtrCount,      
    mApiOspfv3AreaScopeLsaCount, 
    mApiOspfv3AreaScopeLsaCksumSum,     
    mApiOspfv3AreaSummary,             
    mApiOspfv3AreaStatus,             
    mApiOspfv3StubMetric,            
    mApiOspfv3AreaNssaTranslatorRole, 
    mApiOspfv3AreaNssaTranslatorState,   
    mApiOspfv3AreaNssaTranslatorStabilityInterval,  
    mApiOspfv3AreaNssaTranslatorEvents,            

    mApiOspfv3AsLsdbType, /* mApiOspfv3_t enumeration value = 29 */ 
    mApiOspfv3AsLsdbRouterId,   
    mApiOspfv3AsLsdbLsid,                             
    mApiOspfv3AsLsdbSequence,  
    mApiOspfv3AsLsdbAge,      
    mApiOspfv3AsLsdbChecksum,              
    mApiOspfv3AsLsdbAdvertisement,        
        
    mApiOspfv3AreaLsdbAreaId,  /* mApiOspfv3_t enumeration value = 36 */ 
    mApiOspfv3AreaLsdbType,         
    mApiOspfv3AreaLsdbRouterId,    
    mApiOspfv3AreaLsdbLsid,       
    mApiOspfv3AreaLsdbSequence,  
    mApiOspfv3AreaLsdbAge,      
    mApiOspfv3AreaLsdbChecksum,
    mApiOspfv3AreaLsdbAdvertisement,     
       
    mApiOspfv3LinkLsdbIfIndex,  /* mApiOspfv3_t enumeration value = 44 */ 
    mApiOspfv3LinkLsdbType,              
    mApiOspfv3LinkLsdbRouterId,         
    mApiOspfv3LinkLsdbLsid,            
    mApiOspfv3LinkLsdbSequence,       
    mApiOspfv3LinkLsdbAge,           
    mApiOspfv3LinkLsdbChecksum,     
    mApiOspfv3LinkLsdbAdvertisement,  
         
    mApiOspfv3HostAddressType, /* mApiOspfv3_t enumeration value = 52 */ 
    mApiOspfv3HostAddress,     
    mApiOspfv3HostMetric,     
    mApiOspfv3HostStatus,    
    mApiOspfv3HostAreaID,   

    mApiOspfv3IfIndex, /* mApiOspfv3_t enumeration value = 57 */        
    mApiOspfv3IfAreaId,           
    mApiOspfv3IfType,            
    mApiOspfv3IfAdminStat,      
    mApiOspfv3IfRtrPriority,   
    mApiOspfv3IfTransitDelay,                 
    mApiOspfv3IfRetransInterval,             
    mApiOspfv3IfHelloInterval,              
    mApiOspfv3IfRtrDeadInterval,           
    mApiOspfv3IfPollInterval,             
    mApiOspfv3IfState,                   
    mApiOspfv3IfDesignatedRouter,       
    mApiOspfv3IfBackupDesignatedRouter,  
    mApiOspfv3IfEvents,                 
    mApiOspfv3IfStatus,                
    mApiOspfv3IfMulticastForwarding,  
    mApiOspfv3IfDemand,               
    mApiOspfv3IfMetricValue,         
    mApiOspfv3IfLinkScopeLsaCount,  
    mApiOspfv3IfLinkLsaCksumSum,   
    mApiOspfv3IfInstId,           
         
    mApiOspfv3VirtIfAreaId, /* mApiOspfv3_t enumeration value = 78 */                
    mApiOspfv3VirtIfNeighbor,              
    mApiOspfv3VirtIfIndex,                
    mApiOspfv3VirtIfTransitDelay,        
    mApiOspfv3VirtIfRetransInterval,    
    mApiOspfv3VirtIfHelloInterval,     
    mApiOspfv3VirtIfRtrDeadInterval,  
    mApiOspfv3VirtIfState,           
    mApiOspfv3VirtIfEvents,         
    mApiOspfv3VirtIfStatus,        
    mApiOspfv3VirtIfLinkScopeLsaCount,   
    mApiOspfv3VirtIfLinkLsaCksumSum,    
         
    mApiOspfv3NbrIfIndex, /* mApiOspfv3_t enumeration value = 90 */       
    mApiOspfv3NbrRtrId,           
    mApiOspfv3NbrAddressType,    
    mApiOspfv3NbrAddress,       
    mApiOspfv3NbrOptions,      
    mApiOspfv3NbrPriority,    
    mApiOspfv3NbrState,      
    mApiOspfv3NbrEvents,    
    mApiOspfv3NbrLsRetransQLen,       
    mApiOspfv3NbrHelloSuppressed,    
    mApiOspfv3NbrIfId,              

    mApiOspfv3NbmaNbrIfIndex,  /* mApiOspfv3_t enumeration value = 101 */        
    mApiOspfv3NbmaNbrAddressType,       
    mApiOspfv3NbmaNbrAddress,          
    mApiOspfv3NbmaNbrPriority,        
    mApiOspfv3NbmaNbrRtrId,          
    mApiOspfv3NbmaNbrState,         
    mApiOspfv3NbmaNbrStorageType,  
    mApiOspfv3NbmaNbrStatus,      
          
    mApiOspfv3VirtNbrArea,  /* mApiOspfv3_t enumeration value = 109 */              
    mApiOspfv3VirtNbrRtrId,               
    mApiOspfv3VirtNbrIfIndex,            
    mApiOspfv3VirtNbrAddressType,       
    mApiOspfv3VirtNbrAddress,            
    mApiOspfv3VirtNbrOptions,           
    mApiOspfv3VirtNbrState,            
    mApiOspfv3VirtNbrEvents,          
    mApiOspfv3VirtNbrLsRetransQLen,  
    mApiOspfv3VirtNbrHelloSuppressed,  
    mApiOspfv3VirtNbrIfId,            

    mApiOspfv3AreaAggregateAreaID, /* mApiOspfv3_t enumeration value = 120 */    
    mApiOspfv3AreaAggregateAreaLsdbType,    
    mApiOspfv3AreaAggregatePrefixType,     
    mApiOspfv3AreaAggregatePrefix,        
    mApiOspfv3AreaAggregatePrefixLength, 
    mApiOspfv3AreaAggregateStatus,      
    mApiOspfv3AreaAggregateEffect     

} mApiOspfv3_t;

/****************************************************************************
* Manamgenet Interface draft-ietf-ospf-ospfv3-mib-05 MIB object enumerations
*/
typedef enum { 
        unknown = 0, 
        ipv4    = 1, 
        ipv6    = 2, 
        ipv4z   = 3,
        ipv6z   = 4,   
        dns     = 16 
} inetAddressType_t;


typedef enum {
    EmApiOspfv3_true  = 1,
    EmApiOspfv3_false = 2
} mApiOspfv3TrueValue_t;

typedef enum {
    EmApiOspfv3_enabled  = 1,
    EmApiOspfv3_disabled = 2
} mApiOspfv3Status_t;

/* Row Status enumerations. This is the same for all tables with the 
 * rowStatus object 
 */
typedef enum {
    EmApiOspfv3RowStatus_active        = 1,
    EmApiOspfv3RowStatus_notInService  = 2,
    EmApiOspfv3RowStatus_notReady      = 3,
    EmApiOspfv3RowStatus_createAndGo   = 4,
    EmApiOspfv3RowStatus_createAndWait = 5,
    EmApiOspfv3RowStatus_destroy       = 6
} mApiOspfv3RowStatus_t;

typedef enum {
    EmApiOspfv3ImportAsExtern_importExternal   = 1,
    EmApiOspfv3ImportAsExtern_importNoExternal = 2,
    EmApiOspfv3ImportAsExtern_importNssa       = 3
} mApiOspfv3AreaImportAsType_t;

typedef enum {
    EmApiOspfv3AreaSummary_noAreaSummary   = 1,
    EmApiOspfv3AreaSummary_sendAreaSummary = 2
} mApiOspfv3AreaSummary_t;

typedef enum {
EmApiOspfv3AreaNssaTranslatorState_enable  = 1,
EmApiOspfv3AreaNssaTranslatorState_elected = 2,
EmApiOspfv3AreaNssaTranslatorState_disable = 3
} EmApiOspfv3AreaNssaTranslatorState_t;

typedef enum {
    EmApiOspfv3LinkLsdbType_linkLsa            = 8,
    EmApiOspfv3AreaLsdbType_routerLsa          = 8193,
    EmApiOspfv3AreaLsdbType_networkLsa         = 8194,
    EmApiOspfv3AreaLsdbType_interAreaPrefixLsa = 8195,
    EmApiOspfv3AreaLsdbType_interAreaRouterLsa = 8196,
    EmApiOspfv3AreaLsdbType_groupMembershipLsa = 8198,
    EmApiOspfv3AreaLsdbType_nssaExternalLsa    = 8199,
    EmApiOspfv3AreaLsdbType_intraAreaPrefixLsa = 8201,
    EmApiOspfv3AsLsdbType_asExternalLink       = 16389
} mApiOspfv3LsdbType_t;

typedef enum {
    EmApiOspfv3IfType_broadcast         = 1,
    EmApiOspfv3IfType_nbma              = 2,
    EmApiOspfv3IfType_pointToPoint      = 3,
    EmApiOspfv3IfType_pointToMultipoint = 5
} mApiOspfv3IfType_t;


typedef enum {
    EmApiOspfv3IfState_down                   = 1,
    EmApiOspfv3IfState_loopback               = 2,
    EmApiOspfv3IfState_waiting                = 3,
    EmApiOspfv3IfState_pointToPoint           = 4,
    EmApiOspfv3IfState_designatedRouter       = 5,
    EmApiOspfv3IfState_backupDesignatedRouter = 6,
    EmApiOspfv3IfState_otherDesignatedRouter  = 7
} mApiOspfv3IfState_t;


typedef enum {
    EmApiOspfv3IfMulticastForwarding_blocked   = 1,
    EmApiOspfv3IfMulticastForwarding_multicast = 2,
    EmApiOspfv3IfMulticastForwarding_unicast   = 3
} mApiOspfv3IfMcastType_t;

typedef enum {
    EmApiOspfv3VirtIfState_down         = 1,
    EmApiOspfv3VirtIfState_pointToPoint = 4
} mApiOspfv3VirtIfState_t;

typedef enum {
    EmApiOspfv3NbrState_down          = 1,
    EmApiOspfv3NbrState_attempt       = 2,
    EmApiOspfv3NbrState_init          = 3,
    EmApiOspfv3NbrState_twoWay        = 4,
    EmApiOspfv3NbrState_exchangeStart = 5,
    EmApiOspfv3NbrState_exchange      = 6,
    EmApiOspfv3NbrState_loading       = 7,
    EmApiOspfv3NbrState_full          = 8
} mApiOspfv3NbrState_t;

typedef enum {
               EmApiOspfv3NbrStorageType_other       = 1,
               EmApiOspfv3NbrStorageType_volatile    = 2,
               EmApiOspfv3NbrStorageType_nonVolatile = 3,
               EmApiOspfv3NbrStorageType_permanent   = 4,
               EmApiOspfv3NbrStorageType_readOnly    = 5
} mApiOspfv3NbrStorageType_t;


typedef enum {
    EmApiOspfv3AreaAggregateEffect_advertiseMatching      = 1,
    EmApiOspfv3AreaAggregateEffect_doNotAdvertiseMatching = 2
} mApiOspfv3AreaAggregateEffect_t;

/* mApiOspfv3ClientType_t enumeration type is used to identify the 
 * type of client that attaches to the mApiOspfv3Area_t or mApiOspfv3If_t.
 * It is used when creating an mApiOspfv3Client_t node
 */
typedef enum {
    EmApiOspfv3ClientType_intf  = 1,  /* client is physical interface */
    EmApiOspfv3ClientType_vintf = 2,  /* client is virtual interface  */
    EmApiOspfv3ClientType_host  = 3,  /* client is host interface     */
    EmApiOspfv3ClientType_nbr   = 4,  /* client is neighbor           */
    EmApiOspfv3ClientType_ag          /* client is area aggregation   */

} mApiOspfv3ClientType_t;


/***********************************************************************
 * Mamagement Interface draft-ietf-ospf-ospfv3-mib-05 Data Structure.
 */

/* mApiOspfv3Client_t is used by mApiOspfv3Area_t to track instances of
 * interface, virtual interface and host interface that are attached to
 * this area. It is also used by mApiOspfv3If_t to track instances of 
 * neighbor that are associated with the interface. The <clientOldState> 
 * mApiOspfv3RowStatus_t enumeration type is only used by mApiOspfv3Area_t
 * to remember the existing rowStatus of the clients before it forces the 
 * client to make the rowStatus state transition due to the rowStatus 
 * changes occur in the mApiOspfv3Area_t itself
 */
typedef struct mApiOspfv3Client
{
   NODE      node;       /* linked list node */
   void     *pClient;    /* pointer to client data structure */
   int       oldStatus;  /* previous row stauts of the attached client */
   mApiOspfv3ClientType_t 
             clientType; /* the type of client */
} mApiOspfv3Client_t;


/* OSPFv3 General Group variables
 */
typedef struct mApiospfv3MulticastExtensions
{
    ulong_t  intraAreaMulticast:1; 
    ulong_t  interAreaMulticast:1; 
    ulong_t  interAsMulticast:1;
    ulong_t  unsued:5;      

} mApiospfv3MulticastExtensions_t;

typedef struct mApiOspfv3GenGroup
{
    ulong_t               ospfv3RouterId;                 /* read-write */
    mApiOspfv3Status_t    ospfv3AdminStat;                /* read-write */
    ulong_t               ospfv3VersionNumber;            /* read-only */
    mApiOspfv3TrueValue_t ospfv3AreaBdrRtrStatus;         /* read-only */ 
    mApiOspfv3TrueValue_t ospfv3ASBdrRtrStatus;           /* read-write */
    ulong_t               ospfv3AsScopeLsaCount;          /* read-only */
    ulong_t               ospfv3AsScopeLsaCksumSum;       /* read-only */
    ulong_t               ospfv3OriginateNewLsas;         /* read-only */
    ulong_t               ospfv3RxNewLsas;                /* read-only */
    long                  ospfv3ExtAreaLsdbLimit;         /* read-write */
    mApiospfv3MulticastExtensions_t 
                          ospfv3MulticastExtensions;      /* read-only */
    ulong_t               ospfv3ExitOverflowInterval;     /* read-write */
    ushort                ospfv3MulticastExtensionsLen;
    /* the following read-write objects are implemented as read-only */

    mApiOspfv3TrueValue_t ospfv3DemandExtensions;          /* read-only */
    mApiOspfv3TrueValue_t ospfv3TrafficEngineeringSupport; /* read-only */
    

} mApiOspfv3GenGroup_t;

/* an instance of mApiOspfv3AreaClass_t object will always be created each time a new
 * OSPFv3 Area is added to the Management Database.
 */
typedef struct mApiOspfv3Area
{
    AVL_NODE      node;               /* AVL Tree node */
    void          *pSpfStub;          /* pointer to the stub associate with this area */
    LIST          listIfAttached;     /* Interface clients attached, unsorted */
    LIST          listAreaAggregate;  /* Area Aggregate instances */

    ulong_t       defaultStubMetric;  /* default stub metric */
    ushort_t      numActiveIf;        /* number of active interface attached */
    ushort_t      numActiveVirtIf;    /* number of active virtual interface attached */
    ushort_t      numActiveHost;      /* number of active host interface attached */
    ushort_t      numIfAttached;      /* number of interface instances attached */
    ushort_t      numVirtIfAttached;  /* number of virtual interface instances attached */
    ushort_t      numHostAttached;    /* number of host interface instances attached */
    ushort_t      numAgAttached;      /* number of area aggregate instances attached */
    rsAction_t    createdAs;          /* the method this row is created */
    mApiOspfv3TrueValue_t  isTransitArea;
                                      /* TRUE if this is a transit area */
    
    /* objects associate with the ospfv3AreaTable */
    ulong_t                 ospfv3AreaId;                   /* index object */
    ulong_t                 ospfv3ImportAsExtern;            /* read-create */
    ulong_t                 ospfv3SpfRuns;                  /* read-only */
    ulong_t                 ospfv3AreaBdrRtrCount;          /* read-only */
    ulong_t                 ospfv3AsBdrRtrCount;            /* read-only */
    ulong_t                 ospfv3AreaScopeLsaCount;        /* read-only */
    ulong_t                 ospfv3AreaScopeLsaCksumSum;     /* read-only */
    mApiOspfv3AreaSummary_t ospfv3AreaSummary;              /* read-create */
    mApiOspfv3RowStatus_t   ospfv3AreaStatus;               /* read-create */
    ulong_t                 ospfv3StubMetric;               /* read-create */
    ushort_t                ospfv3AreaNssaTranslatorRole;   /* read-create */ 
    ushort_t                ospfv3AreaNssaTranslatorState;  /* read-only   */
    ulong_t                 ospfv3AreaNssaTranslatorStabilityInterval; 
                                                            /* read-create */
    ulong_t                 ospfv3AreaNssaTranslatorEvents; /* read-only   */
} mApiOspfv3Area_t;


/* objects associate with the ospfv3AsLsdbTable */
typedef struct mApiOspfv3AsLsdb 
{
   AVL_NODE             node;                      /* AVL Tree node */
   mApiOspfv3LsdbType_t ospfv3AsLsdbType;          /* index object */ 
   ulong_t              ospfv3AsLsdbRouterId;      /* index object */   
   ulong_t              ospfv3AsLsdbLsid;          /* index object */    
   long                 ospfv3AsLsdbSequence;      /* read-only */ 
   ulong_t              ospfv3AsLsdbAge;           /* read-only */ 
   ulong_t              ospfv3AsLsdbChecksum;      /* read-only */            
   char*                ospfv3AsLsdbAdvertisement; /* read-only */        
   ushort_t             asLsdbLen;                 /* advertisement length, not MIB object */
} mApiOspfv3AsLsdb_t;

/* objects associate with the ospfv3AreaLsdbTable */
typedef struct mApiOspfv3AreaLsdb
{
   AVL_NODE             node;                        /* AVL Tree node */
   ulong_t              ospfv3AreaLsdbAreaId;        /* index object */ 
   mApiOspfv3LsdbType_t ospfv3AreaLsdbType;          /* index object */   
   ulong_t              ospfv3AreaLsdbRouterId;      /* index object */    
   ulong_t              ospfv3AreaLsdbLsid;          /* index object */    
   long                 ospfv3AreaLsdbSequence;      /* read-only */   
   ulong_t              ospfv3AreaLsdbAge;           /* read-only */   
   ulong_t              ospfv3AreaLsdbChecksum;      /* read-only */    
   char*                ospfv3AreaLsdbAdvertisement; /* read-only */
   ushort_t             ospfv3AreaLsdbLen;           /* advertisement length, not MIB object */
} mApiOspfv3AreaLsdb_t;

/* objects associate with the ospfv3LinkLsdbTable */
typedef struct mApiOspfv3LinkLsdb
{
   AVL_NODE             node;                   /* AVL Tree node */
   ushort_t             ospfv3LinkLsdbIfIndex;  /* index object */              
   mApiOspfv3LsdbType_t ospfv3LinkLsdbType;     /* index object */ 
   ulong_t              ospfv3LinkLsdbRouterId; /* index object */ 
   ulong_t              ospfv3LinkLsdbLsid;     /* index object */ 
   long                 ospfv3LinkLsdbSequence; /* read-only */ 
   ulong_t              ospfv3LinkLsdbAge;      /* read-only */ 
   ulong_t              ospfv3LinkLsdbChecksum; /* read-only */ 
   char*                ospfv3LinkLsdbAdvertisement;
                                                /* read-only */ 
   ushort_t             ospfv3LinkLsdbLen;      /* advertisement length, not MIB object */
} mApiOspfv3LinkLsdb_t;

/* objects associate with the ospfv3HostTable */
typedef struct mApiOspfv3Host
{
    AVL_NODE             node;           /* AVL Tree node */
    ulong_t              defaultMtu;     /* default mtu from in_ifaddr structure */
    ulong_t              defaultNetmask; /* default netmask from in_ifaddr structure */
    ulong_t              defaultMetric;  /* default metric from in_ifaddr structure */
    rsAction_t           createdAs;      /* the method this row is created */
    void                 *pArea;         /* area associated with this host */
    inetAddressType_t     ospfv3HostAddressType; /* index object */
    struct in6_addr       ospfv3HostAddress;     /* index object */
    ulong_t               ospfv3HostMetric;      /* read-create */
    mApiOspfv3RowStatus_t ospfv3HostStatus;      /* read-create */
    ulong_t               ospfv3HostAreaId;      /* read-create */
} mApiOspfv3Host_t;

/* objects associate with the ospfv3IfTable */
typedef struct mApiOspfv3If
{
    AVL_NODE      node;                 /* AVL Tree node */
    LIST          listNbrAttached;      /* neighbors attached to this interface, unsorted */
    ushort_t      numNbrAttached;       /* number of neighbors attached to this interface */
    LIST          listNbmaNbrAttached;  /* NBMA neighbors attached to this interface, unsorted */
    ushort_t      numNbmaNbrAttached;   /* number of NBMA neighbors attached to this interface */
    ulong_t       defaultMtu;           /* default mtu from in_ifaddr structure */
    ulong_t       defaultNetmask;       /* default netmask from in_ifaddr structure */
    ulong_t       defaultMetric;        /* default metric from in_ifaddr structure */
    rsAction_t    createdAs;            /* the method this row is created */

    void          *pIfMetric;        /* metric associate with this interface */
    void          *pArea;            /* pointer to area this interface connects */
    char          sysIfName[IFNAMSIZ];  /* interface name, e.g. `en'' or ``lo'' */
    ushort_t      sysIfIndex;        /* numeric abbreviation for this interface */
    int           sysIfFlags;        /* interface flags */

    ulong_t               ospfv3IfIndex;                  /* index object */ 
    ulong_t               ospfv3IfAreaId;                 /* read-create */
    mApiOspfv3IfType_t    ospfv3IfType;                   /* read-create */
    mApiOspfv3Status_t    ospfv3IfAdminStat;              /* read-create */ 
    ulong_t               ospfv3IfRtrPriority;            /* read-create */
    ulong_t               ospfv3IfTransitDelay;           /* read-create */
    ulong_t               ospfv3IfRetransInterval;        /* read-create */
    ulong_t               ospfv3IfHelloInterval;          /* read-create */
    ulong_t               ospfv3IfRtrDeadInterval;        /* read-create */
    ulong_t               ospfv3IfPollInterval;           /* read-create */
    mApiOspfv3IfState_t   ospfv3IfState;                  /* read-only */
    ulong_t               ospfv3IfDesignatedRouter;       /* read-only */
    ulong_t               ospfv3IfBackupDesignatedRouter; /* read-only */
    ulong_t               ospfv3IfEvents;                 /* read-only */
    mApiOspfv3RowStatus_t ospfv3IfStatus;                 /* read-create */
    ulong_t               ospfv3IfMetricValue;            /* read-create */
    ulong_t               ospfv3IfLinkScopeLsaCount;      /* read-only */
    ulong_t               ospfv3IfLinkLsaCksumSum;         /* read-only */
    ulong_t               ospfv3IfInstId;                 /* read-create */

    /* the following read-create objects are implemented as read-only */
    mApiOspfv3IfMcastType_t  ospfv3IfMulticastForwarding;
    mApiOspfv3TrueValue_t    ospfv3IfDemand;      
} mApiOspfv3If_t;

/* objects associate with the ospfv3VirtIfTable */
typedef struct mApiOspfv3VirtIf
{
    AVL_NODE                 node;                /* AVL Tree node */
    void                    *pArea;               /* pointer to transit area */
    rsAction_t               createdAs;           /* the method this row is created */

    ulong_t                  ospfv3VirtIfAreaId;            /* index object */
    ulong_t                  ospfv3VirtIfNeighbor;          /* index object */
    ushort_t                 ospfv3VirtIfIndex;             /* read-create */
    ulong_t                  ospfv3VirtIfTransitDelay;      /* read-create */
    ulong_t                  ospfv3VirtIfRetransInterval;   /* read-create */
    ulong_t                  ospfv3VirtIfHelloInterval;     /* read-create */
    ulong_t                  ospfv3VirtIfRtrDeadInterval;   /* read-create */
    mApiOspfv3VirtIfState_t  ospfv3VirtIfState;             /* read-only */
    ulong_t                  ospfv3VirtIfEvents;            /* read-only */
    mApiOspfv3RowStatus_t    ospfv3VirtIfStatus;            /* read-create */
    ulong_t                  ospfv3VirtIfLinkScopeLsaCount; /* read-create */
    ulong_t                  ospfv3VirtIfLinkLsaksumSum;    /* read-only */

} mApiOspfv3VirtIf_t;


/* objects associate with the ospfv3NbrTable */
typedef struct mApiOspfv3Nbr
{
    AVL_NODE               node;                       /* AVL Tree node */
    void                   *pIf;                       /* interface associated with */
    ushort_t               ospfv3NbrIfIndex;           /* index object */
    ulong_t                ospfv3NbrRtrId;             /* index object */
    inetAddressType_t      ospfv3NbrAddressType;       /* read-only */
    struct in6_addr        ospfv3NbrAddress;            /* read-only */
    ulong_t                ospfv3NbrOptions;           /* read-only */
    ulong_t                ospfv3NbrPriority;          /* read-create */
    mApiOspfv3NbrState_t   ospfv3NbrState;             /* read-only */
    ulong_t                ospfv3NbrEvents;            /* read-only */
    ulong_t                ospfv3NbrLsRetransQLen;     /* read-only */
    mApiOspfv3TrueValue_t  ospfv3NbrHelloSuppressed;   /* read-only */
    ulong_t                ospfv3NbrIfId;              /* read-only */
} mApiOspfv3Nbr_t;

/* objects associate with the ospfv3NbmaNbrTable */
typedef struct mApiOspfv3NbmaNbr
{
    AVL_NODE              node;      /* AVL Tree node */
    void                 *pIf;       /* interface associated with */
    rsAction_t            createdAs; /* the method this row is created */
    
    ushort_t                   ospfv3NbmaNbrIfIndex;     /* index object  */
    inetAddressType_t          ospfv3NbmaNbrAddressType; /* index object  */
    struct in6_addr            ospfv3NbmaNbrAddress;     /* index object  */
    ulong_t                    ospfv3NbmaNbrPriority;    /* read-create   */
    ulong_t                    ospfv3NbmaNbrRtrId;       /* read-only     */
    mApiOspfv3NbrState_t       ospfv3NbmaNbrState;       /* read-only     */
    mApiOspfv3NbrStorageType_t ospfv3NbmaNbrStorageType; /* read-create   */
    mApiOspfv3RowStatus_t      ospfv3NbmaNbrStatus;      /* read-create   */
} mApiOspfv3NbmaNbr_t;


/* objects associate with the ospfv3VirtNbrTable */
typedef struct mApiOspfv3VirtNbr
{
    AVL_NODE              node;                         /* AVL Tree node */
    ulong_t               ospfv3VirtNbrArea;            /* index object  */
    ulong_t               ospfv3VirtNbrRtrId;           /* index object  */
    inetAddressType_t     ospfv3VirtNbrAddressType;     /* read-only */
    struct in6_addr       ospfv3VirtNbrAddress;         /* read-only */
    ulong_t               ospfv3VirtNbrOptions;         /* read-only */
    mApiOspfv3NbrState_t  ospfv3VirtNbrState;           /* read-only */
    ulong_t               ospfv3VirtNbrEvents;          /* read-only */
    ulong_t               ospfv3VirtNbrLsRetransQLen;   /* read-only */
    mApiOspfv3TrueValue_t ospfv3VirtNbrHelloSuppressed; /* read-only */
   ushort_t               ospfv3VirtNbrIfId;            /* read-only */
} mApiOspfv3VirtNbr_t;

/* objects associate with the ospfv3AreaAggregateTable */
typedef struct mApiOspfv3AreaAggregate
{
    AVL_NODE                       node;        /* AVL Tree node */
    void                          *pArea;       /* area associated with */
    rsAction_t                     createdAs;   /* the method this row is created */
    ulong_t                        ospfv3AreaAggregateAreaID;       /* index object */
    mApiOspfv3LsdbType_t           ospfv3AreaAggregateAreaLsdbType; /* index object */
    inetAddressType_t              ospfv3AreaAggregatePrefixType;   /* index object */
    struct in6_addr                ospfv3AreaAggregatePrefix;       /* index object */
    ulong_t                        ospfv3AreaAggregatePrefixLength; /* index object */
    mApiOspfv3RowStatus_t          ospfv3AreaAggregateStatus;       /* read-create  */
    mApiOspfv3AreaAggregateEffect_t    ospfv3AreaAggregateEffect;       /* read-create  */
} mApiOspfv3AreaAggregate_t;


/* function pointer requires by ospfv3_mApi_avlTreeWalk() routine */
typedef STATUS (* AVL_WALK_FUNC)(void *pNode, void *arg);

/***************************************************************************************
 * Public Function Prototypes for Management Interface 
 */

/* ospfv3GeneralGroup helper routines */

IMPORT STATUS ospfv3_mApi_globalParmGet(mApiOspfv3GenGroup_t *thisGenGroup, 
                                        mApiRequest_t        *pRequest, 
                                        mApiObject_t         *pObject);

IMPORT STATUS ospfv3_mApi_globalParmSet(mApiRequest_t *pRequest, 
                                        mApiObject_t  *pObject,
                                        mApiReqType_t  reqType);

/* ospfv3AreaTable helper routines */

IMPORT STATUS ospfv3_mApi_areaHelperGet( void          *pRow, 
                                         mApiRequest_t *pRequest,
                                         mApiObject_t  *pObject );

IMPORT STATUS ospfv3_mApi_areaHelperSet(mApiRequest_t *pRequest, 
                                        rsRequest_t    rsReqType );

IMPORT void ospfv3_mApi_areaHelperCreate( mApiOspfv3Area_t *pStaticArea );

IMPORT STATUS ospfv3_mApi_areaHelperRegisterIf( mApiOspfv3If_t *pIf, 
                                                int             oldStatus );
/* ospfv3AsLasbTable helper routine */

IMPORT STATUS ospfv3_mApi_as_lsdbHelperGet( void          *pRow, 
                                            mApiRequest_t *pRequest,
                                            mApiObject_t  *pObject );

IMPORT STATUS ospfv3_mApi_area_lsdbHelperGet( void          *pRow, 
                                              mApiRequest_t *pRequest,
                                              mApiObject_t  *pObject );

IMPORT STATUS ospfv3_mApi_link_lsdbHelperGet( void          *pRow, 
                                              mApiRequest_t *pRequest,
                                              mApiObject_t  *pObject );

/* ospfv3IfTable helper routines */

IMPORT STATUS ospfv3_mApi_ifHelperGet( void *pRow, 
                                       mApiRequest_t *pRequest,
                                       mApiObject_t  *pObject );

IMPORT STATUS ospfv3_mApi_ifHelperSet(mApiRequest_t *pRequest, 
                                      rsRequest_t    rsReqType );

IMPORT void ospfv3_mApi_ifHelperCreate( mApiOspfv3If_t *pStaticIf);
 
IMPORT void ospfv3_mApi_ifHelperDelete( ulong_t ospfv3IfIpAddr );

/* ospfv3NbrTable helper routines */

IMPORT STATUS ospfv3_mApi_nbrHelperGet( void          *pRow, 
                                        mApiRequest_t *pRequest,
                                        mApiObject_t  *pObject );

IMPORT STATUS ospfv3_mApi_nbrHelperRegister( mApiOspfv3Nbr_t *pNbr,  ulong_t ospfv3IfIndex);
IMPORT void ospfv3_mApi_nbrHelperDeregister( mApiOspfv3Nbr_t *pNbr, ulong_t ospfv3IfIndex );
                                           

/* common AVL compare routine */

IMPORT int ospfv3MapiOidCompare( int oidLength, 
                                 ulong_t *pOid1, 
                                 ulong_t *pOid2 );

int ospfv3Ipv6AddressCompare(struct in6_addr address1, struct in6_addr address2);

/* misc AVL routines */

IMPORT STATUS ospfv3_mApi_avlTreeWalk( AVL_TREE * pRoot, 
                                       AVL_WALK_FUNC ospfv3MapiTreeWalk,
                                       void *arg );

/* draft-ietf-ospf-ospfv3-mib-05 specific helper initialization routine */

IMPORT STATUS ospfv3_mApi_configurationGet( void );
IMPORT void ospfv3_mApi_reset( void );
IMPORT void ospfv3_mApi_initGeneralGroup( BOOL resetAllToDefault );
IMPORT STATUS ospfv3_mApi_initRsLib(void);
IMPORT STATUS ospfv3_mApi_initAvlTree(void);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __INCospfv3MapiHelper_h */
