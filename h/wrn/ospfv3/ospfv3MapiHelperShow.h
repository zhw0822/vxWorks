/* ospfv3MapiHelperShow.h */

/* Copyright 2004 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*
modification history
--------------------
01b,21aug03,agi  ported to Accordion stack
01a,01nov02,xli  Initial file created
*/
 
/* 
DESCRIPTION

This file contains the dispaly string used by ospfv3MapiHelperShow.c when
formatting the output.
*/

#ifndef _INCospfv3MapiHelperShow_h
#define _INCospfv3MapiHelperShow_h
                                   
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


/* row status display strings */
char *ospfv3_rowStatus[] = {
      "unknown", "active", "notInService", "notReady", "createAndGo", "createAndWait",
      "destroy"
};

/* true value display strings */
char *ospfv3_trueValue[] = { "unknown", "true", "false" };

/* status display strings */
char *ospfv3_status[] = { "unknown", "enabled", "disabled" };

char *ospfv3_ifState[] = {
     "unknown", "down", "loopback", "waiting", "pointToPoint", "DR", "backupDR",
     "otherDR"
};

/* display strings for OSPFv3 interface type */
char *ospfv3_ifType[] = {
     "unknown", "broadcast", "nbma", "pointToPoint", "unknown", "pointToMultipoint"
};

/* display strings for OSPFv3 LSDB type */
char *ospfv3_lsdbType[] = {
     "unknown", "routerLink", "networkLink", "summaryLink", "asSummaryLink",
     "asSummaryLink", "asExternalLink", "multicastLink", "nssaExternalLink"
};

/* display strings for OSPFv3 Neighbor state */
char *ospfv3_nbrState[] = {
     "unknown", "down", "attempt", "init", "twoWay", "exchangeStart", "exchange",
     "loading", "full"
};


char *ospfv3_asType[] = {
     "unknown", "importExternal", "importNoExternal", "importNssa"
};
    
char *ospfv3_summaryType[] = { "unknown", "noAreaSummary", "sendAreaSummary" };

char *ospfv3_stubMetricType[] = { "unknown", "ospfv3Metric", "comparableCost", "nonComparable"};

char *ospfv3_mcastType[] = { "unknown", "blocked", "multicast", "unicast" };

char *ospfv3_permanence[] = { "unknown", "dynamic", "permanent" };

char *ospfv3_extType = "asExternalLink";

char *ospfv3_agType[] = { "unknown", "summaryLink", "nssaExternalLink" };
    
char *ospfv3_agEffect[] = { "unknown", "advertiseMatching", "doNotAdvertiseMatching"};

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* _INCospfv3MapiHelperShow_h */
