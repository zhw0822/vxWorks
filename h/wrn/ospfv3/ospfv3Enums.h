/* ospfv3Enums.h - OSPFv3 enums header file */

/* Copyright 2004 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*
modification history
--------------------
01j,21oct03,ram Modifications for adjacency establishment
01i,10oct03,ram Modifications for LSDB, dynamic config, & NSSA cleanup
01h,25sep03,ram Second general modifications submission
01g,29may03,dsk add enums for ospf task names (used as index in ospf tasks array) and 
                enum of task states for task synchronization at initialization
01f,07mar03,dsk NSSA hash table moved to area structure from main ospf structure
01e,18feb03,dsk ported from OSPFv2 to OSPFv3 structures
01d,06Jan03,hme deleted some OSPFv2 specific data structures and added new data structures for OSPFv3
01c,23dec02,agi propogated TMS code merge for SPR#84284
01b,14nov02,agi first pass at clean-up
01a,23oct02,agi written
*/

#ifndef __INCospfv3Enumsh
#define __INCospfv3Enumsh

#ifdef __cplusplus
extern "C" {
#endif

enum OSPFV3_RETURN_FLAGS
{
	OSPFV3_NO_PROBLEM = 0x0,
	OSPFV3_NO_BUFFERS = 0x1,
	OSPFV3_BAD_REQUEST = 0x2
};

enum	OSPFV3_UNION_PACKET_TYPES
{
	OSPFV3_HELLO_PACKET	= 0x01,
	OSPFV3_DATABASE_DESCRIPTION_PACKET	= 0x02,
	OSPFV3_LINK_STATE_REQUEST_PACKET = 0x03,
	OSPFV3_LINK_STATE_UPDATE_PACKET = 0x04,
	OSPFV3_LINK_STATE_ACKNOWLEDGEMENT_PACKET = 0x05
};

enum	OSPFV3_PACKET_STATE
{
	GOOD_RECEIVE = 0x00,
	OSPFV3_ERROR_ACKNOWLEDGEMENT_BAD = 0x01,
	OSPFV3_ERROR_ACKNOWLEDGEMENT_DUPLICATE = 0x02,
	OSPFV3_ERROR_ACKNOWLEDGEMENT_STATE = 0x03,
	OSPFV3_ERROR_ACKNOWLEDGEMENT_TYPE = 0x04,
	OSPFV3_ERROR_DATABASE_DESCRIPTION_E_BIT = 0x05,
	OSPFV3_ERROR_DATABASE_DESCRIPTION_ROUTER_ID = 0x06,
	OSPFV3_ERROR_DATABASE_DESCRIPTION_STATE = 0x07,
	OSPFV3_ERROR_DATABASE_DESCRIPTION_TYPE = 0x08,
	OSPFV3_ERROR_HELLO_E_BIT = 0x09,
	OSPFV3_ERROR_HELLO_ID = 0x0a,
	OSPFV3_ERROR_HELLO_MASK = 0x0b,
	OSPFV3_ERROR_HELLO_ROUTER_DEAD_INTERVAL = 0x0c,
	OSPFV3_ERROR_HELLO_TIMER = 0x0d,
	OSPFV3_ERROR_HELLO_VIRTUAL = 0x0e,
	OSPFV3_ERROR_OSPFV3_AUTHENTICATION_KEY = 0x0f,
	OSPFV3_ERROR_OSPFV3_AUTHENTICATION_TYPE = 0x10,
	OSPFV3_ERROR_OSPFV3_INTERFACE_DOWN = 0x11,
	OSPFV3_ERROR_OSPFV3_PACKET_TYPE = 0x12,
	OSPFV3_ERROR_REQUEST_BOGUS = 0x13,
	OSPFV3_ERROR_REQUEST_EMPTY = 0x14,
	OSPFV3_ERROR_UPDATE_STATE = 0x15,
	STOP_PROCESSING_PACKET = 0x16,
	/* nssa bit */
	OSPFV3_ERROR_HELLO_N_BIT = 0x17,
	OSPFV3_ERROR_DATABASE_DESCRIPTION_N_BIT = 0x18,
    /*opaque lsa error option jkw*/
	OSPFV3_ERROR_OPAQUE_OPTION = 0x19,
	OSPFV3_ERROR_DATABASE_DESCRIPTION_MTU_MISMATCH = 0x1a

};

enum	OSPFV3_NEIGHBOR_EVENT
{
	OSPFV3_HELLO_RECEIVED = 0x0000,
	OSPFV3_START = 0x01,
	OSPFV3_TWO_WAY_RECEIVED = 0x02,
	OSPFV3_NEGOTIATION_DONE = 0x03,
	OSPFV3_EXCHANGE_DONE = 0x04,
	OSPFV3_BAD_LINK_STATE_REQUEST = 0x05,
	OSPFV3_LOADING_DONE = 0x06,
	OSPFV3_ADJACENCY_OK = 0x07,
	OSPFV3_SEQUENCE_NUMBER_MISMATCH = 0x08,
	OSPFV3_ONE_WAY = 0x09,
	OSPFV3_KILL_NEIGHBOR = 0x0a,
	OSPFV3_INACTIVITY_TIMER = 0x0b,
	OSPFV3_DOWN_INDICATION_FROM_LOWER_LEVEL_PROTOCOLS_RECEIVED = 0x0c
};

enum	OSPFV3_NEIGHBOR_MODE
{
	OSPFV3_CLEAR_MODE = 0x00,
	OSPFV3_SLAVE = 0x01,
	OSPFV3_MASTER = 0x02,
	OSPFV3_SLAVE_HOLD = 0x04		/* holding the last database summary delay */
};

enum	OSPFV3_NEIGHBOR_STATE
{
	OSPFV3_NEIGHBOR_DOWN = 0x00,
	OSPFV3_NEIGHBOR_ATTEMPT = 0x01,
	OSPFV3_NEIGHBOR_INITIALIZING = 0x02,
	OSPFV3_NEIGHBOR_2_WAY = 0x03,
	OSPFV3_NEIGHBOR_EXCHANGE_START = 0x04,
	OSPFV3_NEIGHBOR_EXCHANGE = 0x05,
	OSPFV3_NEIGHBOR_LOADING = 0x06,
	OSPFV3_NEIGHBOR_FULL = 0x07
};

enum	OSPFV3_INTERFACE_EVENT
{
	OSPFV3_INTERFACE_UP = 0x00,
	OSPFV3_WAIT_TIMER = 0x01,
	OSPFV3_BACKUP_SEEN = 0x02,
	OSPFV3_NEIGHBOR_CHANGE = 0x03,
	OSPFV3_LOOP_INDICATION = 0x04,
	OSPFV3_UNLOOP_INDICATION = 0x05,
	OSPFV3_INTERFACE_DOWN = 0x06
};

enum	OSPFV3_INTERFACE_STATE
{
	OSPFV3_INTERFACE_IS_DOWN	= 0x00,
	OSPFV3_INTERFACE_LOOPBACK = 0x01,
	OSPFV3_INTERFACE_WAITING = 0x02,
	OSPFV3_INTERFACE_POINT_TO_POINT = 0x03,
	OSPFV3_INTERFACE_DESIGNATED_ROUTER_OTHER = 0x04,
	OSPFV3_INTERFACE_BACKUP_DESIGNATED_ROUTER = 0x05,
	OSPFV3_INTERFACE_DESIGNATED_ROUTER = 0x06
};

enum	OSPFV3_INTERFACE_TYPE
{
	OSPFV3_POINT_TO_POINT = 0x01,
	OSPFV3_BROADCAST = 0x02,
	OSPFV3_NBMA = 0x03,
	OSPFV3_POINT_TO_MULTIPOINT = 0x04,
	OSPFV3_VIRTUAL_LINK = 0x05
};

enum	OSPFV3_SEQUENCE_STATE
{
	OSPFV3_NORMAL = 0x00,
	OSPFV3_WAITING_FOR_MAXIMUM_SEQUENCE_INSTANCE_TO_BE_FLUSHED = 0x01,
	OSPFV3_DONE_FLUSHING_MAXIMUM_SEQUENCE_INSTANCE = 0x02
};


/* Link State Types */

enum OSPFV3_LS_FLOODING_SCOPE
{
	OSPFV3_LINK_LOCAL_FLOOD = 0x00,
	OSPFV3_AREA_FLOOD       = 0x20,
    OSPFV3_AS_FLOOD         = 0x40,
    OSPFV3_RESERVERD_FLOOD          = 0x60,
    OSPFV3_UBIT_ON_LINK_LOCAL_FLOOD = 0x80,
    OSPFV3_UBIT_ON_AREA_FLOOD       = 0xA0,
    OSPFV3_UBIT_ON_AS_FLOOD         = 0xC0,
    OSPFV3_UBIT_ON_RESERVERD_FLOOD  = 0xE0
};

enum OSPFV3_LS_TYPE
    {
    OSPFV3_LS_UNKNOWN                       = 0x00,
    OSPFV3_LS_ROUTER                        = 0x01,
    OSPFV3_LS_NETWORK                       = 0x02,
    OSPFV3_LS_INTER_AREA_PREFIX             = 0x03,
    OSPFV3_LS_INTER_AREA_ROUTER             = 0x04,
    OSPFV3_LS_AS_EXTERNAL                   = 0x05,
    OSPFV3_LS_GROUP_MEMBERSHIP              = 0x06,
    OSPFV3_LS_UNUSED                        = 0x07,
    OSPFV3_LS_LINK                          = 0x08,
    OSPFV3_LS_INTRA_AREA_PREFIX             = 0x09,
    OSPFV3_LS_MAX                           = 0x0a
};

/* Use these indexes for hashes of LSAs instead of LSA type, to
avoid holes in the hash table, as an empty hash with 256 bins occupies 256 * 8 = 2K memory */
typedef enum OSPFV3_INTERFACE_LSA_HASH
{
    OSPFV3_INTERFACE_UNKNOWN_LSA_HASH   = 0x00,
    OSPFV3_LINK_LSA_HASH                = 0x01,
    OSPFV3_NUM_INTERFACE_LSA_HASHES     = 0x02
} OSPFV3_INTERFACE_LSA_HASH;

/* Use these indexes for hashes of LSAs instead of LSA type, to
avoid holes in the hash table, as an empty hash with 256 bins occupies 256 * 8 = 2K memory */
typedef enum OSPFV3_AREA_LSA_HASH
{
    OSPFV3_AREA_UNKNOWN_LSA_HASH        = 0x00,
    OSPFV3_ROUTER_LSA_HASH              = OSPFV3_LS_ROUTER,
    OSPFV3_NETWORK_LSA_HASH             = OSPFV3_LS_NETWORK,
    OSPFV3_INTER_AREA_PREFIX_LSA_HASH   = OSPFV3_LS_INTER_AREA_PREFIX,
    OSPFV3_INTER_AREA_ROUTER_LSA_HASH   = OSPFV3_LS_INTER_AREA_ROUTER,
    OSPFV3_INTRA_AREA_PREFIX_LSA_HASH   = 0x05,
    OSPFV3_NUM_AREA_LSA_HASHES          = 0x06

} OSPFV3_AREA_LSA_HASH;

/* Use these indexes for hashes of LSAs instead of LSA type, to
avoid holes in the hash table, as an empty hash with 256 bins occupies 256 * 8 = 2K memory */
typedef enum OSPFV3_AS_LSA_HASH
{
    OSPFV3_AS_UNKNOWN_LSA_HASH   = 0x00,
    OSPFV3_EXTERNAL_LSA_HASH     = 0x01,
    OSPFV3_NUM_AS_LSA_HASHES     = 0x02
} OSPFV3_AS_LSA_HASH;

typedef enum OSPFV3_LSA_REQUEST_INDEX
{
    OSPFV3_ROUTER_LSA_REQUEST_INDEX              = 0x00,
    OSPFV3_NETWORK_LSA_REQUEST_INDEX             = 0x01,
    OSPFV3_INTER_AREA_PREFIX_LSA_REQUEST_INDEX   = 0x02,
    OSPFV3_INTER_AREA_ROUTER_LSA_REQUEST_INDEX   = 0x03,
    OSPFV3_INTRA_AREA_PREFIX_LSA_REQUEST_INDEX   = 0x04,
    OSPFV3_EXTERNAL_LSA_REQUEST_INDEX            = 0x05,
    OSPFV3_LINK_LSA_REQUEST_INDEX                = 0x06,
    OSPFV3_LINK_SCOPE_UNKNOWN_LSA_INDEX          = 0x07,
    OSPFV3_AREA_SCOPE_UNKNOWN_LSA_INDEX          = 0x08,
    OSPFV3_AS_SCOPE_UNKNOWN_LSA_INDEX            = 0x09,
    OSPFV3_NUM_LSA_REQUEST_INDEX                 = 0x0A

} OSPFV3_LSA_REQUEST_INDEX;


enum OSPFV3_ROUTER_LINK_TYPE
{
	OSPFV3_ROUTER_LINK_TYPE_PPP = 0x1,
	OSPFV3_ROUTER_LINK_TYPE_TRANSIT_NETWORK = 0x2,
	OSPFV3_ROUTER_LINK_TYPE_RESERVED = 0x3, 
	OSPFV3_ROUTER_LINK_TYPE_VIRTUAL = 0x4
};


enum OSPFV3_ACKNOWLEDGEMENT_RETURN_TYPE
{
	OSPFV3_NO_ACKNOWLEDGEMENTS_LEFT = 0x00,
	OSPFV3_ACKNOWLEDGEMENTS_LEFT = 0x01
};

enum OSPFV3_ROUTING_TABLE_UPDATE_ACTION
{
	OSPFV3_NO_ACTION = 0x00,
	OSPFV3_REPLACE_PATH = 0x01,
	OSPFV3_ADD_PATH = 0x02,
	OSPFV3_DELETE_PATH = 0x03 /* RTM ### changes */
};

enum OSPFV3_ROUTE_STATUS
{
	/* No new data under a routing table node or next_hop_block */
	OSPFV3_ROUTE_IS_NOT_NEW = 0x00,
	/*
	 * New data under a routing table node or next_hop_block.
	 * can be a new next hop block under a routing table node or a new routing table node.
	 * or an old next hop block is not available any more.
	 */
	OSPFV3_ROUTE_IS_NEW = 0x01,
	/*
	 * This routing table node or next block is no longer available
	 */
	OSPFV3_ROUTE_IS_NOT_AVAILABLE_NOW = 0x02,
	/*
	 * doesn't matter
	 */
	OSPFV3_ROUTE_STATUS_DONT_CARE = 0x03
};

typedef enum OSPFV3_ROUTE_DESTINATION_TYPE
{
	OSPFV3_DESTINATION_TYPE_NETWORK = 0x00,
	OSPFV3_DESTINATION_TYPE_ASBR = 0x01,           /* autonomous system border router */
	OSPFV3_DESTINATION_TYPE_ABR = 0x02,            /* area border router */
	OSPFV3_DESTINATION_TYPE_VIRTUAL = 0x03,
	OSPFV3_DESTINATION_TYPE_ASE = 0x04,            /* external to the autonomous system */
    OSPFV3_DESTINATION_TYPE_ROUTER = 0x05,         /* intra-area router */ 
    /* generic, any type of routing table entry */
	OSPFV3_DESTINATION_TYPE_WILDCARD = 0xFF
} OSPFV3_ROUTE_DESTINATION_TYPE;

enum OSPFV3_ROUTE_PATH_TYPE
{
	OSPFV3_ROUTE_PATH_TYPE_INTRA = 0x00,
	OSPFV3_ROUTE_PATH_TYPE_INTER = 0x01,
	OSPFV3_ROUTE_PATH_TYPE_1_EXTERNAL = 0x02,
	OSPFV3_ROUTE_PATH_TYPE_2_EXTERNAL = 0x03,
	OSPFV3_ROUTE_PATH_TYPE_WILDCARD = 0xFF
};

enum OSPFV3_COUNTER_ACTION_TYPE
{
	OSPFV3_INCREMENT_COUNTERS = 0x0,
	OSPFV3_DECREMENT_COUNTERS = 0x1
};

enum OSPFV3_PRINTF_GROUPS
{
	OSPFV3_INTERFACE_PRINTF = 0x0,
	OSPFV3_NEIGHBOR_PRINTF = 0x1,
	OSPFV3_MEMORY_PRINTF = 0x2,
	OSPFV3_ALARM_PRINTF = 0x3,
	OSPFV3_SNMP_PRINTF = 0x4,
	OSPFV3_PACKET_PRINTF = 0x5,
	OSPFV3_ROUTING_TABLE_PRINTF = 0x6,
	OSPFV3_DEBUG_PRINTF = 0x7,
	OSPFV3_RTM_PRINTF = 0x08,
	OSPFV3_PATRICIA_DEBUG_PRINTF = 0x09,
	OSPFV3_DB_OVERFLOW_PRINTF = 0x0A,
	OSPFV3_SEARCH_PRINTF = 0x0B,
	OSPFV3_PROLOGUE_PRINTF = 0x0C
};

enum OSPFV3_SEARCH_TYPE
{
	OSPFV3_SEARCH_LSA_TYPE = 0x01,
	OSPFV3_SEARCH_LSA_ID = 0x02,
	OSPFV3_SEARCH_ADVERTISING_ROUTER = 0x03
};


enum OSPFV3_MODE
{
	PASSIVE = 0x00,
	ACTIVE = 0x01
};

typedef signed long seq_t;	/* signed 32 bit number */

typedef enum 
{
	OSPFINVALDLSSEQ	= 0x80000000L,	/* Invalid LS sequence number (reserved or unused) */
	OSPFINITLSSEQ = 0x80000001L,	/* Initial LS Sequence value */
	OSPFMAXLSSEQ = 0x7fffffffL,		/* Maximum LS Sequence value */
	OSPFLSINVALID = 0x80000000L	/* Invalid LS Sequence value */
} SEQNO_T;

typedef enum 
{
    OSPFV3_TASK_STATE_NOT_CREATED = 0x00,
    OSPFV3_TASK_STATE_CREATED,
    OSPFV3_TASK_STATE_INIT_COMPLETE,
    OSPFV3_TASK_STATE_RUNNING,
    OSPFV3_TASK_STATE_ERROR
} OSPFV3_TASK_STATES;

typedef enum
{
    OSPFV3_TASK_TIMER,
    OSPFV3_TASK_SPF,
    OSPFV3_TASK_INPUT,
    OSPFV3_TASK_RTM,
    OSPFV3_TASK_MAPI,
    OSPFV3_NUM_TASKS
} OSPFV3_TASKS;

#ifdef __cplusplus
}
#endif

#endif /* __INCospfv3Enumsh */
