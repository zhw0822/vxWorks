/* ospfv3MapiWrnHelper.h - header file for WindNet OSPFv3 Enterprise MIB  */

/* Copyright 2004 Wind River, Inc. */
#include "copyright_wrs.h"

/*
 * modification history
 * --------------------
 * 01b,25jan03,xli Add WRN OSPFv3 interprise MIB specific changes 
 * 01a,15nov02,xli Initial file creation.
 */

/*
DESCRIPTION
This file defines the WindNet OSPFv3 Enterprise MIB specific 
Management Interface local ID enumerations, the MIB object 
enumerations, the MIB object sizes and some function prototypes.

INCLUDE FILES: N/A
*/

#ifndef __INCospfv3MapiWrnHelper_h
#define __INCospfv3MapiWrnHelper_h

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/* Defines */

/* the following defines provides the number of sub-identifier 
 * (the instance length) of each table in the WRN OSPFv3 Enterprise MIB.
 */
#define OSPFV3_LOCAL_LSDB_INSTANCE_LEN     17
#define OSPFV3_WRN_INTF_INSTANCE_LEN       4

/*****************************************************************************
* Management Interface Local ID enumerations for WindNet OSPFv3 Enterprise MIB.
*/
typedef enum {
   mApiWrnOspfv3RedistributeDefaultRoutes = 1,
   mApiWrnOspfv3RedistributeStaticRoutes,
   mApiWrnOspfv3RedistributeRIPRoutes,
   mApiWrnOspfv3RedistributeBGPRoutes

} mApiWrnOspfv3_t;

/****************************************************************************
* Management Interface WindNet OSPFv3 Enterprise MIB Data Structure.
*/
typedef struct mApiWrnOspfv3GenGroup
{
  mApiOspfv3TrueValue_t  wrnOspfv3RedistributeDefaultRoutes; /* read-write */
  mApiOspfv3TrueValue_t  wrnOspfv3RedistributeStaticRoutes;  /* read-write */
  mApiOspfv3TrueValue_t  wrnOspfv3RedistributeRIPRoutes;     /* read-write */
  mApiOspfv3TrueValue_t  wrnOspfv3RedistributeBGPRoutes;     /* read-write */

} mApiWrnOspfv3GenGroup_t;

/****************************************************************************
* Management Interface WindNet OSPFv3 Enterprise MIB Function Prototypes.
*/

/* wrnOspfv3GeneralGroup helper routines */
IMPORT STATUS wrnOspfv3_mApi_globalParmGet(
                            mApiWrnOspfv3GenGroup_t *thisGenGroup, 
                            mApiRequest_t           *pRequest,  
                            mApiObject_t            *pObject );

IMPORT STATUS wrnOspfv3_mApi_globalParmSet( mApiRequest_t *pRequest, 
                                            mApiObject_t  *pObject,
                                            mApiReqType_t  reqType );
IMPORT STATUS mApi2Ospfv3_configWrnGenGroup( void *pGenParams );

IMPORT void mApi2Ospfv3_configRedistributeOpts( void *pGenParams );

/* wrn-ospfv3 mib specific initialization routines */
IMPORT STATUS wrnOspfv3_mApi_initAvlTree( void );

IMPORT void wrnOspfv3_mApi_initGeneralGroup( BOOL resetAllToDefault );

IMPORT STATUS wrnOspfv3_mApi_initRsLib( void );

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __INCospfv3MapiWrnHelper_h */
