/* ospfv3InterfaceStateMachine.h - OSPFv3 interface state machine header file */

/* Copyright 2004 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*
modification history
--------------------
01a,23oct02,agi  written
*/

#ifndef __INCospfv3InterfaceStateMachineh
#define __INCospfv3InterfaceStateMachineh

#ifdef __cplusplus
extern "C" {
#endif

typedef void (*INTERFACE_TRANSITION_FUNCTION)
    (OSPFV3_INTERFACE *sptr_interface);

typedef struct OSPFV3_INTERFACE_STATE_MACHINE
{
	INTERFACE_TRANSITION_FUNCTION fptr_interface_transition_function;
} OSPFV3_INTERFACE_STATE_MACHINE;


OSPFV3_INTERFACE_STATE_MACHINE ospfv3_interface_event_processing_table
    [OSPFV3_NUMBER_OF_INTERFACE_EVENTS][OSPFV3_NUMBER_OF_INTERFACE_STATES] =
{
/* -------------------------------------------------------------------------- */
/* OSPFV3_INTERFACE_UP 0    */
/* -------------------------------------------------------------------------- */
{
/* OSPFV3_INTERFACE_IS_DOWN */		
{(INTERFACE_TRANSITION_FUNCTION) ospfv3_process_interface_up_event},
/* OSPFV3_INTERFACE_LOOPBACK */	
{NULL},
/* OSPFV3_INTERFACE_WAITING */		
{NULL},
/* OSPFV3_INTERFACE_POINT_TO_POINT */		
{NULL},
/* OSPFV3_INTERFACE_DESIGNATED_ROUTER_OTHER */		
{NULL},
/* OSPFV3_INTERFACE_BACKUP_DESIGNATED_ROUTER */		
{NULL},
/* OSPFV3_INTERFACE_DESIGNATED_ROUTER */		
{NULL}
},

/* -------------------------------------------------------------------------- */
/* OSPFV3_WAIT_TIMER 1 */
/* -------------------------------------------------------------------------- */
{
/* OSPFV3_INTERFACE_IS_DOWN */		
{NULL},
/* OSPFV3_INTERFACE_LOOPBACK */		
{NULL},
/* OSPFV3_INTERFACE_WAITING */		
{(INTERFACE_TRANSITION_FUNCTION) ospfv3_process_interface_wait_timer_event},
/* OSPFV3_INTERFACE_POINT_TO_POINT */		
{(INTERFACE_TRANSITION_FUNCTION) ospfv3_process_interface_wait_timer_event},
/* OSPFV3_INTERFACE_DESIGNATED_ROUTER_OTHER */		
{NULL},
/* OSPFV3_INTERFACE_BACKUP_DESIGNATED_ROUTER */		
{NULL},
/* OSPFV3_INTERFACE_DESIGNATED_ROUTER */		
{NULL}
},

/* -------------------------------------------------------------------------- */
/* OSPFV3_BACKUP_SEEN 2 */
/* -------------------------------------------------------------------------- */
{
/* OSPFV3_INTERFACE_IS_DOWN */		
{NULL},
/* OSPFV3_INTERFACE_LOOPBACK */		
{NULL},
/* OSPFV3_INTERFACE_WAITING */		
{(INTERFACE_TRANSITION_FUNCTION) ospfv3_process_interface_backup_seen_event},
/* OSPFV3_INTERFACE_POINT_TO_POINT */		
{NULL},
/* OSPFV3_INTERFACE_DESIGNATED_ROUTER_OTHER */		
{NULL},
/* OSPFV3_INTERFACE_BACKUP_DESIGNATED_ROUTER */		
{NULL},
/* OSPFV3_INTERFACE_DESIGNATED_ROUTER */		
{NULL}
},

/* -------------------------------------------------------------------------- */
/* OSPFV3_NEIGHBOR_CHANGE 3 */
/* -------------------------------------------------------------------------- */
{
/* OSPFV3_INTERFACE_IS_DOWN */		
{NULL},
/* OSPFV3_INTERFACE_LOOPBACK */	
{NULL},
/* OSPFV3_INTERFACE_WAITING */	
{NULL},
/* OSPFV3_INTERFACE_POINT_TO_POINT */		
{NULL},
/* OSPFV3_INTERFACE_DESIGNATED_ROUTER_OTHER */		
{(INTERFACE_TRANSITION_FUNCTION) ospfv3_process_interface_neighbor_change_event},
/* OSPFV3_INTERFACE_BACKUP_DESIGNATED_ROUTER */		
{(INTERFACE_TRANSITION_FUNCTION) ospfv3_process_interface_neighbor_change_event},
/* OSPFV3_INTERFACE_DESIGNATED_ROUTER */
{(INTERFACE_TRANSITION_FUNCTION) ospfv3_process_interface_neighbor_change_event}
},

/* -------------------------------------------------------------------------- */
/* OSPFV3_LOOP_INDICATION 4 */
/* -------------------------------------------------------------------------- */
{
/* OSPFV3_INTERFACE_IS_DOWN */		
{(INTERFACE_TRANSITION_FUNCTION) ospfv3_process_interface_loop_indication_event},
/* OSPFV3_INTERFACE_LOOPBACK */		
{(INTERFACE_TRANSITION_FUNCTION) ospfv3_process_interface_loop_indication_event},
/* OSPFV3_INTERFACE_WAITING */		
{(INTERFACE_TRANSITION_FUNCTION) ospfv3_process_interface_loop_indication_event},
/* OSPFV3_INTERFACE_POINT_TO_POINT */		
{(INTERFACE_TRANSITION_FUNCTION) ospfv3_process_interface_loop_indication_event},
/* OSPFV3_INTERFACE_DESIGNATED_ROUTER_OTHER */		
{(INTERFACE_TRANSITION_FUNCTION) ospfv3_process_interface_loop_indication_event},
/* OSPFV3_INTERFACE_BACKUP_DESIGNATED_ROUTER */		
{(INTERFACE_TRANSITION_FUNCTION) ospfv3_process_interface_loop_indication_event},
/* OSPFV3_INTERFACE_DESIGNATED_ROUTER */		
{(INTERFACE_TRANSITION_FUNCTION) ospfv3_process_interface_loop_indication_event}
},

/* -------------------------------------------------------------------------- */
/* OSPFV3_UNLOOP_INDICATION 5 */
/* -------------------------------------------------------------------------- */
{
/* OSPFV3_INTERFACE_IS_DOWN */		
{NULL},
/* OSPFV3_INTERFACE_LOOPBACK */		
{(INTERFACE_TRANSITION_FUNCTION) ospfv3_process_interface_unloop_indication_event},
/* OSPFV3_INTERFACE_WAITING */		
{NULL},
/* OSPFV3_INTERFACE_POINT_TO_POINT */		
{NULL},
/* OSPFV3_INTERFACE_DESIGNATED_ROUTER_OTHER */		
{NULL},
/* OSPFV3_INTERFACE_BACKUP_DESIGNATED_ROUTER */		
{NULL},
/* OSPFV3_INTERFACE_DESIGNATED_ROUTER */	
{NULL}
},

/* -------------------------------------------------------------------------- */
/* OSPFV3_INTERFACE_DOWN 6 */
/* -------------------------------------------------------------------------- */
{
/* OSPFV3_INTERFACE_IS_DOWN */		
{NULL},
/* OSPFV3_INTERFACE_LOOPBACK */		
{(INTERFACE_TRANSITION_FUNCTION) ospfv3_process_interface_down_event},
/* OSPFV3_INTERFACE_WAITING */		
{(INTERFACE_TRANSITION_FUNCTION) ospfv3_process_interface_down_event},
/* OSPFV3_INTERFACE_POINT_TO_POINT */		
{(INTERFACE_TRANSITION_FUNCTION) ospfv3_process_interface_down_event},
/* OSPFV3_INTERFACE_DESIGNATED_ROUTER_OTHER */		
{(INTERFACE_TRANSITION_FUNCTION) ospfv3_process_interface_down_event},
/* OSPFV3_INTERFACE_BACKUP_DESIGNATED_ROUTER */		
{(INTERFACE_TRANSITION_FUNCTION) ospfv3_process_interface_down_event},
/* OSPFV3_INTERFACE_DESIGNATED_ROUTER */		
{(INTERFACE_TRANSITION_FUNCTION) ospfv3_process_interface_down_event}
}
};

#ifdef __cplusplus
}
#endif

#endif /* __INCospfv3InterfaceStateMachineh */
