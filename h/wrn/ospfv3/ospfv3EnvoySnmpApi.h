/* ospfv3EnvoySnmpApi.h */

/* Copyright 2004 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*
modification history
--------------------
01c,07apr04,ram OSPF v2/v3 coexistance compile
01b,30sep03,ram Fix for linker error
01a,12nov02,xli  Initial file creation.
*/

/*
DESCRIPTION

This file contains the definitions for constants, data structures and function prototypes
that can be used by the application that need to integrate Envoy SNMP to Manamgenet
Interface implemented the Neutral MIB API
*/


#ifndef __INCospfv3EnvoySnmpApi_h
#define __INCospfv3EnvoySnmpApi_h

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


/* OSPFv3-Envoy debug macro. */
#undef ospfv3EnvoyDbgEnabled
#ifdef  ospfv3EnvoyDbgEnabled
#define ospfv3EnvoyPrintf( _x )  { printf _x; }
#else
#define ospfv3EnvoyPrintf( _x )
#endif

/* OSPFv3 specific MIB API application defined error */
#define OSPFV3_MAPI_INVALID_INSTANCE              1000

/* envoyOspfv3Request_t structure is data structure that is necessary to manipulate the
 * envoy snmp varbinds and the mApiRequest_t data structure. The pValueBuf is a
 * pointer to an array of ulong_t. The size of array must be large enough to
 * accomodate the getproc, nextproc, testproc, setproc and undoproc requests for
 * retrieving all objects from a tabular row in multiple varbinds.. Envoy snmp will
 * serialize all the transactions, subsequent getproc, nextproc, testproc, setproc
 * and/or undoproc will not be called until the current one is completed. Therefore,
 * the pValueBuf pointer can be reused each time a new getproc, nextproc, testproc,
 * setproc and/or undoproc is started. The pOctetBuf is a pointer to buffer allocated
 * specifically for octet string. For getproc and nextproc, the memory pointed to by the
 * pOctetBuf must be either statically or dynamically allocated and the <dynamic> boolean
 * flag must be set to TRUE if memory is dynamically allocated. If dynamic memory
 * allocation is used, Envoy snmp requires that the memory is allocated using
 * SNMP_memory_alloc() and must be marked as dynamic when the getproc_got_string() is
 * called so that Envoy SNMP will free the memory using SNMP_memory_free() when the
 * packet gets encoded. For that reason, the pOctetBuf pointer can be reused for the next
 * transaction after the getproc_got_string() is called. For testproc, setproc and/or
 * undoproc, there is no restriction to how the memory must be allocated. However, the
 * <dynamic> boolean flag must still be set accordingly. If SNMP_memory_alloc() is used
 * to allocated the memory, application must free the memory using SNMP_memory_free()
 * when the testproc, setproc and/or undoproc is completed (Envoy snmp will not free the
 * memory in this case).
 */
typedef struct envoyOspfv3Request {
    SEM_ID        envoyBSem;       /* binary semaphore for synchronization into Envoy */
    ulong_t       *pValueBuf;      /* pointer to array of ulong_t */
    ushort_t      numValueBuf;     /* number of valueBuf allocated */
    ushort_t      bufUsedCnt;      /* number of valueBuf used */
    char          *pOctetBuf;      /* pointer to buffer for octet string */
    ulong_t       octetBufLen;     /* sizeof data pointed to by pOctetBuf */
    BOOL          dynamic;         /* is pOctetBuf dynamically allocated? */
    mApiObject_t  *pObjectList;    /* pointer to array of MIB API objec info */
    ulong_t       *pObjCookie;     /* pointer to array of ulong_t for object cookie */
} envoyOspfv3Request_t;


/* misc covertion routines */

void ospfv3_copy_oids( OIDC_T *best_inst, OIDC_T *tlist, int tcount );

IMPORT ushort_t ospfv3_mApi2EnvoyErrorGet( ushort_t mApiError );

/* prototype for adding/removing subtree to SNMP agent MIB tree */

STATUS  ospfv3_envoy_snmpdTreeAdd( char      *pTreeOidStr,
                                   MIBNODE_T *pTreeAddr,
                                   SEM_ID     ospfv3_envoyBSem );

void ospfv3_envoy_snmpdTreeRemove( char   *pTreeOidStr,
                                   SEM_ID  ospfv3_envoyBSem );

/* routine to build MAPI TEST/SET/UNDO request message */

IMPORT int ospfv3_envoy_buildSetReq( int              tcount,
                                     OIDC_T          *tlist,
                                     SNMP_PKT_T      *pktp,
                                     VB_T            *vbp,
                                     envoyOspfv3Request_t  *pEnvoyOspfv3Req,
                                     mApiRequest_t   *pRequest,
                                     mApiReqType_t    reqType,
                                     int              mApiOidOffset );

/* routine to mark a varbind (used by testproc, setproc and undoproc) */
IMPORT void ospfv3_envoy_markVarbind( VB_T          *vbp,
                                      SNMP_PKT_T    *pktp,
                                      mApiRequest_t *pRequest,
                                      mApiReqType_t  reqType );

/* routine to build MAPI GET/GET NEXT request message */

IMPORT STATUS ospfv3_envoy_buildGetReq( int             tcount,
                                        OIDC_T         *tlist,
                                        VB_T           *vbp,
                                        envoyOspfv3Request_t *pEnvoyOspfv3Req,
                                        mApiRequest_t  *pRequest,
                                        int             mApiOidOffset );

/* routine to process MIB API GET/GET NEXT response */

IMPORT void ospfv3_envoy_processGetResp( VB_T           *vbp,
                                         SNMP_PKT_T     *pktp,
                                         envoyOspfv3Request_t *pEnvoyOspfv3Req,
                                         mApiRequest_t  *pRequest,
                                         mApiReqType_t   reqType,
                                         int             mApiOidOffset );

/* routine to process MIB API GET/GET NEXT response for scalar objects */

IMPORT void ospfv3_envoy_processScalarGetResp( VB_T           *vbp,
                                               SNMP_PKT_T     *pktp,
                                               envoyOspfv3Request_t *pEnvoyOspfv3Req,
                                               mApiRequest_t  *pRequest,
                                               mApiReqType_t   reqType,
                                               int             mApiOidOffset );

/* misc method routines related to this Envoy Management Facility */

IMPORT void ospfv3Envoy_undo( OIDC_T      lastmatch,
                              int         tcount,
                              OIDC_T     *tlist,
                              SNMP_PKT_T *pktp,
                              VB_T       *vbp );
IMPORT void ospfv3_envoy_clearBuffer( envoyOspfv3Request_t *pEnvoyOspfv3Req );

IMPORT void ospfv3_envoy_mApiDestroy( envoyOspfv3Request_t *pEnvoyOspfv3Req );

IMPORT envoyOspfv3Request_t *ospfv3_envoy_mApiInit( int numBuf );

/* routines for setting snmp errors */

IMPORT void ospfv3_envoy_setGenError( SNMP_PKT_T    *pktp,
                                      VB_T          *vbp,
                                      mApiReqType_t  reqType );

IMPORT void ospfv3_envoy_setNoSuchNameError( SNMP_PKT_T    *pktp,
                                             VB_T          *vbp,
                                             mApiReqType_t  reqType );

IMPORT void ospfv3_envoy_processGetRespError( SNMP_PKT_T    *pktp,
                                              VB_T          *vbp,
                                              mApiReqType_t  reqType,
                                              int            mApiErr );

#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __INCospfv3EnvoySnmpApi_h */
