/************************************************************************/
/* Copyright 2000-2005 Wind River Systems, Inc.                         */
/************************************************************************/
/*
modification history
--------------------
01a,01mar2005,hms	created                        
*/

#if !defined (__IKECERTS_H__)
#define __IKECERTS_H__

#ifdef IKE_CERTIFICATE_SUPPORT

#include <vxWorks.h>
#include <openssl/x509_vfy.h>

extern STATUS ikeCAListSet
    (
    char * CAFilenames  /* a comma-delimited list of CA filenames */
    );

extern STATUS ikeCRLListSet
    (
    char * CRLFilenames  /* a comma-delimited list of CRL filenames */
    );

extern STATUS ikeCertStoreVerifyFlagsSet
    (
    unsigned long flags
    );

extern STATUS ikeCertStoreRefresh();

#endif /* IKE_CERTIFICATE_SUPPORT */

#endif /* __IKECERTS_H__ */

