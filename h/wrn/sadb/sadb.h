/* sadb.h - API routines for Security Association Database */

/* Copyright 2005 Wind River Systems, Inc. */

/*
modification history
--------------------
01a,26apr05,rlm  Creation.
*/

#ifndef __SADB_H
#define __SADB_H

#if defined(__cplusplus) || defined(c_plusplus)
extern "C" {
#endif


/*
DESCRIPTION
This file contains API routines for accessing the security association
database (SADB)

INCLUDE FILES:
*/

/* includes */

/* defines */

/* typedefs */

/* public functions */

void sadbMsgCClientInit
    (
    void
    );

int sadbDebug
    (
    char *p_action /* configuration string */
    );

STATUS sadbDump
    (
    void
    );

int sadbMon
    (
    char *p_action /* configuration string */
    );

STATUS sadbShow
    (
    void
    );

STATUS sadbShowSA
    (
    char *pConfStr /* configuration string */
    );

STATUS sadbPSKeepAlive
    (
    char *pConfStr /* configuration string */
    );

void sadbPSSetGlobalKeepAlive
    (
    BOOL keep_alive /* TRUE to enable, FALSE to disable */
    );

BOOL sadbPSGetGlobalKeepAlive
    (
    void
    );

STATUS sadbPSGlobalKeepAlive
    (
    char *p_action /* "enable", "disable", or "status" */
    );

#if defined(__cplusplus) || defined(c_plusplus)
}
#endif /* __cplusplus */

#endif /* __SADB_H */

