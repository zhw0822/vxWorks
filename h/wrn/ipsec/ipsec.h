/* ipsec.h - Interface routines for IPsec configuration */

/* Copyright 2005 Wind River Systems, Inc. */

/*
modification history
--------------------
01c,06may05,djp  Added ipsecUsrAppInit()
01b,05may05,rlm  Moved proto for ipsecNetJobMaxSet() from private ipsecP.h to
                 here. (Needed by configlette usrNetIpsecInit.c)
01a,26apr05,rlm  Creation.
*/

#ifndef __IPSEC_H
#define __IPSEC_H

#if defined(__cplusplus) || defined(c_plusplus)
extern "C" {
#endif


/*
DESCRIPTION

INCLUDE FILES:
*/

/* includes */

/* defines */

/* typedefs */

/* public functions */

void ipsecUsrAppInit(void);

void ipsecShowVer
    (
    void
    );

STATUS ipsecAttachIf
    (
    char *cptrAddrString /* IP address */
    );

STATUS ipsecDetachIf
    (
    char *cptrAddrString /* IP address */
    );

STATUS ipsecDFBit
    (
    char *cptrDFBitConfig /* parameter string */
    );

STATUS ipsecSetPMTUAge
    (
    char *cptrPMTUAgeConfig /* parameter string */
    );

STATUS ipsecShow
    (
    void
    );

STATUS ipsecShowIf
    (
    void
    );

int ipsecMon
    (
    char *cptr_action /* parameter string */
    );

int ipsecDebug
    (
    char *cptr_action /* parameter string */
    );

/* NOTE: ipsecNetJobMaxSet() is NOT a public API. However the prototype is
 * required to be here so that the usrNetIpsecInit.c configlette can access it.
 */
STATUS ipsecNetJobMaxSet
    (
    int maxJobs
    );

#if defined(__cplusplus) || defined(c_plusplus)
}
#endif /* __cplusplus */

#endif /* __IPSEC_H */

