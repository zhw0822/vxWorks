/* ipsec_mkm.h - API functions for IPsec MKM Configuration */

/* Copyright 2005 Wind River Systems, Inc. */

/*
modification history
--------------------
01a,26apr05,rlm  Creation.
*/

#ifndef __IPSEC_MKM_H
#define __IPSEC_MKM_H

#if defined(__cplusplus) || defined(c_plusplus)
extern "C" {
#endif


/*
DESCRIPTION

INCLUDE FILES:
*/

/* includes */

/* defines */

/* typedefs */

/* public functions */

BOOL mkmInitialize
    (
    void
    );

void mkmShutdown
    (
    void
    );

STATUS mkmAddBypass
    (
    char *cptr_mkm_sa
    );

STATUS mkmAddDiscard
    (
    char *cptr_mkm_sa
    );

STATUS mkmAddTransport
    (
    char *cptr_mkm_sa
    );

STATUS mkmAddTunnel
    (
    char *cptr_mkm_sa
    );

STATUS mkmSetInboundAH
    (
    char *cptr_value_string
    );

STATUS mkmSetOutboundAH
    (
    char *cptr_value_string
    );

STATUS mkmSetInboundESP
    (
    char *cptr_value_string
    );

STATUS mkmSetOutboundESP
    (
    char *cptr_value_string
    );

STATUS mkmCommit
    (
    char *cptr_mkm_sa
    );

STATUS mkmCommitAll
    (
    void
    );

STATUS mkmAbortAdd
    (
    char *cptr_mkm_sa
    );

STATUS mkmDeleteSA
    (
    char *cptr_mkm_sa
    );

STATUS mkmShow
    (
    void
    );

#if defined(__cplusplus) || defined(c_plusplus)
}
#endif /* __cplusplus */

#endif /* __IPSEC_MKM_H */
