/* ipsecInit.h - IP Security Initialization header file */

/* Copyright 2000-2005 Wind River Systems, Inc. */

/*
modification history
--------------------
01d,06may05,rlm  Added prototype for usrIpsecInit(). Removed legacy K&R C
                 prototype for ikeInit().
01c,30nov04,jfb  Beautified again
01b,29nov04,jfb  Beautified
01a,24mar00,aos  written.
*/

#ifndef __ipsecInit_h__
#define __ipsecInit_h__

#ifdef __cplusplus
extern "C"
    {
#endif

#if ((CPU_FAMILY==I960) && (defined __GNUC__))
  #pragma align 1 /* tell gcc960 not to optimize alignments */
#endif          /* CPU_FAMILY==I960 */

/* ipsec configuration parameters */
typedef struct ipseccfgparams
    {
    unsigned int task_priority;
    } IPSEC_CFG_PARAMS;

#if ((CPU_FAMILY==I960) && (defined __GNUC__))
  #pragma align 0 /* turn off alignment requirement */
#endif          /* CPU_FAMILY==I960 */

#ifndef IPSEC_AUTO_START
  #define IPSEC_AUTO_START 1
#endif /* IPSEC_AUTO_START */

#ifndef IPSEC_TASK_PRIORITY
  #define IPSEC_TASK_PRIORITY 49
#endif /* IKE_TASK_PRIORITY */

/* function declarations */

extern STATUS ipsecInit
    (
    char         * ipsecCfg,
    unsigned int   task_priority
    );

STATUS usrIpsecInit
    (
    int auto_start
    );

#ifdef __cplusplus
    }
#endif

#endif /* __ipsecInit_h__ */
