/* mkmInit.h - header file for ipsec_mkm_init.c */
/* Copyright 2000-2001 Wind River Systems, Inc. */
/************************************************************************/

#if !defined (__MKM_INIT_H__)
#define __MKM_INIT_H__

#ifdef __cplusplus
extern "C"{
#endif
#if ((CPU_FAMILY==I960) && (defined __GNUC__))
#pragma align 1 /* tell gcc960 not to optimize alignments */
#endif          /* CPU_FAMILY==I960 */
/* manual key manager configuration parameters */

#if ((CPU_FAMILY==I960) && (defined __GNUC__))
#pragma align 0 /* turn off alignment requirement */
#endif          /* CPU_FAMILY==I960 */
#ifndef MKM_AUTO_START
#define MKM_AUTO_START          1
#endif

/* function declarations */
#if defined(__STDC__) || defined(__cplusplus)
extern STATUS mkmInit
    (
    char *mkmCfg
    );
#else  /* __STDC__ */
extern STATUS mkmInit ();
#endif /* __STDC__ */
#ifdef __cplusplus
}
#endif
#endif /* __MKM_INIT_H__ */
