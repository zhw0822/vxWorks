/* ospfCommonLib.h - OSPF commmon header file */

/* Copyright 2003 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*
modification history
--------------------
01f,22apr04,ram AVL & Memory migration to shared library
01e,07apr04,ram OSPF v2/v3 coexistance compile
01d,06apr04,ram OSPF v2/v3 coexistance changes
01c,12aug03,agi ported to Accordion stack
01b,05aug03,agi changed include file list
                updated copyright
01a,28jul03,agi created (ported from OSPFv2)
*/

#ifndef __INCospfCommonLibh
#define __INCospfCommonLibh

#ifdef __cplusplus
extern "C" {
#endif

#if defined (WIN32)
#define OSPF_EXPORT __declspec(dllexport)
#else
#define OSPF_EXPORT
#endif

#define MINIMUM_UINT(a,b) ((a)<(b)?(a):(b))

#include <vxWorks.h>    /* VxWorks standard typedefs */

#if defined (__VXWORKS__)
    #if !defined (WIN32)
        #define _pack __attribute__ ((packed))  /* gnu cc pack attribute */
    #endif
#else
    #define _pack
#endif

#if! defined bool
    typedef enum bool
    {
        true = 1,
        false = 0
    } bool;
#endif /* bool */

#include <ospfCommonUtils.h>
#include <ospfCommonVnvramStr.h>
#include <ospfCommonPrototypes.h>
#include <ospfCommonMemPart.h>

#ifdef __cplusplus
}
#endif

#endif /* __INCospfCommonLibh */
