/* ospfCommonVnvramStr.h - OSPF common NVRAM header file */

/* Copyright 2003 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*
modification history
--------------------
01b,06apr04,ram OSPF v2/v3 coexistance changes
01a,29may03,agi created (ported from OSPFv2)
*/

#ifndef __INCospfCommonVnvramStrh
#define __INCospfCommonVnvramStrh

#ifdef __cplusplus
extern "C" {
#endif

typedef struct  OSPF_CONFIGURATION_FUNCTION
    {
    void (*fptr_parameter_function) (
        char *cptr_start_of_configuration_string,
        ULONG parameter_1,
        ULONG ulptr_parameter_2,
        ULONG parameter_3);

    BOOLEAN *eptr_enable_string;
    char *cptr_parameter_string;
    ULONG   parameter_1;
    ULONG   ulptr_parameter_2;
    ULONG   parameter_3;
    ULONG   minimum_value;
    ULONG   maximum_value;
    } OSPF_CONFIGURATION_FUNCTION;

typedef struct  OSPF_CONFIGURATION_TABLE
    {
    BOOLEAN                 valid;
    OSPF_CONFIGURATION_FUNCTION function[200];
    } OSPF_CONFIGURATION_TABLE;

typedef struct  OSPF_NVRAM_CLASS
    {
    BOOLEAN display_configuration_file;
    BYTE    current_configuration_section_index;
    OSPF_CONFIGURATION_TABLE *  sptr_current_configuration_table;
    BYTE    value[12];
    BYTE    number_of_configurations;
    } OSPF_NVRAM_CLASS;


#ifdef __cplusplus
}
#endif

#endif /* __INCospfCommonVnvramStrh */
