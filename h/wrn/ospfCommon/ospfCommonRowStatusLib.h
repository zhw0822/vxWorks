/* ospfCommonRowStatusLib.h - OSPF common row status header file. */

/* Copyright 2003 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*
modification history
--------------------
01a,06aug03,agi created (ported from OSPFv2)
*/

/*
DESCRIPTION
This file defines the types and function prototypes for rowStatusLib
*/

#ifndef __INCospfCommonRowStatush
#define __INCospfCommonRowStatush

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/* defines */


/* enums */

/* Action/state enum type */
typedef enum
{
    RS_undefined = 0,   /* row does not exist */
    RS_active,          /* active */
    RS_notInService,    /* not in service */
    RS_notReady,        /* not ready */
    RS_createAndGo,     /* create and go */
    RS_createAndWait,   /* create and wait */
    RS_destroy,         /* destroy */
    RS_setOtherObject,  /* set a non-RowStatus object */
    /* all others are errors */
    RS_inconsistValue,  /* inconsistent */
    RS_wrongValue,      /* wrongValue */
    RS_genError,        /* Generic ERROR */
    RS_noCreation       /* could not create row */
} rsState_t;

typedef rsState_t rsAction_t;

#define RS_noError      RS_undefined
#define RS_stateMax     RS_notReady
#define RS_actionMax    RS_setOtherObject

/* condition type */
typedef enum
{
    RS_NO_CONDITION = 0,  /* unconditional */
    RS_CONDITION1,      /* notInService is supported */
    RS_CONDITION2,      /* all columns are valid */
    RS_CONDITION3,      /* notInService supported and all columns are valid */
    RS_CONDITION4,      /* creation allowed when modifying a non-existing row */
    RS_CONDITION5      /* modifying a column of an active row is supported */
} rsCondition_t;

typedef enum
{
    RS_ACCESS_NONE = 0,
    RS_ACCESS_RS_GET,       /* row status value get */
    RS_ACCESS_RS_SET,       /* row status value set */
    RS_ACCESS_RS_VALID,     /* validate row status value */
    RS_ACCESS_COL_SET,      /* column set */
    RS_ACCESS_COL_VALID,    /* validate column */
    RS_ACCESS_ROW_VALID,    /* validate row */
    RS_ACCESS_ROW_DELETE    /* delete row */
} rsRowAccessCode_t;

typedef enum 
{
    RS_GET_REQUEST = 1,
    RS_SET_NVM_REQUEST,
    RS_VALIDATE_REQUEST,
    RS_COMMIT_REQUEST,
    RS_ERROR_REQUEST
} rsRequest_t;

/* typedefs */

typedef STATUS (* RS_CREATE_FUNC) (void * pRequest, void ** pEntry,
                                    rsAction_t action, rsRequest_t rsRequest);

typedef STATUS (* RS_ACCESS_FUNC) (void * pEntry, void * pObjInfo,
                                   rsRowAccessCode_t accessFlag);

typedef STATUS (* RS_STATUS_FUNC) (void *pEntry, rsState_t *rsState, 
                                   rsRowAccessCode_t accessFlag);

typedef STATUS (* RS_ERROR_FUNC) ( void * pObjInfo, int rsError);

typedef struct
{
    rsState_t       state1;         /* next state 1 if there was no error */
    rsState_t       state2;         /* next state 2 if there was no error */
    rsCondition_t   cond;           /* if non-zero, state is conditional */
    rsState_t       error;          /* error code if error occured */
} rsRowStatus_t;

typedef struct
{
    BOOL            notInService;         /* is notInService supported ? */
    BOOL            modifyActive;         /* is modifying an active row supported ? */
    BOOL            createAndGo;          /* is createAndGo supported ? */
    BOOL            createWhileValidate;  /* is createWhileValidate supported? */
    RS_CREATE_FUNC  rowCreateRtn;         /* row manipulation routine */
    RS_ACCESS_FUNC  rowAccessRtn;         /* row manipulation routine */
    RS_STATUS_FUNC  rowStatusAccessRtn;   /* status object manipulation routine */
    RS_ERROR_FUNC   errorConvertRtn;      /* error conversion routine */
    rsAction_t      rsShadow;             /* shadow value for the next rowStatus action 
                                           * state for handling the createAndWait and 
                                           * createAndGo row operations 
                                           */

} rsParams_t;

/* externs */

IMPORT STATUS rsRowStatusSet (rsParams_t * pParams, void * pRequest,
                              rsAction_t action, rsRequest_t rsRequest,
                              BOOL lastCol);
IMPORT STATUS rsParamsInit (rsParams_t ** ppParams, 
                            RS_CREATE_FUNC  rowCreateRtn,
                            RS_ACCESS_FUNC  rowAccessRtn, 
                            RS_STATUS_FUNC  rowStatusAccessRtn, 
                            RS_ERROR_FUNC   errorConvertRtn);
IMPORT void rsParamsDelete (rsParams_t * pParams);
IMPORT STATUS rsParamsNotInServiceSet (void * pParams, BOOL notInService);
IMPORT STATUS rsParamsModifyActiveSet (void * pParams, BOOL notInService);
IMPORT STATUS rsParamsCreateAndGoSet (void *  pParams, BOOL createAndGo);
IMPORT STATUS rsParamsCreateWhileValidateSet (void *  pParams, BOOL createWhileValidate);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __INCospfCommonRowStatush */
