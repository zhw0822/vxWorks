/* dot1xSuppLibP.h - 802.1X supplicant private header */

/* Copyright 2002-2005 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*
modification history
--------------------
01k,04may05,eja  Add key event type
01j,11apr05,eja  Add enums for assoc/disassoc events
01i,23mar05,eja  Remove unused event types
01h,22mar05,eja  Add macros for timer task
01g,18mar05,eja  Add new timeout macro
01f,01mar05,eja  Code cleanup
01e,04feb05,eja  80211i/WPA integration
01d,18jan05,eja  Moved 802.11i defines to commonIoctl file
01c,12jan05,rb  Added DOT1X_PORTSTATUS structure
01b,10jan05,rb  Added definitions for 802.11 integration
01a,05jan05,eja  Remove inclusion of wlanEnd.h
*/

/*
DESCRIPTION
*/

#ifndef __INCdot1xSuppLibPh
#define __INCdot1xSuppLibPh

#ifdef __cplusplus
extern "C" {
#endif

/* headers */
#include <vxWorks.h>
#include <intLib.h>
#include <wrn/dot1x/dot1xEapolLib.h> 
#include <wrn/dot1x/dot1xSuppLib.h>

/* Task configuration */
#define DOT1X_SUPP_TASK_NAME "tDot1xSupp"
#define DOT1X_SUPP_TASK_PRIORITY 100
#define DOT1X_SUPP_TASK_STACK_SIZE 10000
#define DOT1X_MSG_TIMEOUT 50
#define DOT1X_SUPP_TMR_NAME "tDot1xTmr"
#define DOT1X_SUPP_TMR_PRIORITY 150
#define DOT1X_SUPP_TMR_STACK_SIZE 10000

/* Default timer periods in seconds */
#define DOT1X_SUPP_AUTH_PERIOD 30
#define DOT1X_SUPP_HELD_PERIOD 60
#define DOT1X_SUPP_START_PERIOD 30
#define DOT1X_SUPP_ERROR_WAIT 10

/* Default number of EAPOL-Starts before giving up */
#define DOT1X_SUPP_MAX_START 3

/* dot1xSuppStatus */
#define DOT1X_SUPP_UNAUTHORIZED 0
#define DOT1X_SUPP_AUTHORIZED 1

/* EAP request or response for identity type */
#define EAP_RESPONSE_HEADER_LENGTH 5

/* EAP size of a NAK response */
#define EAP_NAK_SIZE 6

/* Message handling */
#define DOT1X_SUPP_MAX_MESSAGES 20

/* Timeouts if 1/2 second, 1 second period for timer */
#define ONE_SECOND (sysClkRateGet())
#define HALF_SECOND (ONE_SECOND>>1)
#define HALF_SECOND_SAFE (intContext() ? NO_WAIT : HALF_SECOND)

/* Types of messages used internally by the supplicant to indicate events */
typedef enum
    {
    SUPP_DATA_PACKET = 0, /* Incoming EAP packet event */
    SUPP_CONNECT_EVENT, /* Incoming event from driver */
    SUPP_DISCONNECT_EVENT, /* Incoming disconnect from driver */
    SUPP_KEY_SET_EVENT /* Incoming key packet */
    } DOT1X_SUPP_EVENT_TYPE;

/* Message used for signalling supplicant events internally */
typedef struct
    {
    INT32 type; /* Event Type */
    VOID *id; /* Event ID */
    VOID *data; /* Message data */
    } DOT1X_SUPP_EVENT;

/* Application configuration */
#define DOT1X_MODE_DEFAULT DOT1X_MODE_WIRELESS
#define DOT1X_UNICAST_SUPPORT FALSE

/* For timing */
IMPORT int sysClkRateGet (void);


#ifdef __cplusplus
}
#endif

#endif /* __INCdot1xSuppLibPh */
