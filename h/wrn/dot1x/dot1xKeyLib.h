/* dot1xKeyLib.h - IEEE 802.1X Key management interface header file*/

/* Copyright 2002-2003 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*
modification history
--------------------
01b,21apr03,ggg  File name, location change
01a,28Feb02,myz  written
*/


#ifndef __INCdot1xKeyLibh
#define __INCdot1xKeyLibh

#ifdef __cplusplus
extern "C" {
#endif

/*** defines ***/

/* dot1xKeyLibInit argument values */

#define DOT1X_KEY_TIME_ASYNC_MODE     0  /* run in asynchronise mode */
#define DOT1X_KEY_TIME_SYNC_MODE      1  /* run in synchronise mode  */

/*** typedefs ***/

typedef enum {
    DOT1X_AUTH_SNTP_COUNTER,    /* Use NTP time value for the replay counter */
    DOT1X_AUTH_LOCAL_COUNTER    /* Use local count value for the replay counter*/
    } DOT1X_AUTH_REPLAY_CNT_TYPE;

typedef struct {
    UINT8 * pKey;    /* points to a key */
    int     len;     /* key length */
    } DOT1X_AUTH_GENERIC_KEY_DESC;

typedef struct {
    DOT1X_AUTH_GENERIC_KEY_DESC key;   /* the key descriptor */
    UINT8 index;                     /* key index */
    } DOT1X_AUTH_WEP_KEY_DESC;

#if defined(__STDC__) || defined(__cplusplus)

extern STATUS dot1xAuthWepKeyGenerate (DOT1X_AUTH_GENERIC_KEY_DESC *,
                                     DOT1X_AUTH_GENERIC_KEY_DESC *);
extern STATUS dot1xAuthEapolKeyPacketBuild (char *, UINT8,
	                                  DOT1X_AUTH_GENERIC_KEY_DESC *,
	                                  DOT1X_AUTH_WEP_KEY_DESC *);
extern void dot1xKeyLibInit (DOT1X_AUTH_REPLAY_CNT_TYPE,int,char *, FUNCPTR);
extern void dot1xKeyNtpTimeServerSet  (char *);
extern void dot1xKeyNtpTimeGetFuncSet (FUNCPTR);
extern void dot1xKeyRandomKeySet  (UINT8 *);
extern void dot1xKeyRandomSeedSet (UINT8 *,int);
extern void dot1xKeyNtpTimeGetAsync (void);
extern void dot1xKeyNtpTimeGet (void);

#else

extern STATUS dot1xAuthWepKeyGenerate();
extern STATUS dot1xAuthEapolKeyPacketBuild();
extern void dot1xKeyLibInit();
extern void dot1xKeyNtpTimeGetAsync ();
extern void dot1xKeyNtpTimeGet ();
extern void dot1xKeyNtpTimeServerSet();
extern void dot1xKeyNtpTimeGetFuncSet();
extern void dot1xKeyRandomSeedSet ();
extern void dot1xKeyRandomKeySet  ();

#endif

#ifdef __cplusplus
}
#endif

#endif  /* end #ifndef __INCdot1xKeyLibh */
