/* dot1xAuthSvrLib.h - IEEE 802.1X Authetication Server interface file*/

/* Copyright 2002-2003 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*
modification history
--------------------
01b,21apr03,ggg  File name, location change
01a,08Feb02,myz  written
*/


#ifndef __INCdot1xAuthSvrLibh
#define __INCdot1xAuthSvrLibh

#ifdef __cplusplus
extern "C" {
#endif

#define DOT1X_AS_NORMAL_SHUTDOWN 0  /* Library shutdown type, normal shutdown*/
#define DOT1X_AS_FORCE_SHUTDOWN  1  /* Library shutdown type, forced shutdown*/
#define DOT1X_AS_STATION_ID_LEN  6  /* attribute station ID length */

#define DOT1X_AS_WIRELESS_PORT_TYPE   19  /* wireless port type */  
#define DOT1X_AS_ETHERNET_PORT_TYPE   15  /* ethernet port type */

#define DOT1X_AS_SESSION_SEND_KEY     1   /* key type, session send key */
#define DOT1X_AS_SESSION_RCEV_KEY     2   /* key type, session receive key */

/* Result codes of the authentication server interface */

enum DOT1X_AS_CODE {
    DOT1X_AS_ACCESS_ACCEPT = 1,    /* access accept code */
    DOT1X_AS_ACCESS_REJECT,        /* access reject code */ 
    DOT1X_AS_ACCOUNTING_RESPONSE,  /* accounting response code */
    DOT1X_AS_ACCESS_CHALLENGE,     /* access challenge code */
    DOT1X_AS_SESSION_KEY,          /* session key received code */
    DOT1X_AS_GEN_ERROR,            /* general error code */
    DOT1X_AS_RETRY_LIMIT_REACHED   /* server retry limit reached code */
    };

/*** typedefs ***/

/* attribute header structure, attributes deliverd to user */

typedef struct {
    UINT16 type;    /* what type of attribute */
    UINT16 len;     /* length of the attribute, excluding header */
    } DOT1X_AS_ATTR_HEADER_T;

#define DOT1X_AS_ATTR_HEADER_LEN (sizeof(DOT1X_AS_ATTR_HEADER_T))

/* The required user supplied info for sending the EAP packet */

typedef struct {
    FUNCPTR pUserCallback; /* routine called when reply comes back */
    int mtu;               /* MTU Max Tranmission Unit */
    char * pId;            /* User Identification */
    int  len;              /* length of the User Identification */
    char callingId[DOT1X_AS_STATION_ID_LEN]; /* source MAC address */
    char calledId [DOT1X_AS_STATION_ID_LEN]; /* Port MAC address */
    UINT8 * pState;        /* State attribute if there is one */
    int   sLen;            /* length of the previous field */
    int   portType;        /* port type */
    } DOT1X_AS_REQUIRED_INFO;


#if defined(__STDC__) || defined(__cplusplus)

extern STATUS dot1xAuthServerEapMsgSend (UINT32 msgHandle, UINT8 * pMsg,
                                  int     len,
                                  DOT1X_AS_REQUIRED_INFO * pInfo);
extern STATUS dot1xAuthServerLibInit (void);
extern STATUS dot1xAuthServerLibShutDown (int);

#else

extern STATUS dot1xAuthServerEapMsgSend();
extern STATUS dot1xAuthServerLibInit ();
STATUS dot1xAuthServerLibShutDown ();

#endif

#ifdef __cplusplus
}
#endif

#endif /* __INCdot1xAuthSvrLibh */
