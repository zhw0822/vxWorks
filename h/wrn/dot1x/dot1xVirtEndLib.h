/* dot1xVirtEndLib.h - Virtual End driver */

/* Copyright 2002-2005 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*
modification history
--------------------
01a,26Jul05,eja  Created
*/

#ifndef __INCdot1xVirtEndLibh
#define __INCdot1xVirtEndLibh

#ifdef __cplusplus
extern "C" {
#endif

#include <vxWorks.h>

/* typedefs */
typedef int (*VIRT_END_CFG_HANDLER)();

/* defines */
#define	DOT1X_VIRT_END_DEV_NAME "dot1x"
#define DOT1X_VIRT_END_DEV_NAME_LEN (strlen(DOT1X_VIRT_END_DEV_NAME) + 1)

/* enums */
typedef enum
    {
    DOT1X_VIRT_END_SUPP_HDLR = 0,
    DOT1X_VIRT_END_AUTH_HDLR
    } DOT1X_VIRT_END_HDLR;

void * dot1xVirtEndDevInit(void);
STATUS dot1xVirtEndDevHandlerInstall(void *, int, void *);

#ifdef __cplusplus
}
#endif

#endif /* __INCdot1xVirtEndLibh */
