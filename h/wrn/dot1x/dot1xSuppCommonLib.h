/* dot1xMdLib.h - 802.1X supplicant common routines header file */

/* Copyright 2003 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*
modification history
--------------------
01a,26aug03,ggg  written.
*/

#ifndef __INCdot1xSuppCommonLibh
#define __INCdot1xSuppCommonLibh

#ifdef __cplusplus
extern "C" {
#endif

extern void dot1xSuppUnicode(char *, char *);
extern void dot1xSuppNtPasswordHash(UINT8 *, int, UINT8 *);
extern void dot1xSuppDesKeyBitInsert(UINT8 *,   UINT8 *);
extern void dot1xSuppChallengeResponse(UINT8 * , UINT8 * , UINT8 *);

#ifdef __cplusplus
}
#endif

#endif /* __INCdot1xSuppCommonLibh */

