/* dot1xMdLib.h - 802.1X supplicant message digest library header file */

/* Copyright 2003 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*
modification history
--------------------
01a,21apr03,ggg  written.
*/

#ifndef __INCdot1xMdLibh
#define __INCdot1xMdLibh

#ifdef __cplusplus
extern "C" {
#endif

/* Algorithms: MD4 or MD5 */
typedef enum
{
        MD4 = 0x04,
        MD5 = 0x05
} MD_ALGORITHM;

#define HMAC_MD5_RESULT_LENGTH          16      /* HMAC-MD5 result length */

/* Prototypes */
extern void dot1xMd(unsigned char *, unsigned long, unsigned char *,
                    MD_ALGORITHM);
extern void dot1xHmacMd5(unsigned char *, int, unsigned char *, int,
                         unsigned char *);

#ifdef __cplusplus
}
#endif

#endif /* __INCdot1xMdLibh */
