/* dot1xSuppTlsLib.h - 802.1X EAP-TLS Authentication header */

/* Copyright 2002-2005 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*
modification history
--------------------
01h,05aug05,eja  Version 2.1 update
01g,25jul05,eja  API change and cleanup
01f,27jun05,eja  Cleanup
01e,07jun05,eja  Fix for trace bug B0490
01d,18mar05,eja  Reduced session count to 1
01c,01mar05,eja  Code cleanup
01b,17feb05,eja  Remove EAP_TLS time macros
01a,21dec04,eja  Add public API prototype to header
*/

/*
DESCRIPTION
*/
#ifndef __INCdot1xSuppTlsLibh
#define __INCdot1xSuppTlsLibh

#ifdef __cplusplus
extern "C" {
#endif

/* Library header dependencies */
#include <vxWorks.h>
#include <openssl/ssl.h>
#include <wrn/dot1x/dot1xSuppLib.h>
#include <wrn/dot1x/dot1xEapLib.h>

/* Max ttls sessions */
#define EAP_TLS_MAX_SESSIONS (1)

/* Inner authentication handler init type */ 
typedef SUPP_SESSION_OBJ * (* EAP_TLS_AUTH_HDLR)(); 

/* IO handlers */
typedef VOID (*EAP_TLS_TX_HDLR)(); 
typedef VOID (*EAP_TLS_RX_HDLR)(); 

/* Defines an EAP-TLS packet */
typedef struct eap_tls_pkt
    {
    EAP_HDR hdr;   /* Eap Packet header */
    char    type;  /* Packet Type field */
    char    flags; /* Flags L(0)Length  M(1)More fragments S(2)Start */
    } _WRS_PACK_ALIGN(1) EAP_TLS_PKT_HDR; 

/* EAP-TLS flag bits */
#define EAP_TLS_LENGTH_INCLUDED ((UCHAR)0x80)
#define EAP_TLS_FIRST_FRAGMENT EAP_TLS_LENGTH_INCLUDED
#define EAP_TLS_MORE_FRAGMENTS ((UCHAR)0x40)
#define EAP_TLS_START_SESSION ((UCHAR)0x20)
#define EAP_TLS_ACK_PACKET ((UCHAR)0x00)
#define EAP_TLS_MAX_FRAG_SZ (1300)
#define EAP_TLS_SESSION_KEY_SIZE (32) 
#define EAP_TLS_MAX_ENCRYPT_LABEL_SIZE (256)

/* EAP-TLS authentication states */
typedef enum
    {
    EAP_TLS_WAIT_FOR_START = 0, /* Waiting to start */
    EAP_TLS_WAIT_FOR_ACK,       /* Waiting for acknowledgement from server */
    EAP_TLS_WAIT_FOR_REQUEST,   /* Waiting for request */
    EAP_TLS_WAIT_FOR_FRAG,      /* Waiting on next packet fragment */
    EAP_TLS_WAIT_FOR_SUCCESS,   /* Waiting for success */
    EAP_TLS_AUTHENTICATED,      /* Authentication is complete */
    EAP_TLS_IDLE                /* Idle state */
    } EAP_TLS_AUTH_STATE;

/* EAP-TLS authentication protocols */
typedef enum
    {
    EAP_TLS_AUTH_TTLS = 0, /* Used in TTLS context */
    EAP_TLS_AUTH_PEAP,     /* Used in PEAP context */
    EAP_TLS_AUTH_TLS       /* Used in EAP-TLS context */
    } EAP_TLS_AUTH_METHOD;

/* EAP-TLS Packet types */
typedef enum
    {
    EAP_TLS_RESPONSE, /* Resonse packet */
    EAP_TLS_REQUEST,  /* Request packet */
    EAP_TLS_ACK,      /* ACK packet */
    EAP_TLS_NAK,      /* NAK packet */ 
    EAP_TLS_ERROR     /* ERROR */
    } EAP_TLS_PKT_TYPE;

/* EAP-TLS macros */
#define EAP_TLS_MAX_SIZE (16384+sizeof(EAP_HDR)) /* Max packet 16388 */
#define EAP_TLS_SESSION_KEY_SIZE (32) /* Size of session key */
#define EAP_TLS_ENCRYPT_LABEL "client EAP encryption" /* Used in key PRF */
#define EAP_TLS_HANDSHAKE_DONE (3)

/* Stats structure */
typedef struct eap_tls_ssl_stats_s
    {
    INT32 rxFragments; /* Total number of Rx TLS record fragments */
    INT32 txFragments; /* Total number of Tx TLS record fragments */
    INT32 rxRecords;   /* Total number of Rx TLS records */
    INT32 txRecords;   /* Total number of Tx TLS records */
    INT32 txErrors;    /* Total number of Tx errors */   
    INT32 rxErrors;    /* Total number of Rx errors */
    INT32 rxBytes;     /* Total number of Rx bytes */
    INT32 txBytes;     /* Total number of Txx bytes */
    } EAP_TLS_SSL_STATS_OBJ;

/* Defines SSL context */
typedef struct eap_tls_ssl_s
    {
    SSL * object;                   /* Pointer to SSL info */
    BIO * source;                   /* SSL input BIO */
    BIO * sink;                     /* SSL output BIO */
    char outData[EAP_TLS_MAX_SIZE]; /* output buffer from SSL */
    UINT32 outSize;                 /* size of SSL output */
    char * outPtr;                  /* locaton in output buffer from SSL */
    UINT32 outSizeLeft;             /* size of SSL output remaining */
    char inData[EAP_TLS_MAX_SIZE];  /* input buffer to SSL */
    UINT32 inSize;                  /* size of SSL input */
    char * inPtr;                   /* location in input buffer to SSL */
    struct timespec time;           /* Unix style time structure */
    EAP_TLS_SSL_STATS_OBJ stats; 
    } EAP_TLS_SSL_OBJ;

typedef struct eap_tls_ssl_ctx_s
    {
    SSL_CTX *       ctx;     /* ssl context */
    EAP_TLS_SSL_OBJ env;     /* ssl session parameters */
    BOOL            started; /* Flag indicates that a session is in progress */
    BOOL            libInit; /* Flag to indicate context state */
    } EAP_TLS_SSL_CTX;

/* Defines a EAP-TLS IO structure */
typedef struct eap_tls_io_s
    {
    EAP_TLS_TX_HDLR  send;         /* Handles outgoing packets */
    EAP_TLS_PKT_TYPE sendType;     /* Type of packet to create */
    UINT32           sendFlags;    /* Special packet flags */
    EAP_TLS_RX_HDLR  receive;      /* Handles incoming packets */
    EAP_TLS_PKT_TYPE receiveType;  /* Type of packet to create */
    UINT32           receiveFlags; /* Special packet flags */
    } EAP_TLS_IO_OBJ;

/* Defines a EAP-TLS Handshake structure */
typedef struct eap_tls_handshake_s
    {
    BOOL  complete; /* Handshake completion state */
    INT32 count;    /* Handshake count - TLS uses 3 way handshake */
    } EAP_TLS_HANDSHAKE_OBJ;

/* Defines a EAP-TLS authentication object */
typedef struct eap_tls_auth_s
    {
    SUPP_SESSION_OBJ      curr;      /* Current Authentication object */
    SUPP_SESSION_OBJ      next;      /* Next Authentication object */
    SUPP_ENV_OBJ *        env;       /* Supplicant parameter cache */
    EAP_TLS_AUTH_STATE    state;     /* TTLS authetication current state */
    EAP_TLS_SSL_CTX       ssl;       /* SSL session context */
    EAP_TLS_IO_OBJ        io;        /* IO mechanism */
    EAP_TLS_HANDSHAKE_OBJ handshake; /* Indicates TLS handshake state */
    } EAP_TLS_SESSION_OBJ;

/* This structure defines the EAP-TLS control object */
typedef struct eap_tls_control_s
    {
    EAP_TLS_SESSION_OBJ * session[EAP_TLS_MAX_SESSIONS]; /* Allowed sessions */
    BOOL                  sessionActive;
    } EAP_TLS_CONTROL_OBJ;

void * dot1xSuppTlsInit(void *);

#ifdef __cplusplus
}
#endif

#endif /* __INCdot1xSuppTlsLibh */
