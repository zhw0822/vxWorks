/* dot1xDebugLibP.h - 802.1X debug header */

/* Copyright 2005-2006 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*
modification history
--------------------
01c,28sep05,eja  Change enum DOT1X_ERROR_NONE to 0
01b,23sep05,eja  Add support for mschapv2, gtc, and md5 debugging areas
01a,09sep05,eja  Make debug call printf from logMsg
*/

/*
DESCRIPTION
*/

#ifndef __INCdot1xDebugLibh
#define __INCdot1xDebugLibh

#include <vxWorks.h>
#include <stdio.h>

/* Dot1x debug levels */
typedef enum 
    {
    DOT1X_DEBUG_NONE = 0,
    DOT1X_DEBUG_FATAL,
    DOT1X_DEBUG_ERROR,
    DOT1X_DEBUG_INFO,
    DOT1X_DEBUG_FLOOD
    } DOT1X_DEBUG_LEVELS;

/* Dot1x debug areas */
typedef enum
    {
    DOT1X_AREA_ALL = 0,
    DOT1X_AREA_INIT,
    DOT1X_AREA_SUPP,
    DOT1X_AREA_AUTH,
    DOT1X_AREA_LEAP,
    DOT1X_AREA_PEAP,
    DOT1X_AREA_TLS,
    DOT1X_AREA_TTLS,
    DOT1X_AREA_MSCHAPV2,
    DOT1X_AREA_MD5,
    DOT1X_AREA_GTC,
    DOT1X_AREA_EAPOL
    } DOT1X_DEBUG_AREAS;
extern INT32 dot1xDebugArray[];

#define DOT1X_LOG(level, area, message) \
    if ((level) <= dot1xDebugArray[(area)]) printf message

#ifdef __cplusplus
extern "C" {
#endif

extern STATUS dot1xDebugLevelSet
    (
    int area, /* Debug area to get level for */
    int level /* Debug level to set */
    );

#ifdef __cplusplus
}
#endif

#endif /* __INCdot1xDebugLibh */
