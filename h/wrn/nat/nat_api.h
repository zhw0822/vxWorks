/* nat_api.h */

/* NAT Application Programming Interface */

/* Copyright 2000-2005 Wind River Systems, Inc. */

/* @format.tab-size 4, @format.use-tabs true, @format.new-line lf */

/*
modification history
--------------------
01c,10mar05,svk  Add port trigger and DMZ host declarations
01b,16feb05,svk  Remove old branding
01a,25apr03,zhu  updated copyright
070201	tk		Replace natXlatAdd and natXlatDelete with natIpXlatAdd and natIpXlatDelete.
				Add comments.
051701	tk		Add new functions declaration: natTcpStaticAdd, natUdpStaticAdd,
				natTcpStaticDelete, natUdpStaticDelete, natPassThruListAdd,
				natPassThruListDelete, natPassThruListShow.
*/

#ifndef nat_api_h
#define nat_api_h

#include "vxWorks.h"

#ifdef __cplusplus
extern "C" {
#endif

/*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
The following API are intended to allow a human client to perform certain
functions that can affect the operations of the NAT core or just to display
certain information that can be useful to the client.  These functions can
be invoked from the Tornado shell (i.e. WindShell) although they can also
be called by a software client.
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*/

/* Display all NAT operating characteristics */
STATUS 	natShow(void);

/* Enable/Disable NAT operations */
STATUS	natEnable(BOOL enable);

/* Enable/Disable a specific NAT port (interface) */
STATUS	natPortEnable(int port, BOOL enable);

/* Enable/Disable static TCP/UDP server translations */
STATUS	natStaticXlatEnable(BOOL enable);

/* Display ICMP Translation List (NAPT mode only) */
STATUS 	natIcmpXlatShow(void);

/* Display all translation lists */
STATUS 	natXlatShow(void);

/* Display TCP translation list */
STATUS 	natTcpXlatShow(void);

/* Display UDP translation list (NAPT mode only) */
STATUS 	natUdpXlatShow(void);

/* Functions to add and delete TCP and UDP static port-based entries (NAPT mode only).
   These functions allow user to expose designated private servers to global clients.
*/
STATUS natTcpStaticAdd(
			char *localAddr, 
			u_short localPort, 
			u_short globalPort
			);

STATUS natUdpStaticAdd(
			char *localAddr, 
			u_short localPort, 
			u_short globalPort
			);

STATUS natTcpStaticDelete(
			char *localAddr, 
			u_short localPort, 
			u_short globalPort
			);

STATUS natUdpStaticDelete(
			char *localAddr, 
			u_short localPort, 
			u_short globalPort
			);

/* Retrieve the global IP address for the specified local address */
STATUS	natGetGlobalAddr(char *localAddr);

/* Set DMZ Host */
STATUS natDMZHostSet (char * hostAddr); 

/* Port Trigger entry action type */
typedef enum 
    {
    NAT_PORT_TRIGGER_ADD = 1,
    NAT_PORT_TRIGGER_ENABLE,
    NAT_PORT_TRIGGER_DISABLE,
    NAT_PORT_TRIGGER_DELETE,
    NAT_PORT_TRIGGER_ENABLE_ALL,
    NAT_PORT_TRIGGER_DELETE_ALL,
    NAT_PORT_TRIGGER_DISABLE_ALL,
    NAT_PORT_TRIGGER_ACTION_END
    } NAT_PORT_TRIGGER_ACTION_TYPE;

/* Port Trigger modes */
#define PORT_TRIGGER_STRICT     1  /* strict mode */
#define PORT_TRIGGER_NONSTRICT  2  /* non-strict mode */

/* Perform Port Trigger entry action */
STATUS natPortTriggerEntryAction (
                                 NAT_PORT_TRIGGER_ACTION_TYPE action,
                                 USHORT mode, 
                                 USHORT triggerProtocol, 
                                 USHORT triggerPortStart,
                                 USHORT triggerPortEnd, 
                                 USHORT incomingProtocol, 
                                 USHORT incomingPortStart,
                                 USHORT incomingPortEnd 
                                 );

#ifdef NAT_PASS_THRU_ENABLE
/* Add/delete address/mask pair to pass-through list.  The address/mask pair
   refers to the destination host.  Once added to the pass-through list,
   all packets sent to this destination host/subnet will not get translated.
   Customers should use this feature with discretion as doing so may expose
   the identities of their private hosts to the global network.
*/
STATUS	natPassThruListAdd(
			char * address, 
			char * mask
			);

STATUS	natPassThruListDelete(
			char * address, 
			char * mask
			);

STATUS  natPassThruListShow();
#endif


/*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
The following API are intended for use by software clients only.  Although
they are invocable from the shell, they won't serve much purpose to human
clients since they won't know in advance what the global port number will be
in the dynamic address translation.  Currently, these API functions are called
by the ALG API functions to create address binds in the NAT core.  They can
be called by any software client that interact with the NAT core to create or 
delete an address bind, although it is recommended that the ALG API functions
natBindSet() and natBindFree() be used instead.
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*/

/* Function to add a TCP/UDP/IP control block (or entry) in the TCP translation
   list maintained by the NAT core.  Note that all entries in the list are dynamic
   in that they get created when needed and deleted when their time stamps
   expired.  The boolean flag static_entry simply serves for informational
   purpose that the entry is created as a result of the presence of the static
   entry which may have been configured at initialization (i.e. by means of the
   configuration file) or by means of API functions specified above in this
   file.
*/
/* TCP Translation Entry Add Routine (NAPT mode only) */
u_long	natTcpXlatAdd(
			u_long 	localAddr,
			u_short localPort,
			u_long 	globalAddr, 
			u_short globalPort,
			BOOL 	static_entry
			);

/* TCP Translation Entry Delete Routine (NAPT mode only) */
STATUS 	natTcpXlatDelete(
			u_long	localAddr, 
			u_short localPort,
			u_long	globalAddr, 
			u_short globalPort
			);

/* UDP Translation Entry Add Routine (NAPT mode only) */
u_long	natUdpXlatAdd(
			u_long 	localAddr, 
			u_short localPort,
			u_long 	globalAddr, 
			u_short globalPort,
			BOOL	static_entry
			);

/* UDP Translation Entry Delete Routine (NAPT mode only) */
STATUS 	natUdpXlatDelete(
			u_long 	localAddr, 
			u_short localPort,
			u_long 	globalAddr, 
			u_short globalPort
			);

/* IP Translation Entry Add Routine (Basic NAT mode only) */
u_long	natIpXlatAdd(
			u_long localAddr, 
			BOOL static_entry
			);

/* IP Translation Entry Delete Routine (Basic NAT mode only) */
STATUS  natIpXlatDelete(
			u_long localAddr
			);

#ifdef __cplusplus
}
#endif

#endif /* Dont' add anything after this line */
