/* natMgmt.h - The definitions for remote management of NAT */

/* Copyright 2000-2005 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*
modification history
--------------------
01j,12jun05,zhu  fixed compiler warning
01i,08apr05,zhu  fixed SPR#103072
01h,28sep04,zhu  added m2NatTreeAdd declaration
01g,14sep04,myz  change DISPLAY_STR_SIZE to NAT_DISPLAY_STR_SIZE to avoid 
                 conflict with the same definition in in6_var.h.
01f,25apr03,svk  Implement version number
01f,25apr03,zhu  updated copyright
01e,28aug01,zhu  making bug fixes
01d,21apr01,zhu  fixing basic NAT bugs
01c,12feb01,zhu  add three LEN defines for NAT
01b,13nov00,ann  adding another #define NAT_BIND_EXISTS
01a,14aug00,ann  written
*/

/*
 * This file contains all the definitions and declerations for
 * the stub routines associated with the NAT MIB.
 */

#ifndef __INCnatmgmth
#define __INCnatmgmth

#ifdef __cplusplus
extern "C" {
#endif

#include "semLib.h"
#include "avlLib.h"

/* Defines for the scalar group */

#define NAT_NUM                        1
#define NAT_SW_VER                     2
#define NAT_SESSION_NUM                3

#define NAT_TBL_INDEX_LEN       1
#define NAT_BINDTBL_INDEX_LEN   10
#define NAT_SESSTBL_INDEX_LEN   10

#define NAT_DISPLAY_STR_SIZE          255     /* Max Length for display string */

/* 
 * Defines for the NAT Table group - Note that all the writeable objects
 * have maskable values
 */

#define NAT_SESS_MAX_IDLETIME          1
#define NAT_UDP_SESS_MAX_IDLETIME      2
#define NAT_INDEX                      3
#define NAT_ICMP_SESS_MAX_IDLETIME     4
#define NAT_PR_REALM_TYPE              5
#define NAT_EXT_REALM_TYPE             6
#define NAT_TYPE_MASK                  7
#define NAT_TCPUP_SESS_MAX_IDLETIME    8
#define NAT_TYPE                       9
#define NAT_TOTAL_BINDS                10
#define NAT_MAX_STATIC_BINDS           11
#define NAT_CURR_STATIC_BINDS          12
#define NAT_MAX_STATIC_TCP_BINDS       13
#define NAT_CURR_STATIC_TCP_BINDS      14
#define NAT_MAX_STATIC_UDP_BINDS       15
#define NAT_TCPDISC_SESS_MAX_IDLETIME  16
#define NAT_CURR_STATIC_UDP_BINDS      17
#define NAT_TCPCLOS_SESS_MAX_IDLETIME  32
#define NAT_TCPSYNC_SESS_MAX_IDLETIME  64
#define NAT_LOG_MASK                   128
#define NAT_ENABLE                     256
#define NAT_STATIC_BIND_ENABLE         512
#define NAT_DEF_X_ENABLE               1028
#define NAT_GLOBAL_ADDR_FILTER         2048

/* Defines for the bind table */

#define NAT_BIND_LOCADDR               1
#define NAT_BIND_LOCPORT               2
#define NAT_BIND_STATIC                3
#define NAT_BIND_REMADDR               4
#define NAT_BIND_KIND                  5
#define NAT_BIND_LEASE_LEFT            6
#define NAT_BIND_MAX_IDLETIME          7
#define NAT_BIND_REMPORT               8 
#define NAT_BIND_CURR_IDLETIME         9
#define NAT_BIND_DIR                   10
#define NAT_BIND_LOC_IFNUM             11
#define NAT_BIND_EXT_IFNUM             12
#define NAT_BIND_LOC_IFNAME            13
#define NAT_BIND_EXT_IFNAME            14
#define NAT_BIND_CONTR_AGENT           15
#define NAT_BIND_PROTO                 16
#define NAT_BIND_ACTION                32

/* Defines for the session table */

#define NAT_SESS_PROTO                 3
#define NAT_SESS_LOCADDR               5
#define NAT_SESS_LOCPORT               6
#define NAT_SESS_REMADDR               7
#define NAT_SESS_REMPORT               9
#define NAT_XSESS_LOCADDR              10
#define NAT_XSESS_LOCPORT              11
#define NAT_XSESS_REMADDR              12
#define NAT_XSESS_REMPORT              13
#define NAT_SESS_END                   14
#define NAT_SESS_IDLETIME_LEFT         15
#define NAT_SESS_PKT_MOD               17
#define NAT_SESS_DIR                   18
#define NAT_SESS_CONTR_AGENT           19

/* Modes of NAT */

#define NAT_BASIC                      1
#define NAT_NAPT                       2
#define NAT_BI_DIR                     4
#define NAT_TWICE                      8
#define NAT_RSA_IP                     16
#define NAT_RSAP_IP                    32

/* Defines for NAT debug objects */

#define NAT_PRINT_ENABLED              1
#define NAT_INIT_PRINT_ENABLED         2
#define NAT_DATA_PRINT_ENABLED         4
#define NAT_TRACE_PRINT_ENABLED        8
#define NAT_ERROR_PRINT_ENABLED        16

/* Other defines */

#define NUM_NAT_DEVICES                1  /* Number of NAT devices */
#define PR_REALM_TYPE                  "IPv4"
#define EXT_REALM_TYPE                 "IPv4"
#define NAT_TYPES_SUPPORTED            NAT_BASIC | NAT_NAPT 
#define GET_VALUE                      1
#define NEXT_VALUE                     2
#define TCP_TRANS_LIST                 1
#define UDP_TRANS_LIST                 2
#define ICMP_TRANS_LIST                3
#define IP_TRANS_LIST                  4
#define STATIC_ENTRY                   1
#define DYNAMIC_ENTRY                  2
#define ADDRESS_BINDING                1
#define TRANSPORT_BINDING              2
#define NAT_UNIDIRECTIONAL             1
#define NAT_BIDIRECTIONAL              2
#define NAT_NOMODIFIER                 1
#define NAT_IPMODIFIER                 2

#define NAT_BIND_ACTIVE                1
#define NAT_BIND_CREATE                4
#define NAT_BIND_DELETE                6

/* Defines for nat session end */

#define NAT_OTHER                      1
#define NAT_NO_HEURISTIC               2
#define NAT_END_IDLETIME               3
#define NAT_EXT_AGENT                  4


/* Structures */

#if ((CPU_FAMILY==I960) && (defined __GNUC__))
#pragma align 1    /* Tell gnu960 not to optimize alignments */
#endif    /* CPU_FAMILY==I960 */

typedef struct nat_scalars 
    {
    UINT16        natNumber;
    unsigned char natSwVersion[NAT_DISPLAY_STR_SIZE];
    UINT16        natSessionNumber;
    unsigned char natPrRealmType[NAT_DISPLAY_STR_SIZE];
    unsigned char natExtRealmType[NAT_DISPLAY_STR_SIZE];
    } NAT_SCALARS;
 
typedef struct nat_tbl_entry 
    {
    UINT16        natIndex;
    UINT16        natTypeMask;
    UINT16        natType;
    INT16         natTotalBinds;
    INT16         natMaxStaticBinds;
    INT16         natCurrStaticBinds;
    INT16         natCurrDynamicBinds;
    INT16         natMaxStaticIpBinds;
    INT16         natCurrStaticIpBinds;
    INT16         natCurrDynamicIpBinds;
    INT16         natMaxStaticTcpBinds;
    INT16         natCurrStaticTcpBinds;
    INT16         natCurrDynamicTcpBinds;
    INT16         natMaxStaticUdpBinds;
    INT16         natCurrStaticUdpBinds;
    INT16         natCurrDynamicUdpBinds;
    UINT32        natSessionMaxIdleTime;
    UINT32        natUdpSessionMaxIdleTime;
    UINT32        natIcmpSessionMaxIdleTime;
    UINT32        natTcpUpSessionMaxIdleTime;
    UINT32        natTcpDiscSessionMaxIdleTime;
    UINT32        natTcpClosingSessionMaxIdleTime;
    UINT32        natTcpSyncSessionMaxIdleTime;
    UINT16        natLogMask;
    BOOL          natEnable;
    BOOL          natDefXEnable;
    BOOL          natGlobalAddrFilter;
    BOOL          natStaticBindEnable;
    } NAT_TBL_ENTRY;
 
typedef struct nat_bindtbl_entry {
    BOOL          natBindStatic;
    UINT16        natBindType;
    unsigned long natBindLocalAddress;
    UINT16        natBindLocalPort;
    unsigned long natBindRemAddress;
    UINT16        natBindRemPort;
    unsigned long natBindMaxLeaseTime;
    unsigned long natBindLeaseLeft;
    unsigned long natBindMaxIdle;
    unsigned long natBindCurrIdle;
    UINT16        natBindDirection;
    int           natBindLocalIfNumber;
    int           natBindExtIfNumber;
    unsigned char natBindLocalIfName[NAT_DISPLAY_STR_SIZE];
    unsigned char natBindExtIfName[NAT_DISPLAY_STR_SIZE];
    UINT16        natBindProto;
    UINT16        natBindAction;
    UINT16        natBindContrAgent;
} NAT_BINDTBL_ENTRY;
 
typedef struct nat_sesstbl_entry {
    UINT16        natSessionProto;
    unsigned long natSessionLocalAddress;
    UINT16        natSessionLocalPort;
    unsigned long natSessionRemAddress;
    UINT16        natSessionRemPort;
    unsigned long natXSessionLocalAddress;
    UINT16        natXSessionLocalPort;
    unsigned long natXSessionRemAddress;
    UINT16        natXSessionRemPort;
    UINT16        natSessionEnd;
    unsigned long natSessionIdleTimeLeft;
    UINT16        natSessionContrAgent;
    UINT16        natSessionDirection;
    UINT16        natSessionPacketModifier;
} NAT_SESSTBL_ENTRY;

typedef struct nat_data {
    NAT_SCALARS   natScalars;
    SEM_ID        natMgmtSem;
} NAT_DATA;

typedef struct nat_bind_node {
    NAT_BINDTBL_ENTRY *    pNatBindTbl;
} NAT_BIND_NODE;


#if ((CPU_FAMILY==I960) && (defined __GNUC__))
#pragma align 0    /* Turn off the alignment restriction */
#endif    /* CPU_FAMILY==I960 */

/* Forward Declarations */

STATUS natMgmtInit (void);
STATUS m2NatScalarsGet (NAT_SCALARS *);
STATUS m2NatScalarsSet (NAT_SCALARS *, int);
STATUS m2NatTblEntryGet (NAT_TBL_ENTRY *, int);
STATUS m2NatTblEntrySet (NAT_TBL_ENTRY *, int);
STATUS m2NatBindTblEntryGet (NAT_BINDTBL_ENTRY *, int);
STATUS m2NatBindTblEntrySet (NAT_BINDTBL_ENTRY *, int);
STATUS m2NatSessTblEntryGet (NAT_SESSTBL_ENTRY *, int);
void   natRealmModify (NAT_SCALARS *, char *, char *);
void dumbInit (void);
STATUS m2NatTreeAdd (void);
STATUS natTcpStaticFind(u_long,u_short,u_short);
STATUS natUdpStaticFind(u_long,u_short,u_short);
int natTypeFind (void);

#ifdef __cplusplus
}
#endif

#endif    /* __INCnatmgmth */
