/* usrBasicRadiusBSPBuildCfg.h - header file for WindNet RADIUS demo
 * configuration through project and command-line build 
 */

/* Copyright 2005 Wind River Systems, Inc. */

#include "copyright_wrs.h"

/*
modification history
--------------------
20aug05,dps added new declaration RADIUS_STACK_VERSION for ipv6 support
08aug05,dps added ipv6_address in RADIUS_PROJ_CONFIG_SERVER_INFO
01b,03feb05,snd Changed copyright to 2005
01a,30dec04,snd  created, as fix for SPR 100161. This file is
                 header file for WindNet RADIUS demo configuration
                 through project and command-line build
*/

/*
DESCRIPTION
This file contains structure definitions, typedefs, forward declarations
required for WindNet RADIUS demo configuration through project
and command-line build.

\NOMANUAL
*/


#ifndef __INCusrBasicRadiusBSPBuildCfgh
#define __INCusrBasicRadiusBSPBuildCfgh

#ifdef __cplusplus
extern "C" {
#endif

/* includes */
/* #ifndef INCLUDE_INETLIB */
#define INCLUDE_INETLIB 

#include <vxWorks.h>
#include <ioLib.h>
#include <sysLib.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <taskLib.h>
#include <inetLib.h>
#include <socket.h>
#include <sockLib.h>
#include <inetLib.h>




/* RADIUS_PROJ_CLASS stores RADIUS debug flags, authentication port,
* accounting port, max recv packet size, set default values and
* set disable response authenticator flags.
* These variables are stored with values set by the user during
* project build or command-line build
*
*
* RADIUS_PROJ_CONFIG_SERVER_INFO[] stores settings of
* primary and secondary RADIUS servers configured
*/

typedef struct RADIUS_PROJ_CONFIG_SERVER_INFO /* used for storing config info */
    {
    ULONG ip_address;                /* IP address of RADIUS server */
    char  *str_ipv6_address;         /* IPV6 address of RADIUS server */
    char *secret;                    /* Shared secret of RADIUS servers */
    UINT max_retransmit_count;       /* Max no of retrans for RADIUS server */
    UINT retransmission_interval;    /* Timeout for Retransmission */
    UINT type;                       /* Type of server,
                                        0 - both, 1- accounting,
                                        2 - authentication
                                      */
    } RADIUS_PROJ_CONFIG_SERVER_INFO;

typedef struct RADIUS_PROJ_CLASS  /* RADIUS debug flags and other settings */
    {
    BOOL proj_configuration_enabled; 
    BOOL enabled;
    UINT authentication_client_port;
    UINT accounting_client_port;
    UINT maximum_rx_packet_size;
    BOOL set_default_values;
    BOOL printing_enabled;
    BOOL alarm_printing_enabled;
    BOOL initialization_printing_enabled;
    BOOL socket_printing_enabled;
    BOOL data_printing_enabled;
    BOOL trace_printing_enabled;
    BOOL disable_verify_response_authenticator;
    } RADIUS_PROJ_CLASS;

/* globals */

/* enum declaration for radius stack version */
typedef enum RADIUS_STACK_VERSION
{
    IPV4_ONLY = 1,
    IPV6_ONLY,
    IPV4_IPV6
}RADIUS_STACK_VERSION;

IMPORT RADIUS_PROJ_CLASS radius_config_data;
IMPORT RADIUS_PROJ_CONFIG_SERVER_INFO radius_config_primary_server;
IMPORT RADIUS_PROJ_CONFIG_SERVER_INFO radius_config_secondary_server;
IMPORT BOOL radius_config_primary_server_enabled;
IMPORT BOOL radius_config_secondary_server_enabled;
IMPORT RADIUS_STACK_VERSION radius_ip_version;

/* forward declarations */
void radius_populate_client_data (void);
void radius_populate_secondary_server_data (void);
void radius_populate_primary_server_data (void);
void radius_populate_ip_version (void);

#ifdef __cplusplus
}
#endif

#endif /* __INCusrBasicRadiusBSPBuildCfgh */

