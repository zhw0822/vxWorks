/************************************************************************/
/*	Copyright (C) 1999 RouterWare, Inc.     							*/
/*	Unpublished - rights reserved under the Copyright Laws of the		*/
/*	United States.  Use, duplication, or disclosure by the 				*/
/*	Government is subject to restrictions as set forth in 				*/
/*	subparagraph (c)(1)(ii) of the Rights in Technical Data and 		*/
/*	Computer Software clause at 252.227-7013.							*/
/*	RouterWare, Inc., 3961 MacArthur Blvd. Suite 212, Newport Beach, CA	*/
/************************************************************************/ 

/* pppauth_if.h */

/* C Header file, use 3 space tab stops */

#ifndef __PPPAUTH_IF_H__
#define __PPPAUTH_IF_H__

#include <rwos.h>		/* bool and UINT definitions */

typedef ULONG	PPPCTRL_SESSION_HANDLE;

struct PPPAUTH_CALLBACK_IF;	/* eliminate warnings */

typedef bool(*PPPAUTH_VERIFICATION_CALLBACK)(struct PPPAUTH_CALLBACK_IF*, PPPCTRL_SESSION_HANDLE, UINT id, bool result, 
															BYTE* p_message, UINT message_length);

typedef struct PPPAUTH_CALLBACK_IF
{
	bool (*fp_chap_get_challenge_complete) 				(struct PPPAUTH_CALLBACK_IF*, PPPCTRL_SESSION_HANDLE,
																			BYTE* p_challenge, UINT challenge_length, 
																			BYTE* p_name, UINT name_length);
	bool (*fp_chap_get_response_complete) 					(struct PPPAUTH_CALLBACK_IF*, PPPCTRL_SESSION_HANDLE, UINT id, 
																			BYTE* p_response, UINT response_length, 
																			BYTE* p_name, UINT name_length);
	bool (*fp_pap_get_user_name_password_complete)		(struct PPPAUTH_CALLBACK_IF*, PPPCTRL_SESSION_HANDLE, 
																			BYTE* p_user_name, UINT user_name_length, 
																			BYTE* p_password, UINT password_length);
	PPPAUTH_VERIFICATION_CALLBACK fp_chap_verification_complete;
	PPPAUTH_VERIFICATION_CALLBACK	fp_pap_verification_complete;

} PPPAUTH_CALLBACK_IF;

typedef struct PPPAUTH_IF
{
	bool (*fp_register)								(struct PPPAUTH_IF*, PPPAUTH_CALLBACK_IF*);
	bool (*fp_unregister)							(struct PPPAUTH_IF*);
	bool (*fp_chap_get_challenge) 				(struct PPPAUTH_IF*, PPPCTRL_SESSION_HANDLE);
	bool (*fp_chap_get_response) 					(struct PPPAUTH_IF*, PPPCTRL_SESSION_HANDLE, UINT id, 
																BYTE* p_challenge, UINT challenge_length, 
																BYTE* p_user_name, UINT user_name_length);
	bool (*fp_chap_verify_response) 				(struct PPPAUTH_IF*, PPPCTRL_SESSION_HANDLE, UINT id, 
																BYTE* p_challenge, UINT challenge_length, 
																BYTE* p_response, UINT response_length, 
																BYTE* p_name, UINT name_length);
	bool (*fp_pap_get_user_name_password) 		(struct PPPAUTH_IF*, PPPCTRL_SESSION_HANDLE);
	bool (*fp_pap_verify_user_name_password) 	(struct PPPAUTH_IF*, PPPCTRL_SESSION_HANDLE, UINT id, 
																BYTE* p_user_name, UINT name_length, 
																BYTE* p_password, UINT password_length);
} PPPAUTH_IF;

/* Temporary Kludge until Module Manager is done */
PPPAUTH_IF* pppauth_create (void);
bool pppauth_destroy (PPPAUTH_IF* pp_if);

#endif	/* Don't add anything after this line */