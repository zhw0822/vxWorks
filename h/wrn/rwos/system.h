/*
modification history
--------------------
01c,07may02,kc  Added TMS-specific modification - change RWOS_MAXIMUM_FILE_SIZE to 512.
01b,06mar02,kc  Fixed incorrect TRACE macro defines.
01a,09may02,ark Added in fix for SPR 70535	
*/
#if !defined (_SYSTEM_H_)
#define _SYSTEM_H_

/*#if !defined (__VxWORKS__)

#error VxWorks OS is not defined

#endif  
*/

/****************************************************************************/
/* System types															    */
/****************************************************************************/
typedef int RWOS_TASK_ARG_TYPE_HANDLE;

/****************************************************************************/
/* Global variables														    */
/****************************************************************************/
UINT rwos_task_reference_counter;


/****************************************************************************/
/* System constants														    */
/****************************************************************************/
#define RWOS_PRIORITY_SAFE_QUEUING_MODE 		SEM_Q_PRIORITY | SEM_INVERSION_SAFE | SEM_DELETE_SAFE
#define RWOS_FIFO_SAFE_QUEUING_MODE				SEM_Q_FIFO | SEM_DELETE_SAFE
#define RWOS_MUTEX_SEMAPHORE     				RWOS_PRIORITY_SAFE_QUEUING_MODE
#define VXWORKS_MAXIMUM_TASK_ARGS				0xA
#define RWOS_TASK_BASE_PRIORITY					0x64 			
#define RWOS_TASK_DEBUG_OPTIONS					VX_PRIVATE_ENV 
#define RWOS_TASK_NODEBUG_OPTIONS				VX_PRIVATE_ENV | VX_UNBREAKABLE
#define RWOS_TASK_STACK_SIZE	   				0x7D0		/*2K*/
#define RWOS_MAXIMUM_STRING_SIZE				0x32
#define RWOS_TASK_CLOCK_RATE					0x12
#define RWOS_RUNNING_TASK						0x1
#define RWOS_TASK_NAME							"tRWOS"
#define RWOS_TASK_DELAY 					   	0xA
#define RWOS_TASK_TIMEOUT						0x5				/*timeout in ticks */
#define RWOS_ACTIVE_TASK_ID						0x00
#define RWOS_RAMDRIVE_ADDRESS					0
#define RWOS_RAMDRIVE_BYTES_PER_BLOCK 			512
#define RWOS_RAMDRIVE_BLOCK_PER_TRACK			400
#define RWOS_RAMDRIVE_NUMBER_OF_BLOCKS			400
#define RWOS_RAMDRIVE_BLOCK_OFFSET				0
#define RWOS_RAMDRIVE_VOLUME_NAME				"DEV1:"
#define RWOS_CONFIGURATION_FILE_NAME			"config"
#ifdef INCLUDE_TMS_MODS
#define RWOS_MAXIMUM_FILE_SIZE					512
#else
#define RWOS_MAXIMUM_FILE_SIZE					4000
#endif /* INCLUDE_TMS_MODS */
#define MILLISECONDS 							1000

#if defined (__VxDEBUG__)
	#define RWOS_TASK_OPTIONS				RWOS_TASK_DEBUG_OPTIONS

	#define TRACE 							printf

	#define LOG								logMsg

#else
	#define RWOS_TASK_OPTIONS				RWOS_TASK_NODEBUG_OPTIONS

	#define TRACE

	#define LOG

#endif /*__VxDEBUG__*/


/****************************************************************************/
/* RwOS Task variables														*/	
/****************************************************************************/
void* p_rwos_intance_handle;

/****************************************************************************/
/* RwOS Package function													*/	
/****************************************************************************/
typedef void (*FP_RWOS_MODULE_STARTUP) (const char*p_configuration_file);


/****************************************************************************/
/* Package Template Entry function											*/
/*																			*/	
/****************************************************************************/
int rwos_xxxxx_package_entry_template (const char* p_configuratioin_file, const char* p_ftp_server_address, const char* p_directory, const char* p_user, const char* p_password);

void rwos_xxxxx_package_exit (void);

int rwos_ike_package_entry (const char* p_configuration_file, const char* p_ftp_server_address, const char* p_directory, const char* p_user, const char* p_password);
/****************************************************************************/
/* System functions															*/
/****************************************************************************/
int create_system_task (const char* p_task_name, void* p_task_entry_point, UINT task_priority, UINT task_stack_size /*=2k*/,...);

HOST_OS_MUTEX_HANDLE system_create_mutex_semaphore (const char *p_mutex_name);
bool system_destroy_mutex_semaphore (HOST_OS_MUTEX_HANDLE mutex_semaphore_handle);
bool system_acquire_mutex_semaphore (HOST_OS_MUTEX_HANDLE mutex_semaphore_handle, UINT timeout);
bool system_release_mutex_semaphore (HOST_OS_MUTEX_HANDLE mutex_semaphore_handle);
bool system_event_set (HOST_OS_EVENT_HANDLE event_handle);
bool system_event_reset (HOST_OS_EVENT_HANDLE event_handle);
HOST_OS_EVENT_HANDLE system_event_create (void);
void system_event_destroy (HOST_OS_EVENT_HANDLE event);


/****************************************************************************/
/* RwOS Entry point															*/
/****************************************************************************/
UINT rwos_main (FP_RWOS_MODULE_STARTUP  system_module_table[], const char* p_configuration_file_contents );
UINT rwos_task_main (FP_RWOS_MODULE_STARTUP  system_module_table[], const char* p_configuration_file_contents, char* p_task_name, UINT task_priority );

#endif /*_SYSTEM_H_*/

