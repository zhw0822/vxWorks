#if !defined (__RWBUFFERPOOL_H__)
#define __RWBUFFERPOOL_H__

typedef UINT RW_BUFFER_POOL_HANDLE;

RW_BUFFER_POOL_HANDLE rw_buffer_pool_create (UINT buffer_size, char* p_pool_memory_segment, UINT pool_memory_segment_size);

bool rw_buffer_pool_destroy (RW_BUFFER_POOL_HANDLE buffer_pool, char** p_pool_memory_segment, UINT* p_pool_memory_segment_size);

RW_PACKET_HANDLE rw_buffer_pool_get_rw_packet (RW_BUFFER_POOL_HANDLE buffer_pool, UINT reserved_header_size);

#endif /* __RWBUFFERPOOL_H__ */
