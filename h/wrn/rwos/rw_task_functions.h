/*
 * Modification History
 * --------------------
 */

#if !defined (_RW_TASK_FUNCTIONS_H_)
#define _RW_TASK_FUNCTIONS_H_

/****************************************************************************/
/* System functions															*/
/****************************************************************************/
int rw_create_system_task (const char* p_task_name, void* p_task_entry_point, UINT task_priority, UINT task_stack_size,UINT task_options,...);


/****************************************************************************/
/* RW Task OS Entry point													*/
/****************************************************************************/
UINT rw_task_main (FP_RWOS_MODULE_STARTUP  system_module_table[], 
                     const char* p_configuration_file_contents, char* p_task_name, 
                     UINT task_priority, UINT task_options );

#endif /*_RW_TASK_FUNCTIONS_H_*/
