/* Copyright 2001 Wind River Systems, Inc. */

/*
modification history
--------------------
01  Aug1301,aos		Added TID as a parameter to rwos_send_close_signal () function
*/

/************************************************************************/
/*	Copyright (C) 1998 RouterWare, Inc.	 								*/
/*	Unpublished - rights reserved under the Copyright Laws of the		*/
/*	United States.  Use, duplication, or disclosure by the 				*/
/*	Government is subject to restrictions as set forth in 				*/
/*	subparagraph (c)(1)(ii) of the Rights in Technical Data and 		*/
/*	Computer Software clause at 252.227-7013.							*/
/*	RouterWare, Inc., 3961 MacArthur Blvd. Suite 212, Newport Beach, CA	*/
/************************************************************************/
#if !defined (_RW_HOST_OS_INTERFACE_H_)
#define _RW_HOST_OS_INTERFACE_H_

typedef UINT RWOS_HANDLE;
typedef UINT HOST_OS_EVENT_HANDLE;
typedef UINT HOST_OS_MUTEX_HANDLE;

typedef bool(*FP_HOST_OS_EVENT_SET) (HOST_OS_EVENT_HANDLE); 
typedef bool(*FP_HOST_OS_EVENT_RESET) (HOST_OS_EVENT_HANDLE);
typedef HOST_OS_EVENT_HANDLE (*FP_HOST_OS_EVENT_CREATE) (void);
typedef void (*FP_HOST_OS_EVENT_DESTROY)(HOST_OS_EVENT_HANDLE);
typedef HOST_OS_MUTEX_HANDLE (*FP_HOST_OS_MUTEX_CREATE) (const char *p_mutex_name);	
typedef bool(*FP_HOST_OS_MUTEX_DESTROY) (HOST_OS_MUTEX_HANDLE);
typedef bool(*FP_HOST_OS_MUTEX_ACQUIRE) (HOST_OS_MUTEX_HANDLE, UINT max_wait_time);
typedef bool(*FP_HOST_OS_MUTEX_RELEASE) (HOST_OS_MUTEX_HANDLE);

/****************************************************************************/
typedef struct HOST_OS_CALLBACK_IF
{
	FP_HOST_OS_EVENT_SET fp_host_os_event_set;

	FP_HOST_OS_EVENT_RESET fp_host_os_event_reset;

	FP_HOST_OS_EVENT_CREATE fp_host_os_event_create;

	FP_HOST_OS_EVENT_DESTROY fp_host_os_event_destroy;

	FP_HOST_OS_MUTEX_CREATE fp_host_os_mutex_create;	

	FP_HOST_OS_MUTEX_DESTROY fp_host_os_mutex_destroy;

	FP_HOST_OS_MUTEX_ACQUIRE fp_host_os_mutex_acquire;

	FP_HOST_OS_MUTEX_RELEASE fp_host_os_mutex_release;

}HOST_OS_CALLBACK_IF;


/****************************************************************************/
/*	rwos interface															*/		
/****************************************************************************/
RW_EXPORT bool rwos_initialize (HOST_OS_CALLBACK_IF* p_host_os_call_back_interface, UINT clock_ticks_per_second, void** pp_rwos_handle);
RW_EXPORT void rwos_request_to_shutdown (void);
RW_EXPORT void rwos_exit (void);
RW_EXPORT bool rwos_scheduler_dispatch_active_objects (void);
RW_EXPORT UINT rwos_scheduler_dispatch_idle_objects (void);
RW_EXPORT bool rwos_process_driver_manager_events (void);
RW_EXPORT void rwos_dispatch_loop (void);
RW_EXPORT void rwos_send_close_signal (UINT TID);
RW_EXPORT bool rwos_wait_for_close_event (void);
	
/****************************************************************************/
/* Set methods for rwos event handles										*/
/****************************************************************************/
RW_EXPORT void rwos_set_event_handle (HOST_OS_EVENT_HANDLE event);
RW_EXPORT void rwos_set_close_event_handle (HOST_OS_EVENT_HANDLE close_event);
RW_EXPORT void rwos_set_driver_manager_event_handle (HOST_OS_EVENT_HANDLE driver_manager_event);


#endif /* _RW_HOST_OS_INTERFACE_H_ */
