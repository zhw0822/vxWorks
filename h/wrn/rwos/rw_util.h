/* Copyright 2001 Wind River Systems, Inc. */

/*
modification history
--------------------
02d,22aug05,djp  Removed _pack reference to eliminate compiler warning
02c,07sep04,djp  Merged nortel modifications
02b,13sep04,cdw  Added defines for INCLUDE_IPSEC_MEMORY_ROUTINES
02a,09may02,ark  Added in fix for SPR 70535	
01a,13aug01,aos  Redirecting all memory call to RWOS memory manager
*/


/************************************************************************/
/*	Copyright (C) 1999 RouterWare, Inc.     							*/
/*	Unpublished - rights reserved under the Copyright Laws of the		*/
/*	United States.  Use, duplication, or disclosure by the 				*/
/*	Government is subject to restrictions as set forth in 				*/
/*	subparagraph (c)(1)(ii) of the Rights in Technical Data and 		*/
/*	Computer Software clause at 252.227-7013.							*/
/*	RouterWare, Inc., 3961 MacArthur Blvd. Suite 212, Newport Beach, CA	*/
/************************************************************************/
#if !defined (__RW_UTIL_H__)
#define __RW_UTIL_H__
/* ************************************************************************************************************************ */
#include <stddef.h>

#if defined (INCLUDE_IPSEC_MEMORY_ROUTINES)
#include <wrn/ipsec/ipsec_memory_routines.h>
#endif

/* ************************************************************************************************************************ */
/*#include "rw_system_configuration.h"																						*/
/* ************************************************************************************************************************ */
#define MAXIMUM_NUMBER_OF_BYTES_IN_MAC_ADDRESS 16
#define ETHERNET_MAC_ADDRESS_LENGTH 6
#define NUMBER_OF_BITS_IN_BYTE 8

#define BROADCAST_IP_ADDRESS	0xffffffff
#define MULTICAST_IP_ADDRESS	0xf0000000
#define NULL_IP_ADDRESS ((IP_ADDRESS)(0))


/* Internet address class. Note the IP address is in host order. */
#define IN_CLASS_A(ip_address) (((ULONG) (ip_address) & 0x80000000L) == 0)
#define IN_CLASS_A_NET										0xff000000L
#define IN_CLASS_A_HOST										0x00ffffffL

#define IN_CLASS_B(ip_address) (((ULONG) (ip_address) & 0xc0000000L) == 0x80000000L)
#define IN_CLASS_B_NET										0xffff0000L
#define IN_CLASS_B_HOST	 									0x0000ffffL

#define IN_CLASS_C(ip_address) (((ULONG) (ip_address) & 0xe0000000L) == 0xc0000000L)
#define IN_CLASS_C_NET		  								0xffffff00L
#define IN_CLASS_C_HOST		  								0x000000ffL

#define IN_CLASS_D(ip_address) (((ULONG) (ip_address) & 0xf0000000L) == 0xe0000000L)

#if !defined (IN_MULTICAST_CLASS)
	#define IN_MULTICAST_CLASS(ip_address) IN_CLASS_D (ip_address)
#endif


#if 0  /* These conflict with coreip/in.h definitions! -rlm 2004-08-09 */
#if !defined (IN_EXPERIMENTAL)
	#define IN_EXPERIMENTAL(ip_address) (((ULONG) (ip_address) & 0xe0000000L) == 0xe0000000L)
#endif

#if !defined (IN_BADCLASS)
	#define IN_BADCLASS(ip_address)	(((ULONG) (ip_address) & 0xf0000000L) == 0xf0000000L)
#endif
#endif /* -rlm 2004-08-09 */


#if !defined (INTERNET_ADDRESS_ANY)	   
	#define INTERNET_ADDRESS_ANY	    						(ULONG) 0x00000000L
#endif

#if !defined (INTERNET_ADDRESS_LOOPBACK)
	#define INTERNET_ADDRESS_LOOPBACK	  					(ULONG) 0x7F000000L
#endif

#define	INTERNET_ADDRESS_LOOPBACK_MASK						(ULONG) 0x00FFFFFFL

#if !defined (INTERNET_ADDRESS_BROADCAST)
	#define INTERNET_ADDRESS_BROADCAST	  				(ULONG) 0xffffffffL
#endif


#define IN_LOOPBACK_ADDRESS(ip_address)	(((ULONG) (ip_address) | INTERNET_ADDRESS_LOOPBACK_MASK) == INTERNET_ADDRESS_LOOPBACK) 					
#define IN_BROADCAST(ip_address) (((ULONG)(ip_address) == INTERNET_ADDRESS_BROADCAST))

#define IN_LOOPBACK_NET					  							127



#define F_BROADCAST_IP_ADDRESS(ip_address) ((ip_address) == ((IP_ADDRESS) (0xffffffff)))
#define F_MULTICAST_IP_ADDRESS(ip_address) (((ip_address) & 0xf0000000) == 0xf0000000)
#define F_NULL_IP_ADDRESS(ip_address) ((ip_address) == ((IP_ADDRESS)(0)))


#define IP_VERSION_4 4
#define IP_WORD_SIZE 4
#define IP_PACKET_HEADER_MINIMUM_LENGTH 20
#define IPV6_PACKET_HEADER_MINIMUM_LENGTH 40
#define IP_PACKET_HEADER_MAXIMUM_LENGTH 60
#define IP_PACKET_MAXIMUM_SIZE 65535
#define IP_FRAGMENT_OFFSET_MASK 0x1fff
#define IP_DEFAULT_TIME_TO_LIVE 32
#define IP_BYTES_PER_FRAGMENT_BLOCK 8

#define LOOPBACK_IP_ADDRESS 0x7f000001
#define SUBNET_MASK_ALL_ONES 0xffffffff

#define IP_REASSEMBLY_DURATION 60000 /* in milliseconds == 60 seconds */

#define ICMP_ECHO_REQUEST 8
#define ICMP_ECHO_REPLY 0
#define ECHO_AGENT_MESSAGE_HEADER_SIZE 8

#if !defined (__VxWORKS__)
#if !defined (WAIT_FOREVER)
#define WAIT_FOREVER 0xffffffff
#endif 
#endif

#define PARAMETER_NOT_USED(parameter_one) (parameter_one = parameter_one)
/* ************************************************************************************************************************ */

#if defined (__WRN_DEFAULT_HEAP_MANAGER__)
#define buffer_malloc malloc
#define buffer_free free

#define table_malloc calloc
#define table_free free

#define message_malloc malloc
#define message_free free

#elif defined (INCLUDE_IPSEC_MEMORY_ROUTINES)
#define buffer_malloc ipsecMemoryAllocate
#define buffer_free(ptr) do { ipsecMemoryFree(ptr); ptr = NULL; } while(0)

#define table_malloc ipsecMemoryCalloc
#define table_free(ptr) do { ipsecMemoryFree(ptr); ptr = NULL; } while(0)

#define message_malloc ipsecMemoryAllocate
#define message_free(ptr) do { ipsecMemoryFree(ptr); ptr = NULL; } while(0)

#else

#define buffer_malloc rw_memory_allocate
#define buffer_free(ptr) do { rw_memory_free(ptr); ptr = NULL; } while(0)

#define table_malloc rw_memory_calloc
#define table_free(ptr) do { rw_memory_free(ptr); ptr = NULL; } while(0)

#define message_malloc rw_memory_allocate
#define message_free(ptr) do { rw_memory_free(ptr); ptr = NULL; } while(0)

#endif /*__WRN_DEFAULT_HEAP_MANAGER__*/

/* ************************************************************************************************************************ */
#define high_byte_of_dword(double_word)  (((double_word) >> 24) & 0x000000ff) /* High byte of word double_word.   */
#define high_low_byte_of_dword(double_word)  (((double_word) >> 16) & 0x000000ff) /* High byte of word double_word.   */
#define low_high_of_dword(double_word)  (((double_word) >> 8) & 0x000000ff) /* High byte of word double_word.   */
#define low_byte_of_dword(double_word)  ((double_word) & 0x000000ff) /* Low byte of word double_word.   */

#if !defined (_WRS_V2__)
/* ************************************************************************************************************************ */
#define high_byte_of_ulong(double_word)  (((double_word) >> 24) & 0x000000ff) /* High byte of word double_word.   */
#define high_low_byte_of_ulong(double_word)  (((double_word) >> 16) & 0x000000ff) /* High byte of word double_word.   */
#define low_high_of_ulong(double_word)  (((double_word) >> 8) & 0x000000ff) /* High byte of word double_word.   */
#define low_byte_of_ulong(double_word)  ((double_word) & 0x000000ff) /* Low byte of word double_word.   */
/* ************************************************************************************************************************ */
#define high_byte_of_ushort(ushort)  (((ushort)>>8)&0x00ff) /* High byte of ushort ushort.   */
#define low_byte_of_ushort(ushort)  ((ushort)&0x00ff) /* Low byte of ushort ushort.   */
#define low_ushort_of_ulong(double_ushort)  (WORD)(((double_ushort) >> 16) & 0x0000ffff) /* High byte of ushort double_ushort.   */
#define high_ushort_of_ulong(double_ushort)  (WORD)((double_ushort) & 0x0000ffff) /* Low byte of ushort double_ushort.   */
#define two_bytes_to_ushort(high_byte,low_byte) ((((high_byte)&0x00ff)<<8)|((low_byte)&0x00ff))
#endif
/* ************************************************************************************************************************ */

#if !defined BYTE
typedef unsigned char BYTE;
#endif

#if !defined BOOLEAN
typedef enum BOOLEAN
{
	 _FALSE = 0 		/* FALSE and TRUE are already defined here */
	,_TRUE = 1
} BOOLEAN;
#endif

#if !defined _struct
#define _struct struct 
#endif

#if !defined _union
#define _union union
#endif

#if !defined _enum
#define	_enum  enum		
#endif

#if !defined STRINGS_MATCH
#define	STRINGS_MATCH ((int) NULL)
#endif

#if !defined PSTR
typedef char* PSTR;
#endif

#if defined (GLOBAL_FILE)
	#define GLOBAL	
#else
	#define GLOBAL extern
#endif

#if defined (__RW_SYSTEM__)
	extern CONFIGURATION_TABLE rw_system_configuration_table;			
#endif

	
/* ************************************************************************************************************************ */
typedef UINT	DRIVER_MANAGER_INSTANCE;
typedef UINT	ROUTE_MANAGER_INSTANCE;
typedef UINT	LINK_DRIVER_INSTANCE;
typedef UINT	LINK_DRIVER_MANAGER_INSTANCE;
typedef UINT	ARP_INSTANCE;
typedef UINT	IP_INSTANCE;
typedef UINT 	IPSEC_INSTANCE;
typedef UINT	ECHO_AGENT_INSTANCE;
typedef UINT	DRIVER_MANAGER_HANDLE;
typedef UINT 	IP_INTERFACE_HANDLE;

typedef UINT	LINK_DRIVER_INTERFACE_HANDLE;
typedef UINT	ARP_USER_HANDLE;
typedef UINT	IP_IF_HANDLE;
typedef UINT	ARP_IF_HANDLE;
typedef UINT	INTERFACE_HANDLE;
typedef ULONG	IP_ADDRESS;

#define INVALID_HANDLE ((UINT)NULL)
/* ************************************************************************************************************************ */
typedef enum ETHERNET_PROTOCOL_TYPE
{
	IP_PROTOCOL = 0x0800,
	ARP_PROTOCOL = 0x0806
} ETHERNET_PROTOCOL_TYPE;

typedef enum ARP_RESULT
{
	ARP_FAIL = 0,
	ARP_SUCCESS = 1,
	ARP_PENDING = 2
} ARP_RESULT;

typedef enum ECHO_TEST
{
	ECHO_FAIL =0,
	ECHO_PASS = 1
} ECHO_TEST;


typedef enum  TEST 
{ 
	PASS = 1, 
	FAIL = 0
} TEST;

#if !defined (__BIG_ENUMS__)
	#define ULONG_ENUM(enum_name) enum enum_name 
	#define USHORT_ENUM(enum_name) enum enum_name 
	#define BYTE_ENUM(enum_name) enum enum_name 
#else
	#define ULONG_ENUM(enum_name) ULONG
	#define USHORT_ENUM(enum_name) USHORT
	#define BYTE_ENUM(enum_name) BYTE
#endif

/* ************************************************************************************************************************ */
UINT deserialize_byte (BYTE** ptr_to_bptr_location);
UINT deserialize_ushort (BYTE** ptr_to_usptr_location);
ULONG deserialize_ulong (BYTE** ptr_to_ulptr_location);
void deserialize_short_bit_field (UINT size, USHORT *usptr_start, ...);

void serialize_byte (UINT source, BYTE** ptr_to_bptr_destination);
void serialize_ushort (UINT source, BYTE** ptr_to_usptr_destination);
void serialize_ulong (ULONG source, BYTE** ptr_to_ulptr_destination);
enum TEST serialize_short_bit_field (UINT size, USHORT *usptr_destination, ...);

/* Misc prototypes */
ULONG host_to_net_long (ULONG host_order_long_value);
char *convert_ip_address_to_dot_format (char *cptr_array_to_store_dot_format_address, ULONG ip_address);

USHORT host_to_net_short (USHORT host_order_short_value);
USHORT net_to_host_short (USHORT net_order_short_value);

ULONG host_to_net_long (ULONG host_order_long_value);
ULONG net_to_host_long (ULONG net_order_long_value);

/* ************************************************************************************************************************ */
#endif /* __RW_UTIL_H__ */

