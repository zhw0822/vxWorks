/* nfs3dLib.h - NFS v3 Server Library Header */

/* Copyright 1984 - 2003 Wind River Systems, Inc. */

/* 
modification history
--------------------
01a,08aug03,smc  written
*/

#ifndef __INCnfs3dLibh
#define __INCnfs3dLibh

#ifdef __cplusplus
extern "C" {
#endif

#include "limits.h"
#include "xdr_nfs3.h"

#define NFS3_MAXPATHLEN            1024
#define NFS3_MAXNAMLEN             255
#define NFS3_DEFAULT_STACK_SIZE    14000
#define NFS3_DEFAULT_NFS_SERVERS   4
#define NFS3_DEFAULT_PRIORITY      55
#define NFS3_DEFAULT_FILE_SYSTEMS  10

/* NFS specific FS Info parameters */
#define NFS3_MAXBLKSIZE    8192
#define NFS3_PAGE_SIZE     1024
#define NFS3_FSF_DEFAULT   0x0018

#define NFS3_FSF_LINK        0x0001
#define NFS3_FSF_SYMLINK     0x0002
#define NFS3_FSFHOMOGENEOUS  0x0008
#define NFS3_FSF_CANSETTIME  0x0010

/* Max num of entries in the Queue */
#define QLEN_PER_SRVR           10

/* #define OUTPUT_FMT              "%15s      %8d\n" */

/* 
 * NFS3_ACCESS_WRITE is defined as an OR of
 * MODIFY | EXTEND | DELETE accesses.
 * 0x0004 | 0x0008 | 0x0010
 */

#define NFS3_ACCESS_WRITE  0x001C
#define NFS3_ACCESS_RONLY  0x0023
#define NFS3_ACCESS_RW     0x003F

#define NFS3_DOS_WR_ACCESS     0x092
#define NFS3_DOS_RW            0x01FF
                           
/* Macros to access the reply structure members of NFS v3 procedures */
#define FILE_HANDLE_LEN      data.data_len
#define FILE_HANDLE          data.data_val

#define POST_OP_HANDLE_LEN   post_op_fh3_u.handle.FILE_HANDLE_LEN
#define POST_OP_HANDLE       post_op_fh3_u.handle.FILE_HANDLE

#define ATTR_FOL         attributes_follow

#define BEFORE_ATTR_FOL  before.ATTR_FOL
#define AFTER_ATTR_FOL   after.ATTR_FOL

#define OBJ_ATTR_FOLLOW        obj_attributes.attributes_follow
#define SYMLINK_ATTR_FOLLOW    symlink_attributes.attributes_follow
#define FILE_ATTR_FOLLOW       file_attributes.attributes_follow
#define DIR_ATTR_FOLLOW        dir_attributes.attributes_follow

/* File Handle Information as per DOS FS on VxWorks */

typedef struct
    {
    int    volumeId;        /* Volume identifier of the File System */
    ULONG  fsId;            /* Inode of the exported directory */
    ULONG  inode;           /* Inode of the file as returned from stat */
    INT8   reserved[52];    /* NFS v3 File Handle has 64 bytes */
    } NFS3_FILE_HANDLE;

/* NFS v3 Procedure call statistics */

typedef struct
    {
    int nullCalls;          /* Number of calls to NFSPROC_NULL */
    int getattrCalls;       /* Number of calls to NFSPROC_GETATTR */
    int setattrCalls;       /* Number of calls to NFSPROC_SETATTR */
    int lookupCalls;        /* Number of calls to NFSPROC_LOOKUP */ 
    int accessCalls;        /* Number of calls to NFSPROC_ACCESS */
    int readlinkCalls;      /* Number of calls to NFSPROC_READLINK */
    int readCalls;          /* Number of calls to NFSPROC_READ */
    int writeCalls;         /* Number of calls to NFSPROC_WRITE */
    int createCalls;        /* Number of calls to NFSPROC_CREATE */
    int mkdirCalls;         /* Number of calls to NFSPROC_MKDIR */
    int symlinkCalls;       /* Number of calls to NFSPROC_SYMLINK */
    int mknodCalls;         /* Number of calls to NFSPROC_MKNOD */
    int removeCalls;        /* Number of calls to NFSPROC_REMOVE */
    int rmdirCalls;         /* Number of calls to NFSPROC_RMDIR */
    int renameCalls;        /* Number of calls to NFSPROC_RENAME */
    int linkCalls;          /* Number of calls to NFSPROC_LINK */
    int readdirCalls;       /* Number of calls to NFSPROC_READDIR */
    int readdirplusCalls;   /* Number of calls to NFSPROC_READDIRPLUS */
    int fsstatCalls;        /* Number of calls to NFSPROC_FSSTAT */
    int fsinfoCalls;        /* Number of calls to NFSPROC_FSINFO */
    int pathconfCalls;      /* Number of calls to NFSPROC_PATHCONF */
    int commitCalls;        /* Number of calls to NFSPROC_COMMIT */
    } NFS3_SERVER_STATUS;   

/* 
 * Union of Expected Argument for NFS v3 procedure 
 * call (wherever applicable).
 * Description against each argument not given as the member
 * name is descriptive enough
 * The definitions of types are available in "xdr_nfs3.h" file
 */

typedef union
    {
    GETATTR3args     nfsproc_getattr_3_arg;
    SETATTR3args     nfsproc_setattr_3_arg;
    LOOKUP3args      nfsproc_lookup_3_arg;
    ACCESS3args      nfsproc_access_3_arg;
    READLINK3args    nfsproc_readlink_3_arg;
    READ3args        nfsproc_read_3_arg;
    WRITE3args       nfsproc_write_3_arg;
    CREATE3args      nfsproc_create_3_arg;
    MKDIR3args       nfsproc_mkdir_3_arg;
    SYMLINK3args     nfsproc_symlink_3_arg;
    MKNOD3args       nfsproc_mknod_3_arg;
    REMOVE3args      nfsproc_remove_3_arg;
    RMDIR3args       nfsproc_rmdir_3_arg;
    RENAME3args      nfsproc_rename_3_arg;
    LINK3args        nfsproc_link_3_arg;
    READDIR3args     nfsproc_readdir_3_arg;
    READDIRPLUS3args nfsproc_readdirplus_3_arg;
    FSSTAT3args      nfsproc_fsstat_3_arg;
    FSINFO3args      nfsproc_fsinfo_3_arg;
    PATHCONF3args    nfsproc_pathconf_3_arg;
    COMMIT3args      nfsproc_commit_3_arg;
    } NFS3D_ARGUMENT;


#ifdef __cplusplus
}
#endif

#endif /* __INCnfs3dLibh */
