/* nfs3Drv.h - nfs3Drv header */

/* Copyright 1984 - 2004 Wind River Systems, Inc. */

/*
modification history
--------------------
01b,05nov04,vvv  added nfsDriver.h (SPR #103655)
01a,10oct03,snd  written                                 
*/

#ifndef __INCnfs3Drvh
#define __INCnfs3Drvh

#ifdef __cplusplus
extern "C" {
#endif

#include "vwModNum.h"
#include "limits.h"
#include "hostLib.h"
#include "xdr_nfs3.h"
#include "nfsDriver.h"

/* function declarations */

#if defined(__STDC__) || defined(__cplusplus)

extern STATUS  nfs3Drv (void);
extern int     nfs3DrvNumGet (void);
extern STATUS  nfs3Mount (char *host, char *fileSystem, char *localName);
extern STATUS  nfs3MountAll (char *host, char *clientName, BOOL quiet);
extern STATUS  nfs3UnmountAll (char *host);
extern STATUS  nfs3DevInfoGet (unsigned long nfsDevHandle,NFS_DEV_INFO * info);

#else

extern STATUS  nfs3Drv ();
extern int     nfs3DrvNumGet ();
extern STATUS  nfs3Mount ();
extern STATUS  nfs3MountAll ();
extern STATUS  nfs3UnmountAll ();
extern NFS3_DEV * nfsDevFind ();
extern STATUS   nfs3DevInfoGet ();

#endif    /* __STDC__ */


#ifdef __cplusplus
}
#endif

#endif /* __INCnfs3Drvh */
