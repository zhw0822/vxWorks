/* netLib.h - header file for the tNetTask */

/* Copyright 2001 - 2003, 2005 Wind River Systems, Inc. */

/*
modification history
--------------------
01o,25aug05,dlk  Added section tags.
01n,08jul05,dlk  Implement netJobAdd() using jobQueueLib.
01m,25nov03,wap  Merge in changes for Snowflake
01l,20nov03,niq  Remove copyright_wrs.h file inclusion
01k,05nov03,cdw  Removal of unnecessary _KERNEL guards.
01j,04nov03,rlm  Ran batch header path update for header re-org.
01i,03nov03,rlm  Removed wrn/coreip/ prefix from #includes for header re-org.
01h,15aug03,nee  Merging from ACCORDION_BASE6_MERGE_BASELINE
01g,10jun03,vvv  include netVersion.h
01f,13may03,vvv  deleted TCP_MSL_CFG to avoid duplicate definitions
01e,01may03,spm  Tornado 2.2 CP1 merge (from ver 01p,22jan03,wap:
                 TOR2_2-CP1-F label, tor2_2-patch branch, /wind/river VOB)
01d,30dec02,ann  changing the value of IP_PROTO_NUM_MAX to 16
01c,26feb02,ham  renamed to netLib.h
01b,23aug01,ann  correcting the references to netlibh after the rename
01a,15aug01,ann  ported to clarinet from AE1.1 version 01o.
*/

#ifndef __INCnetLibh
#define __INCnetLibh

#ifdef __cplusplus
extern "C" {
#endif

#include <netVersion.h>

#include <jobQueueLib.h>

#define NET_TASK_QJOB_PRI	16

#ifndef HIDE_NET_JOB_INFO
extern struct _JOB_QUEUE netJobInfo;  /* not really, but it starts with one */
#endif

/* For convenience only. Using &netJobInfo avoids a dereference. */

extern JOB_QUEUE_ID netJobQueueId;

#if ((CPU_FAMILY==I960) && (defined __GNUC__))
#pragma align 1                 /* tell gcc960 not to optimize alignments */
#endif  /* CPU_FAMILY==I960 */

/* ip configuration parameters */

typedef struct ipcfgparams
    {
    int         ipCfgFlags;             /* ip configuration flags */
    int         ipDefTtl;               /* ip packet default time to live */
    int         ipIntrQueueLen;         /* ip interrupt queue length */
    int         ipFragTtl;              /* ip fragment time to live */
    } IP_CFG_PARAMS;

/* icmp configuration parameters */

typedef struct icmpcfgparams
    {
    int         icmpCfgFlags;           /* icmp configuration flags */
    } ICMP_CFG_PARAMS;

/* udp configuration parameters */

typedef struct udpcfgparams
    {
    int         udpCfgFlags;            /* udp configuration flags */
    int         udpSndSpace;            /* udp send space */
    int         udpRcvSpace;            /* udp receive space */
    } UDP_CFG_PARAMS;

/* tcp configuration parameters */

typedef struct tcpcfgparams
    {
    int         tcpCfgFlags;            /* tcp configuration flags */
    int         tcpSndSpace;            /* tcp send space */
    int         tcpRcvSpace;            /* tcp receive space */
    int         tcpConnectTime;         /* tcp connect time out */
    int         tcpReTxThresh;          /* tcp no. dup ACK to trig fast reTx */
    int         tcpMssDflt;             /* tcp default maximum segment size */
    int         tcpRttDflt;             /* tcp default RTT if no data */
    int         tcpKeepIdle;            /* tcp idle time before first probe */
    int         tcpKeepCnt;             /* tcp max probes before dropping */
    FUNCPTR	pTcpRandFunc;		/* tcp random function for tcp_init */
    UINT32      tcpMsl;                 /* tcp max segment lifetime */
    } TCP_CFG_PARAMS;

#if ((CPU_FAMILY==I960) && (defined __GNUC__))
#pragma align 0                 /* turn off alignment requirement */
#endif  /* CPU_FAMILY==I960 */

#define IP_PROTO_NUM_MAX        16      /* maximum size of protocol switch */
#define IP6_PROTO_NUM_MAX        16     /* maximum size of protocol switch */

/* ip configuration defines */
#define IP_NO_FORWARDING        0x000000000     /* no ip forwarding of pkts */
#define IP_DO_FORWARDING        0x000000001     /* do ip forwarding of pkts */
#define IP_DO_REDIRECT          0x000000002     /* do ip redirecting of pkts */
#define IP_DO_LARGE_BCAST       0x000000004     /* do broadcasting pkt > MTU */
#define IP_DO_CHECKSUM_SND      0x000000008     /* calculate ip checksum */
#define IP_DO_CHECKSUM_RCV      0x000000010
#define IP_DO_ARP_ON_FLAGCHG	0x000000020	/* send gratuitous ARP whenever
                                                 * interface status changes
                                                 * (up/down) instead of just
                                                 * when address is configured.
                                                 */
#define IP_DO_IF_STATUS_CHECK   0x000000040     /* check status of interface */
						/* associated with dest addr */
						/* for incoming packet       */

/* default ip configurations */
#ifndef IP_FLAGS_DFLT
#define IP_FLAGS_DFLT           (IP_DO_FORWARDING | IP_DO_REDIRECT | \
                                 IP_DO_CHECKSUM_SND | IP_DO_CHECKSUM_RCV)
#endif  /* IP_FLAGS_DFLT */

#ifndef IP_TTL_DFLT
#define IP_TTL_DFLT             64              /* default ip queue length */
#endif  /* IP_TTL_DFLT */

#ifndef IP_QLEN_DFLT
#define IP_QLEN_DFLT            50
#endif  /* IP_QLEN_DFLT */

#ifndef IP_FRAG_TTL_DFLT
#define IP_FRAG_TTL_DFLT        60              /* 60 slow time outs, 30secs */
#endif  /* IP_FRAG_TTL_DFLT */

/* udp configuration defines */
#define UDP_DO_NO_CKSUM         0x00000000
#define UDP_DO_CKSUM_SND        0x00000001      /* do check sum on sends */
#define UDP_DO_CKSUM_RCV        0x00000002      /* do check sum on recvs */

/* default udp configurations */

#ifndef UDP_FLAGS_DFLT
#define UDP_FLAGS_DFLT          (UDP_DO_CKSUM_SND | UDP_DO_CKSUM_RCV)
#endif  /* UDP_FLAGS_DFLT */

#ifndef UDP_SND_SIZE_DFLT
#define UDP_SND_SIZE_DFLT       9216            /* default send buffer size */
#endif  /* UDP_SND_SIZE_DFLT */

#ifndef UDP_RCV_SIZE_DFLT
#define UDP_RCV_SIZE_DFLT       41600           /* default recv buffer size */
#endif  /* UDP_RCV_SIZE_DFLT */

/* tcp configuration defines */

#define TCP_NO_RFC1323          0x00000000      /* tcp no RFC 1323 support */
#define TCP_DO_RFC1323          0x00000001      /* tcp RFC 1323 support*/

/* default tcp configurations */

#ifndef TCP_FLAGS_DFLT
#define TCP_FLAGS_DFLT          (TCP_DO_RFC1323)
#endif  /* TCP_FLAGS_DFLT */

#ifndef TCP_SND_SIZE_DFLT
#define TCP_SND_SIZE_DFLT       8192            /* default send buffer size */
#endif  /* TCP_SND_SIZE_DFLT */

#ifndef TCP_RCV_SIZE_DFLT
#define TCP_RCV_SIZE_DFLT       8192            /* default recv buffer size */
#endif  /* TCP_RCV_SIZE_DFLT */

#ifndef TCP_CON_TIMEO_DFLT
#define TCP_CON_TIMEO_DFLT      150             /* 75 secs */
#endif  /* TCP_CON_TIMEO_DFLT */

#ifndef TCP_REXMT_THLD_DFLT
#define TCP_REXMT_THLD_DFLT     3               /* 3 times */
#endif  /* TCP_REXMT_THLD_DFLT */

#ifndef TCP_MSS_DFLT
#define TCP_MSS_DFLT            512             /* default max segment size */
#endif  /* TCP_MSS_DFLT */

#ifndef TCP_RND_TRIP_DFLT
#define TCP_RND_TRIP_DFLT       3               /* 3 secs if no data avail */
#endif  /* TCP_RND_TRIP_DFLT */

#ifndef TCP_IDLE_TIMEO_DFLT
#define TCP_IDLE_TIMEO_DFLT     14400           /* idle timeouts, 2 hrs */
#endif  /* TCP_IDLE_TIMEO_DFLT */

#ifndef TCP_MAX_PROBE_DFLT
#define TCP_MAX_PROBE_DFLT      8               /* probes before dropping */
#endif  /* TCP_MAX_PROBE_DFLT */

#ifndef TCP_RAND_FUNC
#define TCP_RAND_FUNC           (FUNCPTR)random /* TCP random function */
#endif  /* TCP_RAND_FUNC */

/* icmp configuration defines */
#define ICMP_NO_MASK_REPLY      0x000000000     /* no mask reply support */
#define ICMP_DO_MASK_REPLY      0x000000001     /* do icmp mask reply */

/* default icmp configurations */

#ifndef ICMP_FLAGS_DFLT
#define ICMP_FLAGS_DFLT         (ICMP_NO_MASK_REPLY)
#endif  /* ICMP_FLAGS_DFLT */

extern void * netGtfTickInformation; /* read only */

/* function declarations */

#if defined(__STDC__) || defined(__cplusplus)

extern STATUS   netJobAdd (FUNCPTR routine, int param1, int param2, int param3,
                           int param4, int param5);
extern STATUS   netLibInit (void);
extern void     netErrnoSet (int status);
extern void     netTask (void);
extern STATUS   ipLibInit (IP_CFG_PARAMS * ipCfg);
extern STATUS   rawIpLibInit (void);
extern STATUS   rawLibInit (void);
extern STATUS   udpLibInit (UDP_CFG_PARAMS * udpCfg);
extern STATUS   tcpLibInit (TCP_CFG_PARAMS * tcpCfg);
extern STATUS   icmpLibInit (ICMP_CFG_PARAMS * icmpCfg);
extern STATUS   igmpLibInit (void);
extern void     ipFilterLibInit (void);
extern STATUS   routeSockLibInit (void);
_WRS_INITTEXT
extern void     splSemInit (void);
extern void     mbinit (void * pNetPoolCfg);
extern void     domaininit (void *);
extern STATUS	netJobAlloc (u_int numJobs);
_WRS_FASTTEXT
extern void	netGtfTickFunc (void * pTm, void * arg);

#else   /* __STDC__ */

extern STATUS   netJobAdd ();
extern STATUS   netLibInit ();
extern void     netErrnoSet ();
extern void     netTask ();
extern STATUS   ipLibInit ();
extern STATUS   rawIpLibInit ();
extern STATUS   rawLibInit ();
extern STATUS   udpLibInit ();
extern STATUS   tcpLibInit ();
extern STATUS   icmpLibInit ();
extern STATUS   igmpLibInit ();
extern void     ipFilterLibInit ();
extern STATUS   routeSockLibInit ();
_WRS_INITTEXT
extern void     splSemInit ();
extern void     mbinit ();
extern void     domaininit ();
extern STATUS   netJobAlloc ();
_WRS_FASTTEXT
extern void     netGtfTickFunc ();

#endif  /* __STDC__ */

#define netJobAdd(f, a1, a2, a3, a4, a5) \
    (jobQueueStdPost (&netJobInfo, NET_TASK_QJOB_PRI,		    \
		      (VOIDFUNCPTR)(f), (void *)(a1), (void *)(a2), \
		      (void *)(a3), (void *)(a4), (void *)(a5)))

#ifdef __cplusplus
}
#endif

#endif /* __INCnetLibh */

