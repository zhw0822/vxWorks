/* mip6_command.h - MIP6 user space program command header file */
/*             kept around to see whether to be reused for show routines */

/* Copyright 2005 Wind River Systems, Inc. */

/*	$KAME: command.h,v 1.1 2004/12/09 02:18:31 t-momose Exp $	*/

/*
 * Copyright (C) 2004 WIDE Project.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the project nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE PROJECT AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE PROJECT OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/*
modification history
--------------------
01f,01spe05,tlu  rename mip6 file name
01e,19aug05,xli  change the show routine names
01d,27jul05,xli  modified command show routine prototypes
01c,22jun05,tlu  move in the show routines and debugging routines
01b,14jun05,tlu  add _cplusplus condition
01a,22feb05,tlu  1st round of porting Kame MIP6
*/


#if !defined(_COMMAND_H_)
#define _COMMAND_H_

#ifdef __cplusplus
extern "C" {
#endif

/*
struct command_table {
	char *command;
	void (*cmdfunc)(int, char *);
	char *helpmsg;
};

int command_init(char *, struct command_table *, size_t, u_short);
*/

/* Show routines */

void mip6CommandInit();
void mip6BindingCacheShow();
void mip6KernelBindingCacheShow();

void mip6BindinfUpdateListShow();
void mip6KernelBindinfUpdateListShow();

void mip6StatisticsShow();

void mip6StatusShow(char *);

void imip6NoroShow();


/* Debugging routines */
void mip6DataEntryFlush(char *);


#ifdef __cplusplus
}
#endif

#endif /* _COMMAND_H_ */
