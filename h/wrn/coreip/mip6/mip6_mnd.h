/* mip6_mnd.h - MIP6 mobile node header file */

/* Copyright 2005 Wind River Systems, Inc. */

/*      $KAME: mnd.h,v 1.1 2004/12/09 02:18:38 t-momose Exp $  */
/*
 * Copyright (C) 2004 WIDE Project.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the project nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE PROJECT AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE PROJECT OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/*
modification history
--------------------
01l,28sep05,xli  change parameter "mip6options" to "mip6CfgString"`
01k,01sep05,tlu  rename mip6 file name
01j,19aug05,tlu  remove Kame defined global data structures
01i,09aug05,tlu  add semaphore mn_data_mutex to protect MN's data structures
01h,08aug05,tlu  modify the private APIs
01g,03jul05,xli  remove mnInitializeDaemon
01f,21jul05,tlu  add debug to configuration data
01e,19jul05,xli  move mip6CfgMnDaemon() to mip6.h
01d,22jun05,tlu  move out public APIs 
01c,14jun05,tlu  add _cplusplus condition
01b,31may05,tlu  rename constants.h
01a,19feb05,tlu  1st round of porting Kame MIP6
*/


#if !defined (_MND_H_) 
#define _MND_H_

#ifdef __cplusplus
extern "C" {
#endif


#include <mip6/mip6_constants.h>
#include <mip6/mip6.h>


/* MN meta structure */
typedef struct MN_CLASS
{
    /* Configuration data */
    char homeagent[V6ADDRPRESZ];
    char hoa_ifname[IFNAMESZ];
    int debug;


    /* semaphore used to protect global data structures */
    SEM_ID  mn_data_mutex;


    /* tasks */
    TASK_DESCRIPTOR mn_tasks[2];  /* MN tasks */

} MN_CLASS;


/* Private APIs */
STATUS mn_create_daemon_task();
STATUS mn_daemon_task_entry_ptr();
STATUS mn_initialize_env();
STATUS mn_initialize_default();
STATUS mip6CfgMnDaemon(char* mip6CfgString);



#ifdef __cplusplus
}
#endif

#endif /* _MND_H_ */
