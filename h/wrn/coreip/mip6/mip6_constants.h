/* constants.h - MIP6 home agent routines */

/* Copyright 2005 Wind River Systems, Inc. */


/*
modification history
--------------------
01c,04jul05,tlu  increase stack size
01b,14jun05,tlu  add _cplusplus condition
01a,02mar05,tlu  initial creation 
*/


#if !defined (_CONSTANTS_H_) 
#define _CONSTANTS_H_


#ifdef __cplusplus
extern "C" {
#endif


#define MN_TASK_DAEMON  0
#define MN_TASK_MD  1
#define HA_TASK_DAEMON  0

#define MN_DAEMON_TASK_PRIORITY  100
#define MN_DAEMON_TASK_STACK_SIZE  (50 * 1024)

#define MN_MD_TASK_PRIORITY  100
#define MN_MD_TASK_STACK_SIZE  (50 * 1024)

#define HA_DAEMON_TASK_PRIORITY  100
#define HA_DAEMON_TASK_STACK_SIZE  (50 * 1024)

#define V6ADDRPRESZ	39
#define IFNAMESZ	10


typedef enum
{
  MIP6_TASK_STATE_CREATED = 1,
  MIP6_TASK_STATE_INIT_COMPLETE
} MIP6_TASK_STATES;


#ifdef __cplusplus
}
#endif

#endif /* _CONSTANTS_H_ */
