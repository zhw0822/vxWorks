/* sockLib.h -  UNIX BSD 4.3 compatible socket library header */

/*
* Copyright (c) 1984-2005 Wind River Systems, Inc.
*
* The right to copy, distribute or otherwise make use of this software
* may be licensed only pursuant to the terms of an applicable Wind River
* license agreement.
*/

/*
modification history
--------------------
03n,17aug05,mcm  Using sys/time.h for RTP space.
03m,14sep04,dlk  Omit kernel APIs when _WRS_KERNEL is not defined.
03n,28may04,dlk  Make all socket back-ends share a single IO system device.
		 Remove fdMax parameter to sockLibInit().
03m,24mar04,mcm  Including time.h instead of sys/time.h.
03m,28may04,niq  Merging from base6 label POST_ITER5_FRZ16_REBASE (ver
                 /main/vdt/base6_itn5-int/1)
03l,18nov03,nee  adding <sys/time.h> for RTPs
03k,05nov03,cdw  Removal of unnecessary _KERNEL guards.
03j,04nov03,rlm  Ran batch header path update for header re-org.
03i,03nov03,rlm  Removed wrn/coreip/ prefix from #includes for header re-org.
03h,27oct03,cdw  update include statements post header re-org.
03g,24oct03,cdw  update include statements post header re-org.
03f,10jun03,vvv  include netVersion.h
03e,27feb02,ann  correcting the path to sockFunc.h after relocation
03d,02feb02,ham  changed for tornado build.
03c,30oct01,ppp  merging from munich
03b,28aug01,ann  correcting the return value in the prototype for sockLibInit
03a,15aug01,ann  ported to clarinet from AE1.1 version 02l.
*/

#ifndef __INCsockLibh
#define __INCsockLibh

#ifdef __cplusplus
extern "C" {
#endif

#ifdef _WRS_KERNEL    
#include <sys/times.h>
#else
#include <sys/time.h>    
#endif
#include <sys/socket.h>
#include <netVersion.h>

/* typedefs */

#ifdef _WRS_KERNEL
#include <sockFunc.h>
typedef struct sockLibMap
    {
    int			domainMap;	/* mapping address family  */
    int			domainReal;	/* real address family     */
    SOCK_FUNC *		pSockFunc;	/* socket function table   */
    struct sockLibMap *	pNext;		/* next socket lib mapping */
    } SOCK_LIB_MAP;
#endif /* _WRS_KERNEL */

/* function declarations */

#if defined(__STDC__) || defined(__cplusplus)

#ifdef _WRS_KERNEL
extern STATUS   sockLibInit (void);
extern STATUS 	sockLibAdd (FUNCPTR sockLibInitRtn, int domainMap,
                            int domainReal);
#endif /* _WRS_KERNEL */

extern STATUS 	bind (int s, struct sockaddr *name, int namelen);
extern STATUS 	connect (int s, struct sockaddr *name, int namelen);
extern STATUS 	connectWithTimeout (int sock, struct sockaddr *adrs,
				    int adrsLen, struct timeval *timeVal);
extern STATUS 	getpeername (int s, struct sockaddr *name, int *namelen);
extern STATUS 	getsockname (int s, struct sockaddr *name, int *namelen);
extern STATUS 	getsockopt (int s, int level, int optname, char *optval,
			    int *optlen);
extern STATUS 	listen (int s, int backlog);
extern STATUS 	setsockopt (int s, int level, int optname, char *optval,
			    int optlen);
extern STATUS 	shutdown (int s, int how);
extern int 	accept (int s, struct sockaddr *addr, int *addrlen);
extern int 	recv (int s, char *buf, int bufLen, int flags);
extern int 	recvfrom (int s, char *buf, int bufLen, int flags,
			  struct sockaddr *from, int *pFromLen);
extern int 	recvmsg (int sd, struct msghdr *mp, int flags);
extern int 	send (int s, const char *buf, int bufLen, int flags);
extern int 	sendmsg (int sd, struct msghdr *mp, int flags);
extern int 	sendto (int s, caddr_t buf, int bufLen, int flags,
			struct sockaddr *to, int tolen);
extern int 	socket (int domain, int type, int protocol);

#else	/* __STDC__ */

#ifdef _WRS_KERNEL
extern STATUS 	sockLibInit ();
extern STATUS 	sockLibAdd ();
#endif

extern STATUS 	bind ();
extern STATUS 	connect ();
extern STATUS 	connectWithTimeout ();
extern STATUS 	getpeername ();
extern STATUS 	getsockname ();
extern STATUS 	getsockopt ();
extern STATUS 	listen ();
extern STATUS 	setsockopt ();
extern STATUS 	shutdown ();
extern int 	accept ();
extern int 	recv ();
extern int 	recvfrom ();
extern int 	recvmsg ();
extern int 	send ();
extern int 	sendmsg ();
extern int 	sendto ();
extern int 	socket ();

#endif	/* __STDC__ */

#ifdef __cplusplus
}
#endif

#endif /* __INCsockLibh */
