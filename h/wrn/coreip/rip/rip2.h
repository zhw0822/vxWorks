/* rip2.h - Defines specifically for RIP version 2 */

/* Copyright 1984 - 2003 Wind River Systems, Inc. */

/*
modification history
--------------------
01j,05nov03,cdw  Removal of unnecessary _KERNEL guards.
01i,04nov03,rlm  Ran batch header path update for header re-org.
01h,03nov03,rlm  Removed wrn/coreip/ prefix from #includes for header re-org.
01g,10jun03,vvv  include netVersion.h
01f,12sep02,hsh  add c++ protection
01e,29mar01,spm  merged changes from version 01e of tor2_0.open_stack
                 branch (wpwr VOB, base 01d) for unified code base
01d,15may97,gnn  added another new define.
01c,08may97,gnn  added a new define.
01b,24feb97,gnn  made rip_2_pkt look more logical.
01a,14nov96,gnn  written.

*/

#ifndef __INCrip2h
#define __INCrip2h

#ifdef __cplusplus
extern "C" {
#endif

/*
DESCRIPTION

INCLUDE FILES:
*/

/* includes */

#include <netVersion.h>    

/* defines */
#define RIP2_AUTH_LEN 16
#define RIP2_AUTH 0xffff
#define RIP2_DEST_OFFSET 2
#define RIP2_GATE_OFFSET 10
#define RIP2_MASK_OFFSET 6

/* typedefs */

#if ((CPU_FAMILY==I960) && (defined __GNUC__))
#pragma align 1                 /* tell gcc960 not to optimize alignments */
#endif  /* CPU_FAMILY==I960 */

typedef struct rip_2_pkt
    {
    UINT16 family;
    UINT16 tag;
    UINT32 dest;
    UINT32 subnet;
    UINT32 gateway;
    UINT32 metric;
    } RIP2PKT;

typedef struct rip2AuthPktHdr
    {
    UINT16 authIdent;        /* authentication identifier (always 0xff) */
    UINT16 authType;         /* the authentication type */
    UINT16 packetLen;        /* the offset (in bytes) to the digest */
    UINT8  keyId;            /* key identifier used to compute digest */
    UINT8  authDataLen;      /* length of the digest */
    UINT32 sequence;         /* sequence number of the packet */
    UINT32 zero1;            /* zeroed out */
    UINT32 zero2;            /* zeroed out */
    } RIP2_AUTH_PKT_HDR;

typedef struct rip2AuthPktTrl
    {
    UINT16 authIdent;        /* authentication identifier (always 0xff) */
    UINT16 authTag;          /* the authentication type/tag (always 0x01) */
    UINT8  authDigest [AUTH_KEY_LEN];  /* the computed packet digest */
    } RIP2_AUTH_PKT_TRL;

#if ((CPU_FAMILY==I960) && (defined __GNUC__))
#pragma align 0                 /* turn off alignment requirement */
#endif  /* CPU_FAMILY==I960 */

/* globals */

/* locals */

/* forward declarations */
#ifdef __cplusplus
}
#endif

#endif /* __INCrip2h */
