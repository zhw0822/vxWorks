/* adv_net.h - Network header for project facility build */

/* Copyright 1984-2005 Wind River Systems, Inc. */

/*
modification history
--------------------
01m,27sep05,nee  marked for deprecation; fixed build errors
01l,31jan05,sar  Fix defects found in GTF code inspection of 10/26/2004
                 as described in SPRs 106685 - 105688 (removed gtfLibInit)
01k,19sep04,spm  updated hostname cache initialization for virtual stack
                 startup: removed obsolete hostSetup.h file
01j,04nov03,rlm  Ran batch header path update for header re-org.
01i,03nov03,rlm  Removed wrn/coreip/ prefix from #includes for header re-org.
01h,15sep03,vvv  updated path for new headers
01g,08sep03,vvv  merged from ACCORDION_BASE6_MERGE_BASELINE
01f,26aug02,ham  added string.h and taskLib.h
01e,18jul02,ham  consolidated across t3/t22/t202. removed netconf.h,
                 added m2Ipv6Lib.h
01d,16jul02,hsh  add semLib.h wdLib.h selectLib.h before clarinet.h to make sure
                 it compiles for ppc
01d,10apr02,rvr  made changes required for tor202 porting
01d,28feb02,ham  added route.h, sysctlbyname.
01c,27feb02,ham  changed daemon.h to netLib.h
01b,08feb02,ham  added missing externs to resolve compilation warnings.
01a,03feb02,ham  wrote.
*/

#warning "This file is deprecated and will be removed in a future release"

#ifndef __INCadv_neth
#define __INCadv_neth

#ifdef __cplusplus
extern "C" {
#endif

#include <sllLib.h>
#include <dllLib.h>
#include <lstLib.h>
#include <sys/times.h>
#include <semLib.h>
#include <string.h>
#include <taskLib.h>
#include <wdLib.h>
#include <selectLib.h>
#include <private/clarinet.h>
#include <sys/mem_stru.h>
#include <sys/ds_conf.h>
#include <sys/queue.h>
#include <private/gtf_core.h>
#include <netLib.h>
#include <net/domain.h>
#include <end.h>
#include <muxLib.h>
#include <muxTkLib.h>
#include <net/socketvar.h>
#include <sys/socket.h>
#include <sockFunc.h>
#include <sockLib.h>
#include <bsdSockLib.h>
#include <net/if.h>
#include <netinet/in.h>
#include <net/if_var.h>
#include <net/if_arp.h>
#include <netinet/if_ether.h>
#include <netinet/in_systm.h>
#include <netinet/ip.h>
#include <netinet6/in6_var.h>
#include <netinet6/nd6.h>
#include <ipProto.h>
#include <arpLib.h>
#include <m2Lib.h>
#include <m2Ipv6Lib.h>
#include <sys/sysctl.h>
#include <sysctlLib.h>
#include <net/route.h>

/* new for virtualization*/
#ifdef VIRTUAL_STACK
#include <netinet/vsLib.h>
#include <cfgDefs.h>
#endif /* VIRTUAL_STACK */


/* Config params headers */

#include <netCore.h>
#include <ip6Lib.h>
#include <protos/nd6Lib.h>
#include <protos/icmpv6Lib.h>

/* extern */

extern int routec (char *);
extern int ifconfig (char *);
extern STATUS ndp (char *);
extern void netstatInit (void);
extern void netShowInit (void);
extern int sysctlbyname (char *, void *, size_t *, void *, size_t);

/* old config macros */

#ifdef INCLUDE_NET_INIT
#define INCLUDE_NETWORK
#endif

#ifdef __cplusplus
}
#endif

#endif /* __INCadv_neth */
