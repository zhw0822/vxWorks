/* nfs2Drv.h - nfs2Drv header */

/* Copyright 1984 - 2003 Wind River Systems, Inc. */

/*
modification history
--------------------
01a,10oct03,snd  written.
*/

#ifndef __INCnfs2Drvh
#define __INCnfs2Drvh

#ifdef __cplusplus
extern "C" {
#endif

#include "wrn/netVersion.h"
#include "vwModNum.h"
#include "limits.h"
#include "hostLib.h"
#include "xdr_nfs2.h"

/* function declarations */

#if defined(__STDC__) || defined(__cplusplus)

extern STATUS     nfs2Drv (void);
extern int        nfs2DrvNumGet (void);
extern STATUS     nfs2Mount (char *host, char *fileSystem, char *localName);
extern STATUS     nfs2MountAll (char *host, char *clientName, BOOL quiet);
extern STATUS     nfs2UnmountAll (char *host);
#else

extern STATUS     nfs2Drv ();
extern int        nfs2DrvNumGet ();
extern STATUS     nfs2Mount ();
extern STATUS     nfs2MountAll ();
extern STATUS     nfs2UnMountAll ();

#endif    /* __STDC__ */


#ifdef __cplusplus
}
#endif

#endif /* __INCnfs2Drvh */
