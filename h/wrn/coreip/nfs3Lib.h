/* nfs3Lib.h - Network File System library header */

/* Copyright 1984-2004 Wind River Systems, Inc. */

/*
modification history
--------------------
01d,05nov04,vvv  added nfsCommon.h (SPR #103655)
01c,23aug04,rp   merged from COMP_WN_IPV6_BASE6_ITER5_TO_UNIFIED_PRE_MERGE
01b,28may04,niq  Merging from base6 label POST_ITER5_FRZ16_REBASE (ver
                 /main/vdt/base6_itn5_networking-int/1)
01a,10oct03,snd  written                                      
*/

#ifndef __INCnfs3Libh
#define __INCnfs3Libh

#ifdef __cplusplus
extern "C" {
#endif

#include "vwModNum.h"
#include "dirent.h"
#include "iosLib.h"
#include "ioLib.h"
#include "sys/stat.h"
#include "xdr_mnt3.h"
#include "xdr_nfs3.h"
#include "nfs3Defines.h"
#include "nfsCommon.h"
    
#define  NAME3_MAX 255
/* nfs file descriptor */

typedef struct                 /* NFS3_FD - NFS file descriptor */
    {
    NFS3_DEV *nfsDev;          /* pointer to this file's NFS device */
    char     curFilename [NAME_MAX+1]; /* Current file name */    
    nfs_fh3  fileHandle;         /* file's file handle */
    nfs_fh3  dirHandle;            /* file's directory file handle */
    fattr3   fileAttr;            /* file's attributes */
    unsigned int mode;             /* (O_RDONLY, O_WRONLY, O_RDWR) */
    unsigned int fileCurByte;  /* number of current byte in file */
    SEM_ID   nfsFdSem;            /* accessing semaphore */
    BOOL     cacheValid;        /* TRUE: cache valid and remaining */
                              /* fields in structure are valid. */
                              /* FALSE: cache is empty or */
                /* cacheCurByte not the same as the */
                	/* current byte in the file. */
    char    *cacheBuf;        /* pointer to file's read cache */
    char     *cacheCurByte;        /* pointer to current byte in cache */
    unsigned int cacheBytesLeft;    /* number of bytes left in cache */
                	/* between cacheCurByte and the end */
                	/* of valid cache material.  NOTE: */
                	/* This number may be smaller than */
                	/* the amount of room left in for */
                	/* the cache writing. */
    BOOL     cacheDirty;        /* TRUE: cache is dirty, not flushed */
    cookie3     *pdir_cookie;        /* This is the NFS3 directory cookie */
    cookieverf3 dir_cookieverf; /* The directory entry's cookie verifier */
                                /* Stored here because there is no space */
                                /* in the Directory entry DIR structure */
    READDIR3res  *dirCache;     /* cache of dir list entries */
    } NFS3_FD;



/* function declarations */

#if defined(__STDC__) || defined(__cplusplus)

extern STATUS   nfs3DirMount (char *hostName, dirpath dirname,
                              nfs_fh3 *pFileHandle);

extern STATUS   nfs3DirUnmount (char * hostName, dirpath dirName);
extern STATUS   nfs3ExportRead (char *hostName, exports *pExports);
extern void     nfs3FileAttrGet (fattr3 *pFattr, struct stat *pStat);

extern STATUS   nfs3FsAttrGet (char* pHostName,nfs_fh3* pDirHandle,
                               struct statfs* pArg );

extern int      nfs3FileRead (char *hostName, nfs_fh3 *pHandle,
                              unsigned int offset, unsigned int count,
                              char *buf, fattr3 *pNewAttr);
extern STATUS  nfs3FileRemove (char *hostName, nfs_fh3 *pMountHandle,
                                char *fullFileName);
extern int     nfs3FileWrite (char *hostName, nfs_fh3 *pHandle,
                               unsigned int offset, unsigned int count,
                               char *data, fattr3 *pNewAttr);
extern int     nfs3LookUpByName (char *hostName, char *fileName,
                                  nfs_fh3 *pMountHandle, LOOKUP3res *pDirOpRes,
                                  nfs_fh3 *pDirHandle, BOOL removeFlag);
extern int     nfs3ThingCreate (char *hostName, char * fullFileName, 
                                 nfs_fh3 *pMountHandle, CREATE3res *pDirOpRes, 
                                 nfs_fh3 *pDirHandle, u_int mode); 
                                 
extern STATUS   nfs3DirReadOne ( NFS3_FD *pFd,DIR *  pDir );

extern STATUS   nfs3FsInfoGet  (char * pHostName, nfs_fh3 * pMountHandle,
                                nfsinfo * pArg);
extern STATUS   nfs3Rename ( char  *hostName, nfs_fh3  *pMountHandle, 
                             char  *oldName,  nfs_fh3  *pOldDirHandle,
                             char  *newName);
extern STATUS nfs3MntUnmountAll ( char *hostName); 

extern STATUS nfs3MntDump (char *hostName);

extern STATUS   nfs3ExportRead (char *hostName, exports *pExports);

#else

extern STATUS     nfs3DirMount ();
extern STATUS     nfs3DirUnmount ();
extern STATUS     nfs3DirReadOne ();
extern STATUS     nfs3ExportFree ();
extern STATUS     nfs3ExportRead ();
extern void    nfs3FileAttrGet ();
extern STATUS  nfs3FsAttrGet ();
extern int     nfs3FileRead ();
extern STATUS  nfs3FileRemove ();
extern int     nfs3FileWrite ();
extern int     nfs3LookUpByName ();
extern int     nfs3ThingCreate ();
extern STATUS  nfs3DirReadOne ();
extern STATUS  nfs3FsInfoGet ();
extern STATUS  nfs3MntUnmountAll ();
extern STATUS  nfs3Rename();
extern STATUS  nfs3MntUnmountAll();
extern STATUS  nfs3MntDump();
extern STATUS  nfs3ExportRead ();
#endif    /* __STDC__ */

#ifdef __cplusplus
}
#endif

#endif /* __INCnfs3Libh */
