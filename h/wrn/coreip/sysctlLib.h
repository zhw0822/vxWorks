/* sysctlLib.h - Header file for the sysctl library */

/* Copyright 2001-2005 Wind River Systems, Inc. */

/*
modification history
--------------------
01t,20jul05,vvv  added sctpSysctlInit() and sctp6SysctlInit() prototypes
01s,20may05,kch  Added mip6ProtoSysctlInit() and ip6MrouteSysctlInit()
                 prototypes.
01r,29apr05,kch  Added mldSysctlInit().
01q,21apr05,kch  Removed in6RmxSysctlInit() prototype.
01p,07feb05,wap  Allow sysctl init routines to be scaled out
01o,24jan05,vvv  osdep.h cleanup
01n,20jan05,sar  Removal of divert, dummynet and fw code.
01m,17sep04,niq  Fix a typo
01l,23aug04,rp   merged from COMP_WN_IPV6_BASE6_ITER5_TO_UNIFIED_PRE_MERGE
01k,28may04,niq  Merging from base6 label POST_ITER5_FRZ16_REBASE (ver
                 /main/vdt/base6_itn5-int/1)
01j,20nov03,niq  Remove copyright_wrs.h file inclusion
01i,04nov03,rlm  Ran batch header path update for header re-org.
01h,03nov03,rlm  Removed wrn/coreip/ prefix from #includes for header re-org.
01g,24oct03,cdw  update include statements post header re-org.
01f,08sep03,vvv  merged from ACCORDION_BASE6_MERGE_BASELINE
01e,17jul02,ppp  adding an extern to the defination for sysctlLibInit
01d,03feb02,ham  changes for tornado build.
01c,12dec01,ann  Adding the changes from testing
01b,03dec01,niq  Add prototypes for sysctlInit routines
01a,22oct01,ann  written
*/

#ifndef __INCsysctllibh
#define __INCsysctllibh

#ifdef __cplusplus
extern "C" {
#endif

#define INCLUDE_SYSCTL

#ifdef INCLUDE_SYSCTL

#include <sys/sysctl.h>

/* Extern */

/* Prototypes */

STATUS sysctlInit (void);
IMPORT STATUS sysctlLibInit (void);

IMPORT STATUS mbufSysctlInit (void);
IMPORT STATUS kernSysctlInit (void);
IMPORT STATUS ifSysctlInit (void);
IMPORT STATUS ifEtherSubrSysctlInit (void);
IMPORT STATUS ifMibSysctlInit (void);
IMPORT STATUS ifEtherSysctlInit (void);
IMPORT STATUS rtsockSysctlInit (void);
IMPORT STATUS socketSysctlInit (void);


#if 1  /* These will be called from configlettes. So we wouldn't need them */
IMPORT STATUS archSysctlInit (void);
IMPORT STATUS icmpv4SysctlInit (void);
IMPORT STATUS igmpSysctlInit (void);
IMPORT STATUS inSysctlInit (void);
IMPORT STATUS inPcbSysctlInit (void);
IMPORT STATUS in4ProtoSysctlInit (void);
IMPORT STATUS ipInputSysctlInit (void);
IMPORT STATUS ipRawSysctlInit (void);
IMPORT STATUS inGifSysctlInit (void);
IMPORT STATUS inRmxSysctlInit (void);
IMPORT STATUS ipSysctlInit (void);
IMPORT STATUS ipFlowSysctlInit (void);
IMPORT STATUS in6ProtoSysctlInit (void);
IMPORT STATUS tcpv4SysctlInit (void);
IMPORT STATUS tcpv6SysctlInit (void);
IMPORT STATUS udpv4SysctlInit (void);
IMPORT STATUS udpv6SysctlInit (void);
IMPORT STATUS nd6SysctlInit (void);
IMPORT STATUS icmp6SysctlInit (void);
IMPORT STATUS mldSysctlInit (void);
IMPORT STATUS ip6MrouteSysctlInit (void);
IMPORT STATUS mip6ProtoSysctlInit (void);
IMPORT STATUS ipsecv4SysctlInit (void);
IMPORT STATUS ipsecv6SysctlInit (void);
IMPORT STATUS ipsecKeySysctlInit (void);
IMPORT STATUS usrHwSysctlInit (void);
IMPORT STATUS sctpSysctlInit (void);
IMPORT STATUS sctp6SysctlInit (void);
#endif

#ifdef _WRS_KERNEL
void   sysctl_sysctl_debug_dump_node (struct sysctl_oid_list *, int);
#endif

#endif    /* #ifdef SYSCTL */

#ifdef __cplusplus
}
#endif

#endif /* __INCsysctllibh */

