/* rlogLib.h - header file for rlogLib.c */

/* Copyright 1984-2004 Wind River Systems, Inc. */

/*
modification history
--------------------
01i,25feb04,asr  updates resulting from review comments
01h,06feb04,asr  included lstlib.h
01g,30jan04,dlk  Remove extra tokens at end of #ifndef directives.
01f,12dec03,rp   support for multiple rlogin sessions
01e,05nov03,cdw  Removal of unnecessary _KERNEL guards.
01d,04nov03,rlm  Ran batch header path update for header re-org.
01c,03nov03,rlm  Removed wrn/coreip/ prefix from #includes for header re-org.
01b,02oct02,ant    virtualization changes
01c,06aug03,nee  Accordion Base6 merge from ACCORDION_BASE6_MERGE_BASELINE
                 label
01b,10jun03,vvv  include netVersion.h
01a,10jun02,ann  ported to clarinet from AE1.1 ver 02b
*/

#ifndef __INCrlogLibh
#define __INCrlogLibh

#ifdef __cplusplus
extern "C" {
#endif

#include <lstLib.h>
#include <netVersion.h>

#ifndef PTY_DEVICE_NAME_MAX_LEN
#define PTY_DEVICE_NAME_MAX_LEN 128 /* The maximum string length of a pty device */
#endif

/* The maximum string length of pty i/o task name */
#ifndef IO_TASK_NAME_MAX_LEN
#define IO_TASK_NAME_MAX_LEN 128
#endif

/* command interpreter control operations */

#ifndef REMOTE_START
#define REMOTE_START 0
#endif
#ifndef REMOTE_STOP
#define REMOTE_STOP 1
#endif

#ifdef _WRS_KERNEL
/* rlogin server's session data */
typedef struct
    {
    NODE        node;                /* for link-listing */
    char        ptyRemoteName[PTY_DEVICE_NAME_MAX_LEN];  
                                     /* name of the pty for this session */
    int         socket;              /* socket connection */
    int         inputFd;             /* input to command interpreter */
    int         outputFd;            /* output to command interpreter */
    int         slaveFd;             /* command interpreter side fd of pty */
    int         outputTask;          /* Task ID to send data to socket */
    int         inputTask;           /* Task ID to send data to socket */
    FUNCPTR     parserControl;       /* source of input/output files */
    BOOL        busyFlag;            /* input/output tasks in use? */
    BOOL        loggedIn;            /* Has a shell been started on this slot? */
    UINT32      userData;            /* A storage for application developers */
    int         shellTaskId;         /* TaskID of shell associated with session */
    } RLOGIND_SESSION_DATA;

/* rlogin server's task data */
typedef struct
    {
    RLOGIND_SESSION_DATA *pSession;     /* link to session data for tasks */
    } RLOGIND_TASK_DATA;

#endif /* _WRS_KERNEL */

/* function declarations */

#if defined(__STDC__) || defined(__cplusplus)

extern STATUS 	rlogin (char *host);
#ifndef VIRTUAL_STACK
  extern void 	rlogChildTask (void);
#else
  extern void 	rlogChildTask (int stackNum);
#endif /* VIRTUAL_STACK */

#ifdef _WRS_KERNEL
  #ifndef VIRTUAL_STACK
    extern STATUS 	rlogInit (void);
    extern void 	rlogind (void);
    extern void 	rlogInTask (RLOGIND_SESSION_DATA *);
    extern void 	rlogOutTask (RLOGIND_SESSION_DATA *);
  #else
    extern void 	rlogind (int stackNum);
    extern void 	rlogInTask (RLOGIND_SESSION_DATA *, int stackNum);
    extern void	        rlogOutTask (RLOGIND_SESSION_DATA *, int stackNum);
    extern void 	rlogChildTask (int stackNum);
  #endif /* VIRTUAL_STACK */
#endif /* _WRS_KERNEL */

#else	/* __STDC__ */

extern STATUS 	rlogin ();
extern void 	rlogChildTask ();
#ifdef _WRS_KERNEL
  extern STATUS 	rlogInit ();
  extern void 	rlogInTask ();
  extern void 	rlogOutTask ();
  extern void 	rlogind ();
#endif /* _WRS_KERNEL */

#endif	/* __STDC__ */

#ifdef __cplusplus
}
#endif

#endif /* __INCrlogLibh */
