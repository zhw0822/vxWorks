/* vsRdisc.h - virtual stack data for Router Discovery */

/* Copyright 2003 Wind River Systems, Inc. */

/*
modification history
--------------------
01g,04nov03,rlm  Ran batch header path update for header re-org.
01f,03nov03,rlm  Removed wrn/coreip/ prefix from #includes for header re-org.
01e,27mar03,kal  Code review modifications - terminateFlag -> runningFlag
01d,13dec02,kal  Added variables for exit sock and task opts
01c,04nov02,kal  Ported from Router Stack 1.1 to Accordion stack
01b,26oct01,spm  isolated virtual stack data structures (SPR #71092)
01a,29mar01,spm  file creation: copied from version 01b of tor2_0.open_stack
                 branch (wpwr VOB) for unified code base
*/

#ifndef __INCvsrdisch
#define __INCvsrdisch


/* includes */

#include <vxWorks.h>
#include <wdLib.h>

/* defines */

/* typedefs */

typedef struct vsRdisc
    {
    WDOG_ID wdId;
    int rdiscSock;
    int rdiscExitSock;
    int rdiscExitPort;
    BOOL runningFlag;
    BOOL debugFlag;
    int rdiscNumInterfaces;
    struct ifrd *pIfDisc;
    int MaxAdvertInterval;
    int MinAdvertInterval;
    SEM_ID rdiscIfSem;
    int rdiscTaskPri;
    int rdiscTaskOpts;
    int rdiscTaskStackSize;
    } VS_RDISC;

/* macros */

#define VS_RDISC_DATA (vsRdiscTbl[myStackNum])

#define wdId                VS_RDISC_DATA->wdId
#define rdiscSock           VS_RDISC_DATA->rdiscSock
#define rdiscExitSock       VS_RDISC_DATA->rdiscExitSock
#define rdiscExitPort       VS_RDISC_DATA->rdiscExitPort
#define runningFlag         VS_RDISC_DATA->runningFlag
#define debugFlag           VS_RDISC_DATA->debugFlag
#define rdiscNumInterfaces  VS_RDISC_DATA->rdiscNumInterfaces
#define pIfDisc             VS_RDISC_DATA->pIfDisc
#define MaxAdvertInterval   VS_RDISC_DATA->MaxAdvertInterval
#define MinAdvertInterval   VS_RDISC_DATA->MinAdvertInterval
#define rdiscIfSem          VS_RDISC_DATA->rdiscIfSem
#define rdiscTaskPri        VS_RDISC_DATA->rdiscTaskPri
#define rdiscTaskOpts       VS_RDISC_DATA->rdiscTaskOpts
#define rdiscTaskStackSize  VS_RDISC_DATA->rdiscTaskStackSize

#endif /* __INCvsrdisch */
