/* vsArp.h - virtual stack data for ARP and RARP */

/* Copyright 2000 - 2005 Wind River Systems, Inc. */

/*
modification history
--------------------
01j,22jun05,spm  fixed proxy ARP server for overlapping networks (SPR #109580)
01i,09feb05,sar  Move gtf include files per GTF code inspection of 10/26/2004
01h,23aug04,rp   merged from COMP_WN_IPV6_BASE6_ITER5_TO_UNIFIED_PRE_MERGE
01g,28may04,niq  Merging from base6 label POST_ITER5_FRZ16_REBASE (ver
                 /main/vdt/base6_itn5_networking-int/1)
01f,05nov03,cdw  Removal of unnecessary _KERNEL guards.
01e,04nov03,rlm  Ran batch header path update for header re-org.
01d,03nov03,rlm  Removed wrn/coreip/ prefix from #includes for header re-org.
01c,06sep02,hgo  removed #ifdef from VS_ARP
01b,17sep02,syy  Merge from version 2 of Accordion Munich branch:
                 (modifications for IPv4/6 design: 01b,19jun02,hgo )
01a,11jul02,spm  automatic merge from version 1 of Accordion Munich branch:
                 (copied from Router Stack release: 01b,26oct01,spm)
*/

#ifndef __INCvsArph
#define __INCvsArph

/*
 *
 *  1) arpt_prune (specifies how often list is checked for outdated entries)
 *  2) arpt_keep (specifies how long an entry is kept without refresh)
 *  3) arpt_down (remaining time in the table when an entry is declared down)
 *  4) arp_intrq 
 *  5) arp_maxtries (max tries to resolve Ethernet address)
 *  6) arp_proxyall
 *  7) useloopback (use loopbackinterface for local traffic)
 *  8) log_arp_wrong_iface (enable resolution error logging)
 *  9) arp_gratuituous (send gratuituous ARP when link/inet is enabled)
 *
 * static in arprtrequest()/if_ether.c
 * 
 * 10) null_sdl (const -> not modified)
 * 11) arpinitdone (only used for single instance)
 *
 */


/* includes */
#include <netinet/if_ether.h>
#include <private/gtf_core.h>

/* defines */

/* typedefs */

typedef struct vsArp
    {
    /* definitions from netinet/if_ether.c */
    FUNCPTR                  proxyArpHook;    
    int                     _priv_arpt_prune;
    int                     _priv_arpt_keep;
    int                     _priv_arpt_down;
    LIST_HEAD(,llinfo_arp)  _arp_llinfo;
    int                     _priv_arp_inuse;
    int                     _priv_arp_allocated;
    int                     _priv_arpinit_done;
    GTF_TIMER              *_priv_pArpTimer;
    int                     _priv_arpMaxEntries;
    int                     _priv_arp_maxtries;
    int                     _priv_useloopback;
    int                     _priv_arp_proxyall;
    int                     _priv_log_arp_wrong_iface;
    int                     _arp_gratuituous;
    struct rtentry *        pProxyRoute;
    } VS_ARP;


/* macros for "non-private" global variables */

#define VS_ARP_DATA ((VS_ARP *)vsTbl[myStackNum]->pArpGlobals)

#define proxyArpHook	 VS_ARP_DATA->proxyArpHook
#define arp_llinfo       VS_ARP_DATA->_arp_llinfo
#define arp_gratuituous  VS_ARP_DATA->_arp_gratuituous
#define pProxyRoute	 VS_ARP_DATA->pProxyRoute

#endif /* __INCvsArph */

