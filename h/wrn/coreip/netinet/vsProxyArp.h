/* vsProxyArp.h - virtual stack data for Proxy Arp */

/* Copyright 2000 - 2002 Wind River Systems, Inc. */

/*
modification history
--------------------
01c,04nov03,rlm  Ran batch header path update for header re-org.
01b,03nov03,rlm  Removed wrn/coreip/ prefix from #includes for header re-org.
01a,26jul02,vlk  ported from Synth(Router 1.1)
*/

#ifndef __INCvsprxArph
#define __INCvsprxArph

/* include */
#include <hashLib.h>

/* typedefs */
typedef struct vsProxyArp 
{
    /* defined in proxyArpLib.c */	
    BOOL	arpDebug;
    BOOL	proxyArpVerbose;
    BOOL	proxyBroadcastVerbose;
    BOOL 	proxyBroadcastFwd;
    BOOL	arpRegister;
    int		clientTblSizeDefault;
    int		portTblSizeDefault;

    SEMAPHORE	priv_proxySem;
    SEM_ID	priv_proxySemId;
    int		proxySemOptions;
    HASH_ID	priv_clientTbl;
    LIST	priv_proxyNetList;
    SEMAPHORE	priv_portTblSem;
    SEM_ID	priv_portTblSemId;
    int		portTblSemOptions;
    HASH_ID	priv_portTbl;
    BOOL	priv_proxyLibInitialized;
    int		priv_hashKeyArg;

    int 	clnt_tbl_sz;
    int 	port_tbl_sz;

    char * 	proxyd_proxy_addr;
    char * 	proxyd_main_addr;

    char 	priv_etherbuf[18];
} VS_PROXY_ARP;

/* macros for "non-private" global variables */

#define VS_PROXYARP_DATA ((VS_PROXY_ARP *)vsTbl[myStackNum]->pProxyArpGlobals)

#define arpDebug	        VS_PROXYARP_DATA->arpDebug
#define proxyArpVerbose         VS_PROXYARP_DATA->proxyArpVerbose
#define proxyBroadcastVerbose	VS_PROXYARP_DATA->proxyBroadcastVerbose
#define	proxyBroadcastFwd       VS_PROXYARP_DATA->proxyBroadcastFwd
#define	arpRegister	        VS_PROXYARP_DATA->arpRegister
#define	clientTblSizeDefault    VS_PROXYARP_DATA->clientTblSizeDefault
#define	portTblSizeDefault      VS_PROXYARP_DATA->portTblSizeDefault

#define	proxySemOptions		VS_PROXYARP_DATA->proxySemOptions
#define	portTblSemOptions       VS_PROXYARP_DATA->portTblSemOptions

#define clnt_tbl_sz             VS_PROXYARP_DATA->clnt_tbl_sz
#define port_tbl_sz             VS_PROXYARP_DATA->port_tbl_sz

#define proxyd_proxy_addr       VS_PROXYARP_DATA->proxyd_proxy_addr
#define proxyd_main_addr        VS_PROXYARP_DATA->proxyd_main_addr

#endif /* __INCvsprxArph */

