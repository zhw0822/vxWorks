/* vsIgmp.h - virtual stack data for IGMP */

/* Copyright 2000 - 2001 Wind River Systems, Inc. */
 
/*
modification history
--------------------
01e,07jul05,kch  Added _igmpGetRouterAlertHook.
01d,05nov03,cdw  Removal of unnecessary _KERNEL guards.
01c,04nov03,rlm  Ran batch header path update for header re-org.
01b,03nov03,rlm  Removed wrn/coreip/ prefix from #includes for header re-org.
01a,17jun02,ant  taken from from synth
*/

#ifndef __INCvsIgmph
#define __INCvsIgmph


/* includes */

#include <netinet/igmp_var.h> 	/* for mcastHashInfo structure */

/* defines */


/* typedefs */

typedef struct vs_igmp_global_data
    {
    struct igmpstat    _igmpstat;
    int                priv_igmp_timers_are_running;
    u_long             priv_igmp_all_hosts_group;
    u_long             priv_igmp_all_rtrs_group;
    struct mbuf        *priv_router_alert;
    struct router_info *priv_Head;
    struct route       priv_igmprt;
    VOIDFUNCPTR 	_igmpJoinAlertHook; 
    VOIDFUNCPTR 	_igmpLeaveAlertHook;
    VOIDFUNCPTR 	_igmpQuerierTimeUpdateHook;
    VOIDFUNCPTR 	_igmpMessageHook;
    VOIDFUNCPTR 	_igmpJoinGrpHook;
    VOIDFUNCPTR 	_igmpLeaveGrpHook;
    FUNCPTR		_igmpGetRouterAlertHook;
    } VS_IGMP;

/* macros */

/*
 * IGMP is optional and in.c used to check for the _igmpJoinGrpHook
 * and _igmpLeaveGrpHook being non-NULL before calling them. That is
 * valid only when we have a valid VS_IGMP_DATA. If we don't, then
 * we'll need the IS_INITIALIZED_* macros below to check if the
 * hooks are valid
 */

#define VS_IGMP_DATA ((VS_IGMP *)vsTbl[myStackNum]->pIgmpGlobals)

#define _igmpstat		VS_IGMP_DATA->_igmpstat
#define _igmpJoinAlertHook	VS_IGMP_DATA->_igmpJoinAlertHook
#define _igmpLeaveAlertHook	VS_IGMP_DATA->_igmpLeaveAlertHook
#define _igmpQuerierTimeUpdateHook VS_IGMP_DATA->_igmpQuerierTimeUpdateHook
#define _igmpMessageHook		VS_IGMP_DATA->_igmpMessageHook
#define _igmpJoinGrpHook 	VS_IGMP_DATA->_igmpJoinGrpHook
#define _igmpLeaveGrpHook	VS_IGMP_DATA->_igmpLeaveGrpHook
#define _igmpGetRouterAlertHook  VS_IGMP_DATA->_igmpGetRouterAlertHook

#define IS_INITIALIZED_JOIN_GRP_HOOK (VS_IGMP_DATA != NULL && \
					_igmpJoinGrpHook != NULL)
#define IS_INITIALIZED_LEAVE_GRP_HOOK (VS_IGMP_DATA != NULL && \
					_igmpLeaveGrpHook != NULL)

#endif /* __INCvsIgmph */
