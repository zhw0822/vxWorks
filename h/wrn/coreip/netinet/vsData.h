/* vsData.h - virtual stack data */

/*
 * Copyright (c) 2000-2005 Wind River Systems, Inc.
 *
 * The right to copy, distribute or otherwise make use of this software
 * may be licensed only pursuant to the terms of an applicable Wind River
 * license agreement. No license to Wind River intellectual property rights
 * is granted herein. All rights not licensed by Wind River are reserved
 * by Wind River.
 */

/*
modification history
--------------------
02q,20jul05,vvv  added pSctpGlobals
02p,20may05,kch  Added pMip6Globals for mobility support.
02o,19apr05,rp   merged from comp_wn_ipv6_mld_interim-dev
02n,25feb05,niq  comment cleanup
02m,19sep04,spm  reverse checkin 02k: hostname cache still needed
02l,19sep04,rae  mrouteGlobals is now void * pMrouteGlobals
02k,19sep04,spm  cleanup hostname initialization: remove unused globals
02j,17sep04,niq  Code cleanup
02i,23aug04,rp   merged from COMP_WN_IPV6_BASE6_ITER5_TO_UNIFIED_PRE_MERGE
02h,13aug04,vvv  fixed build error
02g,12aug04,kc   VS data structure reorganization for IPv6 MIB2 interface  
                 global variables.
02f,11aug04,niq  VS data structure reorganization
02e,28may04,niq  Merging from base6 label POST_ITER5_FRZ16_REBASE (ver
                 /main/vdt/base6_itn5_networking-int/1)
02d,04nov03,rlm  Ran batch header path update for header re-org.
02c,03nov03,rlm  Removed wrn/coreip/ prefix from #includes for header re-org.
02b,24oct03,cdw  update include statements post header re-org.
02a,04apr03,wie  sections framed with ifdef 0 and jw_ removed
01z,15jan03,hgo  added pIcmpUtilGlobals
01y,12dec02,hgo  added FASTUDP(6)
01x,25nov02,ism  removed pRtadvGlobals
01v,18nov02,hgo  added pHostSetupGlobals
01u,04nov02,kal  moved Router Discovery globals to its own table
01t,29aug02,hgo  add pBsdSockGlobals, pRtadvGlobals,
                 removed pSockGlobals, pNdpGlobals, pRipGlobals, pripngGlobals
01s,27aug02,kal  add pMldGlobals
01r,26aug02,kal  took out _icmpErrorHook - not used
01q,26aug02,hgo  added pRaw-,pSock-,pNd6-,Ndp-,pRipGlobals
01p,26aug02,vlk  added pDnsGlobals, pRarpGlobals
01o,26aug02,kal  took out splSem stuff
01n,26aug02,ant  added pIcmpv6Globals, pDhcprGlobals, pDhcpCommonGlobals,
		 pMrouteGlobals 
01c,11jul02,spm  fixed modification history after automatic merge
01b,11jul02,wrs  automatic merge from version 5 of Accordion Munich branch:
                 (xxjul02,wie  design review changes and cleanup)
01a,24jun02,spm  automatic merge from version 1 of Accordion Munich branch:
                 (initial version of file copied from ver. 13 of tor3_x.synth
                  branch in /wind/river VOB: modhist 01l,04jun02,vvv)
*/

#ifndef __INCvsDatah
#define __INCvsDatah

#include <netinet/vsNetCore.h>    /* always included */
#include <netBufLib.h>    /* for NET_POOL and NET_POOL_ID */
#include <semLib.h>                  /* for SEM_ID structure */
#include <wdLib.h>                   /* for WDOG_ID structure */
#include <sys/socket.h>       /* for sockproto structure */
#include <net/route.h>        /* for route_cb structure */
#include <netinet/vsM2.h>    /* always included */
#include <netinet/vsShow.h>    /* always included */
#include <vs/vsIpRoute.h>    /* always included */

/* defines */

#define NLOOP           1


/*
 * This code is directly imported from ip_input.c where it was once
 * a static struct but will now be part of the global struct.
 * We could have put it in a more "appropriate" place in an IP related
 * include file but I don't want to muddy those waters in this release.
 */

typedef struct bsd_global_data
    {
/* VS Layer.  Things the VS system needs. */
    void * pVsGlobalDataHdr;
       
/* Common storage for access to protocols, domains, and socket interface */

   void * pCoreGlobals; 	/* defined in vsNetCore.h */

/* if layer Globals */
    void * pIfGlobals;          /* defined in vsIf.h */

/* sysctl */

    void * pSysctlGlobals; 	/* defined in vsSysctl.h */

/* RTM */

    void * pRouteGlobals; 	/* defined in vsIpRoute.h */

/* Radix routing storage */

    void * pRadixGlobals; 	/* defined in vsRadix.h */
    
/* routing socket storage */

    VS_RTSOCK  rtsockGlobals; 	/* defined in vsIpRoute.h */
    
/* Proxy ARP Layer */
    void * pProxyArpGlobals; 	/* defined in vsProxyArp.h */

/* ARP Layer */
    void * pArpGlobals; 	/* defined in vsArp.h */
    
/* RARP Layer */
    void * pRarpGlobals; 	/* defined in vsRarp.h */    

/* ICMP Layer */
    void * pIcmpGlobals; 	/* defined in vsIcmp.h */

/* ICMP Utilities */
    void * pIcmpUtilGlobals;    /* defined in vsIcmpUtil.h */

/* ICMPv6 Layer */
    void * pIcmpv6Globals; 	/* defined in vsIcmpv6.h */    

/* IP Layer */
    void * pIpGlobals; 		/* defined in vsIp.h */
    
/* IP6 Layer */
    void * pIp6Globals; 		/* defined in vsIp6.h */

#ifdef MIP6
/* IP6 Layer */
    void * pMip6Globals; 		/* defined in vsMip6.h */
#endif /* MIP6 */

/* Show Routine Globals */

    VS_UDPSHOW udpShowGlobals; 	/* defined in vsShow.h */

    VS_TCPSHOW tcpShowGlobals; 	/* defined in vsShow.h */
    
/* UDP Layer */
    void * pUdpGlobals; 	/* defined in vsUdp.h */

#ifdef FASTUDP
/* Fast UDP Layer */
    void * pFastUdpGlobals;     /* defined in vsFastUdp.h */
    BOOL fastUdpInitialized;    /* normally defined in hooksinit.c  */

/* Fast UDP Layer */
    void * pFastUdp6Globals;     /* defined in vsFastUdp6.h */
    BOOL fastUdp6Initialized;    /* normally defined in hooksinit.c  */
#endif

/* m2 Layer: defined in vsM2.h */
    void * pM2SysGlobals;
    VS_M2_IF m2IfGlobals;
    void * pM2IpGlobals;
#ifdef INET6
    VS_M2_IPV6_IF m2Ipv6IfGlobals;
#endif

/* IGMPv2 Layer */
/* Definitions from netinet/igmp.c */

    void * pIgmpGlobals; 	/* defined in vsIgmp.h */
    
/* Router side IGMP globals */
    
    void * pIgmpRGlobals; 	/* defined in vsIgmpR.h */

/* TCP Layer */
    void * pTcpGlobals; 	/* defined in vsTcp.h */

/* RAW_CB */
    void * pRawGlobals; 	/* defined in vsRaw.h */

/* BSDSOCKET Layer */
    void * pBsdSockGlobals;     /* defined in vsBsdSock.h */

/* Neighbor Discovery */
    void * pNd6Globals; 	/* defined in vsNd6.h */

/* Local Hostname Cache */

    void * pHostnameGlobals;    /* defined in vsHost.h */

/* Host Table */
    void * pHostGlobals; 	/* defined in vsHost.h */

/* Domain Name System (DNS) */
    void * pDnsGlobals;		/* defined in vsDns.h */

/* Fast Forward Globals */
    void * pFastFrwdGlobals; 	/* defined in vsFastPath.h */
    int    ffIpv4State;		/* normally defined in hooksinit.c */
    int    ffIpv6State;		/* normally defined in hooksinit.c */

/* DHCP SERVER Globals */
    void * pDhcpsGlobals; 	/* defined in vsDhcps.h */

/* DHCP RELAY AGENT Globals */
    void * pDhcprGlobals; 	/* defined in vsDhcpr.h */

/* DHCP Common Globals, common for SERVER and RELAY AGENT */
    void * pDhcpCommonGlobals; 	/* defined in vsDhcpCommon.h */

/* mroute Globals */
    void *  pMrouteGlobals; 	/* defined in vsMcast.h */

/* mroute6 Globals */
    void * pMroute6Globals; 	/* defined in vsMroute6.h */

/* MLD Globals */
    void * pMldGlobals; 	/* defined in vsMld.h */
    
#ifdef SCTP
/* SCTP Globals */
    void * pSctpGlobals;        /* defined in vsSctp.h */
#endif

} BSD_GLOBAL_DATA;

IMPORT BSD_GLOBAL_DATA* vsTbl[];


/* macros */

#ifdef FASTUDP
#define fastUdpInitialized  vsTbl[myStackNum]->fastUdpInitialized
#endif

#define ffIpv4State	vsTbl[myStackNum]->ffIpv4State
#define ffIpv6State	vsTbl[myStackNum]->ffIpv6State


#endif /* __INCvsDatah */
