/* ip4_ext_out.h - typedefs and externs for the external hooks
 * used by the IPv4 stack on the output side */

/* Copyright 2002 Wind River Systems, Inc. */

/*
modification history
--------------------
01b,20nov03,niq  Remove copyright_wrs.h file inclusion
01a,07aug03,nee  Merging to Base6 from ACCORDION_BASE6_MERGE_BASELINE label on
                 Accordion codeline

*/

#ifndef __INCip4ExtOuth
#define __INCip4ExtOuth

#ifdef __cplusplus
extern "C" {
#endif

/*
 * This file contains typedefs for some of the hooks used by the IPv4 stack.
 * as well as a general sizing function used in both cases.
 * The similar hooks used by the IPv6 stack can be found in a different file.
 * The functions are separated in order to minimize the include files required
 * to use all one set of functions.
 */

typedef int (*IPSEC_OUTPUT_FUNCPTR)      (struct mbuf        ** m,
                                          struct mbuf         * options,
                                          struct route        * ro,
                                          int                   flags,
                                          struct ip_moptions  * imo,
                                          struct sockaddr_in  * dest,
                                          struct in_ifaddr    * ia);

/*
 * This is a generic function that may be used for either v4 or v6 packets.
 */
typedef size_t (*SIZING_ROUTINE_FUNCPTR) (struct mbuf  * m,
				          struct ifnet * ifp);

/* This routine is for applications to call to determine how much
   space to reserve for other modules such as IPSec */
extern size_t hdrSizeFunction (struct mbuf * m, struct ifnet * ifp);


#ifndef VIRTUAL_STACK
/* We only define the externals and macros here if we aren't supporting
 * virtual stacks.  If we are supporting them then the corresponding
 * definitions are in vsIp.h
 */

/*
 * Externals for the currently defined hooks.
 */

extern IPSEC_OUTPUT_FUNCPTR   _ipsecOutput;
extern SIZING_ROUTINE_FUNCPTR _ipsecSizing;

#define SET_IPSECOUTPUT(foo) (_ipsecOutput = foo)
#define SET_IPSECSIZING(foo) (_ipsecSizing = foo)

/* for backwards compatibility */
#define _func_ipsecOutput _ipsecOutput

#endif /* #ifndef VIRTUAL_STACK */

#ifdef __cplusplus
}
#endif

#endif /* __INCip4ExtOuth */

