/* vsIgmpR.h - virtual stack Macros for IGMP routing */

/* Copyright 2000 - 2001 Wind River Systems, Inc. */

/*
modification history
--------------------
01c,11aug04,rae  fix for Dual Stack
01b,04nov03,rlm  Ran batch header path update for header re-org.
01a,29mar01,spm  file creation: copied from version 01c of tor2_0.open_stack
                 branch (wpwr VOB) for unified code base
*/

#ifndef __INCvsIgmpRh
#define __INCvsIgmpRh


/* includes */

#include "vxWorks.h"
#include "avlUintLib.h"
#include "avlLib.h"
#include "lstLib.h"
#include "wdLib.h"
#include "msgQLib.h"
#include <netinet/in_var.h>
#include <netinet/igmp_var.h>
#include <IGMPv2/igmp_externs.h>
#include <IGMPv2/igmpPortLib.h>
#include <IGMPv2/igmpCacheLib.h>
#include <IGMPv2/igmpRouterLib.h>
#include <IGMPv2/igmp_structures.h>


/* defines */

#define VS_IGMPR_DATA ((VS_IGMP_R *)vsTbl[myStackNum]->pIgmpRGlobals)

/* typedefs */

typedef struct vsIgmpRStruct
    {
    IGMP_CLASS vs_igmp;
    SEM_ID        igmpCacheSem;
    AVL_TREE      igmpCacheTree;
    AVL_TREE *    igmpCacheTreePtr;
    SEM_ID        igmpPortSem;
    AVLU_TREE      igmpPortTree;
    AVLU_TREE *    igmpPortTreePtr;
    int igmpSocket;
    WDOG_ID igmpWd;
    MSG_Q_ID igmpMsgQ;
    SEM_ID   igmpMSem;
    int igmpTaskId;
    int  numIgmpIfs;
    IGMP_PORT_INFO portToIPArr[MAX_PORT_NUM];
    int freeVifs[MAXVIFS];
    LIST igmpNotifyHookList;
    } VS_IGMP_R;

/* macros */

#define vs_igmp          VS_IGMPR_DATA->vs_igmp
#define freeVifs         VS_IGMPR_DATA->freeVifs
#define portToIPArr      VS_IGMPR_DATA->portToIPArr
#define numIgmpIfs       VS_IGMPR_DATA->numIgmpIfs
#define igmpTaskId       VS_IGMPR_DATA->igmpTaskId
#define igmpMsgQ         VS_IGMPR_DATA->igmpMsgQ
#define igmpMSem         VS_IGMPR_DATA->igmpMSem
#define igmpWd           VS_IGMPR_DATA->igmpWd
#define igmpSocket       VS_IGMPR_DATA->igmpSocket
#define igmpCacheTreePtr VS_IGMPR_DATA->igmpCacheTreePtr
#define igmpCacheTree    VS_IGMPR_DATA->igmpCacheTree
#define igmpCacheSem     VS_IGMPR_DATA->igmpCacheSem
#define igmpPortTreePtr  VS_IGMPR_DATA->igmpPortTreePtr
#define igmpPortTree     VS_IGMPR_DATA->igmpPortTree
#define igmpPortSem      VS_IGMPR_DATA->igmpPortSem
#define igmpNotifyHookList VS_IGMPR_DATA->igmpNotifyHookList
#endif /* __INCvsIgmpRh */

