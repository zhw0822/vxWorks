/* vsTcp.h - virtual stack data for TCP */

/* Copyright 2000 - 2004 Wind River Systems, Inc. */

/*
modification history
--------------------
01i,11aug05,dlk  Disable TTCP.
01j,13sep04,vvv  renamed path_mtu_discovery to fix Sysctl/virtual stack
                 incompatibility (SPR #91565)
01i,23aug04,rp   merged from COMP_WN_IPV6_BASE6_ITER5_TO_UNIFIED_PRE_MERGE
01h,28may04,niq  Merging from base6 label POST_ITER5_FRZ16_REBASE (ver
                 /main/vdt/base6_itn5_networking-int/1)
01g,05nov03,cdw  Removal of unnecessary _KERNEL guards.
01f,04nov03,rlm  Ran batch header path update for header re-org.
01e,03nov03,rlm  Removed wrn/coreip/ prefix from #includes for header re-org.
01d,07mar03,spm  fixed VS support after merge from CLARINET-1_1-KAME-UPDATE
01c,19sep02,hgo  removed #ifdef's from VS_TCP
01b,17sep02,syy  Merge from version 2 of Accordion Munich branch:
                 (modifications for IPv4/6 design, 01b,21jun02,hgo)
01a,11jul02,spm  automatic merge from version 1 of Accordion Munich branch:
                 (copied from Router Stack release: 01b,26oct01,spm)
*/

#ifndef __INCvsTcph
#define __INCvsTcph

/*
 * Remaining globals: shared between stacks (not part of virtual structure).
 *
 *  1) tcpstates (constant list of possible states)
 *  2) tanames (constant list of names of log events for debugging)
 *  3) tcptimers (constant list of names of TCP timers for debugging)
 *  4) prurequests (constant list of names of socket events for debugging)
 *  5) tcp_outflags (constant list of per-state flags for outgoing segments)
 *  6) tcp_backoff (constant list of values for backoff retransmit intervals)
 *  7) tcp_totbackoff (sum of values from tcp_backoff list)
 *  8) tcp_syn_backoff
 *  9) tcp_usrreqs
 * 10) tcp6_usrreqs
 *
 */

/* includes */

#include <netinet/tcp_debug.h> /* for TCP_NDEBUG and tcp_debug structure */

/* typedefs */

    /* Encapsulated (former) globals for TCP. */

typedef struct vsTcp
    {
    /* defined in tcp_debug.c */

    int 		tcpDebugCons;  /* Display TCP trace output? */
    BOOL 		tcpDebugConsVerbose;    /* Use verbose form? */

    struct tcp_debug 	_priv_tcp_debug[TCP_NDEBUG];/* Log of trace records */
    int 		_priv_tcp_debx;          /* Count of trace records */
    int 	tcp_debug_valid;        /* High water mark of trace records */
    /* defined in tcp_input.c */

    u_char              _tcp_saveipgen[40];
    struct tcphdr       _tcp_savetcp;
    int 		_priv_tcprexmtthresh;
#ifdef TTCP
    tcp_cc              _tcp_ccgen;
#endif
    struct tcpstat 	_tcpStat;                /* TCP statistics */
    int                 _priv_tcp_log_in_vain;   /* log all incoming TCP connections */
    int                 _priv_tcp_blackhole;     /* do not send RST when dropping refused
                                                    connections */
    int                 _tcp_delack_enabled;     /* delay ACK to try and piggyback it onto
                                                    the packet */
    int                 _priv_drop_synfin;       /* drop TCP packets with SYN+FIN set */
    long 		_tcpOutResets; 		/* MIB-II packet counter */
    struct inpcbhead 	_tcb; 			/* queue of active tcpcb's */
    struct inpcbinfo 	_tcbinfo;
    TCP_REASS_INFO      _tcp_reass_info;

    /* defined in tcp_output.c */

    int                 pmtu_discovery;          /* enable path MTU discovery */
    int                 _ss_fltsz;               /* slow start flight size */
    int                 _ss_fltsz_local;         /* slow start flight size for local
                                                    networks */
    int                 _tcp_do_newreno;         /* enable NewReno Algorithms */

    /* defined in tcp_subr.c */

    int 		_tcp_mssdflt;            /* default TCP maximum segment size */
    int 		_tcp_v6mssdflt;
    int 		_priv_tcp_do_rfc1323;    /* enable RFC1323 (high performance) TCP
                                                    extensions */
#ifdef TTCP
    int 		_priv_tcp_do_rfc1644;    /* enable RFC1644 (TTCP) extensions */
#endif /* TTCP */
    int                 _priv_tcp_tcbhashsize;   /* size of TCP control-block hashtable */
    int                 _priv_do_tcpdrain;       /* enable tcp_drain routine for extra help
                                                    when low on mbufs */
    int                 _priv_icmp_may_rst;      /* certain ICMP unreachable message may abort
                                                    connections in SYN_SENT */
    int                 tcp_isn_reseed_interval;

    /* TCP bandwidth limiting values. */

    int 		tcp_inflight_enable;
    int 		tcp_inflight_debug;
    int 		tcp_inflight_min;
    int 		tcp_inflight_max;


    /* defined in tcp_timer.c */

    int 		_tcp_keepinit;
    int 		_tcp_keepidle;
    int 		_tcp_keepintvl;
    int 		_tcp_delacktime;         /* time before a delayed ACK is sent */
    int                 _tcp_msl;                /* maximum segment life time */
    int 		tcp_rexmit_min;
    int 		tcp_rexmit_slop;
    int                 _priv_always_keepalive;  /* assume SO_KEEPALIVE on all TCP connections */
    int                 _priv_tcp_keepcnt;

    int 		_tcp_maxpersistidle;
    int 		_tcp_maxidle;

    /* defined in tcp_usrreq.c */

    u_long 		_tcp_sendspace;          /* maximum outgoing TCP datagram size */
    u_long 		_tcp_recvspace;          /* maximum incoming TCP datagram size */

    /* defined in hooksinit.c */

    VOIDFUNCPTR		__func_tcp_drain;
    } VS_TCP;

/* macros for "non-private" global variables */

#define VS_TCP_DATA ((VS_TCP *)vsTbl[myStackNum]->pTcpGlobals)

#define tcp_saveipgen       VS_TCP_DATA->_tcp_saveipgen
#define tcp_savetcp         VS_TCP_DATA->_tcp_savetcp
#define tcpStat             VS_TCP_DATA->_tcpStat
#define tcp_delack_enabled  VS_TCP_DATA->_tcp_delack_enabled
#define tcpOutResets        VS_TCP_DATA->_tcpOutResets
#ifdef TTCP
#define tcp_ccgen           VS_TCP_DATA->_tcp_ccgen
#define tcp_do_rfc1644      VS_TCP_DATA->_priv_tcp_do_rfc1644
#endif
#define tcp_do_rfc1323      VS_TCP_DATA->_priv_tcp_do_rfc1323
#define tcb                 VS_TCP_DATA->_tcb
#define tcbinfo             VS_TCP_DATA->_tcbinfo
#define tcp_reass_info      VS_TCP_DATA->_tcp_reass_info

#define ss_fltsz            VS_TCP_DATA->_ss_fltsz
#define ss_fltsz_local      VS_TCP_DATA->_ss_fltsz_local
#define pmtu_discovery      VS_TCP_DATA->pmtu_discovery
#define tcp_do_newreno      VS_TCP_DATA->_tcp_do_newreno

#define tcp_mssdflt         VS_TCP_DATA->_tcp_mssdflt
#define tcp_v6mssdflt       VS_TCP_DATA->_tcp_v6mssdflt

#define tcp_keepinit        VS_TCP_DATA->_tcp_keepinit
#define tcp_keepidle        VS_TCP_DATA->_tcp_keepidle
#define tcp_keepintvl       VS_TCP_DATA->_tcp_keepintvl
#define tcp_delacktime      VS_TCP_DATA->_tcp_delacktime
#define tcp_msl             VS_TCP_DATA->_tcp_msl
#define tcp_rexmit_min      VS_TCP_DATA->tcp_rexmit_min
#define tcp_rexmit_slop     VS_TCP_DATA->tcp_rexmit_slop

#define tcp_maxpersistidle  VS_TCP_DATA->_tcp_maxpersistidle
#define tcp_maxidle         VS_TCP_DATA->_tcp_maxidle

#define tcp_sendspace       VS_TCP_DATA->_tcp_sendspace
#define tcp_recvspace       VS_TCP_DATA->_tcp_recvspace

#define _func_tcp_drain     VS_TCP_DATA->__func_tcp_drain

#endif /* __INCvsTcph */
