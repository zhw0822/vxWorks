/* tcp_seq.h - TCP sequence number manipulation */

/* Copyright 2001-2005 Wind River Systems, Inc. */

/*
 * Copyright (c) 1982, 1986, 1993, 1995
 *	The Regents of the University of California.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *	This product includes software developed by the University of
 *	California, Berkeley and its contributors.
 * 4. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *	@(#)tcp_seq.h	8.3 (Berkeley) 6/21/95
 * $FreeBSD: src/sys/netinet/tcp_seq.h,v 1.11.2.6 2001/12/14 20:16:59 jlemon Exp $
 */

/*
modification history
--------------------
01m,07feb05,vvv  _KERNEL cleanup
01l,20nov03,niq  Remove copyright_wrs.h file inclusion
01k,05nov03,cdw  Removal of unnecessary _KERNEL guards.
01j,04nov03,rlm  Ran batch header path update for header re-org.
01i,03nov03,rlm  Removed wrn/coreip/ prefix from #includes for header re-org.
01h,08aug03,niq  Merging from Accordion label ACCORDION_BASE6_MERGE_BASELINE
01g,10jun03,vvv  include netVersion.h
01f,01feb03,pas  merge from FreeBSD 4.7 - removed global tcp_iss
01e,10sep02,hsh  add c++ protection
01d,08feb02,pas  EAR code cleanup
01c,08oct01,pas  include random.h
01b,18sep01,pas  changed tcp_random18() to use RANDOM() instead of arc4random()
01a,13aug01,pas  Created from FreeBSD4.3-RELEASE(July-17-01).
*/  

#ifndef _NETINET_TCP_SEQ_H_
#define _NETINET_TCP_SEQ_H_

#ifdef __cplusplus
extern "C" {
#endif
     
#ifdef _WRS_KERNEL
#include <netVersion.h>

 /*
 * TCP sequence numbers are 32 bit integers operated
 * on with modular arithmetic.  These macros can be
 * used to compare such integers.
 */
#define	SEQ_LT(a,b)	((int)((a)-(b)) < 0)
#define	SEQ_LEQ(a,b)	((int)((a)-(b)) <= 0)
#define	SEQ_GT(a,b)	((int)((a)-(b)) > 0)
#define	SEQ_GEQ(a,b)	((int)((a)-(b)) >= 0)

/* for modulo comparisons of timestamps */
#define TSTMP_LT(a,b)	((int)((a)-(b)) < 0)
#define TSTMP_GEQ(a,b)	((int)((a)-(b)) >= 0)

/*
 * TCP connection counts are 32 bit integers operated
 * on with modular arithmetic.  These macros can be
 * used to compare such integers.
 */
#define	CC_LT(a,b)	((int)((a)-(b)) < 0)
#define	CC_LEQ(a,b)	((int)((a)-(b)) <= 0)
#define	CC_GT(a,b)	((int)((a)-(b)) > 0)
#define	CC_GEQ(a,b)	((int)((a)-(b)) >= 0)

/* Macro to increment a CC: skip 0 which has a special meaning */
#define CC_INC(c)	(++(c) == 0 ? ++(c) : (c))

/*
 * Macros to initialize tcp sequence numbers for
 * send and receive from initial send and receive
 * sequence numbers.
 */
#define	tcp_rcvseqinit(tp) \
	(tp)->rcv_adv = (tp)->rcv_nxt = (tp)->irs + 1

#define	tcp_sendseqinit(tp) \
	(tp)->snd_una = (tp)->snd_nxt = (tp)->snd_max = (tp)->snd_up = \
	    (tp)->snd_recover = (tp)->iss

#define TCP_PAWS_IDLE	(24 * 24 * 60 * 60 * hz)
					/* timestamp wrap-around time */

#ifndef VIRTUAL_STACK
extern tcp_cc	tcp_ccgen;		/* global connection count */
#endif
#endif /* _WRS_KERNEL */

#ifdef __cplusplus
}
#endif
    
#endif /* _NETINET_TCP_SEQ_H_ */
