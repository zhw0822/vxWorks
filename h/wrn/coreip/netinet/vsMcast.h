/* vsMcast.h - virtual stack data for IGMP */

/*
 * Copyright (c) 2000-2005 Wind River Systems, Inc.
 *
 * The right to copy, distribute or otherwise make use of this software
 * may be licensed only pursuant to the terms of an applicable Wind River
 * license agreement. No license to Wind River intellectual property rights
 * is granted herein. All rights not licensed by Wind River are reserved
 * by Wind River.
 */

/*
modification history
--------------------
01j,16jul05,dlk  Remove tbf_reprocess_qHandle; it must be vif-specific, and
		 is now a member of struct tbf.
01i,27apr05,kch  Move ip_mrouter definition to VS_IP.
01h,25feb05,niq  add support for router alert
01g,31jan05,niq  merge mroute changes from comp_wn_ipv6_multicast_interim-dev
                 branch  (ver 1)
01f,19sep04,rae  fix up
01e,04nov03,rlm  Ran batch header path update for header re-org.
01d,14jun01,spm  added modification history entries for automatic merges
01c,14jun01,wrs  automatic merge from version 2 of tor3_x.synth.irvine remote
                 branch: updated to use new multicast routing support
01b,17may01,wrs  automatic merge from version 1 of tor3_x.synth.irvine remote
                 branch: new implementation for PIM support from BSD 4.4 Net/3
01a,29mar01,spm  file creation: copied from version 01a of tor2_0.open_stack
                 branch (wpwr VOB) for unified code base
*/

#ifndef __INCvsMcasth
#define __INCvsMcasth


/* includes */
#include "netinet/ip_mroute.h"
#include "sys/callout.h"
#ifdef PIM
#include <netinet/pim.h>
#include <netinet/pim_var.h>
#endif


/* defines */

#define VS_IP_MROUTE_DATA ((VS_IP_MROUTE *)vsTbl[myStackNum]->pMrouteGlobals)

typedef struct vs_ip_mroute
{
/*
 * Globals.  All but ip_mrouter and ip_mrtproto could be static,
 * except for netstat or debugging purposes.
 */

#ifdef RSVP
	u_int				rsvpdebug;
#endif /* RSVP */

	struct mrtstat			_mrtstat;
	struct mfc *			mfctable[MFCTBLSIZ];
	struct vif 			viftable[MAXVIFS];
	u_char 				nexpire[MFCTBLSIZ];
	u_int				mrtdebug ;  /*debug level 	*/
	struct callout_handle  		expire_upcalls_ch;
	
/*
 * Define the token bucket filter structures
 * tbftable -> each vif has one of these for storing info 
 */
	struct tbf 			tbftable[MAXVIFS];
	void *				tbf_reprocess_qHandle;
/*
 * 'Interfaces' associated with decapsulator (so we can tell
 * packets that went through it from ones that get reflected
 * by a broken gateway).  These interfaces are never linked into
 * the system ifnet list & no routes point to them.  I.e., packets
 * can't be sent this way.  They only exist as a placeholder for
 * multicast source verification.
 */
	struct ifnet 			multicast_decap_if[MAXVIFS];

/*
 * Bandwidth meter variables and constants
 */
/*
 * Pending timeouts are stored in a hash table, the key being the
 * expiration time. Periodically, the entries are analysed and processed.
 */

	struct bw_meter *		bw_meter_timers[BW_METER_BUCKETS];
	struct callout_handle 		bw_meter_ch;

/*
 * Pending upcalls are stored in a vector which is flushed when
 * full, or periodically
 */
	struct bw_upcall			bw_upcalls[BW_UPCALLS_MAX];
	u_int				bw_upcalls_n; /*# of pending upcalls */
	struct callout_handle 		bw_upcalls_ch;

#ifdef PIM
	struct pimstat 			_pimstat;
	struct ifnet 			multicast_register_if;
	vifi_t 				reg_vif_num;
#endif /* VIRTUAL_STACK */

/*
 * Private variables.
 */
	vifi_t	   			numvifs;
	int 				have_encap_tunnel;

/*
 * one-back cache used by ipip_input to locate a tunnel's vif
 * given a datagram's src ip address.
 */
	u_long 				last_encap_src;
	struct vif *			last_encap_vif;
/*
 * whether or not special PIM assert processing is enabled.
 */
	int 				pim_assert;

	uint32_t 			mrt_api_config;

/*
 * Static stack variables
 */

	int				ipm_last_log;
	struct route			ipm_ro;
	uint32_t 			ipm_last_tv_sec;
} VS_IP_MROUTE;


/* typedefs */

/* macros */



#define _mrtstat  		VS_IP_MROUTE_DATA->_mrtstat
#define _mfctable  		VS_IP_MROUTE_DATA->mfctable
#define _viftable  		VS_IP_MROUTE_DATA->viftable
#define nexpire  		VS_IP_MROUTE_DATA->nexpire
#define mrtdebug  		VS_IP_MROUTE_DATA->mrtdebug 
#define rsvpdebug  		VS_IP_MROUTE_DATA->rsvpdebug 
#define expire_upcalls_ch  	VS_IP_MROUTE_DATA->expire_upcalls_ch
#define tbftable  		VS_IP_MROUTE_DATA->tbftable
#define tbf_reprocess_qHandle  	VS_IP_MROUTE_DATA->tbf_reprocess_qHandle
#define multicast_decap_if  	VS_IP_MROUTE_DATA->multicast_decap_if
#define bw_meter_timers  	VS_IP_MROUTE_DATA->bw_meter_timers
#define bw_meter_ch  		VS_IP_MROUTE_DATA->bw_meter_ch
#define bw_upcalls  		VS_IP_MROUTE_DATA->bw_upcalls
#define bw_upcalls_n  		VS_IP_MROUTE_DATA->bw_upcalls_n
#define bw_upcalls_ch  		VS_IP_MROUTE_DATA->bw_upcalls_ch
#define _pimstat  		VS_IP_MROUTE_DATA->_pimstat
#define multicast_register_if  	VS_IP_MROUTE_DATA->multicast_register_if
#define reg_vif_num  		VS_IP_MROUTE_DATA->reg_vif_num

/*
 * Private variables.
 */
#define numvifs  		VS_IP_MROUTE_DATA->numvifs
#define have_encap_tunnel  	VS_IP_MROUTE_DATA->have_encap_tunnel
/*
 * one-back cache used by ipip_input to locate a tunnel's vif
 * given a datagram's src ip address.
 */
#define last_encap_src  		VS_IP_MROUTE_DATA->last_encap_src
#define last_encap_vif 		VS_IP_MROUTE_DATA->last_encap_vif

/*
 * whether or not special PIM assert processing is enabled.
 */
#define pim_assert 		VS_IP_MROUTE_DATA->pim_assert
#define mrt_api_config 		VS_IP_MROUTE_DATA->mrt_api_config
#define ipm_last_log 		VS_IP_MROUTE_DATA->ipm_last_log
#define ipm_ro 			VS_IP_MROUTE_DATA->ipm_ro
#define ipm_last_tv_sec 	VS_IP_MROUTE_DATA->ipm_last_tv_sec

#endif /* __INCvsMcasth */
