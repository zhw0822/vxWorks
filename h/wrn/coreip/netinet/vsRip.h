/* vsRip.h - virtual stack data for RIP */

/* Copyright 2000 - 2004 Wind River Systems, Inc. */

/*
modification history
--------------------
01k,13aug04,niq  Code cleanup
01j,11aug04,niq  Add capability to use class based netmask for routes to
                 external networks - Cisco style
01i,15mar04,rp   merged from orion
01h,05nov03,cdw  Removal of unnecessary _KERNEL guards.
01g,04nov03,rlm  Ran batch header path update for header re-org.
01f,03nov03,rlm  Removed wrn/coreip/ prefix from #includes for header re-org.
01e,20mar03,hgo  use IMPORT instead of extern, added priv_badfrom(2)
01d,05sep02,hgo  own vsRipTbl
01c,17jul02,hgo  copied from Router 1.1 and changed header file paths
01b,01aug02,spm  fixed modification history after automatic merge
01a,18jul02,wrs  automatic merge from version 1 of Accordion Munich branch:
                 (copied from Router Stack release: 01c,26oct01,spm)
*/

#ifndef __INCvsRiph
#define __INCvsRiph

#include <rip/m2RipLib.h> 	/* for MIB structure definitions */
#include <rip/table.h> 		/* for rthash structure definition */
#include <rip/defs.h> 		/* for rip_globals structure definition */

/* 
 * NOTE: This include file (directly and indirectly) redefines commonly used
 * structure elements (such as rt_flags) which will prevent source code
 * outside the rip directory from compiling. Therefore it is excluded from the
 * vsData.h include file and is not referenced outside of the rip directory.
 */

#define MAXPACKETSIZE           512     /* From rip/ripLib.h */

/*
 * Unresolved globals: (should these be part of structure?):
 *   1) afswitch[] (constant list of AF_INET specific function pointers)
 *   2) af_max     (constant size of switch array)
 *   3) inet_default (constant template for sockaddr_in structure)
 */
 
/* typedefs */

    /*
     * Encapsulated (former) globals for RIP. 
     */

typedef struct vsRip
    {
    /* definitions from rip/ripLib.c */

    int 			routedDebug;
    struct rip_globals 	ripState;
    SEM_ID 		ripLockSem;
    BOOL 		ripFilterFlag;
    LIST 		ripIfExcludeList;
    int 			priv_ripTaskPriority;
    int 			priv_ripTaskOptions;
    int 			priv_ripTaskStackSize;
    int 			priv_ripTimerTaskPriority;
    int 			priv_ripTimerTaskOptions;
    int 			priv_ripTimerTaskStackSize;
    ULONG 		priv_ripTimeSeed;
    BOOL 		priv_ripInitFlag; 
    BOOL 		ripUseClassBasedNetmask;

    /* definitions from rip/startup.c */

    struct interface * 	ripIfNet;
    struct interface ** 	ifnext;
    int 			ripFoundloopback;
    struct sockaddr 	priv_loopaddr;
    struct rt_addrinfo 	priv_info;

    /* definitions from rip/tables.c */

    struct sockaddr 	priv_wildcard;

    /* definitions from rip/input.c */

    struct sockaddr 	priv_badfrom;
    struct sockaddr 	priv_badfrom2;

    /* definitions from rip/table.h */

    struct rthash 	nethash[ROUTEHASHSIZ];
    struct rthash 	hosthash[ROUTEHASHSIZ];
    } VS_RIP;

IMPORT VS_RIP *vsRipTbl[];

/* macros */

#define VS_RIP_DATA (vsRipTbl[myStackNum])

#define routedDebug 	 VS_RIP_DATA->routedDebug
#define ripState 	 VS_RIP_DATA->ripState
#define ripLockSem 	 VS_RIP_DATA->ripLockSem
#define ripFilterFlag 	 VS_RIP_DATA->ripFilterFlag
#define ripIfExcludeList  VS_RIP_DATA->ripIfExcludeList
#define ripIfNet 	 VS_RIP_DATA->ripIfNet
#define ifnext 		 VS_RIP_DATA->ifnext
#define ripFoundloopback  VS_RIP_DATA->ripFoundloopback
#define nethash 	         VS_RIP_DATA->nethash
#define hosthash 	 VS_RIP_DATA->hosthash
#define ripUseClassBasedNetmask 	 VS_RIP_DATA->ripUseClassBasedNetmask
#endif /* __INCvsRiph */
