/* igmp_var.h - igmp header file */

/*
 * Copyright (c) 2001-2005 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
 * Copyright (c) 1988 Stephen Deering.
 * Copyright (c) 1992, 1993
 *	The Regents of the University of California.  All rights reserved.
 *
 * This code is derived from software contributed to Berkeley by
 * Stephen Deering of Stanford University.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *	This product includes software developed by the University of
 *	California, Berkeley and its contributors.
 * 4. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *	from: @(#)igmp_var.h	8.1 (Berkeley) 7/19/93
 * $FreeBSD: src/sys/netinet/igmp_var.h,v 1.17 1999/12/29 04:40:59 peter Exp $
 */

/*
modification history
--------------------
01r,11oct05,niq  Fix bug in passing mblk (SPR: 113559)
01q,07oct05,vvv  protected in_msf.h include for GPP build
01p,29jun05,spm  updated for IGMPv3 support
01o,19apr05,rp   merged from comp_wn_ipv6_mld_interim-dev
01n,07feb05,vvv  _KERNEL cleanup
01m,31jan05,niq  virtual stack changes for sysctl
01l,18jan05,vvv  osdep.h cleanup
01k,23nov03,rp   updated path for random.h
01j,20nov03,niq  Remove copyright_wrs.h file inclusion
01i,05nov03,cdw  Removal of unnecessary _KERNEL guards.
01h,04nov03,rlm  Ran batch header path update for header re-org.
01g,03nov03,rlm  Removed wrn/coreip/ prefix from #includes for header re-org.
01f,23oct03,rlm  updated #includes for header re-org
01e,10jun03,vvv  include netVersion.h
01d,10sep02,hsh  add c++ protection
01c,03dec01,niq  Sysctl changes for Clarinet
01b,08oct01,pas  include random.h
01a,27aug01,hsh  Created from BSD 4.3
*/

#ifndef _NETINET_IGMP_VAR_H_
#define _NETINET_IGMP_VAR_H_

#ifdef __cplusplus
extern "C" {
#endif
     
#include <netVersion.h>
#include <netinet/in_var.h>

/*
 * Internet Group Management Protocol (IGMP),
 * implementation-specific definitions.
 *
 * Written by Steve Deering, Stanford, May 1988.
 *
 * MULTICAST Revision: 3.5.1.3
 */

#include <netinet/igmp.h>

#if (defined (IGMPV3) || defined (MLDV2))
#include <netinet/in_msf.h>
#endif

struct igmpstat {
	u_int	igps_rcv_total;		/* total IGMP messages received */
	u_int	igps_rcv_tooshort;	/* received with too few bytes */
	u_int	igps_rcv_toolong;	/* received messages over MTU size */
	u_int	igps_rcv_badsum;	/* received with bad checksum */
	u_int	igps_rcv_badttl;	/* received with bad TTL */
	u_int	igps_rcv_nora;		/* received with no Router Alert */
	u_int	igps_rcv_v1_queries;	/* received v1 membership queries */
	u_int	igps_rcv_v2_queries;	/* received v2 membership queries */
	u_int	igps_rcv_v3_queries;	/* received v3 membership queries */
	u_int	igps_rcv_badqueries;	/* received invalid queries */
	u_int	igps_rcv_query_fails;	/* receiving membership query fails */
	u_int	igps_rcv_reports;	/* received v1/v2 membership reports */
	u_int	igps_rcv_badreports;	/* received invalid v1/v2 reports */
	u_int	igps_rcv_ourreports;	/* received v1/v2 reports for our groups */
	u_int	igps_snd_v1v2_reports;	/* sent v1/v2 membership reports */
	u_int	igps_snd_v3_reports;	/* sent v3 membership reports */

};

#ifdef IGMPV3_DEBUG
#define igmplog(x)      do { if (1) log x; } while (/*CONSTCOND*/ 0)
#else
#define igmplog(x)      /* empty */
#endif

#ifdef _WRS_KERNEL
#include <random.h>
#define IGMP_RANDOM_DELAY(X) (random() % (X) + 1)

extern	struct router_info * rti_head;

/*
 * States for IGMPv2's leave processing
 */
#define IGMP_OTHERMEMBER			0
#define IGMP_IREPORTEDLAST			1

/*
 * IGMPv3 default variables
 */
#define	IGMP_DEF_RV		2	/* Default Robustness Variable */
#define	IGMP_DEF_QI		125	/* Query Interval (125 sec.) */
#define	IGMP_DEF_QRI		100	/* Query Response Interval (10 sec.) */
#define	IGMP_OQPI		((IGMP_DEF_RV * IGMP_DEF_QI) + IGMP_DEF_QRI/2)
#define	IGMP_GMI		((IGMP_DEF_RV * IGMP_DEF_QI) + IGMP_DEF_QRI)
#define	IGMP_START_INTVL	IGMP_DEF_QI/4
#define	IGMP_START_CNT		IGMP_DEF_RV
#define	IGMP_LAST_INTVL		1	/* Last Member Query Interval (sec.) */
#define	IGMP_LAST_CNT		IGMP_DEF_RV
#define	IGMP_UNSOL_INTVL	1	/* Unsolicited Report Interval (sec.) */
#define	IGMP_DEF_QUERY		10	/* v1 Max. Response Time (sec.) */

/*
 * We must remember what version the subnet's querier is.
 * We conveniently use the IGMP message type for the proper
 * membership report to keep this state.
 */
#define IGMP_V1_ROUTER				IGMP_V1_MEMBERSHIP_REPORT
#define IGMP_V2_ROUTER				IGMP_V2_MEMBERSHIP_REPORT

/*
 * Revert to new router if we haven't heard from an old router in
 * this amount of time.
 */
#define IGMP_AGE_THRESHOLD			540

void	igmp_init __P((void));
struct	router_info * rti_init(struct ifnet *);
void	igmp_input __P((struct mbuf *, int, int));
void	igmp_joingroup __P((struct in_multi *));
void	igmp_leavegroup __P((struct in_multi *));
void	igmp_sendbuf(struct mbuf *, struct ifnet *);
void	igmp_fasttimo __P((void));
void	igmp_slowtimo __P((void));
int	igmp_get_router_alert(struct mbuf **);
void	igmp_send_state_change_report(struct mbuf **, int *,
				      struct in_multi *, u_int8_t, int);
int	igmp_sysctl(int *, u_int, void *, size_t *, void *, size_t);

#include <sysctlLib.h>
#ifndef VIRTUAL_STACK
SYSCTL_DECL_NODE_EXT(igmp);
#endif /* VIRTUAL_STACK */

#endif /* _WRS_KERNEL */

/*
 * Names for IGMP sysctl objects
 */

#define	IGMPCTL_SENDWITHRA	1	/* kern sends IGMP with Router Alert */
#define	IGMPCTL_DROPWITHNORA	2	/* kern drops IGMP with no Rtr Alert */
#define	IGMPCTL_MAXSRCFILTER	3	/* max sources per group per interface*/
#define	IGMPCTL_SOMAXSRC	4	/* max sources per socket per group */
#define	IGMPCTL_ALWAYS_V3	5
#define IGMPCTL_STATS		6	/* statistics (read-only) */
#define	IGMPCTL_MAXID		7

#define	IGMPCTL_NAMES { \
	{ 0, 0 }, \
	{ "sendwithra", CTLTYPE_INT }, \
	{ "dropwithnora", CTLTYPE_INT }, \
	{ "maxsrcfilter", CTLTYPE_INT }, \
	{ "somaxsrc", CTLTYPE_INT }, \
	{ "always_v3", CTLTYPE_INT }, \
	{ "stats", CTLTYPE_STRUCT }, \
}

#ifdef __cplusplus
}
#endif
    
#endif
