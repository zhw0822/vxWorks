/* vsDhcps.h - virtual stack data for DHCP Server routines */

/* Copyright 2003 - 2004 Wind River Systems, Inc. */

/*
modification history
--------------------
01f,15mar04,rp   merged from orion
01e,05nov03,cdw  Removal of unnecessary _KERNEL guards.
01d,04nov03,rlm  Ran batch header path update for header re-org.
01c,03nov03,rlm  Removed wrn/coreip/ prefix from #includes for header re-org.
01b,24mar03,ant  added three priv_ variables to the VS_DHCPS struct (dhcps.c), 
		 three priv_ variables removed from VS_DHCPS (dhcpsLib.c)
01a,17jul02,ant  taken from synth
*/

#ifndef __INCvsdhcpsh
#define __INCvsdhcpsh

#include <net/bpf.h>
#include <dhcp/dhcp.h>
#include <dhcp/common.h>
#include <dhcpsLib.h>
#include <dhcp/hash.h>
#include <net/uio.h>

typedef struct vs_dhcps_global_data
    {
    /* Globals and locals in dhcpsLib.c */
    DHCPS_RELAY_DESC *pDhcpsRelaySourceTbl; /* Pointer to Relay Table */
    DHCPS_LEASE_DESC *pDhcpsLeasePool;	/* Pointer to Lease Pool */   
    long    dhcps_dflt_lease; 		/* Default for default lease length */
    long    dhcps_max_lease;  		/* Default maximum lease length */
    SEM_ID  dhcpsMutexSem;             	/* Synchronization for lease entry adds. */
    FUNCPTR dhcpsLeaseHookRtn;  	/* Accesses storage for lease bindings. */
    FUNCPTR dhcpsAddressHookRtn; 	/* Preserves additional address entries. */
    int dhcpsMaxSize;       		/* Transmit buffer size & largest supported message. */
    int dhcpsBufSize;       		/* Size of buffer for BPF devices */
    struct if_info *dhcpsIntfaceList;
    BOOL priv_dhcpsInitialized;
    struct bpf_insn priv_dhcpfilter[MAX_DHCPFILTERS];
    struct bpf_program priv_dhcpread;
    
    /* Locals in dhcps.c */
    int nbind;              		/* Number of active or pending lease records. */
    struct msg dhcpsMsgIn;  		/* Pointers to components of incoming message. */
    struct msg dhcpsMsgOut; 		/* Pointers to outgoing message parts. */
    char *dhcpsSendBuf;     		/* Transmit buffer for outgoing messages. */
    char *dhcpsOverflowBuf; 		/* Extra space (for larger messages) starts here. */
    struct iovec sbufvec[2];		/* Socket access to outgoing message.
				         * sbufvec[0] is standard message.
					 * sbufvec[1] contains message extensions if
				         * client accepts longer messages. */
    unsigned char priv_dhcpCookie[MAGIC_LEN]; /* DHCP message indicator. */
    int priv_rdhcplen;             		/* Size of received DHCP message. */
    int priv_overload;             		/* Options in sname or file fields? */
    int priv_off_options;          		/* Index into options field. */
    int priv_off_extopt;           		/* Index into any options in sbufvec[1]. */
    int priv_maxoptlen;            		/* Space available for options. */
    int priv_off_file;             		/* Index into any options within file field. */
    int priv_off_sname;            		/* Index into any options in sname field. */
    char priv_resultHaddrtos [MAX_HLEN * 2 + 8];/* hardware address in cache format
    						 * <type>:0x<value> */    
    char priv_resultCidtos [MAXOPT * 2 + INET_ADDRSTRLEN + 3];
    						/* client identifier in cache format
						 * <type>:0x<value> */
    time_t priv_prev_epoch;

    /* Globals in database.c */

    struct hash_tbl cidhashtable;
    struct hash_tbl iphashtable;    
    struct hash_tbl nmhashtable;   
    struct hash_tbl relayhashtable;
    struct hash_tbl paramhashtable;
    struct hash_member *bindlist;
    struct hash_member *reslist;      

    } VS_DHCPS;


/* Macro */

#define VS_DHCPS_DATA ((VS_DHCPS *)vsTbl[myStackNum]->pDhcpsGlobals)

/* Defines for the dhcp routine globals */

/* Globals in dhcpsLib.c */
#define pDhcpsRelaySourceTbl	VS_DHCPS_DATA->pDhcpsRelaySourceTbl
#define pDhcpsLeasePool		VS_DHCPS_DATA->pDhcpsLeasePool
#define dhcps_dflt_lease	VS_DHCPS_DATA->dhcps_dflt_lease
#define dhcps_max_lease		VS_DHCPS_DATA->dhcps_max_lease
#define dhcpsMutexSem  		VS_DHCPS_DATA->dhcpsMutexSem
#define dhcpsLeaseHookRtn 	VS_DHCPS_DATA->dhcpsLeaseHookRtn
#define dhcpsAddressHookRtn 	VS_DHCPS_DATA->dhcpsAddressHookRtn
#define dhcpsMaxSize 		VS_DHCPS_DATA->dhcpsMaxSize
#define dhcpsBufSize 		VS_DHCPS_DATA->dhcpsBufSize
#define dhcpsIntfaceList	VS_DHCPS_DATA->dhcpsIntfaceList

/* Globals in dhcps.c */
#define nbind			VS_DHCPS_DATA->nbind
#define dhcpsMsgIn		VS_DHCPS_DATA->dhcpsMsgIn
#define dhcpsMsgOut		VS_DHCPS_DATA->dhcpsMsgOut
#define dhcpsSendBuf		VS_DHCPS_DATA->dhcpsSendBuf
#define dhcpsOverflowBuf	VS_DHCPS_DATA->dhcpsOverflowBuf
#define sbufvec			VS_DHCPS_DATA->sbufvec

/* Globals in database.c */
#define cidhashtable		VS_DHCPS_DATA->cidhashtable
#define iphashtable		VS_DHCPS_DATA->iphashtable
#define nmhashtable		VS_DHCPS_DATA->nmhashtable
#define relayhashtable		VS_DHCPS_DATA->relayhashtable
#define paramhashtable		VS_DHCPS_DATA->paramhashtable
#define bindlist		VS_DHCPS_DATA->bindlist
#define reslist			VS_DHCPS_DATA->reslist


#endif /* __INCvsdhcpsh */
