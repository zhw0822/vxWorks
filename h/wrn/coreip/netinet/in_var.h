/* in_var.h - in_var header file */

/*
 * Copyright (c) 2001-2005 Wind River Systems, Inc.
 *
 * The right to copy, distribute or otherwise make use of this software
 * may be licensed only pursuant to the terms of an applicable Wind River
 * license agreement. No license to Wind River intellectual property rights
 * is granted herein. All rights not licensed by Wind River are reserved
 * by Wind River.
 */

/*
 * Copyright (c) 1985, 1986, 1993
 *	The Regents of the University of California.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *	This product includes software developed by the University of
 *	California, Berkeley and its contributors.
 * 4. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *	@(#)in_var.h	8.2 (Berkeley) 1/9/95
 * $FreeBSD: src/sys/netinet/in_var.h,v 1.33.2.3 2001/12/14 20:09:34 jlemon Exp $
 */
 
/*
 * Copyright (c) 2002 INRIA. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *	This product includes software developed by INRIA and its
 *	contributors.
 * 4. Neither the name of INRIA nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */
/*
 * Implementation of Internet Group Management Protocol, Version 3.
 *
 * Developed by Hitoshi Asaeda, INRIA, February 2002.
 */

/*
modification history
--------------------
02a,25aug05,dlk  Add section tags.
01z,13jul05,dlk  Added ipintrqJob declaration.
01y,29jun05,spm  updated for IGMPv3 support
01x,19apr05,rp   merged from comp_wn_ipv6_mld_interim-dev
01w,02mar05,niq  Make router alert support scalable
01v,25feb05,niq  merge from niqbal.tor3.mroute for IP router alert changes
01u,07feb05,vvv  _KERNEL cleanup
01t,31jan05,niq  virtual stack changes for sysctl
01s,24jan05,vvv  osdep.h cleanup
01r,23aug04,rp   merged from COMP_WN_IPV6_BASE6_ITER5_TO_UNIFIED_PRE_MERGE
01q,28may04,niq  Merging from base6 label POST_ITER5_FRZ16_REBASE (ver
                 /main/vdt/base6_itn5_networking-int/2)
01p,25nov03,ppp  added a comment around the inclusion of in6_var.h
01o,21nov03,ppp  rtp work
01n,20nov03,niq  Remove copyright_wrs.h file inclusion
01m,04nov03,rlm  Ran batch header path update for header re-org.
01l,03nov03,rlm  Removed wrn/coreip/ prefix from #includes for header re-org.
01k,23oct03,rlm  updated #includes for header re-org
01j,07aug03,niq  Merging from Accordion label ACCORDION_BASE6_MERGE_BASELINE
01i,23jul03,vvv  added _KERNEL protection to fix warnings (SPR #89813)
01h,09may03,vvv  included if.h
01g,01feb03,pas  merge from FreeBSD 4.7 - added hash table for IP addresses
01f,09sep02,hsh  add c++ protection
01e,28mar02,ppp  modified it for backward compatibilty with A.E. 1.1
01d,03dec01,niq  Sysctl changes for Clarinet
01c,02oct01,ppp  removing the guaed for in6_var.h
01b,18sep01,ppp  temporarily guarding in6_var.h by INET6 for the link process
01a,27aug01,hsh  Created from FreeBSD 4.3
*/


#ifndef _NETINET_IN_VAR_H_
#define _NETINET_IN_VAR_H_

#include <sys/queue.h>

#ifdef _WRS_KERNEL
#include <sys/fnv_hash.h>
#endif

#include <net/if.h>

#ifdef __cplusplus
 extern "C" {
#endif

struct in_ifrtchange_arg {
	int routeUsage;
	struct ifnet *ifp;
	int enable;
	u_char flag;/* not used currently */
};
     
/*
 * Interface address, Internet version.  One of these structures
 * is allocated for each Internet address on an interface.
 * The ifaddr structure contains the protocol-independent part
 * of the structure and is assumed to be first.
 */
struct in_ifaddr {
	struct	ifaddr ia_ifa;		/* protocol-independent info */
#define	ia_ifp		ia_ifa.ifa_ifp
#define ia_flags	ia_ifa.ifa_flags
					/* ia_{,sub}net{,mask} in host order */
	u_long	ia_net;			/* network number of interface */
	u_long	ia_netmask;		/* mask of net part */
	u_long	ia_subnet;		/* subnet number, including net */
	u_long	ia_subnetmask;		/* mask of subnet part */
	struct	in_addr ia_netbroadcast; /* to recognize net broadcasts */
	LIST_ENTRY(in_ifaddr) ia_hash;	/* entry in bucket of inet addresses */
	TAILQ_ENTRY(in_ifaddr) ia_link;	/* list of internet addresses */
	struct	sockaddr_in ia_addr;	/* reserve space for interface name */
	struct	sockaddr_in ia_dstaddr; /* reserve space for broadcast addr */
#define	ia_broadaddr	ia_dstaddr
	struct	sockaddr_in ia_sockmask; /* reserve space for general netmask */
};

struct	in_aliasreq {
	char	ifra_name[IFNAMSIZ];		/* if name, e.g. "en0" */
	struct	sockaddr_in ifra_addr;
	struct	sockaddr_in ifra_broadaddr;
#define ifra_dstaddr ifra_broadaddr
	struct	sockaddr_in ifra_mask;
};
/*
 * Given a pointer to an in_ifaddr (ifaddr),
 * return a pointer to the addr as a sockaddr_in.
 */
#define IA_SIN(ia)    (&(((struct in_ifaddr *)(ia))->ia_addr))
#define IA_DSTSIN(ia) (&(((struct in_ifaddr *)(ia))->ia_dstaddr))

#define IN_LNAOF(in, ifa) \
	((ntohl((in).s_addr) & ~((struct in_ifaddr *)(ifa)->ia_subnetmask))


#ifdef	_WRS_KERNEL
#ifndef VIRTUAL_STACK
extern	TAILQ_HEAD(in_ifaddrhead, in_ifaddr) _in_ifaddrhead;
extern	struct	ifqueue	ipintrq;  /* ip packet input queue (loopback only) */
extern  INTRQ_QJOB ipintrqJob;
#endif /* !VIRTUAL_STACK */
extern	u_char	inetctlerrmap[];

/* 
 * Hash table for IP addresses.
 */

extern	LIST_HEAD(in_ifaddrhashhead, in_ifaddr) *in_ifaddrhashtbl;

#ifndef VIRTUAL_STACK
extern	u_long in_ifaddrhmask;			/* mask for hash table */
#endif /* !VIRTUAL_STACK */

#define INADDR_NHASH_LOG2       9
#define INADDR_NHASH		(1 << INADDR_NHASH_LOG2)

#define INADDR_HASHVAL(x)	fnv_32_buf((&(x)), sizeof(x), FNV1_32_INIT)
#define INADDR_HASH(x) \
	(&in_ifaddrhashtbl[INADDR_HASHVAL(x) & in_ifaddrhmask])


/*
 * Macro for finding the interface (ifnet structure) corresponding to one
 * of our IP addresses.
 */
#define INADDR_TO_IFP(addr, ifp) \
	/* struct in_addr addr; */ \
	/* struct ifnet *ifp; */ \
{ \
	struct in_ifaddr *_ia_; \
\
	LIST_FOREACH(_ia_, INADDR_HASH((addr).s_addr), ia_hash) \
		if (IA_SIN(_ia_)->sin_addr.s_addr == (addr).s_addr) \
			break; \
	(ifp) = (_ia_ == NULL) ? NULL : _ia_->ia_ifp; \
}

/*
 * Macro for finding the internet address structure (in_ifaddr) corresponding
 * to a given interface (ifnet structure).
 */
#define IFP_TO_IA(ifp, ia) \
	/* struct ifnet *ifp; */ \
	/* struct in_ifaddr *ia; */ \
{ \
	for ((ia) = TAILQ_FIRST(&_in_ifaddrhead); \
	    (ia) != NULL && (ia)->ia_ifp != (ifp); \
	    (ia) = TAILQ_NEXT((ia), ia_link)) \
		continue; \
}

/*
 * This information should be part of the ifnet structure but we don't wish
 * to change that - as it might break a number of things
 */

struct router_info {
	struct ifnet *rti_ifp;
	int    rti_type; /* type of router which is querier on this interface */
	int    rti_time; /* # of slow timeouts since last old query */
	struct router_info *rti_next;
	u_int	rti_timer1;	/* IGMPv1 Querier Present timer */
	u_int	rti_timer2;	/* IGMPv2 Querier Present timer */
	u_int	rti_timer3;	/* IGMPv3 General Query (interface) timer */
	u_int	rti_qrv;	/* Querier Robustness Variable */
	u_int	rti_qqi;	/* Querier Interval Variable */
	u_int	rti_qri;	/* Querier Response Interval */
};

/*
 * Internet multicast address structure.  There is one of these for each IP
 * multicast group to which this host belongs on a given network interface.
 * For every entry on the interface's if_multiaddrs list which represents
 * an IP multicast group, there is one of these structures.  They are also
 * kept on a system-wide list to make it easier to keep our legacy IGMP code
 * compatible with the rest of the world (see IN_FIRST_MULTI et al, below).
 */
struct in_multi {
	LIST_ENTRY(in_multi) inm_link;	/* queue macro glue */
	struct	in_addr inm_addr;	/* IP multicast address, convenience */
	struct	ifnet *inm_ifp;		/* back pointer to ifnet */
	struct	ifmultiaddr *inm_ifma;	/* back pointer to ifmultiaddr */
	u_int	inm_timer;		/* IGMP membership report timer */
	u_int	inm_state;		/*  state of the membership */
	struct	router_info *inm_rti;	/* router info*/
	struct	in_multi_source *inm_source;	/* filtered source list */
};


#ifdef ROUTER_ALERT
/* Router alert socket list */
struct ralcb {
	LIST_ENTRY(ralcb) list;
	struct	socket *ral_socket;	/* back pointer to socket */
	struct	sockproto ral_proto;	/* protocol family, protocol */
};
#endif /* ROUTER_ALERT */

#ifdef SYSCTL_DECL
SYSCTL_DECL(_net_inet_ip);
SYSCTL_DECL(_net_inet_raw);
#else
#include <sysctlLib.h>
#ifndef VIRTUAL_STACK
SYSCTL_DECL_NODE_EXT(ip);
SYSCTL_DECL_NODE_EXT(raw);
#endif /* VIRTUAL_STACK */
#endif

#ifndef VIRTUAL_STACK
extern LIST_HEAD(in_multihead, in_multi) _in_multihead;
#ifdef ROUTER_ALERT
extern LIST_HEAD(ralcb_list_head, ralcb) ralcb_list;
#endif /* ROUTER_ALERT */
#else
#ifdef ROUTER_ALERT
LIST_HEAD(ralcb_list_head, ralcb);
#endif /* ROUTER_ALERT */
#endif /* !VIRTUAL_STACK */

/*
 * Structure used by macros below to remember position when stepping through
 * all of the in_multi records.
 */
struct in_multistep {
	struct in_multi *i_inm;
};

/*
 * Macro for looking up the in_multi record for a given IP multicast address
 * on a given interface.  If no matching record is found, "inm" is set null.
 */
#define IN_LOOKUP_MULTI(addr, ifp, inm) \
	/* struct in_addr addr; */ \
	/* struct ifnet *ifp; */ \
	/* struct in_multi *inm; */ \
do { \
	struct ifmultiaddr *ifma; \
\
	LIST_FOREACH(ifma, &((ifp)->if_multiaddrs), ifma_link) { \
		if (ifma->ifma_addr->sa_family == AF_INET \
		    && ((struct sockaddr_in *)ifma->ifma_addr)->sin_addr.s_addr == \
		    (addr).s_addr) \
			break; \
	} \
	(inm) = ifma ? ifma->ifma_protospec : 0; \
} while(0)

/*
 * Macro to step through all of the in_multi records, one at a time.
 * The current position is remembered in "step", which the caller must
 * provide.  IN_FIRST_MULTI(), below, must be called to initialize "step"
 * and get the first record.  Both macros return a NULL "inm" when there
 * are no remaining records.
 */
#define IN_NEXT_MULTI(step, inm) \
	/* struct in_multistep  step; */ \
	/* struct in_multi *inm; */ \
do { \
	if (((inm) = (step).i_inm) != NULL) \
		(step).i_inm = LIST_NEXT((step).i_inm, inm_link); \
} while(0)

#define IN_FIRST_MULTI(step, inm) \
	/* struct in_multistep step; */ \
	/* struct in_multi *inm; */ \
do { \
	(step).i_inm = LIST_FIRST(&_in_multihead); \
	IN_NEXT_MULTI((step), (inm)); \
} while(0)

struct	route;

#ifdef IGMPV3
struct in_multi *in_addmulti (struct in_addr *, struct ifnet *, u_int16_t,
                              struct sockaddr_storage *, u_int, int, int *);
void in_delmulti (struct in_multi *, u_int16_t, struct sockaddr_storage *,
                    u_int, int, int *);
#else
struct	in_multi *in_addmulti __P((struct in_addr *, struct ifnet *));
void	in_delmulti __P((struct in_multi *));
#endif /* IGMPV3 */

int	in_control __P((struct socket *, u_long, caddr_t, struct ifnet *,
			struct proc *));
void	in_rtqdrain __P((void));
_WRS_FASTTEXT
void	ip_input __P((struct mbuf *));
int	in_ifadown __P((struct ifaddr *ifa, int));
int in_ifrtchange __P((struct ifnet *ifp, int ));
void in_changeblackholes __P((struct rtentry *rt, void *xap));
BOOL	in_ifscrub __P((struct ifnet *, struct in_ifaddr *));
int	ipflow_fastforward __P((struct mbuf *));
void	ipflow_create __P((const struct route *, struct mbuf *));
void	ipflow_slowtimo __P((void));

#endif /* _WRS_KERNEL */

#ifdef __cplusplus
}
#endif
    
/* INET6 stuff */
#ifdef _WRS_KERNEL  /* Temporary workaround */
#include <netinet6/in6_var.h>
#endif /* Temporary workaround */

#endif /* _NETINET_IN_VAR_H_ */
