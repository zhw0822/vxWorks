/* vsHost.h - virtual stack data for Host Library */

/* Copyright 2000 - 2004 Wind River Systems, Inc. */

/*
modification history
--------------------
01i,19sep04,spm  cleanup for hostname initialization: move local
                 hostname storage from deleted vsHostSetup.h file
01h,04nov03,rlm  Ran batch header path update for header re-org.
01g,03nov03,rlm  Removed wrn/coreip/ prefix from #includes for header re-org.
01f,01apr03,hgo  removed h_errno
01e,18nov02,hgo  moved _targetName to vsHostSetup.h
01d,22jul02,hgo  global variables of new routines
01c,26oct01,spm  isolated virtual stack data structures (SPR #71092)
01b,16apr01,jpf  Fixing a naming problem with hostInitFlag
01a,29mar01,spm  file creation: copied from version 01a of tor2_0.open_stack
                 branch (wpwr VOB) for unified code base; changed semaphore
                 type for protection domain restrictions
*/

#ifndef __INCvshosth
#define __INCvshosth


/* includes */

#include <vxWorks.h>
#include <lstLib.h>
#include <semLib.h>
#include <sys/types.h>

/* defines */

#define HOSTNAME_LEN 128

/* typedefs */

typedef struct vs_host
    {
    /* definitions from hostLib.c */
    LIST       hostList;
    SEM_ID     hostListSem;
    BOOL       hostInitFlag;	/* FALSE = not done, TRUE = already initialized */
    char       _priv_targetName [MAXHOSTNAMELEN];   /* local hostname */
    } VS_HOST;

typedef struct vs_hostnameCache
    {
    char       _priv_targetName [MAXHOSTNAMELEN];	/* name for this target machine */
    } VS_LOCALNAME;

/* macros for hostname cache */

#define VS_HOSTNAME_DATA ((VS_LOCALNAME *)vsTbl[myStackNum]->pHostnameGlobals)

/* macros for host table */

#define VS_HOST_DATA ((VS_HOST *)vsTbl[myStackNum]->pHostGlobals)

/* Defines for the HOST TABLE globals */
#define hostList        VS_HOST_DATA->hostList
#define hostListSem     VS_HOST_DATA->hostListSem
#define hostInitFlag    VS_HOST_DATA->hostInitFlag

#endif /* __INCvshosth */
