/* vsSysctl.h - virtual stack data for IP */

/*
 * Copyright (c) 2002-2005 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01j,20jul05,vvv  added sctp_node macros
01i,20jul05,kch  added sctp_node and sctp6_node.
01h,20may05,kch  Added mip6_node for mobility support.
01g,29apr05,kch  Added pim6_node and addrctlpolicy_node.
01f,31jan05,niq  virtual stack changes for sysctl
01e,10nov03,rlm  2nd pass of include update for header re-org.
01d,05nov03,cdw  Removal of unnecessary _KERNEL guards.
01c,04nov03,rlm  Ran batch header path update for header re-org.
01b,05sep02,kal  fixed capitalisation of pSysctlCtxList
01a,09juk02,ger  file creation:  
*/

#ifndef __INCvsSysctlh
#define __INCvsSysctlh


/* includes */

#include "vxWorks.h"
/*#include <protos/ip6protosw.h>
#include <netinet6/ip6_var.h>
#include <netinet6/raw_ip6.h>
#include <netinet6/in6_prefix.h>*/
#include <net/domain.h>
#include <sys/sysctl.h>

/* defines */

/* typedefs */

typedef struct vsSysctl
    {
    /* definitions for networking sysctl variables */
        struct sysctl_ctx_list    netSysctlCtxList;
	struct sysctl_ctx_list *  pNetSysctlCtxList;
        struct sysctl_oid_list *  net_node_children;

    /* definitions from various networking files */

        struct sysctl_oid * net_node;
        struct sysctl_oid * inet_node;
        struct sysctl_oid * ip_node;
        struct sysctl_oid * tcp_node;
        struct sysctl_oid * syncache_node;
        struct sysctl_oid * udp_node;
        struct sysctl_oid * portrange_node;
        struct sysctl_oid * icmp_node;
        struct sysctl_oid * raw_node;
        struct sysctl_oid * routetable_node;
        struct sysctl_oid * ether_inet_node;
        struct sysctl_oid * igmp_node;
        struct sysctl_oid * nameinfo_node;
        struct sysctl_oid * hostlib_node;
        struct sysctl_oid * inetlib_node;
        struct sysctl_oid * link_node;
        struct sysctl_oid * generic_node;
        struct sysctl_oid * system_node;
        struct sysctl_oid * ifdata_node;
        struct sysctl_oid * ether_node;
        struct sysctl_oid * addrctlpolicy_node;

#ifdef PIM
        struct sysctl_oid * pim_node;
#endif /* PIM */

#ifdef SCTP
        struct sysctl_oid * sctp_node;
#endif /* SCTP */

#ifdef INET6
        struct sysctl_oid * inet6_node;
        struct sysctl_oid * ip6_node;
        struct sysctl_oid * icmp6_node;
        struct sysctl_oid * nd6_drlist_node;
        struct sysctl_oid * tcp6_node;
        struct sysctl_oid * nd6_prlist_node;
        struct sysctl_oid * udp6_node;

#ifdef WRS_PIM6
        struct sysctl_oid * pim6_node;
#endif /* WRS_PIM6 */

#ifdef SCTP
        struct sysctl_oid * sctp6_node;
#endif /* SCTP */

#ifdef MIP6
        struct sysctl_oid * mip6_node;
#endif /* MIP6 */

#endif /* INET6 */
    } VS_SYSCTL;

/* macros */

#define VS_SYSCTL_DATA ((VS_SYSCTL *)vsTbl[myStackNum]->pSysctlGlobals)


#define netSysctlCtxList     VS_SYSCTL_DATA->netSysctlCtxList
#define pNetSysctlCtxList    VS_SYSCTL_DATA->pNetSysctlCtxList
#define net_node_children    VS_SYSCTL_DATA->net_node_children

#define net_node             VS_SYSCTL_DATA->net_node
#define inet_node            VS_SYSCTL_DATA->inet_node
#define ip_node              VS_SYSCTL_DATA->ip_node
#define tcp_node             VS_SYSCTL_DATA->tcp_node
#define syncache_node        VS_SYSCTL_DATA->syncache_node
#define udp_node             VS_SYSCTL_DATA->udp_node
#define portrange_node       VS_SYSCTL_DATA->portrange_node
#define icmp_node            VS_SYSCTL_DATA->icmp_node
#define raw_node             VS_SYSCTL_DATA->raw_node
#define routetable_node      VS_SYSCTL_DATA->routetable_node
#define ether_inet_node      VS_SYSCTL_DATA->ether_inet_node
#define igmp_node            VS_SYSCTL_DATA->igmp_node
#define nameinfo_node        VS_SYSCTL_DATA->nameinfo_node
#define hostlib_node         VS_SYSCTL_DATA->hostlib_node
#define inetlib_node         VS_SYSCTL_DATA->inetlib_node
#define link_node            VS_SYSCTL_DATA->link_node
#define generic_node         VS_SYSCTL_DATA->generic_node
#define system_node          VS_SYSCTL_DATA->system_node
#define ifdata_node          VS_SYSCTL_DATA->ifdata_node
#define ether_node           VS_SYSCTL_DATA->ether_node
#define addrctlpolicy_node   VS_SYSCTL_DATA->addrctlpolicy_node

#ifdef PIM
#define pim_node             VS_SYSCTL_DATA->pim_node
#endif /* PIM */

#ifdef SCTP
#define sctp_node            VS_SYSCTL_DATA->sctp_node
#endif

#ifdef INET6
#define inet6_node           VS_SYSCTL_DATA->inet6_node
#define ip6_node             VS_SYSCTL_DATA->ip6_node
#define icmp6_node           VS_SYSCTL_DATA->icmp6_node
#define tcp6_node            VS_SYSCTL_DATA->tcp6_node
#define udp6_node            VS_SYSCTL_DATA->udp6_node
#define nd6_drlist_node      VS_SYSCTL_DATA->nd6_drlist_node
#define nd6_prlist_node      VS_SYSCTL_DATA->nd6_prlist_node

#ifdef WRS_PIM6
#define pim6_node            VS_SYSCTL_DATA->pim6_node
#endif /* WRS_PIM6 */

#ifdef SCTP
#define sctp6_node           VS_SYSCTL_DATA->sctp6_node
#endif

#ifdef MIP6
#define mip6_node            VS_SYSCTL_DATA->mip6_node
#endif /* MIP6 */

#endif /* INET6 */

#endif /* __INCvsSysctlh */

