/* vsMip6.h - virtual stack data for mobility */

/*
 * Copyright (c) 2005 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01a,20may05,kch  written.
*/

#ifndef __INCvsMip6h
#define __INCvsMip6h

#ifdef __cplusplus
extern "C" {
#endif

#include <netinet6/mip6.h>
#include <net/if_mip.h>

/* includes */

typedef struct vsMip6
    {
    /* definition from mip6Lib.c */
    struct domain mipdomain;
    struct protosw mipsw[MIP_PROTO_NUM_MAX];
    
    /* definitions from mip6.c */
    struct mip6_bc_internal *mip6_bc_hash[MIP6_BC_HASH_SIZE];
    struct mip6_bc_list mip6_bc_list;
    struct mip6stat mip6stat;
    u_int8_t mip6_nodetype;
    int mip6ctl_use_ipsec;
    int mip6ctl_debug;
    int mip6ctl_rr_hint_ppslim;

#if NMIP > 0
    /* definitions from mip6.c */
    int mip6_rr_hint_pps_count;
    struct timeval mip6_rr_hint_ppslim_last;

    /* definitions from if_mip.c */
    struct mip_softc_list _mip_softc_list;
#endif /* NMIP > 0 */

    } VS_MIP6;

/* macros */

#define VS_MIP6_DATA ((VS_MIP6 *)vsTbl[myStackNum]->pMip6Globals)

#define mipdomain                   VS_MIP6_DATA->mipdomain
#define mipsw                       VS_MIP6_DATA->mipsw

#define mip6_bc_hash                VS_MIP6_DATA->mip6_bc_hash
#define mip6_bc_list                VS_MIP6_DATA->mip6_bc_list
#define mip6stat                    VS_MIP6_DATA->mip6stat
#define mip6_nodetype               VS_MIP6_DATA->mip6_nodetype
#define mip6ctl_use_ipsec           VS_MIP6_DATA->mip6ctl_use_ipsec
#define mip6ctl_debug               VS_MIP6_DATA->mip6ctl_debug
#define mip6ctl_rr_hint_ppslim      VS_MIP6_DATA->mip6ctl_rr_hint_ppslim

#if NMIP > 0
#define mip6_rr_hint_pps_count      VS_MIP6_DATA->mip6_rr_hint_pps_count
#define mip6_rr_hint_ppslim_last    VS_MIP6_DATA->mip6_rr_hint_ppslim_last
#define _mip_softc_list             VS_MIP6_DATA->_mip_softc_list
#endif /* NMIP > 0 */

#ifdef __cplusplus
}
#endif

#endif /* __INCvsMip6h */


