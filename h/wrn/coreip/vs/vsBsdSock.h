/* vsBsdSock.h - virtual stack data for Unix BSD 4.4 compatible sockets */

/*
 * Copyright (c) 2002-2005 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01g,21jul05,dlk  Remove origStackNum (now stored on task execution stack).
01f,05mar05,dlk  Added _priv_soLingerForever.
01e,21jul04,spm  enabled socket operations from different virtual stacks
01d,05nov03,cdw  Removal of unnecessary _KERNEL guards.
01c,04nov03,rlm  Ran batch header path update for header re-org.
01b,03feb03,hgo  removed maxsockets
01a,29aug02,hgo  creation
*/

#ifndef __INCvsBsdSockh
#define __INCvsBsdSockh

/* includes */
#include <net/socketvar.h>

/* typedefs */

    /* Encapsulated (former) globals for SOCKET. */

typedef struct vsBsdSock
    {
    /* defined in uipc_sock.c */
    so_gen_t             _so_gencnt;           /* generation count for sockets */
    int                  _priv_somaxconn;
    BOOL		 _priv_soLingerForever;

    /* defined in uipc_sock2.c */
    u_long               _sb_max;                         
    u_long               _priv_sb_efficiency;  /* parameter for sbreserve() */
    int                  _priv_rnd;
    struct timeval       _priv_old_runtime;
    unsigned int         _priv_cur_cnt;
    unsigned int         _priv_old_cnt;
    } VS_BSDSOCK;

/* macros for "non-private" global variables */

#define VS_BSDSOCK_DATA ((VS_BSDSOCK *)vsTbl[myStackNum]->pBsdSockGlobals)

#define so_gencnt           VS_BSDSOCK_DATA->_so_gencnt
#define sb_max              VS_BSDSOCK_DATA->_sb_max

#endif /* __INCvsBsdSockh */
