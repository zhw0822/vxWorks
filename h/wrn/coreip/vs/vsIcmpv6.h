/* vsIcmpv6.h - virtual stack data for ICMPv6 */

/* Copyright 2000 - 2001 Wind River Systems, Inc. */

/*
modification history
--------------------
01f,17may05,kch  Removed unused priv_icmp6_reflect_rt.
01e,11nov03,cdw  Removal of unnecessary _KERNEL guards.
01d,10nov03,rlm  2nd pass of include update for header re-org.
01c,04nov03,rlm  Ran batch header path update for header re-org.
01b,28aug02,kal  fixed location of icmp6.h
01a,18jun02,ant  written
*/

#ifndef __INCvsIcmpv6h
#define __INCvsIcmpv6h


/* includes */

#include <netinet6/icmp6.h>

/* defines */

/* typedefs */

typedef struct vs_icmpv6_global_data
    {
    /* definitions from icmp6.c */
    struct icmp6stat _icmp6stat;
    int priv_icmp6errpps_count;
    struct timeval priv_icmp6errppslim_last;

    /* definitions from in6_proto.c */
    int	icmp6_rediraccept;
    int	icmp6_redirtimeout;
    int icmp6errppslim;
    int icmp6nodeinfo;

    /* definition from icmp6_redirect_diag() */
     char priv_buf[1024];

    } VS_ICMPV6;


/* macros */

#define VS_ICMPV6_DATA ((VS_ICMPV6 *)vsTbl[myStackNum]->pIcmpv6Globals)

#define _icmp6stat          VS_ICMPV6_DATA->_icmp6stat
#define icmp6_rediraccept   VS_ICMPV6_DATA->icmp6_rediraccept
#define icmp6_redirtimeout  VS_ICMPV6_DATA->icmp6_redirtimeout
#define icmp6errppslim      VS_ICMPV6_DATA->icmp6errppslim
#define icmp6nodeinfo       VS_ICMPV6_DATA->icmp6nodeinfo

#endif /* __INCvsIcmpv6h */
