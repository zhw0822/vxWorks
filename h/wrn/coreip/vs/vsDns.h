/* vsDns.h - virtual stack data for DNS */

/* Copyright 2002 - 2004 Wind River Systems, Inc. */

/*
modification history
--------------------
01f,24aug04,vvv  virtualize dnsMutexSem
01e,17mar04,spm  removed obsolete variable for (deleted) ORG_RESOLVER code
01d,05nov03,cdw  Removal of unnecessary _KERNEL guards.
01c,04nov03,rlm  Ran batch header path update for header re-org.
01b,28aug02,kal  fixed location of !_KERNEL headers
01a,03jul02,vlk  created
*/
#ifndef __INCvsDnsh
#define __INCvsDnsh


/* includes */
#include <resolv/nameser.h> 	  /* for MAXDNAME definition */
#include <resolv/resolv.h>  	  /* for __res_state structure */
#include <resolvLib.h>  /* for RESOLV_PARAMS_S structure */

typedef struct vsDns {

    /* defined in res_debug.c */
    
    char priv_ntos_unname[20];
    char priv_ntop_unname[20];

    /* defined in resolvLib.c */ 
    
    BOOL debugFlag;    

    struct __res_state	_res;
    FUNCPTR	pdnsDebugFunc;
    RESOLV_PARAMS_S	priv_resolvParams;

    SEM_ID dnsMutexSem;
	
    } VS_DNS;
	

/* macros for "non-private" global variables */

#define VS_DNS_DATA ((VS_DNS *)vsTbl[myStackNum]->pDnsGlobals)

#define debugFlag		VS_DNS_DATA->debugFlag
#define _res			VS_DNS_DATA->_res
#define pdnsDebugFunc		VS_DNS_DATA->pdnsDebugFunc
#define dnsMutexSem             VS_DNS_DATA->dnsMutexSem



#endif /* __INCvsDnsh */
