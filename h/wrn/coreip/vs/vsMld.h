/* vsMld.h - virtual stack data for MLD6 */

/*
 * Copyright (c) 2000-2005 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01f,27apr05,rp   virtualization changes
01e,10nov03,rlm  2nd pass of include update for header re-org.
01d,05nov03,cdw  Removal of unnecessary _KERNEL guards.
01c,04nov03,rlm  Ran batch header path update for header re-org.
01b,28aug02,kal  fixed ml6_var.h location
01a,27aug02,kal  created
*/

#ifndef __INCvsMldh
#define __INCvsMldh


/* includes */

#include <netinet/in.h>
#include <netinet/in_var.h>
#include <netinet6/ip6.h>
#include <netinet6/ip6_var.h>
#include <protos/mld6_var.h>

/* defines */

/* typedefs */

typedef struct vs_mld
    {

    /* common from mld6.c and mldv2.c */

    /*struct in6_multihead in6_multihead;*/
    struct router6_info         *Head6;
    struct ip6_pktopts          ip6_opts;
    struct sockaddr_in6         *all_nodes_linklocal;
    struct sockaddr_in6         *all_routers_linklocal;
    struct sockaddr_in6         all_nodes_linklocal0;
    struct sockaddr_in6         all_routers_linklocal0;
	u_int8_t                    hbh_buf[8];

    /* mldv2.c only */
#ifdef MLDV2
    int                         mldmaxsrcfilter;
    int                         mldsomaxsrc;
    int                         mldversion;
    int                         mld_group_timers_are_running;
    int                         mld_interface_timers_are_running;
    int                         mld_state_change_timers_are_running;
    struct sockaddr_in6         *all_v2routers_linklocal;
    struct sockaddr_in6         all_v2routers_linklocal0;
#endif /* MLDV2 */

    } VS_MLD;

/* macros */

#define VS_MLD_DATA ((VS_MLD *)vsTbl[myStackNum]->pMldGlobals)

#define Head6                   VS_MLD_DATA->Head6
#define ip6_opts                VS_MLD_DATA->ip6_opts
#define all_nodes_linklocal     VS_MLD_DATA->all_nodes_linklocal
#define all_routers_linklocal   VS_MLD_DATA->all_routers_linklocal
#define all_nodes_linklocal0    VS_MLD_DATA->all_nodes_linklocal0
#define all_routers_linklocal0  VS_MLD_DATA->all_routers_linklocal0
#define hbh_buf                 VS_MLD_DATA->hbh_buf

#ifdef MLDV2
#define mldmaxsrcfilter         VS_MLD_DATA->mldmaxsrcfilter
#define mldsomaxsrc             VS_MLD_DATA->mldsomaxsrc
#define mldversion              VS_MLD_DATA->mldversion
#define mld_group_timers_are_running \
                      VS_MLD_DATA->mld_group_timers_are_running
#define mld_interface_timers_are_running \
                      VS_MLD_DATA->mld_interface_timers_are_running
#define mld_state_change_timers_are_running \
                      VS_MLD_DATA->mld_state_change_timers_are_running

#define all_v2routers_linklocal  VS_MLD_DATA->all_v2routers_linklocal
#define all_v2routers_linklocal0  VS_MLD_DATA->all_v2routers_linklocal0
#endif /* MLDV2 */

#endif /* __INCvsMldh */

