/* vsLog.h - virtual stack data for applUtilLib */

/* Copyright 2004 Wind River Systems, Inc. */

/*
modification history
--------------------
01a,07sep04,dlk  Written.
*/

#ifndef __INCvsLogh
#define __INCvsLogh

#include <applUtilLib.h>

/* defines */

typedef struct _VS_LOG
    {
    BOOL _loggerInitialized;
    CATEGORY_INFO _catInfo [LOG_NUMCATS];
    } VS_LOG;

extern VS_LOG * vsLogTbl[];

/* macros */

#define VS_LOG_DATA (vsLogTbl[myStackNum])

#define loggerInitialized	VS_LOG_DATA->_loggerInitialized
#define catInfo			VS_LOG_DATA->_catInfo

#endif /* __INCvsLogh */

