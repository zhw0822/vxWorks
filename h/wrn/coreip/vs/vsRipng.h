/* vsRipng.h - Virtual stack data for RIPng */

/*
 * Copyright (c) 2003-2005 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01h,20jun05,niq  Kame merge
01g,25mar03,kal  code review modications - copyright and file header
01f,10jan03,kal  add split horizon and restricted neighbor variables
01e,11nov02,kal  added ripng_exit_sock and ripng_exit_port
01d,18oct02,kal  added support for triggered update timer
01c,09oct02,kal  add ripngStopFlag
01b,05sep02,hgo  own vsRipTbl
01a,09juk02,ger  file creation: based on IPv4 implementation
*/

#ifndef __INCvsRipngh
#define __INCvsRipngh


/* defines */

typedef struct vsripng
    {
    /* definitions from route6d.c  */
    	struct ifc 	*ripng_ifc;
	int		ripng_nifc;		/* number of valid ifc's */
	struct ifc 	**ripng_index2ifc;
	int		ripng_nindex2ifc;
	struct ifc 	*ripng_loopifcp;	/* pointing to loopback */
	fd_set	        *ripng_sockvecp; /* vector to select() for receiving */
	fd_set	        *ripng_recvecp;
	int	        ripng_fdmasks;
	int	        ripng_maxfd;		/* maximum fd for select() */
	int 		ripng_rtsock;		/* the routing socket */
	int		ripng_ripsock;	/* socket to send/receive RIP datagram */
	int             ripng_exit_sock;    /* socket used by ripngStop() */
	int             ripng_exit_port;    /* port used by ripngStop() */

	struct rip6 	*ripngbuf;	/* packet buffer for sending */
	struct riprt 	*ripng_riprt ;

	int		ripng_dflag ;	/* debug flag */
	int		ripng_qflag ;	/* quiet flag */
	int		ripng_nflag ;	/* don't update kernel routing table */
	int		ripng_aflag ;	/* age out even the statically defined routes */
	int             ripng_pflag ;   /* disable poison reverse for new interfaces */
	int	        ripng_hflag ;	/* disable split horizon for new interfaces */
	int		ripng_lflag ;	/* exchange site local routes */
	int		ripng_sflag ;	/* announce static routes w/ split horizon */
	int		ripng_Sflag ;	/* announce static routes to every interface */
	int             ripng_rflag ;    /* restricted neighor list */
	unsigned long 	routetag ;	/* route tag attached on originating case */

	char		*ripng_filter[MAXARGS];
	int		ripng_filtertype[MAXARGS];
	int		ripng_nfilter ;

	char            *ripng_ifhoriz[MAXARGS];
	int             ripng_ifhoriztype[MAXARGS];
	int             ripng_nifhoriz;

	struct nb_list *ripng_nb_list_root;
    
	pid_t		ripng_pid;

	struct sockaddr_storage ripngsin;
    
	int             ripng_need_trigger;
	time_t		ripng_nexttrigger;
	time_t		ripng_nextupdate;

	int		priv_ripng_seq ;

/* Semaphore / Watchdog / Task system */
	SEM_ID		ripngTimerSem, 
			ripngMutexSem;
	WDOG_ID 	ripngTimerDog;
	WDOG_ID         ripngTriggerDog;
	BOOL 		ripngTimerSemInit, 	
			ripngMutexSemInit, 
			ripngTimerDogInit,
                        ripngTriggerDogInit;
	int 		ripngDogTask, 
			ripngTask;	
	int 		priv_ripng_nrt; 
	struct netinfo6 *priv_ripng_np;
	
	BOOL ripngStopFlag;
    
    } VS_RIPNG;

extern VS_RIPNG *vsRipngTbl[];

/* macros */

#define VS_RIPNG_DATA (vsRipngTbl[myStackNum])

#define ripng_ifc     		VS_RIPNG_DATA->ripng_ifc
#define ripng_nifc    		VS_RIPNG_DATA->ripng_nifc
#define ripng_index2ifc     	VS_RIPNG_DATA->ripng_index2ifc
#define ripng_nindex2ifc     	VS_RIPNG_DATA->ripng_nindex2ifc
#define ripng_loopifcp     	VS_RIPNG_DATA->ripng_loopifcp
#define ripng_ifc     		VS_RIPNG_DATA->ripng_ifc
#define ripng_sockvecp     	VS_RIPNG_DATA->ripng_sockvecp
#define ripng_recvecp     	VS_RIPNG_DATA->ripng_recvecp
#define ripng_fdmasks     	VS_RIPNG_DATA->ripng_fdmasks
#define ripng_maxfd     	VS_RIPNG_DATA->ripng_maxfd
#define ripng_rtsock     	VS_RIPNG_DATA->ripng_rtsock
#define ripng_ripsock     	VS_RIPNG_DATA->ripng_ripsock
#define ripng_exit_sock     VS_RIPNG_DATA->ripng_exit_sock
#define ripng_exit_port     VS_RIPNG_DATA->ripng_exit_port
#define ripngbuf     		VS_RIPNG_DATA->ripngbuf
#define ripng_riprt     	VS_RIPNG_DATA->ripng_riprt
#define ripng_dflag     	VS_RIPNG_DATA->ripng_dflag
#define ripng_qflag     	VS_RIPNG_DATA->ripng_qflag
#define ripng_nflag     	VS_RIPNG_DATA->ripng_nflag
#define ripng_aflag     	VS_RIPNG_DATA->ripng_aflag
#define ripng_pflag     	VS_RIPNG_DATA->ripng_pflag
#define ripng_hflag     	VS_RIPNG_DATA->ripng_hflag
#define ripng_lflag     	VS_RIPNG_DATA->ripng_lflag
#define ripng_sflag     	VS_RIPNG_DATA->ripng_sflag
#define ripng_Sflag     	VS_RIPNG_DATA->ripng_Sflag
#define ripng_rflag     	VS_RIPNG_DATA->ripng_rflag
#define routetag     		VS_RIPNG_DATA->routetag
#define ripng_filter     	VS_RIPNG_DATA->ripng_filter
#define ripng_filtertype  	VS_RIPNG_DATA->ripng_filtertype
#define ripng_nfilter     	VS_RIPNG_DATA->ripng_nfilter
#define ripng_ifhoriz     	VS_RIPNG_DATA->ripng_ifhoriz
#define ripng_ifhoriztype  	VS_RIPNG_DATA->ripng_ifhoriztype
#define ripng_nifhoriz     	VS_RIPNG_DATA->ripng_nifhoriz
#define ripng_nb_list_root  VS_RIPNG_DATA->ripng_nb_list_root
#define ripng_pid    		VS_RIPNG_DATA->ripng_pid
#define ripngsin     		VS_RIPNG_DATA->ripngsin
#define ripng_need_trigger  VS_RIPNG_DATA->ripng_need_trigger
#define ripng_nexttrigger   VS_RIPNG_DATA->ripng_nexttrigger
#define ripng_nextupdate    VS_RIPNG_DATA->ripng_nextupdate
#define ripng_seq     		VS_RIPNG_DATA->ripng_seq
#define ripngTimerSem    	VS_RIPNG_DATA->ripngTimerSem
#define ripngMutexSem     	VS_RIPNG_DATA->ripngMutexSem
#define ripngTimerDog    	VS_RIPNG_DATA->ripngTimerDog
#define ripngTriggerDog    	VS_RIPNG_DATA->ripngTriggerDog
#define ripngTimerSemInit     	VS_RIPNG_DATA->ripngTimerSemInit
#define ripngMutexSemInit     	VS_RIPNG_DATA->ripngMutexSemInit
#define ripngTimerDogInit     	VS_RIPNG_DATA->ripngTimerDogInit
#define ripngTriggerDogInit     VS_RIPNG_DATA->ripngTriggerDogInit
#define ripngStopFlag       VS_RIPNG_DATA->ripngStopFlag
#define ripngDogTask     	VS_RIPNG_DATA->ripngDogTask
#define ripngTask     		VS_RIPNG_DATA->ripngTask



#endif /* __INCvsripngh */

