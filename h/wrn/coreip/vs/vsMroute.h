/* vsMroute.h - virtual stack data for mroute */

/* Copyright 2000 - 2001 Wind River Systems, Inc. */

/*
modification history
--------------------
01e,05nov04,rae  Obsolete.  Correct header is vsMcast.h (SPR #103780)
01d,11aug04,niq  VS data structure reorganization
01c,05nov03,cdw  Removal of unnecessary _KERNEL guards.
01b,04nov03,rlm  Ran batch header path update for header re-org.
01a,01jul02,ant  written
*/

/* This header is obsolete, and is liable to be removed.  Do not use. */

#ifndef __INCvsMrouteh
#define __INCvsMrouteh

#include "netinet/vsMcast.h"

#endif /* __INCvsMrouteh */

