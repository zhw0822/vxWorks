/* vsIf.h - virtual stack data for interface layer */

/* Copyright 2001 - 2002 Wind River Systems, Inc. */

/*
modification history
--------------------
01d,26sep02,wie  added timeout handle
01c,06sep02,kal  took out if_init_done
01b,27aug02,ism  added global function pointers
01a,26aug02,ism  created
*/

#ifndef __INCvsIfh
#define __INCvsIfh

typedef struct vs_if_global_data {

    /* from if.c */
    struct ifnethead  ifnet_head;
    int               ifqmaxlen;
    int               if_indexlim;
    int               last_if_index;
    struct ifaddr **  ifnet_addrs;
    struct ifnet **   ifindex2ifnet;
    FUNCPTR           _protoBindStatusGet;

    /* from if_loop.c */
    struct ifnet *    loif;
    int               nloop;

    /* from ifname.c */
    int               ifxs;

    /* from ipProto.c */
    FUNCPTR  _m2SetIfLastChange;
    FUNCPTR  _m2IfTableUpdate;
	
	void *priv_if_slowtimoHandle;

    } VS_IF;

#define VS_IF_DATA ((VS_IF *)vsTbl[myStackNum]->pIfGlobals)

#define ifnet_head          VS_IF_DATA->ifnet_head
#define ifqmaxlen           VS_IF_DATA->ifqmaxlen
#define if_indexlim         VS_IF_DATA->if_indexlim
#define last_if_index       VS_IF_DATA->last_if_index
#define ifnet_addrs         VS_IF_DATA->ifnet_addrs
#define ifindex2ifnet       VS_IF_DATA->ifindex2ifnet
#define _protoBindStatusGet VS_IF_DATA->_protoBindStatusGet

#define loif                VS_IF_DATA->loif
#define nloop               VS_IF_DATA->nloop

#define ifxs                VS_IF_DATA->ifxs

#define _m2SetIfLastChange  VS_IF_DATA->_m2SetIfLastChange
#define _m2IfTableUpdate    VS_IF_DATA->_m2IfTableUpdate

#endif
