/* vsNd6.h - virtual stack data for Neighbor Discovery */

/*
 * Copyright (c) 2000-2005 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01j,12may05,rp   added _nd6_maxqueuelen, removed nd6_defif_installed
01i,03may05,kch  Fixed sysctl variables virtualization (SPR#108823).
01h,28apr05,kch  Fixed build problem. Added nd6_defif_installed variable to
                 VS_ND6.
01g,19apr05,rp   merged from comp_wn_ipv6_mld_interim-dev
01f,05nov03,cdw  Removal of unnecessary _KERNEL guards.
01e,04nov03,rlm  Ran batch header path update for header re-org.
01d,10mar03,nee  consolidate nd6_ifinfo_indexlim and if_indexlim
01c,28aug02,kal  fixed typo in includes
01b,21jun02,hgo  created
*/

#ifndef __INCvsNd6h
#define __INCvsNd6h

#include <netinet6/nd6.h>
#include <netinet/in.h>
#include <sys/callout.h>
#include <net/if_var.h>

/* typedefs */

    /* Encapsulated (former) globals for ND6 */

typedef struct vsNd6
    {
    /* defined in nd6.c */

    int 		_nd6_prune;              /* walk list every 1 seconds */
    int                 _nd6_delay;              /* delay first probe time 5 second */
    int 		_nd6_umaxtries;          /* maximum unicast query */
    int                 _nd6_mmaxtries;          /* maximum muliticast query */
    int                 _nd6_useloopback;        /* use loopback interface for local traffic */
    int                 _nd6_gctimer;            /* 1day: garbage collection timer */
    int                 _priv_nd6_maxndopt;      /* max # of ND options allowed */
    int                 _nd6_maxnudhint;         /* max # of subsequent upper layer hints */
    int                 _nd6_maxqueuelen;         /* max # of packets cached in unresolved ND entries */
    int                 _nd6_debug;
    int                 _priv_nd6_inuse;         /* # of ND entries */
    struct llinfo_nd6   _llinfo_nd6;             /* linked list ND entries */
    struct nd_drhead    _nd_defrouter;
    struct nd_prhead    _nd_prefix;    /* was _nd_prefix */
    int                 _nd6_recalc_reachtm_interval;
    struct sockaddr_in6 _priv_all1_sa;
    struct callout      _nd6_slowtimo_ch;
    struct callout      _nd6_timer_ch;
    int                 _priv_nd6_init_done;

    /* defined in nd6_nbr.c */

    struct dadq_head    _priv_dadq;
    int                 _priv_dad_init;

    /* defined in nd6_rtr.c */

    struct ifnet       *_nd6_defifp;
    int                 _nd6_defifindex;
    int                 _ip6_use_tempaddr;
    int                 _ip6_desync_factor;
    u_int32_t           _ip6_temp_preferred_lifetime;
    u_int32_t           _ip6_temp_valid_lifetime;
    int                 _ip6_temp_regen_advance;
    } VS_ND6;

/* macros for "non-private" global variables */

#define VS_ND6_DATA ((VS_ND6 *)vsTbl[myStackNum]->pNd6Globals)

#define _nd6_prune          VS_ND6_DATA->_nd6_prune
#define _nd6_delay          VS_ND6_DATA->_nd6_delay
#define _nd6_umaxtries      VS_ND6_DATA->_nd6_umaxtries
#define _nd6_mmaxtries      VS_ND6_DATA->_nd6_mmaxtries
#define _nd6_useloopback    VS_ND6_DATA->_nd6_useloopback
#define nd6_gctimer         VS_ND6_DATA->_nd6_gctimer
#define _nd6_maxnudhint     VS_ND6_DATA->_nd6_maxnudhint
#define _nd6_maxqueuelen    VS_ND6_DATA->_nd6_maxqueuelen
#define _nd6_debug          VS_ND6_DATA->_nd6_debug
#define _llinfo_nd6         VS_ND6_DATA->_llinfo_nd6
#define _nd_defrouter       VS_ND6_DATA->_nd_defrouter
#define _nd_prefix          VS_ND6_DATA->_nd_prefix
#define nd6_recalc_reachtm_interval    VS_ND6_DATA->_nd6_recalc_reachtm_interval
#define nd6_slowtimo_ch     VS_ND6_DATA->_nd6_slowtimo_ch
#define nd6_timer_ch        VS_ND6_DATA->_nd6_timer_ch

#define nd6_defifp          VS_ND6_DATA->_nd6_defifp
#define nd6_defifindex      VS_ND6_DATA->_nd6_defifindex
#define ip6_use_tempaddr    VS_ND6_DATA->_ip6_use_tempaddr
#define ip6_desync_factor   VS_ND6_DATA->_ip6_desync_factor
#define ip6_temp_preferred_lifetime    VS_ND6_DATA->_ip6_temp_preferred_lifetime
#define ip6_temp_valid_lifetime        VS_ND6_DATA->_ip6_temp_valid_lifetime
#define ip6_temp_regen_advance         VS_ND6_DATA->_ip6_temp_regen_advance

#endif /* __INCvsNd6h */
