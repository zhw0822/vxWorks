/* vsRarp.h - virtual stack data for RARP */

/* Copyright 2002 - Wind River Systems, Inc. */
/*
modification history
--------------------
01a,16jul02,vlk  created
*/

#ifndef __INCvsRarph
#define __INCvsRarph


/* defines */

/* typedefs */

typedef struct vsRarp
    {
    /* definitions from rarpLib.c */
    SEM_ID    priv_revarpInProgress;
    RARPSTATE priv_rarpState;
    } VS_RARP;


/* macros for "non-private" global variables */

#define VS_RARP_DATA ((VS_RARP *)vsTbl[myStackNum]->pRarpGlobals)



#endif /* __INCvsRarph */

