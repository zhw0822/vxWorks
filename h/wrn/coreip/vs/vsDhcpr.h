/* vsDhcpr.h - virtual stack data for DHCP Relay Agent routines */

/* Copyright 2003 Wind River Systems, Inc. */

/*
modification history
--------------------
01e,05nov03,cdw  Removal of unnecessary _KERNEL guards.
01d,04nov03,rlm  Ran batch header path update for header re-org.
01c,24mar03,ant  three priv_dhcprTaskXxx variables removed from VS_DHCPR
01b,13nov02,ant  priv_dhcpfilter removed from vs_dhcpr_global_data
01a,18jul02,ant  created from vsDhcps.h
*/

#ifndef __INCvsdhcprh
#define __INCvsdhcprh

#include <net/bpf.h>
#include <dhcprLib.h>

typedef struct vs_dhcpr_global_data
    {
    /* Globals in dhcprLib.c */
    int dhcpSPort; 			/* Port used by DHCP servers */
    int dhcpCPort; 			/* Port used by DHCP clients */
    int dhcpMaxHops;    		/* Hop limit before message is discarded. */
    int dhcprBufSize; 			/* Size of buffer for BPF devices */
    char * pDhcprSendBuf; 		/* Buffer for transmitting messages */
    BOOL priv_dhcprInitialized;
    struct if_info *dhcprIntfaceList;
    struct bpf_program priv_dhcpread;

    /* Globals in dhcpr.c */
    struct msg dhcprMsgIn;

    } VS_DHCPR;


/* Macro */

#define VS_DHCPR_DATA ((VS_DHCPR *)vsTbl[myStackNum]->pDhcprGlobals)

/* Defines for the dhcp routine globals */

/* Globals in dhcprLib.c */
#define dhcpSPort     		VS_DHCPR_DATA->dhcpSPort
#define dhcpCPort     		VS_DHCPR_DATA->dhcpCPort
#define dhcpMaxHops   		VS_DHCPR_DATA->dhcpMaxHops
#define dhcprBufSize     	VS_DHCPR_DATA->dhcprBufSize
#define pDhcprSendBuf     	VS_DHCPR_DATA->pDhcprSendBuf
#define dhcprIntfaceList     	VS_DHCPR_DATA->dhcprIntfaceList

/* Globals in dhcpr.c */
#define dhcprMsgIn     		VS_DHCPR_DATA->dhcprMsgIn

#endif /* __INCvsdhcprh */

