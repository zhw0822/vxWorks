/* vsIp6.h - virtual stack data for IP */

/*
 * Copyright (c) 2000-2005 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01t,11aug05,vvv  fixed build error
01s,11aug05,vvv  removed include for in6_prefix.h
01r,02aug05,dlk  Kame merge of udp6_usrreq.c, removed udp6_last_in6pcb.
01q,25jul05,dlk  Restore ip6qmaxlen, add ip6intrq, ip6intrqJob for loopback.
01p,10may05,kch  Added ip6_mcast_pmtu.
01o,28apr05,kch  Added ip6_use_defzone, sid_default, defaultaddrpolicy, 
                 ip6_prefer_tempaddr, addrsel_policytab and udp6_last_in6pcb.
01m,26apr05,kch  Move ip6_mrouter definition here from VS_IP_MROUTE6. Also
                 added IPv6 multicast router hooks and scope6_var.h include.
01l,19apr05,rp   merged from comp_wn_ipv6_mld_interim-dev
01k,12may04,sar  Added fields for IPFW routines.
01j,11nov03,cdw  Removal of unnecessary _KERNEL guards.
01i,10nov03,rlm  2nd pass of include update for header re-org.
01h,04nov03,rlm  Ran batch header path update for header re-org.
01g,11dec02,sar  Virtualize the external hooks
01f,15oct02,niq  Changes for Accordion's new RTM
01e,09oct02,spm  updated for conversion to Patricia tree RIB interface:
                 moved rmx6 untimeout to cleanup routine
01d,26sep02,wie  timeout handles for rmx6
01c,05sep02,kal  added ip6_mapped_addr_on
01b,04sep02,kal  renamed rtq_xxx -> rmx6_rtq_xxx
01a,09jul02,ger  file creation: based on IPv4 implementation
*/

#ifndef __INCvsIp6h
#define __INCvsIp6h


/* includes */

#include "vxWorks.h"
#include <sys/callout.h>
#include <protos/ip6protosw.h>
#include <netinet6/ip6_var.h>
#include <netinet6/raw_ip6.h>
#include <net/domain.h>
#include <netLib.h>
#include <netinet6/ip6_ext_in.h>
#include <netinet6/ip6_ext_out.h>
#include <netinet6/scope6_var.h>
#include <time.h>
#include <netinet/ipfw.h>


/* defines */
#ifndef	IPV6FORWARDING
#ifdef GATEWAY6
#define	IPV6FORWARDING	1	/* forward IP6 packets not for us */
#else
#define	IPV6FORWARDING	0	/* don't forward IP6 packets not for us */
#endif /* GATEWAY6 */
#endif /* !IPV6FORWARDING */

#ifndef	IPV6_SENDREDIRECTS
#define	IPV6_SENDREDIRECTS	1
#endif

/* typedefs */

/*
 * We need to save the IP options in case a protocol wants to respond
 * to an incoming packet over the same route if the packet got here
 * using IP source routing.  This allows connection establishment and
 * maintenance when the remote end is on a network that is not known
 * to us.
 */



typedef struct vsIp6
    {
    /* definitions from frag6.c */
	int 			frag6_doing_reass;
	u_int 			frag6_nfragpackets;
	u_int 			frag6_nfrags;
	struct ip6q 		_ip6q;	/* ip6 reassemble queue */
	struct route_in6 	priv_ro;        /*only used in  frag6_input.c*/
	
	/* definitions from in6.c */
	struct in6_multihead 	_in6_multihead;	/* XXX BSS initialization */
	int  			priv_ip6round ;
	char 			priv_ip6buf[8][48];
        
	/* definitions from in6_ifattach.c */
    	unsigned long 		in6_maxmtu ;
    	int 			ip6_auto_linklocal ;
    	struct callout 		in6_tmpaddrtimer_ch;
	
	/* definitions from in6_proto.c */
	struct domain 		inet6domain ;
	int			ip6_forwarding ;	/* act as router? */
    	int			ip6_sendredirects ;
    	int			ip6_defhlim ;
    	int			ip6_defmcasthlim ;
    	int			ip6_accept_rtadv ;	/* "IPV6FORWARDING ? 0 : 1" is dangerous */
    	int			ip6_maxfragpackets;	/* initialized in frag6.c:frag6_init() */
    	int			ip6_maxfrags;	/* initialized in frag6.c:frag6_init() */
    	int			ip6_log_interval ;
    	int			ip6_hdrnestlimit ;	/* appropriate? */
    	int			ip6_dad_count ;	/* DupAddrDetectionTransmits */
    	u_int32_t 		ip6_flow_seq;
    	int			ip6_auto_flowlabel ;
	int			ip6_gif_hlim ;
    	int			ip6_use_deprecated ;	/* allow deprecated addr (RFC2462 5.5.4) */
    	int			ip6_rr_prune ;	/* router renumbering prefix  walk list     */
    	int			ip6_v6only ;
    	int			ip6_mcast_pmtu; /* enable pMTU discovery for multicast? */
    	u_int32_t 		ip6_id ;
    	int			ip6_keepfaith ;
    	time_t			ip6_log_time ;
	u_long			rip6_sendspace ;
    	u_long			rip6_recvspace ;
	/*will probably be moved to ICMP6 
	int			icmp6_rediraccept;		 accept and process redirects  
    	int	icmp6_redirtimeout ;	 
    	int	icmp6errppslim ;		
    	int	icmp6_nodeinfo ;		 enable/disable NI response */
    	/*will probably be moved to UDP6 
	int	udp6_sendspace ;		  really max datagram size  
    	int	udp6_recvspace ;*/
    	/* definitions from in6_rmx.c */
    	int 			rmx6_rtq_reallyold ;
	/* one hour is ``really old'' */
    	int 			rmx6_rtq_minreallyold ;
	/* never automatically crank down to less */
    	int 			rmx6_rtq_toomany ;
	/* 128 cached routes is ``too many'' */
	int 			rmx6_rtq_timeout ;
	time_t 			priv_last_adjusted_timeout ;
	
	void 			*in6_rtqtimoHandle;
	void			*in6_mtutimoHandle;
	
    	/* definitions from ip6_forward.c */
	struct	route_in6 	ip6_forward_rt;

        /* external hooks from ip6_mroute.c */
        FUNCPTR _mCastRouteFwdHookIPv6;
        FUNCPTR _mCastRouteCmdHookIPv6;
        FUNCPTR _mCastRouterIoctlHookIPv6;
        FUNCPTR _mCastRouterDoneHookIPv6;
        VOIDFUNCPTR _mCastRouterDetachHookIPv6;

        /* external hooks from ip6_forward.c */
        IPSEC_OUTPUT_IPV6_FUNCPTR ipsecForwardIPv6;

	/* from intrq.c */
	struct ifqueue ip6intrq;
	INTRQ_QJOB ip6intrqJob;

	/* definitions from netinet/ip6_input.c */
    	u_char 			ip6_protox[IPPROTO_MAX];
	int 			ip6qmaxlen;
	struct in6_ifaddr 	*_in6_ifaddr; /* first inet address */
    	struct ip6stat 		_ip6stat;
        struct socket           *ip6_mrouter;

        /* external hooks from ip6_input.c */
        INPUT_HOOK_IPV6_FUNCPTR   ipFilterHookIPv6;
        INPUT_HOOK_IPV6_FUNCPTR   ipsecFilterHookIPv6;
        INPUT_HOOK_IPV6_FUNCPTR   ipsecInputIPv6;

        /* external hooks from ip6_output.c */
        IPSEC_OUTPUT_IPV6_FUNCPTR ipsecOutputIPv6;

	/* definitions from raw_ip6.c */
	struct rip6stat 	_rip6stat;

	/* definitions from scope6.c */
	int              ip6_use_defzone;
	struct scope6_id sid_default;

	/* definitions from in6_src.c */
	int                       ip6_prefer_tempaddr;
	struct in6_addrpolicy     defaultaddrpolicy;
	struct addrsel_policyhead addrsel_policytab;

#ifdef INCLUDE_IPFW_HOOKS
    /* external hooks from ipfw_var.c, these are for input, output and
       forward files */
      ipfw_anchor_t ipfw_preinput6;
      ipfw_anchor_t ipfw_input6;
      ipfw_anchor_t ipfw_forward6;
      ipfw_anchor_t ipfw_output6;
#endif /* INCLUDE_IPFW_HOOKS */

    } VS_IP6;

/* macros */

#define VS_IP6_DATA ((VS_IP6 *)vsTbl[myStackNum]->pIp6Globals)

#define frag6_doing_reass     VS_IP6_DATA->frag6_doing_reass
#define frag6_nfragpackets    VS_IP6_DATA->frag6_nfragpackets
#define frag6_nfrags          VS_IP6_DATA->frag6_nfrags
#define _ip6q                 VS_IP6_DATA->_ip6q
#define _in6_multihead        VS_IP6_DATA->_in6_multihead
#define in6_maxmtu            VS_IP6_DATA->in6_maxmtu
#define ip6_auto_linklocal    VS_IP6_DATA->ip6_auto_linklocal 
#define in6_tmpaddrtimer_ch   VS_IP6_DATA->in6_tmpaddrtimer_ch
#define _rr_prefix            VS_IP6_DATA->_rr_prefix
#define inet6domain           VS_IP6_DATA->inet6domain
#define ip6_forwarding        VS_IP6_DATA->ip6_forwarding
#define ip6_sendredirects     VS_IP6_DATA->ip6_sendredirects
#define ip6_defhlim           VS_IP6_DATA->ip6_defhlim
#define ip6_defmcasthlim      VS_IP6_DATA->ip6_defmcasthlim
#define ip6_accept_rtadv      VS_IP6_DATA->ip6_accept_rtadv 
#define ip6_maxfragpackets    VS_IP6_DATA->ip6_maxfragpackets
#define ip6_maxfrags          VS_IP6_DATA->ip6_maxfrags
#define ip6_log_interval      VS_IP6_DATA->ip6_log_interval
#define ip6_hdrnestlimit      VS_IP6_DATA->ip6_hdrnestlimit
#define ip6_dad_count         VS_IP6_DATA->ip6_dad_count
#define ip6_flow_seq          VS_IP6_DATA->ip6_flow_seq
#define ip6_auto_flowlabel    VS_IP6_DATA->ip6_auto_flowlabel
#define ip6_gif_hlim          VS_IP6_DATA->ip6_gif_hlim
#define ip6_use_deprecated    VS_IP6_DATA->ip6_use_deprecated
#define ip6_rr_prune          VS_IP6_DATA->ip6_rr_prune
#define ip6_v6only            VS_IP6_DATA->ip6_v6only
#define ip6_mcast_pmtu        VS_IP6_DATA->ip6_mcast_pmtu
#define ip6_mapped_addr_on    (!ip6_v6only)
#define ip6_id                VS_IP6_DATA->ip6_id
#define ip6_keepfaith         VS_IP6_DATA->ip6_keepfaith
#define ip6_log_time          VS_IP6_DATA->ip6_log_time
#define rip6_sendspace        VS_IP6_DATA->rip6_sendspace
#define rip6_recvspace        VS_IP6_DATA->rip6_recvspace
#define rmx6_rtq_reallyold    VS_IP6_DATA->rmx6_rtq_reallyold
#define rmx6_rtq_minreallyold VS_IP6_DATA->rmx6_rtq_minreallyold
#define rmx6_rtq_toomany      VS_IP6_DATA->rmx6_rtq_toomany
#define rmx6_rtq_timeout      VS_IP6_DATA->rmx6_rtq_timeout
#define ip6_forward_rt        VS_IP6_DATA->ip6_forward_rt
#define ip6_protox            VS_IP6_DATA->ip6_protox
#define _in6_ifaddr           VS_IP6_DATA->_in6_ifaddr
#define _ip6stat              VS_IP6_DATA->_ip6stat
#define ip6_mrouter           VS_IP6_DATA->ip6_mrouter
#define _rip6stat             VS_IP6_DATA->_rip6stat
#define in6_rtqtimoHandle     VS_IP6_DATA->in6_rtqtimoHandle
#define in6_mtutimoHandle     VS_IP6_DATA->in6_mtutimoHandle
#define ip6_use_defzone       VS_IP6_DATA->ip6_use_defzone
#define sid_default           VS_IP6_DATA->sid_default
#define ip6_prefer_tempaddr   VS_IP6_DATA->ip6_prefer_tempaddr
#define defaultaddrpolicy     VS_IP6_DATA->defaultaddrpolicy
#define addrsel_policytab     VS_IP6_DATA->addrsel_policytab
#define ip6qmaxlen	      VS_IP6_DATA->ip6qmaxlen
#define ip6intrq	      VS_IP6_DATA->ip6intrq
#define ip6intrqJob	      VS_IP6_DATA->ip6intrqJob

/* external hooks from ip6_mroute.c */
#define _mCastRouteFwdHookIPv6      VS_IP6_DATA->_mCastRouteFwdHookIPv6
#define _mCastRouteCmdHookIPv6    VS_IP6_DATA->_mCastRouteCmdHookIPv6
#define _mCastRouterIoctlHookIPv6  VS_IP6_DATA->_mCastRouterIoctlHookIPv6
#define _mCastRouterDoneHookIPv6   VS_IP6_DATA->_mCastRouterDoneHookIPv6
#define _mCastRouterDetachHookIPv6 VS_IP6_DATA->_mCastRouterDetachHookIPv6

/* external hooks from ip6_forward.c */
#define _ipsecForwardIPv6     VS_IP6_DATA->ipsecForwardIPv6
#define SET_IPSECFORWARDIPV6(foo) (VS_IP6_DATA->ipsecForwardIPv6 = foo)

/* external hooks from ip6_input.c */
#define _ipFilterHookIPv6     VS_IP6_DATA->ipFilterHookIPv6
#define _ipsecFilterHookIPv6  VS_IP6_DATA->ipsecFilterHookIPv6
#define _ipsecInputIPv6       VS_IP6_DATA->ipsecInputIPv6

#define SET_IPFILTERHOOKIPV6(foo)    (VS_IP6_DATA->ipFilterHookIPv6 = foo)
#define SET_IPSECFILTERHOOKIPV6(foo) (VS_IP6_DATA->ipsecFilterHookIPv6 = foo)
#define SET_IPSECINPUTIPV6(foo)      (VS_IP6_DATA->ipsecInputIPv6 = foo)

/* external hooks from ip6_output.c */
#define _ipsecOutputIPv6      VS_IP6_DATA->ipsecOutputIPv6
#define SET_IPSECOUTPUTIPV6(foo)  (VS_IP6_DATA->ipsecOutputIPv6 = foo)

/* external hooks from ipfw_var.c */
#define ipfw_preinput6 VS_IP6_DATA->ipfw_preinput6
#define ipfw_input6    VS_IP6_DATA->ipfw_input6
#define ipfw_forward6  VS_IP6_DATA->ipfw_forward6
#define ipfw_output6   VS_IP6_DATA->ipfw_output6

#endif /* __INCvsIph */


