/* vsNdp.h - virtual stack data for Neighbor Discovery Protocol */

/* Copyright 2000 - 2002 Wind River Systems, Inc. */

/*
modification history
--------------------
01c,05nov03,cdw  Removal of unnecessary _KERNEL guards.
01b,04nov03,rlm  Ran batch header path update for header re-org.
01a,20aug02,hgo  created
*/

#ifndef __INCvsNdph
#define __INCvsNdph

#include <netinet/in.h>
#include <net/if_dl.h>

/* typedefs */

    /* Encapsulated (former) globals for NDP */

typedef struct vsNdp
    {
    /* defined in ndp.c */
    
    int                 _priv_c1flag;
    int                 _priv_n1flag;
    int                 _priv_t1flag;
    int                 _priv_sock;
    int                 _priv_repeat;
    struct sockaddr_in6 _priv_sin_m;
    struct sockaddr_dl  _priv_sdl_m;
    int                 _priv_flags;
    int                 _priv_found_entry;
    
    } VS_NDP;

#define VS_NDP_DATA ((VS_NDP *)vsTbl[myStackNum]->pNdpGlobals)

#endif /* __INCvsNdph */
