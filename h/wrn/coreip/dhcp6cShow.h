/* dhcp6cShow.h - header file for DHCPv6 client show routines */

/* Copyright 2004 Wind River Systems, Inc. */

/*
modification history
____________________
01a,08sep04,syy  written.
*/

#ifndef __INCdhcp6cShowh
#define __INCdhcp6cShowh

#ifdef __cplusplus
extern "C" {
#endif

IMPORT void     dh6cShowInit (void);
IMPORT STATUS   dh6cInfoShow (void);

#ifdef __cplusplus
}
#endif

#endif /* !__INCdhcp6cShowh */
