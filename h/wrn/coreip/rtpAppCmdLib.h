/* rtpAppCmdLib.h - definitions for the RTP applications command library */

/* Copyright 2004 Wind River Systems, Inc. */

/*
modification history
--------------------
01c,23aug04,rp   merged from COMP_WN_IPV6_BASE6_ITER5_TO_UNIFIED_PRE_MERGE
01b,28may04,niq  Merging from base6 label POST_ITER5_FRZ16_REBASE (ver
                 /main/vdt/base6_itn5_networking-int/2)
01a,09feb04,niq  file creation
*/

/*
DESCRIPTION
This file includes function prototypes and definitions for the RTP applications
command helper library.
*/

#ifndef _INCrtpAppCmdLibh
#define _INCrtpAppCmdLibh

#ifdef __cplusplus
extern "C" {
#endif

/* includes */
#include <dsi/salServer.h>

/* typedefs */
typedef SAL_RTN_STATUS (*SAL_READ_FNPTR)(int sockfd, void *pData);

/* external function declarations */
void * rtpCommandHandlerSpawn (char *pAppId, SAL_READ_FNPTR pDispFunc);
STATUS rtpCommandHandlerStop (char *pAppId, void *pCookie);

#ifdef __cplusplus
}
#endif

#endif /* _INCrtpAppCmdLibh */
