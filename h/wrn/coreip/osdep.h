/* 
 * osdep.h -- vxWorks OS dependent include files
 *            all other files should include this header for 
 *            OS related dependencies
 */

/* Copyright 1984-2005 Wind River Systems, Inc. */

/*
modification history
--------------------
02w,24jan05,vvv  added deprecation message
02v,14oct04,nee  Move snprintf in the double header inclusion protection
                 clause
02u,07sep04,sar  Fix for time wrapping problem
02t,23aug04,rp   merged from COMP_WN_IPV6_BASE6_ITER5_TO_UNIFIED_PRE_MERGE
02s,28may04,niq  Merging from base6 label POST_ITER5_FRZ16_REBASE (ver
                 /main/vdt/base6_itn5_networking-int/1)
02r,09feb04,niq  Enable osdep.h to be included from RTPs
02q,20nov03,niq  osdep.h cleanup
02p,05nov03,rlm  Ran batch header update for header re-org.
02o,24oct03,cdw  update include statements post header re-org.
02n,08sep03,vvv  merged from ACCORDION_BASE6_MERGE_BASELINE
02m,01may03,spm  removed incorrect bypass of header files
02l,13sep02,ppp  adding #include <ctype.h>
02k,13sep02,ham  added stdio.h
02j,27jun02,ppp  Merging TeamF1 changes for t22 (02i,10jun02,rvr  port to tor22)
02i,10jun02,ann  adding more include files for rlogin
02h,08may02,ppp  merging the T202 changes from TeamF1
02g,24apr02,ppp  adding the macro for strspn and strchr
02f,11mar02,ann  adding some lstLib macros
02e,06mar02,ann  adding some more OS files for remLib and netDrv
02d,12feb02,ann  adding a couple of macros
02c,08oct01,pas  remove non-os header files
02b,27sep01,pas  changed RANDOM() to used better PRNG
02a,26sep01,pas  added stricmp, strnicmp, strcasecmp, strncasecmp, snprintf
01z,25sep01,ann  changing the path for unixLib.h from utillib to utils
01y,21sep01,ann  including stat.h from target/h/sys
01x,20sep01,nee  including stdarg.h
01w,18sep01,ham  added extern snprintf temporary for build party.
01v,18sep01,ann  changing the path to unixLib.h
01u,18sep01,qli  fixing ENABLE_INT
01t,18sep01,ham  added MEMCMP.
01s,17sep01,ham  added SPRINTF.
01r,12sep01,ppp  undefining errno
01q,11sep01,ppp  Adding iovec struct
01p,11sep01,ppp  Removed _TYPE_int32_t and iovec definations from the file
01o,11sep01,ham  removed errno, MCLBYTES.
01n,10sep01,deg  added SLL macros.
01m,10sep01,ham  disabled vxWorks headers, redefined errno.
01l,06sep01,qli  adding definition for strncpy
01k,06sep01,ham  added time.h for timeval.
01j,06sep01,ham  corrected unixLib.h's path.
01i,06sep01,ham  added ticks, time_second and SYSTEM_CLOCK_RATE.
01h,05sep01,ham  added OS_MALLOC and DELAY.
01g,28aug01,ann  adding the declaration for curproc and corrected macros
01f,27aug01,ann  adding the prototype for structs that cause warnings
01e,27aug01,ham  removed unnecessary inclusion.
01d,23aug01,ppp  Adding a few definations in
01c,23aug01,ann  adding includes and definitions for sockets and tNet
01a,06aug01,qli  adding MACRO redefinitons

*/


#ifndef _VXWORKS_OSDEP_H_
#define _VXWORKS_OSDEP_H_

#warning "The use of this file is deprecated. Include vxWorks.h instead."

#ifdef _WRS_KERNEL
/* Disable the includes of certain vxWorks header files */

#define __INCmbufh        1
#define __INCuioh         1
#define __qlibh           1
#define __INCif_llch      1
#define __INCrouteh       1


#undef errno 

#define _KERNEL 1
#define KERNEL 1

/* time_second - elapse since GTF startup (seconds) */
extern unsigned long netGtfSeconds;
#define time_second netGtfSeconds

#endif /* _WRS_KERNEL */

#include <vxWorks.h>
#include <ctype.h>
#ifdef _WRS_VXWORKS_5_X
#ifndef PD_ID
#define PD_ID int
#endif
                 
#endif /* _WRS_VXWORKS_5_X */

#ifndef DISABLE_INTS
#define DISABLE_INTS()		intLock()
#endif

#ifndef ENABLE_INTS
#define ENABLE_INTS(levl)	intUnlock(levl)
#endif

#define BZERO(x, y)		bzero((void *)(x), (size_t)(y))

#define curproc NULL

#ifndef MSIZE
#define MSIZE           256             /* size of an mbuf */
#endif
 
#ifndef MCLSHIFT
#define MCLSHIFT        11              /* convert bytes to m_buf clusters */
#endif

#define CL_SIZE_128     128             /* size of the 128 byte cluster */

#define MSG_MBUF        1024            /* XXX  ? */
 
#ifndef BCOPY
#define BCOPY(src, dst, nbytes) bcopy((const char *)(src), (char *)(dst), (int)(nbytes))
#endif
 
#ifndef BCMP
#define BCMP(buf1, buf2, nbytes) bcmp((char *)(buf1), (char *)(buf2), (int)(nbytes))
#endif

#ifndef BZERO
#define BZERO(buf, nbytes)		bzero((char *)(buf), (int)(nbytes))
#endif
 
#ifndef STRLEN
#define STRLEN(s)               strlen(s)
#endif
 
#ifndef STRCMP
#define STRCMP(s1, s2)          strcmp((s1), (s2))
#endif

#ifndef STRNCMP
#define STRNCMP(s1, s2, s3)     strncmp((s1), (s2), (s3))
#endif

#ifndef STRICMP
#define STRICMP(s1, s2)         stricmp((s1), (s2))
#endif

#ifndef STRNICMP
#define STRNICMP(s1, s2, sz)   strnicmp((s1), (s2), (sz))
#endif

#ifndef STRCASECMP
#define STRCASECMP(s1, s2)      stricmp((s1), (s2))
#endif

#ifndef STRNCASECMP
#define STRNCASECMP(s1, s2, sz) strnicmp((s1), (s2), (sz))
#endif

#ifndef STRCPY
#define STRCPY(s1, s2)          strcpy((s1), (s2))
#endif

#ifndef STRNCPY
#define STRNCPY(src, dst, sz)	strncpy(src, dst, sz)
#endif

#ifndef SNPRINTF
#define SNPRINTF		snprintf
#endif

#ifndef STRSPN      
#define STRSPN			strspn
#endif 

#ifndef STRCHR    
#define STRCHR                  strchr
#endif 

#ifndef SPRINTF
#define SPRINTF			sprintf
#endif

#ifndef VSPRINTF
#define VSPRINTF		vsprintf
#endif

#ifndef PRINTF
#define PRINTF                  printf
#endif

#ifndef PRINTERR
#define PRINTERR                printErr
#endif

#ifndef MEMCPY
#define MEMCPY(x, y, z)		memcpy((x), (y), (z))
#endif

#ifndef MEMCMP
#define MEMCMP(x, y, z)		memcmp((x), (y), (z))
#endif

#ifndef MEMSET
#define MEMSET(x, y, z)		memset((x), (y), (z))
#endif

#ifndef OVBCOPY
#define OVBCOPY(x, y, z)	ovbcopy((x), (y), (z))
#endif

#ifndef RANDOM
#define RANDOM()		random()
#endif

#ifndef SRANDOM
#define SRANDOM(x)		srandom(x)
#endif

#ifndef MICROTIME
#define MICROTIME(x)		microtime(x)
#endif

/* The following macros define the semaphore calls */

/* Semaphore types */

#define BINARY_LOCK             1
#define MUTEX_LOCK              2

/* Semaphore states */

#define STATE_FULL              SEM_FULL
#define STATE_EMPTY             SEM_EMPTY

/* Semaphore options */

#define OPTS_PRIORITY           SEM_Q_PRIORITY
#define OPTS_DELETION_SAFE      SEM_DELETE_SAFE

/* Semaphore macros for operations */

#define LOCK                           SEM_ID
#define LOCK_CREATE(type, opts, state) (type == BINARY_LOCK) ?          \
                                            semBCreate(opts, state) :   \
                                            semMCreate(opts)
                            
#define LOCK_DELETE(id)                semDelete (id)

#define OBTAIN_LOCK(id, options)       semTake(id,options)
#define RELEASE_LOCK(id)               semGive(id)


/* The following macros define the select calls */

#define SELECT_LIST                    SEL_WAKEUP_LIST
#define SELECT_WAKEUP_LIST_INIT(id)    selWakeupListInit (id)

#ifndef _WRS_VXWORKS_5_X
#define SELECT_WAKEUP_LIST_FREE(id)     selWakeupListTerm (id)
#else
#define SELECT_WAKEUP_LIST_FREE(id)    semTerminate(&((id)->listMutex))
#endif /* _WRS_VXWORKS_5_X */

/* The following macros define the iosLib functions */

#define FD_TO_SOCK(x)                  iosFdValue(x)
#define SOCK_TO_FD(x,y,z)              iosFdNew (x,y,z)
#define SOCK_BACKEND_INSTALL(a,b,c,d,e,f,g)  iosDrvInstall(a,b,c,d,e,f,g)
#define FD_TO_OBJID(x)                 iosFdObjIdGet(x)
#define OBJ_OWNER_SET(x,y)             objOwnerSet(x,y)

#define DELAY(t)			taskDelay((int)t)	
#define OS_MALLOC(sz)			KHEAP_ALLOC(sz)

/* ticks - #of ticks since system startup */

#define TICKS				(tickGet())

/* Timestamp comparison macros */

#define TICK_LT(x,y) ((long)((x) - (y)) < 0)
#define TICK_GT(x,y) ((long)((x) - (y)) > 0)
#define TICK_LEQ(x,y) ((long)((x) - (y)) <= 0)
#define TICK_GEQ(x,y) ((long)((x) - (y)) >= 0)

#define SYSTEM_CLOCK_RATE		(sysClkRateGet())


/* lstLib macros */

#define    LST_INIT(x)                  lstInit(x)
#define    LST_FIRST(x)                 lstFirst(x)
#define    LST_NEXT(x)                  lstNext(x)
#define    LST_ADD(x,y)                 lstAdd(x,y)
#define    LST_NTH(x,y)                 lstNth(x,y)
#define    LST_COUNT(x)                 lstCount(x)
#define    LST_INSERT(x,y,z)            lstInsert(x,y,z)
#define    LST_DELETE(x,y)              lstDelete(x,y)


/*******************************************************************************
*
* SLL_INIT - initialize singly linked list head
*
* Initialize the specified list to an empty list.
*/

#define SLL_INIT(list)                                                    \
    {                                                                     \
    ((SL_LIST *)(list))->head	 = NULL;                                  \
    ((SL_LIST *)(list))->tail	 = NULL;                                  \
    }

/*******************************************************************************
*
* SLL_PUT_AT_HEAD - add node to beginning of list
*
* This macro adds the specified node to the end of the specified list.
*
*/

#define  SLL_PUT_AT_HEAD(list, node)                                     \
{                                                                        \
    if ((((SL_NODE *)(node))->next = ((SL_LIST *)(list))->head) == NULL) \
	{                                                                \
	((SL_LIST *)(list))->head = ((SL_NODE *)(node));                 \
	((SL_LIST *)(list))->tail = ((SL_NODE *)(node));                 \
	}                                                                \
    else                                                                 \
	((SL_LIST *)(list))->head = (node);                              \
    }

/*******************************************************************************
*
* SLL_PUT_AT_TAIL - add node to end of list
*
* This macro adds the specified node to the end of the specified singly
* linked list.
*
*/

#define  SLL_PUT_AT_TAIL(list, node)                                    \
    {                                                                   \
    ((SL_NODE *)(node))->next = NULL;                                    \
    if (((SL_LIST *)(list))->head == NULL)                              \
	{                                                               \
	((SL_LIST *)(list))->head = (SL_NODE *)(node);                  \
	((SL_LIST *)(list))->tail = (SL_NODE *)(node);                  \
	}                                                               \
    else                                                                \
	((SL_LIST *)(list))->tail->next = (SL_NODE *)(node);            \
	((SL_LIST *)(list))->tail = (SL_NODE *)(node);                  \
    }

/*******************************************************************************
*
* SLL_GET - get (delete and return) first node from list
*
* This macro gets the first node from the specified singly linked list,
* deletes the node from the list, and returns a pointer to the node gotten.
*
*/

#define SLL_GET(list, node)                                            \
    {                                                                  \
    if (((node) = (void *)((SL_LIST *)(list))->head) != NULL)       \
	((SL_LIST *)(list))->head = ((SL_NODE *)(node))->next;         \
    }

/*******************************************************************************
*
* SLL_REMOVE - remove specified node in list
*
* Remove the specified node in a singly linked list.
*/

#define SLL_REMOVE(list, deleteNode, previousNode)                           \
    {                                                                        \
    if (((SL_NODE *)(previousNode)) == NULL)                                 \
	{                                                                    \
	((SL_LIST *)(list))->head = ((SL_NODE *)(deleteNode))->next;         \
	if (((SL_LIST *)(list))->tail == ((SL_NODE *)(deleteNode)))          \
	    ((SL_LIST *)(list))->tail = NULL;                                \
	}                                                                    \
    else                                                                     \
	{                                                                    \
	((SL_NODE *)(previousNode))->next = ((SL_NODE *)(deleteNode))->next; \
	if (((SL_LIST *)(list))->tail == ((SL_NODE *)(deleteNode)))          \
	    ((SL_LIST *)(list))->tail = ((SL_NODE *)(previousNode));         \
	}                                                                    \
    }

/*******************************************************************************
*
* SLL_PREVIOUS - find and return previous node in list
*
* Find and return the previous node in a singly linked list.
*/

#define SLL_PREVIOUS(list, node, previousNode)                              \
    {                                                                       \
    SL_NODE *temp;                                                          \
    (SL_NODE *)(previousNode) = NULL;                                       \
    temp = ((SL_LIST *)(list))->head;                                       \
    if (temp != NULL) && (temp != (node))                                   \
	while (temp->next != NULL)                                          \
	    {                                                               \
	    if (temp->next == (node))                                       \
		{                                                           \
		(SL_NODE *)(previousNode) = temp;                           \
		break;                                                      \
		}                                                           \
	    temp = temp->next;                                              \
	    }                                                               \
    }

/******************************************************************************
*
* DLL_INIT - initialize doubly linked list descriptor
*
* Initialize the specified list to an empty list.
*/

#define DLL_INIT(list)                                                      \
    {                                                                       \
    ((DL_LIST *)(list))->head = NULL;                                       \
    ((DL_LIST *)(list))->tail = NULL;                                       \
    }

/******************************************************************************
*
* DLL_INSERT - insert node in list after specified node
*
* This macro inserts the specified node in the specified list.
* The new node is placed following the specified 'previous' node in the list.
* If the specified previous node is NULL, the node is inserted at the head
* of the list.
*/

#define DLL_INSERT(list, previousNode, node)                                \
    {                                                                       \
    DL_NODE *temp;                                                          \
    if ((previousNode) == NULL)                                             \
	{                                                                   \
	temp = ((DL_LIST *)(list))->head;                                   \
	((DL_LIST *)(list))->head = (DL_NODE *)(node);                      \
	}                                                                   \
    else                                                                    \
	{                                                                   \
        temp = ((DL_NODE *)(previousNode))->next;                           \
        ((DL_NODE *)(previousNode))->next = (DL_NODE *)(node);              \
	}                                                                   \
    if (temp == NULL)                                                       \
	((DL_LIST *)(list))->tail = (DL_NODE *)(node);                      \
    else                                                                    \
       temp->previous = (DL_NODE *)(node);                                  \
    ((DL_NODE *)(node))->next = temp;                                       \
    ((DL_NODE *)(node))->previous = (DL_NODE *)(previousNode);              \
    }

/******************************************************************************
*
* DLL_ADD - add node to end of list
*
* This macro adds the specified node to the end of the specified list.
*/

#define DLL_ADD(list, node)                                                 \
    {                                                                       \
    DL_NODE *listTail = (list)->tail;                                       \
    DLL_INSERT ((list), listTail, (node));                                  \
    } 

/******************************************************************************
*
* DLL_REMOVE - remove specified node in list
*
* Remove the specified node in the doubly linked list.
*/

#define DLL_REMOVE(list, node)                                               \
    {                                                                        \
    if (((DL_NODE *)(node))->previous == NULL)                               \
	((DL_LIST *)(list))->head = ((DL_NODE *)(node))->next;               \
    else                                                                     \
	((DL_NODE *)(node))->previous->next = ((DL_NODE *)(node))->next;     \
    if (((DL_NODE *)(node))->next == NULL)                                   \
	((DL_LIST *)(list))->tail = ((DL_NODE *)(node))->previous;           \
    else                                                                     \
	((DL_NODE *)(node))->next->previous = ((DL_NODE *)(node))->previous; \
    }

/******************************************************************************
*
* DLL_GET - get (delete and return) first node from list
*
* This macro gets the first node from the specified list, deletes the node
* from the list, and returns a pointer to the node gotten.
*
*/

#define DLL_GET(list, node)                                                 \
    {                                                                       \
    (node) = (void *)((DL_LIST *)(list))->head;                             \
    if ((node) != NULL)                                                     \
	{                                                                   \
        ((DL_LIST *)(list))->head = ((DL_NODE *)(node))->next;              \
        if (((DL_NODE *)(node))->next == NULL)                              \
            ((DL_LIST *)(list))->tail = NULL;                               \
        else                                                                \
            ((DL_NODE *)(node))->next->previous = NULL;                     \
	}                                                                   \
    }
int     snprintf (char *, size_t, const char *, ...); /* XXX temporary */

#endif


