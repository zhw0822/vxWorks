/* if_var.h - network interface variable header file */
 
/*
 * Copyright (c) 2001-2005 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */
 
/*
 * Copyright (c) 1982, 1986, 1989, 1993
 *	The Regents of the University of California.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *	This product includes software developed by the University of
 *	California, Berkeley and its contributors.
 * 4. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *	From: @(#)if.h	8.1 (Berkeley) 6/10/93
 * $FreeBSD: src/sys/net/if_var.h,v 1.62 2003/11/12 03:14:29 rwatson Exp $
 * $FreeBSD: src/sys/net/if_var.h,v 1.18.2.15 2002/06/28 12:36:54 luigi Exp $
 */
/*
modification history
--------------------
02h,21sep05,kch  Added if_xname[] to ifnet structure, introduced if_name() 
                 macro to replace if_name() routine for backward compatibility.
                 Also added if_initname() prototype (merged from FreeBSD 5.x
                 if_var.h,v 1.84.2.6) (SPR #112724).
02g,29aug05,kch  Added if_nvlans counter to ifnet structure (SPR #112068).
02f,26aug05,dlk  Add section tags.
02e,26aug05,kch  Removed pTagData pointer from ifnet structure.
02d,12aug05,kch  Moved ifclone prototypes to if_clone.h.
02c,04aug05,vvv  clarified comments for if_index and ifIndex (SPR #106657)
02b,16jul05,dlk  Added intrqFlush() declaration for VIRTUAL_STACK case.
02a,25jun05,dlk  Added INTRQ_QJOB definition.
01z,24jun05,wap  Add link state support
01y,09may05,vvv  added include for if.h
01x,19apr05,rp   merged from comp_wn_ipv6_mld_interim-dev
01w,07feb05,vvv  _KERNEL cleanup
01v,10sep04,kc   Added VLAN_TAG define to protect pTagData in ifnet structure.
01u,12jul04,vvv  fixed compiler warning
01t,28may04,niq  Merging from base6 label POST_ITER5_FRZ16_REBASE (ver
                 /main/vdt/base6_itn5_networking-int/1)
01s,14may04,kc   Added pTagData to struct ifnet for L2 VLAN Tagging support.
01r,25feb04,nee  Adding if6_resolve to ifnet
01q,24feb04,dlk  Add closing brace if __cplusplus is defined.
01p,03feb04,wap  Add extra structure fields for checksum offload support
01o,02feb04,dlk  Restore interface output queues (repair IF_ENQUEUE()).
01n,20nov03,niq  osdep.h cleanup
01m,04nov03,rlm  Ran batch header path update for header re-org.
01l,03nov03,rlm  Removed wrn/coreip/ prefix from #includes for header re-org.
01k,23oct03,rlm  updated #includes for header re-org
01j,24sep03,kkz  Rebasing with BASE6_ITER2_FRZ7 - added IF_DEQUEUEIF macro
01i,08aug03,nee  Merging from label ACCORDION_BASE6_MERGE_BASELINE in
                 accordion codeline to Base6
01h,01feb03,pas  merge from FreeBSD 4.7 - added clone support, polling;
                 changed if_queue_drop() to if_handoff()
01g,18sep02,nee  removed ifr_type from ifreq structure
01f,09sep02,hsh  add c++ protection
01e,24aug02,nee  extended ifnet for admin control;defined admin states for
                 boot/up/down
01d,13feb02,ham  changed for tornado build.
01c,06oct01,ann  removing the prototypes for ether_output and
                 ether_output_frame
01b,11sep01,ann  adding the if_resolve func pointer that is missing
01a,16jul01,ham  created from FreeBSD4.3.
*/

#ifndef	_NET_IF_VAR_H_
#define	_NET_IF_VAR_H_

/*
 * Structures defining a network interface, providing a packet
 * transport mechanism (ala level 0 of the PUP protocols).
 *
 * Each interface accepts output datagrams of a specified maximum
 * length, and provides higher level routines with input datagrams
 * received from its medium.
 *
 * Output occurs when the routine if_output is called, with three parameters:
 *	(*ifp->if_output)(ifp, m, dst, rt)
 * Here m is the mbuf chain to be sent and dst is the destination address.
 * The output routine encapsulates the supplied datagram if necessary,
 * and then transmits it on its medium.
 *
 * On input, each interface unwraps the data received by it, and either
 * places it on the input queue of an internetwork datagram routine
 * and posts the associated software interrupt, or passes the datagram to a raw
 * packet input routine.
 *
 * Routines exist for locating interfaces by their addresses
 * or for locating an interface on a certain network, as well as more general
 * routing and gateway routines maintaining information used to locate
 * interfaces.  These routines live in the files if.c and route.c
 */

#ifdef __cplusplus
 extern "C" {
#endif
     
#ifdef __STDC__
/*
 * Forward structure declarations for function prototypes [sic].
 */
struct	mbuf;
struct	proc;
struct	rtentry;
struct	socket;
struct	ether_header;
#endif /* __STDC_ */

#include <sys/queue.h>	/* get TAILQ macros */
#include <net/mbuf.h>
#include <net/systm.h>	/* XXX */
#ifdef VIRTUAL_STACK
#include <netinet/vsLib.h>
#endif /* VIRTUAL_STACK */
#include <net/unixLib.h>
#include <net/if.h>            /* needed for struct if_data */
#include <jobQueueLib.h>

TAILQ_HEAD(ifnethead, ifnet);   /* we use TAILQs so that the order of */
TAILQ_HEAD(ifaddrhead, ifaddr);	/* instantiation is preserved in the list */
TAILQ_HEAD(ifprefixhead, ifprefix);
LIST_HEAD(ifmultihead, ifmultiaddr);

/*
 * Structure defining a queue for a network interface.
 */
struct	ifqueue {
	struct	mbuf *ifq_head;
	struct	mbuf *ifq_tail;
	int	ifq_len;
	int	ifq_maxlen;
	int	ifq_drops;
};

/* Job for dequeueing packets on protocol input queues (loopback only) */

typedef struct _INTRQ_QJOB
{
	QJOB job;
	struct ifqueue * ifq;
	void (* input) (struct mbuf *);
#ifdef VIRTUAL_STACK
	int vsnum;
#endif
} INTRQ_QJOB;


/*
 * Structure defining a network interface.
 *
 * (Would like to call this struct ``if'', but C isn't PL/1.)
 */

/*
 * NB: For FreeBSD, it is assumed that each NIC driver's softc starts with  
 * one of these structures, typically held within an arpcom structure.   
 * 
 *	struct <foo>_softc {
 *		struct arpcom {
 *			struct  ifnet ac_if;
 *			...
 *		} <arpcom> ;
 *		...   
 *	};
 *
 * The assumption is used in a number of places, including many
 * files in sys/net, device drivers, and sys/dev/mii.c:miibus_attach().
 * 
 * Unfortunately devices' softc are opaque, so we depend on this layout
 * to locate the struct ifnet from the softc in the generic code.
 *
 * Note that not all fields are used by drivers in the FreeBSD source
 * tree. However, who knows what third party software does with fields
 * marked as "unused", such as if_ipending, if_done, and if_poll*,
 * so any attemt to redefine their meaning might end up in binary
 * compatibility problems, even if the size of struct ifnet, and
 * the size and position of its fields do not change.
 * We just have to live with that.
 */
struct ifnet {
	void	*if_softc;		/* pointer to driver state */
	char	if_xname[IFNAMSIZ];	/* external name (name + unit) */
	char	*if_name;		/* name, e.g. ``en'' or ``lo'' */
	TAILQ_ENTRY(ifnet) if_link; 	/* all struct ifnets are chained */
	struct	ifaddrhead if_addrhead;	/* linked list of addresses per if */
        int	if_pcount;		/* number of promiscuous listeners */
	struct	bpf_if *if_bpf;		/* packet filter structure */
	UINT32  ifIndex;                /* External index for this if. It */
	                                /* is the index used by the MIBs. */
					/* It was added specifically for  */
					/* MIB compliance (monotonically  */
					/* increasing index by default).  */
        u_short	if_index;		/* Internal index for this if. It  */
	                                /* is the index used by the stack. */
					/* An index can be re-used.        */
       	short	if_unit;		/* sub-unit for lower level driver */
	short	if_timer;		/* time 'til if_watchdog called */
	u_short	if_nvlans;		/* number of active vlans */
	long	if_flags;		/* up/down, broadcast, etc. */
	int	if_ipending;		/* interrupts pending */
	void	*if_linkmib;		/* link-type-specific MIB data */
	size_t	if_linkmiblen;		/* length of above data */
	struct	if_data if_data;
	struct	ifmultihead if_multiaddrs; /* multicast addresses configured */
	int	if_amcount;		/* number of all-multicast requests */
/* procedure handles */
	int	(*if_output)		/* output routine (enqueue) */
		(struct ifnet *, struct mbuf *, struct sockaddr *,
		     struct rtentry *);
	void	(*if_start)		/* initiate output routine */
		(struct ifnet *);
	union {
		int	(*if_done)		/* output complete routine */
			(struct ifnet *);	/* (XXX not used) */
		int	uif_capabilities;	/* interface capabilities */
	} _u1;
	int	(*if_ioctl)		/* ioctl routine */
		(struct ifnet *, u_long, caddr_t);
	void	(*if_watchdog)		/* timer routine */
		(struct ifnet *);
	union {
		int	(*if_poll_recv)		/* polled receive routine */
			(struct ifnet *, int *);
		int	uif_capenable;		/* enabled features */
	} _u2;
	int	(*if_poll_xmit)		/* polled transmit routine */
		(struct ifnet *, int *);
	void	(*if_poll_intren)	/* polled interrupt reenable routine */
		(struct ifnet *);
	void	(*if_poll_slowinput)	/* input routine for slow devices */
		(struct ifnet *, struct mbuf *);
	void	(*if_init)		/* Init routine */
		(void *);
/* Clarinet - added this func pointer as it is used in the WRS code */
        int     (*if_resolve)();        /* arp resolve at driver level */
        int     (*if6_resolve)();       /* nd resolve at driver level */
	int	(*if_resolvemulti)	/* validate/resolve multicast */
		(struct ifnet *, struct sockaddr **, struct sockaddr *);
	struct	ifqueue if_snd;		/* output queue */
	struct	ifqueue *if_poll_slowq;	/* input queue for slow devices */
	struct	ifprefixhead if_prefixhead; /* list of prefixes per if */
/* Clarinet - added this cookie that holds the drv_ctrl pointer */
	void *  pCookie;                /* data for IP over MUX attachment */
	int if_adminstatus; 
	int admin_inet_status;  
	int admin_inet6_status; /* RFC 2465, admin status control for IPv6 */
	uint32_t if_csum_flags_tx;
	uint32_t if_csum_flags_rx;
#ifdef VIRTUAL_STACK
    VSNUM vsNum;
#endif /* VIRTUAL_STACK */

    /* for IPV6, if_afdata[AF_INET6] points to in6_ifextra data structure */
    void	*if_afdata[AF_MAX];  
    /* int	if_afdata_initialized; */
};
typedef void if_init_f_t (void *);

/*
 * Binary compatability gunk for 4.x ONLY.
 */
#define if_capabilities	_u1.uif_capabilities
#define if_capenable	_u2.uif_capenable

#define	if_mtu		if_data.ifi_mtu
#define	if_type		if_data.ifi_type
#define if_physical	if_data.ifi_physical
#define	if_addrlen	if_data.ifi_addrlen
#define	if_hdrlen	if_data.ifi_hdrlen
#define if_link_state	if_data.ifi_link_state
#define	if_metric	if_data.ifi_metric
#define	if_baudrate	if_data.ifi_baudrate
#define	if_hwassist	if_data.ifi_hwassist
#define	if_ipackets	if_data.ifi_ipackets
#define	if_ierrors	if_data.ifi_ierrors
#define	if_opackets	if_data.ifi_opackets
#define	if_oerrors	if_data.ifi_oerrors
#define	if_collisions	if_data.ifi_collisions
#define	if_ibytes	if_data.ifi_ibytes
#define	if_obytes	if_data.ifi_obytes
#define	if_imcasts	if_data.ifi_imcasts
#define	if_omcasts	if_data.ifi_omcasts
#define	if_iqdrops	if_data.ifi_iqdrops
#define	if_noproto	if_data.ifi_noproto
#define	if_lastchange	if_data.ifi_lastchange
#define if_recvquota	if_data.ifi_recvquota
#define	if_xmitquota	if_data.ifi_xmitquota
#define if_rawoutput(if, m, sa) if_output(if, m, sa, (struct rtentry *)0)

/* for compatibility with other BSDs */
#define	if_addrlist	if_addrhead
#define	if_list		if_link

#define if_name(ifp)	((ifp)->if_xname)

/*
 * Bit values in if_ipending
 */
#define	IFI_RECV	1	/* I want to receive */
#define	IFI_XMIT	2	/* I want to transmit */

/*
 * Output queues (ifp->if_snd) and slow device input queues (*ifp->if_slowq)
 * are queues of messages stored on ifqueue structures
 * (defined above).  Entries are added to and deleted from these structures
 * by these macros, which should be called with ipl raised to splimp().
 */
#define	IF_QFULL(ifq)		((ifq)->ifq_len >= (ifq)->ifq_maxlen)
#define	IF_DROP(ifq)		((ifq)->ifq_drops++)

#define	IF_ENQUEUE(ifq, m) \
    do  { \
	(m)->m_nextpkt = 0; \
	if ((ifq)->ifq_tail == 0) \
		(ifq)->ifq_head = m; \
	else \
		(ifq)->ifq_tail->m_nextpkt = m; \
	(ifq)->ifq_tail = m; \
	(ifq)->ifq_len++; \
        } while (0)

#define	IF_PREPEND(ifq, m) { \
	(m)->m_nextpkt = (ifq)->ifq_head; \
	if ((ifq)->ifq_tail == 0) \
		(ifq)->ifq_tail = (m); \
	(ifq)->ifq_head = (m); \
	(ifq)->ifq_len++; \
}
#if !defined IF_DEQUEUEIF
#define IF_DEQUEUEIF(ifq, m, ifp) { \
        (m) = (ifq)->ifq_head; \
        if (m) { \
                if (((ifq)->ifq_head = (m)->m_act) == 0) \
                        (ifq)->ifq_tail = 0; \
                (m)->m_act = 0; \
                (ifq)->ifq_len--; \
                (ifp) = *(mtod((m), struct ifnet **)); \
                IF_ADJ(m); \
        } \
}
#endif /* IF_DEQUEUEIF */

#define	IF_DEQUEUE(ifq, m) { \
	(m) = (ifq)->ifq_head; \
	if (m) { \
		if (((ifq)->ifq_head = (m)->m_nextpkt) == 0) \
			(ifq)->ifq_tail = 0; \
		(m)->m_nextpkt = 0; \
		(ifq)->ifq_len--; \
	} \
}

#ifdef _WRS_KERNEL

/*
 * #define _IF_QFULL for compatibility with -current
 */
#define	_IF_QFULL(ifq)				IF_QFULL(ifq)

/*
 * IF_HANDOFF* and if_handoff are only used in ether_output() and only if
 * NS is defined (undefined by default). Added NS protection to keep the
 * compiler happy (for "if_handoff not used" warnings).
 */

#ifdef NS
#define IF_HANDOFF(ifq, m, ifp)			if_handoff(ifq, m, ifp, 0)
#define IF_HANDOFF_ADJ(ifq, m, ifp, adj)	if_handoff(ifq, m, ifp, adj)

static __inline int
if_handoff(struct ifqueue *ifq, struct mbuf *m, struct ifnet *ifp, int adjust)
{
	int need_if_start = 0;
	int s = splimp();
 
	if (IF_QFULL(ifq)) {
		IF_DROP(ifq);
		splx(s);
		m_freem(m);
		return (0);
	}
	if (ifp != NULL) {
		ifp->if_obytes += m->m_pkthdr.len + adjust;
		if (m->m_flags & M_MCAST)
			ifp->if_omcasts++;
		need_if_start = !(ifp->if_flags & IFF_OACTIVE);
	}
	IF_ENQUEUE(ifq, m);
	if (need_if_start)
		(*ifp->if_start)(ifp);
	splx(s);
	return (1);
}
#endif /* NS */

/*
 * 72 was chosen below because it is the size of a TCP/IP
 * header (40) + the minimum mss (32).
 */
#define	IF_MINMTU	72
#define	IF_MAXMTU	65535

#endif /* _WRS_KERNEL */

/*
 * The ifaddr structure contains information about one address
 * of an interface.  They are maintained by the different address families,
 * are allocated and attached when an address is set, and are linked
 * together so all addresses for an interface can be located.
 */
struct ifaddr {
	struct	sockaddr *ifa_addr;	/* address of interface */
	struct	sockaddr *ifa_dstaddr;	/* other end of p-to-p link */
#define	ifa_broadaddr	ifa_dstaddr	/* broadcast address interface */
	struct	sockaddr *ifa_netmask;	/* used to determine subnet */
	struct	if_data if_data;	/* not all members are meaningful */
	struct	ifnet *ifa_ifp;		/* back-pointer to interface */
	TAILQ_ENTRY(ifaddr) ifa_link;	/* queue macro glue */
	void	(*ifa_rtrequest)	/* check or clean routes (+ or -)'d */
		(int, struct rtentry *, struct sockaddr *);
	u_short	ifa_flags;		/* mostly rt_flags for cloning */
	u_int	ifa_refcnt;		/* references to this structure */
	int	ifa_metric;		/* cost of going out this interface */
#ifdef notdef
	struct	rtentry *ifa_rt;	/* XXXX for ROUTETOIF ????? */
#endif
	int (*ifa_claim_addr)		/* check if an addr goes to this if */
		(struct ifaddr *, struct sockaddr *);

};
#define	IFA_ROUTE	RTF_UP		/* route installed */

/* for compatibility with other BSDs */
#define	ifa_list	ifa_link

/*
 * The prefix structure contains information about one prefix
 * of an interface.  They are maintained by the different address families,
 * are allocated and attached when an prefix or an address is set,
 * and are linked together so all prefixes for an interface can be located.
 */
struct ifprefix {
	struct	sockaddr *ifpr_prefix;	/* prefix of interface */
	struct	ifnet *ifpr_ifp;	/* back-pointer to interface */
	TAILQ_ENTRY(ifprefix) ifpr_list; /* queue macro glue */
	u_char	ifpr_plen;		/* prefix length in bits */
	u_char	ifpr_type;		/* protocol dependent prefix type */
};

/*
 * Multicast address structure.  This is analogous to the ifaddr
 * structure except that it keeps track of multicast addresses.
 * Also, the reference count here is a count of requests for this
 * address, not a count of pointers to this structure.
 */
struct ifmultiaddr {
	LIST_ENTRY(ifmultiaddr) ifma_link; /* queue macro glue */
	struct	sockaddr *ifma_addr; 	/* address this membership is for */
	struct	sockaddr *ifma_lladdr;	/* link-layer translation, if any */
	struct	ifnet *ifma_ifp;	/* back-pointer to interface */
	u_int	ifma_refcount;		/* reference count */
	void	*ifma_protospec;	/* protocol-specific state, if any */
};

#ifdef _WRS_KERNEL
#define	IFAFREE(ifa) \
	do { \
		if ((ifa)->ifa_refcnt <= 0) \
			ifafree(ifa); \
		else \
			(ifa)->ifa_refcnt--; \
	} while (0)

#ifndef VIRTUAL_STACK
extern	struct ifnethead ifnet_head;
extern struct	ifnet	**ifindex2ifnet;
extern	int ifqmaxlen;
extern	struct ifnet *loif;
extern	int last_if_index;
extern	struct ifaddr **ifnet_addrs;
#endif /* VIRTUAL_STACK */

_WRS_FASTTEXT
int	ether_output(struct ifnet *,
	   struct mbuf *, struct sockaddr *, struct rtentry *);

int	if_addmulti(struct ifnet *, struct sockaddr *, struct ifmultiaddr **);
int	if_allmulti(struct ifnet *, int);
int	if_attach(struct ifnet *);
int	if_delmulti(struct ifnet *, struct sockaddr *);
void	if_detach(struct ifnet *);
void	if_down(struct ifnet *, short);
void	if_initname(struct ifnet *, char *, int);
void	if_route(struct ifnet *, int flag, int fam);
int	if_setlladdr(struct ifnet *, const u_char *, int);
void	if_unroute(struct ifnet *, int flag, int fam);
void	if_up(struct ifnet *, short);
/*void	ifinit(void);*/ /* declared in systm.h for main() */
int	ifioctl(struct socket *, u_long, caddr_t, struct proc *);
int	ifpromisc(struct ifnet *, int);
struct	ifnet *ifunit(const char *);
struct	ifnet *if_withname(struct sockaddr *);

struct	ifaddr *ifa_ifwithaddr(struct sockaddr *);
struct	ifaddr *ifa_ifwithdstaddr(struct sockaddr *);
struct	ifaddr *ifa_ifwithnet(struct sockaddr *);
struct	ifaddr *ifa_ifwithroute(int, struct sockaddr *, struct sockaddr *);
struct	ifaddr *ifaof_ifpforaddr(struct sockaddr *, struct ifnet *);
void	ifafree(struct ifaddr *);

struct	ifmultiaddr *ifmaof_ifpforaddr(struct sockaddr *, struct ifnet *);
int	if_simloop(struct ifnet *ifp, struct mbuf *m, int af, int hlen);

extern void intrqJobHandler (INTRQ_QJOB * pJob);
#ifdef VIRTUAL_STACK
extern void intrqFlush (struct ifqueue * ifq);
#endif

#define IF_LLADDR(ifp)							\
    LLADDR((struct sockaddr_dl *) ifnet_addrs[ifp->if_index - 1]->ifa_addr)

#ifdef DEVICE_POLLING
enum poll_cmd { POLL_ONLY, POLL_AND_CHECK_STATUS, POLL_DEREGISTER };

typedef	void poll_handler_t (struct ifnet *ifp,
		enum poll_cmd cmd, int count);
int	ether_poll_register(poll_handler_t *h, struct ifnet *ifp);
int	ether_poll_deregister(struct ifnet *ifp);
#endif /* DEVICE_POLLING */
#endif /* _WRS_KERNEL */

#ifdef __cplusplus
}
#endif

#endif /* !_NET_IF_VAR_H_ */
