/* fastUdp6Lib.h - defines for the Fast UDP library */

/* Copyright 2002 - 2003 Wind River Systems, Inc. */

/*
modification history
--------------------
01d,05nov03,cdw  Removal of unnecessary _KERNEL guards.
01c,04nov03,rlm  Ran batch header path update for header re-org.
01b,15sep03,vvv  updated path for new headers
01a,10dec02,hgo  copied from fastUdpLib.c
*/

#ifndef __INCfastudp6Libh
#define __INCfastudp6Libh

/* includes */

#include <semLib.h>
#include <lstLib.h>
#include <netinet/in.h>
#include <netBufLib.h>
#include <zbufLib.h>
#include <sys/socket.h>
#include <cfgDefs.h>

/* defines */

/* Fast UDP error numbers */

#define S_fastUdp6Lib_INVALID_PORT          (M_fastUdpLib | 1)
#define S_fastUdp6Lib_INVALID_COOKIE        (M_fastUdpLib | 2)
#define S_fastUdp6Lib_INVALID_ARGS          (M_fastUdpLib | 3)
#define S_fastUdp6Lib_MEM_ERROR             (M_fastUdpLib | 4)
#define S_fastUdp6Lib_INVALID_OPTION        (M_fastUdpLib | 5)
#define S_fastUdp6Lib_NOT_INITIALIZED       (M_fastUdpLib | 6)
#define S_fastUdp6Lib_NO_IPV4MAPPED_SUPPORT (M_fastUdpLib | 7)

/* Fast UDP options */

#define RCV_BUFSIZE   0x0001      /* size of receive buffer in bytes */
#define RCV_LOWAT     0x0002      /* receive low-water mark */
#define RCV_V6ONLY    0x0003      /* receiver doesn't allow IPv4-mapped IPv6 addresses */

/* Fast UDP cookie */

typedef struct fastUdp6
    {
    struct fastUdp6 *next;
    SEM_ID   recvSem;               /* synchronization sem for receive queue */
    M_BLK_ID recvData;              /* head of receive queue */
    u_int    recvDataLen;           /* amount of data in receive queue */
    M_BLK_ID lastPkt;               /* last packet in receive queue */
    struct   udp6Options *options;  /* Fast UDP options */
    u_short  port;                  /* Fast UDP port */
    u_short  error;                 /* error number */
    struct sockaddr_in6 errorAddr;  /* address which sent error notification */
    int      applTaskId;
    } *FASTUDP6_ID;

struct udp6Options
    {
    u_int rcvBuf;     /* limit on receive buffer */
    u_int rcvLowat;   /* receive buffer low-water mark */
    BOOL  rcvV6Only;  /* receiver doesn't allow IPv4-mapped IPv6 addresses */
    };

/* Entry in port configuration table */

typedef struct port6Tbl
    {
    u_short startPort;
    u_short endPort;
    } PORT6_TBL;

typedef struct
    {
    NODE node;
    u_short startPort;
    u_short endPort;
    } PORT6ENTRY;

/* fastUdp configuration parameters */
    
typedef struct fastUdp6_config_params {
    CFG_DATA_HDR         cfgh;

    u_int	         cfg_priv_fastUdp6Debug;
    PORT6_TBL	        *cfg_pPortCfgTbl;
    u_int                cfg_portCfgTblEntries;
    } FASTUDP6_CONFIG_PARAMS;

#ifdef __cplusplus
extern "C" {
#endif

/* forward declarations */

STATUS fastUdp6LibInit (PORT6_TBL *tbl, int numEntries);
STATUS fastUdp6Add (u_short startPort, u_short endPort);
STATUS fastUdp6Del (u_short startPort, u_short endPort);
FASTUDP6_ID fastUdp6Bind (u_short port);
STATUS fastUdp6Unbind (FASTUDP6_ID pCookie);
STATUS fastUdp6Send (u_short port, ZBUF_ID data, int dataLen,
		    struct sockaddr_in6 * dstAddr, void * pDummy);
ZBUF_ID fastUdp6Recv (FASTUDP6_ID pCookie, int *dataLen,
		     struct sockaddr_in6 *srcAddr);
STATUS  fastUdp6OptionSet (FASTUDP6_ID pCookie, u_short option, void *val);
void    fastUdp6DebugSet (u_int debugStatus);

#ifdef __cplusplus
}
#endif

#endif __INCfastudp6Libh
