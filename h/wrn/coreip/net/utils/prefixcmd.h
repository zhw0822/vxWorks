/* prefixcmd.h - Header file for the prefixcmd utility */

/* Copyright 1992-2003 Wind River Systems, Inc. */

/*
modification history
--------------------
01e,05nov03,cdw  Removal of unnecessary _KERNEL guards.
01d,04nov03,rlm  Ran batch header path update for header re-org.
01c,03nov03,rlm  Removed wrn/coreip/ prefix from #includes for header re-org.
01b,10jun03,vvv  include netVersion.h
01a,06may03,ann  written
*/

#ifndef __INCprefixcmdh
#define __INCprefixcmdh

#ifdef __cplusplus
extern "C" {
#endif

#include <netVersion.h>

#if defined(__STDC__) || defined(__cplusplus)

extern int prefixcmd (char *params);
extern void prefixCmdInit (void);

#else	/* __STDC__ */

extern int prefixcmd ();
extern void prefixCmdInit ();

#endif	/* __STDC__ */

#ifdef __cplusplus
}
#endif

#endif /* __INCprefixcmdh */
