/* nfs3Defines.h - Network File System version 3 definitions header */

/* Copyright 1995-2003 Wind River Systems, Inc. */

/*
modification history
--------------------
01a,28jun04,dlk  Added modification history. Removed commented-out
                 S_nfsLib_NFS3ERR_* defines, which interfere with makeStatTbl.
*/


#ifndef __INCnfs3Definesh
#define __INCnfs3Definesh

#ifdef __cplusplus
extern "C" {
#endif

#include "vwModNum.h"

struct nfsinfo {
	uint32 rtmax;
	uint32 rtpref;
	uint32 rtmult;
	uint32 wtmax;
	uint32 wtpref;
	uint32 wtmult;
	uint32 dtpref;
	size3 maxfilesize;
	nfstime3 time_delta;
	uint32 properties;
};

typedef struct nfsinfo nfsinfo;

#define NFS3_FHSIZE 64
#define NFS3_COOKIEVERFSIZE 8
#define NFS3_CREATEVERFSIZE 8
#define NFS3_WRITEVERFSIZE 8

#define PA_ATTR post_op_attr_u.attributes
#define PA_TYPE post_op_attr_u.attributes.type
#define PA_MODE post_op_attr_u.attributes.mode
#define PA_SIZE post_op_attr_u.attributes.size
#define PA_UID post_op_attr_u.attributes.uid
#define PA_GID post_op_attr_u.attributes.gid
#define PA_ATIME post_op_attr_u.attributes.atime
#define PA_MTIME post_op_attr_u.attributes.mtime
#define PA_CTIME post_op_attr_u.attributes.ctime

#define RLOK_ATTR READLINK3res_u.resok.symlink_attributes
#define RLOK_PATH READLINK3res_u.resok.data
#define RLFL_ATTR READLINK3res_u.resfail.symlink_attributes

#define LUOK_FH LOOKUP3res_u.resok.object
#define LUOK_OBJATTR LOOKUP3res_u.resok.obj_attributes
#define LUOK_DIRATTR LOOKUP3res_u.resok.dir_attributes
#define LUFL_DIRATTR LOOKUP3res_u.resfail.dir_attributes

#define CTAR_OBJATTR how.createhow3_u.obj_attributes
#define CTAR_VERF how.createhow3_u.verf
#define CTOK_FH CREATE3res_u.resok.obj
#define CTOK_OBJ CREATE3res_u.resok.obj
#define CTOK_OBJATTR CREATE3res_u.resok.obj_attributes
#define CTOK_WCC CREATE3res_u.resok.dir_wcc
#define CTFL_WCC CREATE3res_u.resfail.dir_wcc

#define REOK_FILEATTR READ3res_u.resok.file_attributes
#define REOK_COUNT READ3res_u.resok.count
#define REOK_EOF READ3res_u.resok.eof
#define REOK_DATA READ3res_u.resok.data
#define REFL_FILEATTR READ3res_u.resfail.file_attributes

#define WROK_FILEWCC WRITE3res_u.resok.file_wcc
#define WROK_COUNT WRITE3res_u.resok.count
#define WROK_COMMIT WRITE3res_u.resok.committed
#define WROK_VERF WRITE3res_u.resok.verf
#define WRFL_FILEWCC WRITE3res_u.resfail.file_wcc

#define PE_SIZE pre_op_attr_u.attributes.size
#define PE_MTIME pre_op_attr_u.attributes.mtime
#define PE_CTIME pre_op_attr_u.attributes.ctime

#define GAOK_ATTR GETATTR3res_u.resok.obj_attributes

#define SATT_MODE mode.set_mode3_u.mode
#define SATT_UID  uid.set_uid3_u.uid
#define SATT_GID  gid.set_gid3_u.gid
#define SATT_SIZE size.set_size3_u.size
#define SATT_ATIME atime.set_atime_u.atime
#define SATT_MTIME atime.set_mtime_u.mtime

#define RDOK_DIRATTR READDIR3res_u.resok.dir_attributes
#define RDOK_COOKIEVERF READDIR3res_u.resok.cookieverf
#define RDOK_REPLY READDIR3res_u.resok.reply

#define FIOK_INFO_OFF FSINFO3res_u.resok.rtmax

#define PF_FH post_op_fh3_u.handle

#define FH_BUF data.data_val
#define COPY_FH(pf1, pf2) \
    bcopy ((char *) (pf1)->data.data_val, (char *) (pf2)->data.data_val, \
            (pf1)->data.data_len); \
    (pf2)->data.data_len = (pf1)->data.data_len;
#define ASSIGN_FH(pf1, pf2) \
    bcopy ((char *) pf1, (char *) pf2, sizeof(nfs_fh3));

#ifdef __cplusplus
}
#endif

#endif /* __INCnfs3Definesh */
