/* rtadvd.h - Router Advertisement */

/*
 * Copyright (c) 2001-2005 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*	$KAME: rtadvd.h,v 1.29 2005/04/01 06:04:37 suz Exp $	*/

/*
 * Copyright (C) 1998 WIDE Project.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the project nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE PROJECT AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE PROJECT OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * $FreeBSD: src/usr.sbin/rtadvd/rtadvd.h,v x.x.x.x xxxx/xx/xx xx:xx:xx xxxx Exp $
 */

/*
modification history
--------------------
01f,14jul05,niq  Remove unneeded extern
01e,20jun05,niq  Kame update
01d,09apr04,spm  fixed build failure: added missing include file
01c,18mar04,spm  merged from Orion for MSP 3.0 baseline
01b,22aug02,kal  increased stack size for T2.2
01a,07feb01,ism  initial port from FreeBSD
*/

#ifndef __INCrtadvdh
#define __INCrtadvdh

#ifdef __cplusplus
extern "C" {
#endif

#include <netinet/in.h>

#include <sys/times.h>    /* for struct timeval definition */

#define ALLNODES "ff02::1"
#define ALLROUTERS_LINK "ff02::2"
#define ALLROUTERS_SITE "ff05::2"
#define ANY "::"
#define RTSOLLEN 8

/* protocol constants and default values */
#define DEF_MAXRTRADVINTERVAL 600
#define DEF_ADVLINKMTU 0
#define DEF_ADVREACHABLETIME 0
#define DEF_ADVRETRANSTIMER 0
#define DEF_ADVCURHOPLIMIT 64
#define DEF_ADVVALIDLIFETIME 2592000
#define DEF_ADVPREFERREDLIFETIME 604800

#define MAXROUTERLIFETIME 9000
#ifdef MIP6
#define MINROUTERLIFETIME 12
#define MIN_MAXMINTERVAL 70
#define MAX_MAXMINTERVAL 1800000
#define MIN_MINMINTERVAL 30
#else
#define MIN_MAXINTERVAL 4
#define MAX_MAXINTERVAL 1800
#define MIN_MININTERVAL 3
#endif
#define MAXREACHABLETIME 3600000

#define MAX_INITIAL_RTR_ADVERT_INTERVAL  16
#define MAX_INITIAL_RTR_ADVERTISEMENTS    3
#define MAX_FINAL_RTR_ADVERTISEMENTS      3
#define MIN_DELAY_BETWEEN_RAS             3
#define MAX_RA_DELAY_TIME                 500000 /* usec */

#define PREFIX_FROM_KERNEL 1
#define PREFIX_FROM_CONFIG 2
#define PREFIX_FROM_DYNAMIC 3

/* taskSpawn parameters, ism start */
#define RTADV_TASK_NAME    "tRtAdv"
#define RTADV_PRI          100
#define RTADV_OPT          0
#define RTADV_SIZE         10000
/* ism end */

/* size of buffer passed to strerror_t */
#define RTADV_ERR_MSG_LEN  150

struct prefix {
	struct prefix *next;	/* forward link */
	struct prefix *prev;	/* previous link */

	struct rainfo *rainfo;	/* back pointer to the interface */

	struct rtadvd_timer *timer; /* expiration timer.  used when a prefix
				     * derived from the kernel is deleted.
				     */

	u_int32_t validlifetime; /* AdvValidLifetime */
	long	vltimeexpire;	/* expiration of vltime; decrement case only */
	u_int32_t preflifetime;	/* AdvPreferredLifetime */
	long	pltimeexpire;	/* expiration of pltime; decrement case only */
	u_int onlinkflg;	/* bool: AdvOnLinkFlag */
	u_int autoconfflg;	/* bool: AdvAutonomousFlag */
#ifdef MIP6
	u_int routeraddr;	/* bool: RouterAddress */
#endif
	int prefixlen;
	int origin;		/* from kernel or config */
	struct in6_addr prefix;
};

#ifdef ROUTEINFO
struct rtinfo {
	struct rtinfo *prev;	/* previous link */
	struct rtinfo *next;	/* forward link */

	u_int32_t ltime;	/* route lifetime */
	u_int rtpref;		/* route preference */
	int prefixlen;
	struct in6_addr prefix;
};
#endif

struct soliciter {
	struct soliciter *next;
	struct sockaddr_in6 addr;
};

struct	rainfo {
	/* pointer for list */
	struct	rainfo *next;

	/* timer related parameters */
	struct rtadvd_timer *timer;
	int initcounter; /* counter for the first few advertisements */
	struct timeval lastsent; /* timestamp when the latest RA was sent */
	int waiting;		/* number of RS waiting for RA */

	/* interface information */
	int	ifindex;
	int	advlinkopt;	/* bool: whether include link-layer addr opt */
	struct sockaddr_dl *sdl;
	char	ifname[16];
	int	phymtu;		/* mtu of the physical interface */

	/* original values of Host variables in kernel */
	u_int	orig_chlim;		/* CurHopLimit */
	u_int	orig_retrans;		/* RetransTimer (msec) */
	u_int	orig_basereachable;	/* BaseReachable (msec) */

	/* Router configuration variables */
	u_short lifetime;	/* AdvDefaultLifetime */
#ifdef MIP6
	u_int	maxminterval;	/* MaxRtrAdvInterval (unit msec) */
	u_int	minminterval;	/* MinRtrAdvInterval (unit msec) */
	u_int	delaybetweenras;
#else
	u_int	maxinterval;	/* MaxRtrAdvInterval */
	u_int	mininterval;	/* MinRtrAdvInterval */
#endif
	int 	managedflg;	/* AdvManagedFlag */
	int	otherflg;	/* AdvOtherConfigFlag */

#ifdef MIP6
	int	haflg;		/* HAFlag */
#endif
	int	rtpref;		/* router preference */
	u_int32_t linkmtu;	/* AdvLinkMTU */
	u_int32_t reachabletime; /* AdvReachableTime */
	u_int32_t retranstimer;	/* AdvRetransTimer */
	u_int	hoplimit;	/* AdvCurHopLimit */
	struct prefix prefix;	/* AdvPrefixList(link head) */
	int	pfxs;		/* number of prefixes */
	long	clockskew;	/* used for consisitency check of lifetimes */

#ifdef MIP6
	u_short	hapref;		/* Home Agent Preference */
	u_short	hatime;		/* Home Agent Lifetime */
#endif
#ifdef ROUTEINFO
	struct rtinfo route;	/* route information option (link head) */
	int	routes;		/* number of route information options */
#endif

	/* actual RA packet data and its length */
	size_t ra_datalen;
	u_char *ra_data;

	/* statistics */
	u_quad_t raoutput;	/* number of RAs sent */
	u_quad_t rainput;	/* number of RAs received */
	u_quad_t rainconsistent; /* number of RAs inconsistent with ours */
	u_quad_t rsinput;	/* number of RSs received */

	/* info about soliciter */
	struct soliciter *soliciter;	/* recent solication source */
};

/* Removed __P () from function declarations, ism */
struct rtadvd_timer * ra_timeout (void *);
void ra_timer_update (void *, struct timeval *);
int prefix_match (struct in6_addr *, int, struct in6_addr *, int);
struct rainfo *if_indextorainfo (int);
struct prefix *find_prefix (struct rainfo *, struct in6_addr *, int);


#ifndef VIRTUAL_STACK
#ifdef MIP6
extern int rtadv_mobileip6;
#endif
#endif

#ifdef __cplusplus
}
#endif

#endif /* __INCrtadvdh */
