/* rdiscLib.h - support for router discovery RFC 1256 */

/* Copyright 2000 - 2002 Wind River Systems, Inc. */

/*
modification history
--------------------
01c,13dec02,kal  Added rdiscStart() for restarting after rdCtl() stop.
01b,04nov02,kal  Ported from Router Stack 1.1 to Accordion stack
01a,29mar01,spm  file creation: copied from version 01b of tor2_0.open_stack
                 branch (wpwr VOB) for unified code base
*/

#ifndef __INCrdiscLibh
#define __INCrdiscLibh

#ifdef __cplusplus
extern "C" {
#endif

/* Configuration structure for the initialisation function rdiscInstInit() */

/*
 * Priority of router discovery task.
 * Options to taskSpawn(1) for router discovery task.
 * Stack size for router discovery task.
 */

typedef struct rdisccfgparams
    {
    CFG_DATA_HDR 	cfgh ;
    int 		    cfg_rdisc_priority; 
    int	    	    cfg_rdisc_options; 
    int             cfg_rdisc_stacksize;
    } RDISC_CFG_PARAMS;


/* cmd-types for the rdCtl-function */

#define SET_ADVERT_LIFETIME	0
#define SET_MIN_ADVERT_INT	1
#define SET_MAX_ADVERT_INT	2
#define SET_MODE	        3
#define SET_FLAG		4
#define SET_ADVERT_PREF		5
#define SET_ADVERT_ADDRESS	6

#define GET_FLAG		7
#define GET_ADVERT_LIFETIME	8
#define GET_ADVERT_PREF		9
#define GET_ADVERT_ADDRESS	10
#define GET_MIN_ADVERT_INT	11
#define GET_MAX_ADVERT_INT	12

/* Modes for the rdCtl routine, valid if cmd=SET_MODE. */
#define MODE_DEBUG_OFF          0    /* Turn debugging off. */
#define MODE_DEBUG_ON           1    /* Turn debugging on. */
#define MODE_STOP               2    /* Stop rdisc */

#ifdef VIRTUAL_STACK
extern STATUS rdiscLibInit (void);
extern STATUS rdiscDestructor (int vsnum);
#endif

extern STATUS rdiscInstInit (void *initvalues);
extern void rdisc ();
extern STATUS rdiscIfReset ();
/* value may be an int (set-cmds) or an int* (get-cmds) */
extern STATUS rdCtl (char *ifName, int cmd, void* value);
/* restart rdisc after it was stopped using rdCtl() */
extern STATUS rdiscStart (void);

#ifdef __cplusplus
}
#endif

#endif /* __INCrdiscLibh */
