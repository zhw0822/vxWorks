/* nfsdLib.h - NFS v3 Server Library Header */

/* Copyright 1984 - 2003 Wind River Systems, Inc. */

/*
modification history
--------------------
01a,15sep03,msc  written
*/

#ifndef __INCnfsdCommonh
#define __INCnfsdCommonh

#ifdef __cplusplus
extern "C" {
#endif

#include "limits.h"
#include "nfs2dLib.h"
#include "nfs3dLib.h"

/* Data Structures */

/* Common data of file handle irrespective of v2 or v3 */
typedef struct
    {
    int    volumeId;        /* Volume identifier of the File System */
    ULONG  fsId;            /* Inode of the exported directory */
    ULONG  inode;           /* Inode of the file as returned from stat */
    } NFS_FILE_HANDLE; 


extern  NFS3_SERVER_STATUS  nfs3ServerStatus;
extern  NFS2_SERVER_STATUS  nfs2ServerStatus;

#if defined(__STDC__) || defined(__cplusplus)

extern STATUS  nfsdStatusGet (void * serverStats, int version);
extern STATUS  nfsdStatusShow (int options);
extern STATUS  nfsdFhCreate (NFS_FILE_HANDLE * parentDir, char * fileName,
                             NFS_FILE_HANDLE * fh);

extern STATUS  nfsdFhToName (NFS_FILE_HANDLE * fh, char * fileName);
extern nfsstat nfsdErrToNfs (int theErr);
extern void    nfsdFhHton (NFS_FILE_HANDLE * fh);
extern void    nfsdFhNtoh (NFS_FILE_HANDLE * fh);
extern int     nfsdFsReadOnly (NFS_FILE_HANDLE * fh);

#else

extern STATUS  nfsdStatusGet ();
extern STATUS  nfsdStatusShow ();
extern STATUS  nfsdFhCreate ();
extern STATUS  nfsdFhToName ();
extern nfsstat nfsdErrToNfs ();
extern void    nfsdFhHton ();
extern void    nfsdFhNtoh ();
extern int     nfsdFsReadOnly ();

#endif  /* __STDC__ */

#ifdef __cplusplus
}
#endif

#endif /* __INCnfsdCommonh */
