/* rtadvLib.h - Router Advertisement for IPv6 */

/* Copyright 1992 - 2004 Wind River Systems, Inc. */

/*
modification history
--------------------
01h,15mar04,rp   merged from orion
01g,05nov03,cdw  Removal of unnecessary _KERNEL guards.
01f,04nov03,rlm  Ran batch header path update for header re-org.
01e,03nov03,rlm  Removed wrn/coreip/ prefix from #includes for header re-org.
01d,06aug03,nee  Accordion Base6 merge from ACCORDION_BASE6_MERGE_BASELINE
                 label
01c,10jun03,vvv  include netVersion.h
01b,11Jul02,ism  added dynamic configuration function prototypes
01a,02Apr02,kal  written.
*/

#ifndef __INCrtadvLibh
#define __INCrtadvLibh

#ifdef __cplusplus
extern "C" {
#endif

#include <netVersion.h>

#include <cfgDefs.h>

typedef struct rtadv_config_params {
    CFG_DATA_HDR         cfgh;

    /* only used for parameter string */

    char                *cfg_pParameterStr;
    } RTADV_CONFIG_PARAMS;

#if defined(__STDC__) || defined(__cplusplus)

extern int      rtadv (char *options);
extern void     rtadv_stop (void);
extern STATUS   set_rtadv_conf(const char *capstr, int append);
extern void     clr_rtadv_conf(void);
extern STATUS   remove_rtadv_conf (char *ifname);
extern void     print_rtadv_conf (char *ifname);

#else	/* __STDC__ */

extern int      rtadv ();
extern void     rtadv_stop ();
extern STATUS   set_rtadv_conf();
extern void     clr_rtadv_conf();
extern STATUS   remove_rtadv_conf ();
extern void     print_rtadv_conf ();

#endif	/* __STDC__ */

#ifdef __cplusplus
}
#endif

#endif /* __INCrtadvLibh */
