/* fastPathMon.h - Definitions for the Fastpath interface towards the RTM */

/* Copyright 1984 - 2004 Wind River Systems, Inc. */

/*
modification history
--------------------
01f,28may04,niq  Merging from base6 label POST_ITER5_FRZ16_REBASE
01e,20nov03,niq  Remove copyright_wrs.h file inclusion
01d,04nov03,rlm  Ran batch header path update for header re-org.
01c,03apr03,niq  Increase the buffer size for receiving messages
01b,15nov02,deg  ffMonShutdown() changed to ffMonDestructor()
01a,07oct02,deg  written
*/

/*
DESCRIPTION
This file contains definitions used by the Fastpath RTM monitor.

*/

#ifndef __INCfastPathMonh
#define __INCfastPathMonh

/* defines */

#define ffMonTaskPriority 75
#define ffMonTaskOptions VX_SUPERVISOR_MODE
#define ffMonTaskStackSize 10000

#define FF_RTBUFSIZE 	2048
#define FF_CFGBUFSIZE	32

#define FF_MON_LIST_INIT(list) DLL_INIT(list)
#define FF_MON_LIST_FIRST(list) (FF_MON_LIST_NODE *)DLL_FIRST(list)
#define FF_MON_LIST_NEXT(node) (FF_MON_LIST_NODE *)DLL_NEXT(node)
#define FF_MON_LIST_PUT_REAR(list, node) DLL_ADD((list), (node))
#define FF_MON_LIST_REMOVE(list, node) DLL_REMOVE ((list),(node))

/* alignment constraint for routing socket */
#define ROUNDUP(a) \
	((a) > 0 ? (1 + (((a) - 1) | (sizeof(long) - 1))) : sizeof(long))

/* typedefs */

typedef DL_NODE FF_MON_NODE;
typedef DL_LIST FF_MON_LIST; 

typedef struct ffMonListNode
{
  FF_MON_NODE node;
  int sockDesc;
} FF_MON_LIST_NODE;

typedef enum 
  {
    FF_MON_STACK_SET_UP = 1,
    FF_MON_STACK_SET_DOWN,
    FF_MON_STACK_IS_UP,
    FF_MON_STACK_IS_DOWN,
    FF_MON_ERROR
  } FF_MON_CMD;

typedef struct ffMonCfgMsg
{
  FF_MON_CMD cmd;
#ifdef VIRTUAL_STACK
  VSNUM vsnum;
#endif /* VIRTUAL_STACK */
  int sockDesc;
  unsigned char len;
} FF_MON_CFG_MSG;

/* function declarations */

IMPORT STATUS ffMonInstInit   (void *cfgParams);
#ifndef VIRTUAL_STACK
IMPORT STATUS ffMonDestructor (void);
#endif /* VIRTUAL_STACK */
IMPORT STATUS ffMonWakeup     (FF_MON_CFG_MSG *);

#endif /* __INCfastPathMonh */
