/* fastPathIp.h - Definitions for the IP Fast Forwarder module  */

/*
 * Copyright (c) 1984-2005 Wind River Systems, Inc.
 *
 * The right to copy, distribute or otherwise make use of this software
 * may be licensed only pursuant to the terms of an applicable Wind River
 * license agreement. No license to Wind River intellectual property rights
 * is granted herein. All rights not licensed by Wind River are reserved
 * by Wind River.
 */

/*
modification history
--------------------
01f,01mar05,niq  Fix the ipFFInit API (SPR 102586)
01e,20nov03,niq  Remove copyright_wrs.h file inclusion
01d,05nov03,cdw  Removal of unnecessary _KERNEL guards.
01c,15sep03,vvv  updated path for new headers
01b,20mar03,niq  Deprecate protocol specific APIs in favor of protocol
                 independent APIs
01a,12mar03,niq  written - consolidated v4/v6 files
*/

/*
DESCRIPTION
This file contains definitions used by the IPv4/IPv6 Fast Forwarder module.
 
*/

#ifndef __INCfastpathIph
#define __INCfastpathIph

/* includes */

#include <fastPath/fastPathFib.h>

/* function declarations */

/* 
 * Following two APIs are deprecated and might go away in the next release
 * Use the protocol independent APIs (ipFFInit, ipFFCacheLookup) instead
 */
IMPORT FF_OBJ_ID ipv4FFInit      (int               numRoutes,
				  int               proto,
				  FIB_DISPATCH_TBL *pFibDispTbl);
IMPORT STATUS ipv4FFCacheLookup (FF_OBJ *pFFObj, struct sockaddr *pDstAddr,
				 FF_PUB_FIB_ENTRY *pPubFibEntry);

/* Protocol independent APIs to initialize the fastpath and to lookup cache */
IMPORT FF_OBJ_ID ipFFInit      (int               numRoutes,
				int               proto,
				FIB_DISPATCH_TBL *pFibDispTbl);
IMPORT STATUS ipFFCacheLookup (FF_OBJ *pFFObj, struct sockaddr *pDstAddr, 
			       FF_PUB_FIB_ENTRY *pPubFibEntry);
#endif /* __INCfastpathIph */
