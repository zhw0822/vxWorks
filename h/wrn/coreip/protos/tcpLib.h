/* tcpLib.h - VxWorks TCP header file */

/* Copyright 2002-2005  Wind River Systems, Inc. */

/*
modification history
--------------------
01l,09feb05,wap  Allow sysctl init routines to be scaled out
01k,23aug04,rp   merged from COMP_WN_IPV6_BASE6_ITER5_TO_UNIFIED_PRE_MERGE
01j,28may04,niq  Merging from base6 label POST_ITER5_FRZ16_REBASE (ver
                 /main/vdt/base6_itn5_networking-int/1)
01i,20nov03,niq  Remove copyright_wrs.h file inclusion
01h,05nov03,cdw  Removal of unnecessary _KERNEL guards.
01g,04nov03,rlm  Ran batch header path update for header re-org.
01f,15sep03,vvv  updated path for new headers
01e,07mar03,spm  changed parameters after merge from CLARINET-1_1-KAME-UPDATE
01d,06sep02,ger  remove tcp_v6mssdflt from config-params (is in ipv6)
01c,04sep02,kal  took out ifdef inclusions of elements for configlettes
01b,28aug02,kal  fixed location of cfgDefs.h
01a,18jun02,hgo  creation
*/

#ifndef __INCtcpLibh
#define __INCtcpLibh

#ifdef __cplusplus
extern "C" {
#endif

#include <cfgDefs.h>

typedef struct tcp_config_params
    {
    CFG_DATA_HDR cfgh;

    /* defined in tcp_input.c */

    int cfg_priv_tcp_log_in_vain;	/* log all incoming TCP connections */
    int cfg_priv_tcp_blackhole;		/* do not send RST when dropping
					   refused connections */
    int cfg_tcp_delack_enabled;		/* delay ACK to try and piggyback it
					   onto the packet */

    /*
     * cfg_priv_drop_synfind was only included ifdef TCP_DROP_SYNFIN but this
     * header is used by configlettes which wouldn't have that defined.
     */
    
    int cfg_priv_drop_synfin;		/* drop TCP packets with SYN+FIN set */
    int cfg_tcp_reass_global_max;       /* max total number of segments in
					   all TCP reassembly queues */
    int cfg_tcp_reass_endpt_max;       /* max number of segments per
					  endpoint reassembly queue, or -1 */

    /* defined in tcp_output.c */

    int cfg_priv_path_mtu_discovery;	/* enable path MTU discovery */
    int cfg_ss_fltsz;			/* slow start flight size */
    int cfg_ss_fltsz_local;		/* slow start flight size for local
                                           networks */
    int cfg_tcp_do_newreno;		/* enable NewReno Algorithms */

    /* defined in tcp_subr.c */

    int cfg_tcp_mssdflt;		/* default TCP maximum segment size */

    int cfg_priv_tcp_do_rfc1323;	/* enable RFC1323 (high performance)
					   TCP extensions */
    int cfg_priv_tcp_do_rfc1644;	/* enable RFC1644 (TTCP) extensions */
    int cfg_priv_do_tcpdrain;		/* enable tcp_drain routine for extra
					   help when low on mbufs */
    int cfg_priv_icmp_may_rst;		/* certain ICMP unreachable message
					   may abort connections in SYN_SENT */
    int cfg_tcp_isn_reseed_interval;
    int cfg_tcp_inflight_enable;	/* allow bandwidth limits */
    int cfg_tcp_inflight_debug;		/* print inflight values */
    int cfg_tcp_inflight_min;		/* minimum inflight window */
    int cfg_tcp_inflight_max;		/* maximum inflight window */

    /* defined in tcp_timer.c */

    int	cfg_tcp_keepinit;
    int	cfg_tcp_keepidle;
    int	cfg_tcp_keepintvl;
    int	cfg_tcp_delacktime;		/* time before a delayed ACK is sent */
    int cfg_tcp_msl;			/* maximum segment life time */
    int cfg_priv_always_keepalive;	/* assume SO_KEEPALIVE on all TCP
					   connections */
    int cfg_priv_tcp_keepcnt;

    /* defined in tcp_usrreq.c */

    u_long cfg_tcp_sendspace;          /* default send socket buffer size */
    u_long cfg_tcp_recvspace;          /* default receive socket buf. size */
    FUNCPTR cfg_privInitSysctl;

    } TCP_CONFIG_PARAMS;

typedef struct tcp6_config_params
    {
    CFG_DATA_HDR cfgh;
    int cfg_tcp_v6mssdflt;		/* One TCP (v6) parameter */
    FUNCPTR cfg_privInitSysctl;

    } TCP6_CONFIG_PARAMS;

#ifdef __cplusplus
}
#endif

#endif /* __INCtcpLibh */
