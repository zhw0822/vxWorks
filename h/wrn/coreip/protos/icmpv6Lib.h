/* icmpv6Lib.h - configuration data for ICMPv6 */

/*
 * Copyright (c) 2000-2005 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01h,29apr05,rp   moved mld config variables to mldLib.h.
01g,21apr05,rp   added mldv2 variables
01f,09feb05,wap  Allow sysctl init routines to be scaled out
01e,05nov03,cdw  Removal of unnecessary _KERNEL guards.
01d,04nov03,rlm  Ran batch header path update for header re-org.
01c,15sep03,vvv  updated path for new headers
01b,03sep02,ger  removed cfg_priv_icmp6errpps_count from config params
01a,24jun02,ant  written
*/

#ifndef __INCicmpv6Libh
#define __INCicmpv6Libh

/* includes */


#include <cfgDefs.h>

/* defines */

/* typedefs */

typedef struct icmpv6_config_params
    {
    CFG_DATA_HDR 	cfgh;
    /*int 		cfg_priv_icmp6errpps_count;   not configurable*/
    int			cfg_icmp6_rediraccept;
    int			cfg_icmp6_redirtimeout;
    int 		cfg_icmp6errppslim;
    int 		cfg_icmp6nodeinfo;
    FUNCPTR		cfg_privInitSysctl;
    } ICMPV6_CONFIG_PARAMS;

#endif /* __INCicmpv6Libh */


