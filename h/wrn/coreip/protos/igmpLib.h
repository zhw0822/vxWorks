/* igmpLib.h - configuration data structute for IGMP */

/* Copyright 2000 - 2005 Wind River Systems, Inc. */

/*
modification history
--------------------
01e,09feb05,wap  Allow sysctl init routines to be scaled out
01d,05nov03,cdw  Removal of unnecessary _KERNEL guards.
01c,04nov03,rlm  Ran batch header path update for header re-org.
01b,15sep03,vvv  updated path for new headers
01a,14aug03,niq  Created
*/

#ifndef __INCigmpLibh
#define __INCigmpLibh

#ifdef __cplusplus
extern "C" {
#endif

/* includes */

#include <netVersion.h>
#include <cfgDefs.h>

/* defines */

/* typedefs */

typedef struct igmp_config_params
    {
    CFG_DATA_HDR	cfgh;
    FUNCPTR		cfg_privInitSysctl;
    } IGMP_CONFIG_PARAMS;

/* prototypes */

#if defined(__STDC__) || defined(__cplusplus)

IMPORT STATUS igmpInstInit (void * InitValues);

#else   /* __STDC__ */

IMPORT STATUS igmpInstInit ();

#endif  /* __STDC__ */

#ifdef __cplusplus
}
#endif

#endif /* __INCigmpLibh */
