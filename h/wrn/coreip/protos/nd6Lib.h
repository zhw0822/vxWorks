/* nd6Lib.h - VxWorks ND6 header file */

/* Copyright 2002-2005 Wind River Systems, Inc. */

/*
modification history
--------------------
01k,12may05,rp   added _nd6_maxqueuelen
01j,09feb05,wap  Allow sysctl init routines to be scaled out.
01i,20nov03,niq  Remove copyright_wrs.h file inclusion
01h,05nov03,cdw  Removal of unnecessary _KERNEL guards.
01g,04nov03,rlm  Ran batch header path update for header re-org.
01f,15sep03,vvv  updated path for new headers
01e,10mar03,nee  consolidate nd6_ifinfo_indexlim and if_indexlim
01d,05sep02,hgo  added use_tempaddr, temp_xxx_lifetime
01c,30aug02,ism  added cfg_nd_ifinfo_indexlim
01b,28aug02,kal  fixed location of cfgDefs.h
01a,08jul02,hgo  creation
*/

#ifndef __INCnd6Libh
#define __INCnd6Libh

#ifdef __cplusplus
extern "C" {
#endif

/* includes */
#include <cfgDefs.h>

#ifdef VIRTUAL_STACK
#include <netinet/vsLib.h>
#endif

typedef struct nd6_config_params {
    CFG_DATA_HDR        cfgh;

    int                 cfg_nd6_prune;       /* walk list every 1 seconds */
    int                 cfg_nd6_delay;       /* delay first probe time 5 second */
    int                 cfg_nd6_umaxtries;   /* maximum unicast query */
    int                 cfg_nd6_mmaxtries;   /* maximum muliticast query */
    int                 cfg_nd6_useloopback; /* use loopback interface for local traffic */
    int                 cfg_nd6_maxnudhint;  /* max # of subsequent upper layer hints */
    int                 cfg_nd6_maxqueuelen;  /* max # of packets cac
hed in unresolved ND entries */
    int                 cfg_nd6_debug;

    int                 cfg_ip6_use_tempaddr;
    u_int32_t           cfg_ip6_temp_preferred_lifetime;
    u_int32_t           cfg_ip6_temp_valid_lifetime;
    FUNCPTR		cfg_privInitSysctl;
    } ND6_CONFIG_PARAMS;

#ifdef VIRTUAL_STACK
extern VS_REG_ID nd6RegistrationNum;
#endif /* VIRTUAL_STACK */
extern ND6_CONFIG_PARAMS nd6DefaultConfigParams;

#if defined(__STDC__) || defined(__cplusplus)
extern STATUS   nd6InstInit (void *);
#ifdef VIRTUAL_STACK
extern STATUS   nd6Destructor (VSNUM);
#endif

#else	/* __STDC__ */
extern STATUS   nd6InstInit ();
#ifdef VIRTUAL_STACK
extern STATUS   nd6Destructor ();
#endif

#endif	/* __STDC__ */


#ifdef __cplusplus
}
#endif

#endif /* __INCnd6Libh */
