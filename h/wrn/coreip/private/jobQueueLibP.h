/* jobQueueLibP.h - private header file for jobQueueLib */

/*
 * Copyright (c) 2005 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01b,13jul05,dlk  Added taskId member to JOB_QUEUE.
01a,06may05,dlk  Written.
*/

/* Note, jobQueueLib.h should be included before this private header */

#ifndef __INCjobQueueLibPh
#define __INCjobQueueLibPh

#include <semLib.h>
#include <private/semLibP.h>
#include <setjmp.h>

#ifdef __cplusplus
extern "C" {
#endif

#define QJOB_ENQUEUED	0x100
#define QJOB_POOL_ALLOC 0x200

typedef struct _QJOB_QUEUE_HEAD
    {
    QJOB * pHead;
    QJOB ** ppTail;
    } QJOB_QUEUE_HEAD;

typedef struct _QJOB_FREE_HEAD
    {
    QJOB_STANDARD * pHead;
    } QJOB_FREE_HEAD;

typedef QJOB_FREE_HEAD QJOB_BLOCK_HEAD; /* [;^] */

typedef struct _QJOB_STD_POOL
    {
    QJOB_FREE_HEAD  free;		/* The free list of standard jobs */
    QJOB_BLOCK_HEAD blocks;		/* The list of blocks of std. jobs */
    UINT32 refCount;			/* How many attached job queues? */
    } QJOB_STD_POOL;

typedef struct _QUIT_QJOB
    {
    QJOB job;
    volatile int * pQuitValue;
    jmp_buf * pJmpBuf;
    VOIDFUNCPTR qFunc;
    void * arg1;
    void * arg2;
    } QUIT_QJOB;

typedef struct _JOB_QUEUE
    {
    QJOB_QUEUE_HEAD	jobs [32];	/* The enqueued jobs */
    UINT32		activePri;	/* which queues hold jobs? */
    QJOB_STD_POOL *	pStdPool;	/* pool for standard jobs */
#ifdef JOBQ_PRI_MASKABLE
    UINT32		priMask;	/* which priorities are enabled? */
#endif
    SEMAPHORE		sem;		/* binary semaphore */
    QJOB_BLOCK_HEAD	blocks;		/* list of blocks of standard jobs */
    UINT32		flags;		/* Is queue in use? */
#define JOBQ_FLG_RUNNING	0x1
#define JOBQ_FLG_CREATED	0x2
    int			taskId;		/* Task currently running */
					/* jobQueueProcess(). */

    QUIT_QJOB		quitJob;
    void *		userVal;	/* reserved for queue creator */
    } JOB_QUEUE;

#define QUIT_JOB_TO_JOB_QUEUE(q) \
    ((JOB_QUEUE *)((char *)(q) - offsetof (JOB_QUEUE, quitJob)))

#ifdef __cplusplus
}
#endif

#endif /* __INCjobQueueLibPh */
