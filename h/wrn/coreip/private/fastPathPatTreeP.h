/* fastPathPatTreeP.h - Private definitions for the Fastpath cache  */

/* Copyright 1984-2001 Wind River Systems, Inc. */

/*
modification history
--------------------
01g,04nov03,rlm  Ran batch header path update for header re-org.
01f,18dec02,vvv  updated the rnh_matchaddr call to reflect the modified API
01e,07aug01,niq  Replace Free with KHEAP_FREE
01d,02may01,niq  Correcting mod history
01c,01may01,niq  Copied over from tor2_0.open_stack branch
01b,20mar01,niq  Adding comments
01a,29jan01,niq  written
*/

/*
DESCRIPTION
 
This include file contains private definitions used by the 
Patricia tree Fastpath cache implementation (fastPathPatTree.c)
 
INCLUDE FILES:
*/

#ifndef __INCfastPathPatTreePh
#define __INCfastPathPatTreePh

/* includes */

#include <net/radix.h>
#include <net/route.h>


/* typedefs */

typedef struct rtentry		CACHE_ENTRY;
typedef CACHE_ENTRY *		CACHE_ENTRY_ID;
typedef struct radix_node_head	CACHE_COOKIE;
typedef CACHE_COOKIE *		CACHE_COOKIE_ID;


/* defines */

/* Definitions for CACHE_ENTRY_ALLOC flags - bitwise OR values */

#define ZERO_IT			1
#define ALLOC_DEST_ADDR		2
#define ALLOC_GATE_ADDR		4

/* Definitions for Cache management */

#define CACHE_ENTRY_SIZE		sizeof(CACHE_ENTRY)

#define CACHE_TABLE_INIT(pCacheCookie, keyLength) \
    (pCacheCookie = NULL, rn_inithead (&pCacheCookie, keyLength))

#define CACHE_TABLE_DESTROY(pCacheCookie) \
    rn_destroyhead (pCacheCookie)

#define CACHE_TABLE_ENTRY_ALLOC(pCacheEntry, flags) \
    (pCacheEntry = ptCacheEntryAlloc (flags))        

#define CACHE_TABLE_ENTRY_FREE(pCacheEntry) \
    { \
    if (GET_CACHE_ENTRY_DEST_ADDR (pCacheEntry)) \
	KHEAP_FREE ((char *)GET_CACHE_ENTRY_DEST_ADDR (pCacheEntry)); \
    if (GET_CACHE_ENTRY_GATE_ADDR (pCacheEntry)) \
	KHEAP_FREE ((char *)GET_CACHE_ENTRY_GATE_ADDR (pCacheEntry)); \
    KHEAP_FREE ((char *)pCacheEntry); \
    }

#define CACHE_TABLE_ENTRY_ADD(pCacheCookie, pCacheEntry, pDstAddr, pNetMask) \
    (pCacheCookie->rnh_addaddr ((caddr_t)pDstAddr, pNetMask, \
				pCacheCookie, pCacheEntry->rt_nodes))

#define CACHE_TABLE_ENTRY_DEL(pCacheCookie, pDstAddr, pNetMask) \
    (pCacheCookie->rnh_deladdr (pDstAddr, pNetMask, pCacheCookie))

#define CACHE_TABLE_ENTRY_MATCHADDR(pCacheEntry, pCacheCookie, pDstIpAddr) \
    { \
    pCacheEntry = (CACHE_ENTRY_ID)(pCacheCookie->rnh_matchaddr (\
						(caddr_t)pDstIpAddr, \
						pCacheCookie, 0)); \
    /* Make sure it is not the ROOT node */ \
    if (pCacheEntry && ((struct radix_node *)pCacheEntry)->rn_flags & RNF_ROOT)\
        pCacheEntry = NULL; \
    }

#define CACHE_TABLE_ENTRY_LOOKUP(pCacheEntry, pCacheCookie, pDstIpAddr, pNetmask) \
    { \
    pCacheEntry = (CACHE_ENTRY_ID)(pCacheCookie->rnh_lookup (\
						(caddr_t)pDstIpAddr,\
						pNetmask, \
						pCacheCookie)); \
    /* Make sure it is not the ROOT node */ \
    if (pCacheEntry && ((struct radix_node *)pCacheEntry)->rn_flags & RNF_ROOT)\
        pCacheEntry = NULL; \
    }
	
#define CACHE_TABLE_WALK(pCacheCookie, pUsrFunc, pArg) \
    rn_walktree (pCacheCookie, pUsrFunc, pArg)

#endif /* __INCfastPathPatTreePh */
