/* fastPathLibP.h - Private definitions for the Fastpath library  */

/* Copyright 1984-2003 Wind River Systems, Inc. */

/*
modification history
--------------------
01e,20nov03,niq  Remove copyright_wrs.h file inclusion
01d,11nov03,cdw  Removal of unnecessary _KERNEL guards.
01c,06nov03,rlm  Ran batch header update for header re-org.
01b,03apr03,niq  Add support for enabling Fastpath v4/v6 separately on an
		 interface
01a,07oct02,deg  written
*/

/*
DESCRIPTION
This file contains private definitions used only by the Fastpath library.
 
*/

#ifndef __INCfastPathLibPh
#define __INCfastPathLibPh

/* includes */

#include <net/if.h> /* acc_deg : For definition of IFF_FP_ENABLE */

/* defines */

#define FF_OBJ_SIZE sizeof(struct ffObj) 
#define FF_NAME_MAX 80	                 /* Max name len of a Fast Forwarder */

/* Arguments to the FIB print command (What types of entries to print) */

#define FF_MAC      0x01
#define FF_INDIRECT 0x02

/* Fastpath state definitions - bitwise OR values */

#define FF_STARTED	0x01 /* Fastpath module has been started */
#define FF_ENABLED	0x02 /* Packet fastforwarding has been enabled */
#define FF_IPFF_ENABLED	0x04 /* IP Packet forwarding has been enabled */

/* Macros to manipulate the state of a Fastpath module */

#define FF_MOD_START(pFFObj)	    pFFObj->ffState |= FF_STARTED
#define FF_MOD_STOP(pFFObj)	    pFFObj->ffState &= ~FF_STARTED
#define FF_MOD_ENABLE(pFFObj)	    pFFObj->ffState |= FF_ENABLED
#define FF_MOD_DISABLE(pFFObj)	    pFFObj->ffState &= ~FF_ENABLED
#define FF_MOD_IPFF_ENABLE(pFFObj)  pFFObj->ffState |= FF_IPFF_ENABLED
#define FF_MOD_IPFF_DISABLE(pFFObj) pFFObj->ffState &= ~FF_IPFF_ENABLED

/* Macros to query the state of a Fast Forwarder module */

#define FF_MOD_STARTED(pFFObj)	     (pFFObj->ffState & FF_STARTED)
#define FF_MOD_STOPPED(pFFObj)	     ((pFFObj->ffState & FF_STARTED) == 0)
#define FF_MOD_ENABLED(pFFObj)	     (pFFObj->ffState & FF_ENABLED)
#define FF_MOD_DISABLED(pFFObj)	     ((pFFObj->ffState & FF_ENABLED) == 0)
#define FF_MOD_IPFF_ENABLED(pFFObj)  (pFFObj->ffState & FF_IPFF_ENABLED)
#define FF_MOD_IPFF_DISABLED(pFFObj) ((pFFObj->ffState & FF_IPFF_ENABLED) == 0)

/* 
 * Macros to manipulate the state of an Interface - Whether fastpath 
 * is enabled on an interface
 */

/* Macros to query the state of an Interface */

#define FF_INTF_ENABLED(pFFObj, pIfp)  \
    ((pFFObj->ffProto == FF_PROTO_IPV4) ? \
     ((pIfp->if_flags & IFF_FPV4_ENABLE) ==   IFF_FPV4_ENABLE) : \
     ((pIfp->if_flags & IFF_FPV6_ENABLE) ==   IFF_FPV6_ENABLE))

#define FF_INTF_DISABLED(pFFObj, pIfp)	(!(FF_INTF_DISABLED(pFFObj, pIfp)))

/* 
 * Macros to manipulate the status of an Interface - Whether interface 
 * is up or down. 
 */

/* Macros to query the status of an Interface */

#define IS_FF_INTF_UP(pFFObj, pIfp)  \
    ((pFFObj->ffProto == FF_PROTO_IPV4) ? \
     ((pIfp->if_flags & (IFF_INET_UP | IFF_UP)) == (IFF_INET_UP | IFF_UP)) : \
     ((pIfp->if_flags & (IFF_INET6_UP | IFF_UP)) == (IFF_INET6_UP | IFF_UP)))

#define IS_FF_INTF_DOWN(pFFObj, pIfp) (!(IS_FF_INTF_UP(pFFObj, pIfp))) 

/* typedefs */

/* Argument passed to the FIB dump routine */

typedef struct ffDumpArg
{
  unsigned char *pMemPtr;
  int 		 memSize;
} FF_DUMP_ARG;

#endif /* __INCffastPathLibPh */
