/* wvNetDEventP.h - header file for WindView WindNet IPv6/IPv4 event ids */

/* Copyright 2003 - 2004 Wind River Systems, Inc. */

/*
modification history
--------------------
01l,07jul04,vvv  fixed warnings (statement not reached)
01k,02apr04,spm  fixed virtual stack build: enabled required values
01j,24feb04,syy  Update wvNetDEventInfo structure
01i,23jan04,syy  Added WV class check to macros
01h,17dec03,syy  Remove evtLogOBinary() workaround
01g,16dec03,syy  Fixed a typo that caused build problem
01f,11dec03,syy  Code adjustment and clean up
01e,08dec03,syy  Networking WindView instrumentation merge
01d,04nov03,syy  WV Instrumentation changes
01c,20oct03,syy  Added #ifdef WV_INSTRUMENTATION
01b,04sep03,tar  Windview instrumentation codereview changes
01a,05aug03,tar  written
*/

#ifndef __INCwvNetDEventPh
#define __INCwvNetDEventPh

#ifdef __cplusplus
extern "C" {
#endif

/* Routines for selecting events based on IP addresses/ or port numbers. */

IMPORT FUNCPTR _func_wvNetAddressFilterTest; 
IMPORT FUNCPTR _func_wvNetPortFilterTest;
    
#define MIN_WVNET_EVENTID 	20000  
#define MAX_WVNET_EVENTID 	24999

#define WVNET_EVENT(id) 	(MIN_WVNET_EVENTID + id)

#define WVNET_MASKSIZE 	8 	/* Allows 57 to 64 events per level. */

/*
 * The WV_BLOCK_START macro prevents wasted cycles by determining if
 * WindView is running and if the given class has been selected. It
 * provides initial screening for reporting network events to WindView.
 * WV_CLASS_3 is for "Additional Instrumentation" selection.
 * The classId is NETD_IP4_DATAPATH_EVENT, NETD_IP4_CTRLPATH_EVENT,
 * NETD_IP6_DATAPATH_EVENT or NETD_IP6_CTRLPATH_EVENT.
 */

#define WV_BLOCK_START(classId)    \
    if (ACTION_IS_SET)    \
        {    \
        if (WV_EVTCLASS_IS_SET (WV_CLASS_3 | classId | WV_ON))    \
            {
/*
 * The WV_NETD_EVENT_TEST macro evaluates to TRUE if the event is selected
 * for inclusion in the WindView updates sent to the host. The event
 * selection status is stored in a bitmap accessed by priority level.
 * This macro is only valid within a test conditional after the
 * WV_BLOCK_START macro verifies that the class is active. The eventLevel
 * parameter has values determined by the constants defined below, and
 * the offset indicates the ordinal number of the event for the given
 * level (ranging from 0 upward).
 */

#define WV_NETD_EVENT_TEST(eventLevel, offset) \
 pWvNetEventMap [eventLevel - 1].bitmap [WVNET_MASKSIZE - 1 - (offset / 8)] & \
                                                           (1 << (offset % 8))

/*
 * The WV_BLOCK_END provides a footer for all code which reports network
 * events to WindView. It closes the open scopes caused by WV_BLOCK_START.
 * The classId parameter is not used, but is included for symmetry.
 */

#define WV_BLOCK_END(classId)    \
            }    \
        }

#if defined (WV_INSTRUMENTATION) && defined (INCLUDE_WVNETD) 

/* Temp workaround for missing evtLogOString() in evtLogLib.c */
#ifndef WVLOGAPI_NEEDED
#define WV_EVT_LOG_OBIN (* _func_evtLogOBinary)
#else    
IMPORT void evtLogOBinary (event_t action, int nParam, int arg1, int arg2, int arg3,
                           int arg4, int arg5, int arg6, int payloadSize,
                           const char *payload);    
#define WV_EVT_LOG_OBIN evtLogOBinary
#endif /* !WVLOGAPI_NEEDED */
    
LOCAL UINT32 wvNetDEventInfo;       /* Event information: see wvNetDLib.h */
    
/*
 * Event data binary block consists of three parts: the data format ID,
 * two optional parameters, each can be either non IP address or IP address.
 * The interpretation of the event data is governed by the format ID (the
 * first byte). Various formats are defined in the section below.
 *
 *   wvNetDEventBlob [0]   - event data format 
 *   wvNetDEventBlob [1-4] - first event data in UINT32 (optional)
 *   wvNetDEventBlob [5-8] - second event data in UINT32 (optional)
 */
LOCAL UCHAR  wvNetDEventBlob[12];   

/*
 * Event data format indicator bit definitions
 */
#define WV_NETD_EVT_PARAM_ONE    0x01
#define WV_NETD_EVT_PARAM_TWO    0x02
#define WV_NETD_EVT_V4_ADDR      0x04
#define WV_NETD_EVT_V6_ADDR      0x08

/*
 * List of allowed event data formats:
 *
 * WV_NETD_EVT_FMT_1P: one non- IP address parameter;
 * WV_NETD_EVT_FMT_2P: two  that non- IP address parameters;
 * WV_NETD_EVT_FMT_1P_V4: one non- IP address parameter and one IPv4 address;
 * WV_NETD_EVT_FMT_1P_V6: one non- IP address parameter and one ptr to the 
 *                        IPv6 address structure;
 * WV_NETD_EVT_FMT_V4: two IPv4 addresses;
 * WV_NETD_EVT_FMT_V6: two ptr's to the IPv6 addresse structures;
 */
#define WV_NETD_EVT_FMT_1P       WV_NETD_EVT_PARAM_ONE
#define WV_NETD_EVT_FMT_2P       WV_NETD_EVT_PARAM_ONE | WV_NETD_EVT_PARAM_TWO
#define WV_NETD_EVT_FMT_1P_V4    WV_NETD_EVT_PARAM_ONE | WV_NETD_EVT_V4_ADDR
#define WV_NETD_EVT_FMT_1P_V6    WV_NETD_EVT_PARAM_ONE | WV_NETD_EVT_V6_ADDR
#define WV_NETD_EVT_FMT_V4       WV_NETD_EVT_V4_ADDR
#define WV_NETD_EVT_FMT_V6       WV_NETD_EVT_V6_ADDR
    
/*
 * The following macros accept an argument of struct sockaddr type and return the
 * sockaddr_in for IPv4 and return the address of the struct for IPv6.
 */   
#define WV_SOCKADDR_IN(s)  (((struct sockaddr_in *)(s))->sin_addr.s_addr)
    
#ifdef INET6
#define WV_SOCKADDR_IN6(s)   (((struct sockaddr_in6 *)(s))->sin6_addr)
    
#define WV_ADDR_GET(pAddr) (pAddr == NULL ? 0 : \
   (((struct sockaddr *)pAddr)->sa_family == AF_INET ? \
    (ULONG *) &WV_SOCKADDR_IN(pAddr) : \
     (((struct sockaddr *)pAddr)->sa_family == AF_INET6 ? \
      (ULONG *) &WV_SOCKADDR_IN6(pAddr) : 0)))
 
#define WV_ADDR_FAMILY_GET(pAddr) (pAddr == NULL ? 0: \
   (((struct sockaddr *)pAddr)->sa_family == AF_INET ? \
    WV_NETD_EVT_V4_ADDR : WV_NETD_EVT_V6_ADDR))
#else
#define WV_ADDR_GET(pAddr) (pAddr == NULL ? 0 : \
   (((struct sockaddr *)pAddr)->sa_family == AF_INET ? \
    (((struct sockaddr_in *)pAddr)->sin_addr.s_addr) : 0))

#define WV_ADDR_FAMILY_GET(pAddr)  (pAddr == NULL ? 0 : \
   (((struct sockaddr *)pAddr)->sa_family == AF_INET ? \
    WV_NETD_EVT_V4_ADDR : 0))
#endif /* INET6 */    

/*
 * The WV_EVT_INFO_SET_ALL macro constructs the WindView event information
 * field in the event data using the following structure:
 *
 * <Component ID>:<Status>:<Filter ID>:<Entity ID>:<Module ID>:<Event Tag>
 *    (2 bits)    (2 bits)   (4 bits)   (8 bits)     (8 bits)   (8 bits)
 *
 * The component ID is a fixed value for all WindNet IPv6/IPv4 stack code, The
 * values of other fields, except Event Tag, use the constants defined
 * below. The packet filter type (Filter ID), Entity ID and Module ID are
 * assigned using local #defin'se within each module. The final field (Event
 * Tag) is a unique identifier for each event within a given module.
 *
 * Currently, the packet filter is not used. But it may be used for refined
 * event filtering someday.
 *
 * The resulting event information is stored in the ULONG variable
 * wvNetDEventInfo, local to the file module.
 */

#define WV_EVT_INFO_SET_ALL(eventLevel, eventTag, status)                  \
     wvNetDEventInfo = (WV_NET_DUAL_ID << 30) | (status << 28)             \
                      | (wvNetDLocalFilter << 24) | (wvNetDEntityId << 16) \
                      |  (wvNetDModuleId << 8) | eventTag;

/* Macro sets evenInfo fields, except "Status" */    
#define WV_EVT_INFO_SET(eventLevel, eventTag)                              \
     wvNetDEventInfo = (WV_NET_DUAL_ID << 30) | (wvNetDLocalFilter << 24)  \
                      | (wvNetDEntityId << 16) | (wvNetDModuleId << 8)     \
                      | eventTag;

/* Store the 4 bytes of an UINT32 into blob */
#define WV_ASSIGN_BLOB(blob, arg) \
         *(&(blob)) = ((((UINT32) arg) >> 24) & 0xff); \
     *(&(blob) + 1) = ((((UINT32) arg) >> 16) & 0xff); \
     *(&(blob) + 2) = ((((UINT32) arg) >>  8) & 0xff); \
     *(&(blob) + 3) = ((((UINT32) arg) >>  0) & 0xff); \
     *(&(blob) + 4) = '\n';
 
/*
 * To minimize the size of the log, for V6 address, only the ptr to struct
 * sockadd will be logged.
 *
 * format: 0x1 - one parameter; 0x3 - two parameters; 
 */
#define WV_EVT_BLOB_SET0(format, arg1, arg2)                 \
  do {                                                       \
       wvNetDEventBlob[0] = (UCHAR) format;                  \
       if ((format) & WV_NETD_EVT_PARAM_ONE)                 \
           WV_ASSIGN_BLOB(wvNetDEventBlob[1], arg1)          \
       if ((format) & WV_NETD_EVT_PARAM_TWO)                 \
           WV_ASSIGN_BLOB(wvNetDEventBlob[5], arg2)          \
      } while (0);

/* for one parameter + one address */    
#define WV_EVT_BLOB_SET1(twoArgs, format, arg1, arg2)        \
  do {                                                       \
       wvNetDEventBlob [0] = (UCHAR) format;                 \
       WV_ASSIGN_BLOB(wvNetDEventBlob[1], arg1)              \
       if (twoArgs)                                          \
           WV_ASSIGN_BLOB(wvNetDEventBlob[5], arg2)          \
     } while (0);

/* 
 * This is same as WV_EVT_BLOB_SET1 except that it assumes twoArgs
 * to be false. It is required to eliminate some compiler warnings.
 */

#define WV_EVT_BLOB_SET1_FALSE(format, arg1, arg2)           \
  do {                                                       \
       wvNetDEventBlob [0] = (UCHAR) format;                 \
       WV_ASSIGN_BLOB(wvNetDEventBlob[1], arg1)              \
     } while (0);

/*
 * This macro is only used in places where the 2nd argument can be either v4
 * or v6 address.  The address passed in must be in struct sockaddr type.
 */
#define WV_EVT_BLOB_SET2(twoArgs, arg1, arg2)                       \
  do {                                                              \
       wvNetDEventBlob [0] = (UCHAR) WV_NETD_EVT_FMT_1P;            \
       WV_ASSIGN_BLOB(wvNetDEventBlob[1], arg1)                     \
    if (twoArgs)                                                    \
           {                                                        \
           wvNetDEventBlob [0] |= (UCHAR) WV_ADDR_FAMILY_GET(arg2); \
           WV_ASSIGN_BLOB(wvNetDEventBlob[5], WV_ADDR_GET(arg2))    \
           }                                                        \
      } while (0);
    
/*
 * This macro is only used in places where the both arguments can be either v4
 * or v6 addresses. Assume that the two arguments are of same address type and
 * only the first can be "0" value. The address passed in must be in struct
 * sockaddr type.
 */
#define WV_EVT_BLOB_SET3(twoArgs, arg1, arg2)                       \
  do {                                                              \
       wvNetDEventBlob [0] = (UCHAR) WV_ADDR_FAMILY_GET(arg1);      \
       WV_ASSIGN_BLOB(wvNetDEventBlob[1], WV_ADDR_GET (arg1))       \
       if (twoArgs)                                                 \
           {                                                        \
           if (wvNetDEventBlob [0] == 0)                            \
               wvNetDEventBlob [0] = (UCHAR) WV_ADDR_FAMILY_GET(arg2);\
           WV_ASSIGN_BLOB(wvNetDEventBlob[5], WV_ADDR_GET(arg2))    \
           }                                                        \
     } while (0);
    
/* 
 * This is same as WV_EVT_BLOB_SET3 except that it assumes twoArgs
 * to be false. It is required to eliminate some compiler warnings.
 */

#define WV_EVT_BLOB_SET3_FALSE(arg1, arg2)                          \
  do {                                                              \
       wvNetDEventBlob [0] = (UCHAR) WV_ADDR_FAMILY_GET(arg1);      \
       WV_ASSIGN_BLOB(wvNetDEventBlob[1], WV_ADDR_GET (arg1))       \
     } while (0);
    
/*
 * The WindView for networking marker events handles cases which do not
 * involve data transfer. The transmission direction (i.e. - status) field
 * in the event identifier is always 0. Up to four additional parameters
 * may be sent to the host.
 */
    
#define WV_NET_MARKER_1(classId, eventLevel, eventOffset, eventTag, eventId, arg1) \
    WV_BLOCK_START (classId) \
    if (WV_NETD_EVENT_TEST (eventLevel, eventOffset)) \
        {    \
        WV_EVT_INFO_SET (eventLevel, eventTag) \
        WV_EVT_LOG_OBIN (eventId, 3, wvNetDEventInfo, (int)arg1, 0, 0, 0, 0, 0, NULL); \
        } \
    WV_BLOCK_END (classId)

#define WV_NET_MARKER_2(classId, eventLevel, eventOffset, eventTag, eventId, arg1, arg2) \
    WV_BLOCK_START (classId) \
    if (WV_NETD_EVENT_TEST (eventLevel, eventOffset)) \
        {    \
        WV_EVT_INFO_SET (eventLevel, eventTag) \
        WV_EVT_LOG_OBIN (eventId, 3, wvNetDEventInfo, (int)arg1, (int)arg2, 0, 0, 0, 0, NULL); \
        } \
    WV_BLOCK_END (classId)

/* when the last argument is a parameter */
#define WV_NET_MARKER_3(classId, eventLevel, eventOffset, eventTag, eventId, arg1, arg2, arg3) \
    WV_BLOCK_START (classId) \
    if (WV_NETD_EVENT_TEST (eventLevel, eventOffset)) \
        {    \
	int format = WV_NETD_EVT_FMT_1P; \
        WV_EVT_INFO_SET (eventLevel, eventTag) \
        WV_EVT_BLOB_SET0 (format, arg3, 0) \
        WV_EVT_LOG_OBIN (eventId, 3, wvNetDEventInfo, (int)arg1, (int)arg2, 0, 0, 0, \
                         sizeof (wvNetDEventBlob), (char *)wvNetDEventBlob); \
        } \
    WV_BLOCK_END (classId)

/* when the last argument is a V4 address */
#define WV_NET_MARKER_3_4(classId, eventLevel, eventOffset, eventTag, eventId, arg1, arg2, arg3) \
    WV_NET_MARKER_30 (classId, eventLevel, eventOffset, eventTag, eventId, WV_NETD_EVT_FMT_V4, arg1, arg2, arg3)

/* when the last argument is a V6 address */
#define WV_NET_MARKER_3_6(classId, eventLevel, eventOffset, eventTag, eventId, arg1, arg2, arg3) \
    WV_NET_MARKER_30 (classId, eventLevel, eventOffset, eventTag, eventId, WV_NETD_EVT_FMT_V6, arg1, arg2, arg3)

/* when the last argument is a V4 or a V6 address */    
#define WV_NET_MARKER_3_0(classId, eventLevel, eventOffset, eventTag, eventId, arg1, arg2, arg3) \
    WV_BLOCK_START (classId) \
    if (WV_NETD_EVENT_TEST (eventLevel, eventOffset)) \
        {    \
        WV_EVT_INFO_SET (eventLevel, eventTag) \
        WV_EVT_BLOB_SET3_FALSE (arg3, 0) \
        WV_EVT_LOG_OBIN (eventId, 3, wvNetDEventInfo, (int)arg1, (int)arg2, 0, 0, 0, \
                         sizeof (wvNetDEventBlob), (char *)wvNetDEventBlob); \
        } \
    WV_BLOCK_END (classId)

#define WV_NET_MARKER_30(classId, eventLevel, eventOffset, eventTag, eventId, format, arg1, arg2, arg3) \
    WV_BLOCK_START (classId) \
    if (WV_NETD_EVENT_TEST (eventLevel, eventOffset)) \
        {    \
        WV_EVT_INFO_SET (eventLevel, eventTag) \
        WV_EVT_BLOB_SET1_FALSE (format, arg3, 0) \
        WV_EVT_LOG_OBIN (eventId, 3, wvNetDEventInfo, (int)arg1, (int)arg2, 0, 0, 0, \
                         sizeof (wvNetDEventBlob), (char *)wvNetDEventBlob); \
        } \
    WV_BLOCK_END (classId)
    
/* when the last two arguments are parameters */
#define WV_NET_MARKER_4(classId, eventLevel, eventOffset, eventTag, eventId, arg1, arg2, arg3, arg4) \
    WV_BLOCK_START (classId) \
    if (WV_NETD_EVENT_TEST (eventLevel, eventOffset)) \
        {    \
        WV_EVT_INFO_SET (eventLevel, eventTag) \
        WV_EVT_BLOB_SET0 (WV_NETD_EVT_FMT_2P, arg3, arg4) \
        WV_EVT_LOG_OBIN (eventId, 3, wvNetDEventInfo, (int)arg1, (int)arg2, 0, 0, 0, \
                         sizeof (wvNetDEventBlob), (char *)wvNetDEventBlob); \
        } \
    WV_BLOCK_END (classId)

/* when the last two arguments consist of one parameter and one V4 address */
#define WV_NET_MARKER_4_14(classId, eventLevel, eventOffset, eventTag, eventId, arg1, arg2, arg3, arg4) \
    WV_NET_MARKER_40(classId, eventLevel, eventOffset, eventTag, eventId, WV_NETD_EVT_FMT_1P_V4, arg1, arg2, arg3, arg4)
    
/* when the last two arguments consist of one parameter and one V4 or V6 address */
#define WV_NET_MARKER_4_21(classId, eventLevel, eventOffset, eventTag, eventId, arg1, arg2, arg3, arg4) \
    WV_BLOCK_START (classId) \
    if (WV_NETD_EVENT_TEST (eventLevel, eventOffset)) \
        {    \
        WV_EVT_INFO_SET (eventLevel, eventTag) \
        WV_EVT_BLOB_SET2 (TRUE, arg3, arg4) \
        WV_EVT_LOG_OBIN (eventId, 3, wvNetDEventInfo, (int)arg1, (int)arg2, 0, 0, 0, \
                         sizeof (wvNetDEventBlob), (char *)wvNetDEventBlob); \
        } \
    WV_BLOCK_END (classId)

/* when the last two arguments are V4 or V6 IP addresses */
#define WV_NET_MARKER_4_22(classId, eventLevel, eventOffset, eventTag, eventId, arg1, arg2, arg3, arg4) \
    WV_BLOCK_START (classId) \
    if (WV_NETD_EVENT_TEST (eventLevel, eventOffset)) \
        {    \
        WV_EVT_INFO_SET (eventLevel, eventTag) \
        WV_EVT_BLOB_SET3 (TRUE, arg3, arg4) \
        WV_EVT_LOG_OBIN (eventId, 3, wvNetDEventInfo, (int)arg1, (int)arg2, 0, 0, 0, \
                         sizeof (wvNetDEventBlob), (char *)wvNetDEventBlob); \
        } \
    WV_BLOCK_END (classId)

#define WV_NET_MARKER_40(classId, eventLevel, eventOffset, eventTag, eventId, format, arg1, arg2, arg3, arg4) \
    WV_BLOCK_START (classId) \
    if (WV_NETD_EVENT_TEST (eventLevel, eventOffset)) \
        {    \
        WV_EVT_INFO_SET (eventLevel, eventTag) \
        WV_EVT_BLOB_SET1 (TRUE, format, arg3, arg4) \
        WV_EVT_LOG_OBIN (eventId, 3, wvNetDEventInfo, (int)arg1, (int)arg2, 0, 0, 0, \
                         sizeof (wvNetDEventBlob), (char *)wvNetDEventBlob); \
        } \
    WV_BLOCK_END (classId)

     /*
      * These marker events use the filter which tests for a particular
      * source and destination port for outgoing packets. If the filter is
      * enabled, the event is not reported to the host unless the ports match
      * the registered values.
      */

#define WV_NET_PORTOUT_MARKER_3(classId, eventLevel, eventOffset, eventTag, srcPort, dstPort, eventId, arg1, arg2, arg3) \
    WV_BLOCK_START (classId) \
    if (WV_NETD_EVENT_TEST (eventLevel, eventOffset) && \
        (* _func_wvNetPortFilterTest) (1, srcPort, dstPort)) \
        {    \
	int format = WV_NETD_EVT_FMT_1P; \
        WV_EVT_INFO_SET (eventLevel, eventTag) \
        WV_EVT_BLOB_SET0 (format, arg3, 0) \
        WV_EVT_LOG_OBIN (eventId, 3, wvNetDEventInfo, (int)arg1, (int)arg2, 0, 0, 0, \
                         sizeof (wvNetDEventBlob), (char *)wvNetDEventBlob); \
        } \
    WV_BLOCK_END (classId)    

     /* 
      * This marker event uses the filter which tests for a particular 
      * destination address for outgoing packets. If the filter is
      * enabled, the event is not reported to the host unless the address
      * matches the registered value.
      */
#define WV_NET_DSTADDROUT_MARKER_3(classId, eventLevel, eventOffset, eventTag, dstAddr, eventId, arg1, arg2, arg3) \
    WV_BLOCK_START (classId) \
    if (WV_NETD_EVENT_TEST (eventLevel, eventOffset) && \
        (* _func_wvNetAddressFilterTest) (1, 1, 0, dstAddr)) \
        {    \
	int format = WV_NETD_EVT_FMT_1P; \
        WV_EVT_INFO_SET(eventLevel, eventTag) \
        WV_EVT_BLOB_SET0(format, arg3, 0) \
        WV_EVT_LOG_OBIN (eventId, 3, wvNetDEventInfo, (int)arg1, (int)arg2, 0, 0, 0, \
                         sizeof (wvNetDEventBlob), (char *)wvNetDEventBlob); \
        } \
    WV_BLOCK_END (classId)

#define WV_NET_DSTADDROUT_MARKER_3_4(classId, eventLevel, eventOffset, eventTag, dstAddr, eventId, arg1, arg2, arg3) \
    WV_NET_DSTADDROUT_MARKER_30 (classId, eventLevel, eventOffset, eventTag, dstAddr, eventId, WV_NETD_EVT_FMT_V4, arg1, arg2, arg3)
    
#define WV_NET_DSTADDROUT_MARKER_30(classId, eventLevel, eventOffset, eventTag, dstAddr, eventId, format, arg1, arg2, arg3) \
    WV_BLOCK_START (classId) \
    if (WV_NETD_EVENT_TEST (eventLevel, eventOffset) && \
        (* _func_wvNetAddressFilterTest) (1, 1, 0, dstAddr)) \
        {    \
        WV_EVT_INFO_SET(eventLevel, eventTag) \
        WV_EVT_BLOB_SET1_FALSE (format, arg3, 0) \
        WV_EVT_LOG_OBIN (eventId, 3, wvNetDEventInfo, (int)arg1, (int)arg2, 0, 0, 0, \
                         sizeof (wvNetDEventBlob), (char *)wvNetDEventBlob); \
        } \
    WV_BLOCK_END (classId)

#define WV_NET_ADDRIN_MARKER_4_4(classId, eventLevel, eventOffset, eventTag, srcAddr, dstAddr, eventId, arg1, arg2, arg3, arg4) \
    WV_NET_ADDRIN_MARKER_40(classId, eventLevel, eventOffset, eventTag, srcAddr, dstAddr, eventId, WV_NETD_EVT_FMT_V4, arg1, arg2, arg3, arg4)
   
#define WV_NET_ADDRIN_MARKER_40(classId, eventLevel, eventOffset, eventTag, srcAddr, dstAddr, eventId, format, arg1, arg2, arg3, arg4) \
    WV_BLOCK_START (classId) \
    if (WV_NETD_EVENT_TEST (eventLevel, eventOffset) && \
        (* _func_wvNetAddressFilterTest) (0, 3, srcAddr, dstAddr)) \
        {    \
        WV_EVT_INFO_SET (eventLevel, eventTag) \
        WV_EVT_BLOB_SET1 (TRUE, format, arg3, arg4) \
        WV_EVT_LOG_OBIN (eventId, 3, wvNetDEventInfo, (int)arg1, (int)arg2, 0, 0, 0, \
                         sizeof (wvNetDEventBlob), (char *)wvNetDEventBlob); \
        } \
    WV_BLOCK_END (classId)

/*
 * The WindView for networking data transfer events store a transmission
 * direction in the status field of the event identifier. Valid values are
 * WV_NET_SEND and WV_NET_RECV. Up to four additional parameters may be sent
 * to the host.
 */

#define WV_NET_EVENT_1(classId, eventLevel, eventOffset, eventTag, eventId, status, arg1) \
    WV_BLOCK_START (classId) \
    if (WV_NETD_EVENT_TEST (eventLevel, eventOffset)) \
        {    \
        WV_EVT_INFO_SET_ALL (eventLevel, eventTag, status) \
        WV_EVT_LOG_OBIN (eventId, 3, wvNetDEventInfo, (int)arg1, 0, 0, 0, 0, 0, NULL); \
        } \
    WV_BLOCK_END (classId)

#define WV_NET_EVENT_2(classId, eventLevel, eventOffset, eventTag, eventId, status, arg1, arg2) \
    WV_BLOCK_START (classId) \
    if (WV_NETD_EVENT_TEST (eventLevel, eventOffset)) \
        {    \
        WV_EVT_INFO_SET_ALL (eventLevel, eventTag, status) \
        WV_EVT_LOG_OBIN (eventId, 3, wvNetDEventInfo, (int)arg1, (int)arg2, 0, 0, 0, 0, NULL); \
        } \
    WV_BLOCK_END (classId)

#define WV_NET_EVENT_3(classId, eventLevel, eventOffset, eventTag, eventId, status, arg1, arg2, arg3) \
    WV_BLOCK_START (classId) \
    if (WV_NETD_EVENT_TEST (eventLevel, eventOffset)) \
        {    \
	int format = WV_NETD_EVT_FMT_1P; \
        WV_EVT_INFO_SET_ALL (eventLevel, eventTag, status) \
        WV_EVT_BLOB_SET0 (format, arg3, 0) \
        WV_EVT_LOG_OBIN (eventId, 3, wvNetDEventInfo, (int)arg1, (int)arg2, 0, 0, 0, \
                         sizeof (wvNetDEventBlob), (char *)wvNetDEventBlob); \
        } \
    WV_BLOCK_END (classId)

#define WV_NET_EVENT_3_4(classId, eventLevel, eventOffset, eventTag, eventId, status, arg1, arg2, arg3) \
    WV_NET_EVENT_30 (classId, eventLevel, eventOffset, eventTag, eventId, status, WV_NETD_EVT_FMT_V4, arg1, arg2, arg3)

#define WV_NET_EVENT_30(classId, eventLevel, eventOffset, eventTag, eventId, status, format, arg1, arg2, arg3) \
    WV_BLOCK_START (classId) \
    if (WV_NETD_EVENT_TEST (eventLevel, eventOffset)) \
        {    \
        WV_EVT_INFO_SET_ALL (eventLevel, eventTag, status) \
        WV_EVT_BLOB_SET1_FALSE (format, arg3, 0) \
        WV_EVT_LOG_OBIN (eventId, 3, wvNetDEventInfo, (int)arg1, (int)arg2, 0, 0, 0, \
                         sizeof (wvNetDEventBlob), (char *)wvNetDEventBlob); \
        } \
    WV_BLOCK_END (classId)

#define WV_NET_EVENT_4(classId, eventLevel, eventOffset, eventTag, eventId, status, arg1, arg2, arg3, arg4) \
    WV_BLOCK_START (classId) \
    if (WV_NETD_EVENT_TEST (eventLevel, eventOffset)) \
        {    \
        WV_EVT_INFO_SET_ALL (eventLevel, eventTag, status) \
        WV_EVT_BLOB_SET0 (WV_NETD_EVT_FMT_2P, arg3, arg4) \
        WV_EVT_LOG_OBIN (eventId, 3, wvNetDEventInfo, (int)arg1, (int)arg2, 0, 0, 0, \
                         sizeof (wvNetDEventBlob), (char *)wvNetDEventBlob); \
        } \
    WV_BLOCK_END (classId)
        
     /*
      * These data transfer events use the filter which tests for a particular
      * source and destination port for outgoing packets. If the filter is
      * enabled, the event is not reported to the host unless the ports match
      * the registered values.
      */

#define WV_NET_PORTOUT_EVENT_2(classId, eventLevel, eventOffset, eventTag, srcPort, dstPort, eventId, status, arg1, arg2) \
    WV_BLOCK_START (classId) \
    if (WV_NETD_EVENT_TEST (eventLevel, eventOffset) && \
        (* _func_wvNetPortFilterTest) (1, srcPort, dstPort)) \
        {    \
        WV_EVT_INFO_SET_ALL (eventLevel, eventTag, status) \
        WV_EVT_LOG_OBIN (eventId, 3, wvNetDEventInfo, (int)arg1, (int)arg2, 0, 0, 0, 0, NULL); \
        } \
    WV_BLOCK_END (classId)

#define WV_NET_PORTOUT_EVENT_3(classId, eventLevel, eventOffset, eventTag, srcPort, dstPort, eventId, status, arg1, arg2, arg3) \
    WV_BLOCK_START (classId) \
    if (WV_NETD_EVENT_TEST (eventLevel, eventOffset) && \
        (* _func_wvNetPortFilterTest) (1, srcPort, dstPort)) \
        {    \
	int format = WV_NETD_EVT_FMT_1P; \
        WV_EVT_INFO_SET_ALL (eventLevel, eventTag, status) \
        WV_EVT_BLOB_SET0 (format, arg3, 0) \
        WV_EVT_LOG_OBIN (eventId, 3, wvNetDEventInfo, (int)arg1, (int)arg2, 0, 0, 0, \
                         sizeof (wvNetDEventBlob), (char *)wvNetDEventBlob); \
        } \
    WV_BLOCK_END (classId)

#define WV_NET_PORTOUT_EVENT_4(classId, eventLevel, eventOffset, eventTag, srcPort, dstPort, eventId, status, arg1, arg2, arg3, arg4) \
    WV_BLOCK_START (classId) \
    if (WV_NETD_EVENT_TEST (eventLevel, eventOffset) && \
        (* _func_wvNetPortFilterTest) (1, srcPort, dstPort)) \
        {    \
        WV_EVT_INFO_SET_ALL (eventLevel, eventTag, status) \
        WV_EVT_BLOB_SET0 (WV_NETD_EVT_FMT_2P, arg3, arg4) \
        WV_EVT_LOG_OBIN (eventId, 3, wvNetDEventInfo, (int)arg1, (int)arg2, 0, 0, 0, \
                         sizeof (wvNetDEventBlob), (char *)wvNetDEventBlob); \
        } \
    WV_BLOCK_END (classId)

     /*
      * These data transfer events use the filter which tests for a particular
      * source and destination port for incoming packets. If the filter is
      * enabled, the event is not reported to the host unless the ports match
      * the registered values.
      */

#define WV_NET_PORTIN_EVENT_2(classId, eventLevel, eventOffset, eventTag, srcPort, dstPort, eventId, status, arg1, arg2) \
    WV_BLOCK_START (classId) \
    if (WV_NETD_EVENT_TEST (eventLevel, eventOffset) && \
        (* _func_wvNetPortFilterTest) (0, srcPort, dstPort)) \
        {    \
        WV_EVT_INFO_SET_ALL (eventLevel, eventTag, status) \
        WV_EVT_LOG_OBIN (eventId, 3, wvNetDEventInfo, (int)arg1, (int)arg2, 0, 0, 0, 0, NULL); \
        } \
    WV_BLOCK_END (classId)

#define WV_NET_PORTIN_EVENT_3(classId, eventLevel, eventOffset, eventTag, srcPort, dstPort, eventId, status, arg1, arg2, arg3) \
    WV_BLOCK_START (classId) \
    if (WV_NETD_EVENT_TEST (eventLevel, eventOffset) && \
        (* _func_wvNetPortFilterTest) (0, srcPort, dstPort)) \
        {    \
	int format = WV_NETD_EVT_FMT_1P; \
        WV_EVT_INFO_SET_ALL (eventLevel, eventTag, status) \
        WV_EVT_BLOB_SET0 (format, arg3, 0) \
        WV_EVT_LOG_OBIN (eventId, 3, wvNetDEventInfo, (int)arg1, (int)arg2, 0, 0, 0, \
                         sizeof (wvNetDEventBlob), (char *)wvNetDEventBlob); \
        } \
    WV_BLOCK_END (classId)
    
#define WV_NET_PORTIN_EVENT_4(classId, eventLevel, eventOffset, eventTag, srcPort, dstPort, eventId, status, arg1, arg2, arg3, arg4) \
    WV_BLOCK_START (classId) \
    if (WV_NETD_EVENT_TEST (eventLevel, eventOffset) && \
        (* _func_wvNetPortFilterTest) (0, srcPort, dstPort)) \
        {    \
        WV_EVT_INFO_SET_ALL (eventLevel, eventTag, status) \
        WV_EVT_BLOB_SET0 (WV_NETD_EVT_FMT_2P, arg3, arg4) \
        WV_EVT_LOG_OBIN (eventId, 3, wvNetDEventInfo, (int)arg1, (int)arg2, 0, 0, 0, \
                         sizeof (wvNetDEventBlob), (char *)wvNetDEventBlob); \
        } \
    WV_BLOCK_END (classId)

#define WV_NET_PORTIN_EVENT_4_4(classId, eventLevel, eventOffset, eventTag, srcPort, dstPort, eventId, status, arg1, arg2, arg3, arg4) \
    WV_NET_PORTIN_EVENT_40(classId, eventLevel, eventOffset, eventTag, srcPort, dstPort, eventId, WV_NETD_EVT_FMT_V4, status, arg1, arg2, arg3, arg4)
    
#define WV_NET_PORTIN_EVENT_40(classId, eventLevel, eventOffset, eventTag, srcPort, dstPort, eventId, format, status, arg1, arg2, arg3, arg4) \
    WV_BLOCK_START (classId) \
    if (WV_NETD_EVENT_TEST (eventLevel, eventOffset) && \
        (* _func_wvNetPortFilterTest) (0, srcPort, dstPort)) \
        {    \
        WV_EVT_INFO_SET_ALL (eventLevel, eventTag, status) \
        WV_EVT_BLOB_SET1 (TRUE, format, arg3, arg4) \
        WV_EVT_LOG_OBIN (eventId, 3, wvNetDEventInfo, (int)arg1, (int)arg2, 0, 0, 0, \
                         sizeof (wvNetDEventBlob), (char *)wvNetDEventBlob); \
        } \
    WV_BLOCK_END (classId)

#define WV_NET_DSTADDROUT_EVENT_3(classId, eventLevel, eventOffset, eventTag, dstAddr, eventId, status, arg1, arg2, arg3) \
    WV_BLOCK_START (classId) \
    if (WV_NETD_EVENT_TEST (eventLevel, eventOffset) && \
        (* _func_wvNetAddressFilterTest) (1, 1, 0, dstAddr)) \
        {    \
        WV_EVT_INFO_SET_ALL (eventLevel, eventTag, status) \
        WV_EVT_BLOB_SET0 (WV_NETD_EVT_FMT_1P, arg3, 0) \
        WV_EVT_LOG_OBIN (eventId, 3, wvNetDEventInfo, (int)arg1, (int)arg2, 0, 0, 0, \
                         sizeof (wvNetDEventBlob), (char *)wvNetDEventBlob); \
        } \
    WV_BLOCK_END (classId)
    
#define WV_NET_DSTADDROUT_EVENT_4_14(classId, eventLevel, eventOffset, eventTag, dstAddr, eventId, status, arg1, arg2, arg3, arg4) \
    WV_NET_DSTADDROUT_EVENT_40(classId, eventLevel, eventOffset, eventTag, dstAddr, eventId, WV_NETD_EVT_FMT_1P_V4, status, arg1, arg2, arg3, arg4)

#define WV_NET_DSTADDROUT_EVENT_4_4(classId, eventLevel, eventOffset, eventTag, dstAddr, eventId, status, arg1, arg2, arg3, arg4) \
    WV_NET_DSTADDROUT_EVENT_40(classId, eventLevel, eventOffset, eventTag, dstAddr, eventId, WV_NETD_EVT_FMT_V4, status, arg1, arg2, arg3, arg4)
    
#define WV_NET_DSTADDROUT_EVENT_40(classId, eventLevel, eventOffset, eventTag, dstAddr, eventId, format, status, arg1, arg2, arg3, arg4) \
    WV_BLOCK_START (classId) \
    if (WV_NETD_EVENT_TEST (eventLevel, eventOffset) && \
        (* _func_wvNetAddressFilterTest) (1, 1, 0, dstAddr)) \
        {    \
        WV_EVT_INFO_SET_ALL (eventLevel, eventTag, status) \
        WV_EVT_BLOB_SET1 (TRUE, format, arg3, arg4) \
        WV_EVT_LOG_OBIN (eventId, 3, wvNetDEventInfo, (int)arg1, (int)arg2, 0, 0, 0, \
                         sizeof (wvNetDEventBlob), (char *)wvNetDEventBlob); \
        } \
    WV_BLOCK_END (classId)
     /*
      * This data transfer event uses the filter which tests for a particular
      * destination address for incoming packets. If the filter is enabled,
      * the event is not reported to the host unless the address matches the
      * registered value.
      */

#define WV_NET_DSTADDRIN_EVENT_4_14(classId, eventLevel, eventOffset, eventTag, dstAddr, eventId, status, arg1, arg2, arg3, arg4) \
    WV_NET_DSTADDRIN_EVENT_40(classId, eventLevel, eventOffset, eventTag, dstAddr, eventId, WV_NETD_EVT_FMT_1P_V4, status, arg1, arg2, arg3, arg4)
    
#define WV_NET_DSTADDRIN_EVENT_4_4(classId, eventLevel, eventOffset, eventTag, dstAddr, eventId, status, arg1, arg2, arg3, arg4) \
    WV_NET_DSTADDRIN_EVENT_40(classId, eventLevel, eventOffset, eventTag, dstAddr, eventId, WV_NETD_EVT_FMT_V4, status, arg1, arg2, arg3, arg4)
    
#define WV_NET_DSTADDRIN_EVENT_40(classId, eventLevel, eventOffset, eventTag, dstAddr, eventId, format, status, arg1, arg2, arg3, arg4) \
    WV_BLOCK_START (classId) \
    if (WV_NETD_EVENT_TEST (eventLevel, eventOffset) && \
        (* _func_wvNetAddressFilterTest) (0, 1, 0, dstAddr)) \
        {    \
        WV_EVT_INFO_SET_ALL (eventLevel, eventTag, status) \
        WV_EVT_BLOB_SET1 (TRUE, format, arg3, arg4) \
        WV_EVT_LOG_OBIN (eventId, 3, wvNetDEventInfo, (int)arg1, (int)arg2, 0, 0, 0, \
                         sizeof (wvNetDEventBlob), (char *)wvNetDEventBlob); \
        } \
    WV_BLOCK_END (classId)
     /*
      * This data transfer event uses the filter which tests for a particular
      * source and destination address for outgoing packets. If the filter is
      * enabled, the event is not reported to the host unless the addresses
      * match both registered values.
      */

#define WV_NET_ADDROUT_EVENT_2(classId, eventLevel, eventOffset, eventTag, srcAddr, dstAddr, eventId, status, arg1, arg2) \
    WV_BLOCK_START (classId) \
    if (WV_NETD_EVENT_TEST (eventLevel, eventOffset) && \
        (* _func_wvNetAddressFilterTest) (1, 3, srcAddr, dstAddr)) \
        {    \
        WV_EVT_INFO_SET_ALL (eventLevel, eventTag, status) \
        WV_EVT_LOG_OBIN (eventId, 3, wvNetDEventInfo, (int)arg1, (int)arg2, 0, 0, 0, 0, NULL); \
        } \
    WV_BLOCK_END (classId)

#define WV_NET_ADDROUT_EVENT_4_14(classId, eventLevel, eventOffset, eventTag, srcAddr, dstAddr, eventId, status, arg1, arg2, arg3, arg4) \
    WV_NET_ADDROUT_EVENT_40(classId, eventLevel, eventOffset, eventTag, srcAddr, dstAddr, eventId, WV_NETD_EVT_FMT_1P_V4, status, arg1, arg2, arg3, arg4)
    
#define WV_NET_ADDROUT_EVENT_4_4(classId, eventLevel, eventOffset, eventTag, srcAddr, dstAddr, eventId, status, arg1, arg2, arg3, arg4) \
    WV_NET_ADDROUT_EVENT_40(classId, eventLevel, eventOffset, eventTag, srcAddr, dstAddr, eventId, WV_NETD_EVT_FMT_V4, status, arg1, arg2, arg3, arg4)
    
#define WV_NET_ADDROUT_EVENT_40(classId, eventLevel, eventOffset, eventTag, srcAddr, dstAddr, eventId, format, status, arg1, arg2, arg3, arg4) \
    WV_BLOCK_START (classId) \
    if (WV_NETD_EVENT_TEST (eventLevel, eventOffset) && \
        (* _func_wvNetAddressFilterTest) (1, 3, srcAddr, dstAddr)) \
        {    \
        WV_EVT_INFO_SET_ALL (eventLevel, eventTag, status) \
        WV_EVT_BLOB_SET1 (TRUE, format, arg3, arg4) \
        WV_EVT_LOG_OBIN (eventId, 3, wvNetDEventInfo, (int)arg1, (int)arg2, 0, 0, 0, \
                         sizeof (wvNetDEventBlob), (char *)wvNetDEventBlob); \
        } \
    WV_BLOCK_END (classId)

     /*
      * This data transfer event uses the filter which tests for a particular
      * source and destination address for incoming packets. If the filter is
      * enabled, the event is not reported to the host unless the addresses
      * match both registered values.
      */

#define WV_NET_ADDRIN_EVENT_2(classId, eventLevel, eventOffset, eventTag, srcAddr, dstAddr, eventId, status, arg1, arg2) \
    WV_BLOCK_START (classId) \
    if (WV_NETD_EVENT_TEST (eventLevel, eventOffset) && \
        (* _func_wvNetAddressFilterTest) (0, 3, srcAddr, dstAddr)) \
        {    \
        WV_EVT_INFO_SET_ALL (eventLevel, eventTag, status) \
        WV_EVT_LOG_OBIN (eventId, 3, wvNetDEventInfo, (int)arg1, (int)arg2, 0, 0, 0, 0, NULL); \
        } \
    WV_BLOCK_END (classId)

#define WV_NET_ADDRIN_EVENT_4(classId, eventLevel, eventOffset, eventTag, srcAddr, dstAddr, eventId, status, arg1, arg2, arg3, arg4) \
    WV_BLOCK_START (classId) \
    if (WV_NETD_EVENT_TEST (eventLevel, eventOffset) && \
        (* _func_wvNetAddressFilterTest) (0, 3, srcAddr, dstAddr)) \
        {    \
        WV_EVT_INFO_SET_ALL (eventLevel, eventTag, status) \
        WV_EVT_BLOB_SET0 (WV_NETD_EVT_FMT_2P, arg3, arg4) \
        WV_EVT_LOG_OBIN (eventId, 3, wvNetDEventInfo, (int)arg1, (int)arg2, 0, 0, 0, \
                         sizeof (wvNetDEventBlob), (char *)wvNetDEventBlob); \
        } \
    WV_BLOCK_END (classId)
    
#define WV_NET_ADDRIN_EVENT_4_4(classId, eventLevel, eventOffset, eventTag, srcAddr, dstAddr, eventId, status, arg1, arg2, arg3, arg4) \
    WV_NET_ADDRIN_EVENT_40(classId, eventLevel, eventOffset, eventTag, srcAddr, dstAddr, eventId, WV_NETD_EVT_FMT_V4, status, arg1, arg2, arg3, arg4)
    
#define WV_NET_ADDRIN_EVENT_40(classId, eventLevel, eventOffset, eventTag, srcAddr, dstAddr, eventId, format, status, arg1, arg2, arg3, arg4) \
    WV_BLOCK_START (classId) \
    if (WV_NETD_EVENT_TEST (eventLevel, eventOffset) && \
        (* _func_wvNetAddressFilterTest) (0, 3, srcAddr, dstAddr)) \
        {    \
        WV_EVT_INFO_SET_ALL (eventLevel, eventTag, status) \
        WV_EVT_BLOB_SET1(TRUE, format, arg3, arg4) \
        WV_EVT_LOG_OBIN (eventId, 3, wvNetDEventInfo, (int)arg1, (int)arg2, 0, 0, 0, \
                         sizeof (wvNetDEventBlob), (char *)wvNetDEventBlob); \
        } \
    WV_BLOCK_END (classId)

#else  /* when WV is excluded */

#define WV_ADDR_GET(pAddr) {}    
#define WV_NET_MARKER_1(classId, eventLevel, eventOffset, eventTag, eventId, arg1) {}
#define WV_NET_MARKER_2(classId, eventLevel, eventOffset, eventTag, eventId, arg1, arg2) {}
#define WV_NET_MARKER_3(classId, eventLevel, eventOffset, eventTag, eventId, arg1, arg2, arg3) {}
#define WV_NET_MARKER_3_4(classId, eventLevel, eventOffset, eventTag, eventId, arg1, arg2, arg3) {}
#define WV_NET_MARKER_3_6(classId, eventLevel, eventOffset, eventTag, eventId, arg1, arg2, arg3) {}
#define WV_NET_MARKER_3_0(classId, eventLevel, eventOffset, eventTag, eventId, arg1, arg2, arg3) {}
#define WV_NET_MARKER_4(classId, eventLevel, eventOffset, eventTag, eventId, arg1, arg2, arg3, arg4) {}
#define WV_NET_MARKER_4_14(classId, eventLevel, eventOffset, eventTag, eventId, arg1, arg2, arg3, arg4) {}
#define WV_NET_MARKER_4_21(classId, eventLevel, eventOffset, eventTag, eventId, arg1, arg2, arg3, arg4) {}
#define WV_NET_MARKER_4_22(classId, eventLevel, eventOffset, eventTag, eventId, arg1, arg2, arg3, arg4) {}
#define WV_NET_PORTOUT_MARKER_3(classId, eventLevel, eventOffset, eventTag, srcPort, dstPort, eventId, arg1, arg2, arg3) {}
#define WV_NET_DSTADDROUT_MARKER_3(classId, eventLevel, eventOffset, eventTag, dstAddr, eventId, arg1, arg2, arg3) {}
#define WV_NET_DSTADDROUT_MARKER_3_4(classId, eventLevel, eventOffset, eventTag, dstAddr, eventId, arg1, arg2, arg3) {}
#define WV_NET_ADDRIN_MARKER_4_4(classId, eventLevel, eventOffset, eventTag, srcAddr, dstAddr, eventId, arg1, arg2, arg3, arg4) {}
#define WV_NET_EVENT_1(classId, eventLevel, eventOffset, eventTag, eventId, status, arg1) {}
#define WV_NET_EVENT_2(classId, eventLevel, eventOffset, eventTag, eventId, status, arg1, arg2) {}
#define WV_NET_EVENT_3(classId, eventLevel, eventOffset, eventTag, eventId, status, arg1, arg2, arg3) {}
#define WV_NET_EVENT_3_4(classId, eventLevel, eventOffset, eventTag, eventId, status, arg1, arg2, arg3) {}
#define WV_NET_EVENT_4(classId, eventLevel, eventOffset, eventTag, eventId, status, arg1, arg2, arg3, arg4) {}
#define WV_NET_PORTOUT_EVENT_2(classId, eventLevel, eventOffset, eventTag, srcPort, dstPort, eventId, status, arg1, arg2) {}
#define WV_NET_PORTOUT_EVENT_3(classId, eventLevel, eventOffset, eventTag, srcPort, dstPort, eventId, status, arg1, arg2, arg3) {}
#define WV_NET_PORTOUT_EVENT_4(classId, eventLevel, eventOffset, eventTag, srcPort, dstPort, eventId, status, arg1, arg2, arg3, arg4) {}
#define WV_NET_PORTIN_EVENT_2(classId, eventLevel, eventOffset, eventTag, srcPort, dstPort, eventId, status, arg1, arg2) {}
#define WV_NET_PORTIN_EVENT_3(classId, eventLevel, eventOffset, eventTag, srcPort, dstPort, eventId, status, arg1, arg2, arg3) {}
#define WV_NET_PORTIN_EVENT_4(classId, eventLevel, eventOffset, eventTag, srcPort, dstPort, eventId, status, arg1, arg2, arg3, arg4) {}
#define WV_NET_PORTIN_EVENT_4_4(classId, eventLevel, eventOffset, eventTag, srcPort, dstPort, eventId, status, arg1, arg2, arg3, arg4) {}
#define WV_NET_DSTADDROUT_EVENT_3(classId, eventLevel, eventOffset, eventTag, dstAddr, eventId, status, arg1, arg2, arg3) {}
#define WV_NET_DSTADDROUT_EVENT_4_14(classId, eventLevel, eventOffset, eventTag, dstAddr, eventId, status, arg1, arg2, arg3, arg4) {}
#define WV_NET_DSTADDROUT_EVENT_4_4(classId, eventLevel, eventOffset, eventTag, dstAddr, eventId, status, arg1, arg2, arg3, arg4) {}
#define WV_NET_DSTADDRIN_EVENT_4_14(classId, eventLevel, eventOffset, eventTag, dstAddr, eventId, status, arg1, arg2, arg3, arg4) {}
#define WV_NET_DSTADDRIN_EVENT_4_4(classId, eventLevel, eventOffset, eventTag, dstAddr, eventId, status, arg1, arg2, arg3, arg4) {}
#define WV_NET_ADDROUT_EVENT_2(classId, eventLevel, eventOffset, eventTag, srcAddr, dstAddr, eventId, status, arg1, arg2) {}
#define WV_NET_ADDROUT_EVENT_4_14(classId, eventLevel, eventOffset, eventTag, srcAddr, dstAddr, eventId, status, arg1, arg2, arg3, arg4) {}
#define WV_NET_ADDROUT_EVENT_4_4(classId, eventLevel, eventOffset, eventTag, srcAddr, dstAddr, eventId, status, arg1, arg2, arg3, arg4) {}
#define WV_NET_ADDRIN_EVENT_2(classId, eventLevel, eventOffset, eventTag, srcAddr, dstAddr, eventId, status, arg1, arg2) {}
#define WV_NET_ADDRIN_EVENT_4(classId, eventLevel, eventOffset, eventTag, srcAddr, dstAddr, eventId, status, arg1, arg2, arg3, arg4) {}
#define WV_NET_ADDRIN_EVENT_4_4(classId, eventLevel, eventOffset, eventTag, srcAddr, dstAddr, eventId, status, arg1, arg2, arg3, arg4) {}    
#endif /* WV_INSTRUMENTATION && INCLUDE_WVNETD */

#ifdef __cplusplus
}
#endif

#endif /* __INCwvNetDEventPh */
