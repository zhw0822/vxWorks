/* m2Lib.h - VxWorks MIB-II interface to SNMP Agent */

/* Copyright 1993-2003 Wind River Systems, Inc. */

/*
modification history
--------------------
01k,02dec03,zl   use avlUintLib instead of avlLib.
01j,20nov03,niq  Remove copyright_wrs.h file inclusion
01i,04nov03,rlm  Ran batch header path update for header re-org.
01h,03nov03,rlm  Removed wrn/coreip/ prefix from #includes for header re-org.
01g,24oct03,cdw  update include statements post header re-org.
01f,20oct03,zl   changed for new avlLib without GENERIC_ARGUMENT
01e,08sep03,vvv  merged from ACCORDION_BASE6_MERGE_BASELINE
01d,12jul02,ann  added a new IOCTL to get the various fields of IP_DRV_CTRL
01c,28mar02,ppp  changed to build with the tornado build environment
01b,25feb01,rae  merged 01b tor2_0.barney (base 01a):
                 RFC2233, some cleanup                 
01a,08dec93,jag  written
*/

#ifndef __INCm2LibPh
#define __INCm2LibPh

#ifdef __cplusplus
extern "C" {
#endif

#include <sys/socket.h>
#include <net/if_arp.h>
#include <net/if.h>
#include <netinet/in.h>
#include <netinet/if_ether.h>
#include <avlUintLib.h>

#if ((CPU_FAMILY==I960) && (defined __GNUC__))
#pragma align 1                 /* tell gcc960 not to optimize alignments */
#endif  /* CPU_FAMILY==I960 */

/* This structure is used to keep track of the  Network Interfaces */

typedef struct
    {
    int              netIfIndex;      /* I/F index returned to SNMP */
    long             netIfType;       /* I/F type from MIB-II specifications */
    unsigned long    netIfSpeed;      /* I/F Speed from MIB-II specifications */
    unsigned long    netIfAdminStatus;  /* I/F old status used for traps */
    unsigned long    netIfLastChange;   /* I/F Time of Interface last change */
    M2_OBJECTID      netIfOid;          /* I/F Object ID */
    struct arpcom  * pNetIfDrv;         /* Pointer to BSD Network Driver I/F */
 
    } NET_IF_TBL;

/*
 *  These IOCTL commands are used exclusively by the mib2Lib.c module to retrive
 *  MIB-II parameters from network drivers.
 */
 
#define SIOCGMIB2CNFG     _IOR('m',  0, int)            /* Get Configuration */
#define SIOCGMIB2CNTRS    _IOR('m',  1, int)            /* Get driver counters */
#define SIOCGMIB2233      _IOR('m',  2, M2_ID **)       /* Get rfc2233 data */
#define SIOCSMIB2233      _IOW('m',  3, IF_SETENTRY)    /* Set rfc2233 data */
#define SIOCGMCASTLIST    _IOR('m',  4, LIST **)        /* Get mcast list */
#define SIOCGMIPDRVCTRL   _IOR('m',  5, struct ipdrvFields *) /* Get IP_DRV_CTRL */
#define SIOCSMIPDRVCTRL   _IOR('m',  6, struct ipdrvFields *) /* Set IP_DRV_CTRL */

/*
 * This structure is used to obtain the configuration information from the
 * network drivers.  This information is static and does not change through the
 * life time of the driver. 
 */
 
typedef struct
    {
    long            ifType;
    M2_OBJECTID     ifSpecific;
 
    } M2_NETDRVCNFG;
 
/*
 * This structure is used to retrive counters from the network driver.  The
 * variable ifSpeed is included here to support devices that can compute the
 * nominal bandwidth.
 */
 
typedef struct
    {
    unsigned long   ifSpeed;
    unsigned long   ifInOctets;
    unsigned long   ifInNUcastPkts;
    unsigned long   ifInDiscards;
    unsigned long   ifInUnknownProtos;
    unsigned long   ifOutOctets;
    unsigned long   ifOutNUcastPkts;
    unsigned long   ifOutDiscards;
 
    } M2_NETDRVCNTRS;

  
 
#if ((CPU_FAMILY==I960) && (defined __GNUC__))
#pragma align 0                 /* turn off alignment requirement */
#endif  /* CPU_FAMILY==I960 */
 
/* 
 * Imported from if.c, this flag is used to notify the MIB-II I/F that a 
 * change in the network interface list has occured.  The table will be read
 * again.
 */
extern int                ifAttachChange;

/* extern NET_IF_TBL * pm2IfTable;  Network I/F table (Allocated Dynamically) */
extern int 	    m2IfCount;
extern AVLU_TREE    pM2IfRoot;    /* The root node of the if tree */
extern M2_IFINDEX * pm2IfTable;  /* Network I/F table (Allocated Dynamically) */
extern int          m2IfCount; 

extern M2_OBJECTID zeroObjectId;

#if defined(__STDC__) || defined(__cplusplus)

extern int m2NetIfTableRead (void);
extern void ifWalk (int);
extern void testTree (void);

#else   /* __STDC__ */

extern int m2NetIfTableRead ();

#endif  /* __STDC__ */

#ifdef __cplusplus
}
#endif

#endif /* __INCm2LibPh */
