/* ip6_ext_in.h - typedefs and externs for the external hooks
 * used by the IPv6 stack on the input side
 */

/* Copyright 2002 Wind River Systems, Inc. */

/*
modification history
--------------------
01c,26apr05,kch  Added ipv6 multicast router hooks.
01b,20nov03,niq  Remove copyright_wrs.h file inclusion
01a,07aug03,nee  Merging to Base6 from ACCORDION_BASE6_MERGE_BASELINE label on
                 Accordion codeline

*/

#ifndef __INCip6ExtInh
#define __INCip6ExtInh

#ifdef __cplusplus
extern "C" {
#endif

/*
 * This file contains typedefs for some of the hooks used by the IPv6 stack.
 * The similar hooks used by the IPv4 stack can be found in a different file.
 * The functions are separated in order to minimize the include files required
 * to use all one set of functions.
 */

/* We don't do anything except if IP6 is installed */
#ifdef INET6

/*
 * This is the general typedef for the input side for IPv6.
 * It should be used by all hooks that don't require a more specific API.
 */

typedef BOOL (*INPUT_HOOK_IPV6_FUNCPTR)   (struct ifnet    * rcvif,
                                           struct mbuf    ** m,
                                           struct ip6_hdr ** ip);


#ifndef VIRTUAL_STACK
/* We only define the externals and macros here if we aren't supporting
 * virtual stacks.  If we are supporting them then the corresponding
 * definitions are in vsIp6.h
 */

/*
 * Externals for the currently defined hooks
 */

extern INPUT_HOOK_IPV6_FUNCPTR     _ipFilterHookIPv6;
#define SET_IPFILTERHOOKIPV6(foo) (_ipFilterHookIPv6 = foo)

/* ipv6 multicast router hooks */
extern FUNCPTR _mCastRouteCmdHookIPv6;
extern FUNCPTR _mCastRouterIoctlHookIPv6;
extern FUNCPTR _mCastRouterDoneHookIPv6;
extern VOIDFUNCPTR _mCastRouterDetachHookIPv6;

/*
 * The IPSec hooks.
 */
extern INPUT_HOOK_IPV6_FUNCPTR   _ipsecFilterHookIPv6;
extern INPUT_HOOK_IPV6_FUNCPTR   _ipsecInputIPv6;

#define SET_IPSECFILTERHOOKIPV6(foo) (_ipsecFilterHookIPv6 = foo)
#define SET_IPSECINPUTIPV6(foo)      (_ipsecInputIPv6 = foo)

#endif /* #ifndef VIRTUAL_STACK */

#endif /* INET6 */

#ifdef __cplusplus
}
#endif

#endif /* __INCip6ExtInh */
