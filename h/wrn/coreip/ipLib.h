/* ipLib.h - configuration data structute for IP */

/* Copyright 2000 - 2004 Wind River Systems, Inc. */

/*
modification history
--------------------
01r,20aug05,dlk  Added cfg_priv_maxnipq and cfg_priv_maxfragsperpacket.
01q,07feb05,wap  Allow sysctl init routines to be scaled out
01p,21jan05,sar  Removal of divert, dummynet and fw code.
01o,23aug04,rp   merged from COMP_WN_IPV6_BASE6_ITER5_TO_UNIFIED_PRE_MERGE
01n,28may04,niq  Merging from base6 label POST_ITER5_FRZ16_REBASE (ver
                 /main/vdt/base6_itn5_networking-int/1)
01m,05nov03,cdw  Removal of unnecessary _KERNEL guards.
01l,04nov03,rlm  Ran batch header path update for header re-org.
01k,03nov03,rlm  Removed wrn/coreip/ prefix from #includes for header re-org.
01j,23oct03,rlm  updated #includes for header re-org
01i,15sep03,vvv  updated path for new headers
01h,13aug03,niq  Merging from Accordion label ACCORDION_BASE6_MERGE_BASELINE
01g,10jun03,vvv  include netVersion.h
01f,15may03,ann  removed all the non-clarinet references
*/

#ifndef __INCipLibh
#define __INCipLibh

#ifdef __cplusplus
extern "C" {
#endif

/* includes */

#include <netVersion.h>
#include <ipProto.h>
#include <netinet/in_systm.h>
#include <netinet/ip_var.h>
#include <netinet/ip_flow.h>
#include <net/socketvar.h> 
#include <netinet/in.h>
#include <netinet/in_var.h>
#include <netinet/ip.h>
#include <netinet/ipprotosw.h>
#include <netinet/in_pcb.h>
#include <netLib.h>
#include <net/domain.h>
#include <cfgDefs.h>

/* defines */

/* typedefs */

typedef struct ip_config_params
    {
    CFG_DATA_HDR cfgh;
    /* definitions from in.c */
    int cfg_priv_subnetsarelocal;

    /* definitions from in_pcb.c */
    int	cfg_ipport_lowfirstauto;
    int	cfg_ipport_lowlastauto;
    int	cfg_ipport_firstauto;
    int	cfg_ipport_lastauto;
    int	cfg_ipport_hifirstauto;
    int	cfg_ipport_hilastauto;

    /* definitions from in_rmx.c */
    int cfg_priv_rtq_reallyold;
    int cfg_priv_rtq_minreallyold;
    int cfg_priv_rtq_toomany;
    int cfg_priv_rtq_timeout;

    /* definitions from in_flow.c */
    int cfg_priv_ipflow_active;

    /* definitions from ip_input.c */
    int	cfg_priv_ipsendredirects;
    int	cfg_priv_ip_dosourceroute;
    int	cfg_priv_ip_acceptsourceroute;
    int	cfg_priv_ip_checkinterface;
    int cfg_priv_maxnipq;
    int cfg_priv_maxfragsperpacket;

    int	cfg_priv_ipqmaxlen;

    /* cfg_priv_ipstealth was ifdef IPSTEALTH but not avail for configlettes */

    int	cfg_priv_ipstealth;

    int	cfg_ipforwarding;
    int	cfg_ip_defttl;

    /* definitions from raw_in.c */
    u_long 	cfg_rip_sendspace;
    u_long 	cfg_rip_recvspace;

    struct ipRouteDispatchTable *  cfg_ipv4RtDispTable; /* v4 dispatch table */
    FUNCPTR	cfg_privInitSysctl;
    } IP_CONFIG_PARAMS;

/* prototypes */

#if defined(__STDC__) || defined(__cplusplus)

IMPORT STATUS ipInstInit (void * InitValues);

#else   /* __STDC__ */

IMPORT STATUS ipInstInit ();

#endif  /* __STDC__ */

#ifdef __cplusplus
}
#endif

#endif /* __INCipLibh */
