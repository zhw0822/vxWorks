/* netCore.h - core network initialization variables */

/* Copyright 2001-2004 Wind River Systems, Inc. */

/*
modification history
--------------------
01i,22sep04,dlk  Add declaration for netCoreSetup().
01h,20sep04,spm  updated virtual stack startup: removed unused storage
                 for packing and unpacking configuration parameters;
                 removed obsolete __STDC__ support for old compilers
01g,20nov03,niq  Remove copyright_wrs.h file inclusion
01f,05nov03,cdw  Removal of unnecessary _KERNEL guards.
01e,04nov03,rlm  Ran batch header path update for header re-org.
01d,03nov03,rlm  Removed wrn/coreip/ prefix from #includes for header re-org.
01c,15sep03,vvv  updated path for new headers
01b,08sep03,vvv  merged from ACCORDION_BASE6_MERGE_BASELINE
01a,12dec02,ann  ported from accordion but removed virtual 
                 stack code.
*/

#ifndef __netcoreh
#define __netcoreh

#ifdef __cplusplus
extern "C" {
#endif

STATUS netCoreSetup (UINT maxLinkHdr, UINT maxProtoHdr,
		     UINT numClusters, UINT maxUnits);

#ifdef VIRTUAL_STACK
extern STATUS netCoreDestructor (VSNUM vsnum);
#endif /* VIRTUAL_STACK */

#ifdef __cplusplus
}
#endif

#endif /* __netcoreh */
