/* arpLib.h - VxWorks ARP table manipulation header file */

/* Copyright 2001 - 2005 Wind River Systems, Inc. */

/*
modification history
--------------------
01l,09feb05,sar  Move gtf include files per GTF code inspection of 10/26/2004
01k,07feb05,vvv  _KERNEL cleanup
01j,28may04,niq  Merging from base6 label POST_ITER5_FRZ16_REBASE (ver
                 /main/vdt/base6_itn5_networking-int/1)
01i,20nov03,niq  Remove copyright_wrs.h file inclusion
01h,05nov03,cdw  Removal of unnecessary _KERNEL guards.
01g,04nov03,rlm  Ran batch header path update for header re-org.
01f,03nov03,rlm  Removed wrn/coreip/ prefix from #includes for header re-org.
01e,23oct03,rlm  updated #includes for header re-org
01d,08sep03,vvv  merged from ACCORDION_BASE6_MERGE_BASELINE
01c,09may03,vvv  included vwModNum.h and in.h
01b,07feb02,ppp  re-enabling arpLibInit
01a,14dec01,ppp  ported from A.E. 1.1
*/

#ifndef __INCarpLibh
#define __INCarpLibh

#ifdef __cplusplus
extern "C" {
#endif

#include <netinet/if_ether.h>
#include <private/gtf_core.h>
#include <vwModNum.h>
#include <netinet/in.h>


#if defined(__STDC__) || defined(__cplusplus)
extern void 	arpLibInit (void);
extern STATUS 	arpAdd (char *host, char *eaddr, int flags);
extern STATUS 	arpDelete (char *host);
extern STATUS 	arpCmd (int cmd, struct in_addr * pIpAddr, u_char *pHwAddr,
			int *pFlags);
extern void 	arpFlush (void);
extern STATUS   arpResolve (char * targetAddr, char * pHwAddr, int numTries,
			    int numTicks);

#else	/* __STDC__ */
extern void	arpLibInit ();
extern STATUS 	arpAdd ();
extern STATUS 	arpDelete ();
extern STATUS 	arpCmd ();
extern void 	arpFlush ();
extern STATUS   arpResolve ();

#endif	/* __STDC__ */

/* error values */

#define S_arpLib_INVALID_ARGUMENT		(M_arpLib | 1)
#define S_arpLib_INVALID_HOST 			(M_arpLib | 2)
#define S_arpLib_INVALID_ENET_ADDRESS 		(M_arpLib | 3)
#define S_arpLib_INVALID_FLAG			(M_arpLib | 4)

#ifdef __cplusplus
}
#endif

#endif /* __INCarpLibh */
