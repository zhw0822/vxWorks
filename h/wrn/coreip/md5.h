/* md5.h - md5 header file */

/* Copyright 2001-2005 Wind River Systems, Inc. */

/* MD5.H - header file for MD5C.C
 * $FreeBSD: src/sys/sys/md5.h,v 1.14 2001/03/17 10:00:46 phk Exp $
 */

/* Copyright (C) 1991-2, RSA Data Security, Inc. Created 1991. All
rights reserved.

License to copy and use this software is granted provided that it
is identified as the "RSA Data Security, Inc. MD5 Message-Digest
Algorithm" in all material mentioning or referencing this software
or this function.

License is also granted to make and use derivative works provided
that such works are identified as "derived from the RSA Data
Security, Inc. MD5 Message-Digest Algorithm" in all material
mentioning or referencing the derived work.

RSA Data Security, Inc. makes no representations concerning either
the merchantability of this software or the suitability of this
software for any particular purpose. It is provided "as is"
without express or implied warranty of any kind.

These notices must be retained in any copies of any part of this
documentation and/or software.
 */

/*
modification history
--------------------
01l,08mar05,vvv  include cdefs.h only in kernel
01k,07feb05,vvv  _KERNEL cleanup
01j,21nov03,rp   added mdString required for PPP
01i,20nov03,niq  Remove copyright_wrs.h file inclusion
01h,05nov03,cdw  Removal of unnecessary _KERNEL guards.
01g,04nov03,rlm  Ran batch header path update for header re-org.
01f,03nov03,rlm  Removed wrn/coreip/ prefix from #includes for header re-org.
01e,10jun03,vvv  include netVersion.h
01d,24apr03,pas  changed API symbols to co-exist with WindManage SNMP
01c,06sep02,hsh  add c++ protection
01b,28mar02,ppp  making changes for the tornado build
01a,24sep01,hsh  Created from BSD 4.3
*/

#ifndef _SYS_MD5_H_
#define _SYS_MD5_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <netVersion.h>

#define MD5Context	_Clarinet_MD5Context
#define MD5_CTX		_Clarinet_MD5_CTX
#define MD5Init		_Clarinet_MD5Init
#define MD5Update	_Clarinet_MD5Update
#define MD5Pad		_Clarinet_MD5Pad
#define MD5Final	_Clarinet_MD5Final
#define MD5Transform	_Clarinet_MD5Transform
#define mdString   	_Clarinet_MDString
    
/* MD5 context. */
typedef struct MD5Context {
  u_int32_t state[4];	/* state (ABCD) */
  u_int32_t count[2];	/* number of bits, modulo 2^64 (lsb first) */
  unsigned char buffer[64];	/* input buffer */
} MD5_CTX;

/* Algorithms: MD4 or MD5 */
typedef enum
{
        MD4 = 0x04,
        MD5 = 0x05
} MD_ALGORITHM;

#ifdef _WRS_KERNEL
#include <sys/cdefs.h>
#endif

void   MD5Init (MD5_CTX *);
void   MD5Update (MD5_CTX *, const unsigned char *, unsigned int);
void   MD5Final (unsigned char [16], MD5_CTX *);
void   mdString ( unsigned char *, unsigned long,
                  unsigned char *, MD_ALGORITHM );
#ifdef _WRS_KERNEL
void MD5Transform __P((u_int32_t [4], const unsigned char [64]));
#endif

#ifdef __cplusplus
}
#endif
    
#endif /* _SYS_MD5_H_ */
