/* ds_conf.h -- data structure configuration definitions */

/*
 * Copyright (c) 2001-2005 Wind River Systems, Inc.
 *
 * The right to copy, distribute or otherwise make use of this software
 * may be licensed only pursuant to the terms of an applicable Wind River
 * license agreement. No license to Wind River intellectual property rights
 * is granted herein. All rights not licensed by Wind River are reserved
 * by Wind River.
 */

/*
modification history
--------------------
02r,25aug05,kch  Guarded MIP6 structure size definitions with MIP6 define.
02q,20aug05,dlk  Replaced obsolete RR_PREFIX entries with entries for
                 struct ipq.
02r,20jul05,vvv  added entries for SCTP
02q,06jul05,kch  Added entries for ipv4/v6 msf and mobility.
02p,24jun05,wap  Add IFMEDIA_ENTRY_DS_ID
02o,25apr05,kch  Added entries for in6_ifextra and nd_ifinfo.
02n,25feb05,niq  Add support for router alert
02m,31jan05,niq  merge mroute changes from comp_wn_ipv6_multicast_interim-dev
                 branch  (ver 1)
02l,19jan05,wap  Add additional types
02k,23aug04,rp   merged from COMP_WN_IPV6_BASE6_ITER5_TO_UNIFIED_PRE_MERGE
02j,28may04,niq  Merging from base6 label POST_ITER5_FRZ16_REBASE (ver
                 /main/vdt/base6_itn5_networking-int/1)
02i,20nov03,niq  Remove copyright_wrs.h file inclusion
02h,05nov03,cdw  Removal of unnecessary _KERNEL guards.
02g,04nov03,rlm  Ran batch header path update for header re-org.
02f,03nov03,rlm  Removed wrn/coreip/ prefix from #includes for header re-org.
02e,06aug03,nee  Accordion Base6 merge from ACCORDION_BASE6_MERGE_BASELINE
                 label
02d,23jul03,vvv  added entries for scope6_id (SPR #88481)
02d,02dec02,spm  merged from WINDNET_IPV6-1_0-FCS label (ver 02c,06sep02,hsh)
02c,15oct02,niq  Changes for Accordion's RTM
02b,24jun02,ism  changed in6_ifstat to in6_ifdata
02a,12jun02,pas  changed DS_MALLOC() to a wrapper around netClusterGet()
01z,17may02,ann  added the M2_IFINDEX data structure
01y,26apr02,ham  removed IPSEC.
01x,22apr02,nee  adding multicast forwarding data strutures
01w,02feb02,ham  changed DS_NUM to variables to be configured by CDF.
01v,12dec01,ann  removing the structures defined for sysctl
01u,31oct01,ham  increased SOCKADDR_DS_NUM.
01t,30oct01,ann  adding the sysctl data structs
01s,29oct01,nee  adding inp_tp for tcpcb
01r,04oct01,ham  added ds for struct ifnet.
01q,02oct01,hsh  add in_hcentry struct into memory pool
01p,01oct01,ppp  fixing a typo
01o,27sep01,ppp  Adding some more structures in for security associations
01n,25sep01,hsh  Adding data structures
01m,24sep01,ppp  Adding two data structures
01l,20sep01,qli  removing MAX_DS_POOL_ID
01k,18sep01,nee  adding the ICMP6_FILTER entry
01j,18sep01,ann  adding the ETHER_MULTI ds
01i,18sep01,qli  removing the sizeof operator
01h,17sep01,qli  adding more data structures
01g,13sep01,ann  adding the Minterfaces MIB table DS
01f,11sep01,qli  fix definition ogf RADIX_NODE_DS_SZ
01e,05sep01,qli  adding route and radix related definitions
01d,21aug01,pas  Added tcp structures.
01c,21aug01,ann  adding the socket structures
01b,08aug01,ppp  Adding some data structures in.
01a,07aug01,qli  created
*/


#ifndef _DS_CONF_H_
#define _DS_CONF_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <netVersion.h>

/* We don't #include any of the header files for these structs, because the
 * definitions aren't needed until the macros are expanded, which will be in a
 * context where the appropriate header files should already be included.
 */

/*
 * If you add any new pools, be sure to add an entry to _dsMallocSizes
 * in uipc_mbuf.c
 */
    
#define IFADDR_DS_SZ		sizeof(struct ifaddr)
#define IFADDR_DS_ID		0

#define IFMULTIADDR_DS_SZ	sizeof(struct ifmultiaddr)
#define IFMULTIADDR_DS_ID	1

#define SOCKADDR_DS_SZ		sizeof(struct sockaddr)
#define SOCKADDR_DS_ID		2

#define SOCKADDR_DL_DS_SZ	sizeof(struct sockaddr_dl)
#define SOCKADDR_DL_DS_ID	3

#define INPCB_DS_SZ		sizeof(struct inpcb)
#define INPCB_DS_ID		4 

#define IN_MULTI_DS_SZ		sizeof(struct in_multi)
#define IN_MULTI_DS_ID		5

#define IN_IFADDR_DS_SZ		sizeof(struct in_ifaddr)
#define IN_IFADDR_DS_ID		6

#define SOCKADDR_IN_DS_SZ	sizeof(struct sockaddr_in)
#define SOCKADDR_IN_DS_ID	7

#define INPCBPORT_DS_SZ		sizeof(struct inpcbport)
#define INPCBPORT_DS_ID		8

#define SOCKET_DS_SZ		sizeof(struct socket)
#define SOCKET_DS_ID		9

#define SOCK_LIB_MAP_BE_DS_SZ	sizeof(struct sockLibMap)
#define SOCK_LIB_MAP_BE_DS_ID	10

#define SOCKADDR_IN6_DS_SZ	sizeof(struct sockaddr_in6)
#define SOCKADDR_IN6_DS_ID	11

#define TSEG_QENT_DS_SZ		sizeof(struct tseg_qent)
#define TSEG_QENT_DS_ID		12

#define IP_MOPTIONS_DS_SZ	sizeof(struct ip_moptions)
#define IP_MOPTIONS_DS_ID	13

#define ROUTER_INFO_DS_SZ	sizeof(struct router_info)
#define ROUTER_INFO_DS_ID	14

#define ND_DEFROUTER_DS_SZ	sizeof(struct nd_defrouter)
#define ND_DEFROUTER_DS_ID	15

#define ND_PFXROUTER_DS_SZ	sizeof(struct nd_pfxrouter)
#define ND_PFXROUTER_DS_ID	16

#define ND_PREFIX_DS_SZ		sizeof(struct nd_prefix)
#define ND_PREFIX_DS_ID		17

#define LLINFO_ND6_DS_SZ	sizeof(struct llinfo_nd6)
#define LLINFO_ND6_DS_ID	18

#define RADIX_NODE_HEAD_DS_SZ	sizeof(struct radix_node_head)
#define RADIX_NODE_HEAD_DS_ID	19

#define RADIX_MASK_DS_SZ	sizeof(struct radix_mask)
#define RADIX_MASK_DS_ID	20

#define RADIX_NODE_DS_SZ	((sizeof(struct radix_node) <<1) + sizeof(struct sockaddr_in6))
#define RADIX_NODE_DS_ID	21
    
#define RTENTRY_DS_SZ		sizeof(struct rtentry)
#define RTENTRY_DS_ID		22

#define IN6_IFADDR_DS_SZ	sizeof(struct in6_ifaddr)
#define IN6_IFADDR_DS_ID	23

#define IN6_MULTI_DS_SZ		sizeof(struct in6_multi)
#define IN6_MULTI_DS_ID		24

#define IP6_PKTOPTS_DS_SZ	sizeof(struct ip6_pktopts)
#define IP6_PKTOPTS_DS_ID	25

#define IN6_PKTINFO_DS_SZ	sizeof(struct in6_pktinfo)
#define IN6_PKTINFO_DS_ID	26

#define RAWCB_DS_SZ		sizeof(struct rawcb)
#define RAWCB_DS_ID		27 

#define M2ID_DS_SZ		sizeof(struct M2_ID_S)
#define M2ID_DS_ID		28

#define IP6_MOPTIONS_DS_SZ	sizeof(struct ip6_moptions)
#define IP6_MOPTIONS_DS_ID	29

#define IN6_MULTI_MSHIP_DS_SZ	sizeof(struct in6_multi_mship)
#define IN6_MULTI_MSHIP_DS_ID	30

#define IPFLOW_DS_SZ		sizeof(struct ipflow)
#define IPFLOW_DS_ID		31

#define ETHER_MULTI_DS_SZ	sizeof(ETHER_MULTI)
#define ETHER_MULTI_DS_ID	32

#define ICMP6_FILTER_DS_SZ	sizeof(struct icmp6_filter)
#define ICMP6_FILTER_DS_ID	33

#define IP6Q_DS_SZ		sizeof(struct ip6q)
#define IP6Q_DS_ID		34

#define IP6ASFRAG_DS_SZ		sizeof(struct ip6asfrag)
#define IP6ASFRAG_DS_ID		35

#define IPQ_DS_SZ		sizeof(struct ipq)
#define IPQ_DS_ID		36

#define RP_ADDR_DS_SZ 		sizeof(struct rp_addr)
#define RP_ADDR_DS_ID		37

#define IN6_IFSTAT_DS_SZ	sizeof(struct in6_ifstat)
#define IN6_IFSTAT_DS_ID	38

#define ICMP6_IFSTAT_DS_SZ	sizeof(struct icmp6_ifstat)
#define ICMP6_IFSTAT_DS_ID	39

#define IN_HCENTRY_DS_SZ	sizeof(struct in_hcentry)
#define IN_HCENTRY_DS_ID	40

#define IFNET_DS_SZ		sizeof(struct ifnet)
#define IFNET_DS_ID		41

#define INP_TP_DS_SZ		sizeof(struct inp_tp)
#define INP_TP_DS_ID		42

#define MFC_DS_SZ		sizeof(struct mfc)
#define MFC_DS_ID		43

#define MF6C_DS_SZ		sizeof(struct mf6c)
#define MF6C_DS_ID		44

#define RTDETQ_DS_SZ		sizeof(struct rtdetq)
#define RTDETQ_DS_ID		45

#define M2IFINDEX_DS_SZ	        sizeof(M2_IFINDEX)
#define M2IFINDEX_DS_ID	        46

#define RADIX_ARG_DS_SZ		sizeof (struct radix_arg)
#define RADIX_ARG_DS_ID		47

#define RTHDR_DS_SZ		sizeof(struct routeHdr)
#define RTHDR_DS_ID		48

#define RT_ENTRY_DS_SZ		sizeof(struct rtentry)
#define RT_ENTRY_DS_ID		49

#define SCOPE6_ID_DS_SZ	        sizeof (struct scope6_id)
#define SCOPE6_ID_DS_ID	        50

#define LLINFO_ARP_DS_SZ	sizeof(struct llinfo_arp)
#define LLINFO_ARP_DS_ID	51

#define MFC_BW_DS_SZ		sizeof(struct bw_meter)
#define MFC_BW_DS_ID		52

#define RALCB_DS_SZ		sizeof(struct ralcb)
#define RALCB_DS_ID		53 

#define IN6_IFEXTRA_DS_SZ	sizeof(struct in6_ifextra)
#define IN6_IFEXTRA_DS_ID	54

#define ND6_IFINFO_DS_SZ	sizeof(struct nd_ifinfo)
#define ND6_IFINFO_DS_ID	55

#define IFMEDIA_ENTRY_DS_SZ	sizeof(struct ifmedia_entry)
#define IFMEDIA_ENTRY_DS_ID	56

#define SOCK_MSF_DS_SZ          sizeof(struct sock_msf)
#define SOCK_MSF_DS_ID          57

#define MSF_HEAD_DS_SZ          sizeof(struct msf_head)
#define MSF_HEAD_DS_ID          58

#define SOCK_MSF_SOURCE_DS_SZ   sizeof(struct sock_msf_source)
#define SOCK_MSF_SOURCE_DS_ID   59

#define SOCKADDR_STORAGE_DS_SZ  sizeof(struct sockaddr_storage)
#define SOCKADDR_STORAGE_DS_ID  60

#define IN_ADDR_SOURCE_DS_SZ    sizeof(struct in_addr_source)
#define IN_ADDR_SOURCE_DS_ID    61

#define IAS_HEAD_DS_SZ          sizeof(struct ias_head)
#define IAS_HEAD_DS_ID          62

#define IN_ADDR_SLIST_DS_SZ     sizeof(struct in_addr_slist)          
#define IN_ADDR_SLIST_DS_ID     63

#define IN_MULTI_SOURCE_DS_SZ   sizeof(struct in_multi_source)
#define IN_MULTI_SOURCE_DS_ID   64

#define IN6_ADDR_SLIST_DS_SZ    sizeof(struct in6_addr_slist)          
#define IN6_ADDR_SLIST_DS_ID    65

#define IN6_AS_HEAD_DS_SZ       sizeof(struct i6as_head)
#define IN6_AS_HEAD_DS_ID       66

#define IN6_ADDR_SOURCE_DS_SZ   sizeof(struct in6_addr_source)
#define IN6_ADDR_SOURCE_DS_ID   67

#define IN6_MULTI_SOURCE_DS_SZ  sizeof(struct in6_multi_source)
#define IN6_MULTI_SOURCE_DS_ID  68

#ifdef MIP6
#define MIP6_BC_INTERNAL_DS_SZ  sizeof(struct mip6_bc_internal)
#define MIP6_BC_INTERNAL_DS_ID  69

#define MIP6_BUL_INTERNAL_DS_SZ sizeof(struct mip6_bul_internal)
#define MIP6_BUL_INTERNAL_DS_ID 70
#endif /* MIP6 */

#ifdef SCTP
#define SCTP_INPCB_DS_SZ        sizeof (struct sctp_inpcb)
#define SCTP_INPCB_DS_ID        71

#define SCTP_TCB_DS_SZ          sizeof (struct sctp_tcb)
#define SCTP_TCB_DS_ID          72

#define SCTP_LADDR_DS_SZ        sizeof (struct sctp_laddr)
#define SCTP_LADDR_DS_ID        73

#define SCTP_NETS_DS_SZ         sizeof (struct sctp_nets)
#define SCTP_NETS_DS_ID         74

#define SCTP_TMITCHUNK_DS_SZ    sizeof (struct sctp_tmit_chunk)
#define SCTP_TMITCHUNK_DS_ID    75

#define SCTP_SOCKETQLIST_DS_SZ  sizeof (struct sctp_socket_q_list)
#define SCTP_SOCKETQLIST_DS_ID  76
#endif /* SCTP */

#ifdef IPSEC
#define SECPOLICY_DS_SZ		sizeof(struct secpolicy)
#define SECPOLICY_DS_ID		77 

#define SECASHEAD_DS_SZ		sizeof(struct secashead)
#define SECASHEAD_DS_ID		78

#define SECASVAR_DS_SZ		sizeof(struct secasvar)
#define SECASVAR_DS_ID		79

#define SECREPLAY_DS_SZ		sizeof(struct secreplay)
#define SECREPLAY_DS_ID		80

#define SECREG_DS_SZ		sizeof(struct secreg)
#define SECREG_DS_ID		81

#define KEYCB_DS_SZ		sizeof(struct keycb)
#define KEYCB_DS_ID		82

#define INPCBPOLICY_DS_SZ	sizeof(struct inpcbpolicy)
#define INPCBPOLICY_DS_ID	83

#define IPSECREQUEST_DS_SZ	sizeof(struct ipsecrequest)
#define IPSECREQUEST_DS_ID	84
#endif /* IPSEC */

#ifdef IPSEC
#define MAX_NUM_POOLS       85
#else
#define MAX_NUM_POOLS       77
#endif

/*
 * If you add any new pools, be sure to add an entry to _dsMallocSizes
 * in uipc_mbuf.c. Don't (re)move this comment as it is a useful reminder.
 */
    
#ifdef __cplusplus
}
#endif
	
#endif 

