/* callout.h - BSD callout facility */

/* Copyright 2001-2005 Wind River Systems, Inc. */

/*-
 * Copyright (c) 1990, 1993
 *	The Regents of the University of California.  All rights reserved.
 * (c) UNIX System Laboratories, Inc.
 * All or some portions of this file are derived from material licensed
 * to the University of California by American Telephone and Telegraph
 * Co. or Unix System Laboratories, Inc. and are reproduced herein with
 * the permission of UNIX System Laboratories, Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *	This product includes software developed by the University of
 *	California, Berkeley and its contributors.
 * 4. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *	@(#)callout.h	8.2 (Berkeley) 1/21/94
 * $FreeBSD: src/sys/sys/callout.h,v 1.15 1999/12/29 04:24:37 peter Exp $
 */

/*
modification history
--------------------
01o,11jul05,dlk  Added callout_ready() macro.
01n,11apr05,sar  Replace global tm with gtfTimerManagerCB
01m,09feb05,sar  Move gtf include files per GTF code inspection of 10/26/2004
01l,07feb05,vvv  _KERNEL cleanup
01k,23aug04,rp   merged from COMP_WN_IPV6_BASE6_ITER5_TO_UNIFIED_PRE_MERGE
01j,28may04,niq  Merging from base6 label POST_ITER5_FRZ16_REBASE (ver
                 /main/vdt/base6_itn5_networking-int/1)
01i,25nov03,wap  Merge changes from Snowflake
01h,20nov03,niq  Remove copyright_wrs.h file inclusion
01g,04nov03,rlm  Ran batch header path update for header re-org.
01f,03nov03,rlm  Removed wrn/coreip/ prefix from #includes for header re-org.
01e,27oct03,rlm  update include statements post header re-org.
01d,09sep02,hsh  add c++ protection
01c,02feb02,ham  changed for tornado build.
01b,29oct01,ham  included gtf_core.h.
01a,06jun01,deg  ported.
*/

#ifndef _SYS_CALLOUT_H_
#define _SYS_CALLOUT_H_

#ifdef _WRS_KERNEL

#include <private/gtf_core.h>
#include <private/gtf_wrapper.h>

#ifdef __cplusplus
extern "C" {
#endif
     
extern GTF_TIMER_MANAGER * gtfTimerManagerCB;


#define callout gtfTimer

#define	CALLOUT_LOCAL_ALLOC TIMER_LOCAL_ALLOC /* was allocated from callfree */
#define	CALLOUT_ACTIVE	    TIMER_ACTIVE   /* callout is currently active */
#define	CALLOUT_PENDING	    TIMER_PENDING  /* callout is waiting for timeout */

#define callout_handle gtfTimer*


/* Note well: callout_deactive should be called by non-GTF code only in
the context of the timer's handler.  This prevents a mutual exclusion issue
involving timerFlags that can occur between a call to callout_deactivate
and the clearing of TIMER_PENDING during a timer expiry. */

#define callout_active(c) (((GTF_TIMER *)(c))->timerFlags & TIMER_ACTIVE)
#define callout_deactivate(c) (((GTF_TIMER *)(c))->timerFlags &= ~TIMER_ACTIVE)
#define	callout_init(c) gtfCalloutInit((GTF_TIMER *)(c)) 
#define callout_pending(c) (((GTF_TIMER *)(c))->timerFlags & TIMER_PENDING)
#define	callout_reset(c, ticks, func, arg) \
    gtfTimerReset (gtfTimerManagerCB, (c), (ticks), \
		   (GTF_TIMER_HANDLER)(func), (arg))
#define callout_stop(c) gtfTimerCancel (gtfTimerManagerCB, (GTF_TIMER *)(c))

#define callout_ready(c) \
    (((((GTF_TIMER *)(c))->timerFlags & (TIMER_PENDING|TIMER_ACTIVE)) == \
      TIMER_ACTIVE) ? (callout_deactivate (c), 1) : 0)

/*
Use callout_ready() in callout handlers as follows:

spl = splnet(); /@ or other application-specific mutual exclusion @/
if (callout_ready (t))
    {
    /@ do the application-specific handler stuff @/
    }
splx (spl);
*/

#ifdef __cplusplus
}
#endif

#endif /* _WRS_KERNEL */
			   
#endif /* _SYS_CALLOUT_H_ */
