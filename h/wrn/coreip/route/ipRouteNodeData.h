/* ipRouteNodeData.h - data structures for the IP routing information base */

/* Copyright 1984 - 2005 Wind River Systems, Inc. */

/*
modification history
--------------------
01d,24feb05,spm  performance updates and code cleanup (SPR #100995)
01c,28may04,niq  Merging from base6 label POST_ITER5_FRZ16_REBASE
01b,09oct02,spm  added field for private RIB data
01a,09aug02,spm  written
*/

/*
DESCRIPTION
This file includes route entry structures for an implementation of
the Routing Information Base component that supports IP. The IP RIB
provides a generic interface for data structures that organize route
entries into RIB nodes according to the destination address and netmask
values. This file also defines macros to retrieve those values from
within the private data structures. 

All RIB implementations must use the common fields from the ribNode
structure defined in this include file at the start of every allocated
RIB node.
*/

#ifndef _INCipRouteNodeDatah
#define _INCipRouteNodeDatah

#ifdef __cplusplus
extern "C" {
#endif

typedef struct ipRouteNode
    {
    struct sockaddr * pDest;     /* "public" RTM address with family/length */
    struct sockaddr * pNetmask;  /* "public" RTM netmask with family/length */

    void *  pRibEntry;  /* Private RIB data: gets internal data structure */
    ULONG * pAddress; 	/* Pointer to IPv4 or IPv6 address bytes in pDest */
    ULONG   prefix;     /* IPv6 prefix length or full IPv4 netmask */
    void  * pRtmEntry; 	/* Private data for RTM: provides list of entries */
    } IP_ROUTE_NODE;

typedef struct ipRouteNode * IP_NODE_ID;

#ifdef __cplusplus
}
#endif

#endif /* _INCipRouteNodeDatah */
