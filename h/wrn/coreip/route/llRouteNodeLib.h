/* llRouteNodeLibh - VxWorks linked list definitions for the IP RIB */

/* Copyright 1984 - 2005 Wind River Systems, Inc. */

/*
modification history
--------------------
01d,07apr05,spm  converted for actual usage from test case
01c,04nov03,rlm  Ran batch header path update for header re-org.
01b,15sep03,vvv  updated path for new headers
01a,09aug02,spm  written
*/

/*
DESCRIPTION
This file includes the route entry structures for an implementation of
the IP Routing Information Base component that uses the native VxWorks
lstLib implementation of a linked list.
*/

#ifndef _INCllRouteNodeLibh
#define _INCllRouteNodeLibh

#ifdef __cplusplus
extern "C" {
#endif

#include <route/ipRouteNodeLib.h>
#include <netinet/in.h>    /* for sockaddr_in definition */

#include "lstLib.h"

struct llRouteNode 
    {
    struct ipRouteNode 	ribNodeEntry; 	/* Required common header for RIB */
    NODE neighbors;		 	/* Adjacent route entries, if any. */
    struct ipRouteNode * pNodeId;       /* Public identifier for RIB node. */
    };

struct llRibHead
    {
    struct ipRibHead llRibHead;            /* Required common structure */
    LIST             ipRouteList;
    };

/*
 * This structure allows private access to the address and netmask in the
 * llRouteNode structure. The add operation uses it to search for an existing
 * node with a particular destination and netmask. This alternate definition
 * avoids the impossible requirement of accessing the previous field in the
 * llRouteNode structure when starting with the NODE member.
 */

struct ipListRouteNode
    {
    NODE neighbors;
    struct ipRouteNode * pNodeId;
    };

#define ROUTE_ENTRY_NODE_ID(pIpListRouteNode) \
    ( ((struct ipListRouteNode *) (pIpListRouteNode))->pNodeId)

#define ROUTE_ENTRY_ADDRESS(pIpListRouteNode) \
 ( *((struct ipListRouteNode *) (pIpListRouteNode))->pNodeId->pAddress)

#define ROUTE_ENTRY_NETMASK(pIpListRouteNode) \
    ( ((struct ipListRouteNode *) (pIpListRouteNode))->pNodeId->prefix)

#ifdef __cplusplus
}
#endif

#endif /* _INCllRouteNodeLibh */
