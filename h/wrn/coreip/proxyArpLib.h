/* proxyArpLib.h - proxy ARP server include file */

/* Copyright 1984 - 2003 Wind River Systems, Inc. */

/*
modification history
--------------------
01o,04nov03,rlm  Ran batch header path update for header re-org.
01n,03nov03,rlm  Removed wrn/coreip/ prefix from #includes for header re-org.
01m,24oct03,cdw  update include statements post header re-org.
01l,15sep03,vvv  updated path for new headers
01k,27sep02,vlk  adjusted includes with _KERNEL switch
01j,31jul02,vlk  ported from Synth; added PROXY_ARP_CONFIG_PARAMS
01i,16nov01,rae  merged from tor2_x.veloce5, ver 01g, base 01d (minor cleanup)
01h,29mar01,spm  merged changes from version 01f of tor2_0.open_stack
                 branch (wpwr VOB, base 01d) for unified code base
01g,25oct00,ham  fixed compilation warnings.
01f,10aug00,spm  updated prototypes to remove compiler warnings
01e,10aug00,spm  merged proxy ARP updates from tor2_0_x branch
01d,22sep92,rrr  added support for c++
01c,04jul92,jcf  cleaned up.
01b,26may92,rrr  the tree shuffle
		  -changed includes to have absolute path from h/
01a,20sep91,elh	  written.
*/

#ifndef __INCproxyArpLibh
#define __INCproxyArpLibh

#ifdef __cplusplus
extern "C" {
#endif

/* includes */

#include <vwModNum.h>
#include <lstLib.h>
#include <semLib.h>
#include <hashLib.h>

#include <net/if.h>
#include <netinet/if_ether.h>
#include <netinet/in.h>
#include <cfgDefs.h>


/* defines */

/* errors */

#define	S_proxyArpLib_INVALID_PARAMETER		(M_proxyArpLib | 1)
#define S_proxyArpLib_INVALID_INTERFACE		(M_proxyArpLib | 2)
#define	S_proxyArpLib_INVALID_PROXY_NET		(M_proxyArpLib | 3)
#define	S_proxyArpLib_INVALID_CLIENT		(M_proxyArpLib | 4)
#define S_proxyArpLib_INVALID_ADDRESS		(M_proxyArpLib | 5)
#define S_proxyArpLib_TIMEOUT			(M_proxyArpLib | 6)

					/* proxy message types 	*/

#define PROXY_PROBE		0 	/* probe 	*/
#define PROXY_REG		1	/* register 	*/
#define PROXY_UNREG		2	/* unregister	*/
#define PROXY_ACK		50	/* ack		*/
#define PROXY_TYPE		0x3000  /* doesn't leave backplane */

#define XMIT_MAX		5
#define XMIT_DELAY		5

/* typedefs */

#if ((CPU_FAMILY==I960) && (defined __GNUC__))
#pragma align 1                 /* tell gcc960 not to optimize alignments */
#endif  /* CPU_FAMILY==I960 */

typedef struct proxy_net		/* proxy network structure 	*/
    {
    NODE		netNode;	/* node for net list		*/
    struct in_addr 	proxyAddr;	/* proxy interface address 	*/
    struct in_addr	mainAddr;	/* main interface address 	*/
    LIST	 	clientList; 	/* list of clients 		*/
    } PROXY_NET;

typedef struct proxy_clnt		/* proxy client structure	*/
    {
    HASH_NODE		hashNode;	/* hash node 			*/
    struct in_addr	ipaddr;		/* client ip address (key)	*/
    u_char 		hwaddr [6]; 	/* client hw address 		*/
    PROXY_NET *		pNet;		/* proxy network information 	*/
    NODE		clientNode;	/* client node on network list 	*/
    } PROXY_CLNT;

typedef struct port_node
    {
    HASH_NODE		hashNode;	/* hash  node			*/
    int			port;		/* port number enabled		*/
    } PORT_NODE;

typedef struct proxy_msg		/* proxy message 		*/
    {
    int			op;			/* operation		*/
    struct in_addr	clientAddr;		/* client address	*/
    struct in_addr	serverAddr;		/* server ip address	*/
    u_char 		clientHwAddr [6];	/* client hw address	*/
    u_char 		serverHwAddr [6];	/* server hw address	*/
    } PROXY_MSG;

/*the default configuration parameter structure*/
typedef struct proxy_arp_config_params {

    CFG_DATA_HDR cfgh;
    
    BOOL	cfg_arpDebug;
    BOOL	cfg_proxyArpVerbose;
    BOOL	cfg_proxyBroadcastVerbose;
    BOOL	cfg_proxyBroadcastFwd;
    BOOL	cfg_arpRegister;

    int     cfg_clnt_tbl_sz;
    int     cfg_port_tbl_sz;
    
    char    * cfg_proxyd_main_addr;
    char    * cfg_proxyd_proxy_addr;

    } PROXY_ARP_CONFIG_PARAMS;

#if ((CPU_FAMILY==I960) && (defined __GNUC__))
#pragma align 0                 /* turn off alignment requirement */
#endif  /* CPU_FAMILY==I960 */

#ifdef VIRTUAL_STACK
extern VS_REG_ID proxyArpRegistrationNum;
#endif /* VIRTUAL_STACK */
extern PROXY_ARP_CONFIG_PARAMS proxy_arpDefaultConfigParams;


#if defined(__STDC__) || defined(__cplusplus)
extern STATUS   proxy_arpInstInit (void *);
#ifdef VIRTUAL_STACK
extern STATUS   proxy_arpDestructor (VSNUM);
#endif

extern STATUS 	proxyArpLibInit (int clientSizeLog2, int portSizeLog2);
extern void 	proxyPortShow (void);
extern STATUS 	proxyPortFwdOn (int port);
extern STATUS 	proxyPortFwdOff (int port);
extern STATUS 	proxyNetCreate (char * proxyAddr, char * mainAddr);
extern STATUS 	proxyNetDelete (char * proxyAddr);

#else   /* __STDC__ */
extern STATUS   proxy_arpInstInit ();
#ifdef VIRTUAL_STACK
extern STATUS   proxy_arpDestructor ();
#endif

extern STATUS 	proxyArpLibInit ();
extern void 	proxyPortShow ();
extern STATUS 	proxyPortFwdOn ();
extern STATUS 	proxyPortFwdOff ();
extern STATUS 	proxyNetCreate ();
extern STATUS 	proxyNetDelete ();

#endif  /* __STDC__ */


#ifdef __cplusplus
}
#endif

#endif /* __INCproxyArpLibh */
