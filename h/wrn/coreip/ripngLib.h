/* ripngLib.h - header file for ripngLib.c */

/* Copyright 1992-2003 Wind River Systems, Inc. */

/*
modification history
--------------------
01g,05nov03,cdw  Removal of unnecessary _KERNEL guards.
01f,04nov03,rlm  Ran batch header path update for header re-org.
01e,03nov03,rlm  Removed wrn/coreip/ prefix from #includes for header re-org.
01d,15sep03,vvv  updated path for new headers
01c,06aug03,nee  Accordion Base6 merge from ACCORDION_BASE6_MERGE_BASELINE
                 label
01b,10jun03,vvv  include netVersion.h
01e,09oct02,kal added ripngStop(), removed variable initialiser
01d,10sep02,vlk removed unnecessary includes
01c,05sep02,hgo removed instInit and destructor
01b,05sep02,ger ifdeffed the function declaration for Instinit and destructor
                removed ifdef in cfg_params
01a,27jun02,ger created.
*/

#ifndef __INCripngLibh
#define __INCripngLibh

#ifdef __cplusplus
extern "C" {
#endif

#include <netVersion.h>

#if ((CPU_FAMILY==I960) && (defined __GNUC__))
#pragma align 1                 /* tell gcc960 not to optimize alignments */
#endif  /* CPU_FAMILY==I960 */

#ifdef VIRTUAL_STACK
#include <cfgDefs.h>
#endif


/* ripng configuration parameters */

typedef struct ripngcfgparams
    {
    CFG_DATA_HDR 	cfgh ;
    int 		cfg_ripng_priority ;
    char		*cfg_ripng_options ;	        
    } RIPNG_CFG_PARAMS;

/* function declarations */

#if defined(__STDC__) || defined(__cplusplus)

extern void     ripngStart (char *cmdString, int priority);
extern void     ripngStop (void);

#else   /* __STDC__ */

extern void     ripngStart ();
extern void     ripngStop ();

#endif  /* __STDC__ */

#ifdef __cplusplus
}
#endif

#endif /* __INCripngLibh */

