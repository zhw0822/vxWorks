/* igmp_state_machine.h - state transition definitions and table */

/* Copyright 1997 - 2001 Wind River Systems, Inc. */

/*
modification history
--------------------
01b,20nov03,niq  Remove copyright_wrs.h file inclusion
01a,29mar01,spm  file creation: copied from version 01c of tor2_0.open_stack
                 branch (wpwr VOB) for unified code base
*/

#if !defined (_IGMP_STATE_MACHINE_H_)
#define _IGMP_STATE_MACHINE_H_

#if defined GLOBAL_FILE

/***************************************************************************************************************/
/*                                    Port State Transition Table                                              */
/*                                                                                                             */
/*    +----------------------------++---------------------------------------------------------------------+    */
/*    |                            ||                               State                                 |    */
/*    |                            ++---------------------------------+-----------------------------------|    */
/*    |                            ||                                 |                                   |    */
/*    | Event                      ||    Non Querier                  |     Querier                       |    */
/*    |============================++=================================+===================================|    */
/*    |General Query Timer Expired ||   o No actions                  |    o Trasmit General Query        |    */
/*    |                            ||                                 |    o Start General Query Timer    |    */
/*    |                            ||                                 |                                   |    */
/*    |                            ||  -> Invalid state               |   -> Querier                      |    */
/*    |----------------------------++---------------------------------+-----------------------------------|    */
/*    |Query Received from Router  ||   o Start other Querier         |    o Start Other Querier          |    */
/*    |    with Lower IP address   ||     Present Timer               |      Present Timer                |    */
/*    |                            ||                                 |                                   |    */
/*    |                            ||  -> Non Querier                 |    -> Non Querier                 |    */
/*    |----------------------------++---------------------------------+-----------------------------------|    */
/*    |Other Querier Present timer ||   o Transmit General Query      |     o No actions                  |    */
/*    |                            ||   o Start General Query Timer   |                                   |    */
/*    |                            ||                                 |                                   |    */
/*    |                            ||  -> Querier                     |    -> Invalid State               |    */
/*    +----------------------------++---------------------------------+-----------------------------------+    */
/*                                                                                                             */
/*                                                                                                             */
/***************************************************************************************************************/

IGMP_ROUTER_PORT_STATE_TRANSITION_TABLE_ENTRY
	igmp_router_port_state_transition_table[NUMBER_OF_IGMP_ROUTER_PORT_STATES][NUMBER_OF_IGMP_ROUTER_PORT_EVENTS] =
{
/*-------------------------------------------------*/
/*        IGMP_ROUTER_PORT_NON_QUERIER_STATE       */
/*-------------------------------------------------*/
{
/* IGMP_ROUTER_PORT_GENERAL_QUERY_TIMER_EXPIRED_EVENT */                        {NULL, NULL, NULL, (enum IGMP_ROUTER_PORT_STATE) IGMP_INVALID_STATE},

/* IGMP_ROUTER_PORT_QUERY_RECEIVED_FROM_ROUTER_WITH_LOWER_IP_ADDRESS_EVENT */	  {igmp_router_start_other_querier_present_timer,
                                                                                 NULL,
                                                                                 NULL,
                                                                                 (enum IGMP_ROUTER_PORT_STATE) IGMP_ROUTER_PORT_NON_QUERIER_STATE},

/* IGMP_ROUTER_PORT_OTHER_QUERIER_PRESENT_TIMER_EXPIRED_EVENT */					  {igmp_router_tx_message,
                                                                                 igmp_router_start_general_query_timer,
                                                                                 NULL,
                                                                                 (enum IGMP_ROUTER_PORT_STATE) IGMP_ROUTER_PORT_QUERIER_STATE}
},
/*-------------------------------------------------*/
/*        IGMP_ROUTER_PORT_QUERIER_STATE           */
/*-------------------------------------------------*/
{
/* IGMP_ROUTER_PORT_GENERAL_QUERY_TIMER_EXPIRED_EVENT */                        {igmp_router_tx_message,
                                                                                 igmp_router_start_general_query_timer,
                                                                                 NULL,
                                                                                 (enum IGMP_ROUTER_PORT_STATE) IGMP_ROUTER_PORT_QUERIER_STATE},

/* IGMP_ROUTER_PORT_QUERY_RECEIVED_FROM_ROUTER_WITH_LOWER_IP_ADDRESS_EVENT */	  {igmp_router_start_other_querier_present_timer,
                                                                                 NULL,
                                                                                 NULL,
                                                                                 (enum IGMP_ROUTER_PORT_STATE) IGMP_ROUTER_PORT_NON_QUERIER_STATE},

/* IGMP_ROUTER_PORT_OTHER_QUERIER_PRESENT_TIMER_EXPIRED_EVENT */					  {NULL, NULL, NULL, (enum IGMP_ROUTER_PORT_STATE) IGMP_INVALID_STATE}
}
};

/***************************************************************************************************************************/
/*                                           Querier Mode Group Next State Table                                           */
/*                                                                                                                         */
/*  +-------------------++----------------------------------------------------------------------------------------------+  */
/*  |                   ||                                                                                              |  */
/*  |                   ||                                            State                                             |  */
/*  |                   ++----------------------------------------------------------------------------------------------|  */
/*  | Event             ||  No Members Present     V1 Members Present      V2 Members Present      Checking Membership  |  */
/*  |-------------------++----------------------+-----------------------+-----------------------+-----------------------|  */
/*  |V2 Report Received || o Notify Routing of  | o Start Group         | o Start Group         | o Start Group         |  */
/*  |                   ||   Group Addition     |   Specific Timer      |   Specific Timer      |   Specific Timer      |  */
/*  |                   || o Start Group        |                       |                       |                       |  */
/*  |                   ||   Specific Timer     |                       |                       |                       |  */
/*  |                   ||                      |                       |                       |                       |  */
/*  |                   ||-> V2 Members Present |-> V1 Members Present  |-> V2 Members Present  |-> V2 Members Present  |  */
/*  |-------------------++----------------------+-----------------------+-----------------------+-----------------------|  */                    
/*  |V1 Report Received || o Notify Routing of  | o Start Group         | o Start Group         | o Start Group         |  */
/*  |                   ||   Group Addition     |   Specific Timer      |   Specific Timer      |   Specific Timer      |  */
/*  |                   || o Start Group        | o Start V1 Host       | o Start V1 Host       | o Start V1 Host       |  */
/*  |                   ||   Specific Timer     |   Timer               |   Timer               |   Timer               |  */
/*  |                   || o Start V1 Host      |                       |                       |                       |  */
/*  |                   ||   Timer              |                       |                       |                       |  */
/*  |                   ||                      |                       |                       |                       |  */
/*  |                   ||-> V1 Members Present |-> V1 Members Present  |-> V1 Members Present  |-> V1 Members Present  |  */
/*  |-------------------++----------------------+-----------------------+-----------------------+-----------------------|  */
/*  |Leave Message      || o No actions         | o No actions          | o Start star timer    | o No actions          |  */
/*  |    Received       ||                      |                       | o Start retransmit    |                       |  */
/*  |                   ||                      |                       |   timer               |                       |  */
/*  |                   ||                      |                       | o Send group specific |                       |  */
/*  |                   ||                      |                       |   query               |                       |  */
/*  |                   ||                      |                       |                       |                       |  */
/*  |                   ||-> Invalid State      |-> Invalid State       |-> Checking Membership |-> Invalid State       |  */
/*  |-------------------++----------------------+-----------------------+-----------------------+-----------------------|  */
/*  |Group Specific     || o No actions         | o Notify Routing of   | o Notify Routing of   | o Notify Routing of   |  */
/*  |    Timer Expired  ||                      |   Group Deletion      |   Group Deletion      |   Group Deletion      |  */
/*  |                   ||                      |                       |                       | o Clear retransmit    |  */
/*  |                   ||                      |                       |                       |   timer               |  */
/*  |                   ||                      |                       |                       |                       |  */
/*  |                   ||                      |                       |                       |                       |  */
/*  |                   ||-> Invalid State      |-> No Members Present  |-> No Members Present  |-> No Members Present  |  */
/*  |-------------------++----------------------+-----------------------+-----------------------+-----------------------|  */
/*  |Retransmit Timer   || o No actions         | o No actions          | o No actions          | o Send group specific |  */
/*  |    Expired        ||                      |                       |                       |   query               |  */
/*  |                   ||                      |                       |                       | o Start retransmit    |  */
/*  |                   ||                      |                       |                       |   timer               |  */
/*  |                   ||                      |                       |                       |                       |  */
/*  |                   ||-> Invalid State      |-> Invalid State       |-> Invalid State       |-> Checking Membership |  */
/*  |-------------------++----------------------+-----------------------+-----------------------+-----------------------|  */
/*  |V1 Host Timer      || o No actions         | o No actions          | o No actions          | o No actions          |  */
/*  |    Expired        ||                      |                       |                       |                       |  */
/*  |                   ||                      |                       |                       |                       |  */
/*  |                   ||-> Invalid State      |-> V2 Members Present  |-> Invalid State       |-> Invalid State       |  */
/*  |-------------------++----------------------+-----------------------+-----------------------+-----------------------|  */
/*  |Group Specific     || o No actions         | o No actions          | o No actions          | o No actions          |  */
/*  |    Query Received ||                      |                       |                       |                       |  */
/*  |                   ||-> Invalid State      |-> Invalid State       |-> Invalid State       |-> Invalid State       |  */
/*  +-------------------++----------------------+-----------------------+-----------------------+-----------------------+  */
/*                                                                                                                         */
/***************************************************************************************************************************/

IGMP_ROUTER_GROUP_STATE_TRANSITION_TABLE_ENTRY
	igmp_router_querier_mode_group_state_transition_table[NUMBER_OF_IGMP_ROUTER_GROUP_STATES][NUMBER_OF_IGMP_ROUTER_GROUP_EVENTS] =
{
/*-------------------------------------------------*/
/*    IGMP_ROUTER_GROUP_NO_MEMBERS_PRESENT_STATE   */
/*-------------------------------------------------*/
{
/* IGMP_ROUTER_GROUP_V2_REPORT_RECEIVED_EVENT */        {igmp_router_notify_routing_of_group_addition,
                                                         igmp_router_start_group_specific_timer,
                                                         NULL,
                                                         (enum IGMP_ROUTER_GROUP_STATE) IGMP_ROUTER_GROUP_V2_MEMBERS_PRESENT_STATE},

/* IGMP_ROUTER_GROUP_V1_REPORT_RECEIVED_EVENT */		  {igmp_router_notify_routing_of_group_addition,
                                                         igmp_router_start_group_specific_timer,
                                                         igmp_router_start_v1_host_timer,
                                                         (enum IGMP_ROUTER_GROUP_STATE) IGMP_ROUTER_GROUP_V1_MEMBERS_PRESENT_STATE},

/* IGMP_ROUTER_GROUP_LEAVE_MESSAGE_RECEIVED_EVENT */	  {NULL, NULL, NULL, (enum IGMP_ROUTER_GROUP_STATE) IGMP_INVALID_STATE},

/* IGMP_ROUTER_GROUP_SPECIFIC_TIMER_EXPIRED_EVENT */	  {NULL, NULL, NULL, (enum IGMP_ROUTER_GROUP_STATE) IGMP_INVALID_STATE},

/* IGMP_ROUTER_GROUP_RETRANSMIT_TIMER_EXPIRED_EVENT */  {NULL, NULL, NULL, (enum IGMP_ROUTER_GROUP_STATE) IGMP_INVALID_STATE},

/* IGMP_ROUTER_GROUP_V1_HOST_TIMER_EXPIRED_EVENT */	  {NULL, NULL, NULL, (enum IGMP_ROUTER_GROUP_STATE) IGMP_INVALID_STATE},

/* IGMP_ROUTER_GROUP_SPECIFIC_QUERY_RECEIVED_EVENT */	  {NULL, NULL, NULL, (enum IGMP_ROUTER_GROUP_STATE) IGMP_INVALID_STATE}
},
/*-------------------------------------------------*/
/*    IGMP_ROUTER_GROUP_V1_MEMBERS_PRESENT_STATE   */
/*-------------------------------------------------*/
{
/* IGMP_ROUTER_GROUP_V2_REPORT_RECEIVED_EVENT */        {igmp_router_start_group_specific_timer,
                                                         NULL,
                                                         NULL,
                                                         (enum IGMP_ROUTER_GROUP_STATE) IGMP_ROUTER_GROUP_V1_MEMBERS_PRESENT_STATE},

/* IGMP_ROUTER_GROUP_V1_REPORT_RECEIVED_EVENT */		  {igmp_router_start_group_specific_timer,
                                                         igmp_router_start_v1_host_timer,
                                                         NULL,
                                                         (enum IGMP_ROUTER_GROUP_STATE) IGMP_ROUTER_GROUP_V1_MEMBERS_PRESENT_STATE},

/* IGMP_ROUTER_GROUP_LEAVE_MESSAGE_RECEIVED_EVENT */	  {NULL, NULL, NULL, (enum IGMP_ROUTER_GROUP_STATE) IGMP_INVALID_STATE},

/* IGMP_ROUTER_GROUP_SPECIFIC_TIMER_EXPIRED_EVENT */	  {igmp_router_notify_routing_of_group_deletion,
                                                         NULL,
                                                         NULL,
                                                         (enum IGMP_ROUTER_GROUP_STATE) IGMP_ROUTER_GROUP_NO_MEMBERS_PRESENT_STATE},

/* IGMP_ROUTER_GROUP_RETRANSMIT_TIMER_EXPIRED_EVENT */  {NULL, NULL, NULL, (enum IGMP_ROUTER_GROUP_STATE) IGMP_INVALID_STATE},

/* IGMP_ROUTER_GROUP_V1_HOST_TIMER_EXPIRED_EVENT */	  {NULL, NULL, NULL, (enum IGMP_ROUTER_GROUP_STATE) IGMP_ROUTER_GROUP_V2_MEMBERS_PRESENT_STATE},

/* IGMP_ROUTER_GROUP_SPECIFIC_QUERY_RECEIVED_EVENT */	  {NULL, NULL, NULL, (enum IGMP_ROUTER_GROUP_STATE) IGMP_INVALID_STATE}
},
/*-------------------------------------------------*/
/*    IGMP_ROUTER_GROUP_V2_MEMBERS_PRESENT_STATE   */
/*-------------------------------------------------*/
{
/* IGMP_ROUTER_GROUP_V2_REPORT_RECEIVED_EVENT */        {igmp_router_start_group_specific_timer,
                                                         NULL,
                                                         NULL,
                                                         (enum IGMP_ROUTER_GROUP_STATE) IGMP_ROUTER_GROUP_V2_MEMBERS_PRESENT_STATE},

/* IGMP_ROUTER_GROUP_V1_REPORT_RECEIVED_EVENT */		  {igmp_router_start_group_specific_timer,
                                                         igmp_router_start_v1_host_timer,
                                                         NULL,
                                                         (enum IGMP_ROUTER_GROUP_STATE) IGMP_ROUTER_GROUP_V1_MEMBERS_PRESENT_STATE},

/* IGMP_ROUTER_GROUP_LEAVE_MESSAGE_RECEIVED_EVENT */	  {igmp_router_start_star_timer,
                                                         igmp_router_start_retransmit_timer,
                                                         igmp_router_tx_message,
                                                         (enum IGMP_ROUTER_GROUP_STATE) IGMP_ROUTER_GROUP_CHECKING_MEMBERSHIP_STATE},

/* IGMP_ROUTER_GROUP_SPECIFIC_TIMER_EXPIRED_EVENT */	  {igmp_router_notify_routing_of_group_deletion,
                                                         NULL,
                                                         NULL,
                                                         (enum IGMP_ROUTER_GROUP_STATE) IGMP_ROUTER_GROUP_NO_MEMBERS_PRESENT_STATE},

/* IGMP_ROUTER_GROUP_RETRANSMIT_TIMER_EXPIRED_EVENT */  {NULL, NULL, NULL, (enum IGMP_ROUTER_GROUP_STATE) IGMP_INVALID_STATE},

/* IGMP_ROUTER_GROUP_V1_HOST_TIMER_EXPIRED_EVENT */	  {NULL, NULL, NULL, (enum IGMP_ROUTER_GROUP_STATE) IGMP_INVALID_STATE},

/* IGMP_ROUTER_GROUP_SPECIFIC_QUERY_RECEIVED_EVENT */	  {NULL, NULL, NULL, (enum IGMP_ROUTER_GROUP_STATE) IGMP_INVALID_STATE}
},
/*-------------------------------------------------*/
/*    IGMP_ROUTER_GROUP_CHECKING_MEMBERSHIP_STATE  */
/*-------------------------------------------------*/
{
/* IGMP_ROUTER_GROUP_V2_REPORT_RECEIVED_EVENT */        {igmp_router_start_group_specific_timer,
                                                         NULL,
                                                         NULL,
                                                         (enum IGMP_ROUTER_GROUP_STATE) IGMP_ROUTER_GROUP_V2_MEMBERS_PRESENT_STATE},

/* IGMP_ROUTER_GROUP_V1_REPORT_RECEIVED_EVENT */		  {igmp_router_start_group_specific_timer,
                                                         igmp_router_start_v1_host_timer,
                                                         NULL,
                                                         (enum IGMP_ROUTER_GROUP_STATE) IGMP_ROUTER_GROUP_V1_MEMBERS_PRESENT_STATE},

/* IGMP_ROUTER_GROUP_LEAVE_MESSAGE_RECEIVED_EVENT */	  {NULL, NULL, NULL, (enum IGMP_ROUTER_GROUP_STATE) IGMP_INVALID_STATE},

/* IGMP_ROUTER_GROUP_SPECIFIC_TIMER_EXPIRED_EVENT */	  {igmp_router_notify_routing_of_group_deletion,
                                                         igmp_router_clear_retransmit_timer,
                                                         NULL,
                                                         (enum IGMP_ROUTER_GROUP_STATE) IGMP_ROUTER_GROUP_NO_MEMBERS_PRESENT_STATE},

/* IGMP_ROUTER_GROUP_RETRANSMIT_TIMER_EXPIRED_EVENT */  {igmp_router_tx_message,
                                                         igmp_router_start_retransmit_timer,
                                                         NULL,
                                                         (enum IGMP_ROUTER_GROUP_STATE) IGMP_ROUTER_GROUP_CHECKING_MEMBERSHIP_STATE},

/* IGMP_ROUTER_GROUP_V1_HOST_TIMER_EXPIRED_EVENT */	  {NULL, NULL, NULL, (enum IGMP_ROUTER_GROUP_STATE) IGMP_INVALID_STATE},

/* IGMP_ROUTER_GROUP_SPECIFIC_QUERY_RECEIVED_EVENT */	  {NULL, NULL, NULL, (enum IGMP_ROUTER_GROUP_STATE) IGMP_INVALID_STATE}
}
};

/***************************************************************************************************************************/
/*                                           Non Querier Mode Group State Transition Table                                 */
/*                                                                                                                         */
/*  +-------------------++----------------------------------------------------------------------------------------------+  */
/*  |                   ||                                                                                              |  */
/*  |                   ||                                            State                                             |  */
/*  |                   ++----------------------------------------------------------------------------------------------|  */
/*  | Event             ||  No Members Present     V1 Members Present      V2 Members Present      Checking Membership  |  */
/*  |-------------------++----------------------+-----------------------+-----------------------+-----------------------|  */
/*  |V2 Report Received || o Notify Routing of  | o Start Group         | o No actions          | o Start Group         |  */
/*  |                   ||   group addition     |   Specific Timer      |                       |   Specific Timer      |  */
/*  |                   || o Start Group        |                       |                       |                       |  */
/*  |                   ||   Specific Timer     |                       |                       |                       |  */
/*  |                   ||                      |                       |                       |                       |  */
/*  |                   ||-> V1 Members Present |-> V1 Members Present  |-> Invalid State       |-> V1 Members Present  |  */
/*  |-------------------++----------------------+-----------------------+-----------------------+-----------------------|  */                    
/*  |V1 Report Received || o Notify Routing of  | o Start Group         | o No actions          | o Start Group         |  */
/*  |                   ||   group addition     |   Specific Timer      |                       |   Specific Timer      |  */
/*  |                   || o Start Group        |                       |                       |                       |  */
/*  |                   ||   Specific Timer     |                       |                       |                       |  */
/*  |                   ||                      |                       |                       |                       |  */
/*  |                   ||-> V1 Members Present |-> V1 Members Present  |-> Invalid State       |-> V1 Members Present  |  */
/*  |-------------------++----------------------+-----------------------+-----------------------+-----------------------|  */
/*  |Leave Message      || o No actions         | o No actions          | o No actions          |  o No actions         |  */
/*  |    Received       ||                      |                       |                       |                       |  */
/*  |                   ||-> Invalid State      |-> Invalid State       |-> Invalid State       |-> Invalid State       |  */
/*  |-------------------++----------------------+-----------------------+-----------------------+-----------------------|  */
/*  |Group Specific     || o No actions         | o Notify Routing of   | o No actions          |  o Notify Routing of  |  */
/*  |    Timer Expired  ||                      |   Group Deletion      |                       |    Group Deletion     |  */
/*  |                   ||                      |                       |                       |                       |  */
/*  |                   ||-> Invalid State      |-> No Members Present  |-> Invalid State       |-> No Members Present  |  */
/*  |-------------------++----------------------+-----------------------+-----------------------+-----------------------|  */
/*  |Retransmit Timer   || o No actions         | o No actions          | o No actions          |  o No actions         |  */
/*  |    Expired        ||                      |                       |                       |                       |  */
/*  |                   ||-> Invalid State      |-> Invalid State       |-> Invalid State       |-> Invalid State       |  */
/*  |-------------------++----------------------+-----------------------+-----------------------+-----------------------|  */
/*  |V1 Host Timer      || o No actions         | o No actions          | o No actions          |  o No actions         |  */
/*  |    Expired        ||                      |                       |                       |                       |  */
/*  |                   ||                      |                       |                       |                       |  */
/*  |                   ||-> Invalid State      |-> Invalid State       |-> Invalid State       |-> Invalid State       |  */
/*  |-------------------++----------------------+-----------------------+-----------------------+-----------------------|  */
/*  |Group Specific     || o No actions         | o Start star timer    | o No actions          |  o No actions         |  */
/*  |    Query Received ||                      |                       |                       |                       |  */
/*  |                   ||                      |                       |                       |                       |  */
/*  |                   ||-> Invalid State      |-> Checking Membership |-> Invalid State       |-> Invalid State       |  */
/*  +-------------------++----------------------+-----------------------+-----------------------+-----------------------+  */
/*                                                                                                                         */
/***************************************************************************************************************************/

IGMP_ROUTER_GROUP_STATE_TRANSITION_TABLE_ENTRY
	igmp_router_non_querier_mode_group_state_transition_table[NUMBER_OF_IGMP_ROUTER_GROUP_STATES][NUMBER_OF_IGMP_ROUTER_GROUP_EVENTS] =
{
/*-------------------------------------------------*/
/*    IGMP_ROUTER_GROUP_NO_MEMBERS_PRESENT_STATE   */
/*-------------------------------------------------*/
{
/* IGMP_ROUTER_GROUP_V2_REPORT_RECEIVED_EVENT */        {igmp_router_notify_routing_of_group_addition,
                                                         igmp_router_start_group_specific_timer,
                                                         NULL,
                                                         (enum IGMP_ROUTER_GROUP_STATE) IGMP_ROUTER_GROUP_V1_MEMBERS_PRESENT_STATE},

/* IGMP_ROUTER_GROUP_V1_REPORT_RECEIVED_EVENT */		  {igmp_router_notify_routing_of_group_addition,
                                                         igmp_router_start_group_specific_timer,
                                                         NULL,
                                                         (enum IGMP_ROUTER_GROUP_STATE) IGMP_ROUTER_GROUP_V1_MEMBERS_PRESENT_STATE},

/* IGMP_ROUTER_GROUP_LEAVE_MESSAGE_RECEIVED_EVENT */	  {NULL, NULL, NULL, (enum IGMP_ROUTER_GROUP_STATE) IGMP_INVALID_STATE},

/* IGMP_ROUTER_GROUP_SPECIFIC_TIMER_EXPIRED_EVENT */	  {NULL, NULL, NULL, (enum IGMP_ROUTER_GROUP_STATE) IGMP_INVALID_STATE},

/* IGMP_ROUTER_GROUP_RETRANSMIT_TIMER_EXPIRED_EVENT */  {NULL, NULL, NULL, (enum IGMP_ROUTER_GROUP_STATE) IGMP_INVALID_STATE},

/* IGMP_ROUTER_GROUP_V1_HOST_TIMER_EXPIRED_EVENT */	  {NULL, NULL, NULL, (enum IGMP_ROUTER_GROUP_STATE) IGMP_INVALID_STATE},

/* IGMP_ROUTER_GROUP_SPECIFIC_QUERY_RECEIVED_EVENT */	  {NULL, NULL, NULL, (enum IGMP_ROUTER_GROUP_STATE) IGMP_INVALID_STATE}
},
/*-------------------------------------------------*/
/*    IGMP_ROUTER_GROUP_V1_MEMBERS_PRESENT_STATE   */
/*-------------------------------------------------*/
{
/* IGMP_ROUTER_GROUP_V2_REPORT_RECEIVED_EVENT */        {igmp_router_start_group_specific_timer,
                                                         NULL,
                                                         NULL,
                                                         (enum IGMP_ROUTER_GROUP_STATE) IGMP_ROUTER_GROUP_V1_MEMBERS_PRESENT_STATE},

/* IGMP_ROUTER_GROUP_V1_REPORT_RECEIVED_EVENT */		  {igmp_router_start_group_specific_timer,
                                                         NULL,
                                                         NULL,
                                                         (enum IGMP_ROUTER_GROUP_STATE) IGMP_ROUTER_GROUP_V1_MEMBERS_PRESENT_STATE},

/* IGMP_ROUTER_GROUP_LEAVE_MESSAGE_RECEIVED_EVENT */	  {NULL, NULL, NULL, (enum IGMP_ROUTER_GROUP_STATE) IGMP_INVALID_STATE},

/* IGMP_ROUTER_GROUP_SPECIFIC_TIMER_EXPIRED_EVENT */	  {igmp_router_notify_routing_of_group_deletion,
                                                         NULL,
                                                         NULL,
                                                         (enum IGMP_ROUTER_GROUP_STATE) IGMP_ROUTER_GROUP_NO_MEMBERS_PRESENT_STATE},

/* IGMP_ROUTER_GROUP_RETRANSMIT_TIMER_EXPIRED_EVENT */  {NULL, NULL, NULL, (enum IGMP_ROUTER_GROUP_STATE) IGMP_INVALID_STATE},

/* IGMP_ROUTER_GROUP_V1_HOST_TIMER_EXPIRED_EVENT */	  {NULL, NULL, NULL, (enum IGMP_ROUTER_GROUP_STATE) IGMP_INVALID_STATE},

/* IGMP_ROUTER_GROUP_SPECIFIC_QUERY_RECEIVED_EVENT */	  {igmp_router_start_star_timer,
                                                         NULL,
                                                         NULL,
                                                         (enum IGMP_ROUTER_GROUP_STATE) IGMP_ROUTER_GROUP_CHECKING_MEMBERSHIP_STATE}
},
/*-------------------------------------------------*/
/*    IGMP_ROUTER_GROUP_V2_MEMBERS_PRESENT_STATE   */
/*-------------------------------------------------*/
{
/* IGMP_ROUTER_GROUP_V2_REPORT_RECEIVED_EVENT */        {NULL, NULL, NULL, (enum IGMP_ROUTER_GROUP_STATE) IGMP_INVALID_STATE},

/* IGMP_ROUTER_GROUP_V1_REPORT_RECEIVED_EVENT */		  {NULL, NULL, NULL, (enum IGMP_ROUTER_GROUP_STATE) IGMP_INVALID_STATE},

/* IGMP_ROUTER_GROUP_LEAVE_MESSAGE_RECEIVED_EVENT */	  {NULL, NULL, NULL, (enum IGMP_ROUTER_GROUP_STATE) IGMP_INVALID_STATE},

/* IGMP_ROUTER_GROUP_SPECIFIC_TIMER_EXPIRED_EVENT */	  {NULL, NULL, NULL, (enum IGMP_ROUTER_GROUP_STATE) IGMP_INVALID_STATE},

/* IGMP_ROUTER_GROUP_RETRANSMIT_TIMER_EXPIRED_EVENT */  {NULL, NULL, NULL, (enum IGMP_ROUTER_GROUP_STATE) IGMP_INVALID_STATE},

/* IGMP_ROUTER_GROUP_V1_HOST_TIMER_EXPIRED_EVENT */	  {NULL, NULL, NULL, (enum IGMP_ROUTER_GROUP_STATE) IGMP_INVALID_STATE},

/* IGMP_ROUTER_GROUP_SPECIFIC_QUERY_RECEIVED_EVENT */	  {NULL, NULL, NULL, (enum IGMP_ROUTER_GROUP_STATE) IGMP_INVALID_STATE}
},
/*-------------------------------------------------*/
/*    IGMP_ROUTER_GROUP_CHECKING_MEMBERSHIP_STATE  */
/*-------------------------------------------------*/
{
/* IGMP_ROUTER_GROUP_V2_REPORT_RECEIVED_EVENT */        {igmp_router_start_group_specific_timer,
                                                         NULL,
                                                         NULL,
                                                         (enum IGMP_ROUTER_GROUP_STATE) IGMP_ROUTER_GROUP_V1_MEMBERS_PRESENT_STATE},

/* IGMP_ROUTER_GROUP_V1_REPORT_RECEIVED_EVENT */		  {igmp_router_start_group_specific_timer,
                                                         NULL,
                                                         NULL,
                                                         (enum IGMP_ROUTER_GROUP_STATE) IGMP_ROUTER_GROUP_V1_MEMBERS_PRESENT_STATE},

/* IGMP_ROUTER_GROUP_LEAVE_MESSAGE_RECEIVED_EVENT */	  {NULL, NULL, NULL, (enum IGMP_ROUTER_GROUP_STATE) IGMP_INVALID_STATE},

/* IGMP_ROUTER_GROUP_SPECIFIC_TIMER_EXPIRED_EVENT */	  {igmp_router_notify_routing_of_group_deletion,
                                                         NULL,
                                                         NULL,
                                                         (enum IGMP_ROUTER_GROUP_STATE) IGMP_ROUTER_GROUP_NO_MEMBERS_PRESENT_STATE},

/* IGMP_ROUTER_GROUP_RETRANSMIT_TIMER_EXPIRED_EVENT */  {NULL, NULL, NULL, (enum IGMP_ROUTER_GROUP_STATE) IGMP_INVALID_STATE},

/* IGMP_ROUTER_GROUP_V1_HOST_TIMER_EXPIRED_EVENT */	  {NULL, NULL, NULL, (enum IGMP_ROUTER_GROUP_STATE) IGMP_INVALID_STATE},

/* IGMP_ROUTER_GROUP_SPECIFIC_QUERY_RECEIVED_EVENT */	  {NULL, NULL, NULL, (enum IGMP_ROUTER_GROUP_STATE) IGMP_INVALID_STATE}
}
};


#else

extern IGMP_ROUTER_PORT_STATE_TRANSITION_TABLE_ENTRY
	igmp_router_port_state_transition_table[NUMBER_OF_IGMP_ROUTER_PORT_STATES][NUMBER_OF_IGMP_ROUTER_PORT_EVENTS];
extern IGMP_ROUTER_GROUP_STATE_TRANSITION_TABLE_ENTRY
	igmp_router_querier_mode_group_state_transition_table[NUMBER_OF_IGMP_ROUTER_GROUP_STATES][NUMBER_OF_IGMP_ROUTER_GROUP_EVENTS];
extern IGMP_ROUTER_GROUP_STATE_TRANSITION_TABLE_ENTRY
	igmp_router_non_querier_mode_group_state_transition_table[NUMBER_OF_IGMP_ROUTER_GROUP_STATES][NUMBER_OF_IGMP_ROUTER_GROUP_EVENTS];
extern IGMP_HOST_STATE_TRANSITION_TABLE_ENTRY
	igmp_host_group_state_transition_table[NUMBER_OF_IGMP_HOST_GROUP_STATES][NUMBER_OF_IGMP_HOST_GROUP_EVENTS];

#endif /* GLOBAL_FILE */

#endif /* _IGMP_STATE_MACHINE_H_ */
