/* igmp_prototypes.h - non-accessible routines from Routerware igmp code */

/* Copyright 1997 - 2001 Wind River Systems, Inc. */

/*
modification history
--------------------
01e,20nov03,niq  Remove copyright_wrs.h file inclusion
01d,04nov03,rlm  Ran batch header path update for header re-org.
01c,03nov03,rlm  Removed wrn/coreip/ prefix from #includes for header re-org.
01b,03nov03,rlm  Relocated from .wind_vxw_h to /vobs/Clarinet_IPv6 vob for
                 header re-org.
01a,29mar01,spm  file creation: copied from version 01b of tor2_0.open_stack
                 branch (wpwr VOB, written by rae) for unified code base;
                 fixed header entries
*/

#ifndef __INCigmp_prototypesh
#define __INCigmp_prototypesh

#include <IGMPv2/igmpR.h>
#include <IGMPv2/igmp_externs.h>

/* igmp_router_receive.c */
void igmp_router_process_received_message (IGMP_MESSAGE *sptr_igmp_message);

/* igmp_router_state.c */
void igmp_process_router_port_state_transition (UINT port, ULONG group_ip_address,enum IGMP_ROUTER_PORT_EVENT event);
void igmp_process_router_group_state_transition (UINT port, ULONG group_ip_address, enum IGMP_ROUTER_GROUP_EVENT event);
void igmp_router_start_general_query_timer (UINT port, ULONG group_ip_address);
void igmp_router_start_other_querier_present_timer (UINT port, ULONG group_ip_address);
void igmp_router_notify_routing_of_group_addition (UINT port, ULONG group_ip_address);
void igmp_router_notify_routing_of_group_deletion (UINT port, ULONG group_ip_address);
void igmp_router_start_group_specific_timer (UINT port, ULONG group_ip_address);
void igmp_router_start_v1_host_timer (UINT port, ULONG group_ip_address);
void igmp_router_start_star_timer (UINT port, ULONG group_ip_address);
void igmp_router_start_retransmit_timer (UINT port, ULONG group_ip_address);
void igmp_router_clear_retransmit_timer (UINT port, ULONG group_ip_address);
void igmp_router_tx_message (UINT port, ULONG group_ip_address);

/* igmp_serialize_message.c */
STATUS igmp_serialize_message (UINT port, IGMP_MESSAGE *sptr_igmp_message);


/* igmp_timer.c */
void igmp_timer (void);

/* igmp_transmit.c */
void igmp_transmit_message (UINT port, ULONG group_ip_address, enum IGMP_MESSAGE_TYPE message_type, int copy_to_router, int copy_to_host);

/* igmp_utilities.c */
char * ipToDot (char * pString, ULONG ipAddr); /* slight mod of inet_ntoa_b */
void igmp_trace (enum IGMP_TRACE_GROUP printf_group, const char *cptr_format, ...);
void igmp_trace_null (enum IGMP_TRACE_GROUP printf_group, const char *cptr_format, ...);
void igmp_display_message (ULONG source_address, ULONG destination_address, enum IGMP_MESSAGE_TYPE igmp_message_type,
						   UINT igmp_message_max_response_time, ULONG igmp_message_group_ip_address);
IGMP_GROUP_CLASS *igmp_add_group_to_port (ULONG group_ip_address, UINT port);
IGMP_GROUP_CLASS *igmp_get_matching_group_node_on_port (ULONG group_ip_address, UINT port);
IGMP_MESSAGE *igmp_create_message (void);
void igmp_free_message (IGMP_MESSAGE *sptr_igmp_message);
void igmp_initialize_timer (IGMP_TIMER *sptr_timer);
void igmp_increment_timer (IGMP_TIMER *sptr_timer);
int igmp_timer_has_expired (IGMP_TIMER *sptr_timer);
void igmp_enable_timer (IGMP_TIMER *sptr_timer, UINT expiry_time);
ULONG igmp_get_ip_address_for_port_from_ip (UINT port);

#endif /* __INCigmp_prototypesh */
