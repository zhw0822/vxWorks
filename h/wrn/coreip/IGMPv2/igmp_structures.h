/* igmp_structures.h - main structures from Routerware igmp code */

/* Copyright 1997 - 2001 Wind River Systems, Inc. */

/*
modification history
--------------------
01e,20nov03,niq  Remove copyright_wrs.h file inclusion
01d,04nov03,rlm  Ran batch header path update for header re-org.
01c,03nov03,rlm  Removed wrn/coreip/ prefix from #includes for header re-org.
01b,03nov03,rlm  Relocated from .wind_vxw_h to /vobs/Clarinet_IPv6 vob for
                 header re-org.
01a,29mar01,spm  file creation: copied from version 01b of tor2_0.open_stack
                 branch (wpwr VOB, written by rae) for unified code base;
                 fixed header entries
*/

#ifndef __INCigmp_structuresh
#define __INCigmp_structuresh

#ifndef NUMBER_OF_IP_PORTS
#define NUMBER_OF_IP_PORTS 32
#endif

#include <m2Lib.h>
#include <IGMPv2/igmp_constants.h>

typedef struct IGMP_MESSAGE
{
	enum IGMP_MESSAGE_TYPE	 type;

	UINT       max_response_time;

	ULONG       group_ip_address;

	ULONG       source_ip_address;

	UINT       receiving_port;

}  IGMP_MESSAGE;

typedef struct IGMP_TIMER
{
	int   enabled;

	UINT      second_counter;

	UINT      expiry_time;

}  IGMP_TIMER;


typedef struct IGMP_ROUTER_GROUP_CLASS
{
	enum IGMP_ROUTER_GROUP_STATE      state;
        
	UINT  group_specific_query_count;
/* same as last member query count */

	IGMP_TIMER     group_specific_timer;
	
	IGMP_TIMER     retransmit_timer;
	
	IGMP_TIMER     v1_host_timer;

}  IGMP_ROUTER_GROUP_CLASS;

typedef struct IGMP_HOST_GROUP_CLASS
{
	enum IGMP_HOST_GROUP_STATE      state;
/* NON_MEMBER | DELAYING_MEMBER | IDLE_MEMBER_STATE */

	int	  this_host_was_last_reporter;

	IGMP_TIMER	  report_delay_timer;

}  IGMP_HOST_GROUP_CLASS;

typedef struct IGMP_GROUP_CLASS
{
	NODE     links;

	int	     enabled;

	UINT       port_number;

	ULONG       multicast_group_ip_address;

	ULONG      ip_address_of_last_reporter;

        ULONG      start_time;
        
	IGMP_ROUTER_GROUP_CLASS	  router_group;

	IGMP_HOST_GROUP_CLASS	  host_group;


}  IGMP_GROUP_CLASS;


typedef struct IGMP_HOST_PORT_CLASS
{
	int   v1_router_present; 

	UINT           v1_router_present_timer;

}  IGMP_HOST_PORT_CLASS;

typedef struct IGMP_ROUTER_PORT_CLASS
{
	enum IGMP_ROUTER_PORT_STATE    state;
/* specifies whether Querier or Non-Querier */

	int     initializing;

        UINT  	number_of_startup_queries_sent;

	UINT  	number_of_startup_queries_to_send;

	UINT    startup_query_interval;

	UINT    general_query_interval;
	
	IGMP_TIMER          general_query_timer;

	UINT                general_query_max_response_time;
	
	UINT                other_querier_present_timeout;

	IGMP_TIMER         other_querier_present_timer;

}  IGMP_ROUTER_PORT_CLASS;

typedef struct IGMP_PORT_CLASS
{
	int      enabled;

	int 	  router_enabled;
	
	int	 host_enabled;

	enum IGMP_VERSION    version;

	UINT       robustness;

	UINT       port_number;

	ULONG       ip_address_of_port;

	ULONG       ip_address_of_querier;

	UINT       max_response_time_from_most_recent_query;

	IGMP_ROUTER_PORT_CLASS	router_port;

	IGMP_HOST_PORT_CLASS  	host_port;

	/* RAE IGMP_MIB_INTERFACE_ENTRY   igmpInterfaceEntry; */
        M2_IGMP  igmpInterfaceEntry;

	LIST      active_group_list;

}  IGMP_PORT_CLASS;

typedef struct IGMP_TRACE_CLASS
{
	int	enabled;

	int   	alarm_enabled;

}  IGMP_TRACE_CLASS;

typedef struct IGMP_CLASS
{
	int	initialization_breakpoint;

	int	enabled;

	UINT       clock_ticks_per_second;

	UINT       number_of_snmp_time_ticks_per_clock_tick;

	UINT       tick_counter;

	ULONG       stack_id;

	UINT       number_of_ports;

	IGMP_TRACE_CLASS	trace;

	IGMP_PORT_CLASS	 port_table[NUMBER_OF_IP_PORTS];

}  IGMP_CLASS;

#endif /* __INCigmp_structuresh */
