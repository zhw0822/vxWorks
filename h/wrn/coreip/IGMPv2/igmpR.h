/* igmpR.h - header file for Routerware igmp code */

/* Copyright 1997 - 2004 Wind River Systems, Inc. */

/*
modification history
--------------------
01g,11aug04,rae  fix VIRTUAL_STACK
01f,20nov03,niq  Remove copyright_wrs.h file inclusion
01e,10nov03,rlm  2nd pass of include update for header re-org.
01d,04nov03,rlm  Ran batch header path update for header re-org.
01c,03nov03,rlm  Removed wrn/coreip/ prefix from #includes for header re-org.
01b,03nov03,rlm  Relocated from .wind_vxw_h to /vobs/Clarinet_IPv6 vob for
                 header re-org.
01a,29mar01,spm  file creation: copied from version 01b of tor2_0.open_stack
                 branch (wpwr VOB, written by rae) for unified code base;
                 fixed header entries
*/

#ifndef __INCigmpRh
#define __INCigmpRh

#include <netinet/in.h>

#include <lstLib.h>
#include <IGMPv2/igmpRouterLib.h>

#include <IGMPv2/igmp_constants.h>
#include <IGMPv2/igmp_structures.h>
#include <IGMPv2/igmp_prototypes.h>
#include <IGMPv2/igmp_globals.h>
#include <IGMPv2/igmp_display_string.h>
#include <IGMPv2/igmp_state_machine_structures.h>
#include <IGMPv2/igmp_state_machine.h>

#ifdef VIRTUAL_STACK
#include <netinet/vsLib.h>
#include <netinet/vsData.h>
#include <netinet/vsIgmpR.h>
#endif

#endif /* __INCigmpRh*/
