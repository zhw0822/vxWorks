/* spdInit.h - header file for spd_init.c */
/************************************************************************/
/* Copyright 2000-2005 Wind River Systems, Inc.                         */
/************************************************************************/

/*
modification history
--------------------
01e,06may05,rlm  Added prototype for usrSpdInit(). Removed legacy K&R
                 prototypes.
01d,30nov04,jfb  Beautified again
01c,29nov04,jfb  Beautified
01b,11jun01,tkp  pompeii enhancements.
01a,19jun00,aos  created.

*/

#if !defined (__SPD_INIT_H__)
#define __SPD_INIT_H__

#ifdef __cplusplus
extern "C"
    {
#endif

#if ((CPU_FAMILY==I960) && (defined __GNUC__))
  #pragma align 1 /* tell gcc960 not to optimize alignments */
#endif /* CPU_FAMILY==I960 */

#if ((CPU_FAMILY==I960) && (defined __GNUC__))
  #pragma align 0 /* turn off alignment requirement */
#endif /* CPU_FAMILY==I960 */

/* function declarations */
extern STATUS spdInit
    (
    char *spdCfg
    );

STATUS usrSpdInit
    (
    void
    );

#ifdef __cplusplus
    }
#endif

#endif /* __SPD_INIT_H__ */
