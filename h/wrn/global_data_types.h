/************************************************************************/
/*	Copyright (C) 1990 RouterWare, Inc.									*/
/*	Unpublished - rights reserved under the Copyright Laws of the		*/
/*	United States.  Use, duplication, or disclosure by the 				*/
/*	Government is subject to restrictions as set forth in 				*/
/*	subparagraph (c)(1)(ii) of the Rights in Technical Data and 		*/
/*	Computer Software clause at 252.227-7013.							*/
/*	RouterWare, Inc., 3961 MacArthur Blvd. Suite 212, Newport Beach, CA	*/
/************************************************************************/
#if !defined (__GLOBAL_DATA_TYPES_H__)
#define __GLOBAL_DATA_TYPES_H__

#define INVALID_HANDLE ((UINT)NULL)
#define MAX_NUMBER_OF_BYTES_IN_MAC_ADDRESS 16

#define WAIT_FOREVER 0xffffffff

typedef UINT LINK_DRIVER_INSTANCE;
typedef UINT LINK_DRIVER_INTERFACE_HANDLE;

typedef UINT ARP_INSTANCE;
typedef UINT ARP_INTERFACE_HANDLE;

typedef UINT IP_INSTANCE;
typedef UINT IP_INTERFACE_HANDLE;

typedef UINT ROUTE_MANAGER_INSTANCE;

typedef UINT UDP_INSTANCE;

typedef UINT ECHO_AGENT_INSTANCE;

#endif /* __GLOBAL_DATA_TYPES_H__ */
