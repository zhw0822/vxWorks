/************************************************************************/
/*	Copyright (C) 1999 RouterWare, Inc.     							*/
/*	Unpublished - rights reserved under the Copyright Laws of the		*/
/*	United States.  Use, duplication, or disclosure by the 				*/
/*	Government is subject to restrictions as set forth in 				*/
/*	subparagraph (c)(1)(ii) of the Rights in Technical Data and 		*/
/*	Computer Software clause at 252.227-7013.							*/
/*	RouterWare, Inc., 3961 MacArthur Blvd. Suite 212, Newport Beach, CA	*/
/************************************************************************/
#if !defined (_RWOS_H_)
#define _RWOS_H_

#if defined (WIN32)
#define RW_EXPORT __declspec(dllexport)
#else
#define RW_EXPORT
#endif
#define RWOS_VERSION     "1.0"
#define RWOS_REV		 "/1" /* 0 for first revision */
#define RWOS_BUILD		 __DATE__

#define MINIMUM_UINT(a,b) ((a)<(b)?(a):(b))

#include <vxWorks.h>	/* VxWorks standard typedefs */

#if defined (__VXWORKS__)
	#if !defined (WIN32)
		#define _pack __attribute__ ((packed))	/* gnu cc pack attribute */
	#endif
#else
	#define _pack 
#endif

#if! defined bool
	typedef enum bool
	{
		true = 1,
		false = 0
	} bool;
#endif /* bool */
#include <rw_util.h>
#include <rwcontainer.h>
#include <rwdispatcher.h>
#include <rwmemory.h>
#include <rwpacket.h>
#include <rwbufferpool.h>
#include <rw_host_os_interface.h>
#include <system.h>
#include <rw_task_functions.h>
#include <module_prototype.h>
#include <rwproto.h>
#ifndef __SNMP_FWD_DECL__
#include <rwutils.h>
#endif
#include <v8022str.h>
#endif /*_RWOS_H_*/

