/************************************************************************/
/*	Copyright (C) 1999 RouterWare, Inc.     							*/
/*	Unpublished - rights reserved under the Copyright Laws of the		*/
/*	United States.  Use, duplication, or disclosure by the 				*/
/*	Government is subject to restrictions as set forth in 				*/
/*	subparagraph (c)(1)(ii) of the Rights in Technical Data and 		*/
/*	Computer Software clause at 252.227-7013.							*/
/*	RouterWare, Inc., 3961 MacArthur Blvd. Suite 212, Newport Beach, CA	*/
/************************************************************************/
#if !defined (__IP_STRUCTURES_H__)
#define __IP_STRUCTURES_H__

typedef struct IP_LOOPBACK_QUEUE
{
	RW_CONTAINER* p_packets;
	RWOS_DISPATCHER dispatcher;
	RWOS_EVENT event;
} IP_LOOKBACK_QUEUE;

typedef struct IP_CLASS
{
	IP_UPPER_IF				parent;
	RW_CONTAINER*			p_interface_list;
	RW_CONTAINER*			p_transport_list;
	RW_CONTAINER*			p_reassembly_list;
	IP_LOOKBACK_QUEUE		loopback;
	IP_LOWER_IF				parent_ip_lower_if;
	IP_LOWER_CALLBACK_IF* p_ip_lower_callback_if;
} IP_CLASS;

bool ip_register_upper_layer_protocol (IP_UPPER_IF* p_ip_instance, IP_TRANSPORT_PROTOCOL transport_protocol_id, 
	IP_UPPER_CALLBACK_IF* p_transport_call_back_interface);

bool ip_deregister_upper_layer_protocol (IP_UPPER_IF* p_ip_instance, IP_UPPER_CALLBACK_IF* p_transport_instance);

bool ip_send_packet (IP_UPPER_IF* p_ip_instance, IP_IF_HANDLE ip_interface, IP_MESSAGE* p_ip_message, 
	IP_ADDRESS next_hop_ip_address);

UINT ip_upper_if_get_mtu (struct IP_UPPER_IF* p_ip_upper_if, IP_IF_HANDLE);

IP_ADDRESS ip_upper_if_get_first_ip_interface_address (IP_UPPER_IF* p_ip_upper_if, IP_IF_HANDLE ip_if_handle);

/************************************************************************/
typedef struct IP_INTERFACE_CLASS
{
	IP_ADDRESS address;

	IP_ADDRESS subnet_mask;

	IP_CLASS* p_ip;

	/*LINK_DRIVER_INTERFACE_HANDLE link_driver_interface;*/
} IP_INTERFACE_CLASS;

typedef struct IP_TRANSPORT_CLASS
{
	IP_UPPER_CALLBACK_IF								parent;

	IP_UPPER_CALLBACK_IF* 								p_transport_instance;

	IP_TRANSPORT_PROTOCOL								protocol;
} IP_TRANSPORT_CLASS;

typedef struct IP_REASSEMBLY_BLOCK
{
	IP_TYPE_OF_SERVICE type_of_service;
	UINT datagram_identifier;
	bool dont_fragment_flag;
	bool more_fragment_flag;
	UINT fragment_offset;
	UINT time_to_live;
	IP_TRANSPORT_PROTOCOL transport_protocol;	
	UINT source_address;
	UINT destination_address;

	RWOS_TIME_STAMP time_stamp;

	RW_CONTAINER* p_fragments;
} IP_REASSEMBLY_BLOCK;

typedef struct IP_REASSEMBLY_FRAGMENT
{
	UINT fragment_offset;

	RW_PACKET_HANDLE payload;
} IP_REASSEMBLY_FRAGMENT;

typedef struct IP_LOOPBACK_PACKET
{
	IP_INTERFACE_CLASS* p_ip_interface;

	IP_MESSAGE* p_ip_message;
} IP_LOOPBACK_PACKET;

#endif /* __IP_STRUCTURES_H__ */
