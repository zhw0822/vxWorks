/************************************************************************/
/*	Copyright (C) 1999 RouterWare, Inc.	 								*/
/*	Unpublished - rights reserved under the Copyright Laws of the		*/
/*	United States.  Use, duplication, or disclosure by the 				*/
/*	Government is subject to restrictions as set forth in 				*/
/*	subparagraph (c)(1)(ii) of the Rights in Technical Data and 		*/
/*	Computer Software clause at 252.227-7013.							*/
/*	RouterWare, Inc., 3961 MacArthur Blvd. Suite 212, Newport Beach, CA	*/
/* ************************************************************************************************************************ */
#if !defined (_RWOS_DRIVER_MANAGER_IF_H_)
#define _RWOS_DRIVER_MANAGER_IF_H_
/* ************************************************************************************************************************ */
/*typedef UINT DRIVER_MANAGER_HANDLE;*/
/* ************************************************************************************************************************ */


typedef struct RWOS_DRIVER_MANAGER_CALLBACK_IF
{
	/*		Lower layer callback methods									*/
	/*		return :																*/
	/*		true =  Continue processing receive indicate events.	*/
	/*		false = All receive indicate events are process.		*/
	bool					(*fp_process_driver_manager_receive_indicate_event) (
								struct RWOS_DRIVER_MANAGER_CALLBACK_IF* p_rwos_driver_manager_callback_if);

}RWOS_DRIVER_MANAGER_CALLBACK_IF;

/* ************************************************************************************************************************ */
/* Rwos driver interface */
bool rwos_register_driver_manager (DRIVER_MANAGER_HANDLE driver_manager_instance, 
	struct RWOS_DRIVER_MANAGER_CALLBACK_IF *p_driver_callback_if ); 
bool rwos_deregister_driver_manager (DRIVER_MANAGER_HANDLE driver_manager_instance, 
	struct RWOS_DRIVER_MANAGER_CALLBACK_IF *p_driver_callback_if);
/* ************************************************************************************************************************ */
/*		Rwos driver event methods											*/
/*		return :																	*/
/*		true = Rwos receive indicate event was set.					*/
/*		false = Rwos failed to set the receive indicate event		*/
bool rwos_signal_driver_manager_receive_indicate_event (RWOS_DRIVER_MANAGER_CALLBACK_IF*);
/* ************************************************************************************************************************ */
#endif /*_RWOS_LOWER_LAYER_IF_H_*/
