/* taskWaitShow.h - header file for taskWaitShow routines */

/* Copyright 2004 Wind River Systems, Inc. */

/*
modification history
--------------------
01a,07sep04,bpn  Written.
*/

#ifndef __INCtaskWaitShowh
#define __INCtaskWaitShowh

#ifdef __cplusplus
extern "C" {
#endif

extern STATUS taskWaitShow (int taskId, int level);

#ifdef __cplusplus
}
#endif

#endif /* __INCtaskWaitShowh */
