/* toolMacros.h - compile time macros for Diab C compiler */

/*
 * Copyright (c) 2001-2005 Wind River Systems, Inc.
 *
 * The right to copy, distribute or otherwise make use of this software
 * may be licensed only pursuant to the terms of an applicable Wind River
 * license agreement. No license to Wind River intellectual property rights
 * is granted herein. All rights not licensed by Wind River are reserved
 * by Wind River.
 */

/*
modification history
--------------------
01l,09sep05,jln  added _WRS_ABSOLUTE_EXTERN
01k,08aug05,wap  Add section relocation macros
01j,17mar04,dat  remove leading underscore from _absSymbols_xxx (95052)
01i,05mar04,dat  temp fix for c++ and deprecated
01h,05feb04,dat  add macros for _WRS_ABSOLUTE
01g,26nov03,dat  Adding INLINE and DEPRECATED
01f,12oct03,dat  new macros for V7 of portable C guide.
01e,23sep03,zl   added _WRS_CONSTRUCTOR definition
01d,26nov01,dat  New macros for alignment checking and unaligned copies
01c,14nov01,dat  Adding underscores to macro names
01b,18oct01,jab  fixed WRS_ASM macro
01a,04sep01,dat  written (assumes Diab 4.4b or later)
*/

#ifndef __INCtoolMacrosh
#define __INCtoolMacrosh

#define _WRS_PACK_ALIGN(x) __attribute__((packed, aligned(x)))

#define _WRS_ASM(x) __asm volatile (x)

#define _WRS_DATA_ALIGN_BYTES(x) __attribute__((aligned(x)))

/* This tool uses the ANSI variadic macro style */

#undef  _WRS_GNU_VAR_MACROS	/* Diab uses ANSI syntax */

/* New macros for alignment issues */

#define _WRS_ALIGNOF(x) 	sizeof(x,1)

#define _WRS_UNALIGNED_COPY(pSrc, pDest, size) \
	(memcpy ((pDest), (pSrc), (size)))

#define _WRS_ALIGN_CHECK(ptr, type) \
	(((int)(ptr) & ( _WRS_ALIGNOF(type) - 1)) == 0 ? TRUE : FALSE)

/*
 * Macros to force code or data into specific sections. These
 * can be used to force the compiler to place a given piece of
 * code or data into a separate section. The idea is to improve
 * cache usage by either clustering frequently referenced code
 * close together, or moving infrequenly used code (i.e. "run-once"
 * init routines that are only used during bootstrap) out of the
 * way. The "init" sections are used for the latter case, while
 * the "fast" sections are used to group performance-critical
 * routines together.
 */

#ifndef _WRS_NO_SPECIAL_SECTIONS
#define _WRS_INITTEXT	__attribute__ ((__section__ (".text.init")))
#define _WRS_FASTTEXT	__attribute__ ((__section__ (".text.fast")))
#define _WRS_INITDATA	__attribute__ ((__section__ (".data.init")))
#define _WRS_FASTDATA	__attribute__ ((__section__ (".data.fast")))
#else
#define _WRS_INITTEXT
#define _WRS_FASTTEXT
#define _WRS_INITDATA
#define _WRS_FASTDATA
#endif

/*
 * macro for static initializer (constructor) definitions; assumes
 * Diab 5.2 or later with constructor support.
 */

#define _WRS_CONSTRUCTOR(rtn,lvl) \
	__attribute__((constructor(lvl))) void _STI__##lvl##__##rtn (void)

/*
 * New Basix macros, (Portable Coding Guide, v7)
 *
 * HAS_GCC_ASM_SYNTAX if Gnu inline assembly syntax is supported.
 * HAS_DCC_ASM_SYNTAX if Diab inline assembly function syntax is supported.
 * INLINE for static inline C functions, (prefix)
 * DEPRECATED for obsolete API warnings. (postfix)
 */


#undef  _WRS_HAS_GCC_ASM_SYNTAX
#define _WRS_HAS_DCC_ASM_SYNTAX
#define _WRS_INLINE		static __inline__

#ifdef __cplusplus
#define _WRS_DEPRECATED(x)	/* temp workaround for c++ compiler failure */
#else
#define _WRS_DEPRECATED(x) 	__attribute__((deprecated(x)))
#endif

/* (New V7) For absolute symbol support. (use with semicolon) */

#define _WRS_ABSOLUTE(name,value) \
	const int name __attribute__((absolute)) = value

#define _WRS_ABSOLUTE_EXTERN(name) \
	extern const char name[]

/* prefix and suffix to surround absolute symbol declarations (nops for diab)*/

#define _WRS_ABSOLUTE_BEGIN(x) STATUS absSymbols_##x (void) {return OK;}
#define _WRS_ABSOLUTE_END 


/*
 * For backward compatibility with v5 of portable C coding guide.
 *
 * These are now obsolete, please don't use them.
 */

#define WRS_PACK_ALIGN(x)	_WRS_PACK_ALIGN(x)
#define WRS_ASM(x) 		_WRS_ASM(x)
#define WRS_DATA_ALIGN_BYTES(x)	_WRS_DATA_ALIGN_BYTES(x)
#undef WRS_GNU_VAR_MACROS

#endif /* __INCtoolMacrosh */
