# defs.common  -  arch independent definitions shared by all toolchains
#
# modification history
# --------------------
# 01f,05jul04,fle  Remove VMA_START value, it is defined in defs.memory
# 01e,05apr04,mcm  Fix for SPR 95829 - defining a new macro LD_LINK_TOOL_DIR
#		   to help set the path for Diab C++ libraries.
# 01d,22jan04,zmm  Move definitions of  ROM_ENTRY, SYS_ENTRY and USR_ENTRY
#                  from defs.diab and defs.gnu here. Use HLL_PREFIX so that
#                  prepended underscores could be redefined in
#                  architecture/toolchain fragments.
# 01c,09jan04,c_c  Removed automatic version search (SPR #92774).
# 01b,08dec03,tpw  Eliminate use of --ignore-vma from .hex builds.
# 01b,14nov03,c_c  Added compiler root path definition.
# 01a,29oct01,tpw  created.
#
# DESCRIPTION
# This file contains architecture independent definitions and flags to be
# shared by all toolchains.
#

# compute the path for compiler. This path is dependent of the toolchain
# version, which is located under WIND_BASE/host/TOOL_FAMILY/version.

CC_ROOT = $(WIND_BASE)/host/$(TOOL_FAMILY)

# Determine CC_BASE (can be overriden from the command line or later)
CC_BASE = $(CC_ROOT)/$(CC_VERSION)

# Determine CC_BIN_DIR
CC_BIN_DIR = $(CC_BASE)/$(CC_HOST)/bin

# Toolchain specific CPU variant independent library directory
LD_LINK_TOOL_DIR = $(TGT_DIR)/lib/$(VX_CPU_FAMILY)/$(CPU)/$(TOOL)

## macros for file conversion and rom image builds

EXTRACT_BIN_FLAG= -O binary --binary-without-bss
EXTRACT_HEX_FLAG= -O srec --gap-fill=0
EXTRACT_SYM_FLAG= --extract-symbol

START_FLAGS     =
HEX_FLAGS	=

NO_VMA_FLAGS	= -I srec -O binary
VMA_FLAGS	= -I binary -O srec --adjust-vma=$(VMA_START)

## bsp flags

HLL_PREFIX              = _

ROM_ENTRY               = $(HLL_PREFIX)romInit
SYS_ENTRY               = $(HLL_PREFIX)sysInit
USR_ENTRY               = $(HLL_PREFIX)usrInit

# linker scripts data alignment

LD_DATA_SEG_NOALIGN	= $(strip $(LD_DATA_SEG_ALIGN_FLAG))=1

# VMA adjust macros

KERNEL_VMA_ADJUST	= @ $(NOP)

## obsolete macros

EXTRACT_BIN_NAME= false Used obsolete EXTRACT_BIN_NAME macro
BINXSYM_NAME	= false Used obsolete BINXSYM_NAME macro
BINHEX_NAME	= false Used obsolete BINHEX_NAME macro
