
/* THIS FILE IS AUTO_GENERATED. PLEASE DO NOT EDIT. */

extern int _exitSc(void *);
extern int creatSc(void *);
extern int _openSc(void *);
extern int closeSc(void *);
extern int readSc(void *);
extern int writeSc(void *);
extern int _ioctlSc(void *);
extern int dupSc(void *);
extern int dup2Sc(void *);
extern int pipeSc(void *);
extern int removeSc(void *);
extern int selectSc(void *);
extern int socketSc(void *);
extern int bindSc(void *);
extern int listenSc(void *);
extern int acceptSc(void *);
extern int connectSc(void *);
extern int sendtoSc(void *);
extern int sendSc(void *);
extern int sendmsgSc(void *);
extern int recvfromSc(void *);
extern int recvSc(void *);
extern int recvmsgSc(void *);
extern int setsockoptSc(void *);
extern int getsockoptSc(void *);
extern int getsocknameSc(void *);
extern int getpeernameSc(void *);
extern int shutdownSc(void *);
extern int mmapSc(void *);
extern int munmapSc(void *);
extern int mprotectSc(void *);
extern int killSc(void *);
extern int pauseSc(void *);
extern int sigpendingSc(void *);
extern int sigprocmaskSc(void *);
extern int _sigqueueSc(void *);
extern int sigsuspendSc(void *);
extern int sigtimedwaitSc(void *);
extern int _sigactionSc(void *);
extern int _sigreturnSc(void *);
extern int chdirSc(void *);
extern int _getcwdSc(void *);
extern int getpidSc(void *);
extern int getppidSc(void *);
extern int waitpidSc(void *);
extern int sysctlSc(void *);
extern int _schedPxInfoGetSc(void *);
extern int sigaltstackSc(void *);
extern int unlinkSc(void *);
extern int linkSc(void *);
extern int fsyncSc(void *);
extern int fdatasyncSc(void *);
extern int renameSc(void *);
extern int fpathconfSc(void *);
extern int pathconfSc(void *);
extern int accessSc(void *);
extern int chmodSc(void *);
extern int eventReceiveSc(void *);
extern int eventSendSc(void *);
extern int eventCtlSc(void *);
extern int msgQSendSc(void *);
extern int msgQReceiveSc(void *);
extern int _msgQOpenSc(void *);
extern int objDeleteSc(void *);
extern int objInfoGetSc(void *);
extern int _semTakeSc(void *);
extern int _semGiveSc(void *);
extern int _semOpenSc(void *);
extern int semCtlSc(void *);
extern int _taskOpenSc(void *);
extern int taskCtlSc(void *);
extern int taskDelaySc(void *);
extern int rtpSpawnSc(void *);
extern int rtpInfoGetSc(void *);
extern int taskKillSc(void *);
extern int _taskSigqueueSc(void *);
extern int _timer_openSc(void *);
extern int timerCtlSc(void *);
extern int pxOpenSc(void *);
extern int pxCloseSc(void *);
extern int pxUnlinkSc(void *);
extern int pxCtlSc(void *);
extern int pxMqReceiveSc(void *);
extern int pxMqSendSc(void *);
extern int pxSemWaitSc(void *);
extern int pxSemPostSc(void *);
extern int pipeDevCreateSc(void *);
extern int pipeDevDeleteSc(void *);
extern int _sdCreateSc(void *);
extern int _sdOpenSc(void *);
extern int sdDeleteSc(void *);
extern int sdMapSc(void *);
extern int sdUnmapSc(void *);
extern int sdProtectSc(void *);
extern int _edrErrorInjectSc(void *);
extern int edrFlagsGetSc(void *);
extern int resSearchNSc(void *);
extern int wvEventSc(void *);
extern int rtpVarAddSc(void *);
extern int sdInfoGetSc(void *);
extern int _shlOpenSc(void *);
extern int _shlUnlockSc(void *);
extern int _shlCloseSc(void *);
extern int _shlGetSc(void *);
extern int _shlPutSc(void *);
extern int objUnlinkSc(void *);
extern int getprlimitSc(void *);
extern int setprlimitSc(void *);
extern int _mctlSc(void *);

_WRS_DATA_ALIGN_BYTES(16) SYSCALL_RTN_TBL_ENTRY STANDARDRtnTbl [] = {
/*0*/	{ _exitSc, 1, "_exit", 0},
/*1*/	{ creatSc, 2, "creat", 0},
/*2*/	{ _openSc, 3, "_open", 0},
/*3*/	{ closeSc, 1, "close", 0},
/*4*/	{ readSc, 3, "read", 0},
/*5*/	{ writeSc, 3, "write", 0},
/*6*/	{ _ioctlSc, 3, "_ioctl", 0},
/*7*/	{ dupSc, 1, "dup", 0},
/*8*/	{ dup2Sc, 2, "dup2", 0},
/*9*/	{ pipeSc, 1, "pipe", 0},
/*10*/	{ removeSc, 1, "remove", 0},
/*11*/	{ selectSc, 5, "select", 0},
#if defined(INCLUDE_SC_SOCKLIB)
/*12*/	{ socketSc, 3, "socket", 0},
#else
	{ NULL, 0, NULL, 0},
#endif
#if defined(INCLUDE_SC_SOCKLIB)
/*13*/	{ bindSc, 3, "bind", 0},
#else
	{ NULL, 0, NULL, 0},
#endif
#if defined(INCLUDE_SC_SOCKLIB)
/*14*/	{ listenSc, 2, "listen", 0},
#else
	{ NULL, 0, NULL, 0},
#endif
#if defined(INCLUDE_SC_SOCKLIB)
/*15*/	{ acceptSc, 3, "accept", 0},
#else
	{ NULL, 0, NULL, 0},
#endif
#if defined(INCLUDE_SC_SOCKLIB)
/*16*/	{ connectSc, 3, "connect", 0},
#else
	{ NULL, 0, NULL, 0},
#endif
#if defined(INCLUDE_SC_SOCKLIB)
/*17*/	{ sendtoSc, 6, "sendto", 0},
#else
	{ NULL, 0, NULL, 0},
#endif
#if defined(INCLUDE_SC_SOCKLIB)
/*18*/	{ sendSc, 4, "send", 0},
#else
	{ NULL, 0, NULL, 0},
#endif
#if defined(INCLUDE_SC_SOCKLIB)
/*19*/	{ sendmsgSc, 3, "sendmsg", 0},
#else
	{ NULL, 0, NULL, 0},
#endif
#if defined(INCLUDE_SC_SOCKLIB)
/*20*/	{ recvfromSc, 6, "recvfrom", 0},
#else
	{ NULL, 0, NULL, 0},
#endif
#if defined(INCLUDE_SC_SOCKLIB)
/*21*/	{ recvSc, 4, "recv", 0},
#else
	{ NULL, 0, NULL, 0},
#endif
#if defined(INCLUDE_SC_SOCKLIB)
/*22*/	{ recvmsgSc, 3, "recvmsg", 0},
#else
	{ NULL, 0, NULL, 0},
#endif
#if defined(INCLUDE_SC_SOCKLIB)
/*23*/	{ setsockoptSc, 5, "setsockopt", 0},
#else
	{ NULL, 0, NULL, 0},
#endif
#if defined(INCLUDE_SC_SOCKLIB)
/*24*/	{ getsockoptSc, 5, "getsockopt", 0},
#else
	{ NULL, 0, NULL, 0},
#endif
#if defined(INCLUDE_SC_SOCKLIB)
/*25*/	{ getsocknameSc, 3, "getsockname", 0},
#else
	{ NULL, 0, NULL, 0},
#endif
#if defined(INCLUDE_SC_SOCKLIB)
/*26*/	{ getpeernameSc, 3, "getpeername", 0},
#else
	{ NULL, 0, NULL, 0},
#endif
#if defined(INCLUDE_SC_SOCKLIB)
/*27*/	{ shutdownSc, 2, "shutdown", 0},
#else
	{ NULL, 0, NULL, 0},
#endif
/*28*/	{ mmapSc, 8, "mmap", 0},
/*29*/	{ munmapSc, 2, "munmap", 0},
/*30*/	{ mprotectSc, 3, "mprotect", 0},
/*31*/	{ killSc, 2, "kill", 0},
/*32*/	{ pauseSc, 0, "pause", 0},
/*33*/	{ sigpendingSc, 1, "sigpending", 0},
/*34*/	{ sigprocmaskSc, 3, "sigprocmask", 0},
/*35*/	{ _sigqueueSc, 4, "_sigqueue", 0},
/*36*/	{ sigsuspendSc, 1, "sigsuspend", 0},
/*37*/	{ sigtimedwaitSc, 3, "sigtimedwait", 0},
/*38*/	{ _sigactionSc, 4, "_sigaction", 0},
/*39*/	{ _sigreturnSc, 0, "_sigreturn", 0},
/*40*/	{ chdirSc, 1, "chdir", 0},
/*41*/	{ _getcwdSc, 2, "_getcwd", 0},
/*42*/	{ NULL, },
/*43*/	{ getpidSc, 0, "getpid", 0},
/*44*/	{ getppidSc, 0, "getppid", 0},
/*45*/	{ waitpidSc, 3, "waitpid", 0},
#if defined(INCLUDE_SC_SYSCTL)
/*46*/	{ sysctlSc, 6, "sysctl", 0},
#else
	{ NULL, 0, NULL, 0},
#endif
#if defined(INCLUDE_POSIX_SCHED)
/*47*/	{ _schedPxInfoGetSc, 2, "_schedPxInfoGet", 0},
#else
	{ NULL, 0, NULL, 0},
#endif
/*48*/	{ sigaltstackSc, 2, "sigaltstack", 0},
/*49*/	{ unlinkSc, 1, "unlink", 0},
/*50*/	{ linkSc, 2, "link", 0},
/*51*/	{ fsyncSc, 1, "fsync", 0},
/*52*/	{ fdatasyncSc, 1, "fdatasync", 0},
/*53*/	{ renameSc, 2, "rename", 0},
/*54*/	{ fpathconfSc, 2, "fpathconf", 0},
/*55*/	{ pathconfSc, 2, "pathconf", 0},
/*56*/	{ accessSc, 2, "access", 0},
/*57*/	{ chmodSc, 2, "chmod", 0},
};
#define STANDARDRtnTblNum (sizeof(STANDARDRtnTbl)/sizeof(STANDARDRtnTbl[0]))

_WRS_DATA_ALIGN_BYTES(16) SYSCALL_RTN_TBL_ENTRY VXWORKSRtnTbl [] = {
/*0*/	{ eventReceiveSc, 4, "eventReceive", 0},
/*1*/	{ eventSendSc, 2, "eventSend", 0},
/*2*/	{ eventCtlSc, 4, "eventCtl", 0},
#if defined(INCLUDE_MSG_Q)
/*3*/	{ msgQSendSc, 5, "msgQSend", 0},
#else
	{ NULL, 0, NULL, 0},
#endif
#if defined(INCLUDE_MSG_Q)
/*4*/	{ msgQReceiveSc, 4, "msgQReceive", 0},
#else
	{ NULL, 0, NULL, 0},
#endif
#if defined(INCLUDE_MSG_Q)
/*5*/	{ _msgQOpenSc, 6, "_msgQOpen", 0},
#else
	{ NULL, 0, NULL, 0},
#endif
/*6*/	{ objDeleteSc, 2, "objDelete", 0},
/*7*/	{ objInfoGetSc, 4, "objInfoGet", 0},
/*8*/	{ _semTakeSc, 2, "_semTake", 0},
/*9*/	{ _semGiveSc, 1, "_semGive", 0},
/*10*/	{ _semOpenSc, 6, "_semOpen", 0},
/*11*/	{ semCtlSc, 4, "semCtl", 0},
/*12*/	{ _taskOpenSc, 1, "_taskOpen", 0},
/*13*/	{ taskCtlSc, 4, "taskCtl", 0},
/*14*/	{ taskDelaySc, 1, "taskDelay", 0},
/*15*/	{ rtpSpawnSc, 7, "rtpSpawn", 0},
/*16*/	{ rtpInfoGetSc, 2, "rtpInfoGet", 0},
/*17*/	{ taskKillSc, 2, "taskKill", 0},
/*18*/	{ _taskSigqueueSc, 4, "_taskSigqueue", 0},
#if defined(INCLUDE_POSIX_TIMERS)
/*19*/	{ _timer_openSc, 5, "_timer_open", 0},
#else
	{ NULL, 0, NULL, 0},
#endif
#if defined(INCLUDE_POSIX_TIMERS)
/*20*/	{ timerCtlSc, 4, "timerCtl", 0},
#else
	{ NULL, 0, NULL, 0},
#endif
#if defined(INCLUDE_POSIX_SEM) || defined(INCLUDE_POSIX_MQ)
/*21*/	{ pxOpenSc, 4, "pxOpen", 0},
#else
	{ NULL, 0, NULL, 0},
#endif
#if defined(INCLUDE_POSIX_SEM) || defined(INCLUDE_POSIX_MQ)
/*22*/	{ pxCloseSc, 1, "pxClose", 0},
#else
	{ NULL, 0, NULL, 0},
#endif
#if defined(INCLUDE_POSIX_SEM) || defined(INCLUDE_POSIX_MQ)
/*23*/	{ pxUnlinkSc, 2, "pxUnlink", 0},
#else
	{ NULL, 0, NULL, 0},
#endif
#if defined(INCLUDE_POSIX_SEM) || defined(INCLUDE_POSIX_MQ)
/*24*/	{ pxCtlSc, 4, "pxCtl", 0},
#else
	{ NULL, 0, NULL, 0},
#endif
#if defined(INCLUDE_POSIX_MQ)
/*25*/	{ pxMqReceiveSc, 6, "pxMqReceive", 0},
#else
	{ NULL, 0, NULL, 0},
#endif
#if defined(INCLUDE_POSIX_MQ)
/*26*/	{ pxMqSendSc, 6, "pxMqSend", 0},
#else
	{ NULL, 0, NULL, 0},
#endif
#if defined(INCLUDE_POSIX_SEM)
/*27*/	{ pxSemWaitSc, 3, "pxSemWait", 0},
#else
	{ NULL, 0, NULL, 0},
#endif
#if defined(INCLUDE_POSIX_SEM)
/*28*/	{ pxSemPostSc, 1, "pxSemPost", 0},
#else
	{ NULL, 0, NULL, 0},
#endif
#if defined(INCLUDE_PIPES)
/*29*/	{ pipeDevCreateSc, 3, "pipeDevCreate", 0},
#else
	{ NULL, 0, NULL, 0},
#endif
#if defined(INCLUDE_PIPES)
/*30*/	{ pipeDevDeleteSc, 2, "pipeDevDelete", 0},
#else
	{ NULL, 0, NULL, 0},
#endif
#if defined(INCLUDE_SHARED_DATA)
/*31*/	{ _sdCreateSc, 8, "_sdCreate", 0},
#else
	{ NULL, 0, NULL, 0},
#endif
#if defined(INCLUDE_SHARED_DATA)
/*32*/	{ _sdOpenSc, 8, "_sdOpen", 0},
#else
	{ NULL, 0, NULL, 0},
#endif
#if defined(INCLUDE_SHARED_DATA)
/*33*/	{ sdDeleteSc, 2, "sdDelete", 0},
#else
	{ NULL, 0, NULL, 0},
#endif
#if defined(INCLUDE_SHARED_DATA)
/*34*/	{ sdMapSc, 3, "sdMap", 0},
#else
	{ NULL, 0, NULL, 0},
#endif
#if defined(INCLUDE_SHARED_DATA)
/*35*/	{ sdUnmapSc, 2, "sdUnmap", 0},
#else
	{ NULL, 0, NULL, 0},
#endif
#if defined(INCLUDE_SHARED_DATA)
/*36*/	{ sdProtectSc, 2, "sdProtect", 0},
#else
	{ NULL, 0, NULL, 0},
#endif
/*37*/	{ _edrErrorInjectSc, 6, "_edrErrorInject", 0},
/*38*/	{ edrFlagsGetSc, 0, "edrFlagsGet", 0},
#if defined(INCLUDE_DNS_RESOLVER)
/*39*/	{ resSearchNSc, 2, "resSearchN", 0},
#else
	{ NULL, 0, NULL, 0},
#endif
#if defined(INCLUDE_WINDVIEW)
/*40*/	{ wvEventSc, 3, "wvEvent", 0},
#else
	{ NULL, 0, NULL, 0},
#endif
/*41*/	{ rtpVarAddSc, 2, "rtpVarAdd", 0},
#if defined(INCLUDE_SHARED_DATA)
/*42*/	{ sdInfoGetSc, 2, "sdInfoGet", 0},
#else
	{ NULL, 0, NULL, 0},
#endif
#if defined(INCLUDE_SHL)
/*43*/	{ _shlOpenSc, 2, "_shlOpen", 0},
#else
	{ NULL, 0, NULL, 0},
#endif
#if defined(INCLUDE_SHL)
/*44*/	{ _shlUnlockSc, 1, "_shlUnlock", 0},
#else
	{ NULL, 0, NULL, 0},
#endif
#if defined(INCLUDE_SHL)
/*45*/	{ _shlCloseSc, 1, "_shlClose", 0},
#else
	{ NULL, 0, NULL, 0},
#endif
#if defined(INCLUDE_SHL)
/*46*/	{ _shlGetSc, 2, "_shlGet", 0},
#else
	{ NULL, 0, NULL, 0},
#endif
#if defined(INCLUDE_SHL)
/*47*/	{ _shlPutSc, 2, "_shlPut", 0},
#else
	{ NULL, 0, NULL, 0},
#endif
/*48*/	{ objUnlinkSc, 2, "objUnlink", 0},
/*49*/	{ getprlimitSc, 4, "getprlimit", 0},
/*50*/	{ setprlimitSc, 4, "setprlimit", 0},
/*51*/	{ _mctlSc, 4, "_mctl", 0},
};
#define VXWORKSRtnTblNum (sizeof(VXWORKSRtnTbl)/sizeof(VXWORKSRtnTbl[0]))

SYSCALL_GROUP_ENTRY syscallGroupTbl [SYSCALL_GROUPS_MAX] = {
	{ NULL, 0},
	{ NULL, 0},
	{ NULL, 0},
	{ NULL, 0},
	{ NULL, 0},
	{ NULL, 0},
	{ NULL, 0},
	{ NULL, 0},
	{ STANDARDRtnTbl, STANDARDRtnTblNum, },
	{ VXWORKSRtnTbl, VXWORKSRtnTblNum, },
	{ NULL, 0},
	{ NULL, 0},
};

char * syscallGroupName [SYSCALL_GROUPS_MAX] = {
	 NULL,
	 NULL,
	 NULL,
	 NULL,
	 NULL,
	 NULL,
	 NULL,
	 NULL,
	"STANDARDGroup",
	"VXWORKSGroup",
	 NULL,
	 NULL,
};

