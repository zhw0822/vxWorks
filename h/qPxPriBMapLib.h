/* qPxPriBMapLib.h - POSIX bit mapped linked list library header file */

/*
 * Copyright (c) 2005 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01a, 07jul05,jln  create 
*/

#ifndef __INCqPxPriBMapLibh
#define __INCqPxPriBMapLibh

#ifdef __cplusplus
extern "C" {
#endif

#include <vxWorks.h>
#include <qClass.h>
#include <vwModNum.h>
#include <qPriNode.h>
#include <dllLib.h>

#include <qPriBMapLib.h>

#ifdef __cplusplus
}
#endif

#endif /* __INCqPxPriBMapLibh */
