/* sdLib.h - shared data library header file */

/* Copyright 2003-2004 Wind River Systems, Inc. */

/*
modification history
--------------------
01i,25oct04,gls  made pVirtAddress a void **
01h,11oct04,gls  added mode parameter to sdOpen (SPR #101348)
01g,02oct04,gls  moved sdLibInit() and sdShowInit() declarations to sdLibP.h
01f,29sep04,gls  made physicalAddr parameter type off_t
01e,20jul04,gls  added hookTblSize parameter to sdLibInit()
01d,01apr04,gls  added sdLibInit() and sdShowInit()
01c,17feb04,gls  fixed copyright
01b,11dec03,gls  added functionality
01a,04nov03,kk   written from AE 1.1 version
*/

#ifndef __INCsdLibh
#define __INCsdLibh

#ifdef __cplusplus
extern "C" {
#endif

#include "vxWorks.h"
#include "vwModNum.h"
#include "sdLibCommon.h"

/* functional API to the library */

SD_ID		sdCreate	(char *		name,
				 int		options,
				 UINT32		size,
				 off_t64	physAddress,
				 MMU_ATTR	attr,
				 void ** 	pVirtAddress);
				 
SD_ID		sdOpen  	(char *		name,
				 int		options,
				 int		mode,
				 UINT32		size,
				 off_t64	physAddress,
				 MMU_ATTR	attr,
				 void ** 	pVirtAddress);

#ifdef __cplusplus
}
#endif	/* __cplusplus */

#endif /* __INCsdLibh */
