/* syscallLib.h - System Call Infrastructure library header */

/* Copyright 1984-2003 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*
modification history
--------------------
01f,12nov03,yvp  Changed syscallLibInit to take hook tbl size param.
01e,31oct03,yvp  Added S_syscallLib_VECTOR_INIT_FAILED.
01d,29sep03,yvp  Added errno S_syscallLib_INVALID_USER_SP.
01c,15sep03,yvp  Moved syscallDispatch prototype to syscallLibP.h
01b,03sep03,yvp  Added extern for syscallGroupTbl.
01a,26aug03,yvp	 written.
*/

#ifndef __INCsyscallLibh
#define __INCsyscallLibh

#ifdef __cplusplus
extern "C" {
#endif

/* includes */

#include "vxWorks.h"
#include "vwModNum.h"

/* generic status codes for syscallLib */

#define S_syscallLib_UNKNOWN_GROUP          (M_syscallLib | 1)
#define S_syscallLib_GROUP_EXISTS           (M_syscallLib | 2)
#define S_syscallLib_INVALID_ROUTINE_COUNT  (M_syscallLib | 3)
#define S_syscallLib_NO_ROUTINES_TBL        (M_syscallLib | 4)
#define S_syscallLib_INVALID_USER_SP        (M_syscallLib | 5)
#define S_syscallLib_VECTOR_INIT_FAILED     (M_syscallLib | 6)

#ifndef _ASMLANGUAGE

/* typedefs */

typedef struct syscall_rtn_tbl_entry
    {
    FUNCPTR    pMethod;
    int        numArgs;     /* numArgs should be the total number of native */
			    /* words that the argument list takes (max = 8) */
			    /* including 64-bit arguments & alignment pad   */
    char *     methodName;
    UINT32     spare;	    /* make sizeof == 16 bytes for cache efficiency */
    } SYSCALL_RTN_TBL_ENTRY;

    /* externals */

extern char * syscallGroupName [];

/* function declarations */

STATUS syscallLibInit (const int);
STATUS syscallGroupRegister (int, char *, int, SYSCALL_RTN_TBL_ENTRY *, BOOL);
void   syscallShow (int, int);

#ifdef __cplusplus
}
#endif

#endif	/* _ASMLANGUAGE */

#endif /* __INCsyscallLibh */
