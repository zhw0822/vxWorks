/* pfwTimer.h -  pfwTimer header file */

/* Copyright 1999 Wind River Systems, Inc. */

/* 
modification history
--------------------
01d,16nov99,koz  Corrected some spelling mistakes
01c,15oct99,koz  Added duration field to PFW_TIME_STAMP
01b,16sep99,koz   Added time stamp interface
01a,16aug99,koz   written 
*/

#ifndef __INCpfwTimerh
#define __INCpfwTimerh

#ifdef __cplusplus
extern "C" {
#endif

#include "pfw/pfw.h"

/* define */

#define PFW_TIME_UNIT_NUM 3 

/* typedefs */

typedef struct
    {
    ULONG start;      /* the system clock tick value when a time stamp starts */
    ULONG duration;   /* duration of time stamp in system clock ticks */
    } PFW_TIME_STAMP;

typedef enum pfwTimeUnit 
    {
    PFW_100_MILLISECOND,
    PFW_SECOND,
    PFW_MINUTE
    } PFW_TIME_UNIT;

typedef STATUS (*PFW_TIMER_HANDLER) (PFW_PLUGIN_OBJ_STATE *, int); 

typedef struct pfwTimerObj PFW_TIMER_OBJ;

/* function declarations */

extern PFW_TIMER_OBJ *pfwTimerCreate (PFW_PLUGIN_OBJ_STATE *state); 
extern STATUS         pfwTimerDelete (PFW_TIMER_OBJ *timerObj);
extern STATUS         pfwTimerStart  (PFW_TIMER_OBJ *timerObj, 
				      PFW_TIME_UNIT timeUnit, 
		                      unsigned int timeoutValue,
				      PFW_TIMER_HANDLER timerHandler, int arg);
extern STATUS         pfwTimerCancel (PFW_TIMER_OBJ* timerObj);
extern STATUS         pfwTimeStampStart (PFW_OBJ *pfwObj,
					 PFW_TIME_STAMP *timeStamp, 
					 PFW_TIME_UNIT timeUnit,
					 unsigned int duration);
extern BOOL           pfwTimeStampExpired (PFW_TIME_STAMP *timeStamp);

#ifdef __cplusplus
}
#endif

#endif /* __INCpfwTimerh */


