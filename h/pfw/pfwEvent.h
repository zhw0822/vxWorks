/* pfwEvent.h - Protocol framework event manager header file */

/* Copyright 1999 Wind River Systems, Inc. */

/* 
modification history
--------------------
01d,31oct99,koz  Added show routine
01c,19sep99,koz  Added the pfwEventStackSubscribe routine
01b,08sep99,koz  Changed interfaces
01a,16aug99,koz  written 
*/

#ifndef __INCpfwEventh
#define __INCpfwEventh

#ifdef __cplusplus
extern "C" {
#endif

#include "pfw/pfw.h"

/* typedefs */

typedef struct pfwEventObj PFW_EVENT_OBJ;
typedef STATUS (*PFW_EVENT_HANDLER) (PFW_PLUGIN_OBJ_STATE *, void *eventData);

/* function declarations */

extern PFW_EVENT_OBJ *pfwEventPublish (PFW_OBJ *pfwObj, char *name);
extern STATUS         pfwEventSubscribe (PFW_PLUGIN_OBJ *pluginObj, 
                                         char * eventName,
					 PFW_EVENT_HANDLER eventHandler);
extern STATUS         pfwEventUnsubscribe (PFW_PLUGIN_OBJ *pluginObj, 
                                           PFW_EVENT_OBJ *eventObj); 
extern STATUS         pfwEventStackSubscribe (PFW_PLUGIN_OBJ_STATE *state, 
                                              PFW_EVENT_OBJ *eventObj,
					      PFW_EVENT_HANDLER eventHandler);
extern PFW_EVENT_OBJ *pfwEventObjGet (PFW_OBJ *pfwObj, char *name);
extern STATUS         pfwEventRaise (PFW_STACK_OBJ *stackObj,
				     PFW_EVENT_OBJ *eventObj, void *eventData);
extern STATUS         pfwEventListShow (PFW_OBJ *pfwObj);

#ifdef __cplusplus
}
#endif

#endif /* __INCpfwEventh */

