/* pfwProfile.h - Protocol framework profile header file */

/* Copyright 1999 Wind River Systems, Inc. */

/* 
modification history
--------------------
01f,29sep00,sj  merging with TOR2_0-WINDNET_PPP-CUM_PATCH_2
01e,03aug00,sj  merge with openstack to bring in prototypes
01d,08jul00,bsn  merged from the PPP radius-ras branch
01c,19may00,qli  added utility functions for set/get stack specific parameters
01b,27oct99,koz  Added MAX_VALUE_STRING_LENGTH
01a,15sep99,sj  brought in pfwParameterAdd and associated definitions from pfw.h
*/

#ifndef __INCpfwProfileh
#define __INCpfwProfileh

#ifdef __cplusplus
extern "C" {
#endif

#include "pfw/pfw.h"

/* defines */

#define MAX_VALUE_STRING_LENGTH 512

/* typedefs */

typedef STATUS (*PFW_PARAMETER_HANDLER) (PFW_OBJ *pfwObj, 
					 PFW_PARAMETER_OPERATION_TYPE,
					 void *, char *);

/* function declarations */

extern PFW_PROFILE_OBJ   *pfwBaseProfileCreate (PFW_OBJ *pfwObj,
					    char *profileName, char *fileName);
extern PFW_PROFILE_OBJ   *pfwProfileCreate (char *profileName, 
				            PFW_PROFILE_OBJ *baseProfileObj,
				            char *fileName);
extern STATUS             pfwProfileDelete (PFW_PROFILE_OBJ *profileObj);
extern PFW_PROFILE_OBJ   *pfwProfileObjGet (PFW_OBJ *pfwObj, char *name);
extern STATUS 	          pfwProfileSet (PFW_PROFILE_OBJ *profileObj, 
				         unsigned int parameterId, char *value);
extern STATUS             pfwParameterAdd (PFW_PLUGIN_OBJ *owner, 
		                              char *parameterName, 
                                              PFW_PARAMETER_HANDLER);
extern unsigned int       pfwParameterIdGet (PFW_OBJ *pfwObj, char *name);
extern PFW_OBJ           *pfwProfilePfwObjGet (PFW_PROFILE_OBJ *profileObj);
extern STATUS             pfwProfileListShow (PFW_OBJ *pfwObj);
extern STATUS             pfwParameterListShow (PFW_OBJ *pfwObj);
extern STATUS             pfwProfileShow (PFW_PROFILE_OBJ *profileObj);

extern STATUS pfwStackParamSet(PFW_STACK_OBJ *stackObj, 
                               unsigned int parameterId,
                               char *value);
extern STATUS pfwStackParamGet(PFW_STACK_OBJ *stackObj,
                               unsigned int parameterId,
                               char *value);

#ifdef __cplusplus
}
#endif

#endif /* __INCpfwProfileh */

