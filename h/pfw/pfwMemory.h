/* pwfMemory.h -  Protocol framework memory manager header file */

/* Copyright 1999 Wind River Systems, Inc. */

/*
modification history
--------------------
01a,30oct99,koz  Included pfw.h
01a,20sep99,koz  written
*/

#ifndef __INCpwfMemoryh
#define __INCpwfMemoryh

#ifdef __cplusplus
extern "C" {
#endif

#include "pfw/pfw.h"

extern void          *pfwMalloc (PFW_OBJ *pfwObj, unsigned int size);
extern STATUS         pfwFree (void *p);
extern STATUS         pfwMemoryShow (PFW_OBJ *pfwObj);

#ifdef __cplusplus
}
#endif

#endif /* __INCpwfMemoryh */
