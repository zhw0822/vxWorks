/* pfwInterface.h - Protocol framework interface manager header file */

/* Copyright 1999 Wind River Systems, Inc. */

/* 
modification history
--------------------
01a,04nov99,koz  Changed pfwInterfaceObjGetNext function parameter
*/

#ifndef __INCpfwInterfaceh
#define __INCpfwInterfaceh

#ifdef __cplusplus
extern "C" {
#endif

#include "vxWorks.h"
#include "dllLib.h"
#include "pfw/pfw.h"

/* defines */

#define PFW_MAX_INTERFACE_NAME_LENGTH 64

/* typedefs */

/*
 *  All user defined interface objects must embedd the following structure as 
 *  the first member. 
 */
typedef struct pfwInterfaceObj
   {
   DL_NODE node;                      /* how framework keeps this in a list */
   unsigned int id;                   /* interface id */
   PFW_PLUGIN_OBJ* pluginObj;         /* pointer to the owner plugin object */
   } PFW_INTERFACE_OBJ;


typedef struct pfwInterfaceStatePair
    {
    PFW_INTERFACE_OBJ *interfaceObj;
    PFW_PLUGIN_OBJ_STATE *state;
    } PFW_INTERFACE_STATE_PAIR;

/* function declarations */

extern unsigned int        pfwInterfaceRegister (PFW_OBJ *pfwObj, char *name);
extern unsigned int        pfwInterfaceIdGet (PFW_OBJ *pfwObj, char *name);
extern STATUS              pfwInterfaceBind (PFW_INTERFACE_OBJ *interfaceObj);
extern PFW_INTERFACE_OBJ  *pfwInterfaceObjGetViaPluginObj 
			       (PFW_PLUGIN_OBJ *pluginObj,
                                unsigned int interfaceId);
extern STATUS              pfwInterfaceObjAndStateGetViaStackObj 
			       (PFW_STACK_OBJ *stackObj, 
				unsigned int interfaceId,
			        PFW_INTERFACE_STATE_PAIR *result); 
extern PFW_INTERFACE_OBJ  *pfwInterfaceObjGetViaPfwObj 
			       (PFW_OBJ *pfwObj, unsigned int interfaceId);
extern PFW_INTERFACE_OBJ  *pfwInterfaceObjGetNext (PFW_INTERFACE_OBJ *);
extern STATUS              pfwInterfaceReferenceAdd 
			       (PFW_INTERFACE_OBJ *interfaceObj);
extern STATUS              pfwInterfaceReferenceDelete 
			       (PFW_INTERFACE_OBJ *interfaceObj);
extern STATUS              pfwInterfaceListShow (PFW_OBJ *pfwObj);

#ifdef __cplusplus
}
#endif

#endif /* __INCpfwInterfaceh */

