/* pwfTable.h -  Table header file */

/* Copyright 1999 Wind River Systems, Inc. */

/*
modification history
--------------------
01d,17oct99,koz  Remove table object structure declaration
01c,31aug99,koz  Removed table macros
01b,16aug99,koz  Converted to a framework file
01a,02aug99,koz  Added add and delete interfaces.
*/

#ifndef __INCpwfTableh
#define __INCpwfTableh

#ifdef __cplusplus
extern "C" {
#endif

#include "pfw/pfw.h"


/* typedefs */

typedef BOOL (*PFW_TABLE_FUNC) (unsigned int key, void *tableItem, void *arg);

typedef struct pfwTableObj PFW_TABLE_OBJ;

extern PFW_TABLE_OBJ *pfwTableCreate (PFW_OBJ *pfwObj, unsigned int initialSize,
                                      int maxSize);
extern STATUS         pfwTableDelete (PFW_TABLE_OBJ *tableObj);
extern unsigned int   pfwTableItemAdd (PFW_TABLE_OBJ *tableObj, void *item);
extern void          *pfwTableItemDelete (PFW_TABLE_OBJ *tableObj,
                                          unsigned int key);
extern void          *pfwTableItemGet (PFW_TABLE_OBJ *tableObj, 
				       unsigned int key);
extern unsigned int   pfwTableTraverse (PFW_TABLE_OBJ *tableObj,
                                        PFW_TABLE_FUNC tableFunc, void *arg);
extern unsigned int   pfwTableItemInsertAfter (PFW_TABLE_OBJ *tableObj,
                                               void *item, unsigned int key);
extern unsigned int   pfwTableItemInsertBefore (PFW_TABLE_OBJ *tableObj,
                                                void *item, unsigned int key);
extern unsigned int   pfwTableItemCountGet (PFW_TABLE_OBJ *tableObj);

extern STATUS         pfwTableShow (PFW_TABLE_OBJ *pfwTableObj);

#ifdef __cplusplus
}
#endif

#endif /* __INCpwfTableh */
