/* pfwLayer.h - Protocol framework layer header file */

/* Copyright 1999 Wind River Systems, Inc. */

/* 
modification history
--------------------
01f,13oct99,koz  Changed the routines related to adding/removing components to
                 stack
01e,21sep99,koz  Added new interfaces (componentStackAddDone,
                 componentStackDeleteDone and componentDelete)
01d,15sep99,koz  Added layer delete interface
01c,24aug99,sj  changed prototypes for nextComponentStateGet and
                 firstComponentStateGet
01b,16aug99,koz  Converted to a framework file
01a,23jul99,koz  written 
*/

#ifndef __INCpfwLayerh
#define __INCpfwLayerh

#ifdef __cplusplus
extern "C" {
#endif

#include "pfw/pfw.h"
#include "pfw/pfwComponent.h"

typedef enum pfwLayerType 
    {
    NON_OPERATIONAL_LAYER, 
    OPERATIONAL_LAYER 
    } PFW_LAYER_TYPE;

typedef enum pfwPacketType 
    {
    DATA_PACKET, 
    CONTROL_PACKET
    } PFW_PACKET_TYPE;

/* 
 * Each layer is represented by a layer object. The data structure for a 
 * layer object is defined below.
 */

typedef struct pfwLayerObj
    {
    PFW_PLUGIN_OBJ pluginObj;
    unsigned int planeId;         /* plane id in which the layer is */
    unsigned int position;        /* layer position in the plane (nth layer) */
    PFW_LAYER_TYPE type;          /* operational or non-operational */
    PFW_PACKET_TYPE (*packetType) /* if layer perform demuxing packets */
                    (void *);     /* to control and user planes then this */
                                  /* function returns the packet type */
    STATUS (*componentAdd)        /* called when a component is added to */
           (PFW_COMPONENT_OBJ *); /* the framework */
    STATUS (*componentDelete)     /* called when a component is deleted from */
           (PFW_COMPONENT_OBJ *); /* the framework */
    STATUS (*addDynamicComponentToStack) /* called to dynamically add a */
           (PFW_PLUGIN_OBJ_STATE *, /* componet to a stack instance if the */
	    PFW_PLUGIN_OBJ_STATE *);/* layer has been already added to the */
	                            /* stack instance. Arguments are states */
				    /* of the layer and component */ 
    STATUS (*deleteDynamicComponentFromStack) /* called to dynamically delete*/
	   (PFW_PLUGIN_OBJ_STATE *, /* a component from a stack instance */
	    PFW_PLUGIN_OBJ_STATE *);/* before the layer is deleted from the */
				    /* stack instance. Arguments are states */
				    /* of the layer and component */
    } PFW_LAYER_OBJ;


/* function declarations */

extern STATUS                pfwLayerAdd (PFW_LAYER_OBJ *layerObj);
extern STATUS                pfwLayerDelete (PFW_LAYER_OBJ *layerObj);
extern PFW_LAYER_OBJ        *pfwLayerObjGet (PFW_OBJ * pfwObj, char *name);
extern PFW_PLUGIN_OBJ_STATE *pfwFirstComponentStateGet ( PFW_PLUGIN_OBJ_STATE
                                                        *layerState);
extern PFW_PLUGIN_OBJ_STATE *pfwNextComponentStateGet ( PFW_PLUGIN_OBJ_STATE 
                                                       *layerState);
extern STATUS                pfwLayerListShow (PFW_OBJ *pfwObj);
extern STATUS                pfwLayerShow (PFW_LAYER_OBJ *layerObj);
#ifdef __cplusplus
}
#endif

#endif /* __INCpfwLayerh */


