/* usrPPPConfig.h - user PPP configuration */
 
/* Copyright 1999 - 2004 Wind River Systems, Inc. */
 
#include "copyright_wrs.h"
 
/*
modification history
--------------------
01d,30sep04,ijm added ppp5xSecretsShow, and ppp5xStackGet
01c,27aug04,ijm vxWorks 5.x equivalent PPP changes
01b,25oct02,ijm added pppAddressSet
01a,06jul03,mk  created
*/


#ifndef  __INCusrPPPConfigh
#define  __INCusrPPPConfigh


#ifdef __cplusplus
extern "C" {
#endif

#include "pfw/pfw.h"
#include "ppp/pppVsEndComponent.h"
#include "ppp/kppp.h"
#include "netinet/sl_compress.h"
#include "pppInterfaces.h"
#include "memLib.h"


/* serial ports */

#define COM1 "0"
#define COM2 "1" 

/* Serial peer types */
 
#define REGULAR       0x0   /* other end is a regular(NON-WINDOWS) machine */ 
#define WIN_SERVER    0x1   /* other end is a WINDOWS server */
#define WIN_CLIENT    0x2   /* other end is a WINDOWS client */
#define MODEM         0x3   /* other end is a modem */
 
/* PPPoE connection modes */

#define PPPOE_HOST_CONNECTION_MODE   "0x2"   /* client mode */
#define PPPOE_AC_CONNECTION_MODE     "0x1"   /* server mode */

#define PPP_BOOT_DEV_STR_SIZE       64       /* e.g. "ppp=1,19200,REGULAR" */
#define PEER_TYPE_STR_SIZE          12
#define ACCM_STR_SIZE               12
#define MAGIC_RANGE_STR_SIZE        20
#define PHONE_STR_SIZE              20
#define MODEM_INIT_STR_SIZE         100 

/* usrWindNetPPP connection status flags */

#define PPP_CONN_OPENED               0x1   /* if set, connected with peer OK */
#define PPP_STACK_DELETED             0x2   /* if set, stack has been deleted */
#define PPP_AUTH_REFUSED              0x4   /* if set, server refused auth */
#define PPP_AUTH_FAILED               0x8   /* if set, client auth failed */
#define PPP_STACK_DEL_IN_PROGRESS     0x10  /* if set, stack is being deleted */
#define PPP_DISCONNECT_IN_PROGRESS    0x20  /* disconnection is in progress */
#define PPP_LCP_OPENED                0x40  /* LCP reached OPENED state */
#define PPP_IPCP_OPENED               0x80  /* IPCP reached OPENED state */ 
#define PPP_PAP_OPENED                0x100 /* PAP reached OPENED satte */
#define PPP_CHAP_OPENED               0x200 /* CHAP reached OPENED satte */



/* externs */

extern void pppAddressSet (PFW_PROFILE_OBJ * profile, char * paramName,
                           char * localValue, char * remoteValue,
                           BOOL negotiationRequired);

typedef struct pppFrameworkParameters
    {
    char         pfwName [PFW_MAX_NAME_LENGTH];  /* framework name */
    unsigned int dataTaskPriority;               /* handles data packets: */
                                                 /* IPv4, IPv6 */
    unsigned int controlTaskPriority;            /* handles control packets */ 
                                                 /* LCP, PAP, CHAP, IPCP... */
    unsigned int dataTaskJobQueueSize;           /* in bytes */
    unsigned int controlTaskJobQueueSize;        /* in bytes */
    PART_ID      memPartition;                   /* memory partition */
    NET_POOL_ID  netPool;                        /* network mbuf pool */
    int          numTty;                         /* number of serial ports */
    } PPP_FRAMEWORK_PARAMETERS;
 

typedef struct pppSerialParameters
    {
    /* serial port parameters */

    int  port;                                   /* e.g., 0 is COM1 */
                                                 /* "1" is COM2 */
    int  baudRate;                               /* e.g.19200, 38400 */
    char peerType [PEER_TYPE_STR_SIZE];          /* "REGULAR" */
                                                 /* "WIN_CLIENT */
                                                 /* "WIN_SERVER" */
                                                 /* "MODEM" */ 
    char accm [ACCM_STR_SIZE];                   /* Asynchronous control */
                                                 /* character map, e.g. */
                                                 /* "00000000L", if NULL */
                                                 /* map = "ffffffffL" */
    } PPP_SERIAL_PARAMETERS;



typedef struct pppServerParameters
    {
    BOOL acceptRemote;                           /* allow peer to set its */
                                                 /* local IP address */
    BOOL useRasPool;                             /* Assign IPv4 addresses */
                                                 /* to clients using */
                                                 /* RAS pool */
    char rasPoolStartAddress [INET_ADDR_LEN];    /* Pool start address */
                                                 /* e.g., "90.0.0.10" */
    int rasPoolSize;                             /* Number of addresses */
                                                 /* in pool, default is 20 */                                       
    char primaryDnsAddress [INET_ADDR_LEN];      /* offer clients this */
                                                 /* address */
    char secondaryDnsAddress [INET_ADDR_LEN];    /* to offer clients */
    BOOL useChap;                                /* authenticate clients */
    int  chapInterval;                           /* In seconds */
    BOOL usePap;                                 /* authenticate clients */
    } PPP_SERVER_PARAMETERS;


typedef struct pppClientParameters
    {
    /* client IPv4 parameters */
    
    BOOL acceptLocal;                       /* peer provides it */
    BOOL acceptRemote;                      /* peer provides it */
    BOOL setDefaultRoute;                   /* set peer address as */
                                            /* default route */
    BOOL useChap;                           /* authenticate to server */
    BOOL usePap;                            /* authenticate to server */
    BOOL getDns;                            /* get DNS adddresses */   
    char userName [NAME_SIZE];              /* client user name */
    char password [NAME_SIZE];              /* client password */

    } PPP_CLIENT_PARAMETERS;


typedef struct pppConnectionMode
    {
    PPP_CLIENT_PARAMETERS client;
    PPP_SERVER_PARAMETERS server;
    } PPP_MODE_PARAMETERS;

/*
 * selected connection parameters.  This is just a subset of available
 * options that can be configured using WindNet PPP API
 */

typedef struct pppBasicLinkParameters
    {
    BOOL sendEchoes;                         
    int  echoInterval;                      /* in seconds */
    int  maxEchoRetries;                    /* if peer does not respond, */
                                            /* maximum retries before */
                                            /* terminating connection */ 
    char localMagicRange [MAGIC_RANGE_STR_SIZE];
                                            /* start,end,step: e.g., */
                                            /* "1,2147483647,0" */
                                            /* start should always be > 0 */
                                            /* step of 0 means any number */
                                            /* within the range is OK. */
    char remoteMagicRange [MAGIC_RANGE_STR_SIZE];
    BOOL acfc;                              /* Adress and Control Field */
                                            /* Compression */
    BOOL pfc;                               /* Protocol Field Compression */
    BOOL localVjc;                          /* Van Jacobson TCP header */
    BOOL remoteVjc;                 
    int  localVjcMaxSlots;                  /* maximum local slots */
    int  remoteVjcMaxSlots;                 /* maximum remote slots */
    BOOL silentMode;                        /* if TRUE, responds to configure */
                                            /* requests, but does not */
                                            /* initiate them.*/  
    } PPP_BASIC_LINK_PARAMETERS;

/* requested configuration options */

typedef struct pppConfigurationOptions
    {
    PPP_SERIAL_PARAMETERS     serial;           /* serial port parameters */
    BOOL                      isServer;         /* client/server mode */
    PPP_MODE_PARAMETERS       mode;             /* server/client parameters */
    PPP_BASIC_LINK_PARAMETERS link;             /* basic link parameters */
    char localIpv4Address     [INET_ADDR_LEN];  /* request this address */
                                                /* for client, "0.0.0.0", */
                                                /* means get from server */
    char remoteIpv4Address    [INET_ADDR_LEN];  /* request this address */
                                                /* for client, "0.0.0.0", */
                                                /* means get from server */
                                                /* for server, "0.0.0.0", */
                                                /* means assign addresses */
                                                /* to clients using RAS pool */
    } PPP_CONFIGURATION_OPTIONS;

/*  cookie returned by windNetPPPInit */

typedef struct pppHandle
    {                          
    PFW_STACK_OBJ * pStackObj;            /* returned PPP stack */
    int      status;                      /* returned ppp connection status */
    PPP_END_IPV4_INFO ipv4Info;           /* returned negotiated DNS/IP */
                                          /* addresses;ppp interface unit # */
    SEM_ID   syncSem;                     /* passed client sync semaphore */
    BOOL     isModemPeer;                 /* peer is modem (client mode) */
    BOOL     isServer;                    /* server mode */ 
    BOOL     setDefaultRoute;             /* client mode */ 
    BOOL     verbose;                     /* TRUE: print usrWindNetPPPInit */
                                          /* status messages. */
    int silentRestartInterval;            /* seconds to wait before */
                                          /* re-starting */
    char phoneString [PHONE_STR_SIZE];           /* number to dial */
    char modemInitString [MODEM_INIT_STR_SIZE];  /* external modem */
                                                 /* initialization string */
    } PPP_HANDLE;


/* externs */

extern PPP_HANDLE * usrWindNetPPPInit (char *pBootDev, char *localAddr,
                                       char *peerAddr);

extern PFW_STACK_OBJ * windnetPPPInit (PFW_OBJ *         pFramework,
                                       PFW_PROFILE_OBJ * pProfile,
                                       PPP_HANDLE *      pUserHandle);

extern STATUS ppp5xConnect (PPP_HANDLE * pppUserHandle);
extern STATUS ppp5xDisconnect (PPP_HANDLE * pppUserHandle);
extern void ppp5xDelete (PPP_HANDLE * pppUserHandle);
void ppp5xShow (PPP_HANDLE * pUserHandle, BOOL showStackProfile);


extern STATUS pppLocalSecretAdd (char * userName, char * password);
extern STATUS pppLocalSecretDelete (char * userName);

extern STATUS pppPeerSecretAdd (char * userName, char * password);
extern STATUS pppPeerSecretDelete (char * userName);

extern void ppp5xSecretsShow (void); 

extern PFW_STACK_OBJ * ppp5xStackGet (PPP_HANDLE * pUserHandle); 

extern STATUS pppLocalUserProfileSet (PFW_OBJ * pFramework,
                                      PFW_PROFILE_OBJ * pProfile,
                                      char * userName, char * password);

extern STATUS modemConnectTest (char * initString, char * modemNumber,
                                char * modemDialResponse,
                                PFW_STACK_OBJ * pModemStack,
                                PPP_UPCALL_FUNCTIONS * pUpcalls,
                                void * handle);

extern STATUS modemDisconnectTest (PFW_STACK_OBJ * pModemStack,
                                   BOOL closeConnection);


#ifdef __cplusplus
}
#endif

#endif /*  __INCusrPPPConfigh  */ 

