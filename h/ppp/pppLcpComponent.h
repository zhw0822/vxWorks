/* pppLcpComponent.h - LCP component header file */

/* Copyright 1999 Wind River Systems, Inc. */

#include "copyright_wrs.h"

/*
modification history
--------------------
01e,29sep00,sj  merging with TOR2_0-WINDNET_PPP-CUM_PATCH_2
01d,09aug00,adb  adding description for lcp_passiveMode
01c,07feb00,sj  added lcp_selfDescribingPadding and lcp_callback features
01b,08nov99,sj  change Init to Create
01a,28sep99,sj 	derived from routerware source base
*/

#ifndef __INCpppLcpComponenth
#define __INCpppLcpComponenth

#ifdef __cplusplus
extern "C" {
#endif


#include "vxWorks.h"
#include "pfw/pfw.h"

/*                    Profile Parameters
 *                   --------------------
 *        NAME                              VALUES/EXPLANATION
 *       ------                            --------------------
 * The following are RFC 1661 & 1662 defined LCP negotiation options
 *
 *	lcp_MRU                      Maximum Receive Unit
 *	lcp_ACCM                     Asynchronous Character Control Map
 *	lcp_authProtocol             Authentication Protocol
 *	lcp_qualityProtocol          Link Quality Protocol @* NOT Supported *@
 *	lcp_magicNumber              Magic Number Option
 *	lcp_PFCompression            Protocol Field Compression 
 *	lcp_ACFCompression           Address and Control Field Compression*
 *	lcp_fcsAlternatives          Alternative FCS; NULL and 16 Bit ONLY!
 *	lcp_selfDescribingPadding    Self Describing Padding for PPP packets
 *	lcp_callbackOperation        LCP callback operation option 
 *
 * The following are implementation specific LCP configuration parameters;
 *
 * NOTE:All time intervals and timeout paramters are measured in SECONDS. Also
 * all of the parameters must be set using quoted integer values; e.g. "20"
 *
 *	lcp_callbackMessage          Human readable message with callback option
 *	lcp_timeRemainingInterval    Interval between Time Remaining Messages
 *	lcp_timeRemainingMessage     ASCII string Time Remaining message 
 *	lcp_txIdentMessage           ASCII string Identification message;
 *                                   Setting this parameter with an Non Zero
 *	lcp_maxConfigRequests        Maximum number of Configuration requests
 *	                             to send in one attempt
 *	lcp_configReqSendInterval    Interval between Configuration requests in 
 *	                             an attempt
 *
 *	lcp_configReqBackoffInterval Backoff interval between attempts. when 
 *	                             this interval expires LCP cycles through
 *	                             a DOWN followed by an UP event triggering
 *	                             another set, "lcp_maxConfigRequests",
 *	                             of configuration requests
 *
 *	lcp_maxTerminationRequests   Maximum number of unacknowledged 
 *	                             Termination requests to send.
 *                   
 *	lcp_TerminationReqInterval   Interval between Termination Requests
 *	lcp_echoRequestSendInterval  Interval between echo requests 
 *	lcp_maxUnAckedEchoRequests   Maximum number or unacknowledged echo
 *	                             requests allowed before link is dropped
 *  lcp_passiveMode              Default is FALSE. If set to TRUE then
 *                               on opening a connection LCP will not
 *                               actively initiate negotiation.
 */

/*
 * LCP SUPPORTED INTERFACES: see ./interfaces/lcpInterfaces.h
 */

/* User interfaces */

STATUS lcpComponentCreate (PFW_OBJ * pfw); 
STATUS lcpComponentDelete (PFW_OBJ * pfw); 

#ifdef __cplusplus
}
#endif

#endif /* __INCpppLcpComponenth */
