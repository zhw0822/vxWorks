/* pppPapComponent.h - PPP Authentication header */

/* Copyright 1999 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*
modification history
--------------------
01b,13jan02,ijm added extern "C" for C++
01a,23oct99,sgv  created from routerware source
*/

#ifndef __INCpppPapComponenth
#define __INCpppPapComponenth

#ifdef __cplusplus
extern "C" {
#endif

#include "vxWorks.h"
#include "pfw/pfw.h"

/*                    Profile Parameters
 *                   --------------------
 *        NAME                              VALUES/EXPLANATION
 *       ------                            --------------------
 *      pap_localUserName            Name of the local user name 
 *      pap_authRequestInterval      Interval (in seconds) between pap   
 *                                   authentication request messages
 *      pap_maxAuthRequestRetries    Maximum number of unacknowledged pap 
 *                                   authentication requests to send
 */

extern STATUS pppPapComponentCreate (PFW_OBJ *pfw);
extern STATUS pppPapComponentDelete (PFW_OBJ *pfw);

#ifdef __cplusplus
}
#endif

#endif /* __INCpppPapComponenth */

