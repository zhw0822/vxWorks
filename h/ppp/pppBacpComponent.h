/* pppBacpComponent.h - BACP component header file */

/* Copyright 1999 Wind River Systems, Inc. */

#include "copyright_wrs.h"

/*
modification history
--------------------
01a,01feb01,sd 	created
*/

#ifndef __INCpppBacpComponenth
#define __INCpppBacpComponenth

#ifdef __cplusplus
extern "C" {
#endif


#include "vxWorks.h"
#include "pfw/pfw.h"

/*                    Profile Parameters
 *                   --------------------
 *        NAME                              VALUES/EXPLANATION
 *       ------                            --------------------
 * The following are RFC 2125 defined BACP negotiation options
 *
 *	bacp_FP						Favored peer
 *
 * The following are implementation specific BACP configuration parameters;
 *
 * NOTE:All time intervals and timeout paramters are measured in SECONDS. Also
 * all of the parameters must be set using quoted integer values; e.g. "20"
 *
 *	bacp_maxConfigRequests			Maximum number of Configuration requests
 *	                             	to send in one attempt
 *	bacp_configReqSendInterval    	Interval between Configuration requests in 
 *	                             	an attempt
 *
 *	bacp_configReqBackoffInterval 	Backoff interval between attempts. when 
 *	                             	this interval expires BACP cycles through
 *	                             	a DOWN followed by an UP event triggering
 *	                             	another set, "bacp_maxConfigRequests",
 *	                             	of configuration requests
 *
 *	bacp_maxConfigFailures			Maximum number of configuration failures
 *									in an attempt.

 *  bacp_maxTerminationRequests  	Maximum number of unacknowledged 
 *	                             	Termination requests to send.
 *                   
 *	bacp_TerminationReqInterval   	Interval between Termination Requests
 *
 * 	bap_maxCallRequests				Maximum number of call requests to send 
 *									in one attempt.
 *  bap_callRequestSendInterval		Interval between two call requests 	 
 *
 * 	bap_maxCallbackRequests			Maximum number of call back requests to 
 *									send in one attempt.
 *  bap_callRequestSendInterval		Interval between two call back requests 
 *
 * 	bap_maxLinkdropRequests			Maximum number of link drop requests to send 
 *									in one attempt.
 *  bap_linkdropRequestSendInterval	Interval between two link drop requests 
 *
 * 	bap_maxIndications				Maximum number of indications to send 
 *									in one attempt.
 *  bap_indicationSendInterval		Interval between two indications 	 
 */

/*
 * BACP SUPPORTED INTERFACES: see ./interfaces/bacpInterfaces.h
 */

/* User interfaces */

STATUS bacpComponentCreate (PFW_OBJ * pfw); 
STATUS bacpComponentDelete (PFW_OBJ * pfw); 

#ifdef __cplusplus
}
#endif

#endif /* __INCpppBacpComponenth */
