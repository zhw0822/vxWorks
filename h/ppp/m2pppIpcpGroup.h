/* m2pppIpcpGroup.h - PPP SNMP agent interface library header */

/* Copyright 1999 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*
modification history
--------------------
01a,06jan00,sj   created.
*/

#include "pfw/pfw.h"
#include "pfw/pfwTable.h"
#include "pfw/pfwMemory.h"
#include "ppp/m2pppLib.h"
#include "ppp/interfaces/ipcpInterfaces.h"

#ifndef __INCm2pppIpcpGrouph
#define  __INCm2pppIpcpGrouph

#ifdef __cplusplus
extern "C" {
#endif

/* defines */

/* defines for enumerated types as specified by RFC 1473 */

/* possible values for pppIpOperStatus */

#define M2_pppIpOperStatus_opened      	1
#define M2_pppIpOperStatus_not_opened	2

/* possible values for pppIpLocalToRemoteCompressionProtocol */

#define M2_pppIpLocalToRemoteCompressionProtocol_none      	1
#define M2_pppIpLocalToRemoteCompressionProtocol_vj_tcp		2

/* possible values for pppIpRemoteToLocalCompressionProtocol */

#define M2_pppIpRemoteToLocalCompressionProtocol_none      	1
#define M2_pppIpRemoteToLocalCompressionProtocol_vj_tcp		2

/* possible values for pppIpLocalToRemoteCompressionProtocol */

#define M2_pppIpConfigAdminStatus_open      	1
#define M2_pppIpConfigAdminStatus_close		2

/* possible values for pppIpConfigCompression */

#define M2_pppIpConfigCompression_none      	1
#define M2_pppIpConfigCompression_vj_tcp	2


/* typedefs */

typedef struct PPP_IPCP_ENTRY_DATA 
    {
    PFW_STACK_OBJ           * stackObj;
    PFW_PLUGIN_OBJ_STATE    * state;
    PPP_IPCP_MIB_INTERFACE  * interface;
    }PPP_IPCP_ENTRY_DATA;

extern STATUS m2pppIpcpGroupRegister (PFW_OBJ * pfwObj);
extern STATUS m2pppIpcpGroupUnregister (PFW_OBJ * pfwObj);
extern STATUS m2pppIpcpEntryLookup (UINT32 oidLen, UINT32 * oid,
					    PPP_IPCP_ENTRY_DATA * data,
					    M2_PPP_ENTRY_MATCH_TYPE match);
#ifdef __cplusplus
}
#endif

#endif /*  __INCm2pppIpcpGrouph */
