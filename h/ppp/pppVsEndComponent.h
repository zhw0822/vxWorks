/* pppVsEndComponent.h - VS END component header file */

/* Copyright 2002 - 2004 Wind River Systems, Inc. */

#include "copyright_wrs.h"

/*
modification history
--------------------
01b,30jul04,ijm added pppIpv4InfoGet, pppIpv6InfoGet, and pppUnitGet 
01a,26may02,ijm created
*/

#ifndef __INCpppVsEndComponenth
#define __INCpppVsEndComponenth

#ifdef __cplusplus
extern "C" {
#endif

#include "vxWorks.h"
#include "pfw/pfw.h"
#include "inetLib.h"

typedef struct pppEndIpv4InfoGet
    {
    int unit;
    char localAddress [INET_ADDR_LEN];
    char remoteAddress [INET_ADDR_LEN];
    char primaryDnsAddress[INET_ADDR_LEN];
    char secondaryDnsAddress [INET_ADDR_LEN];
    BOOL connected;
    } PPP_END_IPV4_INFO;


typedef struct pppEndIpv6InfoGet
    {
    int unit;
    char localAddress [INET6_ADDR_LEN];
    char remoteAddress [INET6_ADDR_LEN];
    BOOL connected;
    } PPP_END_IPV6_INFO;

STATUS pppVsEndComponentCreate (PFW_OBJ * pfw);
STATUS pppVsEndComponentDelete (PFW_OBJ * pfw);

int pppUnitGet (PFW_STACK_OBJ * pStackObj);

STATUS pppIpv4InfoGet (PFW_STACK_OBJ * pStackObj,
                              PPP_END_IPV4_INFO * pIpv4Info);
STATUS pppIpv6InfoGet (PFW_STACK_OBJ * pStackObj,
                              PPP_END_IPV6_INFO * pIpv6Info);

#ifdef __cplusplus
}
#endif

#endif /* __INCpppVsEndComponenth */
