/* m2pppLib.h - PPP SNMP agent interface library header */

/* Copyright 1999 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*
modification history
--------------------
01c,14feb00,adb  WRS conventions
01b,09feb00,adb  Few changes are expected

01a,06jan00,sj   created.
*/

#ifndef __INCm2pppLibh
#define  __INCm2pppLibh

#ifdef __cplusplus
extern "C" {
#endif

/* defines */

#define MAX_IF_INDEX 0x7fffffff /* following MIB RFCs specification */

#define INVALID_IF_INDEX ~0

/* typedefs */

typedef enum
    {
    M2_PPP_ENTRY_EXACT_MATCH,
    M2_PPP_ENTRY_NEXT_MATCH
    }   M2_PPP_ENTRY_MATCH_TYPE;

/* function declarations */

extern STATUS m2pppLibInit(void);
extern STATUS m2pppInterfaceRegister(UINT32 ifIndex, PFW_STACK_OBJ * stackObj);
extern STATUS m2pppInterfaceUnregister(UINT32 ifIndex);
extern STATUS m2pppInterfaceStackObjGet(UINT32 ifIndex, 
                                        PFW_STACK_OBJ ** pStackObj);
extern STATUS m2pppNextInterfaceStackObjGet(UINT32 ifIndex, 
					                        UINT32 * nextIfIndex,
                                            PFW_STACK_OBJ ** pStackObj);
extern STATUS m2pppPreviousInterfaceStackObjGet(UINT32 ifIndex, 
					                            UINT32 * previousIfIndex,
                                                PFW_STACK_OBJ ** pStackObj);
extern STATUS m2pppLibClear (void);

#ifdef __cplusplus
}
#endif

#endif /*  __INCm2pppLibh */
