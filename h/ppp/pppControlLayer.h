/* pppControlLayer.h - PPP muxAdapter interface */

/* Copyright 1984-1999 Wind River Systems, Inc. */

/*
modification history
--------------------
01a,08nov99,sj 	created
*/

#ifndef __INCpppControlLayerh
#define __INCpppControlLayerh

#ifdef __cplusplus
extern "C" {
#endif

/*                    Profile Parameters
 *                   --------------------
 *        NAME                              VALUES/EXPLANATION
 *       ------                            --------------------
 *	"ppp_secretsDatabase"     Specifies the use of a "LOCAL" or "EXTERNAL"
 *                                secrets database for retrieving the password
 *                                or secret to authenticate a peer or
 *                                authenticate with a peer
 */

/* User interfaces */

extern STATUS pppControlLayerCreate (PFW_OBJ * pfw);
extern STATUS pppControlLayerDelete (PFW_OBJ * pfw);
#ifdef __cplusplus
}
#endif

#endif /* __INCpppControlLayerh */
