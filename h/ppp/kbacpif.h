/* kbacpif.h - BAP Port Information structure header file */

/* Copyright 1999 Wind River Systems, Inc. */

#include "copyright_wrs.h"

/*
modification history
--------------------
01a,01feb01,sd 	created from routerware source code
*/


/*
*$Log:: $
 * 
 * 4     4/
 * Bacp
 * v4.2.0
 * check in
 * 
 * 1     4
 * code
 * cleanup
 * , code
 * style
 * changes
 * ,
 * linted,
 * system
 * level
 * test
 * BACP
 * v4.2.0
*/

#ifndef __INCkbacpifh
#define __INCkbacpifh

#ifdef __cplusplus
extern "C" {
#endif

#define DEFAULT_MAXIMUM_MRRU                            1500

typedef enum BAP_LINK_TYPES
	{
	BAP_ISDN_TYPE				= (BYTE) 0x01,
	BAP_X25_TYPE				= (BYTE) 0x02,
	BAP_ANALOG_TYPE 			= (BYTE) 0x04,
	BAP_SWITCHED_DIGITAL_TYPE	= (BYTE) 0x08,
	BAP_ISDN_DOV_TYPE			= (BYTE) 0x10,
	BAP_RESERVED_1_TYPE			= (BYTE) 0x05,
	BAP_RESERVED_2_TYPE			= (BYTE) 0x06,
	BAP_RESERVED_3_TYPE			= (BYTE) 0x07,
	BAP_DEDICATED_PORT			= (BYTE) 0xff
	} BAP_LINK_TYPES;	


enum CALL_STATUS
	{
	CALL_STATUS_OK						= (BYTE) 0x00,
	CALL_STATUS_UNASSIGNED_NUMBER		= (BYTE) 0x01,
	CALL_STATUS_USER_BUSY				= (BYTE) 0x11,
	CALL_STATUS_SECURITY_VIOLATION      = (BYTE) 0xfe,	/* routerware added */
	CALL_STATUS_NON_SPECIFIC_FAILURE	= (BYTE) 0xFF
	} CALL_STATUS;

enum CALL_ACTION
	{
	CALL_ACTION_NO_RETRY	= (BYTE) 0x00,
	CALL_ACTION_RETRY		= (BYTE) 0x01
	} CALL_ACTION;



enum PORT_DIALUP_TYPE
	{
 	PORT_DEDICATED,
	PORT_DIAL_IN,
 	PORT_DIAL_OUT,
 	PORT_DIAL_IN_AND_OUT 
	} PORT_DIALUP_TYPE;

enum PORT_DIAL_STATUS
	{
	PORT_DIAL_DISCONNECTED,
	PORT_DIAL_WAITING_CONNECT_CMD,
	PORT_DIAL_CONNECTING,
	PORT_DIAL_CONNECTED,
	PORT_DIAL_DISCONNECTING
	} PORT_DIAL_STATUS;


#define MAX_LENGTH_OF_PHONE_NUMBER	24

typedef struct PORT_CONNECTION_INFO
    {
	struct PORT_CONNECTION_INFO	* next;
    PFW_PROFILE_OBJ  		    * linkProfileObj; 
	PFW_STACK_OBJ 				* linkStackObj; 
    BYTE_ENUM(BAP_LINK_TYPES)   port_type;
	BOOL						is_port_type_specified; 
    USHORT				     	port_number;
    enum PORT_DIAL_STATUS	    port_dial_status;
    BYTE_ENUM(CALL_ACTION)	    port_action;
    BYTE_ENUM(CALL_STATUS)	    port_status;
    USHORT						port_speed;
    enum PORT_DIALUP_TYPE	   	port_dial_type;
    USHORT			   		port_lcp_local_link_discriminator;
    USHORT			   		port_lcp_remote_link_discriminator;
    char 			port_local_phone_number[MAX_LENGTH_OF_PHONE_NUMBER];
    char 			port_remote_phone_number[MAX_LENGTH_OF_PHONE_NUMBER];
    char    	    port_subaddress_phone_number[MAX_LENGTH_OF_PHONE_NUMBER];
    char  			port_unique_digits_phone_number_length;
    char   		    port_unique_digits_phone_number[MAX_LENGTH_OF_PHONE_NUMBER];

	} PORT_CONNECTION_INFO;

/* typedefs */

typedef struct linkInfo
	{
	struct LINK_INFO			*pNext;
	PFW_PROFILE_OBJ				*pLinkProfileObj;
	BOOL						isPortUsed;
	BYTE_ENUM (BAP_LINK_TYPES)	port_type;
	enum PORT_DIALUP_TYPE		port_dial_type;
	USHORT						port_number;
	enum PORT_DIAL_STATUS		port_dial_status;
	USHORT						bandwidth;
	char		port_local_phone_number [MAX_LENGTH_OF_PHONE_NUMBER];
	char 		port_remote_phone_number [MAX_LENGTH_OF_PHONE_NUMBER];
	char 		port_subaddress_phone_number [MAX_LENGTH_OF_PHONE_NUMBER];
	char 		port_unique_digits_phone_number_length;
	char 		port_unique_digits_phone_number [MAX_LENGTH_OF_PHONE_NUMBER];
	} LINK_INFO;


#ifdef __cplusplus
}
#endif

#endif /* __INCkbacpifh */

