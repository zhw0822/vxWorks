/* pppMuxAdapter.h - PPP muxAdapter interface */

/* Copyright 1984-1999 Wind River Systems, Inc. */

/*
modification history
--------------------
01c,29oct02,ijm added PPP_MUX_ADAPTER_MAX_BINDS definition
01b,18apr01,sj  added description for new profile parameter: muxL2SvcType
01a,05jul99,sj 	created
*/

#ifndef __INCpppMuxAdapterh
#define __INCpppMuxAdapterh

#ifdef __cplusplus
extern "C" {
#endif

/*                    Profile Parameters
 *                   --------------------
 *        NAME                              VALUES/EXPLANATION
 *       ------                            --------------------
 * "muxDevName"               Name of device to bind to: "cpm", "fei", "ln" etc.
 * "muxDevUnit"               Unit number of device: "0", "1" etc.
 *
 * "muxSvcType"               ':' delimited list of Protocol Numbers that
 *                            must be bound to the specified device. For e.g.:
 *                            "0x8863:0x8864" for PPPoE 
 *
 * "muxL2SvcType"             By default this has a value of 0x0 and is unused.
 *                            This may be set to a non-zero value ONLY when 
 *                            used with an END style driver.
 *
 *                            Any non-zero value is used as a protocol number
 *                            and is bound to the END in addition to the
 *                            protocols specified via the muxSvcType parameter.
 *                            When this parameter is used the MUX_ADAPTER 
 *                            pluginObj ALWAYS forwards packets/frames received
 *                            on its send() interface unaltered to the END 
 *                            using muxSend().
 *
 *                            The 'reserved' field in the M_BLK_HDR of the
 *                            leading M_BLK of the packet/frame is set to
 *                            muxL2SvcType, in Network Byte Order, before it
 *                            is sent to the END to allow the END to use it
 *                            in any manner that it chooses.
 *
 *                            It is recommended that values for this parameter
 *                            be chosen from among the interface types (ifType)
 *                            defined in RFC 1213 (IANA-MIB)
 */

#define PPP_MUX_ADAPTER_MAX_BINDS    16  /*
                                          * Maximum number of protocol
                                          * bindings for all PPPoE devices
                                          * allowed for the MUX Adapter component.
                                          *
                                          * Should be large enough to accomodate
                                          * muxSvcType and muxL2SvcType, if any,
                                          * for all PPPoE devices.  This definition is
                                          * used only when PPP is installed on
                                          * the IPv4/Ipv6 dual stack. For all other
                                          * stacks, T2.x or T3.x, the global variable
                                          * muxMaxBinds is used as the upper bound
                                          * value for this parameter
                                          */

/* User interfaces */

extern STATUS muxAdapterCreate (PFW_OBJ * pfw);
extern STATUS muxAdapterDelete (PFW_OBJ * pfw);

#ifdef __cplusplus
}
#endif

#endif /* __INCpppMuxAdapterh */
