/* pppChecksum.h - ppp checksum library header file */

/* Copyright 1999 Wind River Systems, Inc. */

#include "copyright_wrs.h"

/*
modification history
--------------------
01b,16feb00,sj  changing definition of PPP_*BIT_FCS
01a,29dec99,sj 	created
*/

#ifndef __INCpppChecksumh
#define __INCpppChecksumh

#ifdef __cplusplus
extern "C" {
#endif

#include "netBufLib.h"

#define PPPINITFCS32  0xffffffff   /* Initial 32 bit FCS value */
#define PPPGOODFCS32  0xdebb20e3   /* Good final 32 bit FCS value */

#define PPPINITFCS16  0xffff       /* Initial 16 bit FCS value */
#define PPPGOODFCS16  0xf0b8       /* Good final 16 bit FCS value */

#define PPP_NULL_FCS         0
#define PPP_16BIT_FCS        16
#define PPP_32BIT_FCS        32
#define PPP_DEFAULT_FCS_SIZE PPP_16BIT_FCS

extern UINT16 calculate16BitFcs ( UINT16 fcs, M_BLK_ID frame);
extern UINT32 calculate32BitFcs ( UINT32 fcs, M_BLK_ID frame);

#ifdef __cplusplus
}
#endif

#endif /* __INCpppChecksumh */


