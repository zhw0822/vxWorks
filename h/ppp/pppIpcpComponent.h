/* pppIpcpComponent.h - IPCP component header file */

/* Copyright 1999 Wind River Systems, Inc. */

#include "copyright_wrs.h"

/*
modification history
--------------------
01a,12oct99,sgv derived from routerware source base
*/

#ifndef __INCpppIpcpComponenth
#define __INCpppIpcpComponenth

#ifdef __cplusplus
extern "C" {
#endif


#include "vxWorks.h"
#include "pfw/pfw.h"

/*                    Profile Parameters
 *                   --------------------
 *        NAME                              VALUES/EXPLANATION
 *       ------                            --------------------
 * The following are RFC 1332 defined IPCP negotiation options
 *
 *      ipcp_ipAddr                   Set the IP address of the local/peer 
 *				      system
 *      ipcp_vjcParams                Van Jacobson IP Header compression 
 *
 * The following are implementation specific IPCP configuration parameters;
 *
 * NOTE:All time intervals and timeout paramters are measured in SECONDS. Also
 * all of the parameters must be set using quoted integer values; e.g. "20"
 *      ipcp_maxConfigRequests        Maximum number of unacknoledged 
 *				      Configuration requests to send in one 
 *				      attempt
 *      ipcp_configReqSendInterval    Interval between Configuration requests
 *                                    in an attempt
 *
 *      ipcp_maxTerminationRequests   Maximum number of unacknowledged
 *                                    Termination requests to send
 *
 *      ipcp_terminationReqInterval   Interval between Termination Requests
 *
 */

STATUS pppIpcpComponentCreate (PFW_OBJ * pfw);
STATUS pppIpcpComponentDelete (PFW_OBJ * pfw);

#ifdef __cplusplus
}
#endif

#endif /* __INCpppipcpComponenth */
