/* pppLinkIdInterface.h - PPP IP up/down interface */

/* Copyright 1999 Wind River Systems, Inc. */

#include "copyright_wrs.h"

/*
modification history
--------------------
01a,04nov99,sgv  created
*/

#ifndef __INCpppLinkIdInterfaceh
#define __INCpppLinkIdInterfaceh

#ifdef __cplusplus
extern "C" {
#endif

#include "vxWorks.h"
#include "pfw/pfw.h"
#include "pfw/pfwInterface.h"

/*******************************************************************************
* PPP_LINK_ID_INTERFACE
*
* Interface Name: "PPP_LINK_ID_INTERFACE"
*
* This interface is implemented by components of the interface layer of the PPP
* stack. This interface is used to get a link Id for the PPP link. This Id
* is typically unique for each interface in the system and is used by the
* protocols at layer 3 and above.
*
* This interface MUST be published by the INTERFACE_LAYER component in the
* stack.
*/

typedef struct PPP_LINK_ID_INTERFACE
    {
    PFW_INTERFACE_OBJ interfaceObj;
    UINT32 (*pppLinkIdGet)(PFW_PLUGIN_OBJ_STATE * state);
    } PPP_LINK_ID_INTERFACE;

#ifdef __cplusplus
}
#endif

#endif /* __INCpppLinkIdInterfaceh */
