/* pppHcInterfaces.h - PPP Header Compression Component Interfaces */

/* Copyright 2002 Wind River Systems, Inc. */

#include "copyright_wrs.h"

/*
modification history
--------------------
01b,14jan03,adb  branching to tor2_2.windnet_ppp2_0
01a,23apr02,adb  created
*/

#ifndef __INCpppHcInterfacesh
#define __INCpppHcInterfacesh

#ifdef __cplusplus
extern "C" {
#endif

#include "vxWorks.h"
#include "pfw/pfw.h"
#include "pfw/pfwInterface.h"

#include "ppp/pppHeaderCompression.h"

/*******************************************************************************
* PPP_HC_INTERFACE -
*
* Interface Name: "PPP_HC_INTERFACE"
*
* This interface is implemented by the PPP_HEADER_COMPRESSION component 
* of the PPP stack. The PPP_IPCP_COMPONENT and the PPP_IPV6CP_COMPONENT
* will access it to activate to initialize and shutdown header compression
* for IPv4 and IPv6 and protocols carried over IPv4 and IPv6 over PPP.
* Header compression for a particular protocol can be invoked only once.
* If the caller wishes to modify header compression parameters then
* it should call hcShutdown () first before calling hcInitialize () again.
*/

typedef struct PPP_HC_INTERFACE
    {
    PFW_INTERFACE_OBJ   pfwInterfaceObj;

    STATUS (*hcInitialize)
        (
        PFW_PLUGIN_OBJ_STATE *          pPfwPluginObjState,
        HC_PPP_CONFIGURATION_OPTION *   pHcPppConfigurationOption
        );

    STATUS (*hcShutdown)
        (
        PFW_PLUGIN_OBJ_STATE *  pPfwPluginObjState,
        USHORT                  protocol
        );
    
    } PPP_HC_INTERFACE;

#ifdef __cplusplus
}
#endif

#endif /* __INCpppHcInterfacesh */
