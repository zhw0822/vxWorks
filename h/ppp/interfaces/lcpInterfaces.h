/* lcpInterfaces.h - LCP published interfaces */

/* Copyright 1999 Wind River Systems, Inc. */

#include "copyright_wrs.h"

/*
modification history
--------------------
01d,29sep00,sj  merging with TOR2_0-WINDNET_PPP-CUM_PATCH_2
01e,01aug00,adb  Merging with openstack view
01d,11jul00,md  added LCP_CONFIG_REQS_INTERFACE
01c,01mar00,sj  added LCP_PACKET_SEND_INTERFACE
01b,05jan00,sj  adding lcpExtensions and pppLinkConfigEntry and 
             	pppSecurityConfigEntry
01a,15oct99,sj 	created
*/

#include "pfw/pfw.h"
#include "pfw/pfwInterface.h"
#include "ppp/mp.h" /* WindNet Multilink */

#ifndef __INClcpInterfacesh
#define __INClcpInterfacesh

#ifdef __cplusplus
extern "C" {
#endif

/*******************************************************************************
* PPP_LINK_STATUS_ENTRY_INTERFACE - PPP link status interface
*
* Interface Name: "PPP_LINK_STATUS_ENTRY_INTERFACE"
*
* This interface is published and made available by LCP.
* Other components in the system such as IPCP and ASYNC framing use this
* interface to retrieve link related negotiated LCP options
*/
typedef struct PPP_LINK_STATUS_ENTRY_INTERFACE
    {
    PFW_INTERFACE_OBJ interfaceObj;
    UINT32 (*pppLinkStatusPhysicalIndexGet)(PFW_PLUGIN_OBJ_STATE * lcpState);
    UINT32 (*pppLinkStatusBadAddressesGet)(PFW_PLUGIN_OBJ_STATE * lcpState);
    UINT32 (*pppLinkStatusBadControlsGet)(PFW_PLUGIN_OBJ_STATE * lcpState);
    UINT32 (*pppLinkStatusPacketTooLongsGet)(PFW_PLUGIN_OBJ_STATE * lcpState);
    UINT32 (*pppLinkStatusBadFCSsGet)(PFW_PLUGIN_OBJ_STATE * lcpState);
    USHORT (*pppLinkStatusLocalMRUGet) ( PFW_PLUGIN_OBJ_STATE * lcpState);
    USHORT (*pppLinkStatusRemoteMRUGet) ( PFW_PLUGIN_OBJ_STATE * lcpState);

    UINT32 (*pppLinkStatusLocalToPeerACCMapGet)
					    (PFW_PLUGIN_OBJ_STATE * lcpState);
    UINT32 (*pppLinkStatusPeerToLocalACCMapGet)
					    ( PFW_PLUGIN_OBJ_STATE * lcpState);
    UINT32 (*pppLinkStatusLocalToRemoteProtocolCompressionGet)
					    ( PFW_PLUGIN_OBJ_STATE * lcpState);
    UINT32 (*pppLinkStatusRemoteToLocalProtocolCompressionGet) 
					    ( PFW_PLUGIN_OBJ_STATE * lcpState);
    UINT32 (*pppLinkStatusLocalToRemoteACCompressionGet)
					     (PFW_PLUGIN_OBJ_STATE * lcpState);
    UINT32 (*pppLinkStatusRemoteToLocalACCompressionGet)
					     (PFW_PLUGIN_OBJ_STATE * lcpState);

    UINT32 (*pppLinkStatusTransmitFcsSizeGet) (PFW_PLUGIN_OBJ_STATE * lcpState);
    UINT32 (*pppLinkStatusReceiveFcsSizeGet)  (PFW_PLUGIN_OBJ_STATE * lcpState);
    } PPP_LINK_STATUS_ENTRY_INTERFACE;

/*******************************************************************************
* PPP_LINK_CONFIG_ENTRY_INTERFACE - PPP link config interface
*
* Interface Name: "PPP_LINK_CONFIG_ENTRY_INTERFACE"
*
* This interface is published and made available by LCP.
* This interface is intended for any external agent such as SNMP to configure
* values for LCP
*/
typedef struct PPP_LINK_CONFIG_ENTRY_INTERFACE
    {
    PFW_INTERFACE_OBJ interfaceObj;
    ULONG (*pppLinkConfigInitialMRUGet) (PFW_PLUGIN_OBJ_STATE * lcpState);
    STATUS (*pppLinkConfigInitialMRUSet) (PFW_PLUGIN_OBJ_STATE * lcpState,
								ULONG value);
    ULONG (*pppLinkConfigReceiveACCMapGet) (PFW_PLUGIN_OBJ_STATE * lcpState);
    STATUS (*pppLinkConfigReceiveACCMapSet) (PFW_PLUGIN_OBJ_STATE * lcpState,
								ULONG value);
    ULONG (*pppLinkConfigTransmitACCMapGet) (PFW_PLUGIN_OBJ_STATE * lcpState);
    STATUS (*pppLinkConfigTransmitACCMapSet) (PFW_PLUGIN_OBJ_STATE * lcpState,
								ULONG value);
    ULONG (*pppLinkConfigMagicNumberGet) (PFW_PLUGIN_OBJ_STATE * lcpState);
    STATUS (*pppLinkConfigMagicNumberSet) (PFW_PLUGIN_OBJ_STATE * lcpState,
								ULONG value);
    ULONG (*pppLinkConfigFcsSizeGet) (PFW_PLUGIN_OBJ_STATE * lcpState);
    STATUS (*pppLinkConfigFcsSizeSet) (PFW_PLUGIN_OBJ_STATE * lcpState,
								ULONG value);
    }PPP_LINK_CONFIG_ENTRY_INTERFACE;

/*******************************************************************************
* LCP_NEGOTIATED_AUTH_PROTOCOL_INTERFACE - auth protocol interface
*
* Interface Name: "LCP_NEGOTIATED_AUTH_PROTOCOL_INTERFACE"
* 
* This interface is published and made available by LCP. This interface
* is intended for the Control Layer and the Authentication Protocol components
* in the system. It is used to determine the negotiated Authentication protocols
*/
typedef struct LCP_NEGOTIATED_AUTH_PROTOCOL_INTERFACE
    {
    PFW_INTERFACE_OBJ interfaceObj;
    UINT32 (*pppLocalAuthenticationProtocolGet)
					     (PFW_PLUGIN_OBJ_STATE * lcpState);
    UINT32 (*pppRemoteAuthenticationProtocolGet)
					     (PFW_PLUGIN_OBJ_STATE * lcpState);
    } LCP_NEGOTIATED_AUTH_PROTOCOL_INTERFACE;

/*******************************************************************************
* PPP_LINK_STATUS_COUNTER_INCREMENT_INTERFACE - link statistics interface
*
* Interface Name: "PPP_LINK_STATUS_COUNTER_INCREMENT_INTERFACE"
*  
* This interface is published and made available by LCP. This interface is
* intended for the framing and adapter components in the system. It is used
* to increment the link statistics counters.
*/
typedef struct PPP_LINK_STATUS_COUNTER_INCREMENT_INTERFACE
    {
    PFW_INTERFACE_OBJ interfaceObj;
    void (*pppLinkStatusBadAddressesIncrement)(PFW_PLUGIN_OBJ_STATE * lcpState);
    void (*pppLinkStatusBadControlsIncrement)(PFW_PLUGIN_OBJ_STATE * lcpState);
    void (*pppLinkStatusPacketTooLongsIncrement)
					    (PFW_PLUGIN_OBJ_STATE * lcpState);
    void (*pppLinkStatusBadFCSsIncrement)(PFW_PLUGIN_OBJ_STATE * lcpState);

    }PPP_LINK_STATUS_COUNTER_INCREMENT_INTERFACE;

/*******************************************************************************
* LCP_EXTENDED_OPTIONS_INTERFACE - interface for extended LCP options (RFC 1570)
*
* Interface Name: "LCP_EXTENDED_OPTIONS_INTERFACE"
*
* This interface is published and made available by LCP.
* The Control layer uses this to determine if callback has been negotiated.
* Also other protocols such as the Encryption protocols may use this to
* determine if Self Describing Padding is in use 
* 
* Compound frames option described in RFC 1570 is not supported
*/
typedef struct LCP_EXTENDED_OPTIONS_INTERFACE
    {
    PFW_INTERFACE_OBJ interfaceObj;
    UINT8 (*pppLocalToPeerMaxSelfDescribingPad) /* Returns the length(# bytes)*/
	(                                       /* of padding the LOCAL system*/
	PFW_PLUGIN_OBJ_STATE * lcpState         /* has negotiated for use when*/
	);                                      /* sending packets to PEER */

    UINT8 (*pppPeerToLocalMaxSelfDescribingPad) /* Returns the length (#bytes)*/
	(                                       /* of padding the PEER system */
	PFW_PLUGIN_OBJ_STATE * lcpState         /* has negotiated for use when*/
	);                                      /* sending packets to LOCAL */
                                                /* system */

    BOOL (*pppLocalToPeerCallbackOperation)     /* Returns TRUE if the LOCAL */
	(                                       /* system has successfully */
	PFW_PLUGIN_OBJ_STATE * lcpState,        /* negotiated for a CALLBACK */
	UINT8 * operation                       /* by the PEER */
	);

    BOOL (*pppPeerToLocalCallbackOperation)     /* Returns TRUE if the PEER */
	(                                       /* system has successfully */
	PFW_PLUGIN_OBJ_STATE * lcpState,        /* negotiated for the LOCAL */
	UINT8 * operation,                      /* LOCAL system to callback */
	char * msg                              /* the PEER. "operation" and */
	);                                      /* "msg" contains the callback*/
                                                /* instructions */
    } LCP_EXTENDED_OPTIONS_INTERFACE; 

/*******************************************************************************
* LCP_CONFIG_REQS_INTERFACE - interface to ask LCP for Configure Request packets
* Interface Name: "LCP_CONFIG_REQS_INTERFACE"
*
* This interface is published and made available by LCP.  This interface may
* be used by any component in the PPP stack to retrieve the initialReceived,
* lastSent, and lastReceived packet.
*
*/
typedef struct LCP_CONFIG_REQS_INTERFACE
    {
    PFW_INTERFACE_OBJ interfaceObj;
    STATUS (*lcpConfigReqsGet)
	(PFW_PLUGIN_OBJ_STATE *lcpState, M_BLK_ID *initialReceived,
	M_BLK_ID *lastSent, M_BLK_ID *lastReceived);

    } LCP_CONFIG_REQS_INTERFACE;

/*******************************************************************************
* LCP_PACKET_SEND_INTERFACE - interface to ask LCP to send a particular packet
*
* Interface Name: "LCP_PACKET_SEND_INTERFACE"
*
* This interface is published and made available by LCP. This interface may
* be used by any component in the PPP stack to send an LCP packet whose type
* is one of the types supported by this interface.
* 
* However it is mainly intended for use as an interface to send Protocol Reject
* packets  when unrecognized protocols are received.
* 
* Also L2TP extensions to the PPP stack may need to use this interface
* to send other LCP packets
*/
typedef struct LCP_PACKET_SEND_INTERFACE 
    {
    PFW_INTERFACE_OBJ interfaceObj;

    void (*lcpConfigurationRequestSend)(PFW_PLUGIN_OBJ_STATE * lcpState);
    void (*lcpConfigurationAckSend)(PFW_PLUGIN_OBJ_STATE * lcpState,
						    M_BLK_ID requestPacket);

    void (*lcpTerminationRequestSend)(PFW_PLUGIN_OBJ_STATE * lcpState);
    void (*lcpTerminationAckSend)(PFW_PLUGIN_OBJ_STATE * lcpState,
						    M_BLK_ID terminationRequest);

    void (*lcpEchoRequestSend)(PFW_PLUGIN_OBJ_STATE * lcpState);
    void (*lcpEchoReplySend)(PFW_PLUGIN_OBJ_STATE * lcpState,
						    M_BLK_ID echoPacket);

    void (*lcpProtocolRejectSend)(PFW_PLUGIN_OBJ_STATE * lcpState,
						    M_BLK_ID badPacket);
    } LCP_PACKET_SEND_INTERFACE;
/*******************************************************************************
* PPP_SECURITY_CONFIG_ENTRY_INTERFACE - interface for security protocol config
*
* Interface Name: "PPP_SECURITY_CONFIG_ENTRY_INTERFACE"
*
* This interface is published and made available by LCP and is mainly intended
* for the SNMP agent to configure security protocols for a said PPP link
* identified by the "lcpState" argument.
*/
typedef struct PPP_SECURITY_CONFIG_ENTRY_INTERFACE 
    {
    PFW_INTERFACE_OBJ interfaceObj;

    int  (*pppSecurityConfigProtocolGet)     /* retrieve the auth protocol */
	    (                                /* with the specified preference.*/
	    PFW_PLUGIN_OBJ_STATE * lcpState, /* If the specified preference */
	    ULONG preference,                /* is not available the protocol */
	    UINT8 *protocol,                 /* with the next preference is */
	    UINT8 *protocolLen               /* retrived. Always the retrieved*/
	    );                               /* preference is the return value*/
	                                     /* Return value of -1 implies */
                                             /* that nothing is available */

    STATUS (*pppSecurityConfigProtocolSet)   /* add/change the specified */
	    (                                /* protocol at the given */
	    PFW_PLUGIN_OBJ_STATE * lcpState, /* preference level */
	    ULONG preference, 
	    UINT8 * protocol,
	    UINT8 protocolLen
	    );

    STATUS (*pppSecurityConfigStatusSet)     /* Only used to invalidata/delete*/
	    (                                /* the protocol at the said */
	    PFW_PLUGIN_OBJ_STATE * lcpState, /* preference level */
	    ULONG preference,
	    UINT32 configStatus
	    );
    } PPP_SECURITY_CONFIG_ENTRY_INTERFACE;

/* MP related LCP interface functions - WindNet Multilink */ 

typedef	struct LCP_BUNDLE_OPTIONS_INTERFACE 
	{
	PFW_INTERFACE_OBJ interfaceObj;
	STATUS (*lcpMpOptionsGet)
				(
				PFW_PLUGIN_OBJ_STATE 	*pLcpState, 
				LCP_BUNDLE_OPTIONS 		*pLcpBundleOptions
				);
	}LCP_BUNDLE_OPTIONS_INTERFACE;

typedef	struct LCP_PROXYLCP_INTERFACE
	{
	PFW_INTERFACE_OBJ interfaceObj;

	STATUS (*lcpProxyLcpDo)
				(
				PFW_PLUGIN_OBJ_STATE 	*pManagerLcpState, 
				PFW_PLUGIN_OBJ_STATE 	*pMemberLcpState
				);

	}LCP_PROXYLCP_INTERFACE;

#if 0 /* Not supported in this MP Release */
typedef	struct LCP_MP_TIME_INTERFACE
	{
	PFW_INTERFACE_OBJ interfaceObj;
	void (*lcpMPLinkTime)
				(
				PFW_PLUGIN_OBJ_STATE 	*pLcpState
				);
	}LCP_MP_TIME_INTERFACE;
#endif /* Not supported in this MP Release */

/* MP related LCP interface functions - WindNet Multilink */

#ifdef __cplusplus
}
#endif

#endif /* __INClcpInterfacesh */
