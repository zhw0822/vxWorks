/* framingComponentInterfaces.h - generic interfaces for framing component */

/* Copyright 1999 Wind River Systems, Inc. */

#include "copyright_wrs.h"

/*
modification history
--------------------
01b,22aug02,ijm added portId to stackResolve prototype
01a,01nov99,sj 	created
*/

#include "pfw/pfw.h"
#include "pfw/pfwInterface.h"

#ifndef __INCframingComponentInterfacesh
#define __INCframingComponentInterfacesh

#ifdef __cplusplus
extern "C" {
#endif

/*******************************************************************************
* FRAMING_COMPONENT_STACK_RESOLVE_INTERFACE - 
*
* Interface Name: "FRAMING_COMPONENT_STACK_RESOLVE_INTERFACE"
*
* This interface may be used by adapter components to resolve the stackObj
* to which a received frame must be forwarded
* 
* The "owner" parameter refers to the owner of the interface and "frame" is
* the received frame for which the connection needs to be resolved 
*/
typedef struct FRAMING_COMPONENT_STACK_RESOLVE_INTERFACE
    {
    PFW_INTERFACE_OBJ interfaceObj;
    PFW_STACK_OBJ * (*stackResolve)(PFW_PLUGIN_OBJ *owner, M_BLK_ID frame,
				    ULONG portId);
    } FRAMING_COMPONENT_STACK_RESOLVE_INTERFACE;

#ifdef __cplusplus
}
#endif

#endif /* __INCframingComponentInterfacesh */
