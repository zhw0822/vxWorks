/* mpFramingLayerInterfaces.h - mpFraming layer interface header file */

/* Copyright 1999 Wind River Systems, Inc. */

#include "copyright_wrs.h"

/*
modification history
--------------------
01b,18dec01,ak  support for dynamic assignment of MP Member Stack to Manager 
				Stack

01a,23feb01,ak 	created
*/

#include "pfw/pfw.h"
#include "pfw/pfwInterface.h"
#include "ppp/pppInterfaces.h" 

#ifndef __INCmpFramingLayerInterfacesh
#define __INCmpFramingLayerInterfacesh

#ifdef __cplusplus
extern "C" {
#endif

/* MP Framing layer interface structures */

/* MP_BUNDLE_MANAGEMENT_INTERFACE */

typedef	struct MP_BUNDLE_MANAGEMENT_INTERFACE
	{
	PFW_INTERFACE_OBJ interfaceObj;

	PFW_STACK_OBJ * (*mpOpenBundleOrAddToTheExistingBundle) 
						( 
						PFW_PLUGIN_OBJ_STATE	*pMemberLcpState,
						MP_UPCALL_FUNCTIONS		*pMpUpCalls,   
						void					*userHandle
						);

	STATUS (*mpRemoveAPortFromBundle)
				(
				PFW_PLUGIN_OBJ_STATE	*pMpFraminglayerState,
				PFW_STACK_OBJ			*pMemberStackObj
				);

	STATUS (*mpLinkAssign)
				(
				PFW_STACK_OBJ			*pMemberStackObj,
				PFW_STACK_OBJ			*pManagerStackObj
				);

	SL_LIST * (* mpBAPLinkInfoListGet) 
			(PFW_PLUGIN_OBJ_STATE	* pMpFramingLayerState);


	}MP_BUNDLE_MANAGEMENT_INTERFACE;

/* MP framing layer packet received interface */
 
typedef	struct MP_PACKET_RECEIVE_INTERFACE
	{
	PFW_INTERFACE_OBJ interfaceObj;

	STATUS (*mpFramingLayerReceive ) 
			(PFW_PLUGIN_OBJ_STATE *pMpFramingLayerState,
			 PFW_STACK_OBJ *pMemberStackObj, M_BLK_ID  packet); 

	}MP_PACKET_RECEIVE_INTERFACE;

#ifdef __cplusplus
}
#endif

#endif /* __INCmpFramingLayerInterfacesh */
