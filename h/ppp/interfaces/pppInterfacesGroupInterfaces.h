/* pppInterfacesGroupInterfaces.h - PPP IP up/down interface */

/* Copyright 2000 Wind River Systems, Inc. */

#include "copyright_wrs.h"

/*
modification history
--------------------
01c,23may00,adb  Implementation of RFC 2233 counters
01b,20may00,adb  Add counter update macro
01a,15may00,abd  created
*/

#ifndef __INCpppInterfacesGroupInterfacesh
#define __INCpppInterfacesGroupInterfacesh

#ifdef __cplusplus
extern "C" {
#endif

#include "vxWorks.h"
#include "pfw/pfw.h"
#include "pfw/pfwInterface.h"

/* m2Lib.h is always shipped */
#include "m2Lib.h"

/* These defines (M2_ctrId_*) below may not exist in m2Lib.h so we define
 * them here if they are not defined
 */
/* interface counter IDs */

#ifndef M2_ctrId_ifInOctets 
    #define M2_ctrId_ifInOctets            1
#endif

#ifndef M2_ctrId_ifInUcastPkts         
    #define M2_ctrId_ifInUcastPkts         2
#endif

#ifndef M2_ctrId_ifInNUcastPkts        
    #define M2_ctrId_ifInNUcastPkts        3
#endif

#ifndef M2_ctrId_ifInDiscards          
    #define M2_ctrId_ifInDiscards          4
#endif

#ifndef M2_ctrId_ifInErrors            
    #define M2_ctrId_ifInErrors            5
#endif

#ifndef M2_ctrId_ifInUnknownProtos     
    #define M2_ctrId_ifInUnknownProtos     6
#endif

#ifndef M2_ctrId_ifOutOctets           
    #define M2_ctrId_ifOutOctets           7
#endif

#ifndef M2_ctrId_ifOutUcastPkts        
    #define M2_ctrId_ifOutUcastPkts        8
#endif

#ifndef M2_ctrId_ifOutNUcastPkt        
    #define M2_ctrId_ifOutNUcastPkt        9
#endif

#ifndef M2_ctrId_ifOutDiscards         
    #define M2_ctrId_ifOutDiscards         10
#endif

#ifndef M2_ctrId_ifOutErrors           
    #define M2_ctrId_ifOutErrors           11
#endif

#ifndef M2_ctrId_ifInMulticastPkts     
    #define M2_ctrId_ifInMulticastPkts     12
#endif

#ifndef M2_ctrId_ifInBroadcastPkts     
    #define M2_ctrId_ifInBroadcastPkts     13
#endif

#ifndef M2_ctrId_ifOutMulticastPkts    
    #define M2_ctrId_ifOutMulticastPkts    14
#endif

#ifndef M2_ctrId_ifOutBroadcastPkts    
    #define M2_ctrId_ifOutBroadcastPkts    15
#endif

#ifndef M2_ctrId_ifHCInOctets          
    #define M2_ctrId_ifHCInOctets          16
#endif

#ifndef M2_ctrId_ifHCInUcastPkts       
    #define M2_ctrId_ifHCInUcastPkts       17
#endif

#ifndef M2_ctrId_ifHCInMulticastPkts   
    #define M2_ctrId_ifHCInMulticastPkts   18
#endif

#ifndef M2_ctrId_ifHCInBroadcastPkts   
    #define M2_ctrId_ifHCInBroadcastPkts   19
#endif

#ifndef M2_ctrId_ifHCOutOctets         
    #define M2_ctrId_ifHCOutOctets         20
#endif

#ifndef M2_ctrId_ifHCOutUcastPkts      
    #define M2_ctrId_ifHCOutUcastPkts      21
#endif

#ifndef M2_ctrId_ifHCOutMulticastPkts  
    #define M2_ctrId_ifHCOutMulticastPkts  22
#endif

#ifndef M2_ctrId_ifHCOutBroadcastPkts  
    #define M2_ctrId_ifHCOutBroadcastPkts  23
#endif

/* END OF (POSSIBLY) REDUNDANT DEFINITIONS */

/* In order to call the following macro it is assumed that locally we 
 * have defined:    PFW_PLUGIN_OBJ_STATE * pState;
 *                  int pfwAuxIfId;
 *                  PFW_INTERFACE_STATE_PAIR pfwRFC2233CountPair;
 *                  BOOL pfwRFC2233CountTest;
 * and the <state> is valid.
 */
#define RFC2233_COUNT_PAIR_GET(pState,                                         \
                               pfwAuxIfId,                                     \
                               pfwRFC2233CountPair, pfwRFC2233CountTest)       \
    do  {                                                                      \
        pfwRFC2233CountTest =                                                  \
        (((pfwAuxIfId = pfwInterfaceIdGet(pState->pluginObj->pfwObj,           \
                                    "PPP_IF_IFX_TABLE_INTERFACE")) > 0) &&     \
         (pfwInterfaceObjAndStateGetViaStackObj(pState->stackObj,              \
                                                pfwAuxIfId,                    \
                                                &pfwRFC2233CountPair) == OK)); \
        } while(0)
                                                     
/* In order to call the following macro it is assumed that locally we 
 * have defined and populated PFW_INTERFACE_STATE_PAIR pfwRFC2233CountPair; 
 * and we have defined and set BOOL RFC2233Test;
 * and ctrId is one of the 23 defined in m2Lib.h
 * and incrAmount is nonnegative.
 *
 * In most cases we would populate pfwRFC2233CountPair by calling the
 * macro RFC2233_COUNT_PAIR_GET defined above. In some cases -as in the 
 * case of the SIO adapter component which is called in interrupt context
 * and subsequently it cannot call pfwInterfaceIdGet because then
 * a semaphore take would be required (in a pfwTableTraverse)- the 
 * population of pfwRFC2233CountPair would be carried out otherwise. 
 */
#define RFC2233_COUNTER_UPDATE(pfwRFC2233CountTest, pfwRFC2233CountPair,    \
                               ctrId, incrAmount)                           \
    do  {                                                                   \
        if  (pfwRFC2233CountTest)                                           \
            {                                                               \
            (((PPP_IF_IFX_TABLE_INTERFACE *)                                \
              (pfwRFC2233CountPair.interfaceObj))->                         \
             ifIfXCounterUpdate)                                            \
            (pfwRFC2233CountPair.state, ctrId, incrAmount);                 \
            };                                                              \
        } while(0) 

/*******************************************************************************
* PPP_IF_IFX_TABLE_INTERFACE
*
* Interface Name: "PPP_IF_IFX_TABLE_INTERFACE"
*
* This interface is implemented by the END Object component.
* It enables all components of the PPP stack to update the counters and 
* variables that are included in the ifTable and ifXTable 
* of the Interfaces Group MIB as it is mandated by RFC 2233.
*
* This interface MUST be published by the pppEnd component.
*/

typedef struct pppIfIfXTableInterface
    {
    PFW_INTERFACE_OBJ interfaceObj;
    STATUS (*ifIfXCounterUpdate)(PFW_PLUGIN_OBJ_STATE * state, 
                                 UINT ctrId,
                                 /* 1 <= ctrId <= 23 
                                  * 23 M2_ctrId_'s are defined
                                  * in m2Lib.h
                                  */
                                 ULONG incrAmount);
    STATUS (*ifIfXVariableUpdate)(PFW_PLUGIN_OBJ_STATE * state, 
                                  UINT varId,
                                  /* 1 <= ctrId <= 16 
                                   * 16 M2_varId_'s are defined
                                   * in m2Lib.h
                                   */
                                  caddr_t pData);
    } PPP_IF_IFX_TABLE_INTERFACE;

/*******************************************************************************
* PPP_IF_STACK_TABLE_INTERFACE
*
* Interface Name: "PPP_IF_STACK_TABLE_INTERFACE"
*
* This interface is implemented by the END Object component.
* It enables all components of the PPP stack to conform to
* the ifStackTable part of RFC 2233.
*
* This interface MUST be published by the pppEnd component.
*/

typedef struct pppIfStackTableInterface
    {
    PFW_INTERFACE_OBJ interfaceObj;
    STATUS (*ifStackStatusSet)(PFW_PLUGIN_OBJ_STATE * state, 
                               int rowStatus);
                               /* 1 <= rowStatus <= 6
                                * 6 ROW_(status) values
                                * are defined in m2Lib.h
                                * as RFC 2233 dictates.
                                */
    } PPP_IF_STACK_TABLE_INTERFACE;

#ifdef __cplusplus
}
#endif

#endif /* __INCpppInterfacesGroupInterfacesh */
