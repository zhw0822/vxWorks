/* pppControlProtocolInterface.h - PPP Protocol interface definitions */

/* Copyright 1999 Wind River Systems, Inc. */

#include "copyright_wrs.h"

/*
modification history
--------------------
01d,29sep00,sj  merging with TOR2_0-WINDNET_PPP-CUM_PATCH_2
01c,01aug00,adb  Merging with openstack view
01b,11jul00,md  added AUTHEN_ATTRIBUTES_INTERFACE
01a,22oct99,sj 	created
*/

#include "ppp/kstart.h"
#include "ppp/kppp.h"
#include "pfw/pfw.h"
#include "pfw/pfwInterface.h"
#include "netBufLib.h"

#ifndef __INCpppControlProtocolInterfaceh
#define  __INCpppControlProtocolInterfaceh

#ifdef __cplusplus
extern "C" {
#endif

/* PPP Control Layer phases for Link Establishment and Teardown */

typedef enum
    {
    DEAD_PHASE,
    ESTABLISH_PHASE,
    AUTHENTICATE_PHASE,
    NETWORK_PHASE,
    TERMINATE_PHASE,

    TOTAL_PPP_CONTROL_PHASES
    }PPP_CONTROL_PHASE;

/*******************************************************************************
* CONTROL_PROTOCOL_INTERFACE -
*
* Interface Name: "CONTROL_PROTOCOL_INTERFACE"
*
* This interface is published by the PPP Control Layer in the system and is
* bound to and made available by all the control protocols managed by the
* Control Layer.
* 
* This interface is intended for the Control Layer to control the state of
* the Control Layer protocols during link establishment and teardown.
*
* Protocols in this layer are divided into phases depending on the Control
* Layer phase in which the protocol is active
*/
typedef struct CONTROL_PROTOCOL_INTERFACE
    {
    PFW_INTERFACE_OBJ interfaceObj;
    PPP_CONTROL_PHASE (*pppPhaseGet)();
    PPP_STATE         (*pppStateGet)(PFW_PLUGIN_OBJ_STATE *);
    void (*executeStateMachine)
			    (
			    PFW_PLUGIN_OBJ_STATE *pluginState,
			    PPP_EVENT            pppEvent,
			    M_BLK_ID             packet
			    );
    } CONTROL_PROTOCOL_INTERFACE;

 
/*******************************************************************************
* AUTHEN_ATTRIBUTES_INTERFACE - interface to authentication attributes
*
* Interface Name: "AUTHEN_ATTRIBUTES_INTERFACE"
*
* This interface is published and made available by the control layer.
* This interface may be used by any component in the PPP stack to
* retrieve the authentication attributes
*
*/
typedef struct AUTHEN_ATTRIBUTES_INTERFACE
    {
    PFW_INTERFACE_OBJ interfaceObj;
    STATUS (*authenAttributesGet)
	(PFW_PLUGIN_OBJ_STATE * controlLayerState,
	UINT16 *authenType, char *authenName,
	char *authenChallange, UINT8 *authenID, char *authenResponse);
    } AUTHEN_ATTRIBUTES_INTERFACE;

#ifdef __cplusplus
}
#endif

#endif /*  __INCpppControlProtocolInterfaceh */

