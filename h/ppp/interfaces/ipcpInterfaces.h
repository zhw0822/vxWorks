/* ipcpInterfaces.h - IPCP published interfaces */

/* Copyright 1999 Wind River Systems, Inc. */

#include "copyright_wrs.h"

/*
modification history
--------------------
01a,15jan2000,sgv 	created
*/

#include "pfw/pfw.h"
#include "pfw/pfwInterface.h"

#ifndef __INCipcpInterfacesh
#define __INCipcpInterfacesh

#ifdef __cplusplus
extern "C" {
#endif

/*******************************************************************************
* PPP_IPCP_MIB_INTERFACE - PPP MIB status interface
*
* Interface Name: "PPP_IPCP_MIB_INTERFACE"
*
* This interface is published and made available by IPCP.
*/
typedef struct PPP_IPCP_MIB_INTERFACE
    {
    PFW_INTERFACE_OBJ interfaceObj;
    ULONG (*ipcpOperStatusGet)(PFW_PLUGIN_OBJ_STATE * state);
    ULONG (*ipcpLocalToRemoteCompressionProtocolGet)(
	PFW_PLUGIN_OBJ_STATE * state);
    ULONG (*ipcpRemoteToLocalCompressionProtocolGet)(
	PFW_PLUGIN_OBJ_STATE * state);
    ULONG (*ipcpRemoteMaxSlotIdGet)( PFW_PLUGIN_OBJ_STATE * state);
    ULONG (*ipcpLocalMaxSlotIdGet)( PFW_PLUGIN_OBJ_STATE * state);
    ULONG (*ipcpConfigAdminStatusGet) (PFW_PLUGIN_OBJ_STATE * state);
    ULONG (*ipcpConfigCompressionGet) (PFW_PLUGIN_OBJ_STATE * state);
    STATUS (*ipcpConfigAdminStatusSet) (PFW_PLUGIN_OBJ_STATE * state, 
	ULONG value);
    STATUS (*ipcpConfigCompressionSet) (PFW_PLUGIN_OBJ_STATE * state,
	ULONG value);
    } PPP_IPCP_MIB_INTERFACE;


#ifdef __cplusplus
}
#endif

#endif /* __INCipcpInterfacesh */
