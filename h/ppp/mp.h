/* mp.h - MP header file */

/* Copyright 1999 Wind River Systems, Inc. */

#include "copyright_wrs.h"

/*
modification history
--------------------
01k,22oct02,sj  splitting mp.h into mp.h and mpP.h
01j,25jun02,rp  adding missed multilink patch 2 changes
01i,28may02,rvr fixed build warnings (teamf1)
01h,19apr02,jr	packed the structures for ARM fixes.
01g,20mar02,rp  adding newline at end of file
01f,08oct01,ak  Added PPP_WITH_MAC_HEADER_LENGTH
01e,19sep01,ak  added MAX_CLUSTER_SIZE
01d,06sep01,ijm adding remoteMRU. Corrected size of local and remote_user
                name arrays.
01c,30aug01,ak	Added MP_PROFILE_STRING_SIZE
01b,27aug01,ak	Added one variable in the MP_BUNDLE_SENDING_END_CLASS for 
				supporting rotating the member links for non-fragmented
				packets
01a,19feb01,ak 	derived from routerware source base
*/

#ifndef __INCmph
#define __INCmph

#ifdef __cplusplus
extern "C" {
#endif

/* includes */

#include "vxWorks.h"
#include "ppp/kppp.h"
#include "ppp/kstart.h"
#include "sllLib.h"
#include "pfw/pfw.h"
#include "pfw/pfwStack.h"
#include "pfw/pfwComponent.h"
#include "pfw/pfwLayer.h"
#include "pfw/pfwTable.h"
#include "pfw/pfwEvent.h"
#include "pfw/pfwMemory.h"
#include "pfw/pfwInterface.h"
#include "rngLib.h"


/* defines */

#ifndef MAXIMUM_DISCRIMINATOR_ADDRESS_LENGTH
#define MAXIMUM_DISCRIMINATOR_ADDRESS_LENGTH 	200
#endif

/* Bundle Identifier structures */

typedef struct MP_DISCRIMINATOR_CLASS
	{
	BYTE	discriminatorClass; 
	BYTE	length;
	BYTE	address[MAXIMUM_DISCRIMINATOR_ADDRESS_LENGTH];
	} MP_DISCRIMINATOR_CLASS;

typedef struct MP_BUNDLE_IDENTIFIER
	{
	BOOL					link_failed_to_be_authenticated;
	MP_DISCRIMINATOR_CLASS	local_station_discriminator;
	MP_DISCRIMINATOR_CLASS	remote_station_discriminator;
	char 					local_user_name[NAME_SIZE];
	char 					remote_user_name[NAME_SIZE];
	} MP_BUNDLE_IDENTIFIER;

/* LCP Bundle options related structures */

typedef struct LCP_BUNDLE_OPTIONS
	{
	ULONG 					localMRRU;
	ULONG 					remoteMRRU;
	ULONG 					localMRU;			
	ULONG 					remoteMRU;
	BOOL 					use_short_sequence_number_in_tx_end;
	BOOL					use_short_sequence_number_in_rx_end;
	MP_BUNDLE_IDENTIFIER 	bundle_identifier;
	USHORT 					localAuthProtocol; 
	USHORT 					remoteAuthProtocol;
	BOOL					is_link_in_sending_end;
	BOOL					is_link_in_receiving_end;
	USHORT					local_link_discriminator;
	USHORT					remote_link_discriminator;
	} LCP_BUNDLE_OPTIONS;


typedef enum
    {
    MP_BUNDLE_CLOSED_STATE =    0x00,
    MP_BUNDLE_OPENED_STATE =    0x01,
    MP_BUNDLE_INVALID_STATE =   0xff
    }MP_BUNDLE_STATE;

#ifdef __cplusplus
}
#endif

#endif /* __INCmph */

 
