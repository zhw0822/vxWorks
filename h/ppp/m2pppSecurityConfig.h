/* m2pppSecurityConfig.h - PPP SNMP agent interface library header */

/* Copyright 1999 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*
modification history
--------------------
01a,06jan00,sj   created.
*/

#include "pfw/pfw.h"
#include "pfw/pfwTable.h"
#include "pfw/pfwMemory.h"
#include "ppp/m2pppLib.h"
#include "ppp/m2pppSecuritySecretsLib.h"
#include "ppp/interfaces/lcpInterfaces.h"

#ifndef __INCm2pppSecurityConfigh
#define  __INCm2pppSecurityConfigh

#ifdef __cplusplus
extern "C" {
#endif

/* typedefs */

typedef struct PPP_SECURITY_CONFIG_ENTRY_DATA 
    {
    PFW_PLUGIN_OBJ_STATE      * state;
    PFW_INTERFACE_OBJ         * interface;
    UINT32                      link;
    UINT32                      preference;
    PPP_SECURITY_PROTOCOL       protocol;
    PPP_SECURITY_SECRETS_STATUS status;
    }PPP_SECURITY_CONFIG_ENTRY_DATA;

extern STATUS m2pppSecurityConfigRegister (PFW_OBJ * pfwObj);
extern STATUS m2pppSecurityConfigUnregister (PFW_OBJ * pfwObj);
extern STATUS m2pppSecurityConfigEntryLookup (UINT32 oidLen, UINT32 * oid,
					PPP_SECURITY_CONFIG_ENTRY_DATA * data,
					M2_PPP_ENTRY_MATCH_TYPE match);
extern STATUS m2pppSecurityConfigEntrySet(PPP_SECURITY_CONFIG_ENTRY_DATA *data);

#ifdef __cplusplus
}
#endif

#endif /*  __INCm2pppSecurityConfigh */
