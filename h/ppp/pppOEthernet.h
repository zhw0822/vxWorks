/* pppOEthernet.h - PPP over Ethernet interface */

/* Copyright 1984-2005 Wind River Systems, Inc. */

/*
modification history
--------------------
01e,28jan05,ijm Added pppoePeerMacAdrsGet function, SPR#73541
01d,16jan05,ijm As part of PPPoE Fix TCP MSS enhancement,SPR#105644,
                added pppOE_fixTcpAdvMss parameter
01c,18apr01,ijm correct name is pppOEServiceNameDelete (not ..NameDel)
01b,08nov99,sj  Init to Create
01a,07jul99,sj 	created
*/

#ifndef __INCpppOEtherneth
#define __INCpppOEtherneth

#ifdef __cplusplus
extern "C" {
#endif

/* includes */

/* typedefs */

/* defines */

#define PPPOE_AC_MODE     0x1	/* specifies Access Concentrator mode */
#define PPPOE_HOST_MODE   0x2	/* specifies PPPOE Host/Client mode */

/*                    Profile Parameters
 *                   --------------------
 *        NAME                              VALUES/EXPLANATION
 *       ------                            --------------------
 * "pppOE_connectionMode"     "0x1" OR "0x2": for AC OR HOST mode respectively
 *
 * "pppOE_discRetries"        Minimum number of retries during DISCOVERY phase;
 *                            Default is FOUR retries
 *
 * "pppOE_minDiscTimeout"     Minimum/Initial timeout value in seconds for
 *                            retries during the discovery phase; Default is
 *                            ONE second
 *
 * "pppOE_svcName"            Name of service: This is service requested in the
 *                            case of a HOST/CLIENT and is the offered service
 *                            in the case of an AC
 *
 * "pppOE_acName"             Access Concentrator name to use when making offers
 *
 * "pppOE_vendorInfo"         Vendor specific information used during Discovery
 *
 * "pppOE_fixTcpAdvMss"       Enable monitoring outgoing PPPoE packets to reduce
 *                            advertised MSS TCP option for TCP SYN packets
 *                            only, if MSS is larger than 1452 bytes. Maximum
 *                            PPPoE MSS: ETHERMTU - TCPIP header - PPPoE header
 *                            = 1500 - 40 - 8 = 1452 bytes.
 *
 *                            Monitoring may be necessary to correct a
 *                            potential problem for hosts using PPPoE target
 *                            as a gateway to the Internet. PPPoE target does
 *                            not have a problem because TCP for PPPoE target
 *                            gets the correct MTU for the PPPoE interface
 *                            (1492) and advertises an MSS of 1452 bytes.
 *                            However, TCP for hosts on the LAN will use
 *                            ETHERMTU (1500) and advertise an MSS of 1460
 *                            bytes. Packets sent by remote hosts to clients
 *                            behind the PPPoE gateway may be dropped since
 *                            TCP packets always have the "Do not fragment
 *                            bit set", and some routers may not generate
 *                            the appropriate ICMP "Fragmentation needed"
 *                            message to alert the remote host to reduce MSS.
 *
 *                            Enabling this parameter to make up for a
 *                            mis-configured router in the Internet,
 *                            reduces performance on the target.  This
 *                            parameter is provided to satisfy customer
 *                            requests, but its use is strongly discouraged.
 *
 *                            Since the use of PATH MTU discovery is
 *                            strongly recommended for IPv6 nodes it is less
 *                            likely that this problem will affect TCP/IPv6
 *                            packets, and since processing IPv6 headers
 *                            would impact performance even more, enabling
 *                            this parameter will process TCP/IPv4 packets only.
 *
 *
 * NOTE:
 * The PPP_O_ETHERNET pluginObj within a framework instance can be initialized
 * to either PPPOE_HOST_MODE OR PPPOE_AC_MODE but NOT both. This is done by
 * specifying the appropriate value for the 'operatingMode' parameter for
 * pppOEthernetCreate.
 *
 * The value for  "pppOE_connectionMode" profile parameter must always match 
 * the 'operatingMode' for all stacks in the Framework.
 */

/* User Interfaces */

extern STATUS pppOEthernetCreate (PFW_OBJ * , UINT32 maxServices,
							UINT32 operatingMode);
extern STATUS pppOEthernetDelete (PFW_OBJ * );

extern STATUS pppOEServiceNameAdd (PFW_PROFILE_OBJ *, char * pSvcName,
					 PFW_STACK_OBJ_CALLBACKS * callbacks,
					 void * userHandle);

extern STATUS pppOEServiceNameDelete ( PFW_OBJ *,char * pSvcName);
extern void   pppOESessionsShow ( PFW_OBJ * pfw);
extern STATUS pppoePeerMacAdrsGet (PFW_STACK_OBJ * pStackObj,
                                   unsigned char * etherAddr);

#ifdef __cplusplus
}
#endif

#endif /* __INCpppOEtherneth */
