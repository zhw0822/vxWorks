/* pppMPFramingLayer.h - MP Framing Layer header file */

/* Copyright 1999 Wind River Systems, Inc. */

#include "copyright_wrs.h"

/*
modification history
--------------------
01a,01feb01,ak  created
*/

#ifndef __INCpppMpFramingLayerh
#define __INCpppMpFramingLayerh

#ifdef __cplusplus
extern "C" {
#endif

#include "vxWorks.h"
#include "pfw/pfw.h"

STATUS mpFramingLayerCreate	(PFW_OBJ * pfw, UINT mpSendQueueDepth,		
	UINT mpReceiveQueueDepth, UINT mpSendTaskPriority, UINT mpReceiveTaskPriority);

STATUS mpFramingLayerDelete (PFW_OBJ *pfw);

#ifdef __cplusplus
}
#endif

#endif /* __INCpppMpFramingLayerh */
