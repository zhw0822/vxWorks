/* buf.h - file system buffer cache interface */

/* Copyright 2005 Wind River Systems, Inc */

/*
modification history
--------------------
01d,21jul05,jlk  added BUF_USED_ONCE priority macro
01c,12apr05,act  add a field to hold errors; change buf_done to take a buf and
                 an error
01b,17mar05,act  export buf_done() function
01a,16mar05,act  add buf_initmountpoint prototype
 */

#ifndef __INCbufh
#define __INCbufh

#include "sys/queue.h"
#include "semLib.h"
#include "drv/xbd/bio.h"

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Type-name for "logical block number".  Buffers are named by
 * <vnode, logical block number> pairs, so that a file system that
 * wants to get "block 3" of a file (vnode), you just get <vp,3>.
 * (The block size is up to the user of the buffer code.)
 */
typedef unsigned long long lblkno_t;	/* ??? signed? */

struct ucred;
struct mount;

LIST_HEAD(bhashhdr, buf);
TAILQ_HEAD(bfreehdr, buf);

/*
 * Note that a buffer can be both free and on a vnode.  This occurs when
 * the buffer is cacheing valid data.
 *
 * Buffers are taken off their free list while "owned", but are always
 * on a hash chain (kept in the mount point) based on their buffer hash
 * value.
 *
 * Each buffer also has an I/O semaphore that is taken when I/O is
 * started, and given only when the I/O is finished.  Thus, if you
 * can get the I/O semaphore, I/O is done (or was never started).
 * The code to start the I/O should always be able to get the semaphore
 * without waiting (otherwise it is a repeated start w/o first finishing).
 *
 * The B_BUSY bit is really the same information as in b_mutex: if the
 * buffer is owned, it is busy; if not, it is not-busy.  Only the block
 * getting and releasing functions should manipulate this flag.  B_BUSY
 * is documented as "I/O in progress", but this is a white lie, it just
 * means a task has the buffer locked.
 */
struct buf {
	SEM_ID		b_mutex;	/* mutex to lock buffer while owned */
	SEM_ID		b_iosem;	/* I/O semaphore (binary sem) */
	int		b_flags;	/* see below */

	/* don't need b_size yet - HRFS buffers are all one size per mount */
	int		b_bsize;	/* size of data */
	void		*b_data;	/* this buffer's data (shared w/bio) */
	struct bio	*b_bio;		/* ### need more, eventually? */
	int		b_error;	/* error for buf_wait */

#ifdef notyet
	void		(*b_done)(struct buf *); /* for B_CALL */
#endif

	struct vnode	*b_vp;		/* this buffer's vnode */
	lblkno_t	b_lblkno;	/* and its logical block number */

	int		b_priority;	/* cache priority */
	LIST_ENTRY(buf)	b_hash;		/* hash chain */
	LIST_ENTRY(buf)	b_vnbufs;	/* chain of buffers on this vnode */
	TAILQ_ENTRY(buf) b_freelist;	/* chain if free */
};

/*
 * Flags for b_flags.
 *
 * The names are from NetBSD; the bit-value numbers do not match though.
 */
/*	B_AGE				-- move to age queue when I/O done */
#define	B_ASYNC		0x00000001	/* Start I/O, do not wait */
/*	B_BAD				-- bad block revectoring in progress */
#define	B_BUSY		0x00000002	/* I/O in progress */
/*	B_SCANNED			-- already pushed during sync */
/*	B_CALL		0x00000004	-- call b_iodone from buf_done */
#define	B_DELWRI	0x00000008	/* buffer has delayed-write data */
/*	B_DIRTY				-- dirty page (VM system info, n/a) */
#define	B_DONE		0x00000010	/* I/O completed */
#define	B_EINTR		0x00000020	/* I/O was interrupted (for NFS) */
#define	B_ERROR		0x00000040	/* I/O error occurred */
/*	B_GATHERED	0x00000080	-- LFS: already in a segment */
#define	B_INVAL		0x00000100	/* does not contain valid info */
/*	B_LOCKED			-- locked in core (not reusable) */
/*	B_NOCACHE			-- do not cache block after use */
/*	B_CACHE				-- bread() found us in the cache */
/*	B_PHYS				-- I/O to user space */
/*	B_RAW				-- set by physio() for raw transfers */
#define	B_READ		0x00000200	/* read buffer */
/*	B_TAPE				-- magnetic tape I/O */
/*	B_WANTED			-- process wants this buffer */
#define	B_WRITE		0x00000400	/* write buffer */
/*	B_XXX				-- debugging flag */
/*	B_VFLUSH			-- buffer is being synced */

/*
 * Buffer caching "levels" (priorities).  A buffer that contains some
 * valid data goes on one of the "higher-level" queues, in LRU fashion,
 * by putting released buffers at the end of the queue and taking
 * buffers off the front of the queue.
 *
 * The ones in the "trash" queue by definition have no valid data
 * (the code itself does not really need this distinction at the
 * moment, but it seems reasonable to keep it).
 */
#define	BUF_TRASH	0	/* "trash" queue: no valid data */
#define	BUF_USED_ONCE	1	/* blocks that are used just once */
#define	BUF_ORDINARY	2	/* ordinary data/transactions */
#define	BUF_META_LOW	3	/* low priority metadata */
#define	BUF_META_HIGH	4	/* high priority metadata */
#define BUF_NQUEUES	5	/* number of free-list queues */

#define	buf_setpriority(bp, pri) ((bp)->b_priority = (pri))

/*
 * Buffer hashing, per mount-point.  This hash function probably needs
 * tuning.  Note that the current BUFHASH definition depends on BHSIZE
 * being a power of two.
 */
#define BUF_BHSIZE 32
#define BUFHASH(vp, lbn) \
	(((((long)(vp) >> 8) + (int)(lbn))) & (BUF_BHSIZE - 1))

void	buf_initmountpoint(struct mount *);
int	buf_makebuffers(struct mount *, int, int, struct bio *);
void	buf_deletebuffers(struct mount *);

/*
 * getblk() is BSD-compatible, except that we ignore the two "unused"s
 * (so they had better always be 0!  in BSD they control sleep() behavior).
 *
 * bread() does a getblk() and then reads the underlying data if the
 * block was not already in cache.  bwrite() writes the data, and there
 * are several variations on the theme.  In general, once you getblk()
 * or bread(), you own that <vnode,lbn> pair until you brelse() or
 * bwrite() it to give it back.
 *
 * For file systems that move underlying data, we provide buf_moveblk(),
 * to move a buffer (which you must own) from one <vp,lbn> pair to
 * another.  The new vnode may be the same as the old one.  It is OK to
 * move the lbn to a cached LBN (this just invalidates the old cached
 * copy) -- it has to be, since the caller can't tell which LBNs are
 * cached -- but it is an error to move it to a <vp,lbn> pair that is
 * currently both cached and owned.  (This means you have a bug in your
 * file system, since you think you are working with that <vp,lbn> pair
 * on behalf of this task or some other task.)  In this case you will
 * get EBUSY as a return value.  (??? maybe we should just panic?)
 *
 * Both vnodes must be on the same mount point (if not, you get an
 * error, again).
 *
 * We also provide buf_swapdata, which simply swaps the underlying
 * data of two buffers (you must own both) without even looking at
 * anything else.
 */
struct buf *buf_getblk(struct vnode *, lblkno_t, int);
int	buf_moveblk(struct buf *, struct vnode *, lblkno_t);
void	buf_swapdata(struct buf *, struct buf *);

#define getblk(vp, lbn, size, unused1, unused2) buf_getblk(vp, lbn, size)

int	bread(struct vnode *, lblkno_t, int, struct ucred *, struct buf **);
#ifdef notyet
int	breada(struct vnode *, lblkno_t, int, lblkno_t, int,
		struct ucred *, struct buf **);
int	breadn(struct vnode *, lblkno_t, int, lblkno_t *, int *, int,
		struct ucred *, struct buf **);
#endif
int	bwrite(struct buf *);	/* write block, wait for result */
void	buf_startwrite(struct buf *); /* begin write; caller still owns buf */
#ifdef notyet
void	bdwrite(struct buf *);	/* delayed write: write after timeout */
#endif
void	bawrite(struct buf *);	/* async write: start write but don't wait */
void	brelse(struct buf *);	/* release buffer to free list, no I/O */
int	buf_wait(struct buf *);	/* wait for I/O, and return errno (0=OK) */
void	buf_done(struct buf *, int);

#ifdef __cplusplus
}
#endif

#endif /* __INCbufh */
