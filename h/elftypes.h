/* elftypes.h - typedefs of elf types for vxWorks */

/* 
 * Copyright (c) 2003-2005 Wind River Systems, Inc. 
 * 
 * The right to copy, distribute, modify, or otherwise make use of this software
 * may be licensed only pursuant to the terms of an applicable Wind River
 * license agreement. 
 */

/*
modification history
--------------------
01d,25jul05,v_r  Updated copyright.
01c,27mar04,jn   Make elf.h and elftypes.h shared with the host (target
                 server)
01b,20jun03,nwd  Deliver of Tornado 2.2 Cumulative Patch 1
01a,30apr03,jn   derived from Diab elftypes.h header file 
                 /vobs/rome_diab_lib/dlib/include/include.unx/
		 elftypes.h@@/main/1
		 and /.wind_vxw_h/river/target/h/vxWorks.h@@/main/tor2/1 
		 (for elements of Wind River coding standards)
*/

#ifndef __INCelftypesh
#define __INCelftypesh

#ifdef __cplusplus
extern "C" {
#endif

/**************	Exported data, types and macros	*****************/

typedef unsigned long   Elf32_Addr;
typedef unsigned short  Elf32_Half;
typedef unsigned long   Elf32_Off;
typedef long            Elf32_Sword;
typedef unsigned long   Elf32_Word;

#ifdef __cplusplus
}
#endif

#endif /* __INCelftypesh */
