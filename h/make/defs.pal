# defs.pal - PAL related definitions for making libraries
#
# Copyright 2003, Wind River Systems, Inc.
#
# modification history
# --------------------
# 01d,29apr05,md  added CPU variant make depend
# 01c,15mar04,dlk Set DEFS_PAL. Fix modification history.
# 01b,.......,c_c Added a definition for cleaning the CPU variant dependent
#                 libraries.
# 01a,.......,c_c written from  rules.library v 02b
#

## CPU variant builds:
# List of CPU variant builds: CPU_VARIANT_TAG_LIST
# This list is computed for the current CPU/TOOL combinaison. Note that CPU
# variant build(s) are started as part of the 'Default' rule (see rules.library)

# Defining DEFS_PAL non-empty is needed in defs.library by the option build
# logic to ensure we don't skip any directory including this file,
# particularly src/arch.
DEFS_PAL = TRUE

include $(TGT_DIR)/h/make/defs.library

ifneq ($(CPU_VARIANT_LIST),)
CPU_VARIANT_TAG_LIST_TMP1 = 				\
    $(foreach cpuVariantTag, $(CPU_VARIANT_LIST),	\
	$(filter $(CPU)$(TOOL),				\
	    $(CPU_VARIANT_CPU_TOOL_LIST$(cpuVariantTag)))$(cpuVariantTag))
CPU_VARIANT_TAG_LIST_TMP2 = $(filter $(CPU)$(TOOL)%, 	\
				$(CPU_VARIANT_TAG_LIST_TMP1))
CPU_VARIANT_TAG_LIST = $(patsubst $(CPU)$(TOOL)%, %,	\
			    $(CPU_VARIANT_TAG_LIST_TMP2))

# Generate list of make commands for CPU variant builds: CPU_VARIANT_MAKE_CMD

ifneq  ($(CPU_VARIANT_TAG_LIST),)
CPU_VARIANT_MAKE_CMD = echo "Build of CPU variants"
CPU_VARIANT_MAKE_CMD += $(foreach cpuVariant, $(CPU_VARIANT_TAG_LIST), 	       \
    $(CMD_SEPARATOR) $(MAKE) CPU=$(CPU) TOOL=$(TOOL) LIB_DIR_TAG=$(cpuVariant) \
	VX_ARCHIVE_LIST="$(VX_ARCHIVE_LIST$(cpuVariant))" TARGET=$(TARGET) \
	NO_RECURSE=TRUE)

CPU_VARIANT_MAKE_CLEAN = echo "Removal of CPU variants"
CPU_VARIANT_MAKE_CLEAN += $(foreach cpuVariant, $(CPU_VARIANT_TAG_LIST), 	       \
    $(CMD_SEPARATOR) $(MAKE) CPU=$(CPU) TOOL=$(TOOL) LIB_DIR_TAG=$(cpuVariant) \
	VX_ARCHIVE_LIST="$(VX_ARCHIVE_LIST$(cpuVariant))" TARGET=$(TARGET) \
	NO_RECURSE=TRUE rclean) 

CPU_VARIANT_MAKE_DEPEND = echo "Build of CPU variant depend files"
CPU_VARIANT_MAKE_DEPEND += $(foreach cpuVariant, $(CPU_VARIANT_TAG_LIST), 	       \
    $(CMD_SEPARATOR) $(MAKE) CPU=$(CPU) TOOL=$(TOOL) LIB_DIR_TAG=$(cpuVariant) \
	VX_ARCHIVE_LIST="$(VX_ARCHIVE_LIST$(cpuVariant))" TARGET=$(TARGET) \
	NO_RECURSE=TRUE rdepend) 
endif
endif

