/* fcntl.h - standard header */

/*
 * Copyright (c) 1991-1992, 2005 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use 
 * of this software may be licensed only pursuant to the terms 
 * of an applicable Wind River license agreement. 
 */ 

/*
modification history
--------------------
01i,06aug05,pad  Fixed usage of _WRS_KERNEL macro and updated copyright.
01h,03aug05,hya  added fcntl()
01g,09may05,hya  changed user APIs for POSIX compliance.
01f,22sep92,rrr  added support for c++
01e,18sep92,smb  added prototypes for open and creat.
01d,29jul92,smb  rearranged for the stdio library.
01c,04jul92,jcf  cleaned up.
01b,26may92,rrr  the tree shuffle
01a,05dec91,rrr  written.
*/

#ifndef __INCfcntlh
#define __INCfcntlh

#ifdef __cplusplus
extern "C" {
#endif

#include <types/vxANSI.h>
#include <types/vxTypes.h>
#include <sys/fcntlcom.h>

#define	O_NDELAY	_FNDELAY	/* Non-blocking I/O (4.2 style) */

int		fcntl (int fd, int cmd, ...);

#ifdef _WRS_KERNEL

extern int	open (const char *, int, int);
extern int	creat (const char *, int);

#else

extern int      open (const char *, int, ...);
extern int      creat (const char *, mode_t );

#endif	/* _WRS_KERNEL */

#ifdef __cplusplus
}
#endif

#endif /* __INCfcntlh */
