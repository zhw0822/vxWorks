/* wdbArchIfLib.h - header file for arch-specific routines needed by wdb */

/* Copyright 1984-2003 Wind River Systems, Inc. */

/*
modification history
--------------------
01e,03dec03,elg  Fix compilation errors: remove some conflicting declarations.
01d,25apr02,jhw  Added C++ support (SPR 76304).
01c,15jun95,ms	removed _sigCtxIntLock prototype
01b,05apr95,ms	new data types.
01a,21dec94,ms  written.
*/

#ifndef __INCwdbArchIfLibh
#define __INCwdbArchIfLibh

#ifdef __cplusplus
extern "C" {
#endif

/* includes */

#include "wdb/wdb.h"
#include "wdb/wdbRegs.h"
#include "intLib.h"

/* function prototypes */

extern int   	_sigCtxSave	(WDB_IU_REGS *pContext);
extern void	_sigCtxRtnValSet (WDB_IU_REGS *pContext, int val);

#ifdef __cplusplus
}
#endif

#endif  /* __INCwdbArchIfLibh */

