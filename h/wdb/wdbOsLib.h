/* wdbOsLib.h - WDB vxWorks specific header */

/* Copyright 2003 Wind River Systems, Inc. */

/*
modification history
--------------------
01f,23mar04,elg  Macros to get information in context are now OS specific.
01e,12sep03,tbu  Added inclusion of netinet/in_systm.h
01d,29aug03,elg  Redefine GOPHER_STRTOL to do strtoul() (fix SPR #90223).
		 Include vxWorks.h first (fix SPR #90034).
01c,14jul03,pch  Conditional compilation cleanup
01b,06may03,elg  Fix compilation error when Altivec is supported.
01a,03jan03,elg  Written.
*/

#ifndef	__INCwdbOsLibh
#define	__INCwdbOsLibh

#ifdef	__cplusplus
extern "C" {
#endif

/*
DESCRIPTION

The vxWorks specific header contains definitions that cannot be used by other
OSes.
*/

/* includes */

#include "vxWorks.h"	/* this file must be included first */

#include "types/vxANSI.h"
#include "rpc/types.h"

#include "ctype.h"
#include "limits.h"
#include "sioLib.h"
#include "stdlib.h"
#include "string.h"
#include "regs.h"
#include "dspLib.h"
#include "taskHookLib.h"
#include "intLib.h"
#include "fppLib.h"

#if !defined (_WRS_PAL_COPROC_LIB)
#ifdef	_WRS_ALTIVEC_SUPPORT
#include "altivecLib.h"
#endif	/* _WRS_ALTIVEC_SUPPORT */
#endif	/* ! _WRS_PAL_COPROC_LIB */

/* definitions */

/* OS definitions */

#define	WDB_KERNEL_ID		-1	/* kernel ID in WDB agent */

/* access to registers */

#define	WDB_PC(ptr)		(ptr)->reg_pc
#define	WDB_FP(ptr)		(ptr)->reg_fp
#define	WDB_SP(ptr)		(ptr)->reg_sp

/* debug info */

/* on vxWorks, PC does not need to be changed */

#define	_WRS_ADJUST_PC_AFTER_SW_BP(pRegs)

/* gopher routines */

#define	GOPHER_STRTOL		strtoul
#define	GOPHER_WRITE_SCALAR	gopherWriteScalar

/* macros to get and set ID from context structure */

#define	WDB_CTX_ID_GET(pCtx)	((pCtx)->numArgs > 0 ? (pCtx)->args [0] : 0)

#define	WDB_CTX_SYS_INFO_SET(pArgs)					\
	{								\
	(pArgs) [0] = 0;						\
	(pArgs) [1] = 0;						\
	}

#define	WDB_CTX_TASK_INFO_SET(pArgs, tid, pid)				\
	{								\
	(pArgs) [0] = (TGT_INT_T) (tid);				\
	(pArgs) [1] = (TGT_INT_T) (pid);				\
	}

#define	WDB_CTX_RTP_INFO_SET(pArgs, pid)				\
	{								\
	(pArgs) [0] = (TGT_INT_T) (pid);				\
	(pArgs) [1] = 0;						\
	}

#define	WDB_CTX_INFO_COPY(pArgs, pCtx)					\
	{								\
	int	ix;							\
									\
	for (ix = 0; ix < (pCtx)->numArgs; ix++)			\
	    (pArgs) [ix] = (pCtx)->args [ix];				\
	for (ix = (pCtx)->numArgs; ix < WDB_CTX_MAX_ARGS; ix++)		\
	    (pArgs) [ix] = 0;						\
	}

/* globals */
	
IMPORT void	wdbMbufInit		(struct mbuf *, UINT32,
					 CL_BLK *, UINT32);	
IMPORT void	wdbSioTest		(SIO_CHAN *, int, char);
	
#ifdef	__cplusplus
}
#endif	/* __cplusplus */

#endif	/* __INCwdbOsLibh */
