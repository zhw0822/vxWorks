/* wdbTaskLib.h - header file for task support in WDB agent */

/*
 * Copyright (c) 2003-2005 Wind River Systems, Inc.  
 *
 * The right to copy, distribute, modify or otherwise make use  of this 
 * software may be licensed only pursuant to the terms of an applicable Wind 
 * River license agreement.
 */

/*
modification history
--------------------
01b,12jul05,bpn  Added childProcessStop field to WDB_TASK_INFO.
01a,16dec03,elg  Written.
*/

#ifndef __INCwdbTaskLibh
#define __INCwdbTaskLibh

#ifdef __cplusplus
extern "C" {
#endif

/* defines */

#define WDB_INFO(p)	((WDB_TASK_INFO *) (((WIND_TCB *) (p))->pWdbInfo))

/* data types */

typedef struct wdb_task_info		/* task information for WDB */
    {
    struct
    	{
	void *	wdb1;
	void *	wdb2;
	} wdbEvtList;			/* dll for event */
    int		taskBpAddr;		/* current breakpoint address */
    int		taskPc;			/* task program counter */
    int		taskFp;			/* task frame pointer */
    int		taskSp;			/* task stack pointer */
    VOIDFUNCPTR	wdbExitHook;		/* task exit hook */
    UINT32	tid;			/* task ID */
    UINT32	ctxType;		/* context stopped by breakpoint */
    UINT32	status;			/* status of context that hit BP */
    BOOL	childProcessStop;	/* TRUE if CPS is set for that task */
    } WDB_TASK_INFO;

/* function prototypes */

extern UINT32	wdbTaskDeleteHookAdd	(UINT32 tid, VOIDFUNCPTR hook);
extern UINT32	wdbTaskCreateHookAdd	(VOIDFUNCPTR hook,
					 WDB_ACTION_TYPE action);

extern UINT32	wdbTaskBpAdd (WDB_CTX * pCtx, WDB_ACTION * pAction,
			      INSTR * addr, UINT32 count,
      			      UINT32 type, UINT32 * pBpId);

#ifdef __cplusplus
}
#endif

#endif /* __INCwdbTaskLibh */
