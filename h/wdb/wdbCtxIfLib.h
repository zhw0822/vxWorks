/* wdbCtxIfLib.h - header file for the WDB agent context runtime interface */

/* Copyright 1984-2004 Wind River Systems, Inc. */

/*
modification history
--------------------
01f,13apr04,elg  Remove obsolete type WDB_REG_SET_TYPE.
01e,23mar04,elg  Move macros to get information on context in wdbOsLib.h
01d,08mar04,elg  Change WDB_CTX structure.
01c,12sep03,tbu  Moved inclusion of netinet/in_systm.h to wdbOsLib.h
01b,22may03,elg  Modify context creation hook add API to add action type.
01a,11mar03,elg  Written.
*/

#ifndef __INCwdbCtxIfLibh
#define __INCwdbCtxIfLibh

#ifdef __cplusplus
extern "C" {
#endif

/* includes */

#include "wdb/wdb.h"
#include "wdb/wdbRegs.h"

/* data types */

typedef struct wdb_ctx_if
    {
    UINT32 (*create)		(WDB_CTX_CREATE_DESC * pCtx, UINT32 * pRetVal);
    UINT32 (*delete)		(WDB_CTX_KILL_DESC * pCtx);
    UINT32 (*suspend)		(int ctxId);
    UINT32 (*resume)		(int ctxId);
    UINT32 (*attach)		(WDB_CTX * pCtx);
    UINT32 (*detach)		(WDB_CTX * pCtx);
    STATUS (*stop)		(int ctxId);
    STATUS (*cont)		(int ctxId);
    UINT32 (*regsGet)		(WDB_CTX * pCtx, UINT32 type, char ** ppRegs);
    UINT32 (*regsSet)		(WDB_CTX * pCtx, UINT32 type, char * pRegs);
    UINT32 (*createHookAdd)	(VOIDFUNCPTR hook, WDB_ACTION_TYPE actionType);
    UINT32 (*deleteHookAdd)	(UINT32 pCtx, VOIDFUNCPTR hook);
    STATUS (*enterHookAdd)	(WDB_CTX * pCtx, VOIDFUNCPTR hook);
    STATUS (*exitHookAdd)	(WDB_CTX * pCtx, VOIDFUNCPTR hook);
    UINT32 (*statusGet)		(WDB_CTX * pCtx, UINT32 * pStatus);
    STATUS (*idVerify)		(int ctxId);
    void * (*isAttached)	(UINT32 memCtx);
    } WDB_CTX_IF;

/* context specific macros */

#define	WDB_RT_CTX(pCtx)	pWdbRtIf->pWdbCtxIf [(pCtx)->contextType]
#define	WDB_RT_ENTRY(pEntry)						\
	pWdbRtIf->pWdbCtxIf [(pCtx)->context.contextType]

#define WDB_RT_CTX_CREATE(pCtxCreate, pCid)				\
	(WDB_RT_CTX (pCtxCreate) == NULL ?				\
	    WDB_ERR_NO_RT_CTX :						\
	    ((WDB_RT_CTX (pCtxCreate))->create == NULL ?		\
	        WDB_ERR_NO_RT_PROC :					\
		(WDB_RT_CTX (pCtxCreate))->create ((pCtxCreate), (pCid))))

#define WDB_RT_CTX_DELETE(pCtx)						\
	(WDB_RT_ENTRY (pCtx) == NULL ?					\
	    WDB_ERR_NO_RT_CTX :						\
	    ((WDB_RT_ENTRY (pCtx))->delete == NULL ?			\
	        WDB_ERR_NO_RT_PROC :					\
		(WDB_RT_ENTRY (pCtx))->delete (pCtx)))

#define WDB_RT_CTX_SUSPEND(pCtx)					\
	(WDB_RT_CTX (pCtx) == NULL ?					\
	    WDB_ERR_NO_RT_CTX :						\
	    ((WDB_RT_CTX (pCtx))->suspend == NULL ?			\
	        WDB_ERR_NO_RT_PROC :					\
		((WDB_RT_CTX (pCtx))->suspend (WDB_CTX_ID_GET (pCtx)) == OK ? \
		    WDB_OK : WDB_ERR_RT_ERROR)))

#define WDB_RT_CTX_RESUME(pCtx)						\
	(WDB_RT_CTX (pCtx) == NULL ?					\
	    WDB_ERR_NO_RT_CTX :						\
	    ((WDB_RT_CTX (pCtx))->resume == NULL ?			\
	        WDB_ERR_NO_RT_PROC :					\
		((WDB_RT_CTX (pCtx))->resume (WDB_CTX_ID_GET (pCtx)) == OK ? \
		    WDB_OK : WDB_ERR_RT_ERROR)))

#define WDB_RT_CTX_ATTACH(pCtx)						\
	(WDB_RT_CTX (pCtx) == NULL ?					\
	    WDB_ERR_NO_RT_CTX :						\
	    ((WDB_RT_CTX (pCtx))->attach == NULL ?			\
	        WDB_ERR_NO_RT_PROC :					\
		(WDB_RT_CTX (pCtx))->attach (pCtx)))

#define WDB_RT_CTX_DETACH(pCtx)						\
	(WDB_RT_CTX (pCtx) == NULL ?					\
	    WDB_ERR_NO_RT_CTX :						\
	    ((WDB_RT_CTX (pCtx))->detach == NULL ?			\
	        WDB_ERR_NO_RT_PROC :					\
		(WDB_RT_CTX (pCtx))->detach (pCtx)))

#define WDB_RT_CTX_STOP(pCtx)						\
	(WDB_RT_CTX (pCtx) == NULL ?					\
	    WDB_ERR_NO_RT_CTX :						\
	    ((WDB_RT_CTX (pCtx))->stop == NULL ?			\
	        WDB_ERR_NO_RT_PROC :					\
		((WDB_RT_CTX (pCtx))->stop (WDB_CTX_ID_GET (pCtx)) == OK ? \
		    WDB_OK : WDB_ERR_RT_ERROR)))

#define WDB_RT_CTX_CONT(pCtx)						\
	(WDB_RT_CTX (pCtx) == NULL ?					\
	    WDB_ERR_NO_RT_CTX :						\
	    ((WDB_RT_CTX (pCtx))->cont == NULL ?			\
	        WDB_ERR_NO_RT_PROC :					\
		((WDB_RT_CTX (pCtx))->cont (WDB_CTX_ID_GET (pCtx)) == OK ? \
		    WDB_OK : WDB_ERR_RT_ERROR)))

#define WDB_RT_CTX_REGS_GET(pCtx, regType, pRegs)			\
	(WDB_RT_CTX (pCtx) == NULL ?					\
	    WDB_ERR_NO_RT_CTX :						\
	    ((WDB_RT_CTX (pCtx))->regsGet == NULL ?			\
	        WDB_ERR_NO_RT_PROC :					\
		(WDB_RT_CTX (pCtx))->regsGet ((pCtx), (regType), (pRegs))))

#define WDB_RT_CTX_REGS_SET(pCtx, regType, pRegs)			\
	(WDB_RT_CTX (pCtx) == NULL ?					\
	    WDB_ERR_NO_RT_CTX :						\
	    ((WDB_RT_CTX (pCtx))->regsSet == NULL ?			\
	        WDB_ERR_NO_RT_PROC :					\
		(WDB_RT_CTX (pCtx))->regsSet ((pCtx), (regType), (pRegs))))

#define WDB_RT_CTX_CREATE_HOOK_ADD(pCtx, hook, actionType)		\
	(WDB_RT_CTX (pCtx) == NULL ?					\
	    WDB_ERR_NO_RT_CTX :						\
	    ((WDB_RT_CTX (pCtx))->createHookAdd == NULL ?		\
	        WDB_ERR_NO_RT_PROC :					\
		(WDB_RT_CTX (pCtx))->createHookAdd ((hook), (actionType))))

#define WDB_RT_CTX_DELETE_HOOK_ADD(pCtx, hook)				\
	(WDB_RT_CTX (pCtx) == NULL ?					\
	    WDB_ERR_NO_RT_CTX :						\
	    ((WDB_RT_CTX (pCtx))->deleteHookAdd == NULL ?		\
	        WDB_ERR_NO_RT_PROC :					\
		(WDB_RT_CTX (pCtx))->deleteHookAdd (WDB_CTX_ID_GET (pCtx), \
						    (hook))))

#define WDB_RT_CTX_ENTER_HOOK_ADD(pCtx, hook)				\
	(WDB_RT_CTX (pCtx) == NULL ?					\
	    WDB_ERR_NO_RT_CTX :						\
	    ((WDB_RT_CTX (pCtx))->enterHookAdd == NULL ?		\
	        WDB_ERR_NO_RT_PROC :					\
		(WDB_RT_CTX (pCtx))->enterHookAdd (WDB_CTX_ID_GET (pCtx), \
						   (hook))))

#define WDB_RT_CTX_EXIT_HOOK_ADD(pCtx, hook)				\
	(WDB_RT_CTX (pCtx) == NULL ?					\
	    WDB_ERR_NO_RT_CTX :						\
	    ((WDB_RT_CTX (pCtx))->exitHookAdd == NULL ?		\
	        WDB_ERR_NO_RT_PROC :					\
		(WDB_RT_CTX (pCtx))->exitHookAdd (WDB_CTX_ID_GET (pCtx), \
						  (hook))))

#define WDB_RT_CTX_STATUS_GET(pCtx, pStatus)				\
	(WDB_RT_CTX (pCtx) == NULL ?					\
	    WDB_ERR_NO_RT_CTX :						\
	    ((WDB_RT_CTX (pCtx))->statusGet == NULL ?			\
	        WDB_ERR_NO_RT_PROC :					\
		(WDB_RT_CTX (pCtx))->statusGet ((pCtx), (pStatus))))

#define WDB_RT_CTX_ID_VERIFY(pCtx)					\
	(WDB_RT_CTX (pCtx) == NULL ?					\
	    WDB_ERR_NO_RT_CTX :						\
	    ((WDB_RT_CTX (pCtx))->idVerify == NULL ?			\
	        WDB_ERR_NO_RT_PROC :					\
		(WDB_RT_CTX (pCtx))->idVerify (WDB_CTX_ID_GET (pCtx))))

#define	WDB_RT_CTX_IS_ATTACHED(pCtx)					\
	(WDB_RT_CTX (pCtx) == NULL ?					\
	    NULL :							\
	    ((WDB_RT_CTX (pCtx))->isAttached == NULL ?			\
	    	NULL :							\
		(WDB_RT_CTX (pCtx))->isAttached (WDB_CTX_ID_GET (pCtx))))

#ifdef __cplusplus
}
#endif

#endif  /* __INCwdbCtxIfLibh */
