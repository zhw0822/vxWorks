/* wdbRegs.h - header file for register layout as view by the WDB agent */

/* Copyright 1984-2004 Wind River Systems, Inc. */

/*
modification history
--------------------
01l,13apr04,elg  Remove obsolete type WDB_REG_SET_TYPE.
01k,05feb04,aeg  added include of altivecLib.h
01j,27feb03,elg  Merge file with BSD.
01j,03sep02,dtr  Adding wdb external SPE function declarations.
01i,25apr02,jhw  Added C++ support (SPR 76304).
01h,13jul01,kab  Cleanup for merge to mainline
01g,13apr01,pcs  Add Altivec Support.
01f,15mar01,pcs  Change include filename altiVecLib.h to altivecLib.h
01e,29jan01,dtr  Adding altivec register support.
01e,28feb00,frf  Add SH4 support for T2
01d,31jul98,kab  Added DSP register support.
01d,12feb99,dbt  fixed __STDC__ use.
01c,17dec96,ms	 WDB now uses FP_CONTEXT instead of FPREG_SET (SPR 7654).
01b,25may95,ms	 defined register objects.
01a,03may95,ms   written.
*/

#ifndef __INCwdbRegsh
#define __INCwdbRegsh

#ifdef __cplusplus
extern "C" {
#endif

/* includes */

#include "wdb/dll.h"
#include "wdb/wdb.h"

/* data types */

typedef REG_SET WDB_IU_REGS;

typedef FP_CONTEXT WDB_FPU_REGS;
typedef DSP_CONTEXT WDB_DSP_REGS;

typedef struct
    {
    dll_t		node;
    UINT32		regSetType;		/* type of register set */
    void		(*save) (void);		/* save current regs */
    void		(*load) (void);		/* load saved regs */
    void		(*get)  (char ** ppRegs); /* get saved regs */
    void		(*set)  (char *pRegs);	/* change saved regs */
    } WDB_REG_SET_OBJ;

#ifdef  _WRS_ALTIVEC_SUPPORT

#include "altivecLib.h"

typedef ALTIVEC_CONTEXT WDB_ALTIVEC_REGS;

typedef struct
    {
     /*  NOTE:  altivecContext has to be the first element in the struct.
             altivecContext has to be 16 byte aligned. Anyone who uses
             this object should define it as a pointer and memalign to
             get it 16 byte aligned.
     */
    WDB_ALTIVEC_REGS    altivecContext; /* the hardware context */
    WDB_REG_SET_OBJ     regSet;         /* generic register set */
    } ALTIVEC_REG_SET_OBJ;

#endif /* _WRS_ALTIVEC_SUPPORT */

#ifdef  _WRS_SPE_SUPPORT

#include "speLib.h"

typedef SPE_CONTEXT WDB_SPE_REGS;

typedef struct
    {
     /*  NOTE:  speContext has to be the first element in the struct.
         speContext should be _CACHE_ALIGN_SIZE aligned 
         (which is always > 8 bytes). Anyone who uses
         this object should define it as a pointer and memalign to
         get it _CACHE_ALIGN_SIZE byte aligned.
     */
    WDB_SPE_REGS    speContext; /* the hardware context */
    WDB_REG_SET_OBJ     regSet;         /* generic register set */
    } SPE_REG_SET_OBJ;

#endif /* _WRS_SPE_SUPPORT */

/* externs */

IMPORT WDB_IU_REGS *	pWdbExternSystemRegs;

/* function prototypes */

extern  WDB_REG_SET_OBJ * wdbAltivecLibInit (void);
extern  WDB_REG_SET_OBJ * wdbSpeLibInit (void);
extern	WDB_REG_SET_OBJ * wdbFpLibInit		(void);
extern	WDB_REG_SET_OBJ * wdbDspLibInit		(void);
extern	void              wdbExternRegSetObjAdd (WDB_REG_SET_OBJ *pRegSet);

#ifdef __cplusplus
}
#endif

#endif  /* __INCwdbRegsh */
