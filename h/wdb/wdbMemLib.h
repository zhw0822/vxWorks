/* wdbMemLib.h - WDB memory services header file */

/* Copyright 2003 Wind River Systems, Inc. */

/*
modification history
--------------------
01d,30sep03.elg  Fix build warnings.
01c,29sep03,tbu  Changed wdbMemScan usage (now returns a status)
01b,16sep03,tbu  Added _wdbMemCksum
01a,06jan03,elg  Written.
*/

#ifndef __INCwdbMemLibh
#define	__INCwdbMemLibh

#ifdef	__cplusplus
extern "C" {
#endif	/* __cplusplus */

/* externs */

extern UINT32	(*_wdbMemCopy) (char * pSource, char * pDestination, 
				UINT32 nBytes, UINT32 width);
extern UINT32	(*_wdbMemFill) (char * addr, UINT32 nBytes, UINT32 pattern, 
				UINT32 width);
extern UINT32	(*_wdbMemRead) (char * pSource, char ** pDestination, 
				UINT32 nBytes);
extern UINT32	(*_wdbMemScan) (char * pStart, char * pEnd, char * pattern, 
				UINT32 nBytes, int step, BOOL match, 
				TGT_ADDR_T * pAdrr);
extern UINT32	(*_wdbMemCksum) (char * addr, UINT32 nBytes, UINT32 * pCksum);

extern UINT32	wdbMemTest (char * addr, UINT32 nBytes, UINT32 accessType, 
			    UINT32 width);
extern UINT32	wdbCksum (UINT16 * pData, UINT32 nBytes, UINT32 init,
			  BOOL moreData);

#ifdef	__cplusplus
}
#endif	/* __cplusplus */

#endif	/* __INCwdbMemLibh */
