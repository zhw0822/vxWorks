/* romfsDrv.h - header file for romfsDrv.c */

/* Copyright 2003 Wind River Systems, Inc. */

/*
modification history
--------------------
01a,14may03,md  created.
*/

#ifndef __INCromfsDrvh
#define __INCromfsDrvh

#ifdef __cplusplus
extern "C" {
#endif

#include "blkIo.h"

#define ROMFS_BLOCK_SIZE	512	/* block size for I/O operations */

/* function declarations */

#if defined(__STDC__) || defined(__cplusplus)

extern BLK_DEV *romfsBlkDevCreate (char *addr, int size, int offset);

#else

extern BLK_DEV *romBlkDevCreate ();

#endif	/* __STDC__ */

#ifdef __cplusplus
}
#endif

#endif /* __INCromfsDrvh */
