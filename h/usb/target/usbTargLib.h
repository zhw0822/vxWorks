/* usbTargLib.h - Defines interface to usbTargLib.c */

/* Copyright 2004 Wind River Systems, Inc. */

/*
Modification history
--------------------
02d,03aug04,mta  Modification History Changes
02c,19jul04,ami  Coding Convention Changes
02b,18may04,mta  Modification to USB_TARG_STATUS_GET_FUNC
02a,30mar04,ami  changes made according to USB 2.0 Extn Design doc
01e,18sep01,wef  merge from wrs.tor2_0.usb1_1-f for veloce
01d,07may01,wef  changed module number to be (module sub num << 8) |
                 M_usbPeriphLib
01c,02may01,wef  changed module number to be M_<module> + M_usbPeriphLib
01b,05dec00,wef  moved Module number defs to vwModNum.h - add this
                 to #includes
01a,04aug99,rcb  First.
*/

/*
DESCRIPTION

Defines the interface to usbTargLib.c.	usbTargLib.c implements a hardware-
independent interface to USB target controller drivers.
*/

#ifndef __INCusbTargLibh
#define __INCusbTargLibh

#ifdef	__cplusplus
extern "C" {
#endif

/* includes */

#include "usb/usbHandleLib.h"
#include "usb/usb.h"
#include "vwModNum.h"
#include "usb/target/usbHalCommon.h"
#include "usb/target/usbHalLib.h"
#include "usb/usbListLib.h"


/* defines */

#define TARG_TCD_SIG	((UINT32) 0xaaaa00cd) /* signature for Tcd */
#define TARG_PIPE_SIG	((UINT32) 0xaaaa000e) /* signature for Pipe */

/* managements codes */

#define TARG_MNGMT_ATTACH	0x0000  /* TARG Attach Management Code */
#define TARG_MNGMT_DETACH	0x0001  /* TARG Detach Management Code */
#define TARG_MNGMT_DISCONNECT	0x0002  /* TARG Disconnect Management Code */
#define TARG_MNGMT_BUS_RESET  	0x0003  /* TARG Bus Reset Management Code */
#define TARG_MNGMT_SUSPEND	0x0004  /* TARG Suspend Management Code */
#define TARG_MNGMT_RESUME	0x0005  /* TARG Resume Managemnt Code */

/* usbTargLib error codes */

/*
 * USB errnos are defined as being part of the USB peripheral Module, as are
 * all vxWorks module numbers, but the USB Module number is further divided
 * into sub-modules.  Each sub-module has upto 255 values for its own error
 * codes
 */

#define USB_TARG_SUB_MODULE  1

#define M_usbTargLib 	( (USB_TARG_SUB_MODULE  << 8) | M_usbPeriphLib )

#define usbTargErr(x)	(M_usbTargLib | (x))

#define S_usbTargLib_BAD_PARAM		usbTargErr(1)	/* bad parameter */
#define S_usbTargLib_BAD_HANDLE 	usbTargErr(2)	/* bad handle */
#define S_usbTargLib_OUT_OF_MEMORY	usbTargErr(3)	/* out of memory */
#define S_usbTargLib_OUT_OF_RESOURCES	usbTargErr(4)	/* Out of resource */
#define S_usbTargLib_NOT_IMPLEMENTED	usbTargErr(5)	/* not Implemented */
#define S_usbTargLib_GENERAL_FAULT	usbTargErr(6)	/* general fault */
#define S_usbTargLib_NOT_INITIALIZED	usbTargErr(7)	/* uninilialized */
#define S_usbTargLib_CANNOT_CANCEL	usbTargErr(8)	/* cannot cancel */
#define S_usbTargLib_TCD_FAULT		usbTargErr(9)	/* TCD related fault */
#define S_usbTargLib_ENDPOINT_IN_USE	usbTargErr(10)	/* endpoint in use */
#define S_usbTargLib_APP_FAULT		usbTargErr(11)	/* application fault */


/* typedefs */

typedef GENERIC_HANDLE USB_TARG_CHANNEL,*pUSB_TARG_CHANNEL;
typedef GENERIC_HANDLE USB_TARG_DESCR,	*pUSB_TARG_DESCR;
typedef GENERIC_HANDLE USB_TARG_PIPE,	*pUSB_TARG_PIPE;

/* caller-supplied target callbacks */

/* target management callback */

typedef STATUS (*USB_TARG_MANAGEMENT_FUNC)
    (
    pVOID param,
    USB_TARG_CHANNEL targChannel,
    UINT16 mngmtCode,			    /* management code */
    pVOID pContext
    );


/* control pipe callbacks */

typedef STATUS (*USB_TARG_FEATURE_CLEAR_FUNC)
    (
    pVOID		param,		/* TCD specific parameter */
    USB_TARG_CHANNEL	targChannel,	/* target channel */
    UINT8		requestType,	/* request type */
    UINT16		feature,	/* feature to clear */
    UINT16		uIndex		/* wIndex */
    );

typedef STATUS (*USB_TARG_FEATURE_SET_FUNC)
    (
    pVOID		param,		/* TCD Specific Parameter */
    USB_TARG_CHANNEL	targChannel,	/* target channel */
    UINT8		requestType,	/* request type */
    UINT16		feature,	/* feature to set */
    UINT16		index		/* wIndex */
    );

typedef STATUS (*USB_TARG_CONFIGURATION_GET_FUNC)
    (
    pVOID		param,		/* TCD specific parameter */
    USB_TARG_CHANNEL	targChannel,	/* target channel */
    pUINT8		pConfiguration	/* pointer to configuration value */
    );

typedef STATUS (*USB_TARG_CONFIGURATION_SET_FUNC)
    (
    pVOID		param,		/* TCD specific paramter */
    USB_TARG_CHANNEL	targChannel,	/* target channel */
    UINT8		configuration	/* configuration value */
    );

typedef STATUS (*USB_TARG_DESCRIPTOR_GET_FUNC)
    (
    pVOID		param,		/* TCD specific parameter */
    USB_TARG_CHANNEL	targChannel,	/* target channel */
    UINT8		requestType,	/* request type */
    UINT8		descriptorType,	/* descriptor type */
    UINT8		descriptorIndex,/* descriptor index */
    UINT16		languageId,	/* language ID */
    UINT16		length,		/* length of descriptor */
    pUINT8		pBfr,		/* pointer to buffer */
    pUINT16		pActLen		/* length of data sent */
    );

typedef STATUS (*USB_TARG_DESCRIPTOR_SET_FUNC)
    (
    pVOID		param,		/* TCD Specific Parameter */
    USB_TARG_CHANNEL	targChannel,	/* target channel */
    UINT8		requestType,	/* request type */
    UINT8		descriptorType,	/* descriptor type */
    UINT8		descriptorIndex,/* descriptor index */
    UINT16		languageId,	/* language ID */
    UINT16		length,		/* length of descriptor */
    pUINT8		pBfr,		/* pointer to buffer */
    pUINT16		pActLen		/* length of data in buffer */
    );

typedef STATUS (*USB_TARG_INTERFACE_GET_FUNC)
    (
    pVOID		param,		/* TCD specific parameter */
    USB_TARG_CHANNEL	targChannel,	/* target channel */
    UINT16		interface,	/* interface number */
    pUINT8		pAlternateSetting /* alternate setting */
    );

typedef STATUS (*USB_TARG_INTERFACE_SET_FUNC)
    (
    pVOID		param,		/* TCD specific parameter */
    USB_TARG_CHANNEL	targChannel,	/* target channel */
    UINT16		interface,	/* interface number */
    UINT8		alternateSetting /* alternate setting */
    );

typedef STATUS (*USB_TARG_STATUS_GET_FUNC)
    (
    pVOID		param,		/* TCD specific parameter */
    USB_TARG_CHANNEL	targChannel,	/* target channel */
    UINT16		requestType,	/* request type */
    UINT16		index,		/* wIndex */
    UINT16		length,		/* length */
    pUINT8		pBfr		/* pointer to buffer */
    );

typedef STATUS (*USB_TARG_ADDRESS_SET_FUNC)
    (
    pVOID		param,		/* TCD specific parameter */
    USB_TARG_CHANNEL	targChannel,	/* target channel */
    UINT16		deviceAddress	/* device address */
    );

typedef STATUS (*USB_TARG_SYNCH_FRAME_GET_FUNC)
    (
    pVOID		param,		/* TCD specific parameter */
    USB_TARG_CHANNEL	targChannel,	/* target channel */
    UINT16		endpoint,	/* endpoint */
    pUINT16		pFrameNo	/* frame number obtained */
    );

typedef STATUS (*USB_TARG_VENDOR_SPECIFIC_FUNC)
    (
    pVOID		param,		/* TCD specific parameter */
    USB_TARG_CHANNEL	targChannel,	/* target channel */
    UINT8		requestType,	/* type of request */
    UINT8		request,	/* request name */
    UINT16		value,		/* wValue */
    UINT16		index,		/* wIndex */
    UINT16		length		/* wLength */
    );


typedef struct usbTargCallbackTable  /* USB_TARG_CALLBACK_TABLE */
    {
    /* device management callbacks */

    USB_TARG_MANAGEMENT_FUNC	mngmtFunc; /* management callback */

    /* Control pipe callbacks */

    USB_TARG_FEATURE_CLEAR_FUNC		featureClear;	/* feature clear */
    USB_TARG_FEATURE_SET_FUNC		featureSet;	/* feature set */
    USB_TARG_CONFIGURATION_GET_FUNC	configurationGet; /*configuration get*/
    USB_TARG_CONFIGURATION_SET_FUNC	configurationSet; /*configuration set*/
    USB_TARG_DESCRIPTOR_GET_FUNC	descriptorGet;	/* descriptor get */
    USB_TARG_DESCRIPTOR_SET_FUNC	descriptorSet;	/* descriptor set */
    USB_TARG_INTERFACE_GET_FUNC		interfaceGet;	/* interface get */
    USB_TARG_INTERFACE_SET_FUNC		interfaceSet;	/* interface set */
    USB_TARG_STATUS_GET_FUNC		statusGet;	/* status get */
    USB_TARG_ADDRESS_SET_FUNC		addressSet;	/* address set */
    USB_TARG_SYNCH_FRAME_GET_FUNC	synchFrameGet;	/* frame get */
    USB_TARG_VENDOR_SPECIFIC_FUNC	vendorSpecific;	/* vendor specific */
    } USB_TARG_CALLBACK_TABLE, *pUSB_TARG_CALLBACK_TABLE;
    
/* Data Structures of the USB Target Library */

typedef struct targTcd		/* TARG_TCD */
    {
    LINK		tcdLink;	/* link to the next TCD */
    USB_TARG_CHANNEL	targChannel;	/* handle to TCD */
    pUSB_TARG_CALLBACK_TABLE pCallbacks;/* table of application callback*/
    pVOID		callbackParam;  /* callback defined callback parameter*/
    USBHAL_TCD_NEXUS	tcdNexus;	/* TCD Nexus Pointer */
    UINT16		speed;		/* speed of operation */
    USB_TARG_PIPE	defaultControlPipeIN;/* handle to default control In */
                                             /* pipe */
    USB_TARG_PIPE	defaultControlPipeOUT;/*handle to default control Out*/
                                              /* pipe */
    USB_ERP		setupErp;	/* to handle setup transfers */
    USB_ERP		defaultControlErp;/* erp used to transfer to/from */
					  /* default pipe */
    UINT8		setupBfr [sizeof (USB_SETUP)];/*for receiving */
						      /* setup packet */
    UINT8 		dataBfr [USB_MAX_DESCR_LEN]; /* sending/receiving data */
						     /* to/from default control */
						     /* pipe */
    BOOL		controlErpPending;/* flag indicating setup erp pending*/
    MUTEX_HANDLE	tcdMutex;	/* for synchronization operations */
    } TARG_TCD, *pTARG_TCD;


typedef struct targPipe		/* TARG_PIPE */
    {
    USB_TARG_PIPE	pipeHandle;	/* pipe handle information */
    pVOID		pHalPipeHandle;	/* HAL specific pipe handle */
    pTARG_TCD		pTargTcd;       /* pointer to targ_tcd data structure*/
    } TARG_PIPE, *pTARG_PIPE;

/* function declarations */

/*  Function declarations for initializing & uninitializing */

extern STATUS usbTargInitialize (void);


extern STATUS usbTargShutdown (void);


extern STATUS usbTargTcdAttach (USB_TCD_EXEC_FUNC tcdExecFunc, pVOID tcdParam,
                                pUSB_TARG_CALLBACK_TABLE pCallbacks,
                                pVOID callbackParam,
                                pUSB_TARG_CHANNEL pTargChannel);


extern STATUS usbTargTcdDetach (USB_TARG_CHANNEL targChannel);

extern STATUS usbTargEnable (USB_TARG_CHANNEL targChannel);

extern STATUS usbTargDisable (USB_TARG_CHANNEL targChannel);

/* Function declarations for Pipe Specific Request */

extern STATUS usbTargPipeCreate (USB_TARG_CHANNEL targChannel,
                                 pUSB_ENDPOINT_DESCR pEndpointDesc,
                                 UINT16 uConfigurationValue, UINT16 uInterface,
                                 UINT16 uAltSetting,
                                 pUSB_TARG_PIPE pPipeHandle);

extern STATUS usbTargPipeDestroy (USB_TARG_PIPE pipeHandle);

extern STATUS usbTargTransfer (USB_TARG_PIPE pipeHandle, pUSB_ERP pErp);

extern STATUS usbTargTransferAbort (USB_TARG_PIPE pipeHandle, pUSB_ERP pErp);

extern STATUS usbTargPipeStatusSet (USB_TARG_PIPE pipeHandle, UINT16 state);

extern STATUS usbTargPipeStatusGet (USB_TARG_PIPE pipeHandle, pUINT8 pBuf);

/* Function declarations for default pipe requests */

extern STATUS usbTargControlResponseSend (USB_TARG_CHANNEL targChannel,
                                          UINT16 bfrLen, pUINT8 pBfr);


extern STATUS usbTargControlPayloadRcv (USB_TARG_CHANNEL targChannel,
                                        UINT16 bfrLen, pUINT8 pBfr,
                                        ERP_CALLBACK userCallback);

extern STATUS usbTargControlStatusSend (USB_TARG_CHANNEL targChannel);

/* Function declarations for Device Control & Status Information */

extern STATUS usbTargCurrentFrameGet (USB_TARG_CHANNEL targChannel,
                                      pUINT16 pFrameNo);

extern STATUS usbTargSignalResume (USB_TARG_CHANNEL targChannel);

extern STATUS usbTargDeviceFeatureSet (USB_TARG_CHANNEL targChannel,
                                       UINT16 ufeatureSelector,
                                       UINT8    uTestSelector);

extern STATUS usbTargDeviceFeatureClear (USB_TARG_CHANNEL targChannel,
                                         UINT16   ufeatureSelector);

#ifdef	__cplusplus
}
#endif

#endif	/* __INCusbTargLibh */
