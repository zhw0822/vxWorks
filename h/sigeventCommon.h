/* sigeventCommon.h - sigevent structure, needed by several header files */

/* Copyright 2003 Wind River Systems, Inc. */

/*
modification history
--------------------
01a,29sep03,ans  written.
*/

#ifndef __INCsigeventcommonh
#define __INCsigeventcommonh

#ifdef __cplusplus
extern "C" {
#endif

#define SIGEV_NONE              0x0
#define SIGEV_SIGNAL            0x1
#define SIGEV_TASK_SIGNAL       0x2
#define SIGEV_TIMER_SIGNAL      0x4
#define SIGEV_TIMER_TASK_SIGNAL 0x8
#define SIGEV_MASK              0xf

#define IS_SIGEV_TYPE_SUPPORTED(notify) ((notify) & SIGEV_MASK || !(notify)) 
#define IS_SIGEV_SIGNAL_TASK(notify) ((notify) & \
        (SIGEV_TASK_SIGNAL | SIGEV_TIMER_TASK_SIGNAL))


union sigval
    {
    int			sival_int;
    void		*sival_ptr;
    };

struct sigevent
    {
    int                 sigev_signo;  /*  signal number */  
    union sigval        sigev_value;  /*  signal value */
    int                 sigev_notify; /*  notification type */
    };

#ifdef __cplusplus
}
#endif

#endif /* __INCsigeventcommonh */
