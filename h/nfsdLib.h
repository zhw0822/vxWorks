/* nfsdLib.h - Network File System Server library header */

/* Copyright 1994-2004 Wind River Systems, Inc. */

/*
modification history
--------------------
01d,01jul04,dlk  Make this header include nfsdCommon.h and nfsd.h;
                 remove other contents.
01c,21apr94,jmm  more cleanup of prototypes; added NFSD_ARGUMENT
01b,20apr94,jmm  added new prototypes
01a,31mar94,jmm  written.
*/

#ifndef __INCnfsdLibh
#define __INCnfsdLibh

#if 0
#error nfsdLib.h is obsolete. Use nfsdCommon.h and/or nfsd.h.
#endif

#include <nfsdCommon.h>
#include <nfsd.h>

#endif /* __INCnfsdLibh */
