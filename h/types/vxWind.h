/* vxWind.h - WIND object types header file */

/* Copyright 1999-2004 Wind River Systems, Inc. */

/*
modification history
--------------------
01z,26aug05,gls  updated VX_SEMAPHORE_SIZE (SPR #111547)
01y,11aug05,yvp  Added VX_xxx_SIZE macros.
01x,03feb05,kk   added OBJ_HANDLE typedef
01w,29mar04,dcc  moved enum definitions to vxWindCommon.h
01v,01mar04,dcc  modified handleTypeStart to be 100.
01u,02dec03,bwa  removed all MsgC/S references
01t,04nov03,kk   added windSdClass for shared data
01s,29oct03,kam  ISR Object code inspection mods
01r,30sep03,ans  Added windTimerClass
01q,04may03,kam  added windIsr class definition
01p,04jun03,bwa  Added 'windMsgCClass' and 'windMsgSClass' definitions.
01o,11jul03,rec  merge in vxWind.h@@/main/base6_itn1_kernel-int/5
01n,20jun03,bwa  added typedefs of MSG_C_ID and MSG_S_ID
01m,04jun03,bwa  Added 'windMsgCClass' and 'windMsgSClass' definitions.
01l,09may03,to   replace PD with RTP.
01k,28mar03,dcc  ported from AE1.1
01j,13mar01,ahm  Added windSetClass (SPR# 64431)
01i,19sep00,dbs  add hamHandleType to handleType enum
01h,24aug00,bwa  added #ifndef _ASMLANGUAGE
01g,18jul00,bwa  put typedefs of _ID here and removed them from their
                 respective files (SPR #32543)
01f,12jun00,aeg  added windOmsClass to windObjClassType.
01e,19jan00,jkf  added cbio handle type
01d,18jan00,jgn  add new handle type for environments
01c,07jan00,aeg  added handleType and windObjClassType enums.
01b,27jan99,aeg  changed OBJ_ID typedef to void *
01a,07jan99,mak  written.
*/

/*
DESCRIPTION
This file typedefs the opaque types for WIND kernel objects.  They
are typedef-ed here so that other header files can declare functions
containing more than one of these types as parameters without having
to include each other - which could cause circular header file
inclusion problems.

The Wind object type and handle type enumerations are also defined here.
*/

#ifndef __INCvxWindh
#define __INCvxWindh

#ifdef __cplusplus
extern "C" {
#endif

#include "vxWindCommon.h"     /* windObjClassType and handleType enum defs */

#define VX_SEMAPHORE_SIZE 108
#define VX_MSG_Q_SIZE     152
#define VX_WDOG_SIZE      92

#ifndef _ASMLANGUAGE

typedef int			OBJ_HANDLE;

typedef void *			OBJ_ID;
typedef struct wind_class *	CLASS_ID;
typedef struct wind_group *	GROUP_ID;

typedef struct msg_q *		MSG_Q_ID;
typedef struct semaphore *	SEM_ID;
typedef struct wind_rtp *	RTP_ID;
typedef struct wind_task *	TASK_ID;
typedef struct wdog *		WDOG_ID;
typedef struct page_pool_obj *	PAGE_POOL_ID;
typedef struct pgMgrObj *	PAGE_MGR_ID;
typedef struct vm_context *	VM_CONTEXT_ID;
typedef struct mem_part *	PART_ID;
typedef struct wind_isr *       ISR_ID;
#if 0 /* XXX these will be moved here soon */
typedef struct wind_dms *	DMS_ID;
#endif
typedef struct wind_sd *	SD_ID;

#endif /* #ifndef _ASMLANGUAGE */

#ifdef __cplusplus
}
#endif

#endif /* __INCvxWindh */
