/* usbOhci.h - OHCI host controller driver interface definition */

/* Copyright 2004-2005 Wind River Systems, Inc.

   This software includes software licensed to Wind River Systems, Inc.
   by Wipro, Ltd. Wind River licensees may use this software according
   to the terms of their Wind River license agreement(s) applicable to
   this software.
*/

/*
modification history
--------------------
01f,28mar05,pdg  non-pci changes
01e,25feb05,mta  SPR 106276
01d,18aug04,hch  remove the DMA_MALLOC and DMA_FREE macro
01c,16jul04,hch  adding OHCI PCI class definition
01b,26jun03,nld  Changing the code to WRS standards
01a,17mar03,ssh  Initial Version
*/

/*
DESCRIPTION

This file contains the constants, data structures and functions exposed by the
OHCI host controller driver.
*/

/*
INTERNAL
 *******************************************************************************
 * Filename         : OHCI.h
 *
 * Copyright        :
 *
 * THE COPYRIGHT IN THE CONTENTS OF THIS SOFTWARE VEST WITH WIPRO
 * LIMITED A COMPANY INCORPORATED UNDER THE LAWS OF INDIA AND HAVING
 * ITS REGISTERED OFFICE AT DODDAKANNELLI SARJAPUR ROAD  BANGALORE
 * 560 035. DISTRIBUTION OR COPYING OF THIS SOFTWARE BY
 * ANY INDIVIDUAL OR ENTITY OTHER THAN THE ADDRESSEE IS STRICTLY
 * PROHIBITED AND MAY INCUR LEGAL LIABILITY. IF YOU ARE NOT THE
 * ADDRESSEE PLEASE NOTIFY US IMMEDIATELY BY PHONE OR BY RETURN EMAIL.
 * THE ADDRESSEE IS ADVISED TO MAINTAIN THE PROPRIETARY INTERESTS OF
 * THIS COPYRIGHT AS PER APPLICABLE LAWS.
 *
 *
 * Description      : This file contains the constants, data structures and
 *                    functions exposed by the OHCI host controller driver.
 *
 *
 ******************************************************************************/

#ifndef __INCusbOhcih
#define __INCusbOhcih

#ifdef	__cplusplus
extern "C" {
#endif

/* includes */

/* WindView Instrumentation */
#include "usbHcdInstr.h"
#include <cacheLib.h> 
#include "usb/pciConstants.h"

/* defines */

/* OpenHCI PCI class defintion */

#define OHCI_CLASS              0x0c        /* BASEC value for serial bus */
#define OHCI_SUBCLASS           0x03        /* SCC value for USB */
#define OHCI_PGMIF              0x10        /* OpenHCI */

/* To hold the speed of the device (BEGIN) */

/* To hold the value for the full speed devices */
#define USB_OHCI_DEVICE_FULL_SPEED		0x00

/* To hold the value for the low speed devices */
#define USB_OHCI_DEVICE_LOW_SPEED		0x01

/* To hold the speed of the device (END) */

/* Macros used for bus specific handling - Start */

extern pUSB_HCD_BUS_INFO	pOhciBusInfo[];

/* Macro used for converting the CPU memory to bus specific memory */

#define USB_OHCD_CONVERT_TO_BUS_MEM(INDEX, ADDRESS) 			\
			(((ADDRESS) == NULL) ? 0:			\
	                 pOhciBusInfo[INDEX]->pFuncCpuToBus((pVOID)ADDRESS))

/* Macro used for converting the bus specific memory to CPU memory */
#define USB_OHCD_CONVERT_FROM_BUS_MEM(INDEX ,ADDRESS)                         \
                    (((ADDRESS) == 0) ? 0: 				       \
			pOhciBusInfo[INDEX]->pFuncBusToCpu((UINT32)ADDRESS))

/* Macro used for swapping the 32 bit values */

#define USB_OHCD_SWAP_DATA(INDEX,VALUE)				       \
                    (((pOhciBusInfo[INDEX]->pFuncDataSwap) == NULL) ? VALUE : \
                                pOhciBusInfo[INDEX]->pFuncDataSwap(VALUE))                     

/* Macro used for swapping the contents of buffers */

#define USB_OHCD_SWAP_BUFDATA(INDEX,BUFFER,SIZE)			       \
                    (((pOhciBusInfo[INDEX]->pFuncBufferSwap) == NULL) ? SIZE : \
                         pOhciBusInfo[INDEX]->pFuncBufferSwap(BUFFER, SIZE))

/* Macros used for bus specific handling - End */

/* Size of DMA memory */

#define OHCI_FREE(pBfr)		OS_FREE ((char *) pBfr)

#define DMA_FLUSH(pBfr, bytes)	    CACHE_DMA_FLUSH (pBfr, bytes)
#define DMA_INVALIDATE(pBfr, bytes) CACHE_DMA_INVALIDATE (pBfr, bytes)

#define USER_FLUSH(pBfr, bytes)     CACHE_USER_FLUSH (pBfr, bytes)
#define USER_INVALIDATE(pBfr,bytes) CACHE_USER_INVALIDATE (pBfr, bytes)

#define OHCI_STATUS                      INT8
#define OHCI_SUCCESS                     ((INT8) 0)
#define OHCI_FAILURE                     ((INT8) -12)


/* function declarations */

/*******************************************************************************
 * Function Name  : usbOhciInit
 * Description    : Function to initialise the OHCI Host Controller Driver.
 *                  This function detects the number of OHCI Controllers
 *                  present on the system and initializes all the OHCI
 *                  controllers.
 * Parameters     : None.
 * Return Type    : Returns TRUE if the OHCI Host Controllers are initialized
 *                  successfully. Else returns FALSE.
 ******************************************************************************/
BOOLEAN	usbOhciInit (void);

/*******************************************************************************
 * Function Name  : usbOhciExit
 * Description    : Function to uninitialise the OHCI Host Controller Driver.
 * Parameters     : None.
 * Return Type    : Returns TRUE if all the OHCI Controllers are reset and
 *                  the cleanup is successful. Else returns FALSE.
 ******************************************************************************/
BOOLEAN	usbOhciExit (void);

#ifdef	__cplusplus
}
#endif

#endif	/* __INCusbOhcih */

/* End of file usbOhci.h */
