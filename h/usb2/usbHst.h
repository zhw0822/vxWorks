/* usbHst.h - USB Host Stack functional interface definition */

/* Copyright 2004 Wind River Systems, Inc. 

   This software includes software licensed to Wind River Systems, Inc.
   by Wipro, Ltd. Wind River licensees may use this software according  
   to the terms of their Wind River license agreement(s) applicable to 
   this software.
*/

/*
Modification history
--------------------
01b,02jun03,nrv Changing the code to WRS standards
*/

/*
DESCRIPTION

This file contains the application interface to the USB2 Host Stack.

*/


/*
INTERNAL
 *******************************************************************************
 * Filename         : Usbhst.h
 *
 * Copyright        :
 *
 * THE COPYRIGHT IN THE CONTENTS OF THIS SOFTWARE VEST WITH WIPRO
 * LIMITED A COMPANY INCORPORATED UNDER THE LAWS OF INDIA AND HAVING
 * ITS REGISTERED OFFICE AT DODDAKANNELLI SARJAPUR ROAD  BANGALORE
 * 560 035. DISTRIBUTION OR COPYING OF THIS SOFTWARE BY
 * ANY INDIVIDUAL OR ENTITY OTHER THAN THE ADDRESSEE IS STRICTLY
 * PROHIBITED AND MAY INCUR LEGAL LIABILITY. IF YOU ARE NOT THE
 * ADDRESSEE PLEASE NOTIFY US IMMEDIATELY BY PHONE OR BY RETURN EMAIL.
 * THE ADDRESSEE IS ADVISED TO MAINTAIN THE PROPRIETARY INTERESTS OF
 * THIS COPYRIGHT AS PER APPLICABLE LAWS.
 *
 *
 * Description      : This file contains the data structures for usbd and other
 *                    modules.
\ *
 ******************************************************************************/

#ifndef __USBHST_H__
#define __USBHST_H__

#ifdef __cplusplus
extern "C" {
#endif


#include "usbOsal.h"

/********************** MACRO DEFINITION **************************************/


/* Device class for vendor specific*/
#define USBHST_VENDOR_SPECIFIC                  0xFF

/* Hub class code*/
#define USBHST_HUB_CLASS                        0x09

/* Timeout value for URB requests */
#define USBHST_URB_TIMEOUT                      \
        OS_CONVERT_MILLISECONDS_TO_WAIT_VALUE(30000)

/* To hold the URB transfer flags (BEGIN) */

/* Flag to specify that a short transfer is acceptable */
#define USBHST_SHORT_TRANSFER_OK                0x01

/*
 * Flag to specify that the isochronous transfer should be started as
 * soon as possible.
 */
#define USBHST_START_ISOCHRONOUS_TRANSFER_ASAP  0x02

/* To hold the URB transfer flags (END) */

/*
 * Default configuration index is used with USBHST_SetConfiguration ()
 * to set the device into address state.
 */
#define USBHST_DEFAULT_CONFIGURATION_INDEX      0xFF

/* Default interface value */
#define USBHST_DEFAULT_INTERFACE_VALUE          0x00

/************* Three different speeds - Start *********************************/

/* Full speed mode */
#define USBHST_FULL_SPEED                       ((UINT8)0x00)

/* Low speed mode */
#define USBHST_LOW_SPEED                        ((UINT8)0x01)

/* High Speed mode */
#define USBHST_HIGH_SPEED                       ((UINT8)0x02)

/************* Three different speeds - End ***********************************/

/******************* To hold the USB Transfer Types (BEGIN) *******************/

/* Value to specific an USB Control Transfer */
#define USBHST_CONTROL_TRANSFER                 0x00

/* Value to specific an USB Isochronous Transfer */
#define USBHST_ISOCHRONOUS_TRANSFER             0x01

/* Value to specific an USB Bulk Transfer */
#define USBHST_BULK_TRANSFER                    0x02

/* Value to specific an USB Interrupt Transfer */
#define USBHST_INTERRUPT_TRANSFER               0x03

/******************** To hold the USB Transfer Types (END) ********************/

/************* Different types of Feature Selector - Start ********************/

/* Endpoint halt for the endpoints */
#define USBHST_FEATURE_ENDPOINT_HALT            ((UINT16)0x00)

/* Remote wakeup for the device */
#define USBHST_FEATURE_DEVICE_REMOTE_WAKEUP     ((UINT16)0x01)

/* Test mode feature for the endpoints */
#define USBHST_FEATURE_TEST_MODE                ((UINT16)0x02)

/************** Different types of Feature Selector - End *********************/

 

/************** Different test selectors values - Start ***********************/

/* Test selector for TEST_J */
#define USBHST_TEST_J                ((UINT8)0x01)

/* Test selector for TEST_K */
#define USBHST_TEST_K                ((UINT8)0x02)

/* Test selector for TEST_SE0_NAK */
#define USBHST_TEST_SE0_NAK          ((UINT8)0x03)

/* Test selector for TEST_PACKET */
#define USBHST_TEST_PACKET           ((UINT8)0x04)

/* Test selector for TEST_FORCE_ENABLE */
#define USBHST_ TEST_FORCE_ENABLE    ((UINT8)0x05)


/************** Different test selectors values  - End ************************/


/************** Different recipient types  - Start ****************************/

/* Recipient is device */
#define USBHST_RECIPIENT_DEVICE                 0x00

/* Recipient is interface */
#define USBHST_RECIPIENT_INTERFACE              0x01

/* Recipient is endpoint */
#define USBHST_RECIPIENT_ENDPOINT               0x02

/* Other recipients */
#define USBHST_RECIPIENT_OTHER                  0x03

/************** Different recipient types  - End ******************************/

/************** Different descriptor types  - Start ***************************/

/* Defines device descriptor */
#define USBHST_DEVICE_DESC                  ((UINT8) 0x01)

/* Defines configuration descriptor */
#define USBHST_CONFIG_DESC                  ((UINT8) 0x02)

/* Defines string descriptor */
#define USBHST_STRING_DESC                  ((UINT8) 0x03)

/* Defines interface descriptor */
#define USBHST_INTERFACE_DESC               ((UINT8) 0x04)

/* Defines endpoint descriptor */
#define USBHST_ENDPOINT_DESC                ((UINT8) 0x05)

 

/* defines the device qualifier */
#define USBHST_DEVICE_QUALIFIER_DESC        ((UINT8) 0x06)

/* defines the other speed configuration descriptor */
#define USBHST_OTHER_SPEED_CONFIG_DESC      ((UINT8) 0x07)

/* defines the interface power descriptor */
#define USBHST_INTERFACE_POWER_DESC         ((UINT8) 0x08)

/************** Different descriptor types  - End *****************************/


/************** Different Standard Request codes - Start **********************/

#define USBHST_REQ_GET_STATUS               ((UINT8) 0x00)
#define USBHST_REQ_CLEAR_FEATURE            ((UINT8) 0x01)
#define USBHST_REQ_SET_FEATURE              ((UINT8) 0x03)
#define USBHST_REQ_SET_ADDRESS              ((UINT8) 0x05)
#define USBHST_REQ_GET_DESCRIPTOR           ((UINT8) 0x06)
#define USBHST_REQ_SET_DESCRIPTOR           ((UINT8) 0x07)
#define USBHST_REQ_GET_CONFIGURATION        ((UINT8) 0x08)
#define USBHST_REQ_SET_CONFIGURATION        ((UINT8) 0x09)
#define USBHST_REQ_GET_INTERFACE            ((UINT8) 0x0A)
#define USBHST_REQ_SET_INTERFACE            ((UINT8) 0x0B)
#define USBHST_REQ_SYNCH_FRAME              ((UINT8) 0x0C)

/************** Different Standard Request codes - End ************************/

/************** Different USB status codes - Start ****************************/

#define USBHST_STATUS                       INT8
#define USBHST_SUCCESS                      ((INT8)0)
#define USBHST_TRANSFER_COMPLETED           ((INT8)1)
#define USBHST_FAILURE                      ((INT8)-1)
#define USBHST_MEMORY_NOT_ALLOCATED         ((INT8)-2)
#define USBHST_INSUFFICIENT_BANDWIDTH       ((INT8)-3)
#define USBHST_INSUFFICIENT_MEMORY          ((INT8)-4)
#define USBHST_INSUFFICIENT_RESOURCE        ((INT8)-5)
#define USBHST_INVALID_REQUEST              ((INT8)-6)
#define USBHST_INVALID_PARAMETER            ((INT8)-7)
#define USBHST_STALL_ERROR                  ((INT8)-8)
#define USBHST_DEVICE_NOT_RESPONDING_ERROR  ((INT8)-9)
#define USBHST_DATA_OVERRUN_ERROR           ((INT8)-10)
#define USBHST_DATA_UNDERRUN_ERROR          ((INT8)-11)
#define USBHST_BUFFER_OVERRUN_ERROR         ((INT8)-12)
#define USBHST_BUFFER_UNDERRUN_ERROR        ((INT8)-13)
#define USBHST_TRANSFER_CANCELLED           ((INT8)-14)
#define USBHST_TIMEOUT                      ((INT8)-15)
#define USBHST_BAD_START_OF_FRAME           ((INT8)-16)

/************** Different USB status codes - End ******************************/


/*******************************************************************************
 * Macro Name  : USBHST_FILL_SETUP_PACKET
 * Description : Initialize the SETUP_PACKET structure with the requisite
 *               parameter.
 * Parameters  : pSetup         OUT Setup packet to be filled.
 *               uRequestType   IN  Bitmap for request characteristics.
 *               uRequest       IN  Standard request code.
 *               uValue         IN  Word size value field for standard request.
 *               uIndex         IN  Word size index field for standard request.
 *               uSize          IN  Number of bytes to transfer if there is
 *                                  data stage.
 * Return Type : None
 ******************************************************************************/
#define USBHST_FILL_SETUP_PACKET(pSetup,                                     \
                                 uRequestType,                               \
                                 uRequest,                                   \
                                 uValue,                                     \
                                 uIndex,                                     \
                                 uSize)                                      \
{                                                                            \
    /* Store bitmap for request characteristics in the setup packet */       \
    (pSetup)->bmRequestType   = (UINT8)(uRequestType);                       \
                                                                             \
    /* Store the request in the setup packet */                              \
    (pSetup)->bRequest        = (UINT8)(uRequest);                           \
                                                                             \
    /* Store word size value field for request in the setup packet */        \
    (pSetup)->wValue          = OS_UINT16_CPU_TO_LE((UINT16)(uValue));       \
                                                                             \
    /* Store word size index field for request  in the setup packet */       \
    (pSetup)->wIndex          = OS_UINT16_CPU_TO_LE((UINT16)(uIndex));       \
                                                                             \
    /* Store number of bytes to transfer if there is data stage */           \
    (pSetup)->wLength         = OS_UINT16_CPU_TO_LE((UINT16)(uSize));        \
}

/*******************************************************************************
 * Macro Name  : USBHST_FILL_CONTROL_URB
 * Description : Initialize the URB with requisite parameters for control
 *               transfer.
 * Parameters  : Urb                   OUT Pointer to URB.
 *               Device                IN  Handle to the Device.
 *               EndPointAddress       IN  End Point Address for which URB is
 *                                          issued.
 *               TransferBuffer        OUT Pointer to the buffer passed as
 *                                          parameter.
 *               TransferLength        IN  Length of the data transfer
 *                                          requested.
 *               TransferFlags         IN  Flags for data transfer.
 *               SetupPacket           IN  Pointer to setup packet.
 *               Callback              IN  Pointer to the callback function.
 *               Context               IN  Class driver specific URB information
 *               Status                OUT Status of the request.
 * Return Type : None
 ******************************************************************************/
#define USBHST_FILL_CONTROL_URB(Urb,                                       \
                                Device,                                    \
                                EndPointAddress,                           \
                                TransferBuffer,                            \
                                TransferLength,                            \
                                TransferFlags,                             \
                                SetupPacket,                               \
                                Callback,                                  \
                                Context,                                   \
                                Status)                                    \
{                                                                          \
    /* Store the handle to the Device  */                                  \
    (Urb)->hDevice               = (Device);                               \
                                                                           \
    /* Store the end Point Address for which Urb is issued */              \
    (Urb)->uEndPointAddress      = (EndPointAddress);                      \
                                                                           \
    /* Store pointer to the buffer passed as parameter */                  \
    (Urb)->pTransferBuffer       = (TransferBuffer);                       \
                                                                           \
    /* Store length of the data transfer requested */                      \
    (Urb)->uTransferLength       = (TransferLength);                       \
                                                                           \
    /* Store flags for data transfer */                                    \
    (Urb)->uTransferFlags        = (TransferFlags);                        \
                                                                           \
    /* Store pointer to setup packet */                                    \
    (Urb)->pTransferSpecificData = (SetupPacket);                          \
                                                                           \
    /* Store the pointer to the callback function */                       \
    (Urb)->pfCallback            = (Callback);                             \
                                                                           \
    /* Store Class driver specific URB information */                      \
    (Urb)->pContext              = (Context);                              \
                                                                           \
    /* Store the status of the request */                                  \
    (Urb)->nStatus               = (Status);                               \
}

/*******************************************************************************
 * Macro Name  : USBHST_FILL_BULK_URB
 * Description : Initialize the URB with requisite parameters for bulk transfer.
 * Parameters  : Urb                   OUT Pointer to URB
 *               Device                IN  Handle to the Device
 *               EndPointAddress       IN  End Point Address for which URB is
 *                                          issued.
 *               TransferBuffer        OUT Pointer to the buffer passed as
 *                                          parameter.
 *               TransferLength        IN  Length of the data transfer
 *                                          requested.
 *               TransferFlags         IN  Flags for data transfer.
 *               Callback              IN  Pointer to the callback function.
 *               Context               IN  Class driver specific URB information
 *               Status                OUT Status of the request.
 * Return Type : None
 ******************************************************************************/
#define USBHST_FILL_BULK_URB(Urb,                                          \
                             Device,                                       \
                             EndPointAddress,                              \
                             TransferBuffer,                               \
                             TransferLength,                               \
                             TransferFlags,                                \
                             Callback,                                     \
                             Context,                                      \
                             Status)                                       \
{                                                                          \
    /* Store the handle to the Device  */                                  \
    (Urb)->hDevice               = (Device);                               \
                                                                           \
    /* Store the end Point Address for which URB is issued */              \
    (Urb)->uEndPointAddress      = (EndPointAddress);                      \
                                                                           \
    /* Store pointer to the buffer passed as parameter */                  \
    (Urb)->pTransferBuffer       = (TransferBuffer);                       \
                                                                           \
    /* Store length of the data transfer requested */                      \
    (Urb)->uTransferLength       = (TransferLength);                       \
                                                                           \
    /* Store flags for data transfer */                                    \
    (Urb)->uTransferFlags        = (TransferFlags);                        \
                                                                           \
    /* Store pointer to setup packet */                                    \
    (Urb)->pTransferSpecificData = NULL;                                   \
                                                                           \
    /* Store the pointer to the callback function */                       \
    (Urb)->pfCallback            = (Callback);                             \
                                                                           \
    /* Store Class driver specific URB information */                      \
    (Urb)->pContext              = (Context);                              \
                                                                           \
    /* Store the status of the request */                                  \
    (Urb)->nStatus               = (Status);                               \
}

/*******************************************************************************
 * Macro Name  : USBHST_FILL_INTERRUPT_URB
 * Description : Initialize the URB with requisite parameters for interrupt
 *               transfer.
 * Parameters  : Urb                   OUT Pointer to URB
 *               Device                IN  Handle to the Device
 *               EndPointAddress       IN  End Point Address for which URB is
 *                                          issued.
 *               TransferBuffer        OUT Pointer to the buffer passed as
 *                                          parameter.
 *               TransferLength        IN  Length of the data transfer
 *                                          requested.
 *               TransferFlags         IN  Flags for data transfer.
 *               Callback              IN  Pointer to the callback function.
 *               Context               IN  Class driver specific URB information
 *               Status                OUT Status of the request.
 * Return Type : None
*******************************************************************************/
#define USBHST_FILL_INTERRUPT_URB(Urb,                                     \
                                  Device,                                  \
                                  EndPointAddress,                         \
                                  TransferBuffer,                          \
                                  TransferLength,                          \
                                  TransferFlags,                           \
                                  Callback,                                \
                                  Context,                                 \
                                  Status)                                  \
{                                                                          \
    /* Store the handle to the Device  */                                  \
    (Urb)->hDevice               = (Device);                               \
                                                                           \
    /* Store the end Point Address for which URB is issued */              \
    (Urb)->uEndPointAddress      = (EndPointAddress);                      \
                                                                           \
    /* Store pointer to the buffer passed as parameter */                  \
    (Urb)->pTransferBuffer       = (TransferBuffer);                       \
                                                                           \
    /* Store length of the data transfer requested */                      \
    (Urb)->uTransferLength       = (TransferLength);                       \
                                                                           \
    /* Store flags for data transfer */                                    \
    (Urb)->uTransferFlags        = (TransferFlags);                        \
                                                                           \
    /* Store pointer to setup packet */                                    \
    (Urb)->pTransferSpecificData = NULL;                                   \
                                                                           \
    /* Store the pointer to the callback function */                       \
    (Urb)->pfCallback            = (Callback);                             \
                                                                           \
    /* Store Class driver specific (Urb) information */                    \
    (Urb)->pContext              = (Context);                              \
                                                                           \
    /* Store the status of the request */                                  \
    (Urb)->nStatus               = (Status);                               \
}

/*******************************************************************************
 * Macro Name  : USBHST_FILL_ISOCHRONOUS_URB
 * Description : Initialize the URB with requisite parameters for isochronous
 *               transfer.
 *               Device                IN  Handle to the Device
 *               EndPointAddress       IN  End Point Address for which URB is
 *                                         issued.
 *               TransferBuffer        OUT Pointer to the buffer passed as
 *                                         parameter.
 *               TransferLength        IN  Length of the data transfer
 *                                         requested.
 *               TransferFlags         IN  Flags for data transfer.
 *               StartFrame            IN  Start of Frame information.
 *               NumberOfPackets       IN  Number of packets.
 *               Callback              IN  Pointer to the callback function.
 *               Context               IN  Class driver specific URB information
 *               Status                OUT Status of the request.
 * Return Type : None
 ******************************************************************************/
#define USBHST_FILL_ISOCHRONOUS_URB(Urb,                                   \
                                    Device,                                \
                                    EndPointAddress,                       \
                                    TransferBuffer,                        \
                                    TransferLength,                        \
                                    TransferFlags,                         \
                                    StartFrame,                            \
                                    NumberOfPackets,                       \
                                    IsoPacketDesc,                         \
                                    Callback,                              \
                                    Context,                               \
                                    Status)                                \
{                                                                          \
    /* Store the handle to the Device  */                                  \
    (Urb)->hDevice               = (Device);                               \
                                                                           \
    /* Store the end Point Address for which URB is issued */              \
    (Urb)->uEndPointAddress      = (EndPointAddress);                      \
                                                                           \
    /* Store pointer to the buffer passed as parameter */                  \
    (Urb)->pTransferBuffer       = (TransferBuffer);                       \
                                                                           \
    /* Store length of the data transfer requested */                      \
    (Urb)->uTransferLength       = (TransferLength);                       \
                                                                           \
    /* Store flags for data transfer */                                    \
    (Urb)->uTransferFlags        = (TransferFlags);                        \
                                                                           \
    /* Store the start of Frame information */                             \
    (Urb)->uStartFrame           = (StartFrame);                           \
                                                                           \
    /* Store the number of packets */                                      \
    (Urb)->uNumberOfPackets      = (NumberOfPackets);                      \
                                                                           \
    /* Store pointer to setup packet */                                    \
    (Urb)->pTransferSpecificData = (IsoPacketDesc);                        \
                                                                           \
    /* Store the pointer to the callback function */                       \
    (Urb)->pfCallback            = (Callback);                             \
                                                                           \
    /* Store Class driver specific URB information */                      \
    (Urb)->pContext              = (Context);                              \
                                                                           \
    /* Store the status of the request */                                  \
    (Urb)->nStatus               = (Status);                               \
}

/* Header used to identify the descriptor type and length */
typedef struct  usbhst_descriptor_header
    {
    UINT8       uLength;         /* Size of this descriptor in bytes         */ 
    UINT8       uDescriptorType; /* Descriptor Type                          */
    } __attribute__((packed)) USBHST_DESCRIPTOR_HEADER,
      *pUSBHST_DESCRIPTOR_HEADER; 



/* Structure to hold the Standard Device Descriptor */
typedef struct usbhst_device_descriptor
    {
    UINT8  bLength;             /* Size of this descriptor                   */
    UINT8  bDescriptorType;     /* DEVICE Descriptor Type                    */
    UINT16 bcdUSB;              /* BCD USB Spec Release Number               */
    UINT8  bDeviceClass;        /* Class Class code                          */
    UINT8  bDeviceSubClass;     /* SubClass Subclass code                    */
    UINT8  bDeviceProtocol;     /* Protocol code                             */
    UINT8  bMaxPacketSize;      /* Max packet size for endpoint zero         */
    UINT16 idVendor;            /* Vendor ID of this interface               */
    UINT16 idProduct;           /* Product ID of this interface              */
    UINT16 bcdDevice;           /* BCD Device release number                 */
    UINT8  iManufacturer;       /* Manufacturer string descriptor index      */
    UINT8  iProduct;            /* Product string descriptor index           */
    UINT8  iSerialNumber;       /* Dev serial number string descriptor index */
    UINT8  bNumConfigurations;  /* Num of possible configurations            */
    }__attribute__((packed)) USBHST_DEVICE_DESCRIPTOR,
    *pUSBHST_DEVICE_DESCRIPTOR;

 
/* Structure to hold the device qualifier */
typedef struct usbhst_device_qualifier
    {
    UINT8  bLength;             /* Size of this descriptor in bytes          */
    UINT8  bDescriptorType;     /* Interface Descriptor Type                 */
    UINT16 bcdUSB;              /* BCD USB Specification release number      */
    UINT8  bDeviceClass;        /* Device class code                         */
    UINT8  bDeviceSubClass;     /* Device subClass code                      */
    UINT8  bDeviceProtocol;     /* Protocol code                             */
    UINT8  bMaxPacketSize;      /* Max packet size for endpoint zero         */
    UINT8  bNumConfigurations;  /* Num of possible configurations            */
    UINT8  bReserved;           /* reserved for future use                   */
    }__attribute__((packed)) USBHST_DEVICE_QUALIFIER,
    *pUSBHST_DEVICE_QUALIFIER;



/* Structure to hold Standard Configuration Descriptor */
typedef struct usbhst_config_descriptor
    {
    UINT8  bLength;             /* Size of this descriptor in bytes          */
    UINT8  bDescriptorType;     /* CONFIGURATION Descriptor Type             */
    UINT16 wTotalLength;        /* Configuration data length                 */
    UINT8  bNumInterfaces;      /* Number of interfaces supported            */
    UINT8  bConfigurationValue; /* SetConfiguration() function argument      */
    UINT8  iConfiguration;      /* Configuration string descriptor           */
    UINT8  bmAttributes;        /* Bitmap configuration characteristics      */
    UINT8  MaxPower; /* Max pwr consumption (mA) of operational device on bus*/
    }__attribute__((packed)) USBHST_CONFIG_DESCRIPTOR, 
    *pUSBHST_CONFIG_DESCRIPTOR;

/* Structure to hold Standard Interface Descriptor */
typedef struct usbhst_interface_descriptor
    {
    UINT8  bLength;             /* Size of this descriptor in bytes          */
    UINT8  bDescriptorType;     /* Interface Descriptor Type                 */
    UINT8  bInterfaceNumber;    /* Zero-based interface number               */
    UINT8  bAlternateSetting;   /* Alternate setting for interface           */
    UINT8  bNumEndpoints;       /* Num of endpoints (excluding endpoint zero)*/
    UINT8  bInterfaceClass;     /* Class code of this interface              */
    UINT8  bInterfaceSubClass;  /* Subclass code of this interface           */
    UINT8  bInterfaceProtocol;  /* Interface protocol code                   */
    UINT8  iInterface;          /* Interface string descriptor               */
    }__attribute__((packed)) USBHST_INTERFACE_DESCRIPTOR,
    *pUSBHST_INTERFACE_DESCRIPTOR;


/* Structure to hold Standard Endpoint Descriptor */
typedef struct usbhst_endpoint_descriptor 
    {
    UINT8  bLength;             /* Descriptor length in bytes                */
    UINT8  bDescriptorType;     /* Endpoint Descriptor type                  */
    UINT8  bEndpointAddress;    /* Endpoint address on the USB device        */
    UINT8  bmAttributes;        /* Endpoint transfer, sych & usage types attr*/
    UINT16 wMaxPacketSize;      /* Endpoint max packet size                  */
    UINT8  bInterval;           /* Polling interval for endpoint data xfer   */
    }__attribute__((packed)) USBHST_ENDPOINT_DESCRIPTOR,
    *pUSBHST_ENDPOINT_DESCRIPTOR;

/* Structure to hold the String Descriptor */
typedef struct usbhst_string_descriptor
    {
    UINT8  bLength;              /* Descriptor length in bytes                */
    UINT8  bDescriptorType;      /* Endpoint Descriptor type                  */
    UINT16 wUnicodeString[1];    /* UNICODE encoded string                    */
    }__attribute__((packed)) USBHST_STRING_DESCRIPTOR, 
    *pUSBHST_STRING_DESCRIPTOR;

/*
 * This structure is used to store the pointers to entry points and class
 * driver information
 */
typedef struct usbhst_device_driver 
    {
    BOOL    bFlagVendorSpecific;  /* Vendor Specific or class specific flag  */
    UINT16  uVendorIDorClass;     /* Vendor ID (if vendor) or Class Code     */
    UINT16  uProductIDorSubClass; /* Dev ID (if vendor) or SubClass Code     */
    UINT16  uBCDUSBorProtocol;    /* DevRel num (if vendor) or Protocol code */

    /*
     * Function registered as to be called when a matching interface/device
     * is connected
     */
    USBHST_STATUS  (*addDevice)   (UINT32    hDevice,
                                   UINT8     uInterfaceNumber,
				   UINT8     uSpeed,
                                   void      **pDriverData);
    /*
     * Function registered as to be called when a matching interface/device
     * is disconnected
     */
    void          (*removeDevice)  (UINT32    hDevice,
                                    void *    pDriverData);
    /*
     * Function registered as to be called when a matching interface/device
     *  is suspended
     */
    void          (*suspendDevice) (UINT32    hDevice,
                                    void *     pDriverData);

    /*
     * Function registered as to be called when a matching interface/device
     * is resumed
     */
    void          (*resumeDevice)  (UINT32    hDevice,
                                    void *     pDriverData);
    } USBHST_DEVICE_DRIVER, *pUSBHST_DEVICE_DRIVER;


/* Setup packet structure  */
typedef struct usbhst_setup_packet
    {
    UINT8  bmRequestType;        /* Bitmap for request characteristics       */
    UINT8  bRequest;             /* Specific request                         */
    UINT16 wValue;               /* Word size value field for specific req   */
    UINT16 wIndex;               /* Word size index field for specific req   */
    UINT16 wLength;              /* Num of bytes to transfer if data stage   */
    }__attribute__((packed)) USBHST_SETUP_PACKET,
     *pUSBHST_SETUP_PACKET;

/*
 * This structure is used to store the information for isochronous
 * transfer required by USB request Block
 */
typedef struct usbhst_iso_packet_desc 
    {
    UINT32        uLength;       /* Length of data                           */
    UINT32        uOffset;       /* Offset in the pTransferBuffer for data   */
    USBHST_STATUS nStatus;       /* Per frame status.                        */
    } USBHST_ISO_PACKET_DESC, *pUSBHST_ISO_PACKET_DESC;

typedef struct usbhst_urb USBHST_URB, *pUSBHST_URB;
typedef USBHST_STATUS (*pURB_CALLBACK_t)(pUSBHST_URB pURB);

/*
 * This structure is used to store the information required to fill a
 * USB Request Block
 */
struct usbhst_urb 
    {
    UINT32          hDevice;           /* Device handle                      */
    UINT8           uEndPointAddress;  /* EndPoint addr for which URB issued */ 
    UCHAR *          pTransferBuffer;   /* Ptr to the buffer passed as param  */
    UINT32          uTransferLength;   /* Len of the data transfer requested */
    UINT32          uTransferFlags;    /* Flags for data transfer            */
    UINT16           uStartFrame;       /* Start of Frame information         */
    UINT32          uNumberOfPackets;  /* Number of packets                  */


    /*
     * Pointer to setup packet in case of control transfers
     * Pointer to isochronous packet descriptor in case of isochronous transfer.
     * NULL in case of bulk and interrupt transfers
     */
    void *           pTransferSpecificData;

    pURB_CALLBACK_t pfCallback;        /* Pointer to the callback function   */
    void *           pContext;          /* Class driver specific URB info     */
    USBHST_STATUS   nStatus;           /* Request status                     */
    };

/*
 * This structure is used to store the pointers to USBD entry points that
 * are visible to only Hub Class Driver.
 */
typedef struct usbhst_usbd_to_hub_function_list
    {
    /*
     * Pointer to function, which will be called by Hub Class Driver to
     * create a new USB device
     */
    USBHST_STATUS   (*newDevice)(UINT8   uSpeed,
                                       UINT8   uUsbBusIndex,
				       UINT32  uHighSpeedHub,
				       UINT8   uHighSpeedHubPortNumber,
                                       UINT32 *pDeviceHandle);

    /*
     * Pointer to function, which will be called by Hub Class Driver to
     * configure a new USB device
     */
    USBHST_STATUS   (*configureDevice)(UINT32 hDevice);

    /*
     * Pointer to function, which will be called by Hub Class Driver to
     * remove a  USB device
     */
    void            (*removeDevice)(UINT32 hDevice);

    /*
     * Pointer to function, which will be called by Hub Class Driver to
     * suspend a USB device
     */
    void            (*suspendDevice)(UINT32 hDevice);

    /*
     * Pointer to function, which will be called by Hub Class Driver to
     * resume a USB device
     */
    void            (*resumeDevice)(UINT32 hDevice);

 
    /*
     * Pointer to function, which will be called by Hub Class Driver upon
     * completion of Clear TT Request
     */

    USBHST_STATUS   (*clearTTComplete)(USBHST_STATUS nStatus,
                                       UINT32        hHighSpeedHub,
                                       void *        pContext);
    /*
     * Pointer to function, which will be called by Hub Class Driver upon
     * completion of Reset TT Request
     */
    USBHST_STATUS   (*resetTTComplete)(USBHST_STATUS nStatus,
                                       UINT32        uHighSpeedHub,
                                       void *        pContext);


    } USBHST_USBD_TO_HUB_FUNCTION_LIST, *pUSBHST_USBD_TO_HUB_FUNCTION_LIST;

 
/*
 * This structure is used to store the pointers to USBD entry points that
 * are visible only to Enhanced Host Controller(EHCI) Driver.
 */
typedef struct usbHstUsbdToHcdFunctionList
{
    /*
     * Pointer to function, which will be called by Host Controller Driver to
     * submit clear TT request
     */
    USBHST_STATUS  (*clearTTRequest)(UINT32 hHCDriver,
                                     UINT8  uRelBusIndex,
                                     UINT8  uHighSpeedHubAddress,
                                     UINT8  uHighSpeedPortNumber,
                                     UINT16 wValue,
                                     void * pContext);

    /*
     * Pointer to function, which will be called by Host Controller Driver to
     * submit reset TT request
     */
    USBHST_STATUS  (*resetTTRequest)(UINT32 hHCDriver,
                                     UINT8  uRelBusIndex,
                                     UINT8  uHighSpeedHubAddress,
                                     UINT8  uHighSpeedPortNumber,
                                     void * pContext);

} USBHST_USBD_TO_HCD_FUNCTION_LIST, *pUSBHST_USBD_TO_HCD_FUNCTION_LIST;



/* This structure contains the hub class driver function pointers */
typedef struct usbhst_hub_function_list
    {
    /* Function pointer to suspend a device */
    USBHST_STATUS (*selectiveSuspendDevice)(UINT32 uDeviceHandle);

    /* Function pointer to resume a device */
    USBHST_STATUS (*selectiveResumeDevice) (UINT32 uDeviceHandle);

    /* Function pointer to add a root hub */
    USBHST_STATUS (*addRootHub)   (UINT32 uDeviceHandle, 
				   UINT8 uBusHandle, 
				   UINT8 uSpeed);

    /* Function pointer to remove a root hub */
    void  (*removeRootHub)(UINT8 uBusHandle);

    /*
     * Function pointer to check whether the power required for a device can be
     * supported.
     */
    USBHST_STATUS (*checkForPower)(UINT32 uDeviceHandle,
                                   UINT8 uPowerRequirement);

    /* Function pointer to reset a device */
    USBHST_STATUS (*resetDevice)   (UINT32 uDeviceHandle);

 

    /* Function pointer to submit clear TT request */
    USBHST_STATUS (*clearTT)(UINT32 hHighSpeedHub,
                             UINT8  uPort,
                             UINT16 uValue,
                             void *  pContext);

    /* Function pointer to submit reset TT request */
    USBHST_STATUS (*resetTT)(UINT32 hHighSpeedHub,
                             UINT8  uPort,
                             void *  pContext);



    } USBHST_HUB_FUNCTION_LIST, *pUSBHST_HUB_FUNCTION_LIST;


typedef union usbHstFunctionList
{

    USBHST_USBD_TO_HUB_FUNCTION_LIST    UsbdToHubFunctionList;
    USBHST_HUB_FUNCTION_LIST             HubFunctionList;

}USBHST_FUNCTION_LIST, *pUSBHST_FUNCTION_LIST;

/* This structure contains the HC driver function pointers */
typedef struct usbhst_hc_driver
    {
    /* Number of bus for this host controller */
    UINT8       uNumberOfBus;

    /* Function pointer to get the frame number */
    USBHST_STATUS   (*getFrameNumber)  (UINT8   uBusIndex,
                                        UINT16 *puFrameNumber);

    /* Function pointer to set the bit rate */
    USBHST_STATUS   (*setBitRate)      (UINT8   uBusIndex,
                                        BOOL    bIncrement,
                                        UINT32 *puCurrentFrameWidth);

    /* Function pointer to check if required bandwidth is avilable */
    USBHST_STATUS   (*isBandwidthAvailable)
                                           (UINT8   uBusIndex,
                                            UINT8   uDeviceAddress,
                                            UINT8   uDeviceSpeed,
                                            UCHAR  *pCurrentDescriptor,
                                            UCHAR  *pNewDescriptor);

    /* Function pointer to create a pipe */
    USBHST_STATUS   (*createPipe)      (UINT8   uBusIndex,
                                        UINT8   uDeviceAddress,
                                        UINT8   uDeviceSpeed,
                                        UCHAR  *pEndPointDescriptor,
                                        UINT16  uHighSpeedHubInfo,
                                        UINT32 *puPipeHandle);

    /* Function pointer to modify the default pipe */
    USBHST_STATUS   (*modifyDefaultPipe)
                                           (UINT8   uBusIndex,
                                            UINT32  uDefaultPipeHandle,
                                            UINT8   uDeviceSpeed,
                                            UINT8   uMaxPacketSize,
                                            UINT16   uHighSpeedHubInfo);

    /* Function pointer to delete a pipe */
    USBHST_STATUS   (*deletePipe)      (UINT8   uBusIndex,
                                        UINT32  uPipeHandle);

    /* Function pointer to check if there is any request pending on the pipe */
    USBHST_STATUS   (*isRequestPending)(UINT8   uBusIndex,
                                        UINT32  uPipeHandle);

    /* Function pointer to submit USB request  */
    USBHST_STATUS   (*submitURB)       (UINT8          uBusIndex,
                                        UINT32         uPipeHandle,
                                        pUSBHST_URB    pURB);

    /* Function pointer to cancel USB request */
    USBHST_STATUS   (*cancelURB)       (UINT8          uBusIndex,
                                        UINT32         uPipeHandle,
                                        pUSBHST_URB    pURB);

 

  /* Function pointer to submit the status of the clear TT request */
    USBHST_STATUS (*clearTTRequestComplete)(UINT8         uRelativeBusIndex,
                                            void *         pContext,
                                            USBHST_STATUS nStatus);
    /* Function pointer to submit the status of the reset TT request */
    USBHST_STATUS (*resetTTRequestComplete)(UINT8         uRelativeBusIndex,
                                            void *         pContext,
                                            USBHST_STATUS nStatus);


    } USBHST_HC_DRIVER, *pUSBHST_HC_DRIVER;



/**************************** FUNCTIONS DECLARATION ***************************/


/*******************************************************************************
 * Function Name    : USBHST_RegisterDriver
 * Description      : This function registers the class driver with the USB
 *                    Host Stack. This function also associates the devices that
 *                    can be supported by this class driver.
 * Parameters       : pDeviceDriverInfo IN      Information about the class
 *                                              driver being registered.
 *                    pContext          IN OUT  For Non-Hub Class Drivers:
 *                                              Not Interpreted.
 *                                              For Hub Class Drivers:
 *                                              IN: Pointer to structure
 *                                              containing function pointers
 *                                              required by USBD from Hub Driver
 *                                              OUT: Pointer to structure
 *                                              containing function pointers
 *                                              required by Hub Driver from USBD
 * Return Type      : USBHST_INVALID_PARAMETER if Parameters are not valid.
 *                    USBHST_INSUFFICIENT_MEMORY Memory could not be allocated
 *                    USBHST_FAILURE if Driver is already registered
 *                    USBHST_SUCCESS if Class driver is registered successfully.
 ******************************************************************************/
extern USBHST_STATUS usbHstDriverRegister
    (
    pUSBHST_DEVICE_DRIVER pDeviceDriverInfo,
    void                 **pContext
    );

/*******************************************************************************
 * Function Name    : USBHST_DeregisterDriver
 * Description      : This function deregisters the class driver from the USB
 *                    Host Stack. The class driver is unloaded for all the
 *                    supported devices
 * Parameters       : pDeviceDriverInfo IN  Information about the class driver
 *                                          being deregistered
 * Return Type      : USBHST_INVALID_PARAMETER if Parameters are not valid.
 *                    USBHST_FAILURE if Driver is not found or if it is a hub
 *                    class driver and there are some functional devices present
 *                    USBHST_SUCCESS if class driver deregistered successfully.
 ******************************************************************************/
extern USBHST_STATUS usbHstDriverDeregister  
    (
    pUSBHST_DEVICE_DRIVER pDeviceDriverInfo
    );

/*******************************************************************************
 * Function Name    : USBHST_RegisterHCD
 * Description      : This function is used to register a Host Controller
 *                    Driver with the USB Host Stack.
 * Parameters       : pHCDriver     IN  Information about the Host Controller
 *                                      Driver being registered.
 *                    phHCDriver    OUT Parameter to hold the handle for HC
 *                                      Driver.
 * Return Type      : USBHST_INVALID_PARAMETER if Parameters are not valid.
 *                    USBHST_INSUFFICIENT_RESOURCE if Resources are not
 *                     sufficient.
 *                    USBHST_SUCCESS if Host Controller Driver is registered
 *                    successfully.
 ******************************************************************************/
extern USBHST_STATUS usbHstHCDRegister
    (
    pUSBHST_HC_DRIVER   pHCDriver,
    UINT32              *phHCDriver,
    void *              pContext
    );

/*******************************************************************************
 * Function Name    : USBHST_DeregisterHCD
 * Description      : This function is used to deregister a Host Controller
 *                    Driver from the USB Host Stack.
 * Parameters       : hHCDriver    IN  Handle for HC Driver being deregistered.
 * Return Type      : USBHST_INVALID_PARAMETER if Parameters are not valid.
 *                    USBHST_FAILURE if bus count is not zero
 *                    USBHST_SUCCESS if Host Controller Driver is deregistered
 *                    successfully.
 ******************************************************************************/
extern USBHST_STATUS usbHstHCDDeregister
    (
    UINT32 hHCDriver
    );

/*******************************************************************************
 * Function Name    : USBHST_RegisterBus
 * Description      : This function registers an USB Bus corresponding to the
 *                    host controller.
 * Parameters       : hHCDriver     IN  Handle for HC Driver requesting
 *                                      registration for its bus.
 *                    uSpeed        IN  Speed of the USB Bus
 *                    hDefaultPipe  IN  Handle to the default pipe for the USB
 *                                      Bus
 * Return Type      : USBHST_INVALID_PARAMETERS if Parameters are not valid.
 *                    USBHST_INSUFFICIENT_RESOURCES if Resources are not
 *                    sufficient for this operation.
 *                    USBHST_FAILURE   USB Bus is already registered.
 *                    USBHST_SUCCESS   USB Bus is registered successfully.
 ******************************************************************************/
extern USBHST_STATUS usbHstBusRegister
    (
    UINT32    hHCDriver,
    UINT8     uSpeed,
    UINT32    hDefaultPipe
    );

/*******************************************************************************
 * Function Name    : USBHST_DeregisterBus
 * Description      : This function deregisters an USB Bus corresponding to the
 *                    host controller.
 * Parameters       : hHCDriver         IN  Handle for HC Driver requesting
 *                                          deregistration for its bus.
 *                    uBusIndex         IN  Relative Index of the Bus being
 *                                          deregistered.
 *                    hDefaultPipe      IN  Handle to the default pipe for the
 *                                          USB Bus
 * Return Type      : USBHST_INVALID_PARAMETERS if Parameters are not valid.
 *                    USBHST_FAILURE when Attempt to deregister the USB Bus
 *                    while there are functional devices on it.
 *                    USBHST_SUCCESS if USB Bus is deregistered successfully.
 ******************************************************************************/
extern USBHST_STATUS usbHstBusDeregister
    (
    UINT32  hHCDriver,
    UINT32  uBusIndex,
    UINT32  hDefaultPipe
    );

/*******************************************************************************
 * Function Name    : USBHST_SelectiveSuspend
 * Description      : This function is called by the class drivers to
 *                    selectively suspend its device.
 * Parameters       : hDevice   IN  Handle to the device to be suspended
 *                    selectively.
 * Return Type      : USBHST_INVALID_PARAMETERS if Parameters are not valid.
 *                    USBHST_SUCCESS   Hub driver is informed successfully about
 *                    selective suspend of the device.
 ******************************************************************************/
extern USBHST_STATUS usbHstSelectiveSuspend
    (
    UINT32 hDevice
    );

/*******************************************************************************
 * Function Name    : USBHST_SelectiveResume
 * Description      : This function is called by the class drivers to
 *                    selectively resume its device.
 * Parameters       : hDevice   IN  Handle to the device to be resumed
 *                                  selectively.
 * Return Type      : USBHST_INVALID_PARAMETERS if Parameters are not valid.
 *                    USBHST_SUCCESS   Hub driver is informed successfully about
 *                    selective resume of the device.
 ******************************************************************************/
extern USBHST_STATUS usbHstSelectiveResume
    (
    UINT32 hDevice
    );

/*******************************************************************************
 * Function Name    : USBHST_ResetDevice
 * Description      : This function is called by the class drivers to
 *                    reset its device.
 * Parameters       : hDevice   IN  Handle to the device to be reset.
 * Return Type      : USBHST_INVALID_PARAMETERS if Parameters are not valid.
 *                    USBHST_SUCCESS   Hub driver is called successfully
 *                       to reset the device.
 ******************************************************************************/
extern USBHST_STATUS usbHstResetDevice
    (
    UINT32 hDevice
    );

/*******************************************************************************
 * Function Name    : USBHST_GetDescriptor
 * Description      : This function is used to issue GET_DESCRIPTOR USB
 *                    Standard Request.
 * Parameters       : hDevice       IN  Handle to USB Device.
 *                    uDescType     IN  Specify the type of descriptor.
 *                    uDescIndex    IN  Index of configuration or string
 *                                      descriptor.
 *                    swLangID      IN  Language ID in case of string descriptor
 *                    uSize         IN  Number of bytes of descriptor to fetch.
 *                    pBuffer       OUT Pointer to buffer to hold the fetched
 *                                      data.
 * Return Type      : USBHST_INVALID_PARAMETERS if parameters are not valid.
 *                    USBHST_INSUFFICIENT_MEMORY on insufficient resource.
 *                    USBHST_TIMEOUT if request submitted to Host controller
 *                    driver is timed out.
 *                    USBHST_SUCCESS if data for the descriptor is
 *                    successfully fetched
 ******************************************************************************/
extern USBHST_STATUS usbHstGetDescriptor
    (
    UINT32     hDevice,
    UINT8      uDescType,
    UINT8      uDescIndex,
    UINT16     swLangID,
    UINT32    *pSize,
    UCHAR     *pBuffer
    );

/*******************************************************************************
 * Function Name    : USBHST_GetStatus
 * Description      : This function is used to issue GetStatus USB
 *                    Standard Request.
 * Parameters       : hDevice       IN  Handle to USB Device.
 *                    uRecipient    IN  Specify the desired recipient.
 *                    uIndex        IN  Specify the index of recipient info list
 *                    pBuffer       OUT Pointer to buffer to hold the fetched
 *                                      data.
 * Return Type      : USBHST_INVALID_PARAMETERS if parameters are not valid.
 *                    USBHST_INSUFFICIENT_MEMORY on insufficient resource.
 *                    USBHST_TIMEOUT if request submitted to Host controller
 *                    driver is timed out.
 *                    USBHST_SUCCESS if Status of the requested recipient is
 *                    fetched successfully.
 ******************************************************************************/
extern USBHST_STATUS usbHstGetStatus
    (
    UINT32  hDevice,
    UINT8   uRecipient,
    UINT16  uIndex,
    UCHAR  *pBuffer
    );

/*******************************************************************************
 * Function Name    : USBHST_ClearFeature
 * Description      : This function is used to issue ClearFeature USB
 *                    Standard Request.
 * Parameters       : hDevice       IN  Handle to USB Device.
 *                    uRecipient    IN  Specify the desired recipient.
 *                    uIndex        IN  Specify the index of recipient info list
 *                    uFeature      IN  Specify the feature selector
 * Return Type      : USBHST_INVALID_PARAMETERS if parameters are not valid.
 *                    USBHST_INSUFFICIENT_MEMORY on insufficient resource.
 *                    USBHST_TIMEOUT if request submitted to Host controller
 *                    driver is timed out.
 *                    USBHST_SUCCESS if Requested feature is cleared
 *                    successfully.
 ******************************************************************************/
extern USBHST_STATUS usbHstClearFeature
    (
    UINT32   hDevice,
    UINT8    uRecipient,
    UINT16   uIndex,
    UINT16   uFeature
    );

/*******************************************************************************
 * Function Name    : USBHST_GetConfiguration
 * Description      : This function is used to issue GetConfiguration USB
 *                    Standard Request.
 * Parameters       : hDevice       IN  Handle to USB Device.
 *                    pBuffer       OUT Pointer to buffer to hold the fetched
 *                                      data.
 * Return Type      : USBHST_INVALID_PARAMETERS if parameters are not valid.
 *                    USBHST_INSUFFICIENT_MEMORY on insufficient resource.
 *                    USBHST_TIMEOUT if request submitted to Host controller
 *                    driver is timed out.
 *                    USBHST_SUCCESS if current configuration is successfully
 *                    fetched
 ******************************************************************************/
extern USBHST_STATUS usbHstGetConfiguration
    (
    UINT32   hDevice,
    UCHAR   *pBuffer
    );

/*******************************************************************************
 * Function Name    : USBHST_GetInterface
 * Description      : This function is used to issue GetInterface USB
 *                    Standard Request.
 * Parameters       : hDevice          IN  Handle to USB Device.
 *                    uInterfaceNumber IN  Specify the interface number.
 *                    pBuffer          OUT Pointer to buffer to hold the fetched
 *                                         data.
 * Return Type      : USBHST_INVALID_PARAMETERS if parameters are not valid.
 *                    USBHST_INSUFFICIENT_MEMORY on insufficient resource.
 *                    USBHST_TIMEOUT if request submitted to Host controller
 *                    driver is timed out.
 *                    USBHST_SUCCESS if Selected alternate setting for a
 *                    requested interface is fetched successfully.
 ******************************************************************************/
extern USBHST_STATUS usbHstGetInterface
    (
    UINT32       hDevice,
    UINT16       uInterfaceNumber,
    UCHAR       *pBuffer
    );

/*******************************************************************************
 * Function Name    : USBHST_SetConfiguration
 * Description      : This function is used to issue SetConfiguration USB
 *                    Standard Request.
 * Parameters       : hDevice       IN  Handle to USB Device.
 *                    uIndex        IN  Specify the Configuration index.
 * Return Type      : USBHST_INVALID_PARAMETERS if parameters are not valid.
 *                    USBHST_INSUFFICIENT_MEMORY on insufficient resource.
 *                    USBHST_TIMEOUT if request submitted to Host controller
 *                    driver is timed out.
 *                    USBHST_INSUFFICIENT_BANDWIDTH if Insufficient bandwidth
 *                    for new configuration set up.
 *                    USBHST_INSUFFICIENT_RESOURCES Insufficient power
 *                    resources from the hub to support new configuration.
 *                    USBHST_SUCCESS if Configuration is set successfully..
 ******************************************************************************/
extern USBHST_STATUS usbHstSetConfiguration
    (
    UINT32 hDevice,
    UINT16 uIndex
    );

/*******************************************************************************
 * Function Name    : USBHST_SetFeature
 * Description      : This function is used to issue SetFeature USB
 *                    Standard Request.
 * Parameters       : hDevice       IN  Handle to USB Device.
 *                    uRecipient    IN  Specify the desired recipient.
 *                    uIndex        IN  Specify the index of recipient info list
 *                    uFeature      IN  Specify the feature selector
 *                    uTestSlector  IN  Specifiy the test selector in case the
 *                                      uFeature is USBHST_TEST_MODE
 * Return Type      : USBHST_INVALID_PARAMETERS if parameters are not valid.
 *                    USBHST_INSUFFICIENT_MEMORY on insufficient resource.
 *                    USBHST_TIMEOUT if request submitted to Host controller
 *                    driver is timed out.
 *                    USBHST_SUCCESS if Requested feature is set successfully.
 ******************************************************************************/
extern USBHST_STATUS usbHstSetFeature
    (
    UINT32     hDevice,
    UINT8      uRecipient,
    UINT16     uIndex,
    UINT16     uFeature,
    UINT8      uTestSelector
    );

/*******************************************************************************
 * Function Name    : USBHST_SetDescriptor
 * Description      : This function is used to issue SetDescriptor USB
 *                    Standard Request.
 * Parameters       : hDevice       IN  Handle to USB Device.
 *                    uDescType     IN  Type of descriptor to set.
 *                    uDescIndex    IN  Parameter to hold the device handle.
 *                    swLangID      IN  Language ID in case of string descriptor
 *                    uSize         IN  Number of bytes of descriptor to fetch.
 *                    pBuffer       OUT Buffer Pointer containing data.
 * Return Type      : USBHST_INVALID_PARAMETERS if parameters are not valid.
 *                    USBHST_INSUFFICIENT_MEMORY on insufficient resource.
 *                    USBHST_TIMEOUT if request submitted to Host controller
 *                    driver is timed out.
 *                    USBHST_SUCCESS if the descriptor is set successfully.
 ******************************************************************************/
extern USBHST_STATUS usbHstSetDescriptor
    (
    UINT32  hDevice,
    UINT8   uDescType,
    UINT8   uDescIndex,
    UINT16  swLangID,
    UCHAR  *pBuffer,
    UINT32  uSize
    );

/*******************************************************************************
 * Function Name    : USBHST_SetInterface
 * Description      : This function is used to issue SetInterface USB
 *                    Standard Request.
 * Parameters       : hDevice       IN  Handle to USB Device.                .
 *                    uIndex        IN  Specify the interface number.
 *                    uAltIndex     IN  Specify the alternate interface
 *                                      setting value.
 * Return Type      : USBHST_INVALID_PARAMETERS if parameters are not valid.
 *                    USBHST_INSUFFICIENT_MEMORY on insufficient resource.
 *                    USBHST_TIMEOUT if request submitted to Host controller
 *                    driver is timed out.
 *                    USBHST_SUCCESS if the interface is set successfully
 ******************************************************************************/
extern USBHST_STATUS usbHstSetInterface
    (
    UINT32  hDevice,
    UINT16  uIndex,
    UINT16  uAltIndex
    );

/*******************************************************************************
 * Function Name    : USBHST_SetSynchFrame
 * Description      : This function is used to issue SetSynchFrame USB
 *                    Standard Request.
 * Parameters       : hDevice           IN  Handle to USB Device.
 *                    uEndPointNumber   IN  Specify the endpoint number
 *                    pBuffer           OUT Pointer to buffer to hold the
 *                                          synchronization frame number
 * Return Type      : USBHST_INVALID_PARAMETERS if parameters are not valid.
 *                    USBHST_INSUFFICIENT_MEMORY on insufficient resource.
 *                    USBHST_TIMEOUT if request submitted to Host controller
 *                    driver is timed out.
 *                    USBHST_SUCCESS if Synchronization frame for the requested
 *                    end point is set and reported successfully.
 ******************************************************************************/
extern USBHST_STATUS usbHstSetSynchFrame
    (
    UINT32     hDevice,
    UINT16     uEndPointNumber,
    UCHAR     *pBuffer
    );

/*******************************************************************************
 * Function Name    : USBHST_SubmitURB.
 * Description      : This function is used to submit the USB request to HCD.
 * Parameters       : pURB  IN  USB request block structure.
 * Return Type      : USBHST_INVALID_PARAMETERS if Parameters are not valid.
 *                    USBHST_SUCCESS   URB is successfully submitted to the Host
 *                    Controller Driver.
 ******************************************************************************/
extern USBHST_STATUS usbHstURBSubmit
    (
    USBHST_URB *pURB
    );

/*******************************************************************************
 * Function Name    : USBHST_CancelURB.
 * Description      : This function is used to cancel the request submitted to
 *                    HCD.
 * Parameters       : pURB  IN  USB request block structure.
 * Return Type      : USBHST_INVALID_PARAMETERS if Parameters are not valid.
 *                    USBHST_SUCCESS   Host controller driver is requested
 *                    successfully to cancel the URB.
 ******************************************************************************/
extern USBHST_STATUS usbHstURBCancel
    (
    USBHST_URB *pURB
    );

/*******************************************************************************
 * Function Name    : USBHST_SetBitRate
 * Description      : This function is used to set the bit rate of specified bus
 * Parameters       : hDevice              IN   Handle to  device requesting
 *                                              this operation.
 *                    bIncrement           IN   Boolean Flag:
 *                                              0:Decrement the bit rate.
 *                                              1:Increment the bit rate.
 *                    pCurrentFrameWidth   OUT  Pointer to hold the current bit
 *                                              rate.
 * Return Type      : USBHST_INVALID_PARAMETERS if Parameters are not valid.
 *                    USBHST_SUCCESS  Bit rate is set successfully.
 ******************************************************************************/
extern USBHST_STATUS usbHstSetBitRate
    (
    UINT32   hDevice,
    BOOL     bIncrement,
    UINT32  *pCurrentFrameWidth
    );

/*******************************************************************************
 * Function Name    : USBHST_GetFrameNumber
 * Description      : This function is used to get the current frame number
 *                    on specified bus.
 * Parameters       : hDevice       IN  Handle to the device requesting this
 *                                      operation.
 *                    pFrameNumber  OUT Pointer to hold the frame number.
 * Return Type      : USBHST_INVALID_PARAMETERS if Parameters are not valid.
 *                    USBHST_SUCCESS if Frame number is fetched successfully.
 ******************************************************************************/
extern USBHST_STATUS usbHstGetFrameNumber
    (
    UINT32  hDevice,
    UINT16 *pFrameNumber
    );

/*******************************************************************************
 * Function Name    : USBD_Init
 * Description      : This function initializes the global variables.
 * Parameters       : None
 * Return Type      : a) USBHST_FAILURE if USBD could not be initialized.
 *                    b) USBHST_SUCCESS if all the global variables are
 *                       properly initialized.
 ******************************************************************************/
extern USBHST_STATUS usbdInit(void);

/*******************************************************************************
 * Function Name    : USBD_Exit
 * Description      : This function does the clean up.
 * Parameters       : None
 * Return Type      : a) USBHST_FAILURE if bus count is not zero.
 *                    b) USBHST_SUCCESS when USBD successfully exited.              
 ******************************************************************************/
extern USBHST_STATUS usbdExit(void);

#ifdef __cplusplus
}
#endif /* __cplusplus */ 

#endif /* End of __USBHST_H__*/

/**************************** End of file Usbhst.h ****************************/
