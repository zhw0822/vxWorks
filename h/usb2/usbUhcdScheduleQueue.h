/* usbUhcdScheduleQueue.h - header for scheduling and queuing USB UHCD HCD */

/* Copyright 2004 - 2005 Wind River Systems, Inc. 

   This software includes software licensed to Wind River Systems, Inc.
   by Wipro, Ltd. Wind River licensees may use this software according  
   to the terms of their Wind River license agreement(s) applicable to 
   this software.
*/

/*
Modification history
--------------------
01e,28mar05,pdg  non-PCI changes
01d,03mar05,mta  SPR 96604
01c,18jan05,ami  New member outTokScheduled added (SPR #104979 added)
01b,26jun03,mat changing the code to WRS standards.
01a,25apr03,ram written.
*/

/*
DESCRIPTION

This file contains the data structures maintained by the USB UHCD and the HCD 
and the prototypes of the functions used for the scheduling and the queuing 
of transfers requests.
*/


/*
INTERNAL
 *******************************************************************************
 * Filename         : usbUhcdScheduleQueue.h
 *
 * Copyright        :
 *
 * THE COPYRIGHT IN THE CONTENTS OF THIS SOFTWARE VEST WITH WIPRO
 * LIMITED A COMPANY INCORPORATED UNDER THE LAWS OF INDIA AND HAVING
 * ITS REGISTERED OFFICE AT DODDAKANNELLI SARJAPUR ROAD  BANGALORE
 * 560 035. DISTRIBUTION OR COPYING OF THIS SOFTWARE BY
 * ANY INDIVIDUAL OR ENTITY OTHER THAN THE ADDRESSEE IS STRICTLY
 * PROHIBITED AND MAY INCUR LEGAL LIABILITY. IF YOU ARE NOT THE
 * ADDRESSEE PLEASE NOTIFY US IMMEDIATELY BY PHONE OR BY RETURN EMAIL.
 * THE ADDRESSEE IS ADVISED TO MAINTAIN THE PROPRIETARY INTERESTS OF
 * THIS COPYRIGHT AS PER APPLICABLE LAWS.
 *
 *
 * Description      :  This file contains the data structures maintained by the
 *                     HC and the HCD and the prototypes of the functions used
 *                     for the scheduling and the queuing of transfers requests.
 *
 *
 */
#ifndef __INCusbUhcdScheduleQueueh
#define __INCusbUhcdScheduleQueueh

#ifdef __cplusplus
extern "C" {
#endif

/* includes */

#include "usbOsal.h"
#include "usbHst.h"
#include "BusAbstractionLayer.h"
#include "usbPciLib.h"

/* defines */

/*
 * UHCD link pointers, including frame list pointers and the pointers found
 * in USB_UHCD_TD and USB_UHCD_QH structures, use the same basic structure as defined 
 * below.  The exception is the VF (Depth/Breadth select) which is only used
 * in the USB_UHCD_TD structure.
 */

#define USBUHCD_LINK_PTR_MASK	0xfffffff0  /* For the frame list ptr : Not used
                                                                      directly*/
#define USBUHCD_LINK_PTR_ROT	4

#define USBUHCD_LINK_VF		0x00000004  /* for the vf bit */
					    /* 1=Depth first, 0=breadth first */


#define USBUHCD_LINK_QH		0x00000002  /* For the q bit */

#define USBUHCD_LINK_TD		0x00000000  /* 1=QH, 0=TD */


#define USBUHCD_LINK_TERMINATE	0x00000001  /*for the t bit */
                        		    /* 1=End of list, 0=pointer valid */
                        		    
/* Macros to retrieve/format link ptr */

#define USBUHCD_LINK_PTR(x) \
    (((x) & USBUHCD_LINK_PTR_MASK) >> USBUHCD_LINK_PTR_ROT)

#define USBUHCD_LINK_PTR_FMT(x) \
    (((x) << USBUHCD_LINK_PTR_ROT) & USBUHCD_LINK_PTR_MASK)

/* Frame List related macros */			

#define USBUHCD_FRAME_LIST_ENTRIES     1024
#define USBUHCD_FRAME_LIST_ALIGNMENT   4096

/* Transfer Descriptor related macros */

#define UHCI_TD_LEN	32	    /* TD length required by UHCD hardware */
#define UHCI_TD_ACTLEN	sizeof (USB_UHCD_TD)

/*
 * USB_UHCD_TD.dWord1Td definitions.
 */

#define USBUHCD_TDCS_SHORT 	0x20000000  /* Short Packet Detect */
					    /* 1=Enable, 0=Disable */

#define USBUHCD_TDCS_ERRCTR_MASK	0x18000000  /* Error Counter Mask*/
#define USBUHCD_TDCS_ERRCTR_NOLIM	0x00000000  /* No limit */
#define USBUHCD_TDCS_ERRCTR_1ERR	0x08000000  /* 1 error */
#define USBUHCD_TDCS_ERRCTR_2ERR	0x10000000  /* 2 errors */
#define USBUHCD_TDCS_ERRCTR_3ERR	0x18000000  /* 3 errors : used to set  
                                                                                     error count*/

#define USBUHCD_TDCS_LOWSPEED	0x04000000  /* Low Speed Device */
					    /* 1=low speed dev, 0=full speed */
#define USBUHCD_TDCS_ISOCH 	0x02000000  /* Isochronous Select */
					    /* 1=isoch TD, 0=non-isoch TD */
#define USBUHCD_TDCS_COMPLETE	0x01000000  /* Interrupt on Complete */
					    /* 1=Issue Ioc*/


/* USB_UHCD_TD.dWord1Td Status bits */
#define USBUHCD_TDCS_STS_MASK	0x00ff0000  /* Mask for status bits */
#define USBUHCD_TDCS_STS_ACTIVE	0x00800000  /* Active */
#define USBUHCD_TDCS_STS_STALLED	0x00400000  /* Stalled */
#define USBUHCD_TDCS_STS_DBUFERR	0x00200000  /* Data buffer error */
#define USBUHCD_TDCS_STS_BABBLE	0x00100000  /* Babble Detected */
#define USBUHCD_TDCS_STS_NAK	0x00080000  /* NAK Received */
#define USBUHCD_TDCS_STS_TIME_CRC	0x00040000  /* CRC/Timeout error */
#define USBUHCD_TDCS_STS_BITSTUFF	0x00020000  /* Bitstuff error */


/* USB_UHCD_TD.dWord1Td Actlen Related macros */

 
#define USBUHCD_TDCS_ACTLEN_MASK	0x000007ff  /* Actual Length mask */
#define USBUHCD_TDCS_ACTLEN_ROT	0	    /* Actual length bit loc */

#define USBUHCD_PACKET_SIZE_MASK	0x000007ff  /* Mask for 11-bit field */


/* Macro to retrieve USBUHCD_TDCS_ACTLEN
 *
 * NOTE: UHCD actlen is one less than the real length.	
 * The macro compensates automatically.
 * 
 */

#define USBUHCD_TDCS_ACTLEN(x) \
    ((((x) >> USBUHCD_TDCS_ACTLEN_ROT) ) & USBUHCD_PACKET_SIZE_MASK)

/* USB_UHCD_TD.dWord1Td Token related macros */


#define USBUHCD_TDTOK_MAXLEN_MASK	0xffe00000  /* Maximum length mask */
#define USBUHCD_TDTOK_MAXLEN_ROT	21	    /* Maximum length bit loc */

#define USBUHCD_TDTOK_DATA_TOGGLE	0x00080000  /* Data Toggle */
					    /* 0=DATA0, 1=DATA1 */

#define USBUHCD_TDTOK_ENDPT_MASK	0x00078000  /* Endpoint Mask */
#define USBUHCD_TDTOK_ENDPT_ROT	15	    /* Endpoint bit loc */

#define USBUHCD_TDTOK_DEVADRS_MASK 0x00007f00  /* Device Address Mask */
#define USBUHCD_TDTOK_DEVADRS_ROT	8	    /* Device Address bit loc */

#define USBUHCD_TDTOK_PID_MASK	0x000000ff  /* Packet ID mask */
#define USBUHCD_TDTOK_PID_ROT	0	    /* Packet ID bit loc */


/* Macros to retrieve/format USBUHCD_TDTOK_MAXLEN 
 *
 * NOTE: UHCD maxlen is one less than the real length.	The macro 
 * compensates automatically.
 */


#define USBUHCD_TDTOK_MAXLEN(x) \
    ((((x) >> USBUHCD_TDTOK_MAXLEN_ROT) ) & USBUHCD_PACKET_SIZE_MASK)



#define USBUHCD_TDTOK_MAXLEN_FMT(x) \
    ((((x)) & USBUHCD_PACKET_SIZE_MASK) << USBUHCD_TDTOK_MAXLEN_ROT)


/* Macros to retrieve/format USBUHCD_TDTOK_ENDPT */



#define USBUHCD_TDTOK_ENDPT(x) \
    (((x) & USBUHCD_TDTOK_ENDPT_MASK) >> USBUHCD_TDTOK_ENDPT_ROT)



#define USBUHCD_TDTOK_ENDPT_FMT(x) \
    (((x) << USBUHCD_TDTOK_ENDPT_ROT) & USBUHCD_TDTOK_ENDPT_MASK)


/* Macros to retrieve/format USBUHCD_TDTOK_DEVADRS */



#define USBUHCD_TDTOK_DEVADRS(x) \
    (((x) & USBUHCD_TDTOK_DEVADRS_MASK) >> USBUHCD_TDTOK_DEVADRS_ROT)



#define USBUHCD_TDTOK_DEVADRS_FMT(x) \
    (((x) << USBUHCD_TDTOK_DEVADRS_ROT) & USBUHCD_TDTOK_DEVADRS_MASK)



/* Macros to retrieve/format USBUHCD_TDTOK_PID */

#define USBUHCD_TDTOK_PID(x) \
    (((x) & USBUHCD_TDTOK_PID_MASK) >> USBUHCD_TDTOK_PID_ROT)

#define USBUHCD_TDTOK_PID_FMT(x) \
    (((x) << USBUHCD_TDTOK_PID_ROT) & USBUHCD_TDTOK_PID_MASK)

/* Queue Head related macros */

#define UHCI_QH_ALIGNMENT	    16
#define UHCI_QH_LEN	8	    /* QH length required by UHCI hardware */
#define UHCI_QH_ACTLEN	sizeof (USB_UHCD_QH)


/* PCI pointer macros */

#define TO_PCIPTR(p)         TO_LITTLEL(USB_MEM_TO_PCI (p))
#define QH_TO_PCIPTR(p)      (TO_PCIPTR (p) | TO_LITTLEL (USBUHCD_LINK_QH))
#define TD_TO_PCIPTR(p)      (TO_PCIPTR (p))

#define FROM_PCIPTR(d)       USB_PCI_TO_MEM((FROM_LITTLEL(d)))

#define QH_FROM_PCIPTR(d)     \
        ((PUSB_UHCD_QH) (FROM_PCIPTR ( FROM_LITTLEL (d)  &  USBUHCD_LINK_PTR_MASK)))

#define TD_FROM_PCIPTR(d)    \
        ((PUSB_UHCD_TD) (FROM_PCIPTR ( FROM_LITTLEL (d)  &  USBUHCD_LINK_PTR_MASK)))   

#define USBUHCD_END_OF_LIST(INDEX)   USB_UHCD_SWAP_DATA(INDEX, USBUHCD_LINK_TERMINATE)


/* interrupt bits */

#define UHCI_STS_HALTED     0x0020  /* Host Controller Halted */
#define UHCI_STS_PROCERR    0x0010  /* Host Controller Process Error */
#define UHCI_STS_HOSTERR    0x0008  /* Host System Error */
#define UHCI_STS_RESUME     0x0004  /* Resume Detect */
#define UHCI_STS_USBERR     0x0002  /* USB Error Interrupt */
#define UHCI_STS_USBINT     0x0001  /* USB Interrupt */

#define UHC_INT_PENDING_MASK	(UHCI_STS_PROCERR | UHCI_STS_HOSTERR | \
				 UHCI_STS_RESUME | UHCI_STS_USBERR | \
				 UHCI_STS_USBINT)


#if (_BYTE_ORDER == _BIG_ENDIAN)

#define FROM_LITTLEW(w) 	(MSB((w)) | (LSB((w)) << 8))
#define FROM_LITTLEL(l) 	(LONGSWAP((l)))
#define FROM_BIGW(w)		(w)
#define FROM_BIGL(l)		(l)

#else

#define FROM_LITTLEW(w) 	(w)
#define FROM_LITTLEL(l) 	(l)
#define FROM_BIGW(w)		(MSB((w)) | (LSB((w)) << 8))
#define FROM_BIGL(l)		(LONGSWAP((l)))

#endif


#define TO_LITTLEW(w)		FROM_LITTLEW((w))
#define TO_LITTLEL(l)		FROM_LITTLEL((l))
#define TO_BIGW(w)		FROM_BIGW((w))
#define TO_BIGL(w)		FROM_BIGL((l))

/* typedefs */

/*
 * Structure holding the format of the Transfer Descriptor
 * This is to be created for every packet to be transmitted in the USB bus.
 */

typedef struct usbUhcdTd
    {

    /* HC understood Transfer Descriptor format - start */

    UINT32 dWord0Td;   /* DWORD 0 of TD */

    UINT32 dWord1Td;   /* DWORD 1 of TD */

    UINT32 dWord2Td;   /* DWORD 2 of TD */

    UINT32 dWord3Td;   /* DWORD 3 of TD */

    /* HC understood Transfer Descriptor format - End */
    /* HCD understood Transfer Descriptor format - start */

    /*
     * When the TD is to be freed, this address is to be used.
     * This will hold the non-aligned memory address(if allocated memory
     * is not aligned to 16 bytes) or an aligned memory address.
     */
    UINT  offsetForFreeing;

    /* Pointer to the next TD in the queue of requests created by the HCD */

    struct usbUhcdTd *hNextTd;

    /* Pointer to the previous TD in the queue of requests created by the HCD */

    struct usbUhcdTd *hPrevTd;

    /* Pointer to the next TD in the queue maintained by the HC */

    struct usbUhcdTd *vNextTd;

    /* Pointer to the previous TD in the queue maintained by the HC */

    struct usbUhcdTd *vPrevTd;
    
    UINT8 toggle;
    /* HCD understood Transfer Descriptor format - End */

    } USB_UHCD_TD;

typedef struct usbUhcdTd  *PUSB_UHCD_TD;

/*
 * Structure holding the format of the Queue Head.
 * This is to be created for every bulk, control and interrupt endpoint
 * created.
 */

typedef struct usbUhcdQh
    {

    /* HC understood Queue Head format - start */

    UINT32 dWord0Qh;     /* DWORD 0 of QH */

    UINT32 dWord1Qh;     /* DWORD 1 of QH */

    /* HC understood Queue Head format - End */

    /* HCD maintained Queue Head format - start */
    /*
     * When the QH is to be freed, this address is to be used.
     * This will hold the non-aligned memory address(if allocated memory
     * is not aligned to 16 bytes) or an aligned memory address.
     */

    UINT offsetForFreeing;

    /* Pointer to the TD which is attached to the queue */

    USB_UHCD_TD *td;

    /* Pointer to the previous QH in the schedule */

    struct usbUhcdQh *prevQh;

    /* Pointer to the next QH in the schedule */

    struct usbUhcdQh *nextQh;

    /* To hold the data toggle field */

    UINT8 toggle;

    /* To hold the flag indicating whether the endpoint is halted */

    UINT8 halted;

    /* flag indicating whether the endpoint is marked for deletion */

    UINT8 markedForDeletion;

    /* Pointer to the TD which is at the last in the queue */

    USB_UHCD_TD *tdLast;

    /* HCD maintained Queue Head format - End */

    } USB_UHCD_QH;

typedef struct usbUhcdQh  *PUSB_UHCD_QH;

/*
 * Structure holding the format of the frame list of the HC
 * Each element in the list is accessed by the HC on every frame.
 */

typedef struct usbUhcdFrameList
    {
    UINT32 pfl[1024];
    } USB_UHCD_FRAME_LIST;

/*
 * The UHCD HCD maintains an array of 1024 elements to hold the
 * information about the isochronous TDs in a frame.
 * This structure holds information about each element of the array
 */

typedef struct usbUhcdPeriodicTable
    {
    UINT32 bandWidth;     /*
                                   * Bandwidth occupied by the isochronous
                                   * TDs in the frame
                                   */
    USB_UHCD_TD *td;            /* Pointer to the first isochronous TD */
    }USB_UHCD_PERIODIC_TABLE;

/*
 * The UHCD HCD maintains a tree to hold the information about the
 * interrupt transfers which need to be serviced at different polling intervals.
 * This structure holds information about each node of the tree.
 */
typedef struct usbUhcdPeriodicTree
    {
    UINT32 bandWidth; /*
                               * Bandwidth occupied by the interrupt transfers
                               * in a particular node
                               */
    USB_UHCD_QH *qh;        /* Pointer to the first interrupt TD in the node*/
    }USB_UHCD_PERIODIC_TREE;

/*
 * The HCD creates a pipe for every endpoint which needs to be
 * serviced and maintains it in a list.
 * This structure holds information about the HCD maintained pipe
 */
typedef struct usbUhcdHcdPipe
    {
    UINT8 deviceNum;                /* Device number */
    UINT8 endPointNum;              /* Endpoint number */
    UINT8 endPointDirection;        /* Direction of the endpoint */
    UINT8 endPointType;             /* Type of the endpoint */
    UINT8 uDeviceSpeed;
    UINT16 uMaximumPacketSize;
    USB_UHCD_QH *qh;              /*
                                   * Pointer to the QH
                                   * representing the endpoint
                                   */
    UINT listIndex;              /* Index of the node next in the list */
    INT32 bw;                      /*
                                   * To hold the bandwidth
                                   * occupied by the endpoint
                                   */
                                    
    UINT32  lastIndexAllocated;    /* For an Isochronous pipe this field stores the
                                   * the Index allocated to the last request for 
                                   * this endpoint(pipe)
                                   */  
    UINT32 uLastStartIndex; /* Start index of the last request */	

    struct usbUhcdHcdPipe *next; /*
                                  * Pointer to the next pipe structure in the
                                  * list of pipes maintained by the HCD
                                  */
                                  
     BOOL outTokScheduled;        /*
                                   * Out Token Scheduled for Short Packet
                                   * Transfer
                                   */

    }USB_UHCD_HCD_PIPE;

/*
 * The HCD maintains a queue of non-isochronous requests
 * This gives the format of one element of the queue
 */

typedef struct usbUhcdNonIsochronousRequestQueue
    {

    USBHST_URB *pUrb;                   /* Pointer to the URB */
    USB_UHCD_QH *qh;                    /*
                                         * Pointer to the QH to which the
                                         * request is queued
                                         */

    USB_UHCD_HCD_PIPE * pipe;          /* Pointer to pipe structure */

                                  /*thru pipe we can reach queue head , may be we remove qh*/
    USB_UHCD_TD *head;                /*
                                         * Pointer to the TD which
                                         * forms the head of this request
                                         */
    USB_UHCD_TD *tail;                /*
                                         * Pointer to the TD which forms
                                         * the tail of this request
                                         */
    struct usbUhcdNonIsochronousRequestQueue *next;/*
                                                    * Pointer to the next
                                                    * request element in
                                                    * the queue
                                                    */
    }USB_UHCD_NON_ISO_REQUEST_QUEUE;

/*
 * The HCD maintains a queue of isochronous requests
 * This gives the format of one element of the queue
 */

typedef struct usbUhcdIsochronousRequestQueue
    {
    USBHST_URB *pUrb;                   /* Pointer to the URB */

    USB_UHCD_HCD_PIPE * pipe;          /* Pointer to pipe structure */


    USB_UHCD_TD *head;        /*
                                 * Pointer to the TD which forms the head of
                                 * this request
                                 */
    USB_UHCD_TD *tail;        /*
                                 * Pointer to the TD which forms the tail of
                                 * this request
                                 */
    INT32 startIndex;            /*
                                 * Index of the frame in which the transfer
                                 * should be initiated
                                 */
    INT32 bw;                    /* Bandwidth occupied by this endpoint */

    UINT8 markedForDeletion;     /* The request is marked for deletion */

    struct usbUhcdIsochronousRequestQueue *next; /*
                                                 * Pointer to the next request
                                                 * element in the queue
                                                 */
    }USB_UHCD_ISO_REQUEST_QUEUE;

/* This data structure holds the details of the Root hub */

typedef struct usbUhcdRhData
    {
    PUCHAR  pPortStatus;
    USB_UHCD_HCD_PIPE *  pControlPipe;
    USB_UHCD_HCD_PIPE *  pInterruptPipe;
    UINT8       bInterruptEndpointHalted;
    UINT8       uDeviceAddress;  /* Address of the Root hub. */
    UINT8       uConfigValue;    /* Value of the configuration which is set. */
    }USB_UHCD_RH_DATA;

typedef struct usbUhcdRhData *PUSB_UHCD_RH_DATA;


/* This is the data structure maintained for every UHCD Host Controller. */

typedef struct usbUhcdData
{
    UINT8   busIndex;           /* Index into the array of host controllers */
    INT32       busNumber;          /*
                                 * Bus number corresponding to the bus to which the
                                 * Host Controller is connected.
                                 */
    INT32   deviceNumber;       /* Device number for the device. */
    INT32   functionNumber;     /* Function number for the device. */

    UINT8   irq;                /* IRQ number used by the UHC */

    UINT32 usbUhcdUsbBase;      /* Base address of UHCD's regx */

    USB_UHCD_FRAME_LIST * usbUhcdFrameList;   /*
                                               * To hold the UHCD's frame list.
                                               * The UHCD Host controller maintains an array of indices
                                               * which are pointers to the list of requests to be serviced.
                                               */

    USB_UHCD_FRAME_LIST * forFreeing;          /* To Hold the actual pointer to FrameList */


    USB_UHCD_PERIODIC_TABLE * usbUhcdTable;      /*
                                                 * To hold the pointer to the table containing the
                                                 * bandwidth information of the isochronous transfers
                                                 */

    USB_UHCD_PERIODIC_TREE * usbUhcdTree;    /* To hold the tip of the periodic 
                                             * tree maintained for interrupt transfers 
                                             */
 
   
    PUSB_UHCD_QH usbUhcdCQh;    /* Control Queue Head */

    PUSB_UHCD_QH usbUhcdBQh;    /* Bulk Queue Head */


    PUSB_UHCD_QH usbUhcdBRQh;    /* Bandwidth Reclamation Queue Head */

   
    USB_UHCD_HCD_PIPE * hcDefaultPipe;/* default control pipe */

    USB_UHCD_HCD_PIPE *  usbUhcdPipe; /* list of pipes belonging to host conroller */

    USB_UHCD_NON_ISO_REQUEST_QUEUE * usbUhcdNonIsoRequestQ; /* non isoch request list */

    USB_UHCD_NON_ISO_REQUEST_QUEUE * usbUhcdNonIsoRequestQTail; /* non isoch request list tail pointer */

    USB_UHCD_ISO_REQUEST_QUEUE * usbUhcdIsoRequestQ; /* isoch request list */

    USB_UHCD_ISO_REQUEST_QUEUE * usbUhcdIsoRequestQTail; /* isoch request list tail pointer */

    /*INT32 usbUhcdCBBw;*/            /* To hold the bandwidth information of the control and bulk transfers */

    USB_UHCD_RH_DATA rootHub;   /* Root Hub Details of host controller*/

    UINT8 usbUhcdResetChange[2] ;/* Flag used for monitoring Reset Change on ports */

    UCHAR usbUhcdRhConfig;/* Value indicating the configuration number which is set */

    OS_THREAD_ID usbUhcdThdTransferComp; /*Thread ID for the thread created for handling transfer completion */

    OS_THREAD_ID usbUhcdThdManagePort;/*Thread ID for the thread created for managing port */

    SEM_ID usbUhcdSemCheckPort; /* Semaphore used to indicate a change in the Root hub ports */

    SEM_ID usbUhcdSemPortInterruptTransfer;/* Semaphore used by the Root hub emulation module's interrupt task */

    SEM_ID usbUhcdSemCleanupTd; /* Semaphore used to indicate a safe cleanup of resources */

    SEM_ID usbUhcdSemCritical;  /*
                                 * Semphore for mutual exclusion. This semaphore
                                 * for handling shared global data structures
                                 */

/*    WDOG_ID wid;*/ /*
                  * Watch dog timer ID. Watch dog timer is used for scanning
                  * the UHCD's port at regular intervals
                  */
    UINT8 flagRhIntDel;          /* Flag used for checking the deletion of the interrupt
                                    task for Root Hub */
    UINT32 usbUhcdIntStatusValue;    /* place holder to store the Interrupt register value */

    UINT32 usbUhcdIntEnableValue;    /* place holder to store the Interrupt register value */


}UHCD_DATA, *PUHCD_DATA;


/* This data structure holds the details of the Endpoint of a New Interface */

typedef struct usbUhcdNewEndpointDetails
    {
    UINT8       endpointType;   /* Type of the endpoint*/
    INT32       Bw;             /* Bandwidth need by the endpoint. */
    UINT8       pollingInterval;/* Polling interval for the Interrupt pipe. */
    }USB_UHCD_NEW_ENDPOINT_DETAILS;

typedef struct usbUhcdNewEndpointDetails *PUSB_UHCD_NEW_ENDPOINT_DETAILS;




/*******************************************************************************
 * Function Name    : usbUhcdQueueRhRequest
 * Description      : This function handles the request queued to the Root hub.
 * Parameters       : pUrb IN Pointer to the URB.
 * Return Type      : BOOLEAN
 *                    Returns
 *                       TRUE  - If the request for Root hub is queued
 *                               successfully
 *                       FALSE - If the request for the Root hub is not
 *                               queued.
 ******************************************************************************/

extern BOOLEAN usbUhcdQueueRhRequest (PUHCD_DATA pHCDData, USBHST_URB *pUrb ,UINT32 pipeHandle);
extern USBHST_STATUS usbUhcdRhDeletePipe (PUHCD_DATA pHCDData, UINT32 uPipeHandle);
extern USBHST_STATUS usbUhcdRhCreatePipe (PUHCD_DATA pHCDData,
                                          UINT8    uDeviceAddress,
                                          UINT8    uDeviceSpeed,
                                          PUCHAR  pEndpointDescriptor,
                                          PUINT32 puPipeHandle);
/*******************************************************************************
 * Function Name    : usbUhcdCreateFrameList
 * Description      : The HCD maintains
 *                    - a tree of 256 elements to maintain the bandwidth usage
 *                      in every frame for the interrupt transfers.
 *                    - an array of 1024 static isochronous Transfer
 *                      Descriptor elements.
 *                    This function allocates memory for the data structures and
 *                    initialises them. Also this function allocates memory for
 *                    the frame list maintained by the UHCD Host controller and
 *                    initialises the list.
 * Parameters       : None.
 * Return Type      : BOOLEAN
 *                    Returns
 *                        TRUE - If frame list creation is success
 *                        FALSE- If frame list creation is a failure
 ******************************************************************************/
extern BOOLEAN usbUhcdCreateFrameList (PUHCD_DATA pHCDData);

/*******************************************************************************
 * Function Name    : usbUhcdDeleteFrameList
 * Description      : The HCD maintains
 *                    - a tree of 256 elements to maintain the bandwidth usage
 *                      in every frame for the interrupt transfers.
 *                    - an array of 1024 static isochronous Transfer
 *                      Descriptor elements.
 *                    This function deallocates memory for the data structures
 * Parameters       : None.
 * Return Type      : BOOLEAN
 *                    Returns
 *                        TRUE - If frame list deletion is success
 *                        FALSE- If frame list deletion is a failure
 ******************************************************************************/
extern BOOLEAN usbUhcdDeleteFrameList (PUHCD_DATA pHCDData);

/*******************************************************************************
 * Function Name    : usbUhcdCreatePipe
 * Description      : This function creates
 *                     - the HCD maintained pipe
 *                     - HC maintained QH for control, bulk and interrupt
 *                       transfers
 *                     - Updates the bandwidth utilised.
 * Parameters       : pipe IN Pointer to the Pipe data structure.
 * Return Type      : BOOLEAN
 *                    Returns
 *                        TRUE  - If pipe is created successfully
 *                        FALSE - If pipe is not created successfully.
 ******************************************************************************/
extern USBHST_STATUS usbUhcdCreatePipe (UINT8  uBusIndex,
                                        UINT8  uDeviceAddress,
                                        UINT8  uDeviceSpeed,
                                        UCHAR  *pEndpointDescriptor,
                                        UINT16  uHighSpeedHubInfo,
                                        UINT32 *puPipeHandle);
/*******************************************************************************
 * Function Name    : usbUhcdDeletePipe
 * Description      : This function is used to delete the HCD maintained pipe
 *                    and the QH created for the pipe.
 * Parameters       : pipe IN Pointer to the Pipe data structure.
 * Return Type      : BOOLEAN
 *                    Returns
 *                        TRUE  - If pipe is deleted successfully.
 *                        FALSE - If pipe is not deleted successfully.
 ******************************************************************************/
extern USBHST_STATUS usbUhcdDeletePipe (UINT8 uBusIndex,
                                        UINT32 uPipeHandle);
/*******************************************************************************
 * Function Name    : UHCD_SubmitURB
 * Description      : This function is used to submit a transfer request to the
 *                    HCD.
 * Parameters       : pUrb IN Pointer to URB.
 * Return Type      : BOOLEAN
 *                    Returns
 *                        TRUE  - If request is submitted successfully
 *                        FALSE - If request is not submitted successfully
 ******************************************************************************/
extern USBHST_STATUS usbUhcdSubmitUrb (UINT8     uBusIndex,
                                       UINT32    uPipeHandle,
                                       USBHST_URB *pUrb);
/*******************************************************************************
 * Function Name    : usbUhcdCancelUrb
 * Description      : This function is used to cancel a request queued to
 *                    already
 * Parameters       : None.
 * Return Type      : BOOLEAN
 *                    Returns
 *                        TRUE  - If the request is cancelled successfully
 *                        FALSE - If the request is not cancelled
 *                                successfully.
 ******************************************************************************/
extern USBHST_STATUS usbUhcdCancelUrb (UINT8 uBusIndex,
                                       UINT32 uPipeHandle,
                                       USBHST_URB *pUrb);


/*******************************************************************************
 * Function Name    : usbUhcdGetFrameNumber
 * Description      : This function is used to get the current frame number
 *                    of the Host Controller.
 * Parameters       : None.
 * Return Type      : UINT
 *                    Returns the least 11 bits of the current frame number.
 ******************************************************************************/
extern USBHST_STATUS usbUhcdGetFrameNumber (UINT8 uBusIndex,
                                            UINT16 *puFrameNumber);

/*******************************************************************************
 * Function Name    : usbUhcdSetBitRate
 * Description      : This function is used to set the bit rate
 *
 * Parameters       : .
 * Return Type      : USBHST_SUCCESS
 *                    Returns USBHST_SUCCESS, or USBHST_FAILURE in case of failure
 ******************************************************************************/

extern USBHST_STATUS usbUhcdSetBitRate (UINT8 uBusIndex,
                                        BOOL bIncrement,
                                        PUINT32 puCurrentFrameWidth);

/*******************************************************************************
 * Function Name    : usbUhcdIsRequestPending
 * Description      : This function is detrmine if request is pending on pipe
 *
 * Parameters       : uBusIndex Host Controller Bus Index
 *                    uPipeHandle : Handle to pipe
 * Return Type      : USBHST_SUCCESS
 *                    Returns USBHST_SUCCESS, or USBHST_FAILURE
 ******************************************************************************/


extern USBHST_STATUS usbUhcdIsRequestPending (UINT8 uBusIndex,
                                              UINT32 uPipeHandle);

/*******************************************************************************
 * Function Name    : usbUhcdModifyDefaultPipe
 * Description      : This function is modify the default pipe
 *
 * Parameters       : uBusIndex Host Controller Bus Index
 *                    uDefaultPipeHandle : Handle to  default pipe
                      uDeviceSpeed : speed of device
                      uMaxPacketSize : max packet size
 * Return Type      : USBHST_SUCCESS
 *                    Returns USBHST_SUCCESS, or USBHST_FAILURE
 ******************************************************************************/


USBHST_STATUS usbUhcdModifyDefaultPipe (UINT8 uBusIndex,
                                        UINT32 uDefaultPipeHandle,
                                        UINT8 uDeviceSpeed,
                                        UINT8 uMaxPacketSize,
                                        UINT16  uHighSpeedHubInfo);

/*******************************************************************************
 * Function Name    : usbUhcdProcessCompletedTds
 * Description      : This function is used to handle the completed TDs.
 * Parameters       : PUHCD_DATA
 * Return Type      : void
 ******************************************************************************/
extern VOID usbUhcdProcessCompletedTds(PUHCD_DATA pHCDData);


/*******************************************************************************
 * Function Name    : usbUhcdGetCurrentEndpointDetails
 * Description      : This function searches all for the pipes with a
 *                    particular device address.
 *
 * Parameters       :
 *                    uBusindex: Host Controller Bus Index.
 *                    uDeviceAddress : device address
 *                    pListOfCurrentEndpointPipes : Contains the list of the pipes
 *                                                  found
 *                    uNumberOfEndpoint         Store the number of pipes
 *
 * Return Type      : None 
 * Global Variables : g_pUHCDData
 * Calls            :
 * Called by        :
 * To Do            : None
 ******************************************************************************/

extern VOID usbUhcdGetCurrentEndpointDetails(
                            UINT8    uBusIndex,
                            UINT8    uDeviceAddress,
                            USB_UHCD_HCD_PIPE **pListOfCurrentEndpointPipes,
                            pUSBHST_ENDPOINT_DESCRIPTOR pCurrentEndpointDesc,
                            UINT8 *uNumberOfEndpoint);


/*******************************************************************************
 * Function Name    : usbUhcdGetDetailsForEndpoints
 * Description      : This function Get details for the Endpoint 
 *                    of the new interface
 *
 * Parameters       :
 *                    uDeviceSpeed :         IN       device Speed
 *                    uNumberOfEndpoints :   IN       Number of Endpoints
 *                    pEndpointDesc :        IN       all the endpoint desc for 
 *                                                    the Interface
 *                    pNewEndpointDetails    OUT      Details of the Periodic endpoint 
 *
 * Return Type      : None.
 * Global Variables : None
 * Calls            :
 * Called by        :
 * To Do            : None
 ******************************************************************************/
extern VOID usbUhcdGetDetailsForEndpoints(UINT8  uDeviceSpeed,
                                     UINT8 *uNumberOfEndpoints,
                                     pUSBHST_ENDPOINT_DESCRIPTOR *pEndpointDesc,
                                     PUSB_UHCD_NEW_ENDPOINT_DETAILS pNewEndpointDetails);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __INCusbUhcdScheduleQueueh */


