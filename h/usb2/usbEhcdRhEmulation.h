/* usbEhcdRHEmulation.h - Utility Functions for EHCI */

/* Copyright 2004 Wind River Systems, Inc. 

   This software includes software licensed to Wind River Systems, Inc.
   by Wipro, Ltd. Wind River licensees may use this software according  
   to the terms of their Wind River license agreement(s) applicable to 
   this software.
*/

/*
DESCRIPTION
This contains the data structures and prototypes of 
functions of the Root Hub Emulation module used by
the EHCI Host Controller Driver.
*/

/*
INTERNAL
 *******************************************************************************
 * Filename         : usbEhcdRHEmulation.h
 *
 * Copyright        :
 *
 * THE COPYRIGHT IN THE CONTENTS OF THIS SOFTWARE VEST WITH WIPRO
 * LIMITED A COMPANY INCORPORATED UNDER THE LAWS OF INDIA AND HAVING
 * ITS REGISTERED OFFICE AT DODDAKANNELLI SARJAPUR ROAD  BANGALORE
 * 560 035. DISTRIBUTION OR COPYING OF THIS SOFTWARE BY
 * ANY INDIVIDUAL OR ENTITY OTHER THAN THE ADDRESSEE IS STRICTLY
 * PROHIBITED AND MAY INCUR LEGAL LIABILITY. IF YOU ARE NOT THE
 * ADDRESSEE PLEASE NOTIFY US IMMEDIATELY BY PHONE OR BY RETURN EMAIL.
 * THE ADDRESSEE IS ADVISED TO MAINTAIN THE PROPRIETARY INTERESTS OF
 * THIS COPYRIGHT AS PER APPLICABLE LAWS.
 *
 *
 * Description      :  This contains the data structures and prototypes of 
 *                     functions of the Root Hub Emulation module used by 
 *                     the EHCI Host Controller Driver.
 *
 ******************************************************************************/
#ifndef __INCehcdRHEmulationh
#define __INCehcdRHEmulationh

#ifdef	__cplusplus
extern "C" {
#endif

/* defines */

#define USB_EHCD_RH_REQUEST_TYPE            0x60 /* Request type mask */
#define USB_EHCD_RH_STANDARD_REQUEST        0x0  /* Standard request identifier */
#define USB_EHCD_RH_CLASS_SPECIFIC_REQUEST  0x20 /* Class specific request */
#define USB_EHCD_RH_RECIPIENT_MASK          0x1F /* Mask value for indicating the 
                                                recipient */
#define USB_EHCD_RH_DEVICE_DESC_SIZE        0x12 /* Size of device descriptor */
#define USB_EHCD_RH_CONFIG_DESC_SIZE        0x19 /* Size of config descriptor */
#define USB_EHCD_RH_GET_STATUS_SIZE         0x02 /*Size of GetStatus request data */
#define USB_EHCD_RH_GET_CONFIG_SIZE         0x01 /* Size of GetConfiguration size */
#define USB_EHCD_RH_DESCRIPTOR_BITPOSITION  0x08 /* Descriptor type bitposition in
                                                setup packet for
                                                GetDesc request */
#define USB_EHCD_RH_CONFIG_INTERRUPT_MASK   0x17 /* Mask value to enable interrupt*/
#define USB_EHCD_RH_PORT_NUMBER_MASK        0x08 /* Port number mask in 
                                                index field */
#define USB_EHCD_RH_PORT_RESET_CHANGE       0x00100000  /* Port reset change mask */
#define USB_EHCD_RH_PORT_SUSPEND_CHANGE     0x00040000 /* Port suspend change
                                                       mask */
#define USB_EHCD_RH_PORT_SUSPEND_MASK        0x00000004 /* Port suspend mask */

/* Class specific values - Start */
#define USB_EHCD_RH_C_HUB_LOCAL_POWER		0x0
#define USB_EHCD_RH_C_HUB_OVER_CURRENT		0x01
#define USB_EHCD_RH_PORT_CONNECTION			0x0
#define USB_EHCD_RH_PORT_ENABLE			0x01
#define USB_EHCD_RH_PORT_SUSPEND			0x02
#define USB_EHCD_RH_PORT_OVER_CURRENT		0x03
#define USB_EHCD_RH_PORT_RESET			0x04
#define USB_EHCD_RH_PORT_POWER			0x08
#define USB_EHCD_RH_PORT_LOW_SPEED			0x09
#define USB_EHCD_RH_C_PORT_CONNECTION		0x10
#define USB_EHCD_RH_C_PORT_ENABLE			0x11
#define USB_EHCD_RH_C_PORT_SUSPEND			0x12
#define USB_EHCD_RH_C_PORT_OVER_CURRENT		0x13
#define USB_EHCD_RH_C_PORT_RESET			0x14
#define USB_EHCD_RH_PORT_TEST			0x15
#define USB_EHCD_RH_PORT_INDICATOR			0x16
/* Class specific values - End */


extern USBHST_STATUS usbEhcdRhCreatePipe
    (
    pUSB_EHCD_DATA pHCDData,
	UINT8	   uDeviceAddress,
    UINT8	    uDeviceSpeed,
	UCHAR *	pEndpointDescriptor,
    UINT32 *	puPipeHandle
    );


extern USBHST_STATUS usbEhcdRHDeletePipe
    (
    pUSB_EHCD_DATA pHCDData,
	UINT32	uPipeHandle
    );

extern USBHST_STATUS usbEhcdRHSubmitURB
    (
    pUSB_EHCD_DATA pHCDData,
    UINT32	uPipeHandle,
    pUSBHST_URB	pURB
    );

USBHST_STATUS usbEhcdRHCancelURB
    (
    pUSB_EHCD_DATA pHCDData,
    UINT32	uPipeHandle,
    pUSBHST_URB	pURB
    );
extern USBHST_STATUS usbEhcdRHIsRequestPending
    (
    pUSB_EHCD_DATA pHCDData,
    UINT32 uPipeHandle
    );
#endif /* End of __INCehcdRHEmulationh*/
/******************** End of file usbEhcdRHEmulation.h**********************/
