/* usbOsalDebug.h - USB OS Abstraction Layer Debug Definitions */

/* Copyright 2004 Wind River Systems, Inc. 

   This software includes software licensed to Wind River Systems, Inc.
   by Wipro, Ltd. Wind River licensees may use this software according  
   to the terms of their Wind River license agreement(s) applicable to 
   this software.
*/

/*
Modification history
--------------------
01a,17mar03,ssh  written
*/

/*
DESCRIPTION

This file contains the definitions the debugging macros for the OS Abstraction
Layer
*/

/*
INTERNAL
 *******************************************************************************
 * Filename         : OSAL_DEBUG.h
 *
 * Copyright        :
 *
 * THE COPYRIGHT IN THE CONTENTS OF THIS SOFTWARE VEST WITH WIPRO
 * LIMITED A COMPANY INCORPORATED UNDER THE LAWS OF INDIA AND HAVING
 * ITS REGISTERED OFFICE AT DODDAKANNELLI SARJAPUR ROAD  BANGALORE
 * 560 035. DISTRIBUTION OR COPYING OF THIS SOFTWARE BY
 * ANY INDIVIDUAL OR ENTITY OTHER THAN THE ADDRESSEE IS STRICTLY
 * PROHIBITED AND MAY INCUR LEGAL LIABILITY. IF YOU ARE NOT THE
 * ADDRESSEE PLEASE NOTIFY US IMMEDIATELY BY PHONE OR BY RETURN EMAIL.
 * THE ADDRESSEE IS ADVISED TO MAINTAIN THE PROPRIETARY INTERESTS OF
 * THIS COPYRIGHT AS PER APPLICABLE LAWS.
 *
 *
 * Description      : OS Abstraction layer debugging support. This file contains
 *                    the macros to enable or disable the debugging support on
 *                    a module basis.
 *
 *
 ******************************************************************************/

#ifndef __USBOSALDEBUG_H__
#define __USBOSALDEBUG_H__


/*************************** MACROS FOR MESSAGE LOGGING ***********************/

/*
 * NOTE:
 *
 * 1. The file name and the line number will be printed by default.
 * 2. Maximum 4 parameters can be passed to the log message macros. More than
 *    4 parameters will result in error.
 */

/* Macros to hold the debug log severity */
#define DEBUG_OFF         0x00   /* Debug None   */
#define DEBUG_HIGH        0x01   /* Debug High   */
#define DEBUG_MEDIUM      0x02   /* Debug Medium */
#define DEBUG_LOW         0x03   /* Debug Low    */
#define DEBUG_LVL_MASK    0xff

#define USBD              0x01   /* Debug USBD component */
#define HUB               0x02   /* Debug HUB component */
#define EHCD              0x04   /* Debug EHCD component */
#define OHCD              0x08   /* Debug OHCD component */
#define UHCD              0x10   /* Debug UHCD component */

/* Note: To enable HIGH level debugging of the USBD component, then the debug
 *       flag usbdOsalDbg should be set to value 0x00000101. If Low level 
 *       debugging of the UHCD component is enabled, then the usbdOsalDbg 
 *       flag should be set to 0x00001003
 */

/*
 * The following debug macros are enabled for debug build. For release build
 * the debug logs should be saved in to a file. This is currently not supported.
 */
#ifdef DEBUG

 
extern UINT32 usbdOsalDbg;                        /* Define the debugging flag */

/*******************************************************************************
 * MACRO NAME    : OS_LOG_FLAG_INIT
 * DESCRIPTION   : Macro to initialize the logging flag for debug messaging. This 
 *                 macro is intended to only be called once when the USBD 
 *                 interface has been enabled.
 * PARAMETERS    : INIT_VALUE  Initial value of debug log severity
 * RETURN TYPE   : None
 ******************************************************************************/
#define OS_LOG_FLAG_INIT(INIT_VALUE)                                                \
        UINT32 usbdOsalDbg = INIT_VALUE;

/*******************************************************************************
 * MACRO NAME    : OS_LOG_MESSAGE_HIGH
 * DESCRIPTION   : Macro to log the messages of high severity. This macro will
 *                 be enable only if the current debug severity is greater than
 *                 or equal to DEBUG_HIGH.
 * PARAMETERS    : None
 * RETURN TYPE   : None
 ******************************************************************************/
#define OS_LOG_MESSAGE_HIGH(MODULE_NAME, FORMAT, ARG1,ARG2,ARG3,ARG4)               \
        {                                                                           \
        if ( ((usbdOsalDbg & DEBUG_LVL_MASK) >= DEBUG_HIGH) &&                      \
             (((usbdOsalDbg >> 8) & MODULE_NAME) == MODULE_NAME) )            \
            {                                                                       \
            _OS_LOG_MESSAGE(FORMAT, ARG1,ARG2,ARG3,ARG4);                           \
            }                                                                       \
        }                                                                     

/*******************************************************************************
 * MACRO NAME    : OS_LOG_MESSAGE_MEDIUM
 * DESCRIPTION   : Macro to log the messages of medimm severity. This macro will
 *                 be enable only if the current debug severity is greater than
 *                 or equal to DEBUG_MEDIUM.
 * PARAMETERS    : None
 * RETURN TYPE   : None
 ******************************************************************************/
#define OS_LOG_MESSAGE_MEDIUM(MODULE_NAME, FORMAT, ARG1,ARG2,ARG3,ARG4)             \
        {                                                                           \
        if ( ((usbdOsalDbg & DEBUG_LVL_MASK) >= DEBUG_MEDIUM) &&                    \
             (((usbdOsalDbg >> 8) & MODULE_NAME) == MODULE_NAME) )            \
            {                                                                       \
            _OS_LOG_MESSAGE(FORMAT, ARG1,ARG2,ARG3,ARG4);                           \
            }                                                                       \
        }                                                                     

/*******************************************************************************
 * MACRO NAME    : OS_LOG_MESSAGE_LOW
 * DESCRIPTION   : Macro to log the messages of low severity. This macro will
 *                 be enable only if the current debug severity is greater than
 *                 or equal to DEBUG_LOW.
 * PARAMETERS    : None
 * RETURN TYPE   : None
 ******************************************************************************/
#define OS_LOG_MESSAGE_LOW(MODULE_NAME, FORMAT, ARG1,ARG2,ARG3,ARG4)                \
        {                                                                           \
        if ( ((usbdOsalDbg & DEBUG_LVL_MASK) >= DEBUG_LOW) &&                       \
             (((usbdOsalDbg >> 8) & MODULE_NAME) == MODULE_NAME) )            \
            {                                                                       \
            _OS_LOG_MESSAGE(FORMAT, ARG1,ARG2,ARG3,ARG4);                           \
            }                                                                       \
        }                                                                     

/*******************************************************************************
 * MACRO NAME    : OS_LOG_MESSAGE
 * DESCRIPTION   : Macro to log the messages. This macro should be used if the
 *                 raw display is required.
 *
 *                 NOTE: Avoid using this macro in the code. This macro takes
 *                       only two parameter in addition to the format string.
 *
 *                       Also note that this macro cannot be suppressed on 
 *                       module basis.
 * PARAMETERS    : None
 * RETURN TYPE   : None
 ******************************************************************************/
#define OS_LOG_MESSAGE(MODULE_NAME, FORMAT, PARAM1, PARAM2)                         \
        {                                                                           \
        if (((usbdOsalDbg >> 8) & MODULE_NAME) == MODULE_NAME)                \
            {                                                                       \
            _OS_LOG_MESSAGE_RAW(FORMAT, PARAM1, PARAM2);                            \
            }                                                                       \
        }                                                                     
 
#else /* Else of #ifdef DEBUG */

/* Message logging is not enable in release mode */
#define OS_LOG_FLAG_INIT(INIT_VALUE)                                                                 
#define OS_LOG_MESSAGE_HIGH(MODULE_NAME, FORMAT, ARG1,ARG2,ARG3,ARG4)
#define OS_LOG_MESSAGE_MEDIUM(MODULE_NAME, FORMAT, ARG1,ARG2,ARG3,ARG4)
#define OS_LOG_MESSAGE_LOW(MODULE_NAME, FORMAT, ARG1,ARG2,ARG3,ARG4)
#define OS_LOG_MESSAGE(MODULE_NAME, FORMAT, PARAM1, PARAM2)

#endif /* End of #ifdef DEBUG */

 

#endif /* End of __OSAL_DEBUG_H__ */

/*************************** End of File OSAL_Debug.h *************************/

