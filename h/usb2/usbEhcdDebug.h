/* usbEhcdDebug.h - Utility Functions for EHCI */

/* Copyright 2004 Wind River Systems, Inc. 

   This software includes software licensed to Wind River Systems, Inc.
   by Wipro, Ltd. Wind River licensees may use this software according  
   to the terms of their Wind River license agreement(s) applicable to 
   this software.
*/

/*
DESCRIPTION
This contains the debug message macros which are used by
the EHCI Host Controller Driver.
*/
/*
INTERNAL
 *******************************************************************************
 * Filename         : usbEhcdDebug.h
 *
 * Copyright        :
 *
 * THE COPYRIGHT IN THE CONTENTS OF THIS SOFTWARE VEST WITH WIPRO
 * LIMITED A COMPANY INCORPORATED UNDER THE LAWS OF INDIA AND HAVING
 * ITS REGISTERED OFFICE AT DODDAKANNELLI SARJAPUR ROAD  BANGALORE
 * 560 035. DISTRIBUTION OR COPYING OF THIS SOFTWARE BY
 * ANY INDIVIDUAL OR ENTITY OTHER THAN THE ADDRESSEE IS STRICTLY
 * PROHIBITED AND MAY INCUR LEGAL LIABILITY. IF YOU ARE NOT THE
 * ADDRESSEE PLEASE NOTIFY US IMMEDIATELY BY PHONE OR BY RETURN EMAIL.
 * THE ADDRESSEE IS ADVISED TO MAINTAIN THE PROPRIETARY INTERESTS OF
 * THIS COPYRIGHT AS PER APPLICABLE LAWS.
 *
 *
 * Description      :  This contains the debug message macros which are used by
 *                     the EHCI Host Controller Driver.
 *
 ******************************************************************************/
#ifndef __INCehcdDebugh
#define __INCehcdDebugh

#ifdef	__cplusplus
extern "C" {
#endif

#include "usbOsal.h"
/*******************Macros for debug messages - start**********************/
/* Debug levels defined */
#define USB_EHCD_DEBUG_LOW       1
#define USB_EHCD_DEBUG_MEDIUM    2
#define USB_EHCD_DEBUG_HIGH      3

#if 0
#define USB_EHCD_CURRENT_DEBUG_SEVERITY  USB_EHCD_DEBUG_HIGH   /* Current debug
                                                          severity level */
#endif

/*for compilation starts*/
/* Macro definition for logging high severity messages */
#if (USB_EHCD_CURRENT_DEBUG_SEVERITY <= USB_EHCD_DEBUG_HIGH)
#define USB_EHCD_LOG_HIGH(FORMAT, ARGS...)	\
	_OS_LOG_MESSAGE_RAW(FORMAT, 0, 0)
#else
#define USB_EHCD_LOG_HIGH(FORMAT, ARGS...)
#endif /* (CURRENT_DEBUG_SEVERITY <= DEBUG_HIGH) */

/* Macro definition for logging medium severity messages */
#if (USB_EHCD_CURRENT_DEBUG_SEVERITY <= USB_EHCD_DEBUG_MEDIUM)
#define USB_EHCD_LOG_MEDIUM(FORMAT, ARGS...)	\
	_OS_LOG_MESSAGE_RAW(FORMAT, 0, 0)
#else
#define USB_EHCD_LOG_MEDIUM(FORMAT, ARGS...)
#endif /* (CURRENT_DEBUG_SEVERITY <= DEBUG_MEDIUM) */

/* Macro definition for logging low severity messages */
#if (USB_EHCD_CURRENT_DEBUG_SEVERITY <= USB_EHCD_DEBUG_LOW)
#define USB_EHCD_LOG_LOW(FORMAT, ARGS...)\
	_OS_LOG_MESSAGE_RAW(FORMAT, 0, 0)
#else
#define USB_EHCD_LOG_LOW(FORMAT, ARGS...)
#endif

#ifdef	__cplusplus
}
#endif

#endif /* end of __INCehcdDebugh */
/* End of file */
