/* usbEhcdInitExit.h - Initialization/Uninitializations Functions for EHCI */

/* Copyright 2004 Wind River Systems, Inc. 

   This software includes software licensed to Wind River Systems, Inc.
   by Wipro, Ltd. Wind River licensees may use this software according  
   to the terms of their Wind River license agreement(s) applicable to 
   this software.
*/

/*
Modification history
--------------------
01a,27oct,04,hch  add modification history section
*/

/*
DESCRIPTION
This contains the initialization and uninitialization
function prototypes of the EHCI Host Controller Driver.
*/

/*
INTERNAL
 *******************************************************************************
 *
 * Copyright        :
 *
 * THE COPYRIGHT IN THE CONTENTS OF THIS SOFTWARE VEST WITH WIPRO
 * LIMITED A COMPANY INCORPORATED UNDER THE LAWS OF INDIA AND HAVING
 * ITS REGISTERED OFFICE AT DODDAKANNELLI SARJAPUR ROAD  BANGALORE
 * 560 035. DISTRIBUTION OR COPYING OF THIS SOFTWARE BY
 * ANY INDIVIDUAL OR ENTITY OTHER THAN THE ADDRESSEE IS STRICTLY
 * PROHIBITED AND MAY INCUR LEGAL LIABILITY. IF YOU ARE NOT THE
 * ADDRESSEE PLEASE NOTIFY US IMMEDIATELY BY PHONE OR BY RETURN EMAIL.
 * THE ADDRESSEE IS ADVISED TO MAINTAIN THE PROPRIETARY INTERESTS OF
 * THIS COPYRIGHT AS PER APPLICABLE LAWS.
 *
 */


#ifndef __INCusbEhcdInitExith
#define __INCusbEhcdInitExith

#ifdef	__cplusplus
extern "C" {
#endif

/* defines */

extern BOOLEAN usbEhcdInit(VOID);

extern BOOLEAN usbEhcdExit(VOID);

extern VOID usbEhcdDisableHC(int startType);

#ifdef	__cplusplus
}
#endif

#endif /* End of __INCusbEhcdInitExith */

/* End of file */
