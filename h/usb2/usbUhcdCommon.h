/* usbUhcdCommon.h - header file for common USB UHCD HCD registers*/

/* Copyright 2004 Wind River Systems, Inc. 

   This software includes software licensed to Wind River Systems, Inc.
   by Wipro, Ltd. Wind River licensees may use this software according  
   to the terms of their Wind River license agreement(s) applicable to 
   this software.
*/

/*
Modification history
--------------------
01b,26jun03,mat changing the code to WRS standards.
01a,25apr03,ram written.
*/

/*
DESCRIPTION

This file contains the macros which are common for all the the 
USB UHCD HCD functional modules.
*/


/*
INTERNAL
 *******************************************************************************
 * Filename         : usbUhcdCommon.h
 *
 * Copyright        :
 *
 * THE COPYRIGHT IN THE CONTENTS OF THIS SOFTWARE VEST WITH WIPRO
 * LIMITED A COMPANY INCORPORATED UNDER THE LAWS OF INDIA AND HAVING
 * ITS REGISTERED OFFICE AT DODDAKANNELLI SARJAPUR ROAD  BANGALORE
 * 560 035. DISTRIBUTION OR COPYING OF THIS SOFTWARE BY
 * ANY INDIVIDUAL OR ENTITY OTHER THAN THE ADDRESSEE IS STRICTLY
 * PROHIBITED AND MAY INCUR LEGAL LIABILITY. IF YOU ARE NOT THE
 * ADDRESSEE PLEASE NOTIFY US IMMEDIATELY BY PHONE OR BY RETURN EMAIL.
 * THE ADDRESSEE IS ADVISED TO MAINTAIN THE PROPRIETARY INTERESTS OF
 * THIS COPYRIGHT AS PER APPLICABLE LAWS.
 *
 *
 * Description      :  This file contains the macros which are common all the
 *                     the UHCD HCD functional modules.
 *
 *
 */

#ifndef __INCusbUhcdCommonh
#define __INCusbUhcdCommonh

#ifdef __cplusplus
extern "C" {
#endif

/* includes */

#include "usbOsal.h"

/* defines */

#define USB_UHCD_USBCMD      0x00  /* Offset of the USBCMD register */
#define USB_UHCD_USBSTS      0x02  /* Offset of the USBSTS register */
#define USB_UHCD_USBINTR     0x04  /* Offset of the USBINTR register */
#define USB_UHCD_FRNUM       0x06  /* Offset of the FRNUM register */
#define USB_UHCD_FLBASEADD   0x08  /* Offset of the FLBASEADD register */
#define USB_UHCD_SOF_MODIFY  0x0C  /* Offset of the SOF_MODIFY register*/
#define USB_UHCD_PORT1       0x10  /* Offset of the PORT1 register */
#define USB_UHCD_PORT2       0x12  /* Offset of the PORT2 register */

/* To hold the loop iteration number used for settling time of the HC reset */

#define USB_UHCD_NUM_OF_RETRIES      1000000

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __INCusbUhcdCommonh */


