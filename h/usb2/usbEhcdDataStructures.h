/* EHCD_DataStructures.h - Data Structures for EHCI */

/* Copyright 2004 - 2005 Wind River Systems, Inc. 

   This software includes software licensed to Wind River Systems, Inc.
   by Wipro, Ltd. Wind River licensees may use this software according  
   to the terms of their Wind River license agreement(s) applicable to 
   this software.
*/

/*
modification history
--------------------
01l,22apr05,pdg  Fix for 64 bit split isochronous transfer
01k,28mar05,pdg  non-PCI changes
01k,22mar05,mta  SPR 104950
01j,15feb05,pdg  Added comments for 64 bit support
01i,04feb05,pdg  Fix for SPR #104950(USB STACK 2.1 FROM PID 2.1 ISN'T
                 CORRECTLY SUPPORTING INTEL 82801DB/DBM EHCI HOST CONTROLLER)
01h,03dec04,ami  Merged IP Changes
01g,05oct04,mta  SPR100704- Removal of floating point math
01f,16aug04,pdg  Fix for print-stop-reprint
01e,28Jul03,god Incorporated changes identified during integration testing.
01d,23Jul03,gpd Incorporated the changes after testing on MIPS.
01c,03Jul03,gpd Added separate reclamation lists of asynch and periodic lists.
                Added new field for identifying the next element of isochronous
                TD.
01b,26jun03,gpd changing the code to WRS standards.
01a,25apr02,ram written.
*/

/*
DESCRIPTION
This contains the data structures used by the EHCI Host Controller Driver.
*/

/*
INTERNAL
 *******************************************************************************
 * Filename         : EHCD_DataStructures.h
 *
 * Copyright        :
 *
 * THE COPYRIGHT IN THE CONTENTS OF THIS SOFTWARE VEST WITH WIPRO
 * LIMITED A COMPANY INCORPORATED UNDER THE LAWS OF INDIA AND HAVING
 * ITS REGISTERED OFFICE AT DODDAKANNELLI SARJAPUR ROAD  BANGALORE
 * 560 035. DISTRIBUTION OR COPYING OF THIS SOFTWARE BY
 * ANY INDIVIDUAL OR ENTITY OTHER THAN THE ADDRESSEE IS STRICTLY
 * PROHIBITED AND MAY INCUR LEGAL LIABILITY. IF YOU ARE NOT THE
 * ADDRESSEE PLEASE NOTIFY US IMMEDIATELY BY PHONE OR BY RETURN EMAIL.
 * THE ADDRESSEE IS ADVISED TO MAINTAIN THE PROPRIETARY INTERESTS OF
 * THIS COPYRIGHT AS PER APPLICABLE LAWS.
 *
 *
 * Description      :  This contains the data structures used by the EHCI Host
 *                     Controller Driver.
 *
 ******************************************************************************/
#ifndef __EHCD_DATASTRUCTURES_H__
#define __EHCD_DATASTRUCTURES_H__

#ifdef	__cplusplus
extern "C" {
#endif

#include "usb/pciConstants.h"

#define USB_EHCD_MAX_FRAMELIST_SIZE	        1024      /* Maximum size of the
                                                     frame list */
#define USB_EHCD_FRAME_OFFSET               8       /* Offset for frame Index */
#define USB_EHCD_MAX_MICROFRAME_NUMBER      8         /* Maximum microframe number*/
#define USB_EHCD_MAX_FULL_SPEED_UFRAME      4         /* Maximum no. of microframe used
                                                         for split transaction. Means sum
                                                         of start and complete split */
#define USB_EHCD_MAX_TREE_NODES		63        /* Maximum number of nodes
                                                  * in the interrupt tree
                                                  */
#define USB_EHCD_MAX_LEAF_NODES             32        /* Maximum number of leaves
                                                   * available in the interrupt
                                                   * tree
                                                   */
#define USB_EHCD_FIRST_LEAF_NODE_INDEX      31        /* Index of the first
                                                    * leaf node 
                                                    */
#define USB_EHCD_MAX_DELAY_INTERVAL         1000      /* Maximum interval for the
                                                    * Host Controller bit to be
                                                    * set or reset
                                                    */
#define USB_EHCD_PAGE_SIZE                  4096      /* Page size value */
#define USB_EHCD_HC_DATA_STRUCTURE_ALIGNMENT   32     /* Alignment size of the data
                                                   * structures of the Host
                                                   * Controller
                                                   */
#define USB_EHCD_64BIT_HC_DATA_STRUCTURE_ALIGNMENT	64
                                                      /* Alignment size of */
                                                      /* 64 bit SITD data */ 
                                                      /* structure */
        
      
#define USB_EHCD_RH_PORT_STATUS_SIZE           4      /* Size of the port status */
#define USB_EHCD_PARENT_HUB_ADDRESS_MASK       0xFF00 /* To hold the parent 
                                                       * hub address mask
                                                       */
#define USB_EHCD_HUB_PORT_NUMBER_MASK          0x00FF /* Holds the hub port
                                                       * number mask
                                                       */
#define USB_EHCD_INT_THREAD_PRIORITY           100     /* Priority of the interrupt
                                                     * handler thread
                                                     */
#define USB_EHCD_NO_LIST                       -1     /* Value indicating the 
                                                     * empty list
                                                     */
#define USB_EHCD_RH_PORT_CONNECT_CHANGE       0x00010000  /* Port connect
                                                           * change mask
                                                           */
/* Status bit fields of QTD - start */

#define USB_EHCD_QTD_STATUS_ACTIVE              0x80   /* Status indicating that
                                                        * the TD is active
                                                        */
#define USB_EHCD_QTD_STATUS_HALTED              0x40   /* Halted bit mask in
                                                        * the status field
                                                        */
#define USB_EHCD_QTD_STATUS_DATA_BUFFER_ERROR   0x20   /* Data buffer error bit
                                                      * mask in the status field
                                                      */
#define USB_EHCD_QTD_STATUS_BABBLE_DETECTED     0x10   /* Babble detected bit mask
                                                      * in the status field
                                                      */
#define USB_EHCD_QTD_STATUS_XACTERR             0x08   /* Transaction Error bit
                                                      * mask in the status field
                                                      */
#define USB_EHCD_QTD_STATUS_MISSED_UFRAME       0x04 /* Missed microframe bit
                                                      * mask in the status
                                                      * field
                                                      */
#define USB_EHCD_QTD_STATUS_SPLITXSTATE         0x02   /* SplitXState bit mask
                                                        * in the status field
                                                        */
#define USB_EHCD_QTD_STATUS_PING_STATE          0x01   /* Ping State bit mask in
                                                       * the status field
                                                       */
/* Status bit fields of QTD - End */

/* Status bit fields of the ITD - Start */

#define USB_EHCD_ITD_STATUS_ACTIVE                    0x08/* ITD is active */

#define USB_EHCD_ITD_STATUS_BUFFER_ERROR              0x04/* Data buffer error */

#define USB_EHCD_ITD_STATUS_BABBLE_DETECTED           0x02/* Babble detected */

#define USB_EHCD_ITD_STATUS_XACTERR                   0x01/* Transaction error */

/* Status bit fields of the ITD - End */


/* Status bit fields of the SITD - Start */

#define USB_EHCD_SITD_STATUS_ACTIVE                    0x80/* SITD is active */

#define USB_EHCD_SITD_STATUS_ERROR                     0x40/* Response error */

#define USB_EHCD_SITD_STATUS_BUFFER_ERROR              0x20/* Data buffer error */

#define USB_EHCD_SITD_STATUS_BABBLE_DETECTED           0x10/* Babble detected */

#define USB_EHCD_SITD_STATUS_XACTERR                   0x08/* Transaction error */

#define USB_EHCD_SITD_STATUS_MISSED_FRAME              0x04/* Frame missing */

/* Status bit fields of the SITD - End */

/* Type fields of the link pointer - Start */
#define USB_EHCD_TYPE_ITD       0  /* Isochronous TD type */
#define USB_EHCD_TYPE_QH        1  /* Queue Head Type */
#define USB_EHCD_TYPE_SITD      2  /* Split isochronous TD type */
#define USB_EHCD_TYPE_FSTN      3  /* FSTN(Frame Span Traversal Node) type */
/* Type fields of the link pointer - End */

/* Transaction pointer value for SITD - Start */
#define USB_EHCD_SITD_TP_ALL    0  /* Complete payload in one SITD */
#define USB_EHCD_SITD_TP_BEGIN  1  /* Complete payload is more than 188 byte */
#define USB_EHCD_SITD_TP_MID    2  /* Middle of Split isochronous TD */
#define USB_EHCD_SITD_TP_END    3  /* End of Split isochronous TD */
/* Transaction pointer value for SITD - End */

/* Terminate bit field values - Start */
#define USB_EHCD_VALID_LINK          0 /* Valid link */
#define USB_EHCD_INVALID_LINK        1 /* Invalid link */
/* Terminate bit field values - End */

#define USB_EHCD_MAX_ERRORS          3 /* Maximum errors acceptable */

/* Values for the device speeds - End */

/* Data toggle carry values - Start */
#define USB_EHCD_DTC_PRESERVE_IN_QH         0 /* The data toggle bit is 
                                               * preserved in the Queue Head
                                               */
#define USB_EHCD_DTC_RETRIEVE_FROM_QTD      1 /* The data toggle bit is
                                               * retrieved from the QTD
                                               */
/* Data toggle carry values - End */

#define USB_EHCD_DEFAULT_MAX_PACKET_SIZE    64   /* Default maximum packet
                                                  * size value
                                                  */
#define USB_EHCD_MAX_NAK_RATE               255  /* Maximum NAK rate for high
                                                  * speed control/bulk endpoints
                                                  */
#define USB_EHCD_HOST_DELAY                 5  /* Maximum host delay in
                                                  * nanoseconds
                                                  */
#define USB_EHCD_HUB_LS_SETUP               333 /* Hub Low Speed setup time in 
                                               * nanoseconds- this value has
                                                  * to be checked
                                                  */
#define USB_EHCD_DIR_IN                     0x80 /* Value indicating IN
                                                  * direction
                                                  */
#define USB_EHCD_DIR_BIT_POSITION           7 

#define USB_EHCD_ENDPOINT_NUMBER_MASK       0x0F  /* Mask value for the
                                                  * endpoint number
                                                  */
#define USB_EHCD_BIT_STUFF_TIME(uByteCount)  (7 * 8 * uByteCount/6) /* Bit
                                                                  stuff time */
#define USB_EHCD_ENDPOINT_TYPE_MASK         0x03 /* Mask value for the endpoint
                                                  * type
                                                  */
#define USB_EHCD_ENDPOINT_MAX_PACKET_SIZE_MASK   0x7FF /* Mask value for the 
                                                        * maximum packet size
                                                        */
#define USB_EHCD_ENDPOINT_NUMBER_OF_TRANSACTIONS_MASK   0x1800 /* Mask value to 
                                                                * retrieve the 
                                                                * number of
                                                                * transactions
                                                                * in a 
                                                                * microframe
                                                                */
/* Size of the Root hub status information */
#define USB_EHCD_HUBSTATUS_SIZE	    4
#define USB_EHCD_MAX_BW                 125000   /* To hold the maximum
                                                  * bandwidth which can be
                                                  * accomodated
                                                  * 125 microsec = 125 * 1000
                                                  * nanosec 
                                                  */
#define USB_EHCD_EIGHTY_PERCENT_BW      100000  /* To hold the 80 % bandwidth
                                                 * value in a microframe
                                                 */
#define USB_EHCD_MAX_USB20_POLLING_INTERVAL 16  /* To hold the maximum polling
                                                 * interval supported
                                                 */
/* Macros for split masks - start */
#define USB_EHCD_SPLIT_MASK_1           0x1D   /*   Value of mask is 00011101
                                                * Start split in microframe 1
                                                * Complete splits in microframes
                                                * 3,4, 5
                                                */
#define USB_EHCD_SPLIT_MASK_2           0x3A   /*   Value of mask is 00111010
                                                * Start split in microframe 2
                                                * Complete splits in microframes
                                                * 4,5, 6 
                                                */
#define USB_EHCD_SPLIT_MASK_3           0x74   /*   Value of mask is 01110100
                                                * Start split in microframe 3
                                                * Complete splits in microframes
                                                * 5, 6, 7 
                                                */
#define USB_EHCD_SPLIT_MASK_4           0xE8   /*   Value of mask is 11101000
                                                * Start split in microframe 4
                                                * Complete splits in microframes
                                                * 6, 7, 8 
                                                */
/* Macros for split masks - End */


#define USB_EHCD_MAX_USB11_INTERRUPT_POLL_INTERVAL      32   /*Maximum polling 
                                                            * interval which can
                                                            * be supported for 
                                                            * USB 1.1
                                                            * interrupt 
                                                            * transfers
                                                            */
#define USB_EHCD_MAX_INT_SPLIT_MASKS                    4   /* Maximum number of 
                                                             * masks defined for
                                                             * split interrupt
                                                             */
#define USB_EHCD_MAX_DEVICE_ADDRESS                     127 /* Maximum device 
                                                             * address
                                                             */

/* PID values - Start */
#define USB_EHCD_OUT_PID                        0  /* OUT PID */
#define USB_EHCD_IN_PID                         1  /* IN PID */
#define USB_EHCD_SETUP_PID                      2  /* SETUP PID */
/* PID values - End */

#define USB_EHCD_QTD_MAX_TRANSFER_SIZE          20480 /* Maximum size of data
                                                       * which can be 
                                                       * transferred using
                                                       * a QTD
                                                       */
#define	USB_EHCD_SITD_MAX_SPLIT_SIZE		188  /* Maximum size for one split
							                        Transaction */
#define USB_EHCD_SITD_MAX_TRANSFER_SIZE         0x3FF /* Maximum size of data which
                                                     can be transferred using
                                                     a SITD is 1023 byte only*/

#define USB_EHCD_RH_MASK_VALUE                   0x02 /* Mask value used for
                                                       * populating the
                                                       * interrupt status
                                                       * data
                                                       */
#define USB_EHCD_INTERRUPT_ENABLE_MASK           0x17 /* Interrupt enable
                                                       * mask
                                                       */

#define USB_EHCD_QH_64BIT_ADDITIONAL_SIZE		20	/* 
                                                                 * 64 bit QH 
                                                                 * additional
                                                                 * size
                                                                 */
#define USB_EHCD_QTD_64BIT_ADDITIONAL_SIZE		20	/* 
                                                                 * 64 bit QTD 
                                                                 * additional
                                                                 * size
                                                                 */
#define USB_EHCD_ITD_64BIT_ADDITIONAL_SIZE		28	/* 
                                                                 * 64 bit ITD 
                                                                 * additional
                                                                 * size
                                                                 */
#define USB_EHCD_SITD_64BIT_ADDITIONAL_SIZE		8	/* 
                                                                 * 64 bit SITD 
                                                                 * additional
                                                                 * size
                                                                 */
#define USB_EHCD_QH_HEADER_SIZE				20	/* 
								 * QH header
								 * size
								 */
#define USB_EHCD_QTD_HEADER_SIZE			14	/*
								 * QTD header
								 * size
								 */
#define USB_EHCD_ITD_HEADER_SIZE			24	/*
								 * ITD header
								 * size
								 */	
#define USB_EHCD_SITD_HEADER_SIZE			26	/*
								 * SITD header
								 * size
								 */			


/* Masks for data structure member starts   */

/* Masks for frame list  pointer */

#define USB_EHCD_FRAME_LIST_POINTER_VALID_ENTRY_MASK 0x00000001
#define USB_EHCD_FRAME_LIST_POINTER_TYPE_MASK        0x00000006
#define USB_EHCD_FRAME_LIST_POINTER_RESERVED_MASK    0x00000018
#define USB_EHCD_FRAME_LIST_POINTER_MASK             0xFFFFFFE0

/* mask bits for Queue Head */

#define USB_EHCD_QH_HORIZONTAL_LINK_POINTER_T_MASK       0x00000001
#define USB_EHCD_QH_HORIZONTAL_LINK_POINTER_TYPE_MASK               0x00000006
#define USB_EHCD_QH_HORIZONTAL_LINK_POINTER_MASK                    0xFFFFFFE0
#define USB_EHCD_QH_ENDPOINT_CHARACTERISTICS_DEVICE_ADDRESS_MASK    0x0000007F
#define USB_EHCD_QH_ENDPOINT_CHARACTERISTICS_INACTIVATE_MASK        0x00000080
#define USB_EHCD_QH_ENDPOINT_CHARACTERISTICS_ENDPT_NUMBER_MASK      0x00000F00
#define USB_EHCD_QH_ENDPOINT_CHARACTERISTICS_ENDPT_SPEED_MASK       0x00003000
#define USB_EHCD_QH_ENDPOINT_CHARACTERISTICS_DTC_MASK               0x00004000
#define USB_EHCD_QH_ENDPOINT_CHARACTERISTICS_HEAD_RECLAMATION_LIST_MASK \
                                                                    0x00008000
#define USB_EHCD_QH_ENDPOINT_CHARACTERISTICS_MAXIMUM_PACKET_LENGTH_MASK  \
                                                                    0x07FF0000
#define USB_EHCD_QH_ENDPOINT_CHARACTERISTICS_CONTROL_ENDPOINT_FLAG_MASK  \
                                                                    0x08000000
#define USB_EHCD_QH_ENDPOINT_CHARACTERISTICS_RL_MASK                0xF0000000
#define USB_EHCD_QH_ENDPOINT_CAPABILITIES_UFRAME_S_MASK             0x000000FF
#define USB_EHCD_QH_ENDPOINT_CAPABILITIES_UFRAME_C_MASK             0x0000FF00
#define USB_EHCD_QH_ENDPOINT_CAPABILITIES_HUB_ADDR_MASK             0x007F0000
#define USB_EHCD_QH_ENDPOINT_CAPABILITIES_PORT_NUMBER_MASK          0x3F800000
#define USB_EHCD_QH_ENDPOINT_CAPABILITIES_MULT_MASK                 0xC0000000
#define USB_EHCD_QH_CURRENTQTD_POINTER_MASK                         0xFFFFFFE0
#define USB_EHCD_QH_NEXTQTD_POINTER_T_MASK                          0x00000001
#define USB_EHCD_QH_NEXTQTD_POINTER_MASK                            0xFFFFFFE0
#define USB_EHCD_QH_ALTERNATE_NEXTQTDPOINTER_T_MASK                 0x00000001
#define USB_EHCD_QH_ALTERNATE_NEXTQTDPOINTER_NAKCNT_MASK            0x0000001E
#define USB_EHCD_QH_ALTERNATE_NEXTQTDPOINTER_MASK                   0xFFFFFFE0
#define USB_EHCD_QH_TRANSFERINFO_STATUS_MASK                        0x000000FF
#define USB_EHCD_QH_TRANSFERINFO_PID_CODE_MASK                      0x00000300
#define USB_EHCD_QH_TRANSFERINFO_CERR_MASK                          0x00000C00
#define USB_EHCD_QH_TRANSFERINFO_C_PAGE_MASK                        0x00007000
#define USB_EHCD_QH_TRANSFERINFO_IOC_MASK                           0x00008000
#define USB_EHCD_QH_TRANSFERINFO_TOTAL_BYTES_TO_TRASFER_MASK        0x7FFF0000
#define USB_EHCD_QH_TRANSFERINFO_DT_MASK                            0x80000000
#define USB_EHCD_QH_BUFFERPOINTER_CURRENT_OFFSET_MASK               0x00000FFF
#define USB_EHCD_QH_BUFFER_POINTER_MASK                             0xFFFFF000
#define USB_EHCD_QH_BUFFERPOINTER_C_PROG_MASK                       0x000000FF
#define USB_EHCD_QH_BUFFERPOINTER_FRAME_TAG_MASK                    0x0000001F
#define USB_EHCD_BUFFERPOINTER_S_BYTES_MASK                         0x00000FE0

/* Mask bits for siTD */

#define USB_EHCD_SITD_NEXT_LINK_POINTER_T_MASK                      0x00000001
#define USB_EHCD_SITD_NEXT_LINK_POINTER_TYPE_MASK                   0x00000006
#define USB_EHCD_SITD_NEXT_LINK_POINTER_MASK                        0xFFFFFFE0
#define USB_EHCD_SITD_ENDPOINT_CHARACTERISTICS_DEVICE_ADDRESS_MASK  0x0000007F
#define USB_EHCD_SITD_ENDPOINT_CHARACTERISTICS_ENDPT_NUMBER_MASK    0x00000F00
#define USB_EHCD_SITD_ENDPOINT_CHARACTERISTICS_HUB_ADDR_MASK        0x007F0000
#define USB_EHCD_SITD_ENDPOINT_CHARACTERISTICS_PORT_NUMBER_MASK     0x7F000000
#define USB_EHCD_SITD_ENDPOINT_CHARACTERISTICS_DIRECTION_MASK       0x80000000
#define USB_EHCD_SITD_SCHEDULE_CONTROL_UFRAME_S_MASK                0x000000FF
#define USB_EHCD_SITD_SCHEDULE_CONTROL_UFRAME_C_MASK                0x0000FF00
#define USB_EHCD_SITD_TRANSFER_STATE_STATUS_MASK                    0x000000FF
#define USB_EHCD_SITD_TRANSFER_STATE_UFRAME_PROG_C_MASK             0x0000FF00
#define USB_EHCD_SITD_TRANSFER_STATE_TOTAL_BYTES_TO_TRANSFER_MASK   0x03FF0000
#define USB_EHCD_SITD_TRANSFER_STATE_PAGE_SELECT_MASK               0x40000000
#define USB_EHCD_SITD_TRANSFER_STATE_IOC_MASK                       0x80000000
#define USB_EHCD_SITD_BUFFERPOINTER_CURRENT_OFFSET_MASK             0x00000FFF
#define USB_EHCD_SITD_BUFFERPOINTER_BACKPTR_T_MASK                  0X00000001
#define USB_EHCD_SITD_BUFFERPOINTER_MASK                            0xFFFFF000
#define USB_EHCD_SITD_BUFFERPOINTER_TP_MASK                         0x00000018
#define USB_EHCD_SITD_BUFFERPOINTER_TCOUNT_MASK                     0x00000007

/* Mask bits for iTD */

#define USB_EHCD_ITD_NEXT_LINK_POINTER_T_MASK                       0x00000001
#define USB_EHCD_ITD_NEXT_LINK_POINTER_TYPE_MASK                    0x00000006
#define USB_EHCD_ITD_NEXT_LINK_POINTER_MASK                         0xFFFFFFE0
#define USB_EHCD_ITD_TRANSACTION_OFFSET_MASK                        0x00000FFF
#define USB_EHCD_ITD_TRANSACTION_PAGE_SELECT_MASK                   0x00007000
#define USB_EHCD_ITD_TRANSACTION_IOC_MASK                           0x00008000
#define USB_EHCD_ITD_TRANSACTION_LENGTH_MASK                        0x0FFF0000
#define USB_EHCD_ITD_TRANSACTION_STATUS_MASK                        0xF0000000
#define USB_EHCD_ITD_BUFFER_POINTER_DEVICE_ADDRESS_MASK             0x0000007F
#define USB_EHCD_ITD_BUFFER_POINTER_END_POINT_NUMBER_MASK           0x00000F00
#define USB_EHCD_ITD_BUFFER_POINTER_MAX_PACKET_SIZE_MASK            0x000007FF
#define USB_EHCD_ITD_BUFFER_POINTER_DIRECTION_MASK                  0x00000800
#define USB_EHCD_ITD_BUFFER_POINTER_MULT_MASK                       0x00000003
#define USB_EHCD_ITD_BUFFER_POINTER_MASK                            0xFFFFF000

/* Masks for data structure member Ends   */
/* Bit positions of fields in  structure elements start */

/*Frame List Pointer */

#define USB_EHCD_FRAME_LIST_POINTER_VALID_ENTRY_E                       0
#define USB_EHCD_FRAME_LIST_POINTER_TYPE_E                              1
#define USB_EHCD_FRAME_LIST_POINTER_E                                   5

/* Queue Head */

#define USB_EHCD_QH_HORIZONTAL_LINK_POINTER_T_E                         0
#define USB_EHCD_QH_HORIZONTAL_LINK_POINTER_TYPE_E                      1
#define USB_EHCD_QH_HORIZONTAL_LINK_POINTER_E                           5
#define USB_EHCD_QH_ENDPOINT_CHARACTERISTICS_DEVICE_ADDRESS_E           0
#define USB_EHCD_QH_ENDPOINT_CHARACTERISTICS_INACTIVATE_E               7
#define USB_EHCD_QH_ENDPOINT_CHARACTERISTICS_ENDPT_NUMBER_E             8
#define USB_EHCD_QH_ENDPOINT_CHARACTERISTICS_ENDPT_SPEED_E              12
#define USB_EHCD_QH_ENDPOINT_CHARACTERISTICS_DTC_E                      14
#define USB_EHCD_QH_ENDPOINT_CHARACTERISTICS_HEAD_RECLAMATION_LIST_E    15
#define USB_EHCD_QH_ENDPOINT_CHARACTERISTICS_MAXIMUM_PACKET_LENGTH_E    16
#define USB_EHCD_QH_ENDPOINT_CHARACTERISTICS_CONTROL_ENDPOINT_FLAG_E    27
#define USB_EHCD_QH_ENDPOINT_CHARACTERISTICS_RL_E                       28
#define USB_EHCD_QH_ENDPOINT_CAPABILITIES_UFRAME_S_E                    0
#define USB_EHCD_QH_ENDPOINT_CAPABILITIES_UFRAME_C_E                    8
#define USB_EHCD_QH_ENDPOINT_CAPABILITIES_HUB_ADDR_E                    16
#define USB_EHCD_QH_ENDPOINT_CAPABILITIES_PORT_NUMBER_E                 23
#define USB_EHCD_QH_ENDPOINT_CAPABILITIES_MULT_E                        30
#define USB_EHCD_QH_CURRENTQTD_POINTER_E                                5
#define USB_EHCD_QH_NEXTQTD_POINTER_T_E                                 0
#define USB_EHCD_QH_NEXTQTD_POINTER_E                                   5
#define USB_EHCD_QH_ALTERNATE_NEXTQTDPOINTER_T_E                        0
#define USB_EHCD_QH_ALTERNATE_NEXTQTDPOINTER_NAKCNT_E                   1
#define USB_EHCD_QH_ALTERNATE_NEXTQTDPOINTER_E                          5
#define USB_EHCD_QH_TRANSFERINFO_STATUS_E                               0
#define USB_EHCD_QH_TRANSFERINFO_PID_CODE_E                             8
#define USB_EHCD_QH_TRANSFERINFO_CERR_E                                 10
#define USB_EHCD_QH_TRANSFERINFO_C_PAGE_E                               12
#define USB_EHCD_QH_TRANSFERINFO_IOC_E                                  15
#define USB_EHCD_QH_TRANSFERINFO_TOTAL_BYTES_TO_TRANSFER_E              16
#define USB_EHCD_QH_TRANSFERINFO_DT_E                                   31
#define USB_EHCD_QH_BUFFERPOINTER_CURRENT_OFFSET_E                      0
#define USB_EHCD_QH_BUFFER_POINTER_E                                    12
#define USB_EHCD_QH_BUFFERPOINTER_C_PROG_E                              0
#define USB_EHCD_QH_BUFFERPOINTER_FRAME_TAG_E                           0
#define USB_EHCD_QH_BUFFERPOINTER_S_BYTES_E                             5

/* QTD */
#define USB_EHCD_QTD_NEXTQTD_POINTER_T_E                                0
#define USB_EHCD_QTD_NEXTQTD_POINTER_E                                  5
#define USB_EHCD_QTD_ALTERNATE_NEXTQTDPOINTER_T_E                       0
#define USB_EHCD_QTD_ALTERNATE_NEXTQTDPOINTER_E                         5
#define USB_EHCD_QTD_TOKEN_STATUS_E                                     0
#define USB_EHCD_QTD_TOKEN_PID_CODE_E                                   8
#define USB_EHCD_QTD_TOKEN_CERR_E                                       10
#define USB_EHCD_QTD_TOKEN_C_PAGE_E                                     12
#define USB_EHCD_QTD_TOKEN_IOC_E                                        15
#define USB_EHCD_QTD_TOKEN_TOTAL_BYTES_TO_TRANSFER_E                    16
#define USB_EHCD_QTD_TOKEN_DT_E                                         31
#define USB_EHCD_QTD_BUFFERPOINTER_CURRENT_OFFSET_E                     0
#define USB_EHCD_QTD_BUFFER_POINTER_E                                   12

/* siTD */

#define USB_EHCD_SITD_NEXT_LINK_POINTER_T_E                             0
#define USB_EHCD_SITD_NEXT_LINK_POINTER_TYPE_E                          1
#define USB_EHCD_SITD_NEXT_LINK_POINTER_E                               5
#define USB_EHCD_SITD_ENDPOINT_CHARACTERISTICS_DEVICE_ADDRESS_E         0
#define USB_EHCD_SITD_ENDPOINT_CHARACTERISTICS_ENDPT_NUMBER_E           8
#define USB_EHCD_SITD_ENDPOINT_CHARACTERISTICS_HUB_ADDR_E               16
#define USB_EHCD_SITD_ENDPOINT_CHARACTERISTICS_PORT_NUMBER_E            24
#define USB_EHCD_SITD_ENDPOINT_CHARACTERISTICS_DIRECTION_E              31
#define USB_EHCD_SITD_SCHEDULE_CONTROL_UFRAME_S_E                       0
#define USB_EHCD_SITD_SCHEDULE_CONTROL_UFRAME_C_E                       8
#define USB_EHCD_SITD_TRANSFER_STATE_STATUS_E                           0
#define USB_EHCD_SITD_TRANSFER_STATE_UFRAME_PROG_C_E                    8
#define USB_EHCD_SITD_TRANSFER_STATE_TOTAL_BYTES_TO_TRANSFER_E          16
#define USB_EHCD_SITD_TRANSFER_STATE_PAGE_SELECT_E                      30
#define USB_EHCD_SITD_TRANSFER_STATE_IOC_E                              31
#define USB_EHCD_SITD_BUFFERPOINTER_CURRENT_OFFSET_E                    0
#define USB_EHCD_SITD_BUFFERPOINTER_E                                   12
#define USB_EHCD_SITD_BUFFERPOINTER_TP_E                                3
#define USB_EHCD_SITD_BUFFERPOINTER_TCOUNT_E                            0
#define USB_EHCD_SITD_BUFFERPOINTER_BACKPTR_T_E                         0
#define USB_EHCD_SITD_BUFFERPOINTER_BACKPTR_E                           5

/* iTD */

#define USB_EHCD_ITD_NEXT_LINK_POINTER_T_E                              0
#define USB_EHCD_ITD_NEXT_LINK_POINTER_TYPE_E                           1
#define USB_EHCD_ITD_NEXT_LINK_POINTER_E                                5
#define USB_EHCD_ITD_TRANSACTION_OFFSET_E                               0
#define USB_EHCD_ITD_TRANSACTION_PAGE_SELECT_E                          12
#define USB_EHCD_ITD_TRANSACTION_IOC_E                                  15
#define USB_EHCD_ITD_TRANSACTION_LENGTH_E                               16
#define USB_EHCD_ITD_TRANSACTION_STATUS_E                               28
#define USB_EHCD_ITD_BUFFER_POINTER_DEVICE_ADDRESS_E                    0
#define USB_EHCD_ITD_BUFFER_POINTER_END_POINT_NUMBER_E                  8
#define USB_EHCD_ITD_BUFFER_POINTER_MAX_PACKET_SIZE_E                   0
#define USB_EHCD_ITD_BUFFER_POINTER_DIRECTION_E                         11
#define USB_EHCD_ITD_BUFFER_POINTER_MULT_E                              0
#define USB_EHCD_ITD_BUFFER_POINTER_E                                   12

/* Mask bits for QTD */

#define USB_EHCD_QTD_NEXTQTD_POINTER_T_MASK                         0x00000001
#define USB_EHCD_QTD_NEXTQTD_POINTER_MASK                           0xFFFFFFE0
#define USB_EHCD_QTD_ALTERNATE_NEXTQTDPOINTER_T_MASK                0x00000001
#define USB_EHCD_QTD_ALTERNATE_NEXTQTDPOINTER_MASK                  0xFFFFFFE0
#define USB_EHCD_QTD_TOKEN_STATUS_MASK                              0x000000FF
#define USB_EHCD_QTD_TOKEN_PID_CODE_MASK                            0x00000300
#define USB_EHCD_QTD_TOKEN_CERR_MASK                                0x00000C00
#define USB_EHCD_QTD_TOKEN_C_PAGE_MASK                              0x00007000
#define USB_EHCD_QTD_TOKEN_IOC_MASK                                 0x00008000
#define USB_EHCD_QTD_TOKEN_TOTAL_BYTES_TO_TRANSFER_MASK             0x7FFF0000
#define USB_EHCD_QTD_TOKEN_DT_MASK                                  0x80000000
#define USB_EHCD_QTD_BUFFERPOINTER_CURRENT_OFFSET_MASK              0x00000FFF
#define USB_EHCD_QTD_BUFFER_POINTER_MASK                            0xFFFFF000

extern pUSB_HCD_BUS_INFO	pEhciBusInfo[];

/* Macro used for converting the CPU memory to bus specific memory */

#define USB_EHCD_CONVERT_TO_BUS_MEM(INDEX, ADDRESS) 			\
			(((ADDRESS) == NULL) ? 0:			\
	                 pEhciBusInfo[INDEX]->pFuncCpuToBus((pVOID)ADDRESS))

/* Macro used for converting the bus specific memory to CPU memory */
#define USB_EHCD_CONVERT_FROM_BUS_MEM(INDEX ,ADDRESS)                         \
                    (((ADDRESS) == 0) ? 0 : 				       \
			pEhciBusInfo[INDEX]->pFuncBusToCpu((UINT32)ADDRESS))

/* Macro used for swapping the 32 bit values */

#define USB_EHCD_SWAP_DATA(INDEX,VALUE)				       \
                    (((pEhciBusInfo[INDEX]->pFuncDataSwap) == NULL) ? VALUE : \
                                pEhciBusInfo[INDEX]->pFuncDataSwap(VALUE))                     

/* Macro used for swapping the contents of buffers */

#define USB_EHCD_SWAP_BUFDATA(INDEX,BUFFER,SIZE)			       \
                    (((pEhciBusInfo[INDEX]->pFuncBufferSwap) == NULL) ? (SIZE): \
                         pEhciBusInfo[INDEX]->pFuncBufferSwap(BUFFER, SIZE))

/* Bit positions of fields in  structure elements end */

/*******************************************************************************

 * Macro Name       : USB_EHCD_GET_BITFIELD

 * Description      : This macro is used to get the fields of EHCI data
                      structures.

 * Parameters       : INDEX	   IN Index of the host controller
 	
 *                    STRUCT_TYPE  IN Type of Data structure.

 *                    STRUCT_DATA  IN Struct variable

 *                    FIELD      IN Field of the structure

 * Return Type      : Value in the specified field of the register.

 ******************************************************************************/

#define USB_EHCD_GET_BITFIELD(INDEX,                                           \
                              STRUCT_TYPE,                                     \
                              STRUCT_DATA,                                     \
                              FIELD)                                           \
                                                                               \
        ((USB_EHCD_SWAP_DATA(INDEX, (UINT32)(STRUCT_DATA)) &                   \
            USB_EHCD_##STRUCT_TYPE##_##FIELD##_MASK)                           \
              >> USB_EHCD_##STRUCT_TYPE##_##FIELD##_E)

/*******************************************************************************

 * Macro Name       : USB_EHCD_SET_BITFIELD

 * Description      : This macro is used to set the fields of EHCI data
                      structures.

 * Parameters       : INDEX	   IN Index of the host controller
                      STRUCT_TYPE  IN Type of Data structure.
                      STRUCT_DATA  IN Struct variable
 *                    VALUE  IN value of field

 *                    FIELD      IN Field of the structure

 * Return Type      : Value in the specified field of the register.

 ******************************************************************************/
#define USB_EHCD_SET_BITFIELD(INDEX,                                           \
                              STRUCT_TYPE,                                     \
                              STRUCT_DATA,                                     \
                              VALUE,                                           \
                              FIELD)                                           \
                                                                               \
{                                                                              \
                                                                               \
        UINT32 uValueTemp = USB_EHCD_SWAP_DATA(INDEX, (UINT32)(STRUCT_DATA));  \
                                                                               \
        uValueTemp = (uValueTemp &                                             \
         (~USB_EHCD_##STRUCT_TYPE##_##FIELD##_MASK));     	               \
                                                                               \
        uValueTemp = (uValueTemp |                                             \
        (VALUE << USB_EHCD_##STRUCT_TYPE##_##FIELD##_E));                      \
                                                                               \
         STRUCT_DATA = USB_EHCD_SWAP_DATA(INDEX, uValueTemp);                  \
}


/* Errors specific to EHCD */
#define USB_EHCD_TIMEOUT                     ((INT8)15)

/* Data structure holding the status values of the bus initialization function */
typedef enum _USB_EHCD_BUS_STATUS
{
    USB_EHCD_HCBUS_INITIALIZED,       /* HC bus is initialized successfully */
    USB_EHCD_HC_NOT_PRESENT,       /* HC is not present */
    USB_EHCD_HCBUS_NOT_INITIALIZED    /* HC bus is not initialized */
}USB_EHCD_BUS_STATUS, *pUSB_EHCD_BUS_STATUS;


/* Forward declaration for the USB_EHCD_PIPE structure */
typedef struct _USB_EHCD_PIPE USB_EHCD_PIPE, *pUSB_EHCD_PIPE;

/*
 * Structure holding the format of the Queue Head.
 * This is to be created for every bulk, control and interrupt endpoint
 * created.
 */
typedef struct _USB_EHCD_QH
{
    struct _USB_EHCD_QH *pNext;     /* Pointer to the next QH element. */
    struct _USB_EHCD_QTD *pQTD;  /* Pointer to the first QTD attached to the QH. */
    VOID * pAllocatedMem;     /* Pointer to the allocated QH memory. */
    pUSB_EHCD_PIPE  pHCDPipe;    /*
                              * To hold the pointer to the
                              * USB_EHCD_PIPE data structure.
                              */
    UINT32 uSize;		/* Size of QH */		
    UINT32 uQueueHeadHorizontalLinkPointer; /* Queue head horizontal link ptr */
    UINT32 uEndPointCharacteristics;/* Endpoint characteristics */
    UINT32 uEndPointCapabilities;   /* Endpoint capabilities */
    UINT32 uCurrentQtdPointer;      /* Current QTD pointer */
    UINT32 uNextQtdPointer;         /* Next QTD pointer */
    UINT32 uAlternateNextQtdPointer;/* Alternate next QTD pointer */
    UINT32 uTransferInfo;   /* Transfer information */
    UINT32 uBufferPointerPageZero; /* Buffer page 0 */
    UINT32 uBufferPointerPageOne;  /* Buffer page 1 */
    UINT32 uBufferPointerPageTwo;  /* Buffer page 2 */
    UINT32 uBufferPointerPageThree; /* Buffer page 3 */
    UINT32 uBufferPointerPageFour;  /* Buffer page 4 */
} OS_STRUCT_PACKED USB_EHCD_QH, *pUSB_EHCD_QH ;

/*
 * Structure holding the format of the Transfer Descriptor to be queued
 * to the QH. This is to be created for a high speed interrupt, control
 * or bulk data transfer on the bus.
 */
typedef struct _USB_EHCD_QTD
{
    UINT16 uBytesToTransfer;  /* To hold the number of bytes to transfer */



    VOID * pAllocatedMem;         /* Pointer to the allocated qTD */

    struct _USB_EHCD_QTD *pNext;       /* Pointer to the next qTD in the queue */

    UINT32 uSize;		    /* Size of QTD */		
    UINT32 uNextQTDPointer;         /* Next QTD pointer */
    UINT32 uAlternateNextQTDPointer;/* Alternate next QTD pointer */
    UINT32 uTransferInfo;           /* Transfer information */
    UINT32 uBufferPagePointerList[5]; /* Array of buffer pointers */

}OS_STRUCT_PACKED USB_EHCD_QTD, *pUSB_EHCD_QTD ;

/*

 * Structure holding the format of the isochronous Transfer Descriptor.

 * This is to be created for a high speed isochronous data transfer on the bus.

 */

typedef struct _USB_EHCD_ITD

{
    VOID *   pAllocatedMem;  /* To hold the pointer to the allocated memory */
    UINT16 uFrameListIndex; /* To hold the index into the frame list */

    VOID *pPreviousTD; /* pointer to the previous iTD */
    VOID                 *pNext; /*
                              * To hold the pointer to the next element in
                              * the horizontal queue, It can be any type.
                              */
    struct _USB_EHCD_ITD *pVerticalNext;   /*
                                        * To hold the pointer to the next
                                        * element in the vertical queue
                                        */

    UINT8   uPreviousTDType; /* Type of the previous element */
    UINT8   uNextTDType; /* Type of the next element */
    UINT32 uSize;	/* Size of ITD */ 	
    UINT32 uNextLinkPointer;    /* Next link pointer */
    UINT32 uTransactionStatusControlList[8]; /* Transaction status and control
                                              * list
                                              */
    UINT32 uBufferPointerList[7];     /* Array of buffer pointers */

} OS_STRUCT_PACKED USB_EHCD_ITD, *pUSB_EHCD_ITD ;

/*
 * Structure holding the format of the split isochronous Transfer Descriptor.
 * This is to be created for a full speed isochronous data transfer on the bus.
 */
typedef struct _USB_EHCD_SITD
{
    UINT16 uBytesToTransfer;/* To hold the number of bytes to be transferred */

    VOID * pAllocatedMem;/* To hold the pointer to the allocated memory */

    UINT16 uFrameListIndex; /* To hold the index into the frame list */
    VOID * pNext;    /*
                     * To hold the pointer to the next element
                     * in the horizontal queue
                     */
    struct _USB_EHCD_SITD *pVerticalNext;   /*
                                        * To hold the pointer to the next
                                        * element in the vertical queue
                                        */
    VOID * pPreviousTD; /*
                             * To hold the pointer to the
                             * previous isochronous TD element.
                             */

    UINT8   uPreviousTDType; /* Type of the previous element */
    UINT8   uNextTDType; /* Type of the next element */
    UINT32 uSize;	/* Size of siTD */	
    UINT32 uNextLinkPointer;
    UINT32 uEndPointCharacteristics;
    UINT32 uMicroFrameScheduleControl;
    UINT32 uTransferState;
    UINT32 uBufferPointerList[2];
    UINT32 uBackPointer;
} OS_STRUCT_PACKED USB_EHCD_SITD, *pUSB_EHCD_SITD ;

/* Data structure holding the format of one element of the periodic frame list */
typedef UINT32 USB_EHCD_PERIODIC_FRAME_ELEMENT,* pUSB_EHCD_PERIODIC_FRAME_ELEMENT;

/* Forward declaration of the request information */
typedef struct _USB_EHCD_REQUEST_INFO USB_EHCD_REQUEST_INFO, *pUSB_EHCD_REQUEST_INFO;

/* Holds the information about a pipe */
struct _USB_EHCD_PIPE
{
    UINT16 uListIndex;  /* Index into the array of endpoint structures. */
    ULONG  uBandwidth;   /* Bandwidth reserved for this endpoint. */
    UINT8  uEndpointAddress; /* Address of the endpoint */
    UINT8  uAddress;   /* Address of the device holding the endpoint */
    UINT8  uSpeed;     /* Speed of the device */
    UINT8  uEndpointType; /* Type of endpoint */
    UINT8  uEndpointDir; /* Direction of the endpoint */
    UINT8  PipeDeletedFlag;/*
                           * Flag indicating whether the endpoint
                           * is being deleted.
                           */
    OS_EVENT_ID	DeleteSynchEventID; /*
                                      * Event ID used for synchronisation
                                      * of the cancel/delete pipe with HCD.
                                      */
    UINT16 uMaximumPacketSize; /* To hold the maximum packet size */
    pUSB_EHCD_QH pQH;         /*
                           * Pointer to the EHCD_QH data structure
                           * for this pipe.
                           */
    pUSB_EHCD_REQUEST_INFO pRequestQueueHead;  /*
                                            * Pointer to the head of the request
                                            * queue structure.
                                            */
    pUSB_EHCD_REQUEST_INFO pRequestQueueTail;  /*
                                            * Pointer to the tail of the request
                                            * queue structure.
                                            */
    UINT16 uHubInfo;      /*
                           * This holds any additional details needed for an
                           * endpoint like the details of the parent hub address
                           * and the port number to which the device is
                           * connected.
                           */
    UINT8   uUFrameMaskValue;   /*
                                 * To hold the mask value indicating
                                 * the microframes in which the transfer should
                                 * happen
                                 */
    struct _USB_EHCD_PIPE *pNext;/*
                              * Pointer to the next EHCD_PIPE pointer in the
                              * reclamation list
                              */
    UINT8 bIsHalted;    /*
                         * Flag indicating that the endpoint is halted. This is
                         * set only when the low/ full speed control/ bulk
                         * endpoint results in a transaction error.
                         */
                         
    INT16 uLastIndex; /*
                        * This index holds the index which was populated last
                        * during an isochronous transfer request
                        */
    INT16 uStartIndex; /* Start index of the last submitted URB */
}OS_STRUCT_PACKED;

/* Data structure maintained by HCD to hold the request information */
struct _USB_EHCD_REQUEST_INFO
{
    pUSBHST_URB pUrb;     /* Pointer to the URB */
    pUSB_EHCD_PIPE  pHCDPipe; /* Pointer to the HCD maintained data structure */
    UINT32 uAdditionalInfo; /*
                           * Additional information which need to be added for
                           * isochronous requests. For full speed isochronous
                           * requests, this holds the index value.
                           * For high speed isochronous requests, this holds
                           * the mask value indicating the microframes in which
                           * the data needs to be sent.
                           */
    VOID  *pHead;         /* Head of the TD queue for this request. */
    VOID  *pTail;         /* Tail of the TD queue for this request. */
    struct _USB_EHCD_REQUEST_INFO *pNext;/*
                                      * Pointer to the next queue element for
                                      * the endpoint.
                                      */
    struct _USB_EHCD_REQUEST_INFO *pListRequest;/*
                                             * Pointer to the next element in
                                             * the list of requests maintained.
                                             */
};

/* Data structure holding the details of a particular list */

typedef struct _USB_EHCD_LIST_DATA
{
    UINT32 	uBandwidth[USB_EHCD_MAX_MICROFRAME_NUMBER];/*
                                                    * Array holding the
                                                    * bandwidth reserved in all
                                                    * the microframes in a
                                                    * frame.
                                                    */
    UINT32   uNextListIndex;  /* Index to the next list in the tree */
    VOID *  pHeadPointer;    /*
                             * Pointer to the node element which forms the
                             * head of the list
                             */
    VOID * 	pTailPointer;    /* Pointer to the tail of the list. */
    UINT8   uHeadPointerType; /* type of the tail pointer in this list */
} USB_EHCD_LIST_DATA, *pUSB_EHCD_LIST_DATA;

/* This data structure holds the details of the Root hub */
typedef struct _USB_EHCD_RH_DATA
{
    UCHAR 	* pPortStatus;                   /*
                                            * Pointer to an array of port status
                                            * information
                                            */
    UCHAR	HubStatus[USB_EHCD_HUBSTATUS_SIZE];/* Buffer holding the hub status */
    UINT8		uNumDownstreamPorts;    /*
                                         * Number of downstream ports supported
                                         * by the Root hub.
                                         */
    UCHAR	*    pHubInterruptData;      /*
                                         * Buffer holding the data to be
                                         * returned on an interrupt request.
                                         */
    UINT32      uSizeInterruptData;     /*
                                         * To hold the size of
                                         * the interrupt data
                                         */
    pUSB_EHCD_PIPE	pControlPipe;           /* Control pipe information */
    pUSB_EHCD_PIPE  pInterruptPipe;         /*
                                         * Pointer to the USB_EHCD_PIPE data
                                         * structure for the interrupt endpoint.
                                         */
    UINT8		bRemoteWakeupEnabled;  /*
                                        * Flag indicating whether Remote Wakeup
                                        * is enabled or not
                                        */
    UINT8		bInterruptEndpointHalted; /*
                                           * Flag indicating whether the
                                           * interrupt endpoint is halted or not.
                                           */
    UINT8		uDeviceAddress;  /* Address of the Root hub. */
    UINT8		uConfigValue;    /* Value of the configuration which is set. */
}USB_EHCD_RH_DATA, *pUSB_EHCD_RH_DATA;

/* This is the data structure maintained for every EHCI Host Controller. */
typedef struct _USB_EHCD_DATA
{
    UINT32  uBusIndex; /* Index into the array of host controllers */
    UINT8   uIRQ;            /* To hold the Interrupt Request Number */
    UINT8   *pEHCBaseAddress; /*
                               * Base address of the host controller
                               * operational registers
                               */
    UINT8   *pPCIBaseAddress; /*
                               * Base address read from the PCI
                               * Configuration space
                               */
    UINT8	addressMode64; /*
                                   * Set to 1 if the addressing
                                   *  mode of HC is 64 bit
                                   */ 
    pUSB_EHCD_PERIODIC_FRAME_ELEMENT	pFrameList; /*
                                                 * Pointer to the tail of the
                                                 * isochronous list.
                                                 */
    VOID *	pAllocatedFrameList; /* Allocated memory for the frame list. */
    UINT    FrameBandwidth[USB_EHCD_MAX_FRAMELIST_SIZE]
                        [USB_EHCD_MAX_MICROFRAME_NUMBER];
                                                /*
                                                 * To hold the bandwidth
                                                 * occupied in the frame
                                                 */
    USB_EHCD_LIST_DATA 	FrameListData[USB_EHCD_MAX_FRAMELIST_SIZE]; /*
                                                             * Array maintained
                                                             * for the frame
                                                             * list data.
                                                             */
    USB_EHCD_LIST_DATA	TreeListData[USB_EHCD_MAX_TREE_NODES]; /*
                                                        * Array maintained for
                                                        * the list associated
                                                        * with a node.
                                                        */
    OS_EVENT_ID	    InterruptEvent; /*
                                 * Event indicating that an interrupt
                                 * has occurred.
                                 */
    OS_THREAD_ID	IntHandlerThreadID;/*
                                        * Thread ID of the thread which handles
                                        * EHCI interrupts.
                                        */

    OS_EVENT_ID	RequestSynchEventID; /*
                                      * Event ID used for synchronisation
                                      * of the requests.
                                      */
    USB_EHCD_RH_DATA	RHData;          /*
                                      * Data structure holding the details
                                      * of a Root hub.
                                      */
    pUSB_EHCD_REQUEST_INFO	pRequestQueueHead;/*
                                           * Pointer to the head of the request
                                           * queue maintained.
                                           */
    pUSB_EHCD_REQUEST_INFO	pRequestQueueTail;/*
                                           * Pointer to the tail of the request
                                           * queue maintained.
                                           */
    pUSB_EHCD_PIPE              pIsochPipeList; /*
                                            * Pointer to the head of the list
                                            * of Isochronous pipe
                                            */

    pUSB_EHCD_PIPE		pAsynchReclamationListHead;  /*
                                            * Pointer to the head of the list
                                            * of EHCD_PIPE data structures
                                            * which need to be removed.
                                            */
    pUSB_EHCD_PIPE		pPeriodicReclamationListHead;  /*
                                            * Pointer to the head of the list
                                            * of EHCD_PIPE data structures
                                            * which need to be removed.
                                            */

    OS_EVENT_ID	ReclamationListSynchEventID;/*
                                             * Event ID used for
                                             * synchronisation of the EHCD_PIPE
                                             * reclamation list.
                                             */
    pUSB_EHCD_PIPE	pDefaultPipe;            /* Default pipe information */

    pUSB_EHCD_PIPE  pAsynchTailPipe;         /*
                                             * Holds the tail of EHCD_PIPE list
                                             * maintained for the asynchronous
                                             * schedule.
                                             */

    OS_EVENT_ID	BandwidthEventID;           /*
                                             * Event used for synchronising
                                             * the bandwidth reservations
                                             */

    UINT32		uInterruptStatus;   /*
                                     * To hold the status of the interrupt.
                                     * This value is populated in the ISR.
                                     */ 
                                     
    pUSB_EHCD_REQUEST_INFO pHeadAsynchCancelList; /* To hold the head of the
                                                   * list of asynchronous
                                                   * requests which need to be
                                                   * cancelled
                                                   */

    pUSB_EHCD_REQUEST_INFO pHeadPeriodicCancelList; /* To hold the head of the
                                                     * list of periodic
                                                     * requests which need to be
                                                     * cancelled
                                                     */

}USB_EHCD_DATA, *pUSB_EHCD_DATA;

#ifdef	__cplusplus
}
#endif

#endif /* End of __EHCD_DATASTRUCTURES_H__ */
/********************** End of file EHCD_DataStructures.h**********************/
