/* usbBusAbstractionLayer.h - USB Bus Abstraction Layer Definitions */

/* Copyright 2004 - 2005 Wind River Systems, Inc. 

   This software includes software licensed to Wind River Systems, Inc.
   by Wipro, Ltd. Wind River licensees may use this software according  
   to the terms of their Wind River license agreement(s) applicable to 
   this software.
*/

/*
Modification history
--------------------
01d,02mar05,ami  Macros for Maximum Host Controllers removed
01c,22Jul03,gpd  usage of a common bus abstraction layer file for all platforms
01b,08jul03,nld  Changes to make use of usbPci... calls
01a,17mar03,ssh  written
*/

/*
DESCRIPTION

This file contains the definitions to access the HCD Controllers on the PCI Bus.
*/

/*
INTERNAL 
 *******************************************************************************
 * Filename         : BusAbstractionLayer.h
 *
 * Copyright        :
 *
 * THE COPYRIGHT IN THE CONTENTS OF THIS SOFTWARE VEST WITH WIPRO
 * LIMITED A COMPANY INCORPORATED UNDER THE LAWS OF INDIA AND HAVING
 * ITS REGISTERED OFFICE AT DODDAKANNELLI SARJAPUR ROAD  BANGALORE
 * 560 035. DISTRIBUTION OR COPYING OF THIS SOFTWARE BY
 * ANY INDIVIDUAL OR ENTITY OTHER THAN THE ADDRESSEE IS STRICTLY
 * PROHIBITED AND MAY INCUR LEGAL LIABILITY. IF YOU ARE NOT THE
 * ADDRESSEE PLEASE NOTIFY US IMMEDIATELY BY PHONE OR BY RETURN EMAIL.
 * THE ADDRESSEE IS ADVISED TO MAINTAIN THE PROPRIETARY INTERESTS OF
 * THIS COPYRIGHT AS PER APPLICABLE LAWS.
 *
 *
 * Description      : This file contains functions to access the OHCI Controller
 *                    on the PCI Bus.
 *
 * FIX ME           : None.
 *
 * TO DO            : None.
 *
 ******************************************************************************/

#ifndef __INCusbBusAbstractionLayerh
#define __INCusbBusAbstractionLayerh

#ifdef	__cplusplus
extern "C" {
#endif

/* includes */

/*
 * This file is a VxWorks BSP specific file.
 *
 * This file is included for obtaining the platform specific information.
 * For example on a pcPentium environment, there is a difference in interrupt
 * allocation. This file will be used to obtain such information.
 */
#include "drv/pci/pciConfigLib.h"
#include "drv/pci/pciIntLib.h"
#include "vmLib.h"
#include "cacheLib.h"
#include "usb/usbPlatform.h"
#include "usb/usbPciLib.h"


/* defines */

/*
 * Offset of the class code register.
 *
 * NOTE: The offset of the class code register is 0x09. The class code
 *       information is stored at locations 0x09, 0x0A and 0x0B.
 *
 *       In order to reduce the number of read operations, the following
 *       logic is used.
 *
 *       a) Read the data (UINT32) from offset 0x08 (instead of 0x09)
 *       b) Shift the data read by 8 bits to the right.
 *
 *       If an offset of 0x09 is passed to the READ_BUS_LONG() function, it
 *       will return an error (since the offset is not aligned to word [32
 *       bit] boundary).
 */
#define CLASS_CODE_REGISTER_OFFSET                          0x08

/* To hold the offset of the base address register */
#define BASE_ADDRESS_REGISTER_OFFSET                        0x10

/* To hold the class code for UHCI Complaint USB Host Controllers */
#define USB_UHCI_HOST_CONTROLLER                            0x000C0300

/* To hold the class code for OHCI Complaint USB Host Controllers */
#define USB_OHCI_HOST_CONTROLLER                            0x000C0310

/* To hold the class code for EHCI Complaint USB Host Controllers */
#define USB_EHCI_HOST_CONTROLLER                            0x000C0320

/*
 * Specifies the maximum number of USB controllers supported.
 *
 * NOTE: This is a configurable parameter. Based on the requirement, this
 *       can be changed.
 */

/************* MACROS TO HOLD THE STATUS OF PCI FUNCTION CALLS ****************/

/* To specify the request completed successfully */
#define BUS_OPERATION_SUCCESS   OK

/* To specify the request failed */
#define BUS_OPERATION_FAILURE   ERROR

/* To hold the request status */
#define BUS_OPERATION_STATUS    STATUS

/******************* MODULE SPECIFIC VARIABLES DEFINITION *********************/
extern int usbPciIntEnable(UINT16); 
extern int usbPciIntDisable(UINT16); 

/************************* GLOBAL MACRO DEFINITION ****************************/

/*******************************************************************************
 * Macro Name  : FIND_USB_CONTROLLER
 * Description : This function is used to detect USB Controllers on the
 *               PCI Bus. This function will search for a device based
 *               on the PCI class code. If there are multiple devices with
 *               the same PCI class code, the device index is used to uniquely
 *               reference the devices.
 * Parameters  : nClassCode             IN  PCI class code for the device.
 *               nDeviceIndex           IN  Device Index for the device.
 *               pPCIBusNumber          OUT PCI bus number corresponding to
 *                                          the bus on which the device is
 *                                          connected.
 *               pPCIDeviceNumber       OUT PCI device number for the device.
 *               pPCIFunctionNumber     OUT PCI function number for the device.
 * Return Type : TRUE if USB controller is found, FALSE otherwise
 ******************************************************************************/
#define FIND_USB_CONTROLLER(nClassCode,                                 \
                            nDeviceIndex,                               \
                            pPCIBusNumber,                              \
                            pPCIDeviceNumber,                           \
                            pPCIFunctionNumber)                         \
                                                                        \
        usbPciClassFind(((nClassCode >> 16) & 0x00FF),                  \
                        ((nClassCode >> 8) & 0x0000FF),                 \
                        (nClassCode & 0x000000FF),                      \
                        nDeviceIndex,                                   \
                        (UINT8 *)pPCIBusNumber,                         \
                        (UINT8 *)pPCIDeviceNumber,                      \
                        (UINT8 *)pPCIFunctionNumber)



/*******************************************************************************
 * Macro Name  : GET_CLASS_CODE
 * Description : This function is used to obtain the class code of the
 *               USB Host Controller.
 * Parameters  : nPCIBusNumber      IN  Bus number corresponding to the device.
 *               nPCIDeviceNumber   IN  Device number corresponding to the
 *                                      device.
 *               nPCIFunctionNumber IN  Function number corresponding to the
 *                                      device.
 *               pClassCodeBuffer   OUT Pointer to the buffer which will
 *                                      hold the data read.
 * Return Type : None
 ******************************************************************************/
#define GET_CLASS_CODE(nPCIBusNumber,                                   \
                       nPCIDeviceNumber,                                \
                       nPCIFunctionNumber,                              \
                       pClassCodeBuffer)                                \
                                                                        \
        READ_BUS_LONG(nPCIBusNumber,                                    \
                      nPCIDeviceNumber,                                 \
                      nPCIFunctionNumber,                               \
                      CLASS_CODE_REGISTER_OFFSET,                       \
                      pClassCodeBuffer)



/*******************************************************************************
 * Macro Name  : REGISTER_INTERRUPT_HANDLER
 * Description : This function is used to register the interrupt hander for
 *               the OHCI Coontroller.
 *
 *               Attach the interrupt handler. The pciIntConnect() adds the
 *               interrupt handler for the specified IRQ. If multiple interrupt
 *               handlers are connected to the same interrupt number, this
 *               function creates a linked list.
 *
 *               NOTE: Do not use the IRQ number directly. Different BSP have
 *                     different mapping for the IRQ numbers to be actual
 *                     IRQ. For example, on pcPentium IRQ0 is assigned interrupt
 *                     number 0x20. Use the INT_VEC_GET() macro to obtain the
 *                     interrupt number.
 *
 * Parameters  : nIRQNumber  IN  IRQ number for the OHCI controller.
 *               pISR        IN  Pointer to the ISR function.
 *               nParameter  IN  Parameter to be passed to the ISR function.
 * Return Type : OK if interrupt handler is registered successfully, ERROR
 *               otherwise
 ******************************************************************************/
#define REGISTER_INTERRUPT_HANDLER(nIRQNumber,                          \
                                   pISR,                                \
                                   nParameter)                          \
                                                                        \
        usbPciIntConnect(                                               \
            (INT_HANDLER_PROTOTYPE)pISR,                                \
            (pVOID)nParameter,                                          \
            (UINT16)nIRQNumber)

/*******************************************************************************
 * Macro Name  : UNREGISTER_INTERRUPT_HANDLER
 * Description : This function is used to unregister the interrupt handler
 *               for the OHCI Controller.
 *
 *               NOTE: Do not use the IRQ number directly. Different BSP have
 *                     different mapping for the IRQ numbers to be actual
 *                     IRQ. For example, on pcPentium IRQ0 is assigned interrupt
 *                     number 0x20. Use the INT_VEC_GET() macro to obtain the
 *                     interrupt number.
 *
 * Parameters  : nIRQNumber  IN  IRQ number for the OHCI controller.
 *               pISR        IN  Pointer to the ISR function.
 * Return Type : None
 ******************************************************************************/
#define UNREGISTER_INTERRUPT_HANDLER(nIRQNumber,                        \
                                     pISR,                              \
	                             nParameter)                        \
        usbPciIntRestore(                                               \
            (INT_HANDLER_PROTOTYPE)pISR,                                \
            (pVOID)nParameter,                                          \
            (UINT16)nIRQNumber)

/******************* MACROS TO ENABLE OR DISABLE INTERRUPTS *******************/
/*******************************************************************************
 * Function Name  : DISABLE_BUS_INTERRUPT
 * Description    : Macro to disable the interrupts for the OHCI Controller.
 * Parameters     : nIRQNumber  IN  IRQ number for the OHCI controller.
 * Return Type    : None
 ******************************************************************************/
#define DISABLE_BUS_INTERRUPT(nIRQNumber)                               \
        usbPciIntDisable((nIRQNumber))


/*******************************************************************************
 * Function Name  : ENABLE_BUS_INTERRUPT
 * Description    : Macro to enable the interrupts for the OHCI Controller.
 * Parameters     : nIRQNumber  IN  IRQ number for the OHCI controller.
 * Return Type    : None
 ******************************************************************************/

#define ENABLE_BUS_INTERRUPT(nIRQNumber)           \
        usbPciIntEnable(nIRQNumber)

/******************** MODULE SPECIFIC MACRO DEFINITION ************************/

/*******************************************************************************
 * Macro Name  : READ_BUS_LONG
 * Description : This function is used to read a long (32 bits) from the
 *               PCI configuration ROM.
 * Parameters  : nPCIBusNumber       IN  PCI bus number corresponding to the
 *                                       device.
 *               nPCIDeviceNumber    IN  PCI device number corresponding
 *                                       to the device.
 *               nPCIFunctionNumber  IN  PCI function number corresponding 
 *                                       to the device.
 *               nOffset             IN  Offset into the configuration ROM
 *                                       from where the data should be read.
 *               pBuffer             OUT Pointer to the buffer which will
 *                                       hold the data read.
 * Return Type : None
 ******************************************************************************/
#define READ_BUS_LONG(nPCIBusNumber,                                    \
                      nPCIDeviceNumber,                                 \
                      nPCIFunctionNumber,                               \
                      nOffset,                                          \
                      pBuffer)                                          \
                                                                        \
        *(pBuffer) = usbPciDwordGet(nPCIBusNumber,                      \
                                    nPCIDeviceNumber,                   \
                                    nPCIFunctionNumber,                 \
                                    nOffset)

/*******************************************************************************
 * Macro Name  : READ_BUS_BYTE
 * Description : This function is used to read a byte (8 bits) from the
 *               PCI configuration ROM. 
 * Parameters  : nPCIBusNumber       IN  PCI bus number corresponding to the 
 *                                       device.
 *               nPCIDeviceNumber    IN  PCI device number corresponding
 *                                       to the device.
 *               nPCIFunctionNumber  IN  PCI function number corresponding 
 *                                       to the device.
 *               nOffset             IN  Offset into the configuration ROM
 *                                       from where the data should be read.
 *               pBuffer             OUT Pointer to the buffer which will
 *                                       hold the data read.
 * Return Type : None
 ******************************************************************************/
#define READ_BUS_BYTE(nPCIBusNumber,                                    \
                      nPCIDeviceNumber,                                 \
                      nPCIFunctionNumber,                               \
                      nOffset,                                          \
                      pBuffer)                                          \
        *(pBuffer) = usbPciByteGet(nPCIBusNumber,                       \
                                   nPCIDeviceNumber,                    \
                                   nPCIFunctionNumber,                  \
                                   nOffset)


#ifdef	__cplusplus
}
#endif

#endif /* __INCusbBusAbstractionLayerh */

/********************** End of File BusAbstractionLayer.h *********************/

