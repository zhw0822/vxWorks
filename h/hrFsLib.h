/* hrfsLib.h - Highly reliabe file system header file */

/*
 * Copyright (c) 2005 Wind River Systems, Inc. 
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement. 
 */ 
 
/* modification history
--------------------
01z,26aug05,jlk  Added HRFS_BAD_CHAR macro
01y,05aug05,jlk  Added prototype for hrfsChkDsk()
01x,04aug05,jlk  Moved formatter function pointer, hrfsFmtRtn, from hrFsLibP.h
01w,03aug05,pcm  changed parameters to hrfsDevLibInit()
01v,26jul05,jlk  removed unused errnos
01u,22jul05,jlk  code review changes
01t,17jun05,jlk  removed format structure. Adjusted and added formatter 
                 prototypes
01s,07jun05,jlk  added format structure
01r,18may05,jlk  updated errnos and added prototypes
01q,10may05,pcm  changed parameters to hrfsFormat()
01p,29apr05,jlk  updated HRFS version numbers.
01o,29apr05,jlk  updated errnos.
01n,25apr05,jlk  added version #defines and id string from hrFsLibP.h
01m,21apr05,jlk  updated HRFS errno codes
01l,18apr05,pcm  moved _PC_xxx macros for pathconf() to unistd.h
01k,18apr05,pcm  added _PC_xxx macros for pathconf stuff
01j,11apr05,pcm  changed HRFS_MAX_FILENAME_LEN from 252 to 251
01i,10mar05,act  remove phys block size parameter to hrfsDevCreate
01h,09mar05,pcm  added hrfsFormat() and hrfsFormatLibInit()
01g,08mar05,jlk  hrfsDevCreate() uses a device_t now for the XBD
01f,08mar05,pcm  added hrfsDevLibInit() 
01e,08mar05,jlk  added another errno
01d,21feb05,jlk  added an errno
01c,01feb05,pcm  fixed compiler errors
01b,01feb05,pcm  added HRFS time routines
01a,24jan05,jlk  written
*/

#ifndef __INChrfsLib
#define __INChrfsLib

/* includes */

#include "vxWorks.h"

#include "ioLib.h"
#include "time.h"
#include "drv/xbd/xbd.h"

/* defines */

/* Identifiers and version numbers */
#define HRFS_ID_STRING     "WR_HRFS"

#define HRFS_MAJOR_VERSION   0x01
#define HRFS_MINOR_VERSION   0x01

/* The maximum chars in a path, excluding EOS */
#define HRFS_MAX_PATH_LEN         PATH_MAX
/* The maximum chars in a file name, including EOS */
#define HRFS_MAX_FILENAME_LEN     (251)
/* Defines a character that is not allowed in file/directory name */
#define HRFS_BAD_CHAR    (':')
/* Defines how deeply nested subdirs can be */
#define HRFS_MAX_DIRECTORY_DEPTH  MAX_DIRNAMES
/* Defines how many hard links to a file can exist */
#define HRFS_MAX_HARD_LINKS       ((linkCount_t) -1)

/* Limits for the logical block sizes */

/* The minium supported logical block size in bytes */
#define HRFS_MIN_LOGICAL_BLK_SIZE     512
/* The minium supported logical block size in bytes as a power of 2 */
#define HRFS_MIN_LOGICAL_BLK_SIZE_2   9
/* The maximum supported logical block size in bytes */
#define HRFS_MAX_LOGICAL_BLK_SIZE     (8192)
/* The maximum supported logical block size in bytes as a power of 2 */
#define HRFS_MAX_LOGICAL_BLK_SIZE_2   13

/* Limits for the physical block sizes */

/* The minium supported logical block size in bytes */
#define HRFS_MIN_PHYSICAL_BLK_SIZE    512
/* The minium supported logical block size in bytes as a power of 2 */
#define HRFS_MIN_PHYSICAL_BLK_SIZE_2  9
/* The maximum supported logical block size in bytes */
#define HRFS_MAX_PHYSICAL_BLK_SIZE    HRFS_MAX_LOGICAL_BLK_SIZE
/* The maximum supported logical block size in bytes as a power of 2 */
#define HRFS_MAX_PHYSICAL_BLK_SIZE_2  HRFS_MAX_LOGICAL_BLK_SIZE_2


#define HRFS_DEV_MIN_BUFFERS   6    /* minimum # of buffers for HRFS devices */

/* error codes */

#define S_hrfsLib_DISK_FULL                (M_hrfsLib |  1)
#define S_hrfsLib_OUT_OF_INODES            (M_hrfsLib |  2)
#define S_hrfsLib_INVALID_TMR              (M_hrfsLib |  3)
#define S_hrfsLib_UNKNOWN_VOLUME_FORMAT    (M_hrfsLib |  4)
#define S_hrfsLib_BLOCK_NOT_FOUND          (M_hrfsLib |  5)
#define S_hrfsLib_INODE_JOURNAL_FULL       (M_hrfsLib |  6)

/* macros */

/*
 * Convert the number of ticks to milliseconds (rounding down).	 This macro
 * does not guard against overflow.  That would only be an issue on incredibly
 * large tick count conversions.  For example, if the system clock rate were
 * 1 million ticks per second, the overflow could be encountered after the
 * equivalent of 585 years worth of ticks.
 */

#define TICKS_TO_MILLISECONDS(x)    ((1000ULL * (x)) / sysClkRateGet ())

/*
 * Convert the number of milliseconds to ticks (rounding down).	 This macro
 * does not guard against overflow.  That would only be an issue on incredibly
 * large millisecond conversions.  For example, if the system clock rate were
 * 1 million ticks per second, the overflow would be encountered after over
 * half a million years worth of milliseconds.
 */

#define MILLISECONDS_TO_TICKS(x)    (((x) * sysClkRateGet ()) / 1000ULL);


/* typedefs */
/* Time in HRFS is a 64 bit signed quantity of ms since 1970 */
typedef INT64   hrfsTime_t;

typedef struct hrfs_tm
    {
    struct tm tm;    /* original [tm] structure.                   */
    int       msec;  /* milliseconds after the second	- [0..999] */
    } HRFS_TM;


typedef struct hrfs_dev *           HRFS_DEV_ID;
typedef struct hrfs_volume_desc *   HRFS_VOLUME_DESC_ID;

/* forward declarations */

#ifdef __cplusplus
extern "C" {
#endif
extern STATUS (*hrfsFmtRtn)( char *path, UINT64 diskSize, UINT32 blockSize, 
                             UINT32 files ); 

extern STATUS      hrfsFormatLibInit (void);
extern STATUS      hrfsFormatFd (int fd, UINT64 diskSize,
                               UINT32 blkSize, UINT32 numInodes);
extern STATUS      hrfsFormat (char *path, UINT64 diskSize,
                               UINT32 blkSize, UINT32 numInodes);

extern STATUS      hrfsDevLibInit (int maxBufs, int maxFiles);
extern HRFS_DEV_ID hrfsDevCreate (char * pDevName, device_t xbd, int numBufs,
                                  int maxFiles);
extern STATUS      hrfsDevCreate2 (device_t  xbdId, devname_t pDevName);
extern STATUS      hrfsDevDelete (HRFS_DEV_ID hrfsDevId);
extern int         hrfsDiskProbe (device_t  xbdDevice);

extern hrfsTime_t hrfsTimeGet      (void);
extern void       hrfsTimeSet      (hrfsTime_t msec);
extern int        hrfsAscTime      (HRFS_TM * pHrfsTm, char * pBuffer,
                                    size_t bufLength);
extern STATUS     hrfsTimeSplit    (hrfsTime_t msec, HRFS_TM * pHrfsTm);
extern hrfsTime_t hrfsTimeCondense (HRFS_TM * pHrfsTm);

extern STATUS     hrfsChkDsk (char *path, int verbLevel, int flag);


#ifdef __cplusplus
    }
#endif

#endif /* __INChrfsLib */

