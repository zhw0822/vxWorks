/* windPwrLib.h - Power Management header file */

/* Copyright 2002 - 2003 Wind River Systems, Inc */

/*
modification history
--------------------
01d,19oct04,rec  SPR 10255 - move POWER_MGMT_DEFAULT_MODE here
                 add sysCpuPwrEnable prototype
01c,06nov03,rec  make WIND_PWR_DOWN_RTN void fct
01b,26jun03,rec  code review comments
01a,26apr02,rec  written.
*/

/*
* DESCRIPTION
* This header contains the datatype and prototype declarations for
* power management
*/

#ifndef __INCwindPwrLibh
#define __INCwindPwrLibh

#ifdef __cplusplus
extern "C" {
#endif

/* POWER MANAGEMENT DEFAULT MODE */

#ifdef INCLUDE_CPU_SLEEP_MODE_OFF
#define POWER_MGMT_DEFAULT_MODE windPwrModeOff
#elif defined INCLUDE_CPU_SLEEP_MODE_SHORT
#define POWER_MGMT_DEFAULT_MODE windPwrModeShort
#elif defined INCLUDE_CPU_SLEEP_MODE_LONG
#define POWER_MGMT_DEFAULT_MODE windPwrModeLong
#else
#define POWER_MGMT_DEFAULT_MODE windPwrModeLong
#endif /* INCLUDE_CPU_SLEEP_MODE_OFF */

/*
* windPwrModeShort is the CPU power mode where the CPU sleeps between
* system clock ticks but always wakes up for every tick.  In other
* words, the tick interrupt source is not turned off when this CPU
* power mode is set.
*
* windPwrModeLong is the CPU power mode where the CPU sleeps for
* multiple ticks at a time or until an external interrupt wakes
* the system.  With this mode, the BSP turns off the tick interrupt
* source and schedules an interrupt to occur some number of ticks
* in the future.
*/

typedef enum windPwrMode
    {
        windPwrModeOff      = 0,
        windPwrModeShort    = 1,
        windPwrModeLong     = 2
    } WIND_PWR_MODE;

typedef void (*WIND_PWR_DOWN_RTN)( WIND_PWR_MODE mode, ULONG nTicks);
typedef void (*WIND_PWR_UP_RTN)( WIND_PWR_MODE mode, ULONG * nTicks);

/* function prototypes */

#ifdef __STDC__

extern void             sysCpuPwrEnable   (void);
extern void             windPwrModeSet    (WIND_PWR_MODE mode);
extern WIND_PWR_MODE    windPwrModeGet    (void);
extern void             windPwrDownRtnSet (WIND_PWR_DOWN_RTN dRtn);
extern void             windPwrUpRtnSet   (WIND_PWR_UP_RTN   uRtn);
extern void             windPwrDown       (void);

#else

extern void             sysCpuPwrEnable   ();
extern void             windPwrModeSet    ();
extern WIND_PWR_MODE    windPwrModeGet    ();
extern void             windPwrDownRtnSet ();
extern void             windPwrUpRtnSet   ();
extern void             windPwrDown       ();

#endif	/* __STDC__ */

#ifdef __cplusplus
}
#endif

#endif  /* __INCwindPwrLibh */
