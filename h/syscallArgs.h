/* syscallArgs.h - Top-level file to include the syscall argument structures */

/* Copyright 2003 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*
modification history
--------------------
01c,27apr04,mil  Fixed names for MIPS include files.
01b,15dec03,yvp  Changed arch-specific header file names.
01a,10dec03,yvp	 written.
*/

#ifndef __INCsyscallArgsh
#define __INCsyscallArgsh

#ifdef __cplusplus
extern "C" {
#endif

#if     (CPU_FAMILY==PPC)
#include "arch/ppc/syscallArgsppc.h"
#endif

#if	(CPU_FAMILY==ARM)
#include "arch/arm/syscallArgsarm.h"
#endif

#if (CPU_FAMILY==MIPS)
# if (_WRS_INT_REGISTER_SIZE == 8)
#   if (_BYTE_ORDER == _LITTLE_ENDIAN)
#     include "arch/mips/syscallArgsMIPS64LE.h"
#   elif (_BYTE_ORDER == _BIG_ENDIAN)
#     include "arch/mips/syscallArgsMIPS64BE.h"
#   else  /* _BYTE_ORDER */
#     error "Byte order cannot be determined\n"
#   endif  /* _BYTE_ORDER */
# elif (_WRS_INT_REGISTER_SIZE == 4)
#   include "arch/mips/syscallArgsMIPS32.h"
# else  /* _WRS_INT_REGISTER_SIZE */
#   error "Register size cannot be determined\n"
# endif  /* _WRS_INT_REGISTER_SIZE */
#endif  /* CPU_FAMILY==MIPS */

#if	(CPU_FAMILY==I80X86)
#include "arch/i86/syscallArgsx86.h"
#endif

#if	(CPU_FAMILY==SH)
#if		(CPU == SH7600)
#include "arch/sh/syscallArgssh2.h"
#elif		(CPU == SH7700)
#include "arch/sh/syscallArgssh3.h"
#elif		(CPU == SH7750)
#include "arch/sh/syscallArgssh4.h"
#endif 
#endif

#if   (CPU_FAMILY==SIMNT)
#include "arch/simnt/syscallArgsx86.h"
#endif

#if   (CPU_FAMILY==SIMLINUX)
#include "arch/simlinux/syscallArgsx86.h"
#endif

#if   (CPU_FAMILY==SIMSPARCSOLARIS)
#include "arch/simsolaris/syscallArgssparc.h"
#endif


#ifdef __cplusplus
}
#endif

#endif /* __INCsyscallArgsh */
