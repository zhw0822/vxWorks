/* xbdBlkDev.h - BLK_DEV to XBD interface converter */

/*
 * Copyright (c) 2005 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01d,25aug05,pcm  added xbdBlkDevCreateSync()
01c,15jul05,pcm  added xbdBlkDevDelete()
01b,08mar05,pcm  added xbdBlkDevLibInit()
01a,31jan05,rfr  Fixed inclusion of xbd.h
*/

#ifndef __INCxbdBlkDevh
#define __INCxbdBlkDevh


#include <blkIo.h>
#include <drv/xbd/xbd.h>


extern STATUS   xbdBlkDevLibInit     (void);
extern device_t xbdBlkDevCreate      (BLK_DEV *bd, const char *name);
extern device_t xbdBlkDevCreateSync  (BLK_DEV *bd, const char *name);
extern STATUS   xbdBlkDevDelete      (device_t d, BLK_DEV **  ppbd);

#endif
