/* taskLibCommon.h - VxWorks tasking library common (user/kernel) definitions */

/*
 * Copyright (c) 2003-2005 Wind River Systems, Inc.
 *
 * The right to copy, distribute or otherwise make use of this software
 * may be licensed only pursuant to the terms of an applicable Wind River
 * license agreement. 
*/

/*
modification history
--------------------
01x,01apr05,kk   remove old coprocessor options, now all archs PAL'ifies
                 coprocs
01w,28sep04,aeg  added td_excStackCurrent to TASK_DESC.
01v,27sep04,tam  updated VX_NO_STACK_PROTECT documentation
01u,22sep04,md   added taskSafe()/taskUnsafe() prototype.
01u,03aug04,scm  add hook for coprocArm.h...
01t,20may04,dcc  added taskUnlink() prototype.
01s,05may04,cjj  added VX_USR_TASK_OPTIONS.
01r,29apr04,h_k  added SH coprocessor support.
01q,22apr04,dcc  added inclusion of objLib.h
01p,25mar04,dbt  Added SIMPENTIUM support.
01o,13apr04,zmm  Add Pentium coprocessor.
01n,29mar04,dcc  added taskClose() prototype. Moved VX_TASK_OBJ_OWNER option
		 bit to kernel taskLib.h
01m,29mar04,aeg  updated taskOpen() function prototype.
01l,22jan04,aeg  added coprocessor related task option bits.
01k,25nov03,aeg  added S_taskLib_ILLEGAL_OPERATION errno;
		 changed taskOpen() function prototype.
01j,14nov03,kk   added VX_NO_STACK_PROTECT option
01i,31oct03,aeg  added taskIsPended().
01h,10oct03,pcm  added td_pExcStackStart; removed td_pStackLimit
01g,09oct03,aeg  added S_taskLib_NO_TCB errno.
01f,15sep03,kk   added exception stack info and RTP id
01e,12sep03,aeg  added S_taskLib_ILLEGAL_OPTIONS errno.
01d,10sep03,dcc  moved more function prototypes from taskLib.h
01c,09sep03,dcc  added #ifndef	_ASMLANGUAGE directives.
01b,05sep03,dcc  added function prototypes.
01a,19aug03,aeg  written based on v05b of kernel version of taskLib.h
*/

#ifndef __INCtaskLibCommonh
#define __INCtaskLibCommonh

#ifdef __cplusplus
extern "C" {
#endif

#include "vwModNum.h"
#include "eventLibCommon.h"
#include "objLib.h"

/* generic status codes */

#define S_taskLib_NAME_NOT_FOUND		(M_taskLib | 101)
#define S_taskLib_TASK_HOOK_TABLE_FULL		(M_taskLib | 102)
#define S_taskLib_TASK_HOOK_NOT_FOUND		(M_taskLib | 103)
#define S_taskLib_TASK_SWAP_HOOK_REFERENCED	(M_taskLib | 104)
#define S_taskLib_TASK_SWAP_HOOK_SET		(M_taskLib | 105)
#define S_taskLib_TASK_SWAP_HOOK_CLEAR		(M_taskLib | 106)
#define S_taskLib_TASK_VAR_NOT_FOUND		(M_taskLib | 107)
#define S_taskLib_TASK_UNDELAYED		(M_taskLib | 108)
#define S_taskLib_ILLEGAL_PRIORITY		(M_taskLib | 109)
#define S_taskLib_ILLEGAL_OPTIONS		(M_taskLib | 110)
#define S_taskLib_NO_TCB			(M_taskLib | 111)
#define S_taskLib_ILLEGAL_OPERATION		(M_taskLib | 112)


/* miscellaneous */

#define VX_TASK_NAME_LENGTH	31	/* max length of name in TASK_DESC */

/* 
 * Task option bits.  BE SURE to modify either of the 
 * VX_USR_TASK_OPTIONS_BASE or VX_USR_TASK_OPTIONS definitions 
 * below when modifying/adding individual task options.
 */


#define VX_PRIVATE_ENV		0x0080	/* 1 = private environment variables */
#define VX_NO_STACK_FILL	0x0100	/* 1 = avoid stack fill of 0xee */
#define VX_TASK_NOACTIVATE	0x2000  /* taskOpen() does not taskActivate() */
#define VX_NO_STACK_PROTECT	0x4000  /* no over/underflow stack protection,*/
					/* stack space remains executable     */


/* define for all valid user task options */

#define VX_USR_TASK_OPTIONS_BASE    (VX_PRIVATE_ENV 		| \
				     VX_NO_STACK_FILL 		| \
				     VX_TASK_NOACTIVATE		| \
				     VX_NO_STACK_PROTECT)

/* 
 * All 6.0 architectures have PAL'ified the coprocessor layer by specifying 
 * _WRS_PAL_COPROC_LIB.  The definition of the coprocessor task options 
 * (e.g. VX_FP_TASK, VX_DSP_TASK, VX_ALTIVEC_TASK) are now defined in 
 * the architecture file target/h/arch/<arch>/coproc<Arch>.h. 
 */

#define VX_COPROC1_TASK     	0x01000000
#define VX_COPROC2_TASK		0x02000000
#define VX_COPROC3_TASK     	0x04000000
#define VX_COPROC4_TASK     	0x08000000
#define VX_COPROC5_TASK     	0x10000000
#define VX_COPROC6_TASK     	0x20000000
#define VX_COPROC7_TASK     	0x40000000
#define VX_COPROC8_TASK     	0x80000000

/* define for all valid user task options */

#define VX_USR_TASK_OPTIONS    (VX_USR_TASK_OPTIONS_BASE	| \
				VX_COPROC1_TASK			| \
				VX_COPROC2_TASK			| \
				VX_COPROC3_TASK			| \
				VX_COPROC4_TASK			| \
				VX_COPROC5_TASK			| \
				VX_COPROC6_TASK			| \
				VX_COPROC7_TASK			| \
				VX_COPROC8_TASK)	

#if (CPU_FAMILY == PPC)
#include "arch/ppc/coprocPpc.h"
#endif /* (CPU_FAMILY == PPC) */

#if (CPU_FAMILY == I80X86)
#include "arch/i86/coprocI86.h"
#endif /* (CPU_FAMILY == I80X86) */

#if (CPU_FAMILY == ARM)
#include "arch/arm/coprocArm.h"
#endif /* (CPU_FAMILY == ARM) */

#if (CPU_FAMILY == MIPS)
#include "arch/mips/coprocMips.h"
#endif /* (CPU_FAMILY == MIPS) */

#if (CPU_FAMILY == SH)
#include "arch/sh/coprocSh.h"
#endif /* (CPU_FAMILY == SH) */

#if (CPU_FAMILY == SIMNT)
#include "arch/simnt/coprocSimnt.h"
#endif /* (CPU_FAMILY == SIMNT) */

#if (CPU_FAMILY == SIMLINUX)
#include "arch/simlinux/coprocSimlinux.h"
#endif /* (CPU_FAMILY == SIMLINUX) */

#if (CPU_FAMILY == SIMPENTIUM)
#include "arch/simpentium/coprocSimpentium.h"
#endif /* (CPU_FAMILY == SIMPENTIUM) */

#if (CPU_FAMILY == SIMSPARCSOLARIS)
#include "arch/simsolaris/coprocSimsolaris.h"
#endif /* (CPU_FAMILY == SIMSPARCSOLARIS) */

/* typedefs */

#ifndef	_ASMLANGUAGE

typedef struct			/* TASK_DESC - information structure */
    {
    int			td_id;		/* task id */
    int			td_priority;	/* task priority */
    int			td_status;	/* task status */
    int			td_options;	/* task option bits (see below) */
    FUNCPTR		td_entry;	/* original entry point of task */
    char *		td_sp;		/* saved stack pointer */
    char *		td_pStackBase;	/* the bottom of the stack */
    char *		td_pStackEnd;	/* the actual end of the stack */
    int			td_stackSize;	/* size of stack in bytes */
    int			td_stackCurrent;/* current stack usage in bytes */
    int			td_stackHigh;	/* maximum stack usage in bytes */
    int			td_stackMargin;	/* current stack margin in bytes */
    int			td_errorStatus;	/* most recent task error status */
    int			td_delay;	/* delay/timeout ticks */
    EVENTS_DESC		td_events;	/* VxWorks events information */
    char		td_name [VX_TASK_NAME_LENGTH+1];  /* name of task */
    RTP_ID              td_rtpId;         /* RTP owning the task */
    int                 td_excStackSize;  /* size of exception stack in bytes */
    char *              td_pExcStackBase; /* exception stack base */
    char *              td_pExcStackEnd;  /* exception stack end */
    char *              td_pExcStackStart;/* exception stack start */
    int                 td_excStackHigh;  /* exception stack max usage */
    int                 td_excStackMargin;/* exception stack margin */
    int			td_excStackCurrent;/* current exc stack usage (bytes) */
    } TASK_DESC;

/* function declarations */

extern int	taskSpawn 	(char * name, int priority, int options, 
			   	 int stackSize, FUNCPTR entryPt, int arg1,  
			   	 int arg2, int arg3, int arg4, int arg5, 
			   	 int arg6, int arg7, int arg8, int arg9, 
				 int arg10);
extern int	taskCreate 	(char * name, int priority, int options,
			         int stackSize, FUNCPTR entryPt, int arg1,  
			   	 int arg2, int arg3, int arg4, int arg5, 
			   	 int arg6, int arg7, int arg8, int arg9, 
				 int arg10);
extern STATUS	taskActivate 	(int tid);
extern STATUS	taskDelete 	(int tid);
extern STATUS	taskDeleteForce (int tid);
extern STATUS	taskSuspend 	(int tid);
extern STATUS	taskResume 	(int tid);
extern STATUS	taskRestart 	(int tid);
extern STATUS	taskPrioritySet (int tid, int newPriority);
extern STATUS	taskPriorityGet (int tid, int * pPriority);
extern char *	taskName 	(int tid);
extern int	taskNameToId 	(char * name);
extern STATUS	taskIdVerify 	(int tid);
extern int	taskIdSelf 	(void);
extern int	taskIdDefault 	(int tid);
extern BOOL	taskIsReady 	(int tid);
extern BOOL	taskIsSuspended (int tid);
extern BOOL	taskIsPended	(int tid);
extern STATUS	taskInfoGet 	(int tid, TASK_DESC * pTaskDesc);
extern STATUS	taskOptionsGet 	(int tid, int * pOptions);
extern STATUS	taskDelay 	(int ticks);
extern int	taskOpen 	(const char * name, int priority, int options, 
			   	 int mode, char * pStackBase, int stackSize, 
				 void * context, FUNCPTR entryPt, int arg1, 
				 int arg2, int arg3, int arg4, int arg5, 
				 int arg6, int arg7, int arg8, int arg9, 
				 int arg10);
extern STATUS 	taskClose	(int  tid);
extern STATUS 	taskUnlink	(const char * name);
extern STATUS	taskSafe	(void);
extern STATUS	taskUnsafe	(void);

#endif /* _ASMLANGUAGE */
 
#ifdef __cplusplus
}
#endif

#endif /* __INCtaskLibCommonh */
