/*
 * Please do not edit this file.
 * It was generated using rpcgen.
 */

/*
modification history
--------------------
01a,01jul04,dlk  Make this header include xdr_nfs2.h and xdr_nfs3.h.
                 Remove other contents.
*/

#ifndef nfs_protH
#define nfs_protH

#if 0
#error xdr_nfs.h is obsolete. Use xdr_nfs2.h or xdr_nfs3.h or xdr_nfs_common.h.
#endif

#include <xdr_nfs2.h>
#include <xdr_nfs3.h>

#endif /* nfs_protH */
