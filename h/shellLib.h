/* shellLib.h - header for the kernel shell module */

/* Copyright 2003-2005 Wind River Systems, Inc. */

/*
modification history
--------------------
02c,08feb05,bpn  Moved shellPromptFmtStrAdd() API to shellPromptLib.h.
02b,18aug04,bpn  Added CPLUS_SYM_MATCH configuration variable.
02a,28jul04,bpn  Added _WRS_DEPRECATED attributes to obsolete routines.
                 Moved APIs to shellLibP.h
01z,12jul04,bpn  Updated shellLogout() API.
01y,28apr04,bpn  Added shellBackgroundInit() and shellBackgroundRead() 
                 prototypes.
01x,26apr04,bpn  Added new configuration variable AUTOLOGOUT.
01w,07apr04,bpn  Added new configuration variables.
01v,26mar04,bpn  Added shellErrnoSet() and shellErrnoGet() prototypes.
01u,26feb04,bpn  Added shellCompatibleCheck (), shellIdVerify() and 
                 shellTaskGet() prototypes.
01t,25nov03,bpn  Added ALL_SHELL_SESSIONS and CURRENT_SHELL_SESSION definitions.
                 Added shellInOutBlock() and shellInOutUnblock() prototypes.
01s,06nov03,bpn  Update shellXxxxInit() API to use configuration string.
                 Added default configuration variable name
01r,28oct03,bpn  Moved shellDataXxx() API to shellDataLib.h.
01q,23sep03,bpn  Moved structures to shellInternalLib.h. Added prototype
                 for shellPromptFmtStrAdd(). Updated API names.
01p,22aug03,bpn  Added S_shellLib_MISSING_ARG, S_shellLib_UNKNOWN_OPT and 
                 S_shellLib_ARG_ERROR.
01o,22jul03,bpn  Added S_shellLib_UNMATCHED_QUOTE.
01n,26jun03,bpn  Added dummy functions shellRestart() and
                 shellIsRemoteConnectedSet() for rlogLib.
01m,19may03,bpn  Moved structure definition to shellInterpLib.h.
01l,17apr03,lcs  Add routines to configure shell environment.
01k,08apr03,bpn  Added shellCompatibleSet() declaration.
01j,20mar03,bpn  Added new errno value and shellLedModeSet() declaration.
01i,14mar03,bpn  Changed long long to INT64.
01h,13mar03,lcs  Integrate host shell host function call support.
01g,12mar03,lcs	 Added support for host shell.
01f,10mar03,bpn  Added interpreter switch mechanism. Changed SHELL_CONTEXT 
		 structure name to SHELL_CTX.
01e,06mar03,bpn  Added definition of shellExitWrapper().
01d,04mar03,bpn  Added definitions of shellInOutSet() and shellInOutGet().
01c,28feb03,bpn  Added shellxxxPrint() routines declarations. Reworked APIs.
01b,20feb03,bpn  Moved the SHELL_CONTEXT structure to shellLibP.h. Changed
		 SHELL_CTX_INTERP structure.
01a,17feb03,bpn  Written.
*/

#ifndef __INCshellLibh
#define __INCshellLibh

#ifdef __cplusplus
extern "C" {
#endif

/* Includes */
#ifndef HOST
#include <vwModNum.h>
#else
#include <host.h>
#include <shell.h>
#endif

#include <lstLib.h>

/* Defines */

/* Configuration variable names */

#define SHELL_CFG_LED_MODE	"LINE_EDIT_MODE"    /* for ledLib */
#define SHELL_CFG_INTERP	"INTERPRETER"	    /* current interpreter */
#define SHELL_CFG_INTERP_EVAL	"INTERPRETER_EVAL"
#define SHELL_CFG_LINE_LENGTH	"LINE_LENGTH"	    /* shell line length */
#define SHELL_CFG_EXC_PRINT	"EXC_PRINT"	    /* exception notification */
#define SHELL_CFG_BP_PRINT	"BP_PRINT"	    /* breakpoint notif. */
#define SHELL_CFG_AUTOLOGOUT	"AUTOLOGOUT"	    /* inactivity timeout */
#define SHELL_CFG_CPLUS_MATCH	"CPLUS_SYM_MATCH"   /* cplus symbol matching */

/* Special shell session identifier */

#define ALL_SHELL_SESSIONS	((SHELL_ID)-1)
#define CURRENT_SHELL_SESSION	((SHELL_ID)0)

/* Possible errnos */

#define S_shellLib_NO_INTERP		(M_shellLib | 1)
#define S_shellLib_INTERP_EXISTS	(M_shellLib | 2)
#define S_shellLib_NOT_SHELL_TASK	(M_shellLib | 3)
#define S_shellLib_SHELL_TASK_EXISTS	(M_shellLib | 4)
#define S_shellLib_SHELL_TASK_MAX	(M_shellLib | 5)
#define S_shellLib_NO_SHELL_CMD		(M_shellLib | 6)
#define S_shellLib_NO_USER_DATA		(M_shellLib | 7)
#define S_shellLib_NO_LED_MODE		(M_shellLib | 8)
#define S_shellLib_INTERP_INIT_ERROR	(M_shellLib | 9)

#define S_shellLib_LEX_ERROR		(M_shellLib | 20)
#define S_shellLib_SYNTAX_ERROR		(M_shellLib | 21)
#define S_shellLib_INTERNAL_ERROR	(M_shellLib | 22)

#define S_shellLib_UNMATCHED_QUOTE	(M_shellLib | 30)
#define S_shellLib_MISSING_ARG		(M_shellLib | 31)
#define S_shellLib_UNKNOWN_OPT		(M_shellLib | 32)
#define S_shellLib_ARG_ERROR		(M_shellLib | 33)

#define S_shellLib_CONFIG_ERROR		(M_shellLib | 40)

/* Structures */

typedef struct shell_ctx * SHELL_ID;

/* Function declarations */

/* Compatibility with vxWorks 5.5 */

#ifndef HOST
extern STATUS	shellInit (int stackSize, int interactive)
		_WRS_DEPRECATED("VxWorks 5.5 Compatibility");
extern void     shell (BOOL interactive)
		_WRS_DEPRECATED("VxWorks 5.5 Compatibility");
#endif
extern BOOL	shellLock (BOOL request);
extern STATUS	shellOrigStdSet (int which, int fd)
		_WRS_DEPRECATED("VxWorks 5.5 Compatibility");
extern STATUS	shellScriptAbort (void);
extern STATUS	shellHistory (int size);
extern STATUS	shellPromptSet (const char * newPrompt);

/* New shell functions */

extern STATUS	shellConsoleInit (const char * config, int stackSize,
				  const char * shellName, char ** pShellName);

extern STATUS	shellSecureInit (const char * config, int stackSize,
				 const char * shellName, char ** pShellName,
			         int fdin, int fdout, int fderr);

extern STATUS	shellGenericInit (const char * config, int stackSize,
				  const char * shellName, char ** pShellName,
				  BOOL interactive, BOOL loginAccess,
				  int fdin, int fdout, int fderr);

extern BOOL	shellCompatibleCheck (void);

extern STATUS	shellParserControl (UINT32 remoteEvent, UINT32 sessionId,
				    UINT32 slaveFd, VOIDFUNCPTR logoutFunc);

extern void	shellLoginInstall (FUNCPTR logRtn, int logVar);
extern void	shellLogoutInstall (FUNCPTR logRtn, int logVar);
extern STATUS	shellLogout (SHELL_ID shellId);
extern STATUS	shellRestart (SHELL_ID shellId);
extern STATUS	shellAbort (SHELL_ID shellId);
extern int	shellTaskIdDefault (SHELL_ID shellId, int taskId);
extern void	shellTerminate (SHELL_ID shellId);

extern void	shellExitWrapper (int code);

extern STATUS	shellIdVerify (SHELL_ID shellId);
extern int	shellTaskGet (SHELL_ID shellId);
extern SHELL_ID	shellFromTaskGet (int taskId);
extern SHELL_ID	shellFromNameGet (const char * taskName);
extern SHELL_ID	shellFirst ();
extern SHELL_ID	shellNext (SHELL_ID shellId);

extern void	shellErrnoSet (SHELL_ID shellId, int errNo);
extern int	shellErrnoGet (SHELL_ID shellId);

/* Private functions: do not use! */

extern void	shellCompatibleSet (BOOL isCompatible);
extern void	shellTaskDefaultOptionsSet (int taskOpt);
extern int	shellTaskDefaultOptionsGet ();
extern void	shellTaskDefaultStackSizeSet (int stackSize);
extern int	shellTaskDefaultStackSizeGet ();
extern void	shellTaskDefaultPrioritySet (int priority);
extern int	shellTaskDefaultPriorityGet ();
extern void	shellTaskDefaultBaseNameSet (const char * taskBaseName);
extern char *	shellTaskDefaultBaseNameGet ();


#ifdef __cplusplus
}
#endif

#endif /* __INCshellLibh */
