/* cpuPwrLib.h - CPU Power Management framework header file */

/*
 * Copyright (c) 2005 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01c,08sep05,mmi  added light power manager and set/get status API's
01b,27aug05,mmi  mods for not breaking other IA32
01a,05Aug05,mmi  created
*/

/*
* DESCRIPTION
* This header contains the datatype and prototype declarations for cpu
* power framework management
*/

#ifndef __INCcpuPwrLibh
#define __INCcpuPwrLibh


#ifdef __cplusplus
extern "C" {
#endif

#undef  DEBUG_CPU_PWR_MGMT   /* define this to turn on debugging */

/* Defines for CPU power framework events */

#define     CPU_PWR_EVENT_INT_ENTER     (0x00000001)     
#define     CPU_PWR_EVENT_INT_EXIT      (0x00000002)
#define     CPU_PWR_EVENT_IDLE_ENTER    (0x00000004)
#define     CPU_PWR_EVENT_IDLE_EXIT     (0x00000008)
#define     CPU_PWR_EVENT_TASK_SWITCH   (0x00000010)
#define     CPU_PWR_EVENT_CPU_UTIL      (0x00000020)
#define     CPU_PWR_EVENT_THRES_CROSS   (0x00000040)
#define     CPU_PWR_EVENT_PRIO_CHANGE   (0x00000080)


/*
* CPU power C-states in the power management framework. Its defined as follows
*/

typedef enum cpuPwrCState
    {
    cpuPwrC0State = 0,
    cpuPwrC1State = 1,
    cpuPwrC2State = 2,
    cpuPwrC3State = 3,
    cpuPwrC4State = 4,
    cpuPwrC5State = 5,
    cpuPwrCStateError = 6
} CPU_PWR_C_STATE;

/*
 * CPU power P-states in the power management framework. Its defined as follows
 */

typedef enum cpuPwrPState
    {
    cpuPwrCurrentTaskPState = 0,
    cpuPwrP0State = 1,
    cpuPwrP1State = 2,
    cpuPwrP2State = 3,
    cpuPwrP3State = 4,
    cpuPwrP4State = 5,
    cpuPwrP5State = 6,
    cpuPwrP6State = 7,
    cpuPwrPStateError = 8
} CPU_PWR_P_STATE;

/*
 * Thermal interrupt defines
 */

typedef enum cpuPwrThermIntType
    {
    cpuPwrThermIntHot = 0,
    cpuPwrThermIntCold = 1
    } CPU_PWR_THERM_INT_TYPE;

/*
 * Type of Period that may expire the CPU utilization power manager 
 */

typedef enum cpuPwrUtilPeriod 
    {
    cpuPwrPeriod1 = 0,
    cpuPwrPeriod2 = 1
    } CPU_PWR_UTIL_PERIOD;

typedef  struct cpuPwrEventHandlers
    {
    FUNCPTR     intEnterRtn;     /* CPU_PWR_EVENT_INT_ENTER */
    UINT        intEnterRtnArg;  /* Arg passed to intEnterRtn */
    FUNCPTR     intExitRtn;      /* CPU_PWR_EVENT_INT_EXIT */
    UINT        intExitRtnArg;   /* Arg passed to intExitRtn */
    FUNCPTR     idleEnterRtn;    /* CPU_PWR_EVENT_IDLE_ENTER */
    UINT        idleEnterRtnArg; /* Arg passed to idleEnterRtn */
    FUNCPTR     idleExitRtn;     /* CPU_PWR_EVENT_IDLE_EXIT */
    UINT        idleExitRtnArg;  /* Arg passed to idleExitRtn */
    FUNCPTR     taskSwitchRtn;   /* CPU_PWR_EVENT_TASK_SWITCH */
    FUNCPTR     cpuUtilRtn;      /* CPU_PWR_EVENT_CPU_UTIL */
                                 /* event handler */
    FUNCPTR     threshCrossRtn;  /* CPU_PWR_EVENT_THRESH_CROSS */
                                 /* event handler */
    FUNCPTR     priChangeRtn;    /* CPU_PWR_EVENT_PRIORITY_CHANGE */       
    } CPU_PWR_EVENT_HANDLERS, * CPU_PWR_EVENT_HANDLERS_PTR ;

/* Function prototypes */

extern STATUS          cpuPwrEventHandlersSet (CPU_PWR_EVENT_HANDLERS_PTR);
extern STATUS          cpuPwrCStateSet (CPU_PWR_C_STATE);
extern STATUS          cpuPwrPStateSet (CPU_PWR_P_STATE);
extern CPU_PWR_P_STATE cpuPwrPStateGet (void );
extern STATUS          cpuPwrTaskPStateSet (int, CPU_PWR_P_STATE);
extern CPU_PWR_P_STATE cpuPwrTaskPStateGet (int );
extern STATUS          cpuPwrTempThreshSet (int, int);
extern STATUS          cpuPwrTempThreshGet (int *, int * );
extern STATUS          cpuPwrUtilPeriodSet (UINT, UINT );
extern BOOL            cpuPwrSrcCheck (void);
extern BOOL            cpuPwrCheckHotRtn (void);
extern void            cpuPwrThermIntConnect (VOIDFUNCPTR *);

#ifdef __cplusplus
}
#endif

#endif  /* __INCcpuPwrLibh */
