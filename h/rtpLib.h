/* rtpLib.h - real time process library header file */

/* Copyright 2003-2004 Wind River Systems, Inc. */

/*
modification history
--------------------
01r,12oct04,kk   updated kernelBaseLib.h to private/kernelBaseLibP.h,
		 added rtpShow prototype
01q,22sep04,ans  moved rtpLibInit() rtpShowInit() rtpShellCmdInit() to
                 rtpLibP.h
01p,10aug04,kk   renamed rtpBaseLib.h to kernelBaseLib.h
01p,02sep04,yvp  Remove const from rtpLibInit params.
01o,27apr04,ans  Added RTP_DEL_FORCE option
01n,15apr04,kk   rename rtpSelfIdGet to rtpIdSelf(), added include of 
                 rtpBaseLib.h
01m,06apr04,job  Extended rtpLibInit() prototype.
01l,18feb04,ans  Added rtpSignalQueueSz parameter to rtpLibInit ().
01k,17nov03,yvp  Added RTP hook support.
01j,30oct03,nrj  added support for waitpid
01j,22sep03,bpn  Cleaned up. Added rtpShellCmdInit() prototype.
01i,22sep03,kk   remove rtpSpawn, moved to rtpLibCommon.h
01h,15sep03,kk   move common definitions to rtpLibCommon.h,
		 added options arg to rtpDelete(), 
		 added RTP_DEL_VIA_TASK_DELETE option
01g,26aug03,nrj  reorganized and moved to private hdr
01f,18aug03,job  Added rtpShowInit() prototype.
01e,22jul03,pad  Moved _exit() declaration to taskLibP.h for decoupling
                 reasons.
01d,07jul03,nrj  added rtpDelete
01c,07jul03,nrj  removing RTP_ID from here
01b,07jul03,nrj  changed create API
01a,26jun03,nrj  written.
*/

#ifndef __INCrtpLibh
#define __INCrtpLibh

#ifdef __cplusplus
extern "C" {
#endif

/* includes */

#include "vxWorks.h"
#include "dllLib.h"
#include "vwModNum.h"
#include "rtpLibCommon.h"
#include "private/kernelBaseLibP.h"	/* for kernelId */

/* defines */

#define RTP_DEL_VIA_TASK_DELETE 0x1 	/* rtpDelete() via taskDestroy() */
#define RTP_DEL_FORCE           0x2     /* Forceful  rtpDelete()  */

#ifndef	_ASMLANGUAGE

/* function declarations */

extern STATUS 	rtpDelete (RTP_ID id, int options, int status);
extern BOOL	rtpShow (char * rtpNameOrId, int level);

#endif	/* _ASMLANGUAGE */

#ifdef __cplusplus
}
#endif

#endif /* __INCrtpLibh */
