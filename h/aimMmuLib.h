/* aimMmuLib.h - mmu library portion of the Architecture Independent Manager */

/* Copyright 1984-2004 Wind River Systems, Inc.  */

/*
modification history
--------------------
01o,29nov04,dtr  Add region tbl ptr to MMU_TRANS_TBL.
01n,23nov04,dtr  Mod to API for 860 support.
01m,29sep04,dtr  Add Show routines externs.
01l,17sep04,sru  Added 'pWritable' parameter to LOCKIMPORTFUNC.
01k,30aug04,dtr  Modify allocation buffer for static TLB entries.
01j,06aug04,sru  Added support for importing locked regions at startup.
01i,08jun04,sru  Moved attribute definitions to mmuAimBaseLib.h.
01h,19may04,dtr  Add extra function for ISR callable state.
01g,08apr04,dtr  Adding in new feature support aimMmuLock/Unlock/optimization.
01f,04may04,sru  add support for query of TLB size
01e,12nov03,jtp  Change mmuPteSet global parameter type to BOOL
01d,07nov03,sru  change model for bufferwrite
01c,04nov03,jtp  change API for fine-grain PTE get
01b,30oct03,sru  add AD-MMU hook for TransTblGet
01a,07apr03,sru  written

*/

#ifndef __INCaimMmuLibh
#define __INCaimMmuLibh


#ifdef __cplusplus
extern "C" {
#endif

#include "vxWorks.h"
#include "aimMmuBaseLib.h"

/* options for various AIM calls */

#define AIM_MMU_OPT_LOCK_NONE		0
#define AIM_MMU_OPT_LOCK_IMPORT		1

/* masks for page sizes */

#define MMU_PAGE_MASK_1K	  0x0000001
#define MMU_PAGE_MASK_2K	  0x0000002
#define MMU_PAGE_MASK_4K	  0x0000004
#define MMU_PAGE_MASK_8K	  0x0000008
#define MMU_PAGE_MASK_16K	  0x0000010
#define MMU_PAGE_MASK_32K	  0x0000020
#define MMU_PAGE_MASK_64K	  0x0000040
#define MMU_PAGE_MASK_128K	  0x0000080
#define MMU_PAGE_MASK_256K	  0x0000100
#define MMU_PAGE_MASK_512K	  0x0000200
#define MMU_PAGE_MASK_1M	  0x0000400
#define MMU_PAGE_MASK_2M	  0x0000800
#define MMU_PAGE_MASK_4M	  0x0001000
#define MMU_PAGE_MASK_8M	  0x0002000
#define MMU_PAGE_MASK_16M	  0x0004000
#define MMU_PAGE_MASK_32M	  0x0008000
#define MMU_PAGE_MASK_64M	  0x0010000
#define MMU_PAGE_MASK_128M	  0x0020000
#define MMU_PAGE_MASK_256M	  0x0040000
#define MMU_PAGE_MASK_512M	  0x0080000
#define MMU_PAGE_MASK_1G	  0x0100000


#define MMU_PAGE_SIZE_MIN	1024
#define MMU_PAGE_MASK_MIN	MMU_PAGE_MASK_1K
#define BIT_SHIFT_1K            10
#define MAX_BITS                31
typedef char MMU_PAGE_TBL;		/* placeholder */
typedef void *MMU_REGION_TBL;
typedef MMU_REGION_TBL *MMU_CONTEXT_TBL;
typedef int MMU_CONTEXT;

typedef struct mmuTransTblStruct
  {
  MMU_CONTEXT context;
  MMU_REGION_TBL *regionTblPtr;
  } MMU_TRANS_TBL;

/* PTE_ATTR - a structured used to read/write PTE attribute info. */

typedef struct mmu_pte_attr
  { 
  PHYS_ADDR physAddr;
  VIRT_ADDR virtAddr;
  UINT pageSize;
  BOOL global;
  UINT context;
  UINT state;
  BOOL valid;				/* AIM-readable valid state */
  BOOL isrCallable;
  } PTE_ATTR;

typedef struct PTE_ATTR MMU_PTE_ATTR;

/* MMU_PTE_ATTR mask bits.  Used to control PTE set/get functions */

#define PTE_ATTR_PHYS_ADDR	0x01
#define PTE_ATTR_PAGE_SIZE	0x02
#define PTE_ATTR_GLOBAL		0x04
#define PTE_ATTR_CONTEXT	0x08
#define PTE_ATTR_STATE		0x10
#define PTE_ATTR_VIRT_ADDR	0x20
#define PTE_ATTR_VALID		0x40	/* AIM-readable valid state */
#define PTE_ATTR_ISR_CALLABLE   0x80


extern STATUS		aimMmuLibInit ();

typedef struct
    {
    VIRT_ADDR	virtAddr;
    int      	pageSize;
    BOOL	imported;
    } STATIC_ENTRY_REGION;


typedef void      (*TLBINVALLDYNFUNC)(void);
typedef void      (*TLBINVFUNC)(int context, VIRT_ADDR);
typedef STATUS    (*ENABLEFUNC)(BOOL);
typedef void      (*PTESETFUNC)(void *pPte, 
				PHYS_ADDR, VIRT_ADDR, 
				UINT, BOOL, UINT, void *pRte);
typedef void      (*PTEINITFUNC)(void *pPte);
typedef UINT      (*PTESTATEGETFUNC)(void *pPte,void *pRte);
typedef void      (*CURRENTSETFUNC)(MMU_CONTEXT_TBL *, int context);
typedef UINT      (*INTPTEATTRFUNC)(void *pPte);
typedef BOOL      (*BOOLPTEATTRFUNC)(void *pPte);
typedef PHYS_ADDR (*PTEPHYSATTRFUNC)(void *pPte);
typedef UINT      (*INTSTATEATTRFUNC)(UINT state);
typedef BOOL      (*BOOLSTATEATTRFUNC)(UINT state);
typedef VIRT_ADDR (*ADDRXLATEFUNC)(VIRT_ADDR);
typedef UINT *	  (*TRANSTBLGETFUNC)(MMU_CONTEXT_TBL *, int);
typedef void	  (*PAGEWRITEFUNC)(int context,
				   void *pPteSrc, VIRT_ADDR virtAddrSrc, 
				   void *pPteDst, VIRT_ADDR virtAddrDst,
				   UINT numBytes);
typedef STATUS    (*LOCKPTEUPDATEFUNC)(MMU_CONTEXT,
				       void *pPte,
				       VIRT_ADDR,
				       UINT size,
				       BOOL lock);

typedef STATUS    (*LOCKFUNC)(void *pPte,VIRT_ADDR,UINT);
typedef STATUS    (*UNLOCKFUNC)(VIRT_ADDR);

typedef STATUS    (*SETLOCKFUNC)(void *pPte,UINT,BOOL,BOOL);

typedef STATUS    (*MODIFYLOCALPTEFUNC)(void *pPte,void *pLocalPte,
					UINT,PHYS_ADDR,BOOL,BOOL);

typedef UINT      (*PTEMAXPHYSBITSFUNC)(void);
typedef UINT      (*CURRENTGETFUNC)(void);
typedef BOOL      (*PTEISRATTRFUNC)(void *pPte);
typedef STATUS    (*LOCKIMPORTFUNC)(int index, VIRT_ADDR *pVirtAddr, 
				    UINT32 * pSize, BOOL * pWritable);

/*
 * MMU_ARCH_LIB_FUNCS is a data structure that is allocated by the AIM
 * library.  It is filled in with function pointers that are exported 
 * by the architecture-dependent MMU library.  The AIM uses these 
 * function pointers to implement its MMU services.
 */

typedef struct
    {
    TLBINVALLDYNFUNC	mmuTlbInvalidateAllDynamic;
    TLBINVFUNC		mmuTlbInvalidate;
    ADDRXLATEFUNC	kmemPtrToPageTablePtr; /*Optional*/
    ADDRXLATEFUNC	pageTablePtrToKmemPtr; /*Optional*/
    ENABLEFUNC		mmuEnable;
    PTESETFUNC		mmuPteSet;
    PTEINITFUNC		mmuPteInit;
    PTESTATEGETFUNC     mmuPteStateGet;
    BOOLPTEATTRFUNC     mmuPteValidGet;
    BOOLPTEATTRFUNC     mmuPteGlobalGet;
    INTPTEATTRFUNC      mmuPtePageSizeGet;
    PTEPHYSATTRFUNC     mmuPtePhysAddrGet;
    BOOLSTATEATTRFUNC   mmuStateValidGet;
    INTSTATEATTRFUNC    mmuStateCacheModeGet;
    BOOLSTATEATTRFUNC   mmuStateWriteEnabledGet;
    CURRENTSETFUNC	mmuCurrentSet;
    CURRENTGETFUNC	mmuCurrentGet;
    PAGEWRITEFUNC	mmuPageWrite;
    FUNCPTR		mmuAttrTranslate;
    TRANSTBLGETFUNC     mmuTransTblGet;
    LOCKPTEUPDATEFUNC   mmuLockPteUpdate;    /*Optional vmPageOptimize*/
    LOCKFUNC            mmuLock;             /*Optional vmPageLock*/
    UNLOCKFUNC          mmuUnlock;           /*Optional vmPageUnLock*/
    BOOLPTEATTRFUNC     mmuPteLockGet;       /*Optional */
    SETLOCKFUNC         mmuPteSetLockState;  /*Optional vmLock/Unlock*/
    MODIFYLOCALPTEFUNC  mmuPteSetForDynamic; /*Optional vmPageOptimize*/
    PTEMAXPHYSBITSFUNC  mmuPteMaxPhysBitsGet;/*Optional */
    PTEISRATTRFUNC      mmuPteIsrCallableGet;/*Optional vmPageOptimize*/
    LOCKIMPORTFUNC      mmuLockImport;	     /*Optional */
    
    } MMU_ARCH_LIB_FUNCS; 


extern STATUS		aimMmuLibInit ();
extern STATUS		aimMmuShowInit();
extern void		aimMmuMapShow(UINT);
extern void		aimMmuOptimizeMemShow(int);

#ifdef __cplusplus
}
#endif

#endif /* __INCaimMmuLibh */

