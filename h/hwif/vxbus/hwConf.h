/* hwconf.h - hardware configuration resources */

/* Copyright (c) 2005  Wind River Systems, Inc. All rights are reserved. */

/* 
modification history
--------------------
01a,10May05,tor  created
*/

#ifndef INC_hwconfH
#define INC_hwconfH

#ifdef __cplusplus
extern "C" {
#endif


/* includes */

#include <vxWorks.h>

/* defines */

/* typedefs */

typedef const struct hcfResource    HCF_RESOURCE;
typedef const struct hcfDevice  HCF_DEVICE;

struct hcfResource
    {
    char *  name;
    UINT32  type;
    union
        {
        void *  addr;
        char *  string;
        UINT32  integer;
        } u;
    };

struct hcfDevice
    {
    char *      devName;
    int         devUnit;
    int         busType;
    int         busIndex;
    int         count;
    const struct hcfResource * pResource;
    };

/* globals */

IMPORT const VOIDFUNCPTR hcfDriverList[];
IMPORT const int hcfDriverNum;

IMPORT const struct hcfDevice hcfDeviceList[];
IMPORT const int hcfDeviceNum;

/* locals */

/* forward declarations */

STATUS devResourceGet
    (
    const struct hcfDevice *    pDevice,
    char *                      name,
    int             type,
    void **                     pDest
    );

#ifdef __cplusplus
}
#endif

#endif /* INC_hwconfH */
