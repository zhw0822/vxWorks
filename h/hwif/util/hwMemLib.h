/* hwMemLib.h -- hardware memory allocation library header file */

/* Copyright (c) 2005 Wind River Systems, Inc. */

/* 
Modification history
--------------------
01a,31Jan05,tor  created
*/

#ifndef INC_HWMEMLIB_H
#define INC_HWMEMLIB_H

#ifdef __cplusplus
extern "C" {
#endif

/* includes */

/* defines */

#define HWMEM_MAX_ALLOC	4096

#define VMR_MINSIZE	(1<<2)		/* minimum size to allocate */

/* typedefs */

/* globals */

IMPORT char hwMemPool[];

/* locals */

/* forward declarations */

void hwMemLibInit();
STATUS hwMemPoolCreate
    (
    char *	pMem,
    int		size
    );
char * hwMemAlloc(int reqSize);
STATUS hwMemFree(char * pMem);
void * hwVMRInit(char * pSpace, int size);
void * hwVMRAlloc(void * pVMRCookie, int alignShift);
char * hwVMRAddrGet(void * pVMRCookie);
STATUS hwVMRDeActivate(void * pVMRCookie);
void hwMemInit();

#ifdef __cplusplus
}
#endif

#endif /* INC_HWMEMLIB_H */
