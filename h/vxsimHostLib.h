/* vxsimHostLib.h - Host/VxSim interface header */

/* Copyright 2003-2005 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*
modification history
--------------------
01y,11jan05,jmp  added vxsimHostWdbPipe* routines.
01x,03may04,jeg  moved the private APIs to vxsimHostLibP.h file.
01w,14apr04,elp  added vxsimHostSimnetParamGet/Set().
01v,09apr04,jeg  removed vxsimBufLib.h header included
01u,08mar04,jmp  updated vxsimHostBufInit() & vxsimHostBufDelete() prototypes.
		 added missing passfs prototypes.
01t,05mar04,dbt  Removed simip references.
01s,25nov03,jmp  added vxsimHostVirtualMemInfoGet().
01r,13oct03,jeg  added vxsimHostOpendir () prototype. Renamed passFs specific 
		 routines.
01q,07oct03,elp  removed vxsimNetArchLib.h, added SIMNET host driver interfaces.
01p,03oct03,dbt  Added vxsimHostSimipEnetAddrGet() interface.
01o,01oct03,jmp  added MMU routines.
01n,30sep03,elp  added vxsimNetArchLib.h.
01m,26sep03,dbt  Added SIMIP host driver interfaces.
01l,05sep03,jmp  added vxsimHostDllLoad().
01k,07aug03,jeg  added vxsimHostShMemInit () and vxsimHostBusIntGen ()
		 prototypes.
01j,31jul03,jeg  added non-volatile RAM routines declarations
01i,31jul03,jmp  added VxSim buffer library support.
01h,30jul03,jeg  added vxsimHostFtruncate () prototype.
01g,19jun03,jmp  added prototype for vxsimHostDbg().
01f,17jun03,dbt  Added vxsimHostTimeGet() & vxsimHostStat() prototypes.
01e,15may03,jmp  added vxsimHostCurrentPathGet().
01d,30apr03,dbt  Added timestamp timer prototypes.
01c,04apr03,dbt  Removed vxsimHostIntLock().
01b,04apr03,jeg  add common host interface prototypes
01a,30jan03,jmp  written.
*/

#ifndef __INCvxsimHostLibh
#define __INCvxsimHostLibh

/* includes */

#if (CPU == SIMNT)
#include "arch/simnt/vxsimHostArchLib.h"
#include "arch/simnt/mmuArchLib.h"
#elif (CPU == SIMSPARCSOLARIS)
#include "arch/simsolaris/vxsimHostArchLib.h"
#include "arch/simsolaris/mmuArchLib.h"
#elif (CPU == SIMLINUX)
#include "arch/simlinux/vxsimHostArchLib.h"
#include "arch/simlinux/mmuArchLib.h"
#endif
#include "drv/end/simnetEnd.h"
#include "utime.h"
#include "private/vxsimHostLibP.h"

/* common host side defined routines */

extern FUNCPTR  vxsimHostProcAddrGet 	(char * routine);
extern STATUS	vxsimHostDllLoad	(char * dllName);
extern STATUS   vxsimHostWdbPipeInit    (void * inputBuffer,
					 int * pHostTargetFd,
					 int * pTargetHostFd,
					 int * pIntVecNum,
					 int pipePktMtu);
extern void	vxsimHostWdbPipeModeSet	(int pipeFd, int newMode);
extern void	vxsimHostWdbPipeFlush	(int pipeFd);
extern STATUS	vxsimHostWdbPipeRead	(int pipeFd, void * inputBuffer,
					int * pSize);
extern STATUS	vxsimHostWdbPipeWrite	(int pipeFd, void * buffer, int len);
extern STATUS	vxsimHostWdbPipePoll	(void);
extern void	vxsimHostWdbPipePktDrop	(void);
extern void	vxsimHostSleep		(UINT32 duration);

#endif	/* __INCvxsimHostLibh */
