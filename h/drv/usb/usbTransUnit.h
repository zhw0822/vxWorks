/* usbTransUnit.h - Translation Unit definitions */

/* Copyright 2003 - 2005 Wind River Systems, Inc. */

/*
modification history
--------------------
01a,06jan05,ami  added irpMutext in USBTU_PIPE_INFO structure. (SPR #102674)
*/

/*
DESCRIPTION
This file defines constants and structures related to the Translation
Unit.
*/

#ifndef __INCusbTransUnith
#define __INCusbTransUnith

#ifdef	__cplusplus
extern "C" {
#endif


/* includes */


#include <usb/usbPlatform.h>
#include <usb/usb.h>               /* general USB definitions */
#include <usb/usbLib.h>            /* USB utility functions */
#include <usb/usbdLib.h>           /* USBD interface */
#include <usb/ossLib.h>            /* os specific services */
#include <usb/usbQueueLib.h>
#include <usb/usbListLib.h>

#include <msgQLib.h>
#include <taskLib.h>
#include <semLib.h>
#include <fioLib.h>
#include <string.h>
#include <wvLib.h>
    
#include <usb2/usbHst.h>

/* defines */

/* String length definition */

#define USBTU_NAME_LEN		32	/* Maximum length for name */

extern void usbLogMsg (char *,int,int,int,int,int,int);

#ifdef DEBUG_DETAIL
#define USBTU_LOG(str)        \
     usbLogMsg ( "%s (%d): %s \n ",(INT32)__FILE__, __LINE__, (int) str,0,0,0)
#else
#define USBTU_LOG(str)
#endif


/* Wind View Event ID's */

#define USBTU_WV_CLIENT_INIT           23000   /* Client Initialization event ID */
#define USBTU_WV_CLIENT_TRANSFER       23001   /* Client Data Transfer event ID */
#define USBTU_WV_CLIENT_STD            23002   /* Client Standard Requests event ID */
#define USBTU_WV_USBD_INIT             23003   /* USBD Initialization/Shutdown event ID */

/* Wind View Event Filter */

#define USBTU_WV_FILTER       0x00000001

/* Wind View Event Log Message Size */
 
#define USBTU_WV_LOGSIZE      80


/* Typedefs */

/*
 *  USBTU_EVENTCODE
 */

typedef enum usbtu_eventcode
    {
    ADDDEVICE,            /* device attachment event code */
    REMOVEDEVICE,         /* device removal event code */
    SUSPENDDEVICE,        /* device suspend event code */
    RESUMEDEVICE,         /* device resume event code */
    IRPCOMPLETE           /* IRP completion event code */
    } USBTU_EVENTCODE;

/*
 * USBTU_DEVICE_DRIVER
 */

typedef struct usbtu_device_driver
    {
    UINT16 class;                         /* device class code */
    UINT16 subclass;                      /* device subclass code */
    UINT16 protocol;                      /* device protocol code */
    INT8  clientName[USBTU_NAME_LEN];    /* client name          */
    USBD_ATTACH_CALLBACK  attachCallback;  /* attach/detach callback */
    USBD_MNGMT_CALLBACK   mngmtCallback;   /* managemnt callback */
    pVOID                 mngmtCallbackParam; /* management callback param */
    MSG_Q_ID              msgQid;             /* message queue id for client */
    MSG_Q_ID              msgQidIrpComplete;   /* message queue id for IrpComplete */ 
    THREAD_HANDLE         threadHandle;       /* thread handle  */
    THREAD_HANDLE         irpThreadHandle;       /* thread handle  */ 
    pUSBHST_DEVICE_DRIVER pDriverData;        /* pointer to driver data */
    LINK                  tuDriverLink;       /* pointer to next in list */
    } USBTU_DEVICE_DRIVER, *pUSBTU_DEVICE_DRIVER;

/*
 * USBTU_NODE_INFO
 */

typedef struct usbtu_node_info
    {
    UINT32        hDevice;       /* handle to the device */
    UINT16        configuration; /* device configuration */
    UINT16        interface;     /* device interface */
    MSG_Q_ID      msgQid;        /* message queue id of client */
    LINK          devLink;       /* link */
    } USBTU_NODE_INFO, *pUSBTU_NODE_INFO;

/*
 * USBTU_IRP
 */

typedef struct usbtu_irp
    {
      pUSB_IRP pIrp; 
      struct usbtu_irp* nextIrp;
      struct usbtu_irp* prevIrp;
    } USBTU_IRP, * pUSBTU_IRP;

/*
 * USBTU_PIPE_INFO
 */

typedef struct usbtu_pipe_info
    {
    UINT32         hDevice;           /* handle to the device */
    UINT8          endpointAddress;   /* endpointaddress of pipe */
    UINT16         transferType;      /* transfer type on pipe */
    pUSBTU_IRP     irpList;          /* list of irp pending on pipe */
    UINT8          markedForDeletion; /* indicates if pipe is marked for deletion */
    UINT32	   uMaxPacketSize;    /* Maximum packet size for the endpoint */
    SEM_ID         pipeDelete;
    UINT32         bandwidth;
    MUTEX_HANDLE   irpMutex;	      /* for synchronisation */	
    } USBTU_PIPE_INFO, *pUSBTU_PIPE_INFO;

/*
 * USBTU_IRPCONTEXT
 */

typedef struct usbtu_irpcontext
    {
    UINT32           urbIndex;           /* index into array urbPtr */
    pUSBHST_URB      urbPtr;             /* array of USBHST_URB's   */
    UINT32           urbCompleteCount;   /* URB completion count    */
    MSG_Q_ID         msgQid;             /* message queue id of client */
    pUSBTU_PIPE_INFO pipeInfo;           /* pte to pipe info structure */
    } USBTU_IRPCONTEXT, *pUSBTU_IRPCONTEXT;

/*
 * USBTU_CLIENTMSG
 */

typedef struct usbtu_clientmsg
    {
    USBTU_EVENTCODE    eventCode;       /* event code */
    pUSB_IRP           pIrp;            /* pointer to IRP structure */
    UINT32             hDevice;         /* handle to device */
    UINT16             interface;       /* device interface */
    }USBTU_CLIENTMSG, *pUSBTU_CLIENTMSG;

/*
 * USBTU_TUMSG
 */

typedef struct usbtu_tumsg
    {
    USBTU_EVENTCODE     eventCode;      /* event code */
    UINT32              hDevice;        /* handle to device */
    UINT16              interface;      /* device interface */
    pVOID*              ppDriverData;   /* pointer to driver data */
    } USBTU_TUMSG, *pUSBTU_TUMSG;


/* global variable declarations */

extern UINT32 usbtuInitWvFilter;
extern LIST_HEAD     usbtuClientList;   /* list of clients registered with USBD */
extern LIST_HEAD     usbtuDevList;      /* list of devices attached to USB */
extern THREAD_HANDLE usbtuThreadId;     /* thread id of Translation Unit thread */
extern MSG_Q_ID      usbtuMsgQid;       /* message queue id of Translation Unit */
extern MUTEX_HANDLE  usbtuMutex;        /* mutex of Translation Unit */


#ifdef	__cplusplus
}
#endif

#endif	/* __INCusbTransUnith */


/* End of file. */


