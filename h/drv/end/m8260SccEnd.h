/* m8260SccEnd.h - Motorola MPC8260 Serial Communication Controllers (SCC) Ethernet interface header */

/* Copyright 1996-2001 Wind River Systems, Inc. */

/*
modification history
--------------------
01d,16jan03,gjc  SPR#85071 and SPR#85093 :Added define to support DUPLEX.
01c,17may02,gjc  #SPR 75922, undef SCC_BUF if prev defined.
01b,09may02,gjc  Fixing SPR #75046 by setting SCC to Full Duplex Mode.
01a,10may01,g_h  adapted from sbcM8260CpmEnd.h (ver. 01b).
*/

/*
This file contains definitions to support the end-style Network Driver 
for the Motorola CPM core Ethernet controller used in the M68EN360 and 
PPC800-series communications controllers. These definitions are compiler
dependant, meaning that their values are based on the particular cpu
being used.
*/

#ifndef __INCm8260SccEndh
#define __INCm8260SccEndh

#ifdef   __cplusplus
extern "C" {
#endif

#include "drv/sio/m8260Cp.h"
#include "drv/sio/m8260CpmMux.h"
#include "drv/mem/m8260Siu.h"

/* defines */

/*
 * the following may be redefined in the bsp to account for the actual
 * number of SCC devices in the system.
 */

#ifndef MAX_SCC_CHANNELS
#define MAX_SCC_CHANNELS  4       /* max SCC number for Ethernet channels */
#endif /* MAX_SCC_CHANNELS */

/* MPC8260 Dual Ported Ram addresses */

#define PPC8260_DPR_SCC1(dprbase)     ((VINT32 *) (dprbase + 0x8000))
#define PPC8260_DPR_SCC2(dprbase)     ((VINT32 *) (dprbase + 0x8100))
#define PPC8260_DPR_SCC3(dprbase)     ((VINT32 *) (dprbase + 0x8200))
#define PPC8260_DPR_SCC4(dprbase)     ((VINT32 *) (dprbase + 0x8300))

    /* SCC 1 register set */

#define GSMR_L1(base)   (CAST(VUINT32 *)(base + 0x11A00)) /* SCC1 General Mode*/
#define GSMR_H1(base)   (CAST(VUINT32 *)(base + 0x11A04)) /* SCC1 General Mode*/
#define PSMR1(base)     (CAST(VUINT16 *)(base + 0x11A08)) /* SCC1 Proto. Spec */
/* 11A0A RESERVED */
#define TODR1(base)     (CAST(VUINT16 *)(base + 0x11A0C)) /* SCC1 Tx On Demand*/
#define DSR1(base)      (CAST(VUINT16 *)(base + 0x11A0E)) /* SCC1 Data Sync */
#define SCCE1(base)     (CAST(VUINT16 *)(base + 0x11A10)) /* SCC1 Event Reg */
#define SCCM1(base)     (CAST(VUINT16 *)(base + 0x11A14)) /* SCC1 Mask Reg */
#define SCCS1(base)     (CAST(VUINT8  *)(base + 0x11A17)) /* SCC1 Status Reg */
/* 11A18-11A1F RESERVED */

/* SCC 2 register set */

#define GSMR_L2(base)   (CAST(VUINT32 *)(base + 0x11A20)) /* SCC2 General Mode*/
#define GSMR_H2(base)   (CAST(VUINT32 *)(base + 0x11A24)) /* SCC2 General Mode*/
#define PSMR2(base)     (CAST(VUINT16 *)(base + 0x11A28)) /* SCC2 Proto. Spec */
/* 11A2A RESERVED */
#define TODR2(base)     (CAST(VUINT16 *)(base + 0x11A2C)) /* SCC2 Tx On Demand*/
#define DSR2(base)      (CAST(VUINT16 *)(base + 0x11A2E)) /* SCC2 Data Sync */
#define SCCE2(base)     (CAST(VUINT16 *)(base + 0x11A30)) /* SCC2 Event Reg */
#define SCCM2(base)     (CAST(VUINT16 *)(base + 0x11A34)) /* SCC2 Mask Reg */
#define SCCS2(base)     (CAST(VUINT8  *)(base + 0x11A37)) /* SCC2 Status Reg */
/* 11A38-11A3F RESERVED */

/* SCC 3 register set */

#define GSMR_L3(base)   (CAST(VUINT32 *)(base + 0x11A40)) /* SCC3 General Mode*/
#define GSMR_H3(base)   (CAST(VUINT32 *)(base + 0x11A44)) /* SCC3 General Mode*/
#define PSMR3(base)     (CAST(VUINT16 *)(base + 0x11A48)) /* SCC3 Proto. Spec */
/* 11A4A RESERVED */
#define TODR3(base)     (CAST(VUINT16 *)(base + 0x11A4C)) /* SCC3 Tx On Demand*/
#define DSR3(base)      (CAST(VUINT16 *)(base + 0x11A4E)) /* SCC3 Data Sync */
#define SCCE3(base)     (CAST(VUINT16 *)(base + 0x11A50)) /* SCC3 Event Reg */
#define SCCM3(base)     (CAST(VUINT16 *)(base + 0x11A54)) /* SCC3 Mask Reg */
#define SCCS3(base)     (CAST(VUINT8  *)(base + 0x11A57)) /* SCC3 Status Reg */
/* 11A58-11A5F RESERVED */

/* SCC 4 register set */

#define GSMR_L4(base)   (CAST(VUINT32 *)(base + 0x11A60)) /* SCC4 General Mode*/
#define GSMR_H4(base)   (CAST(VUINT32 *)(base + 0x11A64)) /* SCC4 General Mode*/
#define PSMR4(base)     (CAST(VUINT16 *)(base + 0x11A68)) /* SCC4 Proto. Spec */
/* 11A6A RESERVED */
#define TODR4(base)     (CAST(VUINT16 *)(base + 0x11A6C)) /* SCC4 Tx On Demand*/
#define DSR4(base)      (CAST(VUINT16 *)(base + 0x11A6E)) /* SCC4 Data Sync */
#define SCCE4(base)     (CAST(VUINT16 *)(base + 0x11A70)) /* SCC4 Event Reg */
#define SCCM4(base)     (CAST(VUINT16 *)(base + 0x11A74)) /* SCC2 Mask Reg */
#define SCCS4(base)     (CAST(VUINT8  *)(base + 0x11A77)) /* SCC4 Status Reg */
/* 11A78-11A7F RESERVED */


#define SCC_USR_RUNNING_FROM_ROM    0x00000001  /* inhibit zcopy mode */
#define SCC_USR_DUPLEX_HALF         0x00000000  /* Half duplex */
#define SCC_USR_DUPLEX_FULL         0x00000002  /* Full duplex */

#define CPM_DPR_SCC1(baseAddr)  PPC8260_DPR_SCC1 (baseAddr)
#define CPM_GSMR_L1(baseAddr)   GSMR_L1 (baseAddr)
#define END_OBJ_STRING  "MPC8260 PowerQUICC II Enhanced Network Driver"
#define MOT_DEV_NAME    "motscc"
#define MOT_DEV_NAME_LEN 7


/* bsp-specific routine to include */

#define SYS_ENET_ADDR_GET(address)                                      \
if (sysSccEnetAddrGet != NULL)                                          \
    if (sysSccEnetAddrGet (pDrvCtrl->unit, (address)) == ERROR)         \
        {                                                               \
        errnoSet (S_iosLib_INVALID_ETHERNET_ADDRESS);                   \
        return (NULL);                                                  \
        }
 
#define SYS_ENET_ENABLE                                                 \
if (sysSccEnetEnable != NULL)                                           \
    if (sysSccEnetEnable (pDrvCtrl->unit) == ERROR)                     \
        return (ERROR);
 
#define SYS_ENET_DISABLE                                                \
if (sysSccEnetDisable != NULL)                                          \
    sysSccEnetDisable (pDrvCtrl->unit);

/* General SCC Mode Register definitions */

#define SCC_GSMRL_HDLC          0x00000000      /* HDLC mode */
#define SCC_GSMRL_APPLETALK     0x00000002      /* AppleTalk mode (LocalTalk) */
#define SCC_GSMRL_SS7           0x00000003      /* SS7 mode (microcode) */
#define SCC_GSMRL_UART          0x00000004      /* UART mode */
#define SCC_GSMRL_PROFI_BUS     0x00000005      /* Profi-Bus mode (microcode) */
#define SCC_GSMRL_ASYNC_HDLC    0x00000006      /* async HDLC mode (microcode)*/
#define SCC_GSMRL_V14           0x00000007      /* V.14 mode */
#define SCC_GSMRL_BISYNC        0x00000008      /* BISYNC mode */
#define SCC_GSMRL_DDCMP         0x00000009      /* DDCMP mode (microcode) */
#define SCC_GSMRL_ETHERNET      0x0000000c      /* ethernet mode (SCC1 only) */
#define SCC_GSMRL_ENT           0x00000010      /* enable transmitter */
#define SCC_GSMRL_ENR           0x00000020      /* enable receiver */
#define SCC_GSMRL_LOOPBACK      0x00000040      /* local loopback mode */
#define SCC_GSMRL_ECHO          0x00000080      /* automatic echo mode */
#define SCC_GSMRL_TENC          0x00000700      /* transmitter encoding method*/
#define SCC_GSMRL_RENC          0x00003800      /* receiver encoding method */
#define SCC_GSMRL_RDCR_X8       0x00004000      /* receive DPLL clock x8 */
#define SCC_GSMRL_RDCR_X16      0x00008000      /* receive DPLL clock x16 */
#define SCC_GSMRL_RDCR_X32      0x0000c000      /* receive DPLL clock x32 */
#define SCC_GSMRL_TDCR_X8       0x00010000      /* transmit DPLL clock x8 */
#define SCC_GSMRL_TDCR_X16      0x00020000      /* transmit DPLL clock x16 */
#define SCC_GSMRL_TDCR_X32      0x00030000      /* transmit DPLL clock x32 */
#define SCC_GSMRL_TEND          0x00040000      /* transmitter frame ending */
#define SCC_GSMRL_TPP_00        0x00180000      /* Tx preamble pattern = 00 */
#define SCC_GSMRL_TPP_10        0x00080000      /* Tx preamble pattern = 10 */
#define SCC_GSMRL_TPP_01        0x00100000      /* Tx preamble pattern = 01 */
#define SCC_GSMRL_TPP_11        0x00180000      /* Tx preamble pattern = 11 */
#define SCC_GSMRL_TPL_NONE      0x00000000      /* no Tx preamble (default) */
#define SCC_GSMRL_TPL_8         0x00200000      /* Tx preamble = 1 byte */
#define SCC_GSMRL_TPL_16        0x00400000      /* Tx preamble = 2 bytes */
#define SCC_GSMRL_TPL_32        0x00600000      /* Tx preamble = 4 bytes */
#define SCC_GSMRL_TPL_48        0x00800000      /* Tx preamble = 6 bytes */
#define SCC_GSMRL_TPL_64        0x00a00000      /* Tx preamble = 8 bytes */
#define SCC_GSMRL_TPL_128       0x00c00000      /* Tx preamble = 16 bytes */
#define SCC_GSMRL_TINV          0x01000000      /* DPLL transmit input invert */
#define SCC_GSMRL_RINV          0x02000000      /* DPLL receive input invert */
#define SCC_GSMRL_TSNC          0x0c000000      /* transmit sense */
#define SCC_GSMRL_TCI           0x10000000      /* transmit clock invert */
#define SCC_GSMRL_EDGE          0x60000000      /* adjustment edge +/- */
 
#define SCC_GSMRH_RSYN          0x00000001      /* receive sync timing*/
#define SCC_GSMRH_RTSM          0x00000002      /* RTS* mode */
#define SCC_GSMRH_SYNL          0x0000000c      /* sync length */
#define SCC_GSMRH_TXSY          0x00000010      /* transmitter/receiver sync */
#define SCC_GSMRH_RFW           0x00000020      /* Rx FIFO width */
#define SCC_GSMRH_TFL           0x00000040      /* transmit FIFO length */
#define SCC_GSMRH_CTSS          0x00000080      /* CTS* sampling */
#define SCC_GSMRH_CDS           0x00000100      /* CD* sampling */
#define SCC_GSMRH_CTSP          0x00000200      /* CTS* pulse */
#define SCC_GSMRH_CDP           0x00000400      /* CD* pulse */
#define SCC_GSMRH_TTX           0x00000800      /* transparent transmitter */
#define SCC_GSMRH_TRX           0x00001000      /* transparent receiver */
#define SCC_GSMRH_REVD          0x00002000      /* reverse data */
#define SCC_GSMRH_TCRC          0x0000c000      /* transparent CRC */
#define SCC_GSMRH_GDE           0x00010000      /* glitch detect enable */

/* CPM - Communication Processor Module */

/* SCC Ethernet Protocol Specific Mode Register definitions */
 
#define SCC_ETHER_PSMR_FDE      0x0001
#define SCC_ETHER_PSMR_NIB_13   0x0000          /* SFD 13 bits after TENA */
#define SCC_ETHER_PSMR_NIB_14   0x0002          /* SFD 14 bits after TENA */
#define SCC_ETHER_PSMR_NIB_15   0x0004          /* SFD 15 bits after TENA */
#define SCC_ETHER_PSMR_NIB_16   0x0006          /* SFD 16 bits after TENA */
#define SCC_ETHER_PSMR_NIB_21   0x0008          /* SFD 21 bits after TENA */
#define SCC_ETHER_PSMR_NIB_22   0x000a          /* SFD 22 bits after TENA */
#define SCC_ETHER_PSMR_NIB_23   0x000c          /* SFD 23 bits after TENA */
#define SCC_ETHER_PSMR_NIB_24   0x000e          /* SFD 24 bits after TENA */
#define SCC_ETHER_PSMR_LCW      0x0010          /* late collision window */
#define SCC_ETHER_PSMR_SIP      0x0200          /* sample input pins */
#define SCC_ETHER_PSMR_LPB      0x0040          /* loopback operation */
#define SCC_ETHER_PSMR_SBT      0x0080          /* stop backoff timer */
#define SCC_ETHER_PSMR_BRO      0x0100          /* broadcast address */
#define SCC_ETHER_PSMR_PRO      0x0200          /* promiscuous mode */
#define SCC_ETHER_PSMR_CRC      0x0800          /* CRC selection */
#define SCC_ETHER_PSMR_IAM      0x1000          /* individual address mode */
#define SCC_ETHER_PSMR_RSH      0x2000          /* receive short frame */
#define SCC_ETHER_PSMR_FC       0x4000          /* force collision */
#define SCC_ETHER_PSMR_HBC      0x8000          /* heartbeat checking*/
 
/* SCC Ethernet Event and Mask Register definitions */
 
#define SCC_ETHER_SCCX_RXB      0x0001          /* buffer received event */
#define SCC_ETHER_SCCX_TXB      0x0002          /* buffer transmitted event */
#define SCC_ETHER_SCCX_BSY      0x0004          /* busy condition */
#define SCC_ETHER_SCCX_RXF      0x0008          /* frame received event */
#define SCC_ETHER_SCCX_TXE      0x0010          /* transmission error event */
#define SCC_ETHER_SCCX_GRA      0x0080          /* graceful stop event */
 
/* SCC Ethernet Receive Buffer Descriptor definitions */
 
#define SCC_ETHER_RX_BD_CL      0x0001          /* collision condition */
#define SCC_ETHER_RX_BD_OV      0x0002          /* overrun condition */
#define SCC_ETHER_RX_BD_CR      0x0004          /* Rx CRC error */
#define SCC_ETHER_RX_BD_SH      0x0008          /* short frame received */
#define SCC_ETHER_RX_BD_NO      0x0010          /* Rx nonoctet aligned frame */
#define SCC_ETHER_RX_BD_LG      0x0020          /* Rx frame length violation */
#define SCC_ETHER_RX_BD_M       0x0100          /* miss bit for prom mode */
#define SCC_ETHER_RX_BD_F       0x0400          /* buffer is first in frame */
#define SCC_ETHER_RX_BD_L       0x0800          /* buffer is last in frame */
#define SCC_ETHER_RX_BD_I       0x1000          /* interrupt on receive */
#define SCC_ETHER_RX_BD_W       0x2000          /* last BD in ring */
#define SCC_ETHER_RX_BD_E       0x8000          /* buffer is empty */
 
/* SCC Ethernet Transmit Buffer Descriptor definitions */
 
#define SCC_ETHER_TX_BD_CSL     0x0001          /* carrier sense lost */
#define SCC_ETHER_TX_BD_UN      0x0002          /* underrun */
#define SCC_ETHER_TX_BD_RC      0x003c          /* retry count */
#define SCC_ETHER_TX_BD_RL      0x0040          /* retransmission limit */
#define SCC_ETHER_TX_BD_LC      0x0080          /* late collision */
#define SCC_ETHER_TX_BD_HB      0x0100          /* heartbeat */
#define SCC_ETHER_TX_BD_DEF     0x0200          /* defer indication */
#define SCC_ETHER_TX_BD_TC      0x0400          /* auto transmit CRC */
#define SCC_ETHER_TX_BD_L       0x0800          /* buffer is last in frame */
#define SCC_ETHER_TX_BD_I       0x1000          /* interrupt on transmit */
#define SCC_ETHER_TX_BD_W       0x2000          /* last BD in ring */
#define SCC_ETHER_TX_BD_PAD     0x4000          /* auto pad short frames */
#define SCC_ETHER_TX_BD_R       0x8000          /* buffer is ready */

/* typedefs */

/* SCC - Serial Comunications Controller */

typedef struct          /* SCC_ETHER_PROTO */
    {
    UINT32      c_pres;                 /* preset CRC */
    UINT32      c_mask;                 /* constant mask for CRC */
    UINT32      crcec;                  /* CRC error counter */
    UINT32      alec;                   /* alignment error counter */
    UINT32      disfc;                  /* discard frame counter */
    UINT16      pads;                   /* short frame pad value */
    UINT16      ret_lim;                /* retry limit threshold */
    UINT16      ret_cnt;                /* retry limit counter */
    UINT16      mflr;                   /* maximum frame length register */
    UINT16      minflr;                 /* minimum frame length register */
    UINT16      maxd1;                  /* max DMA1 length register */
    UINT16      maxd2;                  /* max DMA2 length register */
    UINT16      maxd;                   /* Rx max DMA */
    UINT16      dma_cnt;                /* Rx DMA counter */
    UINT16      max_b;                  /* max BD byte count */
    UINT16      gaddr1;                 /* group address filter 1 */
    UINT16      gaddr2;                 /* group address filter 2 */
    UINT16      gaddr3;                 /* group address filter 3 */
    UINT16      gaddr4;                 /* group address filter 4 */
    UINT32      tbuf0_data0;            /* save area 0 - current frame */
    UINT32      tbuf0_data1;            /* save area 1 - current frame */
    UINT32      tbuf0_rba0;             /* ? */
    UINT32      tbuf0_crc;              /* ? */
    UINT16      tbuf0_bcnt;             /* ? */
    UINT16      paddr1_h;               /* physical address 1 (MSB) */
    UINT16      paddr1_m;               /* physical address 1 */
    UINT16      paddr1_l;               /* physical address 1 (LSB) */
    UINT16      p_per;                  /* persistence */
    UINT16      rfbd_ptr;               /* Rx first BD pointer */
    UINT16      tfbd_ptr;               /* Tx first BD pointer */
    UINT16      tlbd_ptr;               /* Tx last BD pointer */
    UINT32      tbuf1_data0;            /* save area 0 - next frame */
    UINT32      tbuf1_data1;            /* ? */
    UINT32      tbuf1_rba0;             /* ? */
    UINT32      tbuf1_crc;              /* ? */
    UINT16      tbuf1_bcnt;             /* ? */
    UINT16      tx_len;                 /* Tx frame length counter */
    UINT16      iaddr1;                 /* individual address filter 1 */
    UINT16      iaddr2;                 /* individual address filter 2 */
    UINT16      iaddr3;                 /* individual address filter 3 */
    UINT16      iaddr4;                 /* individual address filter 4 */
    UINT16      boff_cnt;               /* backoff counter */
    UINT16      taddr_h;                /* temp address (MSB) */
    UINT16      taddr_m;                /* temp address */
    UINT16      taddr_l;                /* temp address (LSB) */
    } SCC_ETHER_PROTO;

/* SCC Parameters */

    typedef struct          /* SCC_PARAM */
        {                   /* offset description*/
        volatile INT16  rbase;      /* 00 Rx buffer descriptor base address */
        volatile INT16  tbase;      /* 02 Tx buffer descriptor base address */
        volatile INT8   rfcr;       /* 04 Rx function code */
        volatile INT8   tfcr;       /* 05 Tx function code */
        volatile INT16  mrblr;      /* 06 maximum receive buffer length */
        volatile INT32  rstate;     /* 08 Rx internal state */
        volatile INT32  res1;       /* 0C Rx internal data pointer */
        volatile INT16  rbptr;      /* 10 Rx buffer descriptor pointer */
        volatile INT16  res2;       /* 12 reserved/internal */
        volatile INT32  res3;       /* 14 reserved/internal */
        volatile INT32  tstate;     /* 18 Tx internal state */
        volatile INT32  res4;       /* 1C reserved/internal */
        volatile INT16  tbptr;      /* 20 Tx buffer descriptor pointer */
        volatile INT16  res5;       /* 22 reserved/internal */
        volatile INT32  res6;       /* 24 reserved/internal */
        volatile INT32  rcrc;       /* 28 temp receive CRC */
        volatile INT32  tcrc;       /* 2C temp transmit CRC */
        } SCC_PARAM;
   
typedef struct          /* SCC */
    {
    SCC_PARAM   param;                  /* SCC parameters */
    char        prot[64];               /* protocol specific area */
    } SCC;

typedef struct          /* SCC_REG */
    {
    UINT32      gsmrl;                  /* SCC general mode register - low */
    UINT32      gsmrh;                  /* SCC eneral mode register - high */
    UINT16      psmr;                   /* SCC protocol mode register */
    UINT16      res1;                   /* reserved */
    UINT16      todr;                   /* SCC transmit on demand */
    UINT16      dsr;                    /* SCC data sync. register */
    UINT16      scce;                   /* SCC event register */
    UINT16      res2;                   /* reserved */
    UINT16      sccm;                   /* SCC mask register */
    UINT8       res3;                   /* reserved */
    UINT8       sccs;                   /* SCC status register */
    } SCC_REG;

/* SCC Buffer */

#ifdef SCC_BUF
#undef SCC_BUF
#endif

typedef struct          /* SCC_BUF */
    {
    UINT16      statusMode;     /* status and control */
    UINT16      dataLength;     /* length of data buffer in bytes */
    u_char  *   dataPointer;    /* points to data buffer */
    } SCC_BUF;

/* SCC device descriptor */

typedef struct          /* SCC_ETHER_DEV */
    {
    int                 sccNum;         /* number of SCC device */
    int                 txBdNum;        /* number of transmit buf descriptors */
    int                 rxBdNum;        /* number of receive buf descriptors */
    SCC_BUF *           txBdBase;       /* transmit BD base address */
    SCC_BUF *           rxBdBase;       /* receive BD base address */
    u_char *            txBufBase;      /* transmit buffer base address */
    u_char *            rxBufBase;      /* receive buffer base address */
    UINT32              txBufSize;      /* transmit buffer size */
    UINT32              rxBufSize;      /* receive buffer size */
    int                 txBdNext;       /* next transmit BD to fill */
    int                 rxBdNext;       /* next receive BD to read */
    volatile SCC *      pScc;           /* SCC parameter RAM */
    volatile SCC_REG *  pSccReg;        /* SCC registers */
    UINT32              intMask;        /* interrupt acknowledge mask */
    } SCC_ETHER_DEV;


/* globals */
 
IMPORT STATUS sysSccEnetEnable  (int unit); /* enable ctrl */
IMPORT void   sysSccEnetDisable (int unit); /* disable ctrl */
IMPORT STATUS sysSccEnetAddrGet (int unit, u_char * addr);  /* get enet addr */


#ifdef  __cplusplus
}
#endif

#endif  /* __INCm8260SccEndh */

