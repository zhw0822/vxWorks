/* device.h - Removable device header file */

/* Copyright 2004 Wind River Systems, Inc */

/*
modification history
--------------------
01b,20apr05,rfr  Added devName() function
01a,25mar05,rfr  Removed prototype eventing system.
*/

#ifndef __INCdeviceh
#define __INCdeviceh

#include <types.h>

#define MAX_DEVNAME 10

#define NULLDEV 0

#define DV_TYPE_NONE  0
#define DV_TYPE_XBD   1


struct device;

typedef uint32_t device_t;
typedef char devname_t[MAX_DEVNAME];

struct device 
{
    device_t dv_dev;		/* This device's device_t */
    int dv_type;		/* DV_TYPE_XBD, etc */
    devname_t dv_xname;		/* The name of this device */
    int dv_error;		/* insert/delete errors */
};

/* 
* These are functions to be called by device drivers that conform to this 
* interface. 
*/

int devAttach(struct device *dev, const char *name, int type, device_t *result);
STATUS devDetach(struct device *dev);

/*
* This is the initialization function for the device infrastructure. It must be 
* called before any other device infrastructure calls
*/

STATUS devInit(uint16_t ndevs);

/* 
* These functions manage the mapping between device_t and struct device *.
* Every call to devMap() should be paired with a call to devUnmap().
*/

struct device *devMap(device_t dev);
int devUnmap(struct device *);

STATUS devName(device_t dev, devname_t name);

#endif
