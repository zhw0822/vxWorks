/* dot11RsnLib.h - Contains definitions for the 802.11 RSN Library  */

/* Copyright 2004 Wind River Systems, Inc. */

/* 
Modification History
--------------------
01c,20jan05,rb  Implement MIC countermeasures
01b,14jan05,rb  Added AESKW macros
01a,8nov04,rb   Initial revision
*/

#ifndef __INCdot11RsnLibh
#define __INCdot11RsnLibh
#ifdef  __cplusplus
extern "C" {
#endif /* __cplusplus */

/***************************************************************************
* DOT11_RSN_IE - Used for 802.11i and WPA IEs
****************************************************************************/
typedef struct
    {
    UINT8 oui[3];
    UINT8 suiteType;
    } _WRS_PACK_ALIGN(1) DOT11_RSN_SUITE;

typedef struct
    {
    UINT8  elementId;
    UINT8  length;
    UINT16 version;
    DOT11_RSN_SUITE grpCipherSuite;
    
    /* The rest of the fields are variable length and optional, so we'll let 
    them be dynamically generated and not represented in this structure */

    } _WRS_PACK_ALIGN(1) DOT11_RSN_IE;

#define DOT11_RSN_IE_VERSION            1
#define DOT11_RSN_OUI_0                 0x00
#define DOT11_RSN_OUI_1                 0x0f
#define DOT11_RSN_OUI_2                 0xac


#define DOT11_WPA_IE_VERSION            1
#define DOT11_WPA_OUI_0                 0x00
#define DOT11_WPA_OUI_1                 0x50
#define DOT11_WPA_OUI_2                 0xf2

/* These are the values from 7.3.2.25.1 of IEEE 802.11i for cipher suites */
#define DOT11_RSN_SUITE_GROUP           0
#define DOT11_RSN_SUITE_WEP40           1
#define DOT11_RSN_SUITE_WEP104          5
#define DOT11_RSN_SUITE_TKIP            2
#define DOT11_RSN_SUITE_AES             4

/* These are the values from 7.3.2.25.2 of IEEE 802.11 for auth suites */
#define DOT11_RSN_SUITE_8021X           1
#define DOT11_RSN_SUITE_PSK             2

#define DOT11_AUTH_SUITE_TO_AUTHPOL(x)  x     

/* This is used in the WPA IE to distinguish which WPA feature is being
advertised */
#define DOT11_WPA_SUITE_ADVERTISEMENT   1

#define DOT11_SHA_DIGESTLEN             20 /* Bytes */
#define DOT11_PBKDF2_ITERATION          4096
#define DOT11_PBKDF2_MINCHAR            32
#define DOT11_PBKDF2_MAXCHAR            126

#define DOT11_AESKW_IV_LEN              8

#define DOT11_MIC_FAILURE_TIME          (sysClkRateGet() * 60)

#ifdef  __cplusplus
}
#endif /* __cplusplus */
#endif /* __INCdot11RsnLibh */
