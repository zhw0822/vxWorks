/* dot11SmeLib.h - Contains PRIVATE definitions for intra-SME use */

/* Copyright 2004 Wind River Systems, Inc. */

/* 
Modification History
--------------------
02a,25aug04,rb  Wind River Wireless Ethernet Driver 2.0 FCS
*/

#ifndef __INCdot11SmeLibh
#define __INCdot11SmeLibh
#ifdef  __cplusplus
extern "C" {
#endif /* __cplusplus */

#include <drv/wlan/dot11Lib.h>

/* Known station list functions */
STATUS dot11KslInit(DOT11_FW * pDot11);

/* Station Management Entity functions */
STATUS dot11SmeEssInit(DOT11_FW * pDot11 );

STATUS dot11SmeActualRateCalc(DOT11_FW * pDot11, DOT11_RATES * pRates1,
                              DOT11_RATES * pRates2, 
                              DOT11_RATES * pRates3, 
                              DOT11_RATES * pActualRates);

STATUS dot11SmeRateSort (DOT11_RATES * pRates );





#ifdef  __cplusplus
}
#endif /* __cplusplus */
#endif /* __INCdot11SmeLibh */


