/* dot11HddLib.h - Contains function prototypes for the built-in HDD types*/

/* Copyright 2004 Wind River Systems, Inc. */

/*
Modification History
--------------------
02a,25aug04,rb  Wind River Wireless Ethernet Driver 2.0 FCS
*/

#ifndef __INCdot11HddLibh
#define __INCdot11HddLibh
#ifdef  __cplusplus
extern "C" {
#endif /* __cplusplus */

/* Atheros device driver chip set device and vendor identifiers */
#define AR_PCI_VEN_ID     (0x168C)
#define AR_PCI_DEV_AR5211 (0x0012)
#define AR_PCI_DEV_AR5212 (0x0013)

/* This is the initialize function for the AR521X driver for Atheros based
802.11 A/B/G solutions using the Atheros 5211/5212 MAC */
STATUS ar52Initialize ( DOT11_FW * pDot11 );

#ifdef  __cplusplus
    }
#endif /* __cplusplus */
#endif /* __INCdot11JddLibh */
