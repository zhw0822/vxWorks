/* erfLib.c - Event Reporting Framework Library */

/* 
*  Copyright (c) 2005 Wind River Systems, Inc.
* 
* The right to copy, distribute, modify or otherwise make use
* of this software may be licensed only pursuant to the terms
* of an applicable Wind River license agreement.
*/

/*
modification history
--------------------
01b,22jul05,jmt  Allow multiple tasks and auto unregistration
01a,30mar05,jmt   written
*/

/*
DESCRIPTION
This module provides an Event Reporting Framework Show and debug routines.

INCLUDE FILES: 
*/ 

#ifndef INCerfShowh
#define INCerfShowh

/* defines */

/* typedefs */

/* forward declarations */

void erfShow();
UINT16 erfMaxCategoriesGet();
UINT16 erfMaxTypesGet();
UINT16 erfDefaultQueueSizeGet();

#endif /* INCerfShowh */
