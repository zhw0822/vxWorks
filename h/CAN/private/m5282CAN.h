/* m5282CAN.h - definitions for MCF5282 baords */

/* Copyright 2001 Wind River Systems, Inc. */

/* 
modification history
--------------------
01b,08apr04,lsg Modified for supporting WindNet CAN 1.3
01a,24sep03,eli support for MCF5282 (ColdFire) FlexCAN module

*/

/* 

DESCRIPTION
This file contains the declarations and definitions that are used for
MCF5282 boards

*/

#ifndef _m5282CAN_H_
#define _m5282CAN_H_

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/*Define the number of controllers on-board (or on-chip)*/
#define MAX_M5282_CAN_DEVICES	1

struct m5282CAN_DeviceEntry
    {
    BOOL                    inUse;
    struct WNCAN_Device     canDevice;
    struct WNCAN_Controller canController;
    struct WNCAN_Board      canBoard;
    struct canAccess        canRegAccess;
    BOOL                    allocated;
    WNCAN_ChannelMode       FlexCANchnMode[FLEXCAN_MAX_MSG_OBJ];	
    }; 


#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* _m5282CAN_H_ */
