/* 

DESCRIPTION
This file contains the TOUCAN register offsets 

*/

#ifndef TOUCAN_OFFSETS_H_
#define TOUCAN_OFFSETS_H_

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define TOUCAN_MCR_OFFSET                 0x0                  
#define TOUCAN_TTR_OFFSET                 0x2
#define TOUCAN_ICR_OFFSET                 0x4
#define TOUCAN_CR0_OFFSET                 0x6
#define TOUCAN_CR1_OFFSET                 0x7
#define TOUCAN_PRESDIV_OFFSET             0x8
#define TOUCAN_CR2_OFFSET                 0x9
#define TOUCAN_TIMER_OFFSET               0xA
#define TOUCAN_RXGLOBALMASK_OFFSET        0x10
#define TOUCAN_RXBUFF14MASK_OFFSET        0x14
#define TOUCAN_RXBUFF15MASK_OFFSET        0x18
#define TOUCAN_ESTAT_OFFSET               0x20
#define TOUCAN_IMASK_OFFSET               0x22
#define TOUCAN_IFLAG_OFFSET               0x24
#define TOUCAN_RXECTR_OFFSET              0x26
#define TOUCAN_TXECTR_OFFSET              0x27
#define TOUCAN_REG_MAX_OFFSET             0x28

#define TOUCAN_BUFF0_OFFSET		          0x100
#define TOUCAN_BUFF1_OFFSET		          0x110	
#define TOUCAN_BUFF2_OFFSET		          0x120
#define TOUCAN_BUFF3_OFFSET		          0x130	
#define TOUCAN_BUFF4_OFFSET		          0x140
#define TOUCAN_BUFF5_OFFSET		          0x150	
#define TOUCAN_BUFF6_OFFSET		          0x160
#define TOUCAN_BUFF7_OFFSET		          0x170	
#define TOUCAN_BUFF8_OFFSET		          0x180
#define TOUCAN_BUFF9_OFFSET				  0x190	
#define TOUCAN_BUFF10_OFFSET  		      0x1a0
#define TOUCAN_BUFF11_OFFSET  	          0x1b0	
#define TOUCAN_BUFF12_OFFSET  	          0x1c0
#define TOUCAN_BUFF13_OFFSET              0x1d0	
#define TOUCAN_BUFF14_OFFSET   	          0x1e0
#define TOUCAN_BUFF15_OFFSET	          0x1f0	
#define TOUCAN_BUF_MAX_OFFSET             0x200

#define TOUCAN_BUFF_CTRLSTAT_OFFSET       0
#define TOUCAN_BUFF_IDHI_OFFSET           2  
#define TOUCAN_BUFF_IDLO_OFFSET           4
#define TOUCAN_BUFF_DATA0_OFFSET          6
#define TOUCAN_BUFF_DATA1_OFFSET          7
#define TOUCAN_BUFF_DATA2_OFFSET          8
#define TOUCAN_BUFF_DATA3_OFFSET          9
#define TOUCAN_BUFF_DATA4_OFFSET          0xa
#define TOUCAN_BUFF_DATA5_OFFSET          0xb
#define TOUCAN_BUFF_DATA6_OFFSET          0xc
#define TOUCAN_BUFF_DATA7_OFFSET          0xd

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif
