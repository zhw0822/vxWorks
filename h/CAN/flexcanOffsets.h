/* flexcanOffsets.h - definitions for Flexcan for WNCAN Interface */

/* Copyright 2001 Wind River Systems, Inc. */

/* 
modification history
--------------------
01b,08jun04,lsg  created

*/

/* 

  DESCRIPTION
  This file contains the FlexCAN register offsets 

  
*/
#ifndef FLEXCAN_OFFSETS_H_
#define FLEXCAN_OFFSETS_H_

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


/*****************************************************************************
* Register offsets
*/

#define FLEXCAN_MCR_OFFSET                 0x0
#define FLEXCAN_CR0_OFFSET                 0x6
#define FLEXCAN_CR1_OFFSET                 0x7
#define FLEXCAN_PRESDIV_OFFSET             0x8
#define FLEXCAN_CR2_OFFSET                 0x9
#define FLEXCAN_TIMER_OFFSET               0xA
#define FLEXCAN_RXGLOBALMASK_OFFSET        0x10
#define FLEXCAN_RXBUFF14MASK_OFFSET        0x14
#define FLEXCAN_RXBUFF15MASK_OFFSET        0x18
#define FLEXCAN_ESTAT_OFFSET               0x20
#define FLEXCAN_IMASK_OFFSET               0x22
#define FLEXCAN_IFLAG_OFFSET               0x24
#define FLEXCAN_RXECTR_OFFSET              0x26
#define FLEXCAN_TXECTR_OFFSET              0x27
#define FLEXCAN_REG_MAX_OFFSET             0x28

#define FLEXCAN_BUFF0_OFFSET               0x80
#define FLEXCAN_BUFF1_OFFSET               0x90
#define FLEXCAN_BUFF2_OFFSET               0xA0
#define FLEXCAN_BUFF3_OFFSET               0xB0
#define FLEXCAN_BUFF4_OFFSET               0xC0
#define FLEXCAN_BUFF5_OFFSET               0xD0
#define FLEXCAN_BUFF6_OFFSET               0xE0
#define FLEXCAN_BUFF7_OFFSET               0xF0
#define FLEXCAN_BUFF8_OFFSET               0x100
#define FLEXCAN_BUFF9_OFFSET               0x110
#define FLEXCAN_BUFF10_OFFSET              0x120
#define FLEXCAN_BUFF11_OFFSET              0x130
#define FLEXCAN_BUFF12_OFFSET              0x140
#define FLEXCAN_BUFF13_OFFSET              0x150
#define FLEXCAN_BUFF14_OFFSET              0x160
#define FLEXCAN_BUFF15_OFFSET              0x170
#define FLEXCAN_BUF_MAX_OFFSET             0x280

#define TOUCAN_BUFF_CTRLSTAT_OFFSET        0
#define TOUCAN_BUFF_IDHI_OFFSET            2  
#define TOUCAN_BUFF_IDLO_OFFSET            4
#define TOUCAN_BUFF_DATA0_OFFSET           6
#define TOUCAN_BUFF_DATA1_OFFSET           7
#define TOUCAN_BUFF_DATA2_OFFSET           8
#define TOUCAN_BUFF_DATA3_OFFSET           9
#define TOUCAN_BUFF_DATA4_OFFSET           0xa
#define TOUCAN_BUFF_DATA5_OFFSET           0xb
#define TOUCAN_BUFF_DATA6_OFFSET           0xc
#define TOUCAN_BUFF_DATA7_OFFSET           0xd

#if __cplusplus
}
#endif

#endif  /* _FLEXCAN_OFFSETS_H_ */
