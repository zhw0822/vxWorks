/* motMsCanOffsets.h - register offset definitions for the Motorola MSCAN */

/* Copyright 1984-2004 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/* 
modification history
--------------------
01c,25jun04,bjn  Adding extern "C" for CPP.
01b,05may04,bjn  Removed MOTMSCAN_GetCanId, not required for external usage.
01a,23oct03,bjn  written
*/

/*
DESCRIPTION
This file contains the functions Motorola Scalable CAN register offsets
*/

#ifndef _MOTMSCAN_OFFSETS_H_
#define _MOTMSCAN_OFFSETS_H_

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/* MSCAN Base Register Offsets */

#define can_CR0           (0x00)
#define can_CR1           (0x01)
#define can_BTR0          (0x04)
#define can_BTR1          (0x05)
#define can_RFLG          (0x08)
#define can_RIER          (0x09)
#define can_TFLG          (0x0C)
#define can_TIER          (0x0D)
#define can_TARQ          (0x10)
#define can_TAAK          (0x11)
#define can_TBSEL         (0x14)
#define can_IDAC          (0x15)
#define can_RXERR         (0x1c)
#define can_TXERR         (0x1d)

/* ID Acceptance Registers */
#define can_IDAR0         (0x20)
#define can_IDAR1         (0x21)
#define can_IDAR2         (0x24)
#define can_IDAR3         (0x25)
#define can_IDAR4         (0x30)
#define can_IDAR5         (0x31)
#define can_IDAR6         (0x34)
#define can_IDAR7         (0x35)

/* ID Mask Registers */
#define can_IDMR0         (0x28)
#define can_IDMR1         (0x29)
#define can_IDMR2         (0x2C)
#define can_IDMR3         (0x2D)
#define can_IDMR4         (0x38)
#define can_IDMR5         (0x39)
#define can_IDMR6         (0x3C)
#define can_IDMR7         (0x3D)

/* RX FG Buffer */
#define can_RXIDR0        (0x40)
#define can_RXIDR1        (0x41)
#define can_RXIDR2        (0x44)
#define can_RXIDR3        (0x45)
#define can_RXDSR0        (0x48)
#define can_RXDSR1        (0x49)
#define can_RXDSR2        (0x4c)
#define can_RXDSR3        (0x4d)
#define can_RXDSR4        (0x50)
#define can_RXDSR5        (0x51)
#define can_RXDSR6        (0x54)
#define can_RXDSR7        (0x55)
#define can_RXDLR         (0x58)
#define can_RXTIMH        (0x5c)
#define can_RXTIML        (0x5d)

/* TX FG Buffer */
#define can_TXIDR0        (0x60)
#define can_TXIDR1        (0x61)
#define can_TXIDR2        (0x64)
#define can_TXIDR3        (0x65)
#define can_TXDSR0        (0x68)
#define can_TXDSR1        (0x69)
#define can_TXDSR2        (0x6c)
#define can_TXDSR3        (0x6d)
#define can_TXDSR4        (0x70)
#define can_TXDSR5        (0x71)
#define can_TXDSR6        (0x74)
#define can_TXDSR7        (0x75)
#define can_TXDLR         (0x78)
#define can_TXTBPR        (0x79)
#define can_TXTIMH        (0x7c)
#define can_TXTIML        (0x7d)


/*
 * Additional MSCAN API functions, provided mainly to configure
 * MSCAN filters
 */

/*
 * CAN IDs for MSCAN ID Mask Registers:
 *  '0' bit indicates match the bit.
 *  '1' bit indicates don't match the bit.
 */

#define NO_MATCH_STD_BITS (0x000007ffUL) /* 11 Bits for standard CAN */
#define NO_MATCH_EXT_BITS (0x1fffffffUL) /* 29 Bits for extended CAN */

typedef union
    {
    struct
        {
        UINT32 IDR0_3;
        }
        id32;

    struct
        {
        UINT16 IDR0_1;
        UINT16 IDR2_3;
        }
        id16;

    struct
        {
        UINT8 IDR0;
        UINT8 IDR1;
        UINT8 IDR2;
        UINT8 IDR3;
        }
        id8;
    }
    MSCAN_ID_REG;

#define SET_MSCAN_IDE_BIT (0x01)
#define SET_MSCAN_RTR_BIT (0x02)
#define SET_MSCAN_SRR_BIT (0x04)

void MOTMSCAN_GetIdMsgBuffer (long canId, BOOL ext, UINT8 options,
                              MSCAN_ID_REG * pMSCAN_IDR);

/*****************************************************************************
* Register: IDAC
*/

#define IDAM_BS    (3)

#define IDAM_2_32BIT_FILTERS (0x00)
#define IDAM_4_16BIT_FILTERS (0x10)
#define IDAM_8_08BIT_FILTERS (0x20)
#define IDAM_FILTER_CLOSED   (0x30)

#define IDAM_BITS  (0x30)
#define IDHIT_BITS (0x07)

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* _MOTMSCAN_OFFSETS_H_ */
