/* hcan_2.h -  Hitachi CAN v2 header file */

/* Copyright 2002 Wind River Systems, Inc. */

/* 
modification history 
--------------------
01a,29mar05,lsg  Fix capitalization in include
12Nov02, rgp written

*/


#ifndef _HCAN2_H_
#define _HCAN2_H_

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include <CAN/wnCAN.h>


/***********************************************************************/
/** constants regarding the HCAN controller                           **/
/***********************************************************************/
 
#define HCAN2_MAX_MSG_OBJ     32



/***********************************************************************/
/** data structures for the HCAN controller                           **/
/***********************************************************************/
struct HCan2_ChannelData
{
   WNCAN_ChannelMode chnMode[HCAN2_MAX_MSG_OBJ];
};


/* data placed into the csData of the channel datastructure */
typedef struct _ta_ctrlrdata
{
    void  *regbase;            /* register base address */
    
    /* since this controller can exist either on-chip or part
    ** of the Tahoe-Amanda, those boards will have different
    ** ways to sleep and wake the clock associated with the
    ** controller. Thus, provide for functions that are board
    ** specific, to wake and sleep, enable,disable
    **/
    int    wake_sleep_value;        /* value to pass to wake/sleep fn */

        /* board's wake and sleep functions */
    STATUS (*wakefn)(int);          
    STATUS (*sleepfn)(int);

        /* board's IRQ enable/disable functions */
    void   (*enableirqfn)(struct WNCAN_Device *);
    void   (*disableirqfn)(struct WNCAN_Device *);


} HCan2_PvtCtrlrData;


/***********************************************************************/
/** external globals for the controller                               **/
/***********************************************************************/
extern const WNCAN_ChannelType g_hcan2chnType[HCAN2_MAX_MSG_OBJ];



/***********************************************************************/
/** external functions for the  controller                            **/
/***********************************************************************/
void HCan2_registration(void);

#ifdef __cplusplus
}
#endif /* __cplusplus */



#endif /* _HCAN2_H_ */
