/* flexcan.h - definitions for Flexcan on-chip controller for Windnet CAN Interface */

/* Copyright 2001 Wind River Systems, Inc. */

/* 
modification history
--------------------
01c,20may05,lsg	 Added workaround for chip bug for incorrect 32 bit accesses
                 of FlexCAN registers. This affects Filter register setting 
01b,08apr04,lsg  modified to support WindNet CAN 1.3
01a,24sep03,eli  support for MCF5282 (ColdFire) FlexCAN module

*/

/* 

  DESCRIPTION
  This file contains the declarations and definitions that comprise the
  register definitions and other macros for use with Windnet CAN Interface.
  
*/

#ifndef _FLEXCAN_H_
#define _FLEXCAN_H_

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


/* Internal Memory Map Assignments 
 *
 * This is the internal message sructure as used by the 
 *   CAN device driver source code. The user never uses this structure
 */

typedef struct _flexcanChannel_ {
    volatile unsigned short Control;
    volatile unsigned short Id;
    volatile unsigned short Id2_OR_TimeStamp;
    volatile unsigned char  Data[8];
    volatile unsigned short No_use;

} FlexCAN_StandardMsgType;

#define FLEXCAN_INACTIVE_BUFFER     0x0000
#define FLEXCAN_BUFFER_READY_TO_RX  0x0040
#define FLEXCAN_BUFFER_FULL         0x0020
#define FLEXCAN_BUFFER_OVERRUN      0x0060
#define FLEXCAN_BUFFER_BUSY         0x0010

#define FLEXCAN_TX_NOT_READY        0x0080
#define FLEXCAN_TX                  0x00c0
#define FLEXCAN_RESPONSE_TO_REMOTE  0x00a0

#define FLEXCAN_REGS_BASE            0x001c0000
#define FLEXCAN_BUFFERS_BASE         0x001c0080
#define FLEXCAN_LBUF_LOWEST_ID_FIRST 0
#define FLEXCAN_TSYNC_ENABLE         1
#define FLEXCAN_MAX_MSG_OBJ          16
#define FLEXCAN_REG_MAX_OFFSET       0x080
#define FLEXCAN_BUF_MAX_OFFSET       0x180

#define FLEXCAN_NUM_INT_SRCS         19
#define FLEXCAN_INT_SRC_MIN_NUM      8
#define FLEXCAN_INT_SRC_MAX_NUM      26
/*****************************************************************************
* Register offsets
*/

typedef struct _flexcan_
    {
    volatile unsigned short         can_MCR;    /* Module Configuration Register */       
    volatile unsigned short         can_res1;           
    volatile unsigned short         can_res2;
    volatile unsigned char          can_CR0;
    volatile unsigned char          can_CR1;
    volatile unsigned char          can_PRESDIV;
    volatile unsigned char          can_CR2;
    volatile unsigned short         can_TIMER;
    volatile unsigned short         can_res3[2];
    volatile unsigned short         can_RxGlobalMask_hi;
    volatile unsigned short         can_RxGlobalMask_lo;
    volatile unsigned short         can_RxBuff14Mask_hi;
    volatile unsigned short         can_RxBuff14Mask_lo;
    volatile unsigned short         can_RxBuff15Mask_hi;
    volatile unsigned short         can_RxBuff15Mask_lo;
    volatile unsigned short         can_res4[2];
    volatile unsigned short         can_ESTAT;
    volatile unsigned short         can_IMASK;
    volatile unsigned short         can_IFLAG;
    volatile unsigned char          can_RxECTR;
    volatile unsigned char          can_TxECTR;
    } _FlexCAN;                                          /* 384 bytes total */

/* Module Configuration Register */

#define FLEXCAN_STOP     0x8000      /* Disable FlexCAN clocks */
#define FLEXCAN_FRZ      0x4000      /* FlexCAN enabled to enter debug mode */
#define FLEXCAN_HALT     0x1000      /* FlexCAN enters debug mode */
#define FLEXCAN_NOTRDY   0x0800      /* FlexCAN is in low-power stop or debug mode */
#define FLEXCAN_WAKEMSK  0x0400      /* wakeup interrupt is enabled */
#define FLEXCAN_SOFTRST  0x0200      /* soft reset cycle initiated/completed */
#define FLEXCAN_FRZACK   0x0100      /* FlexCAN has entered/exited debug mode */
#define FLEXCAN_SUPV     0x0080      /* supervisor/user mode */
#define FLEXCAN_SELFWAKE 0x0040      /* self wake enabled */
#define FLEXCAN_APS      0x0020      /* auto-power save */
#define FLEXCAN_STOPACK  0x0010      /* FlexCAN entered low-power stop mode */

/* Control Register 0 */

#define FLEXCAN_BOFFMSK     0x80        /* bus off interrupt enable */
#define FLEXCAN_ERRMSK      0x40        /* error interrupt enable */
#define FLEXCAN_RXMODE      0x04        /* CANRX pin polarity */
#define FLEXCAN_TXMODE_POS  0x00        /* CANTX pin positive polarity */
#define FLEXCAN_TXMODE_NEG  0x01        /* CANTX pin negative polarity */
#define FLEXCAN_TXMODE_OPD  0x02        /* CANTX pin open drain */

/* Control Register 1 */

#define FLEXCAN_SAMP        0x80        /* sampling mode */
#define FLEXCAN_TSYNC       0x20        /* timer synchronize mode */
#define FLEXCAN_LBUF        0x10        /* lowest buffer transmitted first */
#define FLEXCAN_LOM         0x08        /* listen only mode */
#define FLEXCAN_PROPSEG     0x07        /* propagation segment time mask */

/* Error and Status Register (ESTAT) */

#define FLEXCAN_BITERR      0xc000      /* transmit bit error */
#define FLEXCAN_ACKERR      0x2000      /* acknowlege error */
#define FLEXCAN_CRCERR      0x1000      /* crc error */
#define FLEXCAN_FORMERR     0x0800      /* message format error */
#define FLEXCAN_STUFFERR    0x0400      /* bit stuff error */
#define FLEXCAN_TXWARN      0x0200      /* transmit error status */
#define FLEXCAN_RXWARN      0x0100      /* receive error status */
#define FLEXCAN_FCS         0x0030      /* fault confinment state */
#define FLEXCAN_ERR_ACTIVE  0x0000      /* confinment state ERROR ACTIVE */
#define FLEXCAN_ERR_PASSIVE 0x0010      /* confinment state ERROR PASSIVE */
#define FLEXCAN_BOFFINT     0x0004      /* bus off interrupt */
#define FLEXCAN_ERRINT      0x0002      /* error interrupt */
#define FLEXCAN_WAKEINT     0x0001      /* wake interrupt */

typedef struct _flexcanBuffers_
{
   volatile FlexCAN_StandardMsgType  can_MSG[FLEXCAN_MAX_MSG_OBJ];
}_FlexCANbuffers;

typedef volatile _FlexCAN *FlexCAN;
typedef volatile _FlexCANbuffers *FlexCANBuf;

struct canAccess
{
    FlexCAN      pFlexCanRegs;
    FlexCANBuf   pFlexCanBufs;
    UCHAR       FlexcanPropseg;
    USHORT      FlexCANAutoPowersSave;
    USHORT      FlexCANSupv;
    UCHAR       FlexCANTimerSync;
    UCHAR       FlexCANLBuf;
    UCHAR       FlexCANSrcIcrVal[FLEXCAN_NUM_INT_SRCS];
};

/* table of regbases for each can channel */
extern FlexCAN pFlexCanRegsBaseAdr[];
extern FlexCANBuf pFlexCanBufsBaseAdr[];

/*
 * Formatting identifier bits
 */
#define FLEXCAN_ID_BITS_28TO18      0xFFE0
#define FLEXCAN_ID_BITS_17TO15      0x7
#define FLEXCAN_IDE_EXT             0x0008

#define FLEXCAN_M28TO18             0xffe00000
#define FLEXCAN_M17TO0              0x001ffff8

extern const UINT g_FLEXCANchnType[FLEXCAN_MAX_MSG_OBJ];

#define FLEXCAN_RXBUF14_MASK        14
#define FLEXCAN_RXBUF15_MASK        15

/*----------------------------------------------
 * duplicate macros, added in ppcFlexcan.h after 
 * CAN api update
 *
 * FLEXCAN Control Register (FLEXCAN_CR2)
 *(MSB)0   1     2    3   4        5   6    7  
 * --------------------------------------------
 *| RJW       |     PSEG        |    PSEG2     |
 *|           |                 |              |
 * --------------------------------------------
 */

#ifndef FLEXCAN_RJW
#define FLEXCAN_RJW                      0xC0
#endif

#ifndef FLEXCAN_PSEG
#define FLEXCAN_PSEG                     0x38
#endif 

#ifndef FLEXCAN_PSEG2
#define FLEXCAN_PSEG2                    0x07
#endif

#ifndef FLEXCAN_GetM17TO0
#define FLEXCAN_GetM17TO0           0x0007fffe  
#endif

/*------------------------------------------------*/

void FlexCAN_registration(void);

#if __cplusplus
}
#endif

#endif  /* _FLEXCAN_H_ */
