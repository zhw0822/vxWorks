#ifndef _TOUCAN_H_
#define _TOUCAN_H_

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include <drv/misc/can/ppcTouCAN.h>

/***********************************************************************/
/* MPC5XX  Internal Memory Map Assignments (MC5XX)                     */
/***********************************************************************/
/* This is the internal message sructure as used by the 
**   CAN device driver source code. The user never uses this structure
*/

typedef struct _toucanChannel_ {   
    volatile unsigned short Control;
    volatile unsigned short Id;
    volatile unsigned short Id2_OR_TimeStamp;
    volatile unsigned char  Data[8];
    volatile unsigned short No_use;

} TouCAN_StandardMsgType;


/*****************************************************************************
* Register offsets
*/

typedef struct _toucan_
    {
    volatile unsigned short         can_MCR;           
    volatile unsigned short         can_TTR;           
    volatile unsigned short         can_ICR;
    volatile unsigned char          can_CR0;
    volatile unsigned char          can_CR1;
    volatile unsigned char          can_PRESDIV;
    volatile unsigned char          can_CR2;
    volatile unsigned short         can_TIMER;
    volatile unsigned short         can_res1[2];
    volatile unsigned long          can_RxGlobalMask;    
    volatile unsigned long          can_RxBuff14Mask;
    volatile unsigned long          can_RxBuff15Mask;
    volatile unsigned short         can_res2[2];
    volatile unsigned short         can_ESTAT;
    volatile unsigned short         can_IMASK;
    volatile unsigned short         can_IFLAG;
    volatile unsigned char          can_RxECTR;
    volatile unsigned char          can_TxECTR;
    } _TouCAN;                                          /* 384 bytes total */


typedef struct _toucanBuffers_
{
   volatile TouCAN_StandardMsgType  can_MSG[TOUCAN_MAX_MSG_OBJ];
}_TouCANbuffers;

typedef volatile _TouCAN *TouCAN;
typedef volatile _TouCANbuffers *TouCANBuf;

struct canAccess
{
    TouCAN      pTouCanRegs;
    TouCANBuf   pTouCanBufs;
    UCHAR       ToucanPropseg;
    USHORT      TouCANAutoPowersSave;
    USHORT      TouCANSupv;
    UCHAR       TouCANTimerSync;
    UCHAR       TouCANLBuf;
};

/* table of regbases for each can channel */
extern TouCAN pTouCanRegsBaseAdr[];
extern TouCANBuf pTouCanBufsBaseAdr[];

/*
 * Formatting identifier bits
 */
#define TOUCAN_ID_BITS_28TO18      0xFFE0
#define TOUCAN_ID_BITS_17TO15      0x7
#define TOUCAN_IDE_EXT             0x0008

extern const UINT g_TOUCANchnType[TOUCAN_MAX_MSG_OBJ];

#define TOUCAN_RXBUF14_MASK     14
#define TOUCAN_RXBUF15_MASK     15

/*----------------------------------------------
 * duplicate macros, added in ppcToucan.h after 
 * CAN api update
 *
 * TOUCAN Control Register (TOUCAN_CR2)
 *(MSB)0   1     2    3   4        5   6    7  
 * --------------------------------------------
 *| RJW       |     PSEG		|    PSEG2     |
 *|           |                 |              |
 * --------------------------------------------
 */

#ifndef TOUCAN_RJW
#define TOUCAN_RJW                      0xC0
#endif

#ifndef TOUCAN_PSEG
#define TOUCAN_PSEG                     0x38
#endif 

#ifndef TOUCAN_PSEG2
#define TOUCAN_PSEG2                    0x07
#endif

#ifndef TOUCAN_GetM17TO0
#define TOUCAN_GetM17TO0           0x0007fffe  
#endif

/*------------------------------------------------*/

void TouCAN_registration(void);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif  /* _toucan_H_ */
