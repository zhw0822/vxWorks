/* utime.h - POSIX file time modification */

/*
modification history
--------------------
01a,07nov03,dat  started history, added const to utime args, removed
		old style declaration.
*/


#ifndef	__INCutimeh
#define	__INCutimeh

#ifdef __cplusplus
extern "C" {
#endif

struct utimbuf
    {
    time_t	actime;		/* set the access time */
    time_t	modtime;	/* set the modification time */
    };

extern int utime (const char * file, const struct utimbuf * newTimes);

#ifdef __cplusplus
}
#endif

#endif /* __INCdosFsLibh */
