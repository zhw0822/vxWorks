/* passFsLib.h - pass-through file system library header */

/* Copyright 1984-2005 Wind River Systems, Inc. */

/*
modification history
--------------------
01g,11feb05,dbt  Fixed cache support (SPR #106354).
01f,13sep04,jmp  made passFs cache configurable.
01e,09sep04,jmp  added flags and mode to PASS_FILE_DESC (SPR #97903).
01d,02apr04,jeg  Updated PASS_FILE_DESC fd field name.
01c,18mar03,jeg  removed passFs flags and conversion macro.
01b,05mar03,jeg  add several Unix file specific definitions
01a,05jun93,gae  written.
*/


#ifndef __INCpassFsLibh
#define __INCpassFsLibh

#ifdef __cplusplus
extern "C" {
#endif

/* includes */

#include "ioLib.h"
#include "iosLib.h"

/* typedefs */

typedef struct		  /* Volume descriptor */
    {
    DEV_HDR	passDevHdr;	/* tracks the device: only one passFs device */
    char 	passDevName [MAX_FILENAME_LENGTH]; 
    } VOL_DESC;

typedef struct	/*  pass-through file system file descriptor */
    {
    VOL_DESC *	passVdptr;                    /* VOL_DESC (unused on host)  */
    char        path[MAX_FILENAME_LENGTH];    /* file or directory path     */
    int 	fileFd;			      /* -1 for directory	    */
    void *	dirFd;		      	      /* directory descriptor 	    */	
    int		flags;			      /* open flags		    */
    int		mode;			      /* open permissions (mode)    */
    char *	readCache;		      /* pointer to cache content   */
    int 	cacheOffset;		      /* offset into cache	    */
    int 	cacheBytes;		      /* cache content size	    */
    int 	cacheFileOffset;	      /* cache file offset	    */
    } PASS_FILE_DESC;


/* Function declarations */

#if defined(__STDC__) || defined(__cplusplus)

extern void *passFsDevInit (char *devName);
extern STATUS passFsInit (int passfs, BOOL cacheEnable);
	      
#else	/* __STDC__ */

extern void *passFsDevInit ();
extern STATUS passFsInit ();

#endif	/* __STDC__ */

#ifdef __cplusplus
}
#endif

#endif /* __INCpassFsLibh */
