/* coprocI86.h - coprocessor management library header */

/* Copyright 2003-2004 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*
modification history
--------------------
01a,13apr04,zmm  written based on coprocPpc.h 01c.
*/

#ifndef __INCcoprocI86h
#define __INCcoprocI86h

#ifdef __cplusplus
extern "C" {
#endif

/* includes */

#include "taskLib.h"

/* defines */

#define VX_FP_TASK         VX_COPROC1_TASK

#ifdef __cplusplus
}
#endif

#endif /* __INCcoprocI86h */
