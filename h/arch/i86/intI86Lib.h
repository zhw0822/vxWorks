/* intI86Lib.h - I86-specific interrupt library header file */

/* Copyright 1984-2003 Wind River Systems, Inc. */

/*
modification history
--------------------
01e,12oct04,jb  Add interrupt lockout for automatic stack switch
01d,04dec03,jb  Removing info message
01c,22aug03,to   added inline intLock()/intUnlock().
		 deleted K&R style prototypes.
01b,09nov01,hdn  added intConnectCode offset macros
01a,29aug01,hdn  taken from T31 ver 01b.
*/

#ifndef __INCintI86Libh
#define __INCintI86Libh

#ifdef __cplusplus
extern "C" {
#endif


/* offset macro for stuff in the intConnect code */

#define ICC_INT_BASE            1
#define	ICC_INT_ENT		ICC_INT_BASE + 1	/* intConnectCode[1] */
#define	ICC_BOI_PUSH		ICC_INT_ENT + 7	        /* intConnectCode[8] */
#define	ICC_BOI_PARAM		ICC_BOI_PUSH + 1	/* intConnectCode[9] */
#define	ICC_BOI_ROUTN		ICC_BOI_PARAM + 5	/* intConnectCode[14] */
#define	ICC_INT_PARAM		ICC_BOI_ROUTN + 5	/* intConnectCode[19] */
#define	ICC_INT_ROUTN		ICC_INT_PARAM + 5	/* intConnectCode[24] */
#define	ICC_EOI_PARAM		ICC_INT_ROUTN + 5	/* intConnectCode[29] */
#define	ICC_EOI_CALL		ICC_EOI_PARAM + 4	/* intConnectCode[33] */
#define	ICC_EOI_ROUTN		ICC_EOI_CALL + 1	/* intConnectCode[34] */
#define	ICC_ADD_N		ICC_EOI_ROUTN + 6	/* intConnectCode[40] */
#define	ICC_INT_EXIT		ICC_ADD_N + 5	        /* intConnectCode[45] */


/* inline intLock()/intUnlock() */

#if	defined (__DCC__)
#undef	_WRS_HAS_IL_INT_LOCK
#if 0
#info	"WARNING!! inline intLock() is not implemented for DCC!!"
#endif
#elif	defined (__GNUC__)
#define	_WRS_HAS_IL_INT_LOCK

/*
 * magic numbers used in inline asm:
 * #define EFLAGS_IF 0x200 (from h/arch/i86/regsI86.h)
 */

static __inline__ int __inlineIntLock (void)
    {
    int key;
    __asm__ volatile (
	"pushf; popl %0; andl $0x200,%0; cli"
	: "=g" (key) : /* no input */ );
    return key;
    }

static __inline__ void __inlineIntUnlock (int key)
    {
    __asm__ volatile (
	"testl $0x200,%0; jz 0f; sti;0:"
	: /* no output */ : "g" (key) );
    }
#else
#undef	_WRS_HAS_IL_INT_LOCK
#endif	/* __DCC__ */


/* function declarations */

extern FUNCPTR 	intHandlerCreateI86 (FUNCPTR routine, int parameter,
				     FUNCPTR routineBoi, int parameterBoi,
				     FUNCPTR routineEoi, int parameterEoi);
extern void 	intVecSet2 (FUNCPTR * vector, FUNCPTR function,
			    int idtGate, int idtSelector);
extern void	intVecGet2 (FUNCPTR * vector, FUNCPTR * pFunction, 
			    int * pIdtGate, int * pIdtSelector);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __INCintI86Libh */
