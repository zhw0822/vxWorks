/* archI86.h - Intel 80X86 specific header */

/*
 * Copyright (c) 1984-2005 Wind River Systems, Inc.
 *
 * The right to copy, distribute or otherwise make use of this software
 * may be licensed only pursuant to the terms of an applicable Wind River
 * license agreement. 
*/

/*
modification history
--------------------
01x,28apr05,scm  scm  add _ARCH_SUPPORTS_PROTECT_INTERRUPT_STACK
01w,12apr05,kk   remove _WRS_OBJLIB_SUPPORT_VERIFY_TRAP macro (SPR# 106486)
01v,04nov04,scm  add _WRS_OSM_INIT for new OSM API...
01u,01sep04,jb   Adding _WRS_OBJLIB_SUPPORT_VERIFY_TRAP for objVerify support
01t,14jul04,jb   Enable Portable for all functions
01s,17may04,jb   Switch to PORTABLE scheduler.
                 Fix debug register clear on load and go problem
01r,05may04,zmm  Fix diab compiler warnings.
01s,13may04,zl   fixed _WRS_FRAMEP_FROM_JMP_BUF().
01r,27apr04,cjj  Defined workQLib_PORTABLE
01q,27apr04,jmp  moved INSTR definition to archI86Common.h. added macros for
                 getting frame and return PC from a jmp_buf.
01p,03dec03,md   changed to use portable scheduler
01o,25nov03,pes  Move setting of _BYTE_ORDER macro into archI86Common.h.
01n,14nov03,job  Moved some blib stuff to archI86Common.h
01m,13nov03,pes  Add include of archI86Common.h
01l,27aug03,to   added defines for semLib.
01k,07may03,pes  PAL conditional compilation cleanup. Phase 2.
01j,28apr03,pes  PAL conditional compilation cleanup.
01i,12nov02,hdn  made CR4 initialization only for P5 or later (spr 83992)
01h,29oct02,hdn  assigned reserved1 to (FP_CONTEXT *) (spr 70252)
01g,07oct02,hdn  added the architecture extension of the TCB
01f,13mar01,sn   SPR 73723 - define supported toolchains
01e,04oct01,hdn  enclosed ARCH_REGS_INIT macro with _ASMLANGUAGE
01d,21aug01,hdn  added PENTIUM4's _CACHE_ALIGN_SIZE, ARCH_REGS_INIT
01c,10aug01,hdn  added PENTIUM2/3/4 support
01b,25feb99,hdn  added _CACHE_ALIGN_SIZE for Pentium and PentiumPro
01a,07jun93,hdn  written based on arch68k.h
*/

#ifndef __INCarchI86h
#define __INCarchI86h

#ifdef __cplusplus
extern "C" {
#endif

#define _ARCH_SUPPORTS_GCC

#define	_DYNAMIC_BUS_SIZING	FALSE		/* require alignment for swap */

#if	(CPU==PENTIUM4)
#define _CACHE_ALIGN_SIZE	64
#elif	(CPU==PENTIUM || CPU==PENTIUM2 || CPU==PENTIUM3)
#define _CACHE_ALIGN_SIZE	32
#else
#define _CACHE_ALIGN_SIZE	16
#endif	/* (CPU==PENTIUM || CPU==PENTIUM[234]) */


#ifdef	_ASMLANGUAGE

/* 
 * system startup macros used by sysInit(sysALib.s) and romInit(romInnit.s).
 * - no function calls to outside of BSP is allowed.
 * - no references to the data segment is allowed.
 * the CR4 is introduced in the Pentium processor.
 */

#define ARCH_REGS_INIT							\
	xorl	%eax, %eax;		/* zero EAX */			\
	movl    %cr0, %edx;		/* get CR0 */			\
	andl    $0x7ffafff1, %edx;	/* clear PG, AM, WP, TS, EM, MP */ \
	movl    %edx, %cr0;		/* set CR0 */			\
									\
	pushl	%eax;			/* initialize EFLAGS */		\
	popfl;

#define ARCH_CR4_INIT							\
	xorl	%eax, %eax;		/* zero EAX */			\
	movl	%eax, %cr4;		/* initialize CR4 */


#else

/* architecture extension of the TCB (pTcb->reserved2) */

typedef struct x86Ext		/* architecture specific TCB extension */
    {
    unsigned int	reserved0;	/* (DS_CONFIG *) */
    unsigned int	reserved1;	/* (FP_CONTEXT *) for FPU exception */
    unsigned int	reserved2;
    unsigned int	reserved3;
    unsigned int	reserved4;
    unsigned int	reserved5;
    unsigned int	reserved6;
    unsigned int	reserved7;
    } X86_EXT;

/* no function declarations for makeSymTlb/symTbl.c */

/* macros for getting frame and return PC from a jmp_buf */

#define _WRS_FRAMEP_FROM_JMP_BUF(env)   ((char *) (env)[0].reg.reg_fp)
#define _WRS_RET_PC_FROM_JMP_BUF(env)   ((INSTR *) (env)[0].reg.reg_pc)

#endif	/* _ASMLANGUAGE */

/* PAL additions */

/* architecture-specific overrides for fppSave and fppRestore in fppLib.c */
#if ((CPU == PENTIUM) || (CPU == PENTIUM2) || (CPU == PENTIUM3) || (CPU == PENTIUM4))
#define FPP_SAVE(p)	(*_func_fppSaveRtn)(p)
#define FPP_RESTORE(p)	(*_func_fppRestoreRtn)(p)
#endif /* CPU ... */

/* from h/usb/target/usbIsaLib.h */

/* DMA controller selection */

#define _WRS_DMA_I8237

/* trigger signed IO arguments in sysLib.h */

#define _WRS_SIGNED_IO_ARGS
#define _WRS_SYS_IO

/* override _WRS_SR_SIZE in taskArchLib.h */

#define _WRS_SR_SIZE	UINT

/*
 * trigger definition of portWorkQAdd1 in trgLib.c and
 * rBuffLib.c
 */

#define _WRS_PORT_WORK_Q_ADD1

/* used in cacheLib.c:cacheFuncsSet() */

#define _WRS_EXTERN_WRITEBACK

/* used in fppLib.c to align FP_CONTEXT */

/* dummy context must be in data section for now */
#define _WRS_ALIGN_FP_CONTEXT
#define _WRS_FPP_DUMMY_CONTEXT \
_WRS_DATA_ALIGN_BYTES(_CACHE_ALIGN_SIZE) fppDummyContext = {{{0}}}


/* architecture-specific overrides for fppSave and fppRestore in dbgLib.c */
#define _WRS_SHOW_SSE_REGS
#define FPP_DBG_SAVE(p)		(*_func_fppSaveRtn)(p)
#define FPP_DBG_RESTORE(p)	(*_func_fppRestoreRtn)(p)

/* from loadElfLib.c */

#define _WRS_USE_ELF_LOAD_FORMAT

/* from xdr_float.c */

#define FLOAT_NORM

/* from vmShow.c */

#define _WRS_SHOW_WBACK_STATE
#define _WRS_SHOW_GLOBAL_STATE

/* from taskShow.c */

#define _WRS_SHOW_SSE_REGS

/* Make this architecture use the PORTABLE code set */
#define PORTABLE

/* from rBuffLib.c */

#define _WRS_PORT_WORK_Q_ADD1

/* from bLib.c */

#undef _WRS_BLIB_OPTIMIZED

/* from wdbFpLib.c */

#define _WRS_ALIGN_REG_SET

/* from schedLib.c */

#undef _WRS_SCHEDLIB_OPTIMIZED

#define _WRS_BASE6_SCHEDULER
#define _WRS_ENABLE_IDLE_INTS {intUnlock (-1);}

/* from sem*Lib.c, semA*Lib.s */

#undef _WRS_HAS_OPTIMIZED_SEM
#undef _WRS_USE_OPTIMIZED_SEM		/* XXX use optimized sem, for now */

/* from workQLib.c */

#define workQLib_PORTABLE

/* from windALib.s */
#define schedLib_PORTABLE

/* from sysOsmLib.c */

#define _WRS_OSM_INIT

/* Interrupt stack protection, used in usrDepend.c and usrKernelStack.c */
#define _ARCH_SUPPORTS_PROTECT_INTERRUPT_STACK
                                                                                            
/* if OSM support not available for stack protection,
 *  * then support can not be available for interrupt
 *   * stack protection.
 *    */
#if !defined(_WRS_OSM_INIT)
#undef _ARCH_SUPPORTS_PROTECT_INTERRUPT_STACK
#endif

#include "arch/i86/archI86Common.h"

/* end of PAL additions */
#ifdef __cplusplus
}
#endif

#endif /* __INCarchI86h */
