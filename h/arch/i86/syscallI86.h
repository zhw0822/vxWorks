/* syscallI86.h - I86 specific System Call Infrastructure header */

/* Copyright 1984-2003 Wind River Systems, Inc. */

/*
modification history
--------------------
01d,05may05,zmm  Fix SYSCALL_ENTRY_FRAME_SIZE. SPR 108825
01c,02feb04,jb  Fix SPR 93500
01b,04jan04,jb  Adding context to syscall_entry_state
01a,18sep03,jb   written.
*/

/*
DESCRIPTION
This header contains I86-specific definitions and constants used by
the System Call Infrastructure library.

*/

#ifndef __INCsyscallI86h
#define __INCsyscallI86h

#ifndef _ASMLANGUAGE
#include "taskLib.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#define syscallDispatch_PORTABLE	/* Use the portable dispatcher */

    /* defines */

#define SYSCALL_ENTRY_FRAME_SIZE	68

#define SYSCALL_STACK_ARGS          8

#ifndef _ASMLANGUAGE


/* The call gate/Tss mechanism does the copy for us, this is not needed */

#define _SYSCALL_USER_SP_CHECK(sp)

    /* typedefs */

    /*
     * The SYSCALL_ENTRY_STATE structure defines the saved machine state
     * when the system call trap is taken. This information is architecture
     * specific, and is used by the system call dispatcher to restart system
     * calls that have been interrupted by the occurance of signals.
     * System call restart is achieved by restoring the saved state at the
     * time of the system call trap, and re-issuing the system call.
     *
     * All arguments to the system call are saved on the kernel stack, and 
     * the address of that array is passed as an argument to the dispatcher.
     * The layout of this structure must exactly match the ordering of members
     * of the system call entry frame in src/arch/I86/syscallALib.s.
     *
     */

typedef struct syscall_entry_state
    {
    int     ebx;            /* Save reg */
    int     ecx;            /* Save reg */
    int     edx;            /* Save reg */
    void *  pc;             /* return address instruction pointer */
    int     cs;             /* return address code segment */
    int     scn;            /* System Call Number - Also temp error storage */
    int     upc;            /* return to caller pc */
    int     args[8];        /* argument list  */
    int  *  pUStack;        /* user-mode stack pointer */
    int     ss;             /* user stack segment */
    } SYSCALL_ENTRY_STATE;


/* Prototype */
UINT32 sysModGdt (UINT32 offset, UINT32 limit, UINT32 base, UINT32 priority);


#endif /* _ASMLANGUAGE */

#ifdef __cplusplus
}
#endif

#endif /* __INCsyscallI86h */
