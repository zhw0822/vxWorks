/* archI86Common.h - common I86 architecture specific header */

/* Copyright 2003-2004 Wind River Systems, Inc. */

/*
modification history
--------------------
01g,13may04,zl   enabled user trcLib support.
01f,27apr04,jmp  added support for portable trcLib. moved INSTR definition
                 form archI86.h.
01e,13apr04,zmm  Add coprocessor support for Pentium.
01d,25nov03,pes  Move setting of _BYTE_ORDER macro into archI86Common.h
01c,17nov03,jb   Adding defines for syscalls
01b,12nov03,job  Moved some blib stuff from archI86.h
01a,11nov03,pes  written.
*/

#ifndef __INCarchI86Commonh
#define __INCarchI86Commonh

#ifdef __cplusplus
extern "C" {
#endif

/* Common syscall defines */

#define SYSCALL_SEGMENT             8
#define SYSCALL_GATE_OFFSET         67      /* (SYSCALL_SEGMENT * sizeof(GDT) | (DPL3 > 13) ) */

/* from bLib.c */

#define _WRS_BLIB_ALIGNMENT     1       /* 2 byte alignment mask */
#define swab_PORTABLE

#define	_BYTE_ORDER		_LITTLE_ENDIAN

#define _WRS_PAL_COPROC_LIB             /* enable coprocessor abstraction */ 

/* Enable Portable trcLib */

#define trcLib_PORTABLE
#define _WRS_TRC_ARGS_COUNT     5       /* number of arguments to print in */
                                        /* stack trace */

#ifndef _ASMLANGUAGE
typedef unsigned char INSTR;		/* char instructions */
#endif  /* _ASMLANGUAGE */


#ifdef __cplusplus
}
#endif

#endif /* __INCarchI86Commonh */




