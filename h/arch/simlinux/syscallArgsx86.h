/* Aligned for x86 - no alignment needed */


/* THIS FILE IS AUTO_GENERATED. PLEASE DO NOT EDIT. */

#ifndef _ASMLANGUAGE

#include <vxWorks.h>
#include <taskLib.h>
#include <rtpLib.h>
#include <sdLib.h>
#include <shlLib.h>
#include <signal.h>
#include <socket.h>
#include <time.h>
#include <setjmp.h>
#include <resolv/nameser.h>
#include <resolv/resolv.h>
#include <private/semSysCall.h>
#include <private/objLibP.h>
#include <private/rtpLibP.h>
#include <private/mqPxSysCall.h>
#include <private/pxObjSysCall.h>
#include <private/semPxSysCall.h>
#include <private/taskSysCall.h>
#include <private/timerSysCall.h>
#include <private/eventSysCall.h>
#include <sys/resource.h>

struct _exitScArgs 
	{
	int status;
	};

struct creatScArgs 
	{
	const char * name;
	mode_t flag;
	};

struct _openScArgs 
	{
	const char * name;
	int flags;
	int mode;
	};

struct closeScArgs 
	{
	int fd;
	};

struct readScArgs 
	{
	int fd;
	void * buffer;
	size_t maxbytes;
	};

struct writeScArgs 
	{
	int fd;
	const void * buffer;
	size_t nbytes;
	};

struct _ioctlScArgs 
	{
	int fd;
	int function;
	int arg;
	};

struct dupScArgs 
	{
	int fd;
	};

struct dup2ScArgs 
	{
	int fd;
	int fd2;
	};

struct pipeScArgs 
	{
	int filedes[2];
	};

struct removeScArgs 
	{
	const char * name;
	};

struct selectScArgs 
	{
	int width;
	fd_set * readFds;
	fd_set * writeFds;
	fd_set * excFds;
	struct timeval * timeout;
	};

struct socketScArgs 
	{
	int domain;
	int type;
	int protocol;
	};

struct bindScArgs 
	{
	int s;
	struct sockaddr * name;
	int namelen;
	};

struct listenScArgs 
	{
	int s;
	int backlog;
	};

struct acceptScArgs 
	{
	int s;
	struct sockaddr * addr;
	int *addrlen;
	};

struct connectScArgs 
	{
	int s;
	struct sockaddr * name;
	int namelen;
	};

struct sendtoScArgs 
	{
	int s;
	caddr_t buf;
	int bufLen;
	int flags;
	struct sockaddr * to;
	int tolen;
	};

struct sendScArgs 
	{
	int s;
	const char * buf;
	int bufLen;
	int flags;
	};

struct sendmsgScArgs 
	{
	int s;
	struct msghdr * mp;
	int flags;
	};

struct recvfromScArgs 
	{
	int s;
	char * buf;
	int bufLen;
	int flags;
	struct sockaddr *from;
	int * pFromLen;
	};

struct recvScArgs 
	{
	int s;
	char * buf;
	int bufLen;
	int flags;
	};

struct recvmsgScArgs 
	{
	int s;
	struct msghdr * mp;
	int flags;
	};

struct setsockoptScArgs 
	{
	int s;
	int level;
	int optname;
	char * optval;
	int optlen;
	};

struct getsockoptScArgs 
	{
	int s;
	int level;
	int optname;
	char * optval;
	int * optlen;
	};

struct getsocknameScArgs 
	{
	int s;
	struct sockaddr *name;
	int * namelen;
	};

struct getpeernameScArgs 
	{
	int s;
	struct sockaddr *name;
	int *namelen;
	};

struct shutdownScArgs 
	{
	int s;
	int how;
	};

struct mmapScArgs 
	{
	void * addr;
	size_t len;
	int prot;
	int flags;
	int fildes;
	off_t64 off;
	};

struct munmapScArgs 
	{
	void * addr;
	size_t len;
	};

struct mprotectScArgs 
	{
	void * addr;
	size_t len;
	int prot;
	};

struct killScArgs 
	{
	OBJ_HANDLE rtpId;
	int signo;
	};

struct sigpendingScArgs 
	{
	sigset_t * pSet;
	};

struct sigprocmaskScArgs 
	{
	int how;
	const sigset_t * pSet;
	sigset_t * pOset;
	};

struct _sigqueueScArgs 
	{
	OBJ_HANDLE rtpId;
	int signo;
	const union sigval * pValue;
	int sigCode;
	};

struct sigsuspendScArgs 
	{
	const sigset_t * pSet;
	};

struct sigtimedwaitScArgs 
	{
	const sigset_t * pSet;
	struct siginfo * pInfo;
	const struct timespec * pTime;
	};

struct _sigactionScArgs 
	{
	int signo;
	const struct sigaction * pAct;
	struct sigaction * pOact;
	void * retAddr;
	};

struct chdirScArgs 
	{
	const char * name;
	};

struct _getcwdScArgs 
	{
	char * buffer;
	int length;
	};

struct waitpidScArgs 
	{
	OBJ_HANDLE childRtpId;
	int * pStatus;
	int options;
	};

struct sysctlScArgs 
	{
	int *pName;
	u_int nameLen;
	void * pOld;
	size_t * pOldLen;
	void * pNew;
	size_t newLen;
	};

struct _schedPxInfoGetScArgs 
	{
	BOOL * pTimeSlicingOn;
	ULONG * pTimeSlicePeriod;
	};

struct sigaltstackScArgs 
	{
	stack_t * ss;
	stack_t * oss;
	};

struct unlinkScArgs 
	{
	const char * name;
	};

struct linkScArgs 
	{
	const char * name;
	const char * newname;
	};

struct fsyncScArgs 
	{
	int fd;
	};

struct fdatasyncScArgs 
	{
	int fd;
	};

struct renameScArgs 
	{
	const char * oldname;
	const char * newname;
	};

struct fpathconfScArgs 
	{
	int fd;
	int name;
	};

struct pathconfScArgs 
	{
	const char * path;
	int name;
	};

struct accessScArgs 
	{
	const char * path;
	int amode;
	};

struct chmodScArgs 
	{
	const char * path;
	mode_t mode;
	};

struct eventReceiveScArgs 
	{
	UINT32 events;
	UINT32 options;
	int timeout;
	UINT32 * pEvtsReceived;
	};

struct eventSendScArgs 
	{
	int taskId;
	UINT32 events;
	};

struct eventCtlScArgs 
	{
	OBJ_HANDLE handle;
	VX_EVT_CTL_CMD command;
	void * pArg;
	UINT * pArgSize;
	};

struct msgQSendScArgs 
	{
	OBJ_HANDLE handle;
	char * buffer;
	UINT nBytes;
	int timeout;
	int priority;
	};

struct msgQReceiveScArgs 
	{
	OBJ_HANDLE handle;
	char * buffer;
	UINT maxNBytes;
	int timeout;
	};

struct _msgQOpenScArgs 
	{
	const char * name;
	UINT maxMsgs;
	UINT maxMsgLength;
	int options;
	int mode;
	void * context;
	};

struct objDeleteScArgs 
	{
	OBJ_HANDLE handle;
	int options;
	};

struct objInfoGetScArgs 
	{
	OBJ_HANDLE handle;
	void * pInfo;
	UINT * pInfoSize;
	int level;
	};

struct _semTakeScArgs 
	{
	OBJ_HANDLE handle;
	int timeout;
	};

struct _semGiveScArgs 
	{
	OBJ_HANDLE handle;
	};

struct _semOpenScArgs 
	{
	const char * name;
	SEM_TYPE type;
	int initState;
	int options;
	int mode;
	void * context;
	};

struct semCtlScArgs 
	{
	OBJ_HANDLE handle;
	VX_SEM_CTL_CMD command;
	void * pArg;
	UINT * pArgSize;
	};

struct _taskOpenScArgs 
	{
	struct vx_task_open_sc_args * pArgs;
	};

struct taskCtlScArgs 
	{
	int tid;
	VX_TASK_CTL_CMD command;
	void * pArg;
	UINT * pArgSize;
	};

struct taskDelayScArgs 
	{
	int ticks;
	};

struct rtpSpawnScArgs 
	{
	const char * rtpFileName;
	const char ** argv;
	const char ** envp;
	int priority;
	int uStackSize;
	int options;
	int taskOptions;
	};

struct rtpInfoGetScArgs 
	{
	OBJ_HANDLE rtpId;
	RTP_DESC * rtpStruct;
	};

struct taskKillScArgs 
	{
	OBJ_HANDLE taskHandle;
	int signo;
	};

struct _taskSigqueueScArgs 
	{
	OBJ_HANDLE taskHandle;
	int signo;
	const union sigval * pValue;
	int sigCode;
	};

struct _timer_openScArgs 
	{
	const char * name;
	int mode;
	clockid_t clockId;
	struct sigevent * evp;
	void * context;
	};

struct timerCtlScArgs 
	{
	TIMER_CTL_CMD cmdCode;
	int handle;
	void * pArgs;
	UINT * pArgSize;
	};

struct pxOpenScArgs 
	{
	PX_OBJ_TYPE type;
	const char * name;
	int objOpenMode;
	void * attr;
	};

struct pxCloseScArgs 
	{
	OBJ_HANDLE handle;
	};

struct pxUnlinkScArgs 
	{
	PX_OBJ_TYPE type;
	const char * name;
	};

struct pxCtlScArgs 
	{
	OBJ_HANDLE handle;
	PX_CTL_CMD_CODE cmdCode;
	void * pArgs;
	UINT * pArgSize;
	};

struct pxMqReceiveScArgs 
	{
	OBJ_HANDLE handle;
	char * pMsg;
	size_t msgLen;
	unsigned int * pMsgPrio;
	PX_WAIT_OPTION waitOption;
	struct timespec * time_out;
	};

struct pxMqSendScArgs 
	{
	OBJ_HANDLE handle;
	const char * pMsg;
	size_t msgLen;
	unsigned int msgPrio;
	PX_WAIT_OPTION waitOption;
	struct timespec * time_out;
	};

struct pxSemWaitScArgs 
	{
	OBJ_HANDLE handle;
	PX_WAIT_OPTION waitOption;
	struct timespec * time_out;
	};

struct pxSemPostScArgs 
	{
	OBJ_HANDLE handle;
	};

struct pipeDevCreateScArgs 
	{
	const char * name;
	int nMessages;
	int nBytes;
	};

struct pipeDevDeleteScArgs 
	{
	const char * name;
	BOOL force;
	};

struct _sdCreateScArgs 
	{
	char * name;
	int options;
	UINT32 size;
	off_t64 physAddress;
	MMU_ATTR attr;
	void ** pVirtAddress;
	};

struct _sdOpenScArgs 
	{
	char * name;
	int options;
	int mode;
	UINT32 size;
	off_t64 physAddress;
	MMU_ATTR attr;
	void ** pVirtAddress;
	};

struct sdDeleteScArgs 
	{
	OBJ_HANDLE handle;
	int options;
	};

struct sdMapScArgs 
	{
	OBJ_HANDLE handle;
	MMU_ATTR attr;
	int options;
	};

struct sdUnmapScArgs 
	{
	OBJ_HANDLE handle;
	int options;
	};

struct sdProtectScArgs 
	{
	OBJ_HANDLE handle;
	MMU_ATTR attr;
	};

struct _edrErrorInjectScArgs 
	{
	int kind;
	const char *fileName;
	int lineNumber;
	REG_SET *regset;
	void *addr;
	const char *msg;
	};

struct resSearchNScArgs 
	{
	const char * name;
	struct res_target * target;
	};

struct wvEventScArgs 
	{
	int eventId;
	const char * pData;
	unsigned int dataSize;
	};

struct rtpVarAddScArgs 
	{
	void ** pVar;
	void * value;
	};

struct sdInfoGetScArgs 
	{
	OBJ_HANDLE handle;
	SD_DESC * pSdStruct;
	};

struct _shlOpenScArgs 
	{
	const char * name;
	int options;
	};

struct _shlUnlockScArgs 
	{
	SHL_ID shlId;
	};

struct _shlCloseScArgs 
	{
	SHL_ID shlId;
	};

struct _shlGetScArgs 
	{
	SHL_ID shlId;
	SHLINFO *pInfo;
	};

struct _shlPutScArgs 
	{
	SHL_ID shlId;
	const SHLINFO *pInfo;
	};

struct objUnlinkScArgs 
	{
	const char * name;
	enum windObjClassType classType;
	};

struct getprlimitScArgs 
	{
	int idtype;
	OBJ_HANDLE handle;
	int resource;
	struct rlimit *rlp;
	};

struct setprlimitScArgs 
	{
	int idtype;
	OBJ_HANDLE handle;
	int resource;
	struct rlimit *rlp;
	};

struct _mctlScArgs 
	{
	void * addr;
	size_t len;
	int function;
	int arg;
	};

#endif	/* _ASMLANGUAGE */
