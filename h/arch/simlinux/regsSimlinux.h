/* regsSimnt.h - simnt registers header */

/* Copyright 1995-2003 Wind River Systems, Inc. */

/*
modification history
--------------------
01l,31aug04,jmp  made SIMLINUX REG_SET indentical to SIMNT.
01k,01dec03,jmp  added mmuTransTbl (current MMU translation table) to REG_SET.
01j,24nov03,jeg  added macros to manage supervisor bit
01i,14nov03,jmp  added SIM_STATUS_USR.
01h,18sep03,jeg  removed SIM_STATUS_INIT macro.
01g,12sep03,jeg  updated REG_SET structure to split intLockKey by a status bit
                 field. Removed the WIND_TCB_* macros.
01f,27may03,jmp  added Register offsets in TCB.
01e,26mar03,jmp  replaced INSTR by UCHAR to use this file from host side.
01d,17mar03,jmp  renamed macros REG_* in REG_SET_*.
01c,16jan03,elp  added REGS defines (SPR #84278).
01b,29apr98,cym  added intLockKey
01a,27aug97,cym  written
*/

#ifndef __INCregsSimnth
#define __INCregsSimnth

#ifdef __cplusplus
extern "C" {
#endif

#ifndef _ASMLANGUAGE

#define GREG_NUM	8

typedef struct		/* REG_SET - simnt register set (based on x86)	*/
    {
    ULONG	edi;		/* general register			*/
    ULONG	esi;		/* general register			*/
    ULONG	ebp;		/* general register			*/
    ULONG	esp;		/* general register			*/
    ULONG	ebx;		/* general register			*/
    ULONG	edx;		/* general register			*/
    ULONG	ecx;		/* frame pointer register		*/
    ULONG	eax;		/* stack pointer register		*/
    ULONG	eflags;		/* status register			*/
    UCHAR *	pc;		/* program counter			*/
    ULONG	reg_status;	/* status bit field			*/
    UINT32 	mmuTransTbl;	/* current MMU translation table	*/
    char	unused[0xc8];	/* unused - keep for SIMNT backward	*/
				/* compatibility with T2.x releases	*/
    } REG_SET;

#undef pc
#undef spReg
#define spReg			esp
#define fpReg			ebp
#define reg_pc			pc
#define reg_sp			spReg
#define reg_fp			fpReg

#define G_REG_BASE		0x00
#define G_REG_OFFSET(n)		(G_REG_BASE + (n) * sizeof(ULONG))
#define SR_OFFSET		G_REG_OFFSET(GREG_NUM)
#define PC_OFFSET		(SR_OFFSET + sizeof(ULONG))
#endif  /* _ASMLANGUAGE */

/* Register offsets */

#define REG_SET_EDI		0x00
#define REG_SET_ESI		0x04
#define REG_SET_EBP		0x08
#define REG_SET_ESP		0x0c
#define REG_SET_EBX		0x10
#define REG_SET_EDX		0x14
#define REG_SET_ECX		0x18
#define REG_SET_EAX		0x1c
#define REG_SET_EFLAGS		0x20
#define REG_SET_PC		0x24
#define REG_SET_SIMSTATUS	0x28
#define REG_SET_MMU_TRANS_TBL	0x2c

/* status bit fields */

#define SIM_STATUS_INTLOCK	0x0001	/* interrupt locked	*/
#define SIM_STATUS_INTUNLOCK	0x0000	/* interrupt unlocked	*/
#define SIM_STATUS_SUP		0x0002	/* supervisor mode	*/
#define SIM_STATUS_USR		0x0000	/* user mode		*/

/* macros to access interrupt status field */

#define VXSIM_INT_MASK_SET(vxsimStatus) \
    (vxsimStatus |= SIM_STATUS_INTLOCK)

#define VXSIM_INT_MASK_CLEAR(vxsimStatus) \
    (vxsimStatus &= ~(SIM_STATUS_INTLOCK))
    
#define VXSIM_INT_MASK_GET(vxsimStatus) \
    (((vxsimStatus & SIM_STATUS_INTLOCK) == 0) ? FALSE : TRUE)

#define VXSIM_SUP_MODE_SET(vxsimStatus) \
    (vxsimStatus |= SIM_STATUS_SUP)

#define VXSIM_USR_MODE_SET(vxsimStatus) \
    (vxsimStatus &= ~(SIM_STATUS_SUP))
    
#define VXSIM_SUP_MODE_GET(vxsimStatus) \
    (((vxsimStatus & SIM_STATUS_SUP) == 0) ? FALSE : TRUE)

#ifdef __cplusplus
}
#endif

#endif /* __INCregsSimnth */
