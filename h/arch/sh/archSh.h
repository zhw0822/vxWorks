/* archSh.h - Renesas SH specific header */

/* 
 * Copyright (c) 1994-2005 Wind River Systems, Inc. 
 *
 * The right to copy, distribute, modify or otherwise make use 
 * of this software may be licensed only pursuant to the terms 
 * of an applicable Wind River license agreement. 
 */

/*
modification history
--------------------
02f,29apr05,h_k  disabled _WRS_SCHEDLIB_OPTIMIZED and enabled
                 schedLib_PORTABLE. (SPR #107854, #108678).
02e,12apr05,kk   remove _WRS_OBJLIB_SUPPORT_VERIFY_TRAP macro (SPR# 106486)
02d,02feb05,h_k  enabled _ARCH_SUPPORTS_GCC. (SPR #106087)
02c,09sep04,h_k  added _WRS_OBJLIB_SUPPORT_VERIFY_TRAP. (SPR #99844)
02b,12may04,h_k  eliminated _WRS_TEXT_REGION, _WRS_MEM_BASE and
                 _WRS_ADJUST_TEXT_START macros. (SPR #97280)
                 deleted the obsolete SH7729 CPU type.
02a,29apr04,h_k  added SH_WARM_OFFSET.
01z,27apr04,cjj  Defined workQLib_PORTABLE
01y,22mar04,zl   moved trcLib_PORTABLE and INSTR to archShCommon.h; 
		 added _WRS_FRAMEP_FROM_JMP_BUF and _WRS_RET_PC_FROM_JMP_BUF.
01x,19feb04,h_k  defined trcLib_PORTABLE.
01w,14nov03,job  Moved some blib stuff to archShCommon.h
		 Added missing include of archShCommon.h
01v,13nov03,pes  Add include of archShCommon.h
01u,14nov03,job  Moved some blib stuff to archShCommon.h
		 Added missing include of archShCommon.h
01t,13nov03,pes  Add include of archShCommon.h
01s,29oct03,sn   gcc 3.3.2 does not currently support arm or sh
01r,28oct03,pcm  set portable semaphores as default so as not to break the build
01q,10sep03,pcm  enabled base6 scheduler
01p,27aug03,to   added defines for semLib.
01o,07may03,pes  PAL conditional compilation cleanups. Phase 2.
01n,24apr03,pes  CPU conditional compilation cleanups.
01m,05dec02,hk   moved _WRS_HW_FP_SUPPORT from fppShLib.h to archSh.h (SPR#83145)
01l,13mar01,sn   SPR 73723 - define supported toolchains
01k,23aug00,hk   change to use default _CACHE_ALIGN_SIZE for SH7040.
01j,04jun99,zl   hitachi SH4 architecture port, provided by Highlander
		 Engineering. Added include of toolSh.h
01i,05mar99,hk   merged CPU conditional for _CACHE_ALIGN_SIZE.
01h,14sep98,st   added _CACHE_ALIGN_SIZE for SH7750.
01g,22apr97,hk   changed 704X to 7040.
01f,21dec95,hk   added _CACHE_ALIGN_SIZE for SH704X.
01e,16aug95,hk   restored _ARCH_MULTIPLE_CACHELIB.
01d,05jul95,hk   deleted _ARCH_MULTIPLE_CACHELIB.
01c,01jun95,hk   added _ARCH_MULTIPLE_CACHELIB. copyright to '95.
01b,23aug94,sa   added '#undef _DYNAMIC_BUS_SIZING'.
01a,15jun94,hk   written.
*/

#ifndef __INCarchShh
#define __INCarchShh

#ifdef __cplusplus
extern "C" {
#endif

#define _ARCH_SUPPORTS_GCC
#define _ARCH_SUPPORTS_DCC

#define	_ARCH_MULTIPLE_CACHELIB	TRUE		/* multiple cache libraries */

#if (CPU==SH7750)
#define _CACHE_ALIGN_SIZE	32		/* change default in vxArch.h */
#endif

#ifdef  _DYNAMIC_BUS_SIZING
#undef  _DYNAMIC_BUS_SIZING
#endif
#define	_DYNAMIC_BUS_SIZING	FALSE		/* require alignment for swap */

/* 
 * FPU support only for SH-4. SH-3 archives are built with no SH-3E support.
 * For SH-3E some target/src/arch/sh/... files need to be rebuilt with
 * _WRS_HW_FP_SUPPORT defined (e.g. fppALib.s, fppArchLib.c, excALib.s and
 * excArchLib.c).
 */

/* in fppShow.c, re-type _WRS_FLOAT_TYPE as 'float' */
#define _WRS_FLOAT_TYPE	float

#if (CPU==SH7750)
#define _WRS_HW_FP_SUPPORT
#endif

/* PAL Additions */

#if	(CPU==SH7750 || CPU==SH7700)
#define SM_ANCHOR_OFFSET        0x1600	/* not used for SDRAM config. */
#define BOOT_LINE_OFFSET        0x1700
#define EXC_MSG_OFFSET          0x1800


/* This macro is used in kernelLib.c.
 * If mmu is enabled, emulated SH7700 interrupt stack needs to be relocated
 * on a fixed physical address space (P1/P2).  If mmu is disabled, it is
 * also possible to put the interrupt stack on copy-back cache (P0/P3).
 * Note that cache flush is necessary, since bfill() to load 0xee might be done
 * on copy-back cache and we may use the area from its behind.
 */
#define _WRS_INT_STACK_ADJUST \
    { \
    CACHE_DRV_FLUSH (&cacheLib, vxIntStackEnd, (int) intStackSize);\
    vxIntStackBase = (char *)(((UINT32)vxIntStackBase  & 0x1fffffff)\
			    | ((UINT32)intVecBaseGet() & 0xe0000000));\
\
    vxIntStackEnd  = (char *)(((UINT32)vxIntStackEnd   & 0x1fffffff)\
			    | ((UINT32)intVecBaseGet() & 0xe0000000));\
    }

/* warm start offset */
#define SH_WARM_OFFSET		0x8	/* refered by arch.cdf */
#else	/* CPU==SH7600 */
/* warm start offset */
#define SH_WARM_OFFSET		0x18	/* refered by arch.cdf */
#endif  /* (CPU==SH7750 || CPU==SH7700) */

/* replace IF_DEQUEUEIF macro in if.h */
#define	IF_DEQUEUEIF(ifq, m, ifp) { \
	(m) = (ifq)->ifq_head; \
	if (m) { \
		if (((ifq)->ifq_head = (m)->m_act) == 0) \
			(ifq)->ifq_tail = 0; \
		(m)->m_act = 0; \
		(ifq)->ifq_len--; \
		(ifp) = (struct ifnet *)( \
			(*((u_short *)((int)(m)+(m)->m_off+0)) << 16) | \
			(*((u_short *)((int)(m)+(m)->m_off+2)) ) ); \
		IF_ADJ(m); \
	} \
}

#if (CPU==SH7700)
/* override the standard definition of the CACHE_TYPE enum to add
 * support for WAY2 and WAY3 caches.
 */
#define _WRS_CACHE_TYPE

#define	_INSTRUCTION_CACHE 	0	/* Instruction Cache(s) */
#define	_DATA_CACHE		1	/* Data Cache(s) */

#define _WAY2_CACHE 3
#define _WAY3_CACHE 4

#ifndef _ASMLANGUAGE
typedef enum				/* CACHE_TYPE */
    {
    INSTRUCTION_CACHE = _INSTRUCTION_CACHE,
    DATA_CACHE = _DATA_CACHE,
    WAY2_CACHE = _WAY2_CACHE,		/* currently used in SH7700 only */
    WAY3_CACHE = _WAY3_CACHE		/* currently used in SH7700 only */
    } CACHE_TYPE;
#endif /* _ASMLANGUAGE */
#endif /* CPU==SH7700 */


/* moved here from ieeefp.h */

#ifdef __SH3E__
#define _DOUBLE_IS_32BITS
#endif

/* from ffsLib.c */
#undef	PORTABLE
#undef	ffsLib_PORTABLE

/* from memPartLib.c */
#define _WRS_NEED_ALIGNED_HEADER

/* from sigLib.c. Triggers inclusion of code which sends a SIGBUS signal if
 * no entry is found in the signal table.
 */
#define _WRS_SIG_EXCBERRVECNUM

/* from dspLib.c */

#define _WRS_EARLY_DSP_INIT

/* in dbgLib.c */

#define _WRS_LABEL_STRING	"               %s:\n"

/* from loadElfLib.c */

#define _WRS_USE_ELF_LOAD_FORMAT

/* from xdr_float.c */

#define FLOAT_NORM

/* from etherLib.c */

#define _WRS_COPY_ETHERHDR_BY_INTS

/* from bLib.c */

#define _WRS_BLIB_OPTIMIZED

/* from schedLib.c */

#undef	_WRS_SCHEDLIB_OPTIMIZED
#define	schedLib_PORTABLE

#define _WRS_VXLIB_SUPPORT

#define	_WRS_BASE6_SCHEDULER
#define _WRS_ENABLE_IDLE_INTS			\
	{					\
	intLevelSet (0);			\
	if (_func_vxIdleLoopHook != NULL)	\
	    (* _func_vxIdleLoopHook) ();	\
	}

/* from sem*Lib.c, semA*Lib.s */

#define _WRS_HAS_OPTIMIZED_SEM
#define _WRS_USE_OPTIMIZED_SEM
#undef	_WRS_USE_OPTIMIZED_SEM		/* Use portable semaphores for now */

/* from workQLib.c */

#define workQLib_PORTABLE

/* macros for getting frame and return PC from a jmp_buf */

#define _WRS_FRAMEP_FROM_JMP_BUF(env)	((char *) (env)[0].reg.spReg)
#define _WRS_RET_PC_FROM_JMP_BUF(env)	((env)[0].reg.pc)

/* end of PAL additions */

#include "toolSh.h"

#include "arch/sh/archShCommon.h"

#ifdef __cplusplus
}
#endif

#endif /* __INCarchShh */
