/* sh7750.h - SH7750 header */

/* Copyright 1998-2002 Wind River Systems, Inc. */

/*
modification history
--------------------
01k,11jun02,zl   fixed BSC definitions.
01j,07dec01,zl   added BSC register definitions.
01i,10sep01,h_k  added power control support (SPR #69838).
01h,16feb01,hk   delete UBC register field defs (use defs in dbgShLib.h).
01g,15dec00,frf  corrected Timer Control Register bit definitions.
01f,21nov00,zl   renamed TMU registers.
01e,26jul99,zl   added UBC register field definitions
01d,10mar99,hk   moved in UBC register address defs from dbgShLib.h.
01c,26nov98,hk   changed SCIF register prefix. added SCIF_SFDR2, SCIF_SSR2 defs.
01b,22sep98,hms  changed expression of "SCI1, SCI2" to "SCI, SCIF".
01a,02jul98,st   written based on sh7700.h 01h.
*/

/*
DESCRIPTION
This file contains I/O addresses and related constants for the SH7750 chip.
*/

#ifndef __INCsh7750h
#define __INCsh7750h

#ifdef __cplusplus
extern "C" {
#endif


/* INTC (INTerrupt Controller) register addresses */

#define INTC_ICR	((volatile UINT16 *)0xffd00000)	/* control register */
#define INTC_IPRA	((volatile UINT16 *)0xffd00004)	/* priority setup A */
#define INTC_IPRB	((volatile UINT16 *)0xffd00008)	/* priority setup B */
#define INTC_IPRC	((volatile UINT16 *)0xffd0000c)	/* priority setup C */

/* INTC ICR (Interrupt Control Register) bit definitions */

#define ICR_NMI_STATUS		((UINT16)0x8000)   /* NMI input level status  */
#define	ICR_NMI_BLOCK_MODE	((UINT16)0x0200)   /* NMI block mode 	      */
#define ICR_NMI_LEAD_EDGE	((UINT16)0x0100)   /* detect on leading edge  */
#define ICR_NMI_TRAIL_EDGE	((UINT16)0x0000)   /* detect on trailing edge */
#define ICR_NMI_IRL_PIN_MODE	((UINT16)0x0080)   /* IRL pin mode            */

/* FRQCR (FReQuency Control Register) */

#define FRQCR		((volatile UINT16 *)0xffc00000)

/* FRQCR (FReQuency Control Register) bit definitions */

#define FRQCR_DEFAULT		((UINT16)0x0812)    /* for 120MHz */
#define FRQCR_CKOEN		((UINT16)0x0800)
#define FRQCR_PLL1EN		((UINT16)0x0400)
#define FRQCR_PLL2EN		((UINT16)0x0200)
#define	FRQCR_IFC_BY_8		((UINT16)0x0140)
#define	FRQCR_IFC_BY_6		((UINT16)0x0100)
#define	FRQCR_IFC_BY_4		((UINT16)0x00c0)
#define	FRQCR_IFC_BY_3		((UINT16)0x0080)
#define	FRQCR_IFC_BY_2		((UINT16)0x0040)
#define	FRQCR_IFC_BY_1		((UINT16)0x0000)
#define	FRQCR_BFC_BY_8		((UINT16)0x0028)
#define	FRQCR_BFC_BY_6		((UINT16)0x0020)
#define	FRQCR_BFC_BY_4		((UINT16)0x0018)
#define	FRQCR_BFC_BY_3		((UINT16)0x0010)
#define	FRQCR_BFC_BY_2		((UINT16)0x0008)
#define	FRQCR_BFC_BY_1		((UINT16)0x0000)
#define	FRQCR_PFC_BY_8		((UINT16)0x0004)
#define	FRQCR_PFC_BY_6		((UINT16)0x0003)
#define	FRQCR_PFC_BY_4		((UINT16)0x0002)
#define	FRQCR_PFC_BY_3		((UINT16)0x0001)
#define	FRQCR_PFC_BY_2		((UINT16)0x0000)


/* STBCR (Stanby Control Register) */

#define STBCR		((volatile UINT8  *)0xffc00004)
#define STBCR2		((volatile UINT8  *)0xffc00010)


/* WDT (WatchDog Timer) register addresses */

#define WDT_WTCNT_WR	((volatile UINT16 *)0xffc00008)
#define WDT_WTCNT_RD	((volatile UINT8  *)0xffc00008)
#define WDT_WTCSR_WR	((volatile UINT16 *)0xffc0000c)
#define WDT_WTCSR_RD	((volatile UINT8  *)0xffc0000c)

/* WDT WTCNT (Watchdog Timer CouNT) register bit definitions */

#define WTCNT_PASSWD		0x5a00	/* WTCNT write password */

/* WDT WTCSR (Watchdog Timer Control/Status Register) bit definitions */

#define WTCSR_PASSWD		0xa500	/* WTCSR write password */

#define WTCSR_TIMER_DISABLE	0x00	/* stop count */
#if 0	/* definition not used */
#define WTCSR_TIMER_ENABLE	0x80	/* start count */
#define WTCSR_WATCHDOG_MODE	0x40	/* operate as watchdog timer */
#define WTCSR_RSTS_MANUAL	0x20	/* select manual-reset watchdog */
#define WTCSR_WOVF_STATUS	0x10	/* overflow status in watchdog mode */
#define WTCSR_IOVF_STATUS	0x08	/* overflow status in interval mode */
#define WTCSR_PCLK_BY4096	0x07	/* count-up by P-clock/4096 */
#define WTCSR_PCLK_BY1024	0x06	/* count-up by P-clock/1024 */
#define WTCSR_PCLK_BY256	0x05	/* count-up by P-clock/256 */
#define WTCSR_PCLK_BY64		0x04	/* count-up by P-clock/64 */
#define WTCSR_PCLK_BY32		0x03	/* count-up by P-clock/32 */
#define WTCSR_PCLK_BY16		0x02	/* count-up by P-clock/16 */
#define WTCSR_PCLK_BY4		0x01	/* count-up by P-clock/4 */
#define WTCSR_PCLK_BY1		0x00	/* count-up by P-clock/1 */
#endif	/* definition not used */


/* TMU (32bit x 3ch TiMer Unit) register addresses */

#define TMU_TOCR	((volatile UINT8  *)0xffd80000)
#define TMU_TSTR	((volatile UINT8  *)0xffd80004)
#define TMU_TCOR0	((volatile UINT32 *)0xffd80008)
#define TMU_TCNT0	((volatile UINT32 *)0xffd8000c)
#define TMU_TCR0	((volatile UINT16 *)0xffd80010)
#define TMU_TCOR1	((volatile UINT32 *)0xffd80014)
#define TMU_TCNT1	((volatile UINT32 *)0xffd80018)
#define TMU_TCR1	((volatile UINT16 *)0xffd8001c)
#define TMU_TCOR2	((volatile UINT32 *)0xffd80020)
#define TMU_TCNT2	((volatile UINT32 *)0xffd80024)
#define TMU_TCR2	((volatile UINT16 *)0xffd80028)
#define TMU_TCPR2	((volatile UINT32 *)0xffd8002c)

/* TMU_TOCR_TCLK_CNTL (Timer Output Control Register) bit definitions */
#define TOCR_DEFAULT	((UINT8)0x00)
#define TOCR_TCOE	((UINT8)0x01)	/* RTC output */
#define TOCR_TCLK_IN	TOCR_DEFAULT
#define TOCR_TCLK_OUT	TOCR_TCOE

/* TMU_TSTR_START_CNTL (Timer STart Register) bit definitions */
#define TSTR_STOP_ALL	((UINT8)0x00)	/* stop counter 0/1/2 */
#define TSTR_STR2	((UINT8)0x04)	/* start counter 2 */
#define TSTR_STR1	((UINT8)0x02)	/* start counter 1 */
#define TSTR_STR0	((UINT8)0x01)	/* start counter 0 */

/* TMU_TCR_CNTL (Timer Control Register) bit definitions */
#define TCR_DEFAULT	((UINT16)0x0000)
#define TCR_ICPF	((UINT16)0x0200) /* input-capture status (ch.2 only) */
#define TCR_UNF		((UINT16)0x0100) /* underflow status */
#define TCR_ICPE1	((UINT16)0x0080) /* enable input-capture (ch.2 only) */
#define TCR_ICPE0	((UINT16)0x0040) /* enable input-capture interrupt */
#define TCR_UNIE	((UINT16)0x0020) /* enable underflow interrupt */
#define TCR_CKEG_LEAD	((UINT16)0x0000) /* count on leading edge */
#define TCR_CKEG_TRAIL	((UINT16)0x0008) /* count on trailing edge */
#define TCR_CKEG_BOTH	((UINT16)0x0010) /* use both edge (up & down) */
#define TCR_TPSC_P4	((UINT16)0x0000) /* count by P/4 */
#define TCR_TPSC_P16	((UINT16)0x0001) /* count by P/16 */
#define TCR_TPSC_P64	((UINT16)0x0002) /* count by P/64 */
#define TCR_TPSC_P256	((UINT16)0x0003) /* count by P/256 */
#define TCR_TPSC_P1024	((UINT16)0x0004) /* count by P/1024 */
#define TCR_TPSC_RTCCLK	((UINT16)0x0006) /* count by RTCCLK */
#define TCR_TPSC_TCLK	((UINT16)0x0007) /* count by TCLK */

/* For backward compatibility */
#define TMU_TOCR_TCLK_CNTL	TMU_TOCR
#define TMU_TSTR_START_CNTL	TMU_TSTR
#define TMU_TCOR_CONST_0	TMU_TCOR0
#define TMU_TCNT_COUNT_0	TMU_TCNT0
#define TMU_TCR_CNTL_0		TMU_TCR0
#define TMU_TCOR_CONST_1	TMU_TCOR1
#define TMU_TCNT_COUNT_1	TMU_TCNT1
#define TMU_TCR_CNTL_1		TMU_TCR1
#define TMU_TCOR_CONST_2	TMU_TCOR2
#define TMU_TCNT_COUNT_2	TMU_TCNT2
#define TMU_TCR_CNTL_2		TMU_TCR2
#define TMU_TCPR2_CAPTURE	TMU_TCPR2
#define TSTR_START_2		TSTR_STR2
#define TSTR_START_1		TSTR_STR1
#define TSTR_START_0		TSTR_STR0
#define TCR_CNTL_DEFAULT	TCR_DEFAULT


/* UBC (User Break Controller) register addresses */

#define	UBC_BARA	((volatile UINT32 *)0xff200000)	/* Break Address      Register A      */
#define	UBC_BAMRA	((volatile UINT8  *)0xff200004)	/* Break Address Mask Register A (8)  */
#define	UBC_BBRA	((volatile UINT16 *)0xff200008)	/* Break Buscycle     Register A (16) */
#define	UBC_BASRA	((volatile UINT8  *)0xff000014)	/* Break ASid         Register A (8)  */
#define	UBC_BARB	((volatile UINT32 *)0xff20000c)	/* Break Address      Register B      */
#define	UBC_BAMRB	((volatile UINT8  *)0xff200010)	/* Break Address Mask Register B (8)  */
#define	UBC_BBRB	((volatile UINT16 *)0xff200014)	/* Break Buscycle     Register B (16) */
#define	UBC_BASRB	((volatile UINT8  *)0xff000018)	/* Break ASid         Register B (8)  */
#define	UBC_BDRB	((volatile UINT32 *)0xff200018)	/* Break Data         Register B      */
#define	UBC_BDMRB	((volatile UINT32 *)0xff20001c)	/* Break Data    Mask Register B      */
#define	UBC_BRCR	((volatile UINT16 *)0xff200020)	/* BReak Control      Register   (16) */


/* RTC (Real Time Clock) register addresses */

#if 0	/* definition not used */
#define RTC_COUNT_64HZ		((volatile UINT8 *)0xffc80000)	/* R64CNT  */
#define RTC_COUNT_SEC		((volatile UINT8 *)0xffc80004)	/* RSECCNT */
#define RTC_COUNT_MIN		((volatile UINT8 *)0xffc80008)	/* RMINCNT */
#define RTC_COUNT_HOUR		((volatile UINT8 *)0xffc8000c)	/* RHRCNT  */
#define RTC_COUNT_DAY_OF_WEEK	((volatile UINT8 *)0xffc80010)	/* RWKCNT  */
#define RTC_COUNT_DAY		((volatile UINT8 *)0xffc80014)	/* RDAYCNT */
#define RTC_COUNT_MONTH		((volatile UINT8 *)0xffc80018)	/* RMONCNT */
#define RTC_COUNT_YEAR		((volatile UINT16 *)0xffc8001c)	/* RYRCNT  */
#endif	/* definition not used */
#define RTC_ALARM_SEC		((volatile UINT8 *)0xffc80020)	/* RSECAR  */
#define RTC_ALARM_MIN		((volatile UINT8 *)0xffc80024)	/* RMINAR  */
#define RTC_ALARM_HOUR		((volatile UINT8 *)0xffc80028)	/* RHRAR   */
#define RTC_ALARM_DAY_OF_WEEK	((volatile UINT8 *)0xffc8002c)	/* RWKAR   */
#define RTC_ALARM_DAY		((volatile UINT8 *)0xffc80030)	/* RDAYAR  */
#define RTC_ALARM_MONTH		((volatile UINT8 *)0xffc80034)	/* RMONAR  */
#define RTC_CONTROL_1		((volatile UINT8 *)0xffc80038)	/* RCR1    */
#define RTC_CONTROL_2		((volatile UINT8 *)0xffc8003c)	/* RCR2    */


/* SCI (Serial Communication Interface) register addresses */

#define SCI_SMR1	((volatile UINT8 *)0xffe00000)	/* serial mode     */
#define SCI_BRR1	((volatile UINT8 *)0xffe00004)	/* bit rate        */
#define SCI_SCR1	((volatile UINT8 *)0xffe00008)	/* serial control  */
#define SCI_TDR1	((volatile UINT8 *)0xffe0000c)	/* transmit data   */
#define SCI_SSR1	((volatile UINT8 *)0xffe00010)	/* serial status   */
#define SCI_RDR1	((volatile UINT8 *)0xffe00014)	/* receive data    */
#define SCI_SCMR	((volatile UINT8 *)0xffe00018)	/* smart card mode */

/* SCIF (Serial Communication Interface with FIFO) register addresses */

#define SCIF_SMR2	((volatile UINT16 *)0xffe80000)	/* serial mode        */
#define SCIF_BRR2	((volatile UINT8  *)0xffe80004)	/* bit rate           */
#define SCIF_SCR2	((volatile UINT16 *)0xffe80008)	/* serial control     */
#define SCIF_FTDR2	((volatile UINT8  *)0xffe8000c)	/* transmit FIFO data */
#define SCIF_SFDR2	SCIF_FTDR2
#define SCIF_FSR2	((volatile UINT16 *)0xffe80010)	/* serial status      */
#define SCIF_SSR2	SCIF_FSR2
#define SCIF_FRDR2	((volatile UINT8  *)0xffe80014)	/* receive FIFO data  */
#define SCIF_FCR2	((volatile UINT16 *)0xffe80018)	/* FIFO control       */
#define SCIF_FDR2	((volatile UINT16 *)0xffe8001c)	/* FIFO data count    */
#define SCIF_SPTR2	((volatile UINT16 *)0xffe80020)	/* serial port reg    */
#define SCIF_LSR2	((volatile UINT16 *)0xffe80024)	/* line status        */


/* IOP (I/O Port) register addresses */

#define IOP_PCTR	((volatile UINT32 *)0xff80002c)	/* I/O port control */
#define IOP_PDTR	((volatile UINT16 *)0xff800030)	/* I/O port data    */
#define IOP_SCSPTR1	((volatile UINT8  *)0xffe0001c)	/* serial port1 I/O */
#define IOP_SCSPTR2	((volatile UINT16 *)0xffe80020)	/* serial port2 I/O */

/* Bus State Controller (BSC) register definitions */

#define BSC_BCR1	((volatile UINT32 *)0xff800000)	/* Bus control reg. 1 */
#define BSC_BCR2	((volatile UINT16 *)0xff800004)	/* Bus control reg. 2 */
#define BSC_WCR1	((volatile UINT32 *)0xff800008)	/* Wait state ctrl. 1 */
#define BSC_WCR2	((volatile UINT32 *)0xff80000c)	/* Wait state ctrl. 2 */
#define BSC_WCR3	((volatile UINT32 *)0xff800010)	/* Wait state ctrl. 3 */
#define BSC_MCR		((volatile UINT32 *)0xff800014)	/* Memory ctrl. reg.  */
#define BSC_PCR		((volatile UINT16 *)0xff800018)	/* PCMCIA ctrl. reg.  */
#define BSC_RTCSR	((volatile UINT16 *)0xff80001c)	/* Refresh ctrl./stat.*/
#define BSC_RTCNT	((volatile UINT16 *)0xff800020)	/* Refresh timer count*/
#define BSC_RTCOR	((volatile UINT16 *)0xff800024)	/* Refresh time const */
#define BSC_RFCR	((volatile UINT16 *)0xff800028)	/* Refresh count reg. */


/* function declarations */

#ifndef	_ASMLANGUAGE
#if defined(__STDC__) || defined(__cplusplus)

IMPORT	STATUS	sysIntDisable (int intLevel);
IMPORT	STATUS	sysIntEnable (int intLevel);
IMPORT	int	sysBusIntAck (int intLevel);
IMPORT	STATUS	sysBusIntGen (int level, int vector);
IMPORT	STATUS	sysMailboxEnable (char *mailboxAdrs);
IMPORT	STATUS	sysMailboxConnect (FUNCPTR routine, int arg);

#else	/* __STDC__ */

IMPORT	STATUS	sysIntDisable ();
IMPORT	STATUS	sysIntEnable ();
IMPORT	int	sysBusIntAck ();
IMPORT	STATUS	sysBusIntGen ();
IMPORT	STATUS	sysMailboxEnable ();
IMPORT	STATUS	sysMailboxConnect ();

#endif	/* __STDC__ */

#endif	/* _ASMLANGUAGE */

#ifdef __cplusplus
}
#endif

#endif /* __INCsh7750h */
