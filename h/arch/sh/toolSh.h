/* toolSh.h - tool dependent header */

/* Copyright 1984-2004 Wind River Systems, Inc. */
/*
modification history
--------------------
01j,08sep04,h_k  swapped _FUNCTION and FUNCTION.
01i,02sep04,h_k  added LEADING_UNDERSCORE macro and set TRUE.
01h,30nov01,zl   removed unnecessary and non-working TOOL_FAMILY conditional.
01g,31oct01,zl   added Diab support, removed NULL redefinition.
01f,23may00,zl   force definition of _GNU_TOOL.
01e,29apr00,zl   added NULL definition.
01d,29apr00,zl   added _BYTE_ORDER definition back.
01c,21apr00,max  removed vararg, using toolchain includes.
01b,20sep99,zl   define _BYTE_ORDER for little endian.
01a,04jun99,zl   written based on va-sh.h from the GNU distribution
*/

#ifndef __INCtoolShh
#define __INCtoolShh

/* Both GNU and Diab preprocessor define __LITTLE_ENDIAN__ */

#undef _BYTE_ORDER
#ifdef __LITTLE_ENDIAN__
#define	_BYTE_ORDER		_LITTLE_ENDIAN
#else
#define	_BYTE_ORDER		_BIG_ENDIAN
#endif

/*
 * The LEADING_UNDERSCORE macro should be defined to TRUE for toolchains
 * that prefix a leading underscore character, i.e. "_", to symbols in B6
 * for backword compatible with T2.
 */

#define LEADING_UNDERSCORE TRUE

#if	(LEADING_UNDERSCORE == TRUE)
#define _FUNCTION(func)      _##func
#define _FUNC_LABEL(func)    _##func##:
#define _VAR(var)            _##var
#define _VAR_LABEL(var)      _##var##:
#else	/* LEADING_UNDERSCORE == FALSE */
#define _FUNCTION(func)      func
#define _FUNC_LABEL(func)    func##:
#define _VAR(var)            var
#define _VAR_LABEL(var)      var##:
#endif	/* LEADING_UNDERSCORE == TRUE */

/*
 * This extra macro forces an extra level of indirection without which
 * the preprocessor does not fully expand _FUNCTION.
 */

#define FUNCTION(func)     _FUNCTION(func)
#define FUNC_LABEL(func)   _FUNCTION(func): 
#define VAR(var)           _VAR(var)
#define VAR_LABEL(var)     _VAR(var):

/*
 * These macros are used to declare assembly language symbols that need
 * to be typed properly(func or data) to be visible to the OMF tool.
 * So that the build tool could mark them as an entry point to be linked
 * by another PD.
 */

#define GTEXT(sym)          FUNCTION(sym) ; .type FUNCTION(sym),@function
#define GDATA(sym)          VAR(sym) ; .type VAR(sym),@object

#endif /* __INCtoolShh */
