/* regsSh.h - Renesas SH registers */

/* 
 * Copyright (c) 1994-2005 Wind River Systems, Inc. 
 *
 * The right to copy, distribute, modify or otherwise make use 
 * of this software may be licensed only pursuant to the terms 
 * of an applicable Wind River license agreement. 
 */

/*
modification history
--------------------
02a,01mar05,h_k  added _SH_PTEA_SUPPORT definition. (SPR #106555)
01z,19oct04,h_k  added SR_BIT_RB and SR_BIT_BL definitions.
01y,18nov03,h_k  added pteh to REG_SET.
01x,30oct03,h_k  added _SH_REG_SET_SIZE.
01w,11sep03,h_k  added SR_BIT_MD.
01v,26aug03,h_k  add P4_REG_BASE and register offsets for assembly code.
01u,21aug00,hk   add SH7729/SH7410 SR info.
01t,29sep98,hk   code review: minor supplement for SR diagram.
01s,24sep98,hms  modified comment for definition of bits in the SH7750 SR.
01r,17feb97,hk   deleted unnecessary bank register defs in #if 0 clause.
01q,04jan97,hk   deleted register set offset comments.
01p,19aug96,hk   deleted bank registers from SH7700' REG_SET.
01o,24jul96,wt   added fpReg definition for wdbBpLib.h.
01n,13jun96,hk   added bareg[8] to REG_SET for SH7700.
01m,12may96,hk   added sr def for SH7700.
01l,15dec94,hk   adding sr bit definition.
01k,05dec94,hk   swapped sr and pc in REG_SET.
01j,02dec94,hk   refined WIND_TCB_XXX defines.
01i,01dec94,hk   added spReg comment.
01h,14oct94,hk   added spReg definition for taskShow.c.
01g,09oct94,hk   moved dummy FP_CONTEXT, FPREG_SET to fppShLib.h (alpha-0).
01f,09oct94,hk   added FP_CONTEXT.
01e,09oct94,hk   added PC_OFFSET, FPREG_SET.
01d,07oct94,hk   split volatile/non-volatile regs, inserted mach/macl.
01c,27sep94,hk   swapped pr and mac[2] in REG_SET.
01b,26sep94,hk   swapped sr and pc in REG_SET.
01a,15jun94,hk   derived from 01h of MIPS.
*/

#ifndef __INCregsShh
#define __INCregsShh

#ifdef __cplusplus
extern "C" {
#endif

#ifndef	_ASMLANGUAGE

typedef struct			/* SH Register Set */
    {
    ULONG vbr;			/* vector base register */
    ULONG gbr;			/* global base register */
    INSTR *pr;			/* procedure register (for subroutine rtn) */
    ULONG voreg[8];		/* volatile registers (r0 - r7) */
    ULONG mac  [2];		/* multiply and accumulate registers */
    ULONG nvreg[8];		/* non-volatile registers (r8 - r15) */
    INSTR *pc;			/* program counter */
    ULONG sr;			/* status register */
#if	(CPU==SH7750 || CPU==SH7700)
    ULONG pteh;			/* page table entry high */
#endif	/* (CPU==SH7750 || CPU==SH7700) */
    } REG_SET;

#define	fpReg		nvreg[6]	/* needed by wdbBpLib.h */
#define	spReg		nvreg[7]	/* needed by taskShow.c, dbgArchLib.c */

#else	/* _ASMLANGUAGE */

/* On-chip register base address and offsets for assembly code */

#if	(CPU==SH7750)
#define P4_REG_BASE		   0xff000000
#define TRA		0x20	/* 0xff000020: TRApa exception register */
#define EXPEVT		0x24	/* 0xff000024: Exception Event	      */
#define PTEH		0x00	/* 0xff000000: Page Table Entry High */
#define PTEL		0x04	/* 0xff000004: Page Table Entry Low  */
#define TTB		0x08	/* 0xff000008: Translation Table Base */
#define TEA		0x0c	/* 0xff00000c: TLB Exception Address  */
#define MMUCR		0x10	/* 0xff000010: MMU Control Register */
#define CCR		0x1c	/* 0xff00001c: Cache Control Register */
#define PTEA		0x34	/* 0xff000034: Page Table Entry Assistant */
#define _SH_PTEA_SUPPORT	/* enable PTEA register support */
#elif	(CPU==SH7700)
#define TRA		0xd0	/* 0xffffffd0: TRApa exception register */
#define EXPEVT		0xd4	/* 0xffffffd4: EXception EVenT register */
#define TEA		0xfc	/* 0xfffffffc: TLB Exception Address  */

#define P4_REG_BASE		   0xffffffe0
#define MMUCR		0x00	/* 0xffffffe0: MMU Control Register */
#define PTEH		0x10	/* 0xfffffff0: Page Table Entry High */
#define PTEL		0x14	/* 0xfffffff4: Page Table Entry Low  */
#define TTB		0x18	/* 0xfffffff8: Translation Table Base */
/*#define CCR		0x0c	/@ 0xffffffec: Cache Control Register */
#endif	/*CPU==SH7700*/

#endif	/* _ASMLANGUAGE */

#define	REG_SET_VBR		0x00
#define	REG_SET_GBR		0x04
#define	REG_SET_PR		0x08
#define	REG_SET_R0		0x0c
#define	REG_SET_R1		0x10
#define	REG_SET_R2		0x14
#define	REG_SET_R3		0x18
#define	REG_SET_R4		0x1c
#define	REG_SET_R5		0x20
#define	REG_SET_R6		0x24
#define	REG_SET_R7		0x28
#define	REG_SET_MACH		0x2c
#define	REG_SET_MACL		0x30
#define	REG_SET_R8		0x34
#define	REG_SET_R9		0x38
#define	REG_SET_R10		0x3c
#define	REG_SET_R11		0x40
#define	REG_SET_R12		0x44
#define	REG_SET_R13		0x48
#define	REG_SET_R14		0x4c
#define	REG_SET_R15		0x50
#define	REG_SET_PC		0x54
#define	REG_SET_SR		0x58
#if   	(CPU==SH7750 || CPU==SH7700)
#define	REG_SET_PTEH		0x5c

#define	_SH_REG_SET_SIZE	(REG_SET_PTEH + 4)	/* for _sigCtxSave */
#else
#define	_SH_REG_SET_SIZE	(REG_SET_SR + 4)	/* for _sigCtxSave */
#endif	/* (CPU==SH7750 || CPU==SH7700) */

#define PC_OFFSET	REG_SET_PC	/* referenced by pc() in usrLib */

/*
 * Definition of bits in the SH7750 Status Register (SR)
 *
 *  +--+--+--+--+-----+--+------------+---+---+---+---+---+---+---+---+---+---+
 *  |- |MD|RB|BL|  -  |FD|  --------  | M | Q | Interrupt Mask| - | - | S | T |
 *  +--+--+--+--+-----+--+------------+---+---+---+---+---+---+---+---+---+---+
 *   31 30 29 28       15               9   8   7   6   5   4   3   2   1   0
 *
 * Definition of bits in the SH7729 Status Register (SR)
 *
 *  +--+--+--+--+-----+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
 *  |- |MD|RB|BL| RC  | - |DSP|DMY|DMX| M | Q | Interrupt Mask|RF1|RF0| S | T |
 *  +--+--+--+--+-----+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
 *   31 30 29 28 27-16     12  11  10   9   8   7   6   5   4   3   2   1   0
 *
 * Definition of bits in the SH7700 Status Register (SR)
 *
 *  +--+--+--+--+---------------------+---+---+---+---+---+---+---+---+---+---+
 *  |- |MD|RB|BL|  -----------------  | M | Q | Interrupt Mask| - | - | S | T |
 *  +--+--+--+--+---------------------+---+---+---+---+---+---+---+---+---+---+
 *   31 30 29 28                        9   8   7   6   5   4   3   2   1   0
 *
 * Definition of bits in the SH7410 Status Register (SR)
 *
 *  +-----------+-----+-------+---+---+---+---+---+---+---+---+---+---+---+---+
 *  | --------- | RC  | ----- |DMY|DMX| M | Q | Interrupt Mask|RF1|RF0| S | T |
 *  +-----------+-----+-------+---+---+---+---+---+---+---+---+---+---+---+---+
 *   31       28 27-16         11  10   9   8   7   6   5   4   3   2   1   0
 *
 * Definition of bits in the SH7600/SH7040/SH7000 Status Register (SR)
 *
 *  +---------------------------------+---+---+---+---+---+---+---+---+---+---+
 *  |  -----------------------------  | M | Q | Interrupt Mask| - | - | S | T |
 *  +---------------------------------+---+---+---+---+---+---+---+---+---+---+
 *   31                                 9   8   7   6   5   4   3   2   1   0
 */
#define	SR_BIT_T	0x00000001	/* ref: dbgArchLib.c */
#define	SR_BIT_MD	0x40000000	/* ref: taskArchLib.c and
					 *      rtpSigArchLib.c
					 */
#define SR_BIT_RB	0x20000000	/* ref: rtpSigArchLib.c */
#define SR_BIT_BL	0x10000000	/* ref: rtpSigArchLib.c */

/* TCB Register Set	(WIND_TCB_REGS is defined in h/private/taskLibP.h) */

#define	WIND_TCB_VBR	(WIND_TCB_REGS + REG_SET_VBR)
#define	WIND_TCB_GBR	(WIND_TCB_REGS + REG_SET_GBR)
#define	WIND_TCB_PR	(WIND_TCB_REGS + REG_SET_PR)
#define	WIND_TCB_R0	(WIND_TCB_REGS + REG_SET_R0)
#define	WIND_TCB_R1	(WIND_TCB_REGS + REG_SET_R1)
#define	WIND_TCB_R2	(WIND_TCB_REGS + REG_SET_R2)
#define	WIND_TCB_R3	(WIND_TCB_REGS + REG_SET_R3)
#define	WIND_TCB_R4	(WIND_TCB_REGS + REG_SET_R4)
#define	WIND_TCB_R5	(WIND_TCB_REGS + REG_SET_R5)
#define	WIND_TCB_R6	(WIND_TCB_REGS + REG_SET_R6)
#define	WIND_TCB_R7	(WIND_TCB_REGS + REG_SET_R7)
#define	WIND_TCB_MACH	(WIND_TCB_REGS + REG_SET_MACH)
#define	WIND_TCB_MACL	(WIND_TCB_REGS + REG_SET_MACL)
#define	WIND_TCB_R8	(WIND_TCB_REGS + REG_SET_R8)
#define	WIND_TCB_R9	(WIND_TCB_REGS + REG_SET_R9)
#define	WIND_TCB_R10	(WIND_TCB_REGS + REG_SET_R10)
#define	WIND_TCB_R11	(WIND_TCB_REGS + REG_SET_R11)
#define	WIND_TCB_R12	(WIND_TCB_REGS + REG_SET_R12)
#define	WIND_TCB_R13	(WIND_TCB_REGS + REG_SET_R13)
#define	WIND_TCB_R14	(WIND_TCB_REGS + REG_SET_R14)
#define	WIND_TCB_R15	(WIND_TCB_REGS + REG_SET_R15)
#define	WIND_TCB_PC	(WIND_TCB_REGS + REG_SET_PC)
#define	WIND_TCB_SR	(WIND_TCB_REGS + REG_SET_SR)
#define	WIND_TCB_PTEH	(WIND_TCB_REGS + REG_SET_PTEH)

#ifdef __cplusplus
}
#endif

#endif /* __INCregsShh */
