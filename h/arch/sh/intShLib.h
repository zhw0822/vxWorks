/* intShLib.h - SH interrupt library header */

/* Copyright 2000-2000 Wind River Systems, Inc. */

/*
modification history
--------------------
01a,11sep00,hk   created.
*/

#ifndef __INCintShLibh
#define __INCintShLibh

#ifdef __cplusplus
extern "C" {
#endif

#include "vxWorks.h"

#if defined(__STDC__) || defined(__cplusplus)

extern void	intVBRSet (FUNCPTR *baseAddr);
extern int	intSRGet (void);
extern void	intSRSet (int value);
extern STATUS	(* _func_intConnectHook) (VOIDFUNCPTR *, VOIDFUNCPTR, int);
#if (CPU==SH7750 || CPU==SH7700)
extern STATUS	intGlobalSRSet (UINT32 bits, UINT32 mask, int maxTasks);
#endif /* (CPU==SH7750 || CPU==SH7700) */

#else

extern void	intVBRSet ();
extern int	intSRGet ();
extern void	intSRSet ();
extern STATUS	(* _func_intConnectHook) ();
#if (CPU==SH7750 || CPU==SH7700)
extern STATUS	intGlobalSRSet ();
#endif /* (CPU==SH7750 || CPU==SH7700) */

#endif

#ifdef __cplusplus
}
#endif

#endif /* __INCintShLibh */
