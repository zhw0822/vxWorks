/* coprocSh.h - coprocessor management library header */

/* Copyright 1984-2004 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*
modification history
--------------------
01a,21apr04,h_k  written.
*/

#ifndef __INCcoprocShh
#define __INCcoprocShh

#ifdef __cplusplus
extern "C" {
#endif

/* inlcudes */

#include "taskLib.h"

/* defines */

#define VX_FP_TASK	VX_COPROC1_TASK		/* fpu coprocessor support */
#define VX_DSP_TASK	VX_COPROC2_TASK		/* dsp coprocessor support */


#ifdef __cplusplus
}
#endif

#endif /* __INCcoprocMipsh */
