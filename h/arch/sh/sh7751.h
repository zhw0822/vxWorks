/* sh7751.h - SH7751 header */

/* Copyright 1984-1998 Wind River Systems, Inc. */

/*
modification history
--------------------
01d,11jun01,h_k  added for PCI definition.
01c,09mar01,frf  corrected PCI register definition.
01b,20feb01,hk   added closing brace for __cplusplus.
01a,10jul00,frf  Added declaration of new Regs
01a,05apr00,dgy  created.
*/

/*
DESCRIPTION
This file contains I/O addresses and related constants for the SH7751 chip.
It will be used as an additional file to the sh7750.h and may redefine 
elements already created therein.
*/

#ifndef __INCsh7751h
#define __INCsh7751h

#ifdef __cplusplus
extern "C" {
#endif


/* Interrupt Controller Regfisters  */

#define INT_PRI_00                ((volatile UINT32 *)0xfe080000)    /* Interrupt Priority level setting register 00 */
#define INT_REQ_00                ((volatile UINT32 *)0xfe080020)    /* Interrupt Factor  register 00    */
#define INT_MSK_00                ((volatile UINT32 *)0xfe080040)    /* Interrupt Mask  register 00      */
#define INT_MSKCLR_00             ((volatile UINT32 *)0xfe080060)    /* Interrupt Mask Clear register 00 */

/* FRQCR2 (FReQuency Control Register 2) */

#define FRQCR2		((volatile UINT16 *)0xffc00014)


/* TMU register addresses */

/* The 7750 is a 32 bit x 3ch Timer Unit, the 7751 uses the same 	*/
/* definitions for the first 3 timers, but then has 2 additional 	*/
/* available that have the following new register definitions.		*/

#define TMU_TIMESTAMP_0			0	/* Timer 2 */
#define TMU_TIMESTAMP_1			1	/* Timer 3 */
#define TMU_TIMESTAMP_2			2	/* Timer 4 */

#define TMU_TSTR2_START_CNTL	((volatile UINT8  *)0xfe100004)

#define TMU_TCOR_CONST_3		((volatile UINT32 *)0xfe100008)
#define TMU_TCNT_COUNT_3		((volatile UINT32 *)0xfe10000c)
#define TMU_TCR_CNTL_3			((volatile UINT16 *)0xfe100010)

#define TMU_TCOR_CONST_4		((volatile UINT32 *)0xfe100014)
#define TMU_TCNT_COUNT_4		((volatile UINT32 *)0xfe100018)
#define TMU_TCR_CNTL_4			((volatile UINT16 *)0xfe10001c)

/* TMU_TSTR2_START_CNTL (Timer STart Register2) bit definitions */

#define TSTR2_STOP_ALL	((UINT8)0x00)	/* stop counter 3/4 */
#define TSTR2_START_3	((UINT8)0x01)	/* start counter 0 */
#define TSTR2_START_4	((UINT8)0x02)	/* start counter 1 */

#define SH7751_NO_TIMERS		3


/*  Specific PCI Control Registers  */

#define PCIC_PCI_CONFIG_BASE	0xfe200000		    /* configuration register 0 */
#define PCIC_CONFIG_0   (volatile UINT32 *)(PCIC_PCI_CONFIG_BASE+0x00)   /* PCI config reg 0 */
#define PCIC_CONFIG_1   (volatile UINT32 *)(PCIC_PCI_CONFIG_BASE+0x04)   /* PCI config reg 1 */
#define PCIC_CONFIG_2   (volatile UINT32 *)(PCIC_PCI_CONFIG_BASE+0x08)   /* PCI config reg 2 */
#define PCIC_CONFIG_3   (volatile UINT32 *)(PCIC_PCI_CONFIG_BASE+0x0C)   /* PCI config reg 3 */
#define PCIC_CONFIG_4   (volatile UINT32 *)(PCIC_PCI_CONFIG_BASE+0x10)   /* PCI config reg 4 */
#define PCIC_CONFIG_5   (volatile UINT32 *)(PCIC_PCI_CONFIG_BASE+0x14)   /* PCI config reg 5 */
#define PCIC_CONFIG_6   (volatile UINT32 *)(PCIC_PCI_CONFIG_BASE+0x18)   /* PCI config reg 6 */
#define PCIC_CONFIG_7   (volatile UINT32 *)(PCIC_PCI_CONFIG_BASE+0x1C)   /* PCI config reg 7 */
#define PCIC_CONFIG_8   (volatile UINT32 *)(PCIC_PCI_CONFIG_BASE+0x20)   /* PCI config reg 8 */
#define PCIC_CONFIG_9   (volatile UINT32 *)(PCIC_PCI_CONFIG_BASE+0x24)   /* PCI config reg 9 */
#define PCIC_CONFIG_10  (volatile UINT32 *)(PCIC_PCI_CONFIG_BASE+0x28)   /* PCI config reg 10*/
#define PCIC_CONFIG_11  (volatile UINT32 *)(PCIC_PCI_CONFIG_BASE+0x2C)   /* PCI config reg 11*/
#define PCIC_CONFIG_12  (volatile UINT32 *)(PCIC_PCI_CONFIG_BASE+0x30)   /* PCI config reg 12*/
#define PCIC_CONFIG_13  (volatile UINT32 *)(PCIC_PCI_CONFIG_BASE+0x34)   /* PCI config reg 13*/
#define PCIC_CONFIG_14	(volatile UINT32 *)(PCIC_PCI_CONFIG_BASE+0x38)   /* PCI config reg 14*/
#define PCIC_CONFIG_15  (volatile UINT32 *)(PCIC_PCI_CONFIG_BASE+0x3C)   /* PCI config reg 15*/
#define PCIC_CONFIG_16  (volatile UINT32 *)(PCIC_PCI_CONFIG_BASE+0x40)   /* PCI config reg 16*/
#define PCIC_CONFIG_17  (volatile UINT32 *)(PCIC_PCI_CONFIG_BASE+0x44)   /* PCI config reg 17*/

#define PCICR 		 ((volatile UINT32 *)0xfe200100) 		 /* PCI Control Register */
/* Bit field Mask */
#define PCICR_CFINIT		0x0001				/*set to initialise pcic*/	
#define PCICR_INTA		0x0004				/* Software control of  INTA */
#define PCICR_SERR		0x0008				/* Software control of SERR  */
#define PCICR_BMABT		0x0040				/*controls arbitration mode of bus master*/
#define PCICR_BYTESWAP		0x0100				/*data byte endian swap little/big*/
#define PCICR_KEY		0xa5000000			/* Key to enable reg. write  */

#define PCILSR0           ((volatile UINT32 *)0xfe200104)               /* Local space register 0 for PCI */
#define PCILSR1           ((volatile UINT32 *)0xfe200108)               /* Local space register 1 for PCI */
#define PCILAR0           ((volatile UINT32 *)0xfe20010C)               /* Local address register 0 for PCI */
#define PCILAR1           ((volatile UINT32 *)0xfe200110)               /* Local address register 1 for PCI */
#define PCIINT            ((volatile UINT32 *)0xfe200114)               /* PCI interrupt register           */
#define PCIINTM           ((volatile UINT32 *)0xfe200118)               /* PCI interrupt mask register      */
#define PCIALR            ((volatile UINT32 *)0xfe20011C)               /* Error Address data register for PCI */
#define PCICLR            ((volatile UINT32 *)0xfe200120)               /* Error Command data register for PCI */
/* Reserved Register            0xfe200124                     */
/* Reserved Register            0xfe200128                     */
/* Reserved Register            0xfe20012C                     */
#define PCIAINT           ((volatile UINT32 *)0xfe200130)               /* PCI arbiter interrupt register      */
#define PCIAINTM          ((volatile UINT32 *)0xfe200134)               /* PCI arbiter interrupt mask register */
#define PCIBMLR           ((volatile UINT32 *)0xfe200138)               /* Error Bus Master data Register      */
/* Reserved Register            0xfe20013C                     */

/**************  DMA SECTICTION ********************************/

#define PCIDMABT          ((volatile UINT32 *)0xfe200140)      /* DMA Transfet arbitration register for PCI */
/**************  DMA CHANNEL 0  *******************************/

#define PCIDPA0           ((volatile UINT32 *)0xfe200180)      /* DMA transfer PCI address register 1 for PCI */
#define PCIDLA0           ((volatile UINT32 *)0xfe200184)      /* DMA transfer local bus starting address register 0 for PCI */
#define PCIDTC0           ((volatile UINT32 *)0xfe200188)      /* DMA transfer count register 0 for PCI  */
#define PCIDCR0           ((volatile UINT32 *)0xfe20018C)      /* DMA Control Register 0 for PCI         */
/**************  DMA CHANNEL 1  *******************************/

#define PCIDPA1           ((volatile UINT32 *)0xfe200190)      /* DMA transfer PCI address register 1 for PCI */
#define PCIDLA1           ((volatile UINT32 *)0xfe200194)      /* DMA transfer local bus starting address register 1 for PCI */
#define PCIDTC1           ((volatile UINT32 *)0xfe200198)      /* DMA transfer count register 1 for PCI  */
#define PCIDCR1           ((volatile UINT32 *)0xfe20019C)      /* DMA Control Register 1 for PCI         */
/**************  DMA CHANNEL 2  *******************************/

#define PCIDPA2            ((volatile UINT32 *)0xfe2001A0)     /* DMA transfer PCI address register 2 for PCI */
#define PCIDLA2            ((volatile UINT32 *)0xfe2001A4)     /* DMA transfer local bus starting address register 2 for PCI */
#define PCIDTC2            ((volatile UINT32 *)0xfe2001A8)     /* DMA transfer count register 2 for PCI  */
#define PCIDCR2            ((volatile UINT32 *)0xfe2001AC)     /* DMA Control Register 2 for PCI         */
/**************  DMA CHANNEL 3  *******************************/

#define PCIDPA3            ((volatile UINT32 *)0xfe2001B0)     /* DMA transfer PCI address register 3 for PCI */
#define PCIDLA3            ((volatile UINT32 *)0xfe2001B4)     /* DMA transfer local bus starting address register 3 for PCI */
#define PCIDTC3            ((volatile UINT32 *)0xfe2001B8)     /* DMA transfer count register 3 for PCI  */
#define PCIDCR3            ((volatile UINT32 *)0xfe2001BC)     /* DMA Control Register 3 for PCI         */
/************** END DMA SECTION  ******************************/

#define PCIPAR              ((volatile UINT32 *)0xfe2001C0)    /* PIO Address register                   */
#define PCIMBR              ((volatile UINT32 *)0xfe2001C4)    /* Memory space base register             */
#define PCIIOBR             ((volatile UINT32 *)0xfe2001C8)    /* IO space base register                 */
#define PCIPINT             ((volatile UINT32 *)0xfe2001CC)    /* PCI power managment interrupt register */
#define PCIPINTM            ((volatile UINT32 *)0xfe2001D0)    /* PCI power managment interrupt mask register */
#define PCICLKR             ((volatile UINT32 *)0xfe2001D4)    /* PCI Clock control register             */ 
#define PCIBCR1             ((volatile UINT32 *)0xfe2001E0)    /* PCI bus control registrer 1            */
#define PCIBCR2             ((volatile UINT32 *)0xfe2001E4)    /* PCI bus control registrer 2            */
#define PCIWCR1             ((volatile UINT32 *)0xfe2001E8)    /* PCI wait control register 1            */
#define PCIWCR2             ((volatile UINT32 *)0xfe2001EC)    /* PCI wait control register 2            */
#define PCIWCR3             ((volatile UINT32 *)0xfe2001F0)    /* PCI wait control register 3            */
#define PCIMCR              ((volatile UINT32 *)0xfe2001F4)    /* PCIC discrete memory control register  */
#define PCIPCTR             ((volatile UINT32 *)0xfe200200)    /* Port Control Register                  */
#define PCIPDTR             ((volatile UINT32 *)0xfe200204)    /* Port Data Register                     */
#define PCIPDR              ((volatile UINT32 *)0xfe200220)    /* PIO data register                      */
#define PCIIO                   0xfe240000                     /* PCI IO space base address,accesable 8, \
                                                              16,32 according to PCI I/O space (256KB)   */


/* function declarations */

#ifndef	_ASMLANGUAGE
#if defined(__STDC__) || defined(__cplusplus)

#else	/* __STDC__ */

#endif	/* __STDC__ */

#endif	/* _ASMLANGUAGE */

#ifdef __cplusplus
}
#endif

#endif /* __INCsh7751h */




