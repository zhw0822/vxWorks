/* sigShCodes.h - SH codes sent to signal handlers */

/* Copyright 1995-2000 Wind River Systems, Inc. */

/*
modification history
--------------------
01l,21aug00,hk   merge SH7729 to SH7700. merge SH7410 and SH7040 to SH7600.
01k,10feb99,hk   clean-up SH7750 support.
01j,16jul98,st   added SH7750 support.
01j,06may98,jmc  added support for SH-DSP and SH3-DSP CPU types.
01j,26nov97,hms  added fpp support definition.
01i,22apr97,hk   changed 704X to 7040.
01h,09feb97,hk   changed FPE_INTDIV_TRAP for SH7700 to INUM_TRAP_1.
01g,19aug96,hk   changed FPE_INTDIV_TRAP for SH7700.
01f,09aug96,hk   added FPE_INTDIV_TRAP for SH7700.
01e,05aug96,hk   added TLB_xxx, BUS_xxx for SH7700, changed ILL_xxx names.
01d,04aug96,hk   enabled ILL_xxx for SH7700.
01c,15nov95,hk   reincarnated with five signal codes.
01b,01apr95,hk   disabled this file.
01a,31mar95,hk   written based on Mc68k-01b
*/

#ifndef __INCsigShCodesh
#define __INCsigShCodesh

#ifdef __cplusplus
extern "C" {
#endif

#include "iv.h"

#if (CPU==SH7750 || CPU==SH7700)
#define TLB_LOAD_MISS			INUM_TLB_READ_MISS
#define TLB_STORE_MISS			INUM_TLB_WRITE_MISS
#define TLB_INITITIAL_PAGE_WRITE	INUM_TLB_WRITE_INITIAL_PAGE
#define TLB_LOAD_PROTEC_VIOLATION	INUM_TLB_READ_PROTECTED
#define TLB_STORE_PROTEC_VIOLATION	INUM_TLB_WRITE_PROTECTED
#define BUS_LOAD_ADDRESS_ERROR		INUM_READ_ADDRESS_ERROR
#define BUS_STORE_ADDRESS_ERROR		INUM_WRITE_ADDRESS_ERROR
#define FPU_EXCEPTION			INUM_FPU_EXCEPTION
#define ILLEGAL_INSTR_GENERAL		INUM_ILLEGAL_INST_GENERAL
#define ILLEGAL_SLOT_INSTR		INUM_ILLEGAL_INST_SLOT
#define FPE_INTDIV_TRAP			INUM_TRAP_1
#elif (CPU==SH7600 || CPU==SH7000)
#define ILL_ILLINSTR_GENERAL		INUM_ILLEGAL_INST_GENERAL
#define ILL_ILLINSTR_SLOT		INUM_ILLEGAL_INST_SLOT
#define BUS_ADDERR_CPU			INUM_BUS_ERROR_CPU
#define BUS_ADDERR_DMA			INUM_BUS_ERROR_DMA
#define FPE_INTDIV_TRAP			INUM_TRAP_62
#endif /* CPU==SH7600 || CPU==SH7000 */

#ifdef __cplusplus
}
#endif

#endif /* __INCsigShCodesh */
