/* archSimnt.h - i86 VxSim specific header */

/*
 * Copyright 2003-2005 Wind River Systems, Inc.
 *
 * The right to copy, distribute or otherwise make use of this software
 * may be licensed only pursuant to the terms of an applicable Wind River
 * license agreement. No license to Wind River intellectual property rights
 * is granted herein. All rights not licensed by Wind River are reserved
 * by Wind River.
 */

/*
modification history
--------------------
01x,12apr05,kk   removed _WRS_OBJLIB_SUPPORT_VERIFY_TRAP macro (SPR# 106486)
01w,31mar05,elp  added _ARCH_SUPPORTS_PROTECT_INTERRUPT_STACK macro
01v,27aug04,dbt  Added _WRS_OBJLIB_SUPPORT_VERIFY_TRAP.
01u,18may04,tam  removed _WRS_KERNEL_TEXT_START_ADRS definition
01t,18may04,jmp  fixed _WRS_FRAMEP_FROM_JMP_BUF macro.
01s,15may04,zl   fixed _WRS_FRAMEP_FROM_JMP_BUF()
01r,14may04,jmp  removed obsolete stuff.
01q,23mar04,jmp  added support for portable trcLib.
		 added macros for getting frame and return PC from a jmp_buf.
01p,09feb04,aeg  moved _WRS_PAL_COPROC_LIB to archSimntCommon.h
01o,04dec03,dbt  Added support for DIAB compiler.
01n,02dec03,dbt  Updated power management support.
01m,25nov03,pes  Move definition of _BYTE_ORDER to arch<Arch>Common.h.
01l,14nov03,job  Moved blib stuff to archSimntCommon.h
01k,13nov03,pes  Add include of common header
01j,23sep03,dbt  Use new B6 scheduler.
01i,18aug03,dbt  Added PAL support for VxSim.
01h,22jul03,kam  removed define of WV_INSTRUMENTATION
01g,28may03,jmp  removed X86_EXT structure.
01f,22apr03,dbt  Added _WRS_ARCH_IS_SIMULATOR definition.
01e,27mar03,jmp  added X86_EXT. no longer include ntcontext.h
01d,13mar01,sn   SPR 73723 - define supported toolchains
01c,21sep01,jmp  only include ntcontext.h if _ASMLANGUAGE not defined
		 (SPR# 34669).
01b,29jul98,ms   added WV_INSTRUMENTATION
01a,03sep97,cym  written
*/

#ifndef __INCarchSimnth
#define __INCarchSimnth

#ifdef __cplusplus
extern "C" {
#endif

/* Begin PAL */

#define	_ALLOC_ALIGN_SIZE	8		/* 8-byte alignment */
#define _STACK_ALIGN_SIZE	8		/* 8-byte alignment */

#define _DYNAMIC_BUS_SIZING	FALSE		/* no dynamic bus sizing */

#define _WRS_ARCH_IS_SIMULATOR	/* This is a simulator */

#define _ARCH_SUPPORTS_GCC	/* GCC compiler is supported on this arch. */
#define _ARCH_SUPPORTS_DCC	/* DCC compiler is supported on this arch. */

#if CPU!=SIMNT
/* Used in usrDepend.c and usrKernelStack.c */

#define _ARCH_SUPPORTS_PROTECT_INTERRUPT_STACK
#endif

/* moved here from h/private/javaLibP.h */

#define JAVA_C_SYM_PREFIX	""

#ifndef _ASMLANGUAGE
/* macros for getting frame and return PC from a jmp_buf */

#define _WRS_FRAMEP_FROM_JMP_BUF(env)	((char *) (env)[0].reg.reg_fp)
#define _WRS_RET_PC_FROM_JMP_BUF(env)	((INSTR *) (env)[0].reg.reg_pc)
#endif 	/* _ASMLANGUAGE */

/* from loadElfLib.c */

#define _WRS_USE_ELF_LOAD_FORMAT

/* from xdr_float.c */

#define FLOAT_NORM

/* from qPriBMapLib.c */

#define qPriBMapLib_PORTABLE

/* from ffsLib.c */

#define ffsLib_PORTABLE

/* from schedLib.c */

#define _WRS_VXLIB_SUPPORT

#define	_WRS_BASE6_SCHEDULER
#define _WRS_ENABLE_IDLE_INTS

/* from spyLib.c */

#define _WRS_SPY_TASK_SIZE      10000

/* End PAL */

#if (CPU==SIMLINUX)
#include "arch/simlinux/archSimlinuxCommon.h"
#elif (CPU==SIMNT)
#include "arch/simnt/archSimntCommon.h"
#endif

#ifdef __cplusplus
}
#endif

#endif /* __INCarchSimnth */
