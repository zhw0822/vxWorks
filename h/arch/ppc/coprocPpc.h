/* coprocPpc.h - coprocessor management library header */

/* Copyright 2003-2004 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*
modification history
--------------------
01a,23jan04,dtr  Adding in SPE support.
01b,04feb04,aeg  removed include of altivecLib.h
01a,?????03,???  written.
*/

#ifndef __INCcoprocPpch
#define __INCcoprocPpch

#ifdef __cplusplus
extern "C" {
#endif

/* includes */

#include "taskLib.h"

/* defines */

#define VX_FP_TASK         VX_COPROC1_TASK
#define VX_ALTIVEC_TASK    VX_COPROC2_TASK
#define VX_SPE_TASK        VX_COPROC3_TASK

#ifdef __cplusplus
}
#endif

#endif /* __INCcoprocPpch */
