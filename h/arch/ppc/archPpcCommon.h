/* archPpcCommon.h - common Ppc architecture specific header */

/* Copyright 2003-2004 Wind River Systems, Inc. */

/*
modification history
--------------------
01f,01oct04,tam  moved definition of _TYPE_PHYS_ADDR to the kernel
                 architecture scpecific header
01e,22mar04,zl   moved trcLib_PORTABLE and INSTR definitions here.
01d,03feb04,aeg  added _WRS_PAL_COPROC_LIB from archPpc.h
01c,27jan04,gls  added definition of _TYPE_PHYS_ADDR
01b,12nov03,job  Moved some blib stuff from archPpc.h
01a,11nov03,pes  written.
*/

#ifndef __INCarchPpcCommonh
#define __INCarchPpcCommonh

#ifdef __cplusplus
extern "C" {
#endif

/* from bLib.c */

#define _WRS_BLIB_ALIGNMENT     3       /* quad byte alignment mask */

/* from archPpc.h */

/* Enable Coprocessor Abstraction.  For PPC32, must do all or none. */

#define _WRS_PAL_COPROC_LIB

/* from trcLib.c */

#define trcLib_PORTABLE

/* moved here from h/types/vxTypesOld.h */

#ifndef _ASMLANGUAGE
typedef unsigned long INSTR;            /* 32 bit word-aligned instructions */
#endif 	/* _ASMLANGUAGE */

#ifdef __cplusplus
}
#endif

#endif /* __INCarchPpcCommonh */

