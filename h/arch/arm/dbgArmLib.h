/* dbgArmLib.h - header file for ARM-dependent part of debugger */

/* Copyright 1996-2004 Wind River Systems, Inc. */

/*
modification history
--------------------
01g,24feb05,jb  Correct definition of SYSCALL_INST
01f,09dec04,scm  resolve DBG_SYSCALL_SIZE warning, define DBG_SYSCALL_INST
01e,11feb04,elg  Add syscall instruction size macro.
01d,12nov98,cdp  added support for ARM generic Thumb library.
01c,20apr98,dbt  modified for new breakpoint scheme
01b,24apr97,cdp  added Thumb (ARM7TDMI_T) support;
		 changed DBG_BREAK_INST for non-Thumb processors.
01a,09may96,cdp  created
*/

#ifndef	__INCdbgArmLibh
#define	__INCdbgArmLibh

#ifdef __cplusplus
extern "C" {
#endif


#include "esf.h"


#define	BREAK_ESF	ESF
#define	TRACE_ESF	ESF

#define DBG_SYSCALL_SIZE        (sizeof (INSTR) / sizeof (INSTR))
#define	DBG_SYSCALL_INST        0xEF0000FA   /* SWI */

#if ARM_THUMB
#define DBG_BREAK_INST	0xDEFE		/* The undefined instruction used as a
					 * a breakpoint for Thumb-state */
#define DBG_INST_ALIGN	2
#else
#define DBG_BREAK_INST	0xE7FDDEFE	/* The undefined instruction used as a
					 * a breakpoint for ARM-state */
#define DBG_INST_ALIGN	4
#endif	/* ARM_THUMB */

#define DBG_NO_SINGLE_STEP	1	/* no hardware trace support */


#ifdef __cplusplus
}
#endif

#endif	/* __INCdbgArmLibh */
