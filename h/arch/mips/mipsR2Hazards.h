/* mipsR2Hazards.h - assembler definitions header file */

/* Copyright  2005 Wind River Systems, Inc. */

/*
modification history
--------------------
01c,11apr05,d_c  rename mti24kxHazards.h to mipsR2Hazards.h
01b,24feb05,pes  Modified to use the 'ehb' instruction, either by
		 macro or actual instruction (if supported by assembler)
01a,16feb05,pes  Created from mti5kxHazards.h
*/

/* These hazard macros are intended for use with MIPS architecture-
 * dependent assembly files which require handling hazards.
 * These macros support release 2 ISA cores
 * This file is included by the asmMips.h file when the HAZARD_FILE macro
 * is defined using
 * #define HAZARD_FILE "mipsR2Hazards.h"
 */

#ifndef __INCmipsR2Hazardsh
#define __INCmipsR2Hazardsh

/* Hazard macros */
#if defined (_WRS_MIPS_ENABLE_R2_ISA)
#define EHB	ehb
#else
#define EHB	.word	0x000000c0 /* ehb opcode */
#endif /* _WRS_MIPS_ENABLE_R2_ISA */

#define HAZARD_TLB		EHB
#define HAZARD_ERET		EHB
#define HAZARD_CP_READ		EHB
#define HAZARD_CP_WRITE		EHB
#define HAZARD_CACHE_TAG	EHB
#define HAZARD_CACHE		EHB
#define HAZARD_INTERRUPT	EHB

#endif /* __INCmipsR2Hazardsh */

