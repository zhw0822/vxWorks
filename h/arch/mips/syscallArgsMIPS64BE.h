/* Aligned for MIPS64BE - MIPS64 Big-Endian */


/* THIS FILE IS AUTO_GENERATED. PLEASE DO NOT EDIT. */

#ifndef _ASMLANGUAGE

#include <vxWorks.h>
#include <taskLib.h>
#include <rtpLib.h>
#include <sdLib.h>
#include <shlLib.h>
#include <signal.h>
#include <socket.h>
#include <time.h>
#include <setjmp.h>
#include <resolv/nameser.h>
#include <resolv/resolv.h>
#include <private/semSysCall.h>
#include <private/objLibP.h>
#include <private/rtpLibP.h>
#include <private/mqPxSysCall.h>
#include <private/pxObjSysCall.h>
#include <private/semPxSysCall.h>
#include <private/taskSysCall.h>
#include <private/timerSysCall.h>
#include <private/eventSysCall.h>
#include <sys/resource.h>

struct _exitScArgs 
	{
	UINT32 statusPad;
	int status;
	};

struct creatScArgs 
	{
	UINT32 namePad;
	const char * name;
	UINT32 flagPad;
	mode_t flag;
	};

struct _openScArgs 
	{
	UINT32 namePad;
	const char * name;
	UINT32 flagsPad;
	int flags;
	UINT32 modePad;
	int mode;
	};

struct closeScArgs 
	{
	UINT32 fdPad;
	int fd;
	};

struct readScArgs 
	{
	UINT32 fdPad;
	int fd;
	UINT32 bufferPad;
	void * buffer;
	UINT32 maxbytesPad;
	size_t maxbytes;
	};

struct writeScArgs 
	{
	UINT32 fdPad;
	int fd;
	UINT32 bufferPad;
	const void * buffer;
	UINT32 nbytesPad;
	size_t nbytes;
	};

struct _ioctlScArgs 
	{
	UINT32 fdPad;
	int fd;
	UINT32 functionPad;
	int function;
	UINT32 argPad;
	int arg;
	};

struct dupScArgs 
	{
	UINT32 fdPad;
	int fd;
	};

struct dup2ScArgs 
	{
	UINT32 fdPad;
	int fd;
	UINT32 fd2Pad;
	int fd2;
	};

struct pipeScArgs 
	{
	int filedes[2];
	};

struct removeScArgs 
	{
	UINT32 namePad;
	const char * name;
	};

struct selectScArgs 
	{
	UINT32 widthPad;
	int width;
	UINT32 readFdsPad;
	fd_set * readFds;
	UINT32 writeFdsPad;
	fd_set * writeFds;
	UINT32 excFdsPad;
	fd_set * excFds;
	UINT32 timeoutPad;
	struct timeval * timeout;
	};

struct socketScArgs 
	{
	UINT32 domainPad;
	int domain;
	UINT32 typePad;
	int type;
	UINT32 protocolPad;
	int protocol;
	};

struct bindScArgs 
	{
	UINT32 sPad;
	int s;
	UINT32 namePad;
	struct sockaddr * name;
	UINT32 namelenPad;
	int namelen;
	};

struct listenScArgs 
	{
	UINT32 sPad;
	int s;
	UINT32 backlogPad;
	int backlog;
	};

struct acceptScArgs 
	{
	UINT32 sPad;
	int s;
	UINT32 addrPad;
	struct sockaddr * addr;
	UINT32 addrlenPad;
	int *addrlen;
	};

struct connectScArgs 
	{
	UINT32 sPad;
	int s;
	UINT32 namePad;
	struct sockaddr * name;
	UINT32 namelenPad;
	int namelen;
	};

struct sendtoScArgs 
	{
	UINT32 sPad;
	int s;
	UINT32 bufPad;
	caddr_t buf;
	UINT32 bufLenPad;
	int bufLen;
	UINT32 flagsPad;
	int flags;
	UINT32 toPad;
	struct sockaddr * to;
	UINT32 tolenPad;
	int tolen;
	};

struct sendScArgs 
	{
	UINT32 sPad;
	int s;
	UINT32 bufPad;
	const char * buf;
	UINT32 bufLenPad;
	int bufLen;
	UINT32 flagsPad;
	int flags;
	};

struct sendmsgScArgs 
	{
	UINT32 sPad;
	int s;
	UINT32 mpPad;
	struct msghdr * mp;
	UINT32 flagsPad;
	int flags;
	};

struct recvfromScArgs 
	{
	UINT32 sPad;
	int s;
	UINT32 bufPad;
	char * buf;
	UINT32 bufLenPad;
	int bufLen;
	UINT32 flagsPad;
	int flags;
	UINT32 fromPad;
	struct sockaddr *from;
	UINT32 pFromLenPad;
	int * pFromLen;
	};

struct recvScArgs 
	{
	UINT32 sPad;
	int s;
	UINT32 bufPad;
	char * buf;
	UINT32 bufLenPad;
	int bufLen;
	UINT32 flagsPad;
	int flags;
	};

struct recvmsgScArgs 
	{
	UINT32 sPad;
	int s;
	UINT32 mpPad;
	struct msghdr * mp;
	UINT32 flagsPad;
	int flags;
	};

struct setsockoptScArgs 
	{
	UINT32 sPad;
	int s;
	UINT32 levelPad;
	int level;
	UINT32 optnamePad;
	int optname;
	UINT32 optvalPad;
	char * optval;
	UINT32 optlenPad;
	int optlen;
	};

struct getsockoptScArgs 
	{
	UINT32 sPad;
	int s;
	UINT32 levelPad;
	int level;
	UINT32 optnamePad;
	int optname;
	UINT32 optvalPad;
	char * optval;
	UINT32 optlenPad;
	int * optlen;
	};

struct getsocknameScArgs 
	{
	UINT32 sPad;
	int s;
	UINT32 namePad;
	struct sockaddr *name;
	UINT32 namelenPad;
	int * namelen;
	};

struct getpeernameScArgs 
	{
	UINT32 sPad;
	int s;
	UINT32 namePad;
	struct sockaddr *name;
	UINT32 namelenPad;
	int *namelen;
	};

struct shutdownScArgs 
	{
	UINT32 sPad;
	int s;
	UINT32 howPad;
	int how;
	};

struct mmapScArgs 
	{
	UINT32 addrPad;
	void * addr;
	UINT32 lenPad;
	size_t len;
	UINT32 protPad;
	int prot;
	UINT32 flagsPad;
	int flags;
	UINT32 fildesPad;
	int fildes;
	off_t64 off;
	};

struct munmapScArgs 
	{
	UINT32 addrPad;
	void * addr;
	UINT32 lenPad;
	size_t len;
	};

struct mprotectScArgs 
	{
	UINT32 addrPad;
	void * addr;
	UINT32 lenPad;
	size_t len;
	UINT32 protPad;
	int prot;
	};

struct killScArgs 
	{
	UINT32 rtpIdPad;
	OBJ_HANDLE rtpId;
	UINT32 signoPad;
	int signo;
	};

struct sigpendingScArgs 
	{
	UINT32 pSetPad;
	sigset_t * pSet;
	};

struct sigprocmaskScArgs 
	{
	UINT32 howPad;
	int how;
	UINT32 pSetPad;
	const sigset_t * pSet;
	UINT32 pOsetPad;
	sigset_t * pOset;
	};

struct _sigqueueScArgs 
	{
	UINT32 rtpIdPad;
	OBJ_HANDLE rtpId;
	UINT32 signoPad;
	int signo;
	UINT32 pValuePad;
	const union sigval * pValue;
	UINT32 sigCodePad;
	int sigCode;
	};

struct sigsuspendScArgs 
	{
	UINT32 pSetPad;
	const sigset_t * pSet;
	};

struct sigtimedwaitScArgs 
	{
	UINT32 pSetPad;
	const sigset_t * pSet;
	UINT32 pInfoPad;
	struct siginfo * pInfo;
	UINT32 pTimePad;
	const struct timespec * pTime;
	};

struct _sigactionScArgs 
	{
	UINT32 signoPad;
	int signo;
	UINT32 pActPad;
	const struct sigaction * pAct;
	UINT32 pOactPad;
	struct sigaction * pOact;
	UINT32 retAddrPad;
	void * retAddr;
	};

struct chdirScArgs 
	{
	UINT32 namePad;
	const char * name;
	};

struct _getcwdScArgs 
	{
	UINT32 bufferPad;
	char * buffer;
	UINT32 lengthPad;
	int length;
	};

struct waitpidScArgs 
	{
	UINT32 childRtpIdPad;
	OBJ_HANDLE childRtpId;
	UINT32 pStatusPad;
	int * pStatus;
	UINT32 optionsPad;
	int options;
	};

struct sysctlScArgs 
	{
	UINT32 pNamePad;
	int *pName;
	UINT32 nameLenPad;
	u_int nameLen;
	UINT32 pOldPad;
	void * pOld;
	UINT32 pOldLenPad;
	size_t * pOldLen;
	UINT32 pNewPad;
	void * pNew;
	UINT32 newLenPad;
	size_t newLen;
	};

struct _schedPxInfoGetScArgs 
	{
	UINT32 pTimeSlicingOnPad;
	BOOL * pTimeSlicingOn;
	UINT32 pTimeSlicePeriodPad;
	ULONG * pTimeSlicePeriod;
	};

struct sigaltstackScArgs 
	{
	UINT32 ssPad;
	stack_t * ss;
	UINT32 ossPad;
	stack_t * oss;
	};

struct unlinkScArgs 
	{
	UINT32 namePad;
	const char * name;
	};

struct linkScArgs 
	{
	UINT32 namePad;
	const char * name;
	UINT32 newnamePad;
	const char * newname;
	};

struct fsyncScArgs 
	{
	UINT32 fdPad;
	int fd;
	};

struct fdatasyncScArgs 
	{
	UINT32 fdPad;
	int fd;
	};

struct renameScArgs 
	{
	UINT32 oldnamePad;
	const char * oldname;
	UINT32 newnamePad;
	const char * newname;
	};

struct fpathconfScArgs 
	{
	UINT32 fdPad;
	int fd;
	UINT32 namePad;
	int name;
	};

struct pathconfScArgs 
	{
	UINT32 pathPad;
	const char * path;
	UINT32 namePad;
	int name;
	};

struct accessScArgs 
	{
	UINT32 pathPad;
	const char * path;
	UINT32 amodePad;
	int amode;
	};

struct chmodScArgs 
	{
	UINT32 pathPad;
	const char * path;
	UINT32 modePad;
	mode_t mode;
	};

struct eventReceiveScArgs 
	{
	UINT32 eventsPad;
	UINT32 events;
	UINT32 optionsPad;
	UINT32 options;
	UINT32 timeoutPad;
	int timeout;
	UINT32 pEvtsReceivedPad;
	UINT32 * pEvtsReceived;
	};

struct eventSendScArgs 
	{
	UINT32 taskIdPad;
	int taskId;
	UINT32 eventsPad;
	UINT32 events;
	};

struct eventCtlScArgs 
	{
	UINT32 handlePad;
	OBJ_HANDLE handle;
	UINT32 commandPad;
	VX_EVT_CTL_CMD command;
	UINT32 pArgPad;
	void * pArg;
	UINT32 pArgSizePad;
	UINT * pArgSize;
	};

struct msgQSendScArgs 
	{
	UINT32 handlePad;
	OBJ_HANDLE handle;
	UINT32 bufferPad;
	char * buffer;
	UINT32 nBytesPad;
	UINT nBytes;
	UINT32 timeoutPad;
	int timeout;
	UINT32 priorityPad;
	int priority;
	};

struct msgQReceiveScArgs 
	{
	UINT32 handlePad;
	OBJ_HANDLE handle;
	UINT32 bufferPad;
	char * buffer;
	UINT32 maxNBytesPad;
	UINT maxNBytes;
	UINT32 timeoutPad;
	int timeout;
	};

struct _msgQOpenScArgs 
	{
	UINT32 namePad;
	const char * name;
	UINT32 maxMsgsPad;
	UINT maxMsgs;
	UINT32 maxMsgLengthPad;
	UINT maxMsgLength;
	UINT32 optionsPad;
	int options;
	UINT32 modePad;
	int mode;
	UINT32 contextPad;
	void * context;
	};

struct objDeleteScArgs 
	{
	UINT32 handlePad;
	OBJ_HANDLE handle;
	UINT32 optionsPad;
	int options;
	};

struct objInfoGetScArgs 
	{
	UINT32 handlePad;
	OBJ_HANDLE handle;
	UINT32 pInfoPad;
	void * pInfo;
	UINT32 pInfoSizePad;
	UINT * pInfoSize;
	UINT32 levelPad;
	int level;
	};

struct _semTakeScArgs 
	{
	UINT32 handlePad;
	OBJ_HANDLE handle;
	UINT32 timeoutPad;
	int timeout;
	};

struct _semGiveScArgs 
	{
	UINT32 handlePad;
	OBJ_HANDLE handle;
	};

struct _semOpenScArgs 
	{
	UINT32 namePad;
	const char * name;
	UINT32 typePad;
	SEM_TYPE type;
	UINT32 initStatePad;
	int initState;
	UINT32 optionsPad;
	int options;
	UINT32 modePad;
	int mode;
	UINT32 contextPad;
	void * context;
	};

struct semCtlScArgs 
	{
	UINT32 handlePad;
	OBJ_HANDLE handle;
	UINT32 commandPad;
	VX_SEM_CTL_CMD command;
	UINT32 pArgPad;
	void * pArg;
	UINT32 pArgSizePad;
	UINT * pArgSize;
	};

struct _taskOpenScArgs 
	{
	UINT32 pArgsPad;
	struct vx_task_open_sc_args * pArgs;
	};

struct taskCtlScArgs 
	{
	UINT32 tidPad;
	int tid;
	UINT32 commandPad;
	VX_TASK_CTL_CMD command;
	UINT32 pArgPad;
	void * pArg;
	UINT32 pArgSizePad;
	UINT * pArgSize;
	};

struct taskDelayScArgs 
	{
	UINT32 ticksPad;
	int ticks;
	};

struct rtpSpawnScArgs 
	{
	UINT32 rtpFileNamePad;
	const char * rtpFileName;
	UINT32 argvPad;
	const char ** argv;
	UINT32 envpPad;
	const char ** envp;
	UINT32 priorityPad;
	int priority;
	UINT32 uStackSizePad;
	int uStackSize;
	UINT32 optionsPad;
	int options;
	UINT32 taskOptionsPad;
	int taskOptions;
	};

struct rtpInfoGetScArgs 
	{
	UINT32 rtpIdPad;
	OBJ_HANDLE rtpId;
	UINT32 rtpStructPad;
	RTP_DESC * rtpStruct;
	};

struct taskKillScArgs 
	{
	UINT32 taskHandlePad;
	OBJ_HANDLE taskHandle;
	UINT32 signoPad;
	int signo;
	};

struct _taskSigqueueScArgs 
	{
	UINT32 taskHandlePad;
	OBJ_HANDLE taskHandle;
	UINT32 signoPad;
	int signo;
	UINT32 pValuePad;
	const union sigval * pValue;
	UINT32 sigCodePad;
	int sigCode;
	};

struct _timer_openScArgs 
	{
	UINT32 namePad;
	const char * name;
	UINT32 modePad;
	int mode;
	UINT32 clockIdPad;
	clockid_t clockId;
	UINT32 evpPad;
	struct sigevent * evp;
	UINT32 contextPad;
	void * context;
	};

struct timerCtlScArgs 
	{
	UINT32 cmdCodePad;
	TIMER_CTL_CMD cmdCode;
	UINT32 handlePad;
	int handle;
	UINT32 pArgsPad;
	void * pArgs;
	UINT32 pArgSizePad;
	UINT * pArgSize;
	};

struct pxOpenScArgs 
	{
	UINT32 typePad;
	PX_OBJ_TYPE type;
	UINT32 namePad;
	const char * name;
	UINT32 objOpenModePad;
	int objOpenMode;
	UINT32 attrPad;
	void * attr;
	};

struct pxCloseScArgs 
	{
	UINT32 handlePad;
	OBJ_HANDLE handle;
	};

struct pxUnlinkScArgs 
	{
	UINT32 typePad;
	PX_OBJ_TYPE type;
	UINT32 namePad;
	const char * name;
	};

struct pxCtlScArgs 
	{
	UINT32 handlePad;
	OBJ_HANDLE handle;
	UINT32 cmdCodePad;
	PX_CTL_CMD_CODE cmdCode;
	UINT32 pArgsPad;
	void * pArgs;
	UINT32 pArgSizePad;
	UINT * pArgSize;
	};

struct pxMqReceiveScArgs 
	{
	UINT32 handlePad;
	OBJ_HANDLE handle;
	UINT32 pMsgPad;
	char * pMsg;
	UINT32 msgLenPad;
	size_t msgLen;
	UINT32 pMsgPrioPad;
	unsigned int * pMsgPrio;
	UINT32 waitOptionPad;
	PX_WAIT_OPTION waitOption;
	UINT32 time_outPad;
	struct timespec * time_out;
	};

struct pxMqSendScArgs 
	{
	UINT32 handlePad;
	OBJ_HANDLE handle;
	UINT32 pMsgPad;
	const char * pMsg;
	UINT32 msgLenPad;
	size_t msgLen;
	UINT32 msgPrioPad;
	unsigned int msgPrio;
	UINT32 waitOptionPad;
	PX_WAIT_OPTION waitOption;
	UINT32 time_outPad;
	struct timespec * time_out;
	};

struct pxSemWaitScArgs 
	{
	UINT32 handlePad;
	OBJ_HANDLE handle;
	UINT32 waitOptionPad;
	PX_WAIT_OPTION waitOption;
	UINT32 time_outPad;
	struct timespec * time_out;
	};

struct pxSemPostScArgs 
	{
	UINT32 handlePad;
	OBJ_HANDLE handle;
	};

struct pipeDevCreateScArgs 
	{
	UINT32 namePad;
	const char * name;
	UINT32 nMessagesPad;
	int nMessages;
	UINT32 nBytesPad;
	int nBytes;
	};

struct pipeDevDeleteScArgs 
	{
	UINT32 namePad;
	const char * name;
	UINT32 forcePad;
	BOOL force;
	};

struct _sdCreateScArgs 
	{
	UINT32 namePad;
	char * name;
	UINT32 optionsPad;
	int options;
	UINT32 sizePad;
	UINT32 size;
	off_t64 physAddress;
	UINT32 attrPad;
	MMU_ATTR attr;
	UINT32 pVirtAddressPad;
	void ** pVirtAddress;
	};

struct _sdOpenScArgs 
	{
	UINT32 namePad;
	char * name;
	UINT32 optionsPad;
	int options;
	UINT32 modePad;
	int mode;
	UINT32 sizePad;
	UINT32 size;
	off_t64 physAddress;
	UINT32 attrPad;
	MMU_ATTR attr;
	UINT32 pVirtAddressPad;
	void ** pVirtAddress;
	};

struct sdDeleteScArgs 
	{
	UINT32 handlePad;
	OBJ_HANDLE handle;
	UINT32 optionsPad;
	int options;
	};

struct sdMapScArgs 
	{
	UINT32 handlePad;
	OBJ_HANDLE handle;
	UINT32 attrPad;
	MMU_ATTR attr;
	UINT32 optionsPad;
	int options;
	};

struct sdUnmapScArgs 
	{
	UINT32 handlePad;
	OBJ_HANDLE handle;
	UINT32 optionsPad;
	int options;
	};

struct sdProtectScArgs 
	{
	UINT32 handlePad;
	OBJ_HANDLE handle;
	UINT32 attrPad;
	MMU_ATTR attr;
	};

struct _edrErrorInjectScArgs 
	{
	UINT32 kindPad;
	int kind;
	UINT32 fileNamePad;
	const char *fileName;
	UINT32 lineNumberPad;
	int lineNumber;
	UINT32 regsetPad;
	REG_SET *regset;
	UINT32 addrPad;
	void *addr;
	UINT32 msgPad;
	const char *msg;
	};

struct resSearchNScArgs 
	{
	UINT32 namePad;
	const char * name;
	UINT32 targetPad;
	struct res_target * target;
	};

struct wvEventScArgs 
	{
	UINT32 eventIdPad;
	int eventId;
	UINT32 pDataPad;
	const char * pData;
	UINT32 dataSizePad;
	unsigned int dataSize;
	};

struct rtpVarAddScArgs 
	{
	UINT32 pVarPad;
	void ** pVar;
	UINT32 valuePad;
	void * value;
	};

struct sdInfoGetScArgs 
	{
	UINT32 handlePad;
	OBJ_HANDLE handle;
	UINT32 pSdStructPad;
	SD_DESC * pSdStruct;
	};

struct _shlOpenScArgs 
	{
	UINT32 namePad;
	const char * name;
	UINT32 optionsPad;
	int options;
	};

struct _shlUnlockScArgs 
	{
	UINT32 shlIdPad;
	SHL_ID shlId;
	};

struct _shlCloseScArgs 
	{
	UINT32 shlIdPad;
	SHL_ID shlId;
	};

struct _shlGetScArgs 
	{
	UINT32 shlIdPad;
	SHL_ID shlId;
	UINT32 pInfoPad;
	SHLINFO *pInfo;
	};

struct _shlPutScArgs 
	{
	UINT32 shlIdPad;
	SHL_ID shlId;
	UINT32 pInfoPad;
	const SHLINFO *pInfo;
	};

struct objUnlinkScArgs 
	{
	UINT32 namePad;
	const char * name;
	UINT32 classTypePad;
	enum windObjClassType classType;
	};

struct getprlimitScArgs 
	{
	UINT32 idtypePad;
	int idtype;
	UINT32 handlePad;
	OBJ_HANDLE handle;
	UINT32 resourcePad;
	int resource;
	UINT32 rlpPad;
	struct rlimit *rlp;
	};

struct setprlimitScArgs 
	{
	UINT32 idtypePad;
	int idtype;
	UINT32 handlePad;
	OBJ_HANDLE handle;
	UINT32 resourcePad;
	int resource;
	UINT32 rlpPad;
	struct rlimit *rlp;
	};

struct _mctlScArgs 
	{
	UINT32 addrPad;
	void * addr;
	UINT32 lenPad;
	size_t len;
	UINT32 functionPad;
	int function;
	UINT32 argPad;
	int arg;
	};

#endif	/* _ASMLANGUAGE */
