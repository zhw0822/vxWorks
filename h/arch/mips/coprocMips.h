/* coprocMips.h - coprocessor management library header */

/*
 * Copyright (C) 2003, 2005 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01b,27jul05,pes  Add define for VX_DSP_TASK
01a,11apr03,pes  Created
*/

#ifndef __INCcoprocMipsh
#define __INCcoprocMipsh

#ifdef __cplusplus
extern "C" {
#endif

/* inlcudes */

#include "taskLib.h"

/* defines */

#define VX_FP_TASK	VX_COPROC1_TASK
#define VX_DSP_TASK	VX_COPROC2_TASK

#ifdef __cplusplus
}
#endif

#endif /* __INCcoprocMipsh */
