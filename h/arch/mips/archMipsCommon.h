/* archMipsCommon.h - common Mips architecture specific header */

/* Copyright 2003-2004 Wind River Systems, Inc. */

/*
 * This file has been developed or significantly modified by the
 * MIPS Center of Excellence Dedicated Engineering Staff.
 * This notice is as per the MIPS Center of Excellence Master Partner
 * Agreement, do not remove this notice without checking first with
 * WR/Platforms MIPS Center of Excellence engineering management.
 */

/*
modification history
--------------------
01h,30sep04,pes  added define of _WRS_NO_BITS_PHYS_ADDR
01g,01oct04,tam  moved definition of _TYPE_PHYS_ADDR to the kernel
                 architecture scpecific header
01f,17sep04,pes  moved _TYPE_PHYS_ADDDR and _WRS_PHYS_ADDR_IS_64_BITS here
                 from archMips.h
01e,22mar04,zl   moved trcLib_PORTABLE, _RType and INSTR definitions here.
01d,17mar04,pes  Enable Coprocessor Abstraction
01c,25nov03,pes  Moved definition of _BYTE_ORDER to archMipsCommon.h.
01b,12nov03,job  Moved some blib stuff from archMips.h
01a,11nov03,pes  written.
*/

#ifndef __INCarchMipsCommonh
#define __INCarchMipsCommonh

#ifdef __cplusplus
extern "C" {
#endif

#define _WRS_PAL_COPROC_LIB		/* enable coprocessor abstraction */

/* from bLib.c */

#define _WRS_BLIB_ALIGNMENT     3       /* quad byte alignment mask */
#define swab_PORTABLE

#define trcLib_PORTABLE			/* dbgTrcLib.c */

#ifndef _ASMLANGUAGE
typedef unsigned long INSTR;		/* 32 bit word-aligned instructions */

#if	(CPU == MIPS32)
typedef unsigned long _RType;		/* registers are 32 bits */
#elif	(CPU == MIPS64)
typedef unsigned long long _RType;	/* registers are 64 bits */
#else	/* CPU */
#error "Invalid CPU value"
#endif	/* CPU */

#endif	/* _ASMLANGUAGE */
#define _WRS_NO_BITS_PHYS_ADDR	64

#if defined(MIPSEB) || defined(__MIPSEB__)
#undef _BYTE_ORDER
#define	_BYTE_ORDER             _BIG_ENDIAN
#elif defined(MIPSEL) || defined(__MIPSEL__)
#undef _BYTE_ORDER
#define	_BYTE_ORDER		_LITTLE_ENDIAN
#else
#warning "One of MIPSEL or MIPSEB must be defined"
#endif

#ifdef __cplusplus
}
#endif

#endif /* __INCarchMipsCommonh */




