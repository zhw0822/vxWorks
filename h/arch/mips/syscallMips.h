/* syscallMips.h - MIPS specific System Call Infrastructure header */

/* Copyright 1984-2004 Wind River Systems, Inc. */

/*
modification history
--------------------
01h,19aug04,pes  Remove stack pointer range checking until another validation
                 strategy can be identified.
01g,10mar04,pes  *Really* finish implementation of 01e.
01f,10mar04,pes  Finish implementation of 01e.
01e,10mar04,pes  _STACK_ALIGN_SIZE has been re-defined to 8, so we no longer
                 need a custom vlue for the stack alignment test.
01d,12jan04,pes  Modify stack alignment test. _STACK_ALIGN_SIZE is 16 bytes,
                 but in practice only 64-bit (8 byte) alignment is enforced.
01c,30oct03,pcm  implemented changes from code review
01b,21oct03,pcm  changed pStackLimit to pStackEnd
01a,18sep03,pes  written.
*/

/*
DESCRIPTION
This header contains MIPS-specific definitions and constants used by
the System Call Infrastructure library.

*/

#ifndef __INCsyscallMipsh
#define __INCsyscallMipsh

#ifndef _ASMLANGUAGE
#include "taskLib.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#define syscallDispatch_PORTABLE	/* Use the portable dispatcher */

    /* defines */

#if (_WRS_INT_REGISTER_SIZE == 4)
#define SYSCALL_ENTRY_FRAME_SIZE	56
#elif (_WRS_INT_REGISTER_SIZE == 8)
#define SYSCALL_ENTRY_FRAME_SIZE	88
#else
#error "_WRS_INT_REGISTER_SIZE not defined."
#endif

#ifndef _ASMLANGUAGE


typedef struct sycall_mips_stack_args
    {
    _RType args[4];
    } SYSCALL_MIPS_STACK_ARGS;

/*
 * this macro is used to validate the stack pointer and copy necessary
 * arguments from the user stack to the kernel stack only after such
 * validation has been successfully completed. XXX This needs _WRS. PES
 */

#if defined (_WRS_USER_SP_RANGE_CHECK)
#define _SYSCALL_USER_SP_CHECK(sp) \
  { \
  register SYSCALL_MIPS_STACK_ARGS *src; \
  register SYSCALL_MIPS_STACK_ARGS *dst; \
  if (((UINT32)(sp) < (UINT32)(taskIdCurrent->pStackEnd)) || \
      ((UINT32)(sp) > (UINT32)(taskIdCurrent->pStackBase)) || \
      ((UINT32)(sp) & (_STACK_ALIGN_SIZE - 1))) \
      { \
      errnoSet (S_syscallLib_INVALID_USER_SP); \
      return (ERROR); \
      } \
  src = (SYSCALL_MIPS_STACK_ARGS *)&(((SYSCALL_ENTRY_STATE *)(sp))->args[4]); \
  dst = (SYSCALL_MIPS_STACK_ARGS *)&(pState->args[4]); \
  *dst = *src; \
  }
#else /* _WRS_USER_SP_RANGE_CHECK */
#define _SYSCALL_USER_SP_CHECK(sp) \
  { \
  register SYSCALL_MIPS_STACK_ARGS *src; \
  register SYSCALL_MIPS_STACK_ARGS *dst; \
  if (((UINT32)(sp) & (_STACK_ALIGN_SIZE - 1))) \
      { \
      errnoSet (S_syscallLib_INVALID_USER_SP); \
      return (ERROR); \
      } \
  src = (SYSCALL_MIPS_STACK_ARGS *)&(((SYSCALL_ENTRY_STATE *)(sp))->args[4]); \
  dst = (SYSCALL_MIPS_STACK_ARGS *)&(pState->args[4]); \
  *dst = *src; \
  }
#endif /* _WRS_USER_SP_RANGE_CHECK */
		    
    /* typedefs */

    /* 
     * The SYSCALL_ENTRY_STATE structure defines the saved machine state
     * when the system call trap is taken. This information is architecture
     * specific, and is used by the system call dispatcher to restart system
     * calls that have been interrupted by the occurance of signals.
     * System call restart is achieved by restoring the saved state at the
     * time of the system call trap, and re-issuing the system call.
     *
     * All arguments to the system call are saved on the kernel stack, and 
     * the address of that array is passed as an argument to the dispatcher.
     * The layout of this structure must exactly match the ordering of members
     * of the system call entry frame in src/arch/Mips/syscallALib.s.
     *
     */

typedef struct syscall_entry_state 
    {
    _RType	args[8];        /* argument list (a0-a3, user stack) */
    int		scn;		/* System Call Number (SCN) in v0 */
    int  *	pUStack;	/* user-mode stack pointer */
    void *	pSdata;		/* small data pointer (gp) */
    void *	pc;		/* Trap return address (EPC) */
    void *	ra;		/* saved return address register */
    int		statusReg;	/* saved Status Register (SR) */ 
    } SYSCALL_ENTRY_STATE;

#endif /* _ASMLANGUAGE */

#ifdef __cplusplus
}
#endif

#endif /* __INCsyscallMipsh */
