/* archMips.h - MIPS architecture specific header */

/*
 * Copyright (c) 1992, 1993, 1995-1997, 1999-2005 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */


/*
 * This file has been developed or significantly modified by the
 * MIPS Center of Excellence Dedicated Engineering Staff.
 * This notice is as per the MIPS Center of Excellence Master Partner
 * Agreement, do not remove this notice without checking first with
 * WR/Platforms MIPS Center of Excellence engineering management.
 */

/*
modification history
--------------------
02y,29aug05,h_k  added _WRS_SUPPORT_CACHE_XLATE and _WRS_SUPPORT_CACHE_LOCK.
02x,12aug05,pes  Add SR_MX bit, define _WRS_DSP_SUPPORT.
02w,12apr05,kk   remove _WRS_OBJLIB_SUPPORT_VERIFY_TRAP macro (SPR# 106486)
02v,24feb05,slk  SPR 102772: add _ARCH_SUPPORTS_PROTECT_INTERRUPT_STACK
                 and _WRS_STACK_PROTECT_REQUIRES_MMU macros.
02u,10dec04,slk  SPR 104530.  Add define for PM_MMULESS_PAGE_SIZE used by
                 pmLib.c.  This needs to be changed for MIPS to 8192 so the
                 ED&R area is created on an 8K boundary.
02t,09dec04,pes  SPR 103585 et.al.: Added _WRS_NONGLOBAL_NULL_PAGE define.
                 Triggers conditional code in aimMmuLib.c.
02s,01nov04,slk  SPR 92598: Add support for OSM event handling
02r,15oct04,pes  Corrected copyright notice. Add definition of BEV_VEC
02q,01oct04,tam  moved definition of _TYPE_PHYS_ADDR from shared file to
                 kernel header
02p,17sep04,pes  Move _TYPE_PHYS_ADDDR and _WRS_PHYS_ADDR_IS_64_BITS to
                 archMipsCommon.h.
02o,02sep04,pes  Add definition of _WRS_LINKER_DATA_SEG_ALIGN
02n,18may04,mem  Remove R3k.
02m,28apr04,pes  Switch to portable scheduler.
02m,27apr04,cjj  Defined workQLib_PORTABLE
02l,22mar04,zl   moved trcLib_PORTABLE, _RType and INSTR to archMipsCommon.h;
                 added _WRS_FRAMEP_FROM_JMP_BUF and _WRS_RET_PC_FROM_JMP_BUF.
02k,26mar04,slk  Add configuration 1 register definitions
02j,10mar04,pes  *Really* finish implementation of 02h.
02i,10mar04,pes  Finish implementation of 02h.
02h,10mar04,pes  Change _STACK_ALIGN_SIZE to 8 to match compiler behavior.
02g,13feb04,pes  Add typedef override for _TYPE_PHYS_ADDR. Define
                 trcLib_PORTABLE.
02f,07dec03,pes  Removed definitions of _ARCH_LONG_MIN and _ARCH_INT_MIN in
                 favor of those in vxTypesBase.h
02e,25nov03,pes  Moved definition of _BYTE_ORDER to archMipsCommon.h.
02d,14nov03,job  Moved some blib stuff to archMipsCommon.h
02c,13nov03,pes  Add include of archMipsCommon.h
02c,29oct03,pes  Adjust Address mapping macros for greater flexibility.
02b,28oct03,pcm  enabled base 6 scheduler
02a,27aug03,to   added defines for semLib.
01z,07may03,pes  More conditional compilation cleanup.
01y,29apr03,pes  PAL conditional compilation cleanup.
01x,03oct02,jmt  Allow for override of _CACHE_ALIGN_SIZE
01w,13mar01,sn   SPR 73723 - define supported toolchains
01v,09jan02,tlc  Remove inappropriate TLB definitions.
01u,04oct01,agf  add TLB definitions and constants
01t,16jul01,ros  add CofE comment
01s,07jun01,mem  Re-introduce optimized kernel.
01r,13feb01,pes  Add support for RM7000 extended interrupts
01q,05jan01,mem  Remove decl of _wdbDbgCtx routines.
01p,04jan01,agf  Adding TLB_ENTRIES define
01o,03jan01,pes  Remove definition of IS_KSEGM. This is not supported in
                 Tornado 2.
01n,20dec00,pes  Merge some definitions from target/h/arch/r4000.h.
01m,12oct99,yvp  Merge from .../tor2_0_0.coretools-baseline branch to make
                 Tor2 code work with T3 main/LATEST compiler
01l,07sep99,myz  added CW4000_16 compiler switch
01k,22jan97,alp  added  CW4000 and CW4010 support.
01l,11nov99,dra  Updated for T3 toolchain.
01k,18oct99,dra  added CW4000, CW4011, VR4100, VR5000 and VR5400 support.
		 added test for both MIPSEB and MIPSEL defined.
01k,00oct00,sn   removed varargs/typedefs for GNUC
01j,14oct96,kkk  added R4650 support.
01i,05aug96,kkk  Changed _ALLOC_ALIGN_SIZE & _STACK_ALIGN_SIZE to 16.
01i,27jun96,kkk  undo 01h.
01h,30apr96,mem  fixed varargs support for R4000, use 64 bit stack slots.
01g,27may95,cd	 switched from MIPSE[BL] to _BYTE_ORDER for ansi compliance
01f,15oct93,cd   added support for R4000, SDE compiler and 64 bit kernel.
01e,22sep92,rrr  added support for c++
01d,07jun92,ajm  added _ARCH_MULTIPLE_CACHES for cache init
01c,03jun92,ajm  updated file name referenced to match real name
01b,26may92,rrr  the tree shuffle
01a,22apr92,yao  written.
*/

#ifndef __INCarchMipsh
#define __INCarchMipsh

#ifdef __cplusplus
extern "C" {
#endif

#define _ARCH_SUPPORTS_GCC
#define _ARCH_SUPPORTS_DCC

#define	_ARCH_MULTIPLE_CACHELIB		TRUE

#define _WRS_SUPPORT_CACHE_XLATE		/* cache virt-phys translate */
#define _WRS_SUPPORT_CACHE_LOCK			/* cache lock feature */

#define _DYNAMIC_BUS_SIZING		FALSE	/* no dynamic bus sizing */
#define	_ALLOC_ALIGN_SIZE		16
#define _STACK_ALIGN_SIZE		8

#ifdef _WRS_MIPS_CPU_TX79XX
#define _CACHE_ALIGN_SIZE		64 	/* max cache line size */
#else   /* _WRS_CPU_TX79XX */
#define _CACHE_ALIGN_SIZE		32 	/* max cache line size */
#endif  /* _WRS_CPU_TX79XX */

#if	(CPU == MIPS32)

#define _WRS_INT_REGISTER_SIZE		4
#define _WRS_INT_REGISTER_SHIFT		2
#define _WRS_FP_REGISTER_SIZE		4
#define _WRS_FP_REGISTER_SHIFT		2

#elif	(CPU == MIPS64)

#define _WRS_INT_REGISTER_SIZE		8
#define _WRS_INT_REGISTER_SHIFT		3
#define _WRS_FP_REGISTER_SIZE		8
#define _WRS_FP_REGISTER_SHIFT		3

#else	/* CPU */
#error "Invalid CPU value"
#endif	/* CPU */

#define _WRS_DSP_SUPPORT

#define RTP_SPAWN_MEMORY_LEAK_FIX	/* remove when temp code is removed */

#define _RTypeSize			_WRS_INT_REGISTER_SIZE

#define SR_INT_ENABLE			SR_IE	/* interrupt enable bit */

#define POP_SR(p) ((p) & ~SR_EXL)

/*
 * Segment base addresses and sizes
 */

#define	K0BASE		0x80000000
#define	K0SIZE		0x20000000
#define	K1BASE		0xA0000000
#define	K1SIZE		0x20000000

#define KMBASE		"Do not use" /* 0xC0000000 *//* mapped kernel space */
#define	KMSIZE		"Do not use" /* 0x40000000 */

#define	KSBASE		"Do not use" /* 0xC0000000 */
#define	KSSIZE		"Do not use" /* 0x20000000 */
#define	K2BASE		0xC0000000
#define	K2SIZE		0x20000000
#define	K3BASE		0xE0000000
#define	K3SIZE		0x20000000

/*
 * Address conversion macros
 */

/*#define KSEG2_TO_KSEG0_MASK	(~(1<<30)) */ /* Do not use */

#define	K0_TO_K1(x)	((unsigned)(x)|0xA0000000)	/* kseg0 to kseg1 */
#define	K1_TO_K0(x)	((unsigned)(x)&0x9FFFFFFF)	/* kseg1 to kseg0 */

#define	K0_TO_PHYS(x)	((unsigned)(x)&0x1FFFFFFF)	/* kseg0 to physical */
#define	K1_TO_PHYS(x)	((unsigned)(x)&0x1FFFFFFF)	/* kseg1 to physical */

#define	PHYS_TO_K0(x)	((unsigned)(x)|0x80000000)	/* physical to kseg0 */
#define	PHYS_TO_K1(x)	((unsigned)(x)|0xA0000000)	/* physical to kseg1 */

/*
 * The "KM" conversion macros only work for limited sections of the
 * mapped kernel space.
 * 
 */

#define ADDRESS_SPACE_MASK	(~(7<<29))
#define	KM_TO_K0(x)	(((unsigned)(x) & ADDRESS_SPACE_MASK) | K0BASE)
#define	KM_TO_K1(x)	(((unsigned)(x) & ADDRESS_SPACE_MASK) | K1BASE)
#define	KM_TO_PHYS(x)	((unsigned)(x)&0x1FFFFFFF)
#define	PHYS_TO_KM(x)	((unsigned)(x) | K2BASE)

/*
 * Address predicates
 */

#define	IS_KSEG0(x)	((unsigned)(x) >= K0BASE && (unsigned)(x) < K1BASE)
#define	IS_KSEG1(x)	((unsigned)(x) >= K1BASE && (unsigned)(x) < K2BASE)
#define	IS_KUSEG(x)	((unsigned)(x) < K0BASE)

#define IS_UNMAPPED(x)	(((unsigned)(x) & 0xc0000000) == 0x80000000)
#define IS_KSEGM(x)     (!(IS_UNMAPPED(x)))

/* PAL additions */
#define FLOAT_NORM			/* xdr */

#define PORTABLE
#define ffsLib_PORTABLE			/* ffsLib.c */
#define workQLib_PORTABLE		/* workQLib.c */

#define STK_PAGE_GUARD_ATTR		PAGE_MGR_OPTS_SUP_DATA_RO
#define _WRS_ARCH_BLIB
#define	LOGICAL_TO_VIRT(X)		PHYS_TO_KM(K0_TO_PHYS(X))
#define _WRS_DUAL_MAPPED_SYS_PAGE
#define _WRS_VIRTUALLY_INDEXED_CACHES
#define _WRS_CACHE_PAL			/* cache library has been PAL-ified */

/* vxwTaskLib.h */
#define _WRS_NEED_SRSET			/* target/h/vxwTaskLib.h */
#ifndef _WRS_SRSET_ARG_TYPE
#define _WRS_SRSET_ARG_TYPE	UINT32		/* argument to SRSet */
#endif /* _WRS_SRSET_ARG_TYPE */

/* wdbDbgLib.h */
#define WDB_CTX_LOAD(pRegs) _wdbDbgCtxLoad (pRegs)
#define WDB_CTX_SAVE(pRegs) _wdbDbgCtxSave (pRegs)

#ifndef _WRS_SPY_TASK_SIZE
#define _WRS_SPY_TASK_SIZE	10000			/* spyLib.c */
#endif /* _WRS_SPY_TASK_SIZE */

#define _WRS_ARCH_LOADELF	LOADELF_MIPS_MODEL	/* loadElf.c */

#define _WRS_CHECK_REGISTER_WIDTH	/* for register display */

/* Target shell support */
#define _WRS_SHELL_DATA_TYPES		SHELL_ALL_DATA_SUPPORT

/*
 * Exception vectors
 */

#define	T_VEC		K0BASE			/* tlbmiss vector */
#define	X_VEC		(K0BASE+0x80)		/* xtlbmiss vector */
#define	C_VEC		(K1BASE+0x100)		/* cache exception vector */
#define	E_VEC		(K0BASE+0x180)		/* exception vector */

#define	R_VEC		(K1BASE+0x1fc00000)	/* reset vector */
#define BEV_VEC		(K1BASE+0x1fc00380)	/* boot exception vector */
/*
 * Cache alignment macros
 *
 * NOTE: These definitions may migrate to vxWorks.h in a future release.
 */
#define	CACHE_ROUND_UP(x)	ROUND_UP(x, _CACHE_ALIGN_SIZE)
#define	CACHE_ROUND_DOWN(x)	ROUND_DOWN(x, _CACHE_ALIGN_SIZE)

/*
* Cache size constants
*/

#define	R4KMINCACHE	+(1*1024)	/* leading plus for mas's benefit */
#define	R4KMAXCACHE	+(256*1024)	/* leading plus for mas's benefit */

/*
 * Cause bit definitions
 */
#define	CAUSE_BD	0x80000000	/* Branch delay slot */
#define	CAUSE_CEMASK	0x30000000	/* coprocessor error */
#define	CAUSE_CESHIFT	28

#define	CAUSE_IP8	0x00008000	/* External level 8 pending */
#define	CAUSE_IP7	0x00004000	/* External level 7 pending */
#define	CAUSE_IP6	0x00002000	/* External level 6 pending */
#define	CAUSE_IP5	0x00001000	/* External level 5 pending */
#define	CAUSE_IP4	0x00000800	/* External level 4 pending */
#define	CAUSE_IP3	0x00000400	/* External level 3 pending */
#define	CAUSE_SW2	0x00000200	/* Software level 2 pending */
#define	CAUSE_SW1	0x00000100	/* Software level 1 pending */

/* extended interrupt cause bits (e.g., RM7000) */
#define CAUSE_IP16	0x00800000	/* RESERVED */
#define CAUSE_IP15	0x00400000	/* RESERVED */
#define CAUSE_IP14	0x00200000	/* Perf counter */
#define CAUSE_IP13	0x00100000	/* Alt Timer */
#define CAUSE_IP12	0x00080000	/* External level 12 pending */
#define CAUSE_IP11	0x00040000	/* External level 11 pending */
#define CAUSE_IP10	0x00020000	/* External level 10 pending */
#define CAUSE_IP9	0x00010000	/* External level 9 pending */

/* Note: mask includes extended interrupt bit positions */
#define	CAUSE_IPMASK	0x00FFFF00	/* Pending interrupt mask */
#define	CAUSE_IPSHIFT	8

#define	CAUSE_EXCMASK	0x0000007C	/* Cause code bits */
#define	CAUSE_EXCSHIFT	2

/*
 * Status definition bits
 */
#define	SR_CUMASK	0xf0000000	/* coproc usable bits */
#define	SR_CU3		0x80000000	/* Coprocessor 3 usable */
#define	SR_CU2		0x40000000	/* Coprocessor 2 usable */
#define	SR_CU1		0x20000000	/* Coprocessor 1 usable */
#define	SR_CU0		0x10000000	/* Coprocessor 0 usable */
#define SR_RP		0x08000000      /* Use reduced power mode */
#define SR_FR		0x04000000      /* Enable extra floating point regs */
#define SR_RE		0x02000000      /* Reverse endian in user mode */
#define SR_MX		0x01000000      /* Enable DSP/MX extensions */
#define	SR_BEV		0x00400000	/* use boot exception vectors */
#define	SR_TS		0x00200000	/* TLB shutdown */
#define SR_SR		0x00100000	/* soft reset occurred */
#define	SR_CH		0x00040000	/* cache hit */
#define	SR_CE		0x00020000	/* use ECC reg */
#define	SR_DE		0x00010000	/* disable cache errors */
#define	SR_IMASK	0x0000ff00	/* Interrupt mask */
#define	SR_IMASK8	0x00000000	/* mask level 8 */
#define	SR_IMASK7	0x00008000	/* mask level 7 */
#define	SR_IMASK6	0x0000c000	/* mask level 6 */
#define	SR_IMASK5	0x0000e000	/* mask level 5 */
#define	SR_IMASK4	0x0000f000	/* mask level 4 */
#define	SR_IMASK3	0x0000f800	/* mask level 3 */
#define	SR_IMASK2	0x0000fc00	/* mask level 2 */
#define	SR_IMASK1	0x0000fe00	/* mask level 1 */
#define	SR_IMASK0	0x0000ff00	/* mask level 0 */
#define	SR_IBIT8	0x00008000	/* bit level 8 */
#define	SR_IBIT7	0x00004000	/* bit level 7 */
#define	SR_IBIT6	0x00002000	/* bit level 6 */
#define	SR_IBIT5	0x00001000	/* bit level 5 */
#define	SR_IBIT4	0x00000800	/* bit level 4 */
#define	SR_IBIT3	0x00000400	/* bit level 3 */
#define	SR_IBIT2	0x00000200	/* bit level 2 */
#define	SR_IBIT1	0x00000100	/* bit level 1 */

#define	SR_IMASKSHIFT	8

/* R4K specific */
#define	SR_KX		0x00000080	/* enable kernel 64bit addresses */
#define	SR_SX		0x00000040	/* enable supervisor 64bit addresses */
#define	SR_UX		0x00000020	/* enable user 64bit addresses */
#define	SR_KSUMASK	0x00000018	/* mode mask */
#define SR_KSU_K	0x00000000	/* kernel mode */
#define SR_KSU_S	0x00000008	/* supervisor mode */
#define SR_KSU_U	0x00000010	/* user mode */
#define	SR_ERL		0x00000004	/* Error Level */
#define	SR_EXL		0x00000002	/* Exception Level */
#define	SR_IE		0x00000001	/* interrupt enable, 1 => enable */

/*
 * fpa definitions
 */

#define FP_ROUND	0x3		/* r4010 round mode mask */
#define FP_STICKY 	0x7c		/* r4010 sticky bits mask */
#define FP_ENABLE 	0xf80		/* r4010 enable mode mask */
#define FP_EXC		0x3f000		/* r4010 exception mask */

#define FP_ROUND_N	0x0		/* round to nearest */
#define FP_ROUND_Z 	0x1		/* round to zero */
#define FP_ROUND_P 	0x2		/* round to + infinity */
#define FP_ROUND_M 	0x3		/* round to - infinity */

#define FP_STICKY_I	0x4		/* sticky inexact operation */
#define FP_STICKY_U	0x8		/* sticky underflow */
#define FP_STICKY_O	0x10		/* sticky overflow */
#define FP_STICKY_Z	0x20		/* sticky divide by zero */
#define FP_STICKY_V	0x40		/* sticky invalid operation */

#define FP_ENABLE_I	0x80		/* enable inexact operation */
#define FP_ENABLE_U	0x100		/* enable underflow exc  */
#define FP_ENABLE_O	0x200		/* enable overflow exc  */
#define FP_ENABLE_Z	0x400		/* enable divide by zero exc  */
#define FP_ENABLE_V	0x800		/* enable invalid operation exc  */

#define FP_EXC_I	0x1000		/* inexact operation */
#define FP_EXC_U	0x2000		/* underflow */
#define FP_EXC_O	0x4000		/* overflow */
#define FP_EXC_Z	0x8000		/* divide by zero */
#define FP_EXC_V	0x10000		/* invalid operation */
#define FP_EXC_E	0x20000		/* unimplemented operation */

#define FP_COND		0x800000	/* condition bit */
#define FP_FS		0x1000000	/* flush denormalised results to zero */

#define FP_EXC_SHIFT	12
#define FP_ENABLE_SHIFT	7
#define FP_EXC_MASK	(FP_EXC_I|FP_EXC_U|FP_EXC_O|FP_EXC_Z|FP_EXC_V|FP_EXC_E)
#define FP_ENABLE_MASK	(FP_ENABLE_I|FP_ENABLE_U|FP_ENABLE_O|FP_ENABLE_Z| \
			 FP_ENABLE_V)
#define FP_TASK_STATUS	(FP_FS) 	/* all FP exceptions are disabled
					   we only attempt to hide denormalised
					   results for signals (see fppALib.s
					   and spr #7665) */

/*
 * R4000 Config Register
 */
#define CFG_CM		0x80000000	/* Master-Checker mode */
#define CFG_ECMASK	0x70000000	/* System Clock Ratio */
#define CFG_ECBY2	0x00000000 	/* divide by 2 */
#define CFG_ECBY3	0x00000000 	/* divide by 3 */
#define CFG_ECBY4	0x00000000 	/* divide by 4 */
#define CFG_EPMASK	0x0f000000	/* Transmit data pattern */
#define CFG_EPD		0x00000000	/* D */
#define CFG_EPDDX	0x01000000	/* DDX */
#define CFG_EPDDXX	0x02000000	/* DDXX */
#define CFG_EPDXDX	0x03000000	/* DXDX */
#define CFG_EPDDXXX	0x04000000	/* DDXXX */
#define CFG_EPDDXXXX	0x05000000	/* DDXXXX */
#define CFG_EPDXXDXX	0x06000000	/* DXXDXX */
#define CFG_EPDDXXXXX	0x07000000	/* DDXXXXX */
#define CFG_EPDXXXDXXX	0x08000000	/* DXXXDXXX */
#define CFG_SBMASK	0x00c00000	/* Secondary cache block size */
#define CFG_SBSHIFT	22
#define CFG_SB4		0x00000000	/* 4 words */
#define CFG_SB8		0x00400000	/* 8 words */
#define CFG_SB16	0x00800000	/* 16 words */
#define CFG_SB32	0x00c00000	/* 32 words */
#define CFG_SS		0x00200000	/* Split secondary cache */
#define CFG_SW		0x00100000	/* Secondary cache port width */
#define CFG_EWMASK	0x000c0000	/* System port width */
#define CFG_EWSHIFT	18
#define CFG_EW64	0x00000000	/* 64 bit */
#define CFG_EW32	0x00010000	/* 32 bit */
#define CFG_SC		0x00020000	/* Secondary cache absent */
#define CFG_SM		0x00010000	/* Dirty Shared mode disabled */
#define CFG_BE		0x00008000	/* Big Endian */
#define CFG_EM		0x00004000	/* ECC mode enable */
#define CFG_EB		0x00002000	/* Block ordering */
#define CFG_ICMASK	0x00000e00	/* Instruction cache size */
#define CFG_ICSHIFT	9
#define CFG_DCMASK	0x000001c0	/* Data cache size */
#define CFG_DCSHIFT	6
#define CFG_IB		0x00000020	/* Instruction cache block size */
#define CFG_DB		0x00000010	/* Data cache block size */
#define CFG_CU		0x00000008	/* Update on Store Conditional */
#define CFG_K0MASK	0x00000007	/* KSEG0 coherency algorithm */

/*
 * Primary cache mode
 */
#define CFG_C_UNCACHED		2
#define CFG_C_NONCOHERENT	3
#define CFG_C_COHERENTXCL	4
#define CFG_C_COHERENTXCLW	5
#define CFG_C_COHERENTUPD	6

/* 
 * Primary Cache TagLo 
 */
#define TAG_PTAG_MASK           0xffffff00      /* Primary Tag */
#define TAG_PTAG_SHIFT          0x00000008
#define TAG_PSTATE_MASK         0x000000c0      /* Primary Cache State */
#define TAG_PSTATE_SHIFT        0x00000006
#define TAG_PARITY_MASK         0x0000000f      /* Primary Tag Parity */
#define TAG_PARITY_SHIFT        0x00000000


/* 
 * Secondary Cache TagLo 
 */
#define TAG_STAG_MASK           0xffffe000      /* Secondary Tag */
#define TAG_STAG_SHIFT          0x0000000d
#define TAG_SSTATE_MASK         0x00001c00      /* Secondary Cache State */
#define TAG_SSTATE_SHIFT        0x0000000a
#define TAG_VINDEX_MASK         0x00000380      /* Secondary Virtual Index */
#define TAG_VINDEX_SHIFT        0x00000007
#define TAG_ECC_MASK            0x0000007f      /* Secondary Tag ECC */
#define TAG_ECC_SHIFT           0x00000000
#define TAG_STAG_SIZE			19	/* Secondary Tag Width */

/* MIPS32/MIPS64 CP0 Register 16 Select 1 defines */

#define CFG1_MMUMASK        0x7e000000   /* MMU size field */
#define CFG1_MMUSHIFT       25
#define CFG1_ISMASK         0x01c00000   /* icache sets per way */
#define CFG1_ISSHIFT        22
#define CFG1_ILMASK         0x00380000   /* icache line size */
#define CFG1_ILSHIFT        19
#define CFG1_IAMASK         0x00070000   /* icache associativity */
#define CFG1_IASHIFT        16
#define CFG1_DSMASK         0x0000e000   /* dcache sets per way */
#define CFG1_DSSHIFT        13
#define CFG1_DLMASK         0x00001c00   /* dcache line size */
#define CFG1_DLSHIFT        10
#define CFG1_DAMASK         0x00000380   /* dcache associativity */
#define CFG1_DASHIFT        7
#define CFG1_PC             0x00000010   /* perf counter implemented */
#define CFG1_WR             0x00000008   /* watch reg implemented */
#define CFG1_CA             0x00000004   /* code compression implemented */
#define CFG1_EP             0x00000002   /* EJTAG implemented */
#define CFG1_FP             0x00000001   /* FPU implemented */
#define CFG1_CACHE_SETS_PER_WAY     64   /* base size for sets per way shifted
                                          * left by the value in the CFG1
                                          * sets per way field
                                          */

#define CFG1_CACHE_LINE_SIZE         2   /* base cache size shifted left by
                                          * the value in the CFG1 cache line
                                          * size field
                                          */


/*
 * CacheErr register
 */
#define CACHEERR_TYPE		0x80000000	/* reference type: 
						   0=Instr, 1=Data */
#define CACHEERR_LEVEL		0x40000000	/* cache level:
						   0=Primary, 1=Secondary */
#define CACHEERR_DATA		0x20000000	/* data field:
						   0=No error, 1=Error */
#define CACHEERR_TAG		0x10000000	/* tag field:
						   0=No error, 1=Error */
#define CACHEERR_REQ		0x08000000	/* request type:
						   0=Internal, 1=External */
#define CACHEERR_BUS		0x04000000	/* error on bus:
						   0=No, 1=Yes */
#define CACHEERR_BOTH		0x02000000	/* Data & Instruction error:
						   0=No, 1=Yes */
#define CACHEERR_ECC		0x01000000	/* ECC error :
						   0=No, 1=Yes */
#define CACHEERR_SIDX_MASK	0x003ffff8	/* PADDR(21..3) */
#define CACHEERR_SIDX_SHIFT		 3
#define CACHEERR_PIDX_MASK	0x00000007	/* VADDR(14..12) */
#define CACHEERR_PIDX_SHIFT		12


/*
 * Cache operations
 */
#define Index_Invalidate_I               0x0         /* 0       0 */
#define Index_Writeback_Inv_D            0x1         /* 0       1 */
#define Index_Invalidate_SI              0x2         /* 0       2 */
#define Index_Writeback_Inv_SD           0x3         /* 0       3 */
#define Index_Load_Tag_I                 0x4         /* 1       0 */
#define Index_Load_Tag_D                 0x5         /* 1       1 */
#define Index_Load_Tag_SI                0x6         /* 1       2 */
#define Index_Load_Tag_SD                0x7         /* 1       3 */
#define Index_Store_Tag_I                0x8         /* 2       0 */
#define Index_Store_Tag_D                0x9         /* 2       1 */
#define Index_Store_Tag_SI               0xA         /* 2       2 */
#define Index_Store_Tag_SD               0xB         /* 2       3 */
#define Create_Dirty_Exc_D               0xD         /* 3       1 */
#define Create_Dirty_Exc_SD              0xF         /* 3       3 */
#define Hit_Invalidate_I                 0x10        /* 4       0 */
#define Hit_Invalidate_D                 0x11        /* 4       1 */
#define Hit_Invalidate_SI                0x12        /* 4       2 */
#define Hit_Invalidate_SD                0x13        /* 4       3 */
#define Hit_Writeback_Inv_D              0x15        /* 5       1 */
#define Hit_Writeback_Inv_SD             0x17        /* 5       3 */
#define Fill_I                           0x14        /* 5       0 */
#define Hit_Writeback_D                  0x19        /* 6       1 */
#define Hit_Writeback_SD                 0x1B        /* 6       3 */
#define Hit_Writeback_I                  0x18        /* 6       0 */
#define Lock_I				 0x1C	     /* 7	0 */
#define Lock_D				 0x1D	     /* 7	1 */
#define Hit_Set_Virtual_SI               0x1E        /* 7       2 */
#define Hit_Set_Virtual_SD               0x1F        /* 7       3 */

/*
 * Coprocessor 0 operations
 */
#define	C0_READI  0x1		/* read ITLB entry addressed by C0_INDEX */
#define	C0_WRITEI 0x2		/* write ITLB entry addressed by C0_INDEX */
#define	C0_WRITER 0x6		/* write ITLB entry addressed by C0_RAND */
#define	C0_PROBE  0x8		/* probe for ITLB entry addressed by TLBHI */
#define	C0_ERET	  0x18		/* restore for exception */

/*
 * TLB definitions
 */
#define TLB_ENTRIES   64        /* set to size of largest supported core */

/*
 * macros used in fppShow.c to word swap the 32 bit FP reg pair before printing
 * double value if _WRS_FP_REGISTER_SIZE == 4 (e.g., MIPS32 hardware float).
 */
#if (_WRS_FP_REGISTER_SIZE == 4)
#define _WRS_NEED_DOUBLE_TMP

/* word swap the 32 bits FP Reg. pair before printing double value. */
#define _WRS_FP_REG_TEMP(ix) { \
	*(UINT32 *) fpDoubleTmp = \
	  *(UINT32 *) (((UINT32)&fpRegSet + fpRegName[ix].regOff + sizeof(UINT32))); \
	*(UINT32 *) ((UINT32) fpDoubleTmp + sizeof(UINT32)) = \
	  *(UINT32 *) (((UINT32)&fpRegSet + fpRegName[ix].regOff)); \
	fpTmp = fpDoubleTmp; \
	}
#endif	/* _WRS_FP_REGISTER_SIZE == 4 */

/* moved here from h/private/javaLibP.h */

#define JAVA_C_SYM_PREFIX	""

/* moved here from h/private/loadElfLibP.h */
   
/* Check whether the value fits in n bits */

#define CHECK_FITS(val,nBits) (((val) & ~(MASK(nBits))) == 0)


/* moved here from h/types/vxtypesOld.h */

#ifndef _ASMLANGUAGE
/* physical addresses are represented on 64-bit for all MIPS CPUs */

#   define _TYPE_PHYS_ADDR	typedef UINT64 PHYS_ADDR
#endif

#define _WRS_PHYS_ADDR_IS_64_BITS

#define CHAR_FROM_CONST(x)	(char *)(x)
#define VOID_FROM_CONST(x)	(void *)(x)

/* trigger generation of regWidth field of the REG_INDEX struct in h/regs.h. */

#define _WRS_REG_INDEX_REGWIDTH

/* from vmBaseLib.c */

/* #define _WRS_DISABLE_BASEVM */

/* from dbgTaskLib.c and wdbBpLib.c */

    /*
     * On MIPS CPUs, when a breakpoint exception occurs in a branch delay slot,
     * the PC has been changed in the breakpoint handler to match with the
     * breakpoint address.
     * Once the matching has been made, the PC is modified to have its normal
     * value (the preceding jump instruction).
     */

#define _WRS_ADJUST_PC_FOR_BRANCH_DELAY(p) \
    {if ((p)->cause & CAUSE_BD) \
        (p)->reg_pc--;}

/* from loadElfLib.c */

#define _WRS_USE_ELF_LOAD_FORMAT

/* from spyLib.c */

#define _WRS_SPY_TASK_SIZE	10000	/* MIPS specific spy task size */

/* from usrLib.c */

#define _WRS_TASK_REG_WIDTH taskRegName[ix].regWidth
#define _WRS_FPCTL_REG_WIDTH fpCtlRegName[ix].regWidth

/* from xdr_float.c */

#define FLOAT_NORM

/* from vmShow.c */

/* #define _WRS_DISABLE_VM */

/* from taskShow.c */

#define _WRS_CUSTOM_TASK_ARCH_REGS_SHOW

/* from bLib.c */

/*#define _WRS_BLIB_OPTIMIZED*/

/* from schedLib.c */

/*#define _WRS_SCHEDLIB_OPTIMIZED*/
#define	_WRS_BASE6_SCHEDULER

/*
 * Let the idle loop look like a real task by turning all ints on.
 * Without this if a task locks interrupts and suspends itself, and
 * there are no ready tasks, we will lockup.
 */

#define _WRS_ENABLE_IDLE_INTS	{intSRSet (taskSrDefault);}

/* from sem*Lib.c, semA*Lib.s */

#define _WRS_HAS_OPTIMIZED_SEM
/*#define _WRS_USE_OPTIMIZED_SEM */	/* XXX use optimized sem, for now */

/* macros for getting frame and return PC from a jmp_buf */

#define _WRS_FRAMEP_FROM_JMP_BUF(env)	((char *) (env)[0].reg.spReg)
#define _WRS_RET_PC_FROM_JMP_BUF(env)	((INSTR *) (env)[0].reg.raReg)

/*
 * specify that MIPS architecture uses the linker to achieve data
 * segment alignment.
 */
#define _WRS_LINKER_DATA_SEG_ALIGN

/* Enable possibility of including OSM detection if mapped kernel */

#if	defined(INCLUDE_MAPPED_KERNEL)
#define _WRS_OSM_INIT
#endif /* defined(INCLUDE_MAPPED_KERNEL) */

/* Trigger use of non-global null page. For an explanation, see the 
 * description in aimMmuLib.c in the function header for aimMmuLibInit.
 */

#define _WRS_NONGLOBAL_NULL_PAGE

/* Add define for PM_MMULESS_PAGE_SIZE.  needed by pmLib.c so
 * the ED&R area is created on a 8K boundary in all cases.  the 8k
 * boundary size specifies for MIPS the minimum page size.
 */

#define PM_MMULESS_PAGE_SIZE 8192

/* define _ARCH_SUPPORTS_PROTECT_INTERRUPT_STACK indicating support
 * for guard pages on the interrupt stack
 */

#define _ARCH_SUPPORTS_PROTECT_INTERRUPT_STACK

/* define _WRS_STACK_PROTECT_REQUIRES_MMU to indicate all stack
 * protection support features require MMU support on MIPS
 */

#define _WRS_STACK_PROTECT_REQUIRES_MMU

#include "arch/mips/archMipsCommon.h"

/* END PAL */

#ifdef __cplusplus
}
#endif

#endif /* __INCarchMipsh */
