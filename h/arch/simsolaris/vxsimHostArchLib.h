/* vxsimHostArchLib.h - simulator UNIX/VxSim public interface header */

/* Copyright 2003-2005 Wind River Systems, Inc. */

/*
modification history
--------------------
01n,11jan05,jmp  moved vxsimHostWdbPipeInit() to common vxsimHostLib.h.
01m,09mar04,jmp  renamed vxsimHostFdInit() to vxsimHostTtyInit().
01l,13oct03,jeg  moved vxsimHostOpendir () prototype to vxsimHostLib.h.
01k,25aug03,jeg  added vxsimHostGetgid () prototype
01j,05aug03,jeg  moved vxsimHostShMemInit () prototype to vxsimHostLib.h.
01i,16jun03,dbt  Removed vxsimHostStat() & vxsimHostFstatSize().
01h,20may03,jmp  moved vxsimHostModel() to vxsimHostLib.h.
01g,04apr03,dbt  Added vxsimHostContextLoad() prototype.
		 Removed vxsimHostMakecontext() prototype.
01f,03apr03,jeg  moved common routine definitions to target/h/vxsimHostLib.h
01e,01apr03,jeg  Updated routines declaration
01d,20mar03,jeg  added vxsimHostPrintf and vxsimHostProcAddrGet prototype.
01c,19mar03,dbt  Removed some useless routines. 
		 Modified vxsimHostIntLock()/vxsimHostIntUnlock() interfaces
01b,05mar03,jeg  cleanup code. Removed unused routine declarations and Unix
		 type definitions.
01a,27jan03,jeg  written, based on u_Lib.h 
*/

/*
This header contains the prototype declarations for all UNIX and host routines 
call made by VxSim.
*/

#ifndef	__INCvxsimHostArchLibh
#define	__INCvxsimHostArchLibh

#ifdef __cplusplus
extern "C" {
#endif

#include "types/vxCpu.h"	/* Must come before regs.h */
#include "passFsLib.h"		/* Must come before regs.h */
#include "regs.h"		/* for REG_SET */
#include "stat.h"		/* for stat and statfs */

/* host side defined routines */

extern void	vxsimHostTtyInit	(int fd, int raw);
extern STATUS	vxsimHostKill		(int pid, int sig);
extern void 	vxsimHostContextLoad	(REG_SET * pRegSet);
extern void *	vxsimHostMalloc		(int size);
extern STATUS	vxsimHostSigdelset	(int *set, int signo);
extern void *	vxsimHostSignal		(int signo, void * sigwhat);
extern STATUS	vxsimHostSigprocmask	(int how, int *set, int *oldset);
extern void	vxsimHostTimerEnable	(int clk, int ticksPerSecond);
extern void 	vxsimHostUserGet	(char * hostname, char * hostip,
					 char * username, char * cwd);
extern void 	vxsimHostWdbDbgArchInit	(void);
extern int	vxsimHostGetgid		(void);

#ifdef __cplusplus
}
#endif

#endif	/* __INCvxsimHostArchLibh */
