/* coprocSimsolaris.h - coprocessor management library header */

/* Copyright 1984-2003 Wind River Systems, Inc. */

#include "copyright_wrs.h"

/*
modification history
--------------------
01a,19aug04,dbt	 Written
*/

#ifndef __INCcoprocSimsolarish
#define __INCcoprocSimsolarish

#ifdef __cplusplus
extern "C" {
#endif

/* includes */

#include "taskLib.h"

/* defines */

#define VX_FP_TASK         VX_COPROC1_TASK

#ifdef __cplusplus
}
#endif

#endif /* __INCcoprocSimsolarish */
