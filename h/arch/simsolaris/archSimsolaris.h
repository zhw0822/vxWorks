/* archSimsolaris.h - simsparc specific header */

/*
 * Copyright 1993-2005 Wind River Systems, Inc.
 *
 * The right to copy, distribute or otherwise make use of this software
 * may be licensed only pursuant to the terms of an applicable Wind River
 * license agreement. No license to Wind River intellectual property rights
 * is granted herein. All rights not licensed by Wind River are reserved
 * by Wind River.
 */

/*
modification history
--------------------
01y,12apr05,kk   remove _WRS_OBJLIB_SUPPORT_VERIFY_TRAP macro (SPR# 106486)
01x,31mar05,elp  added _ARCH_SUPPORTS_PROTECT_INTERRUPT_STACK macro
01w,27aug04,dbt  Added _WRS_OBJLIB_SUPPORT_VERIFY_TRAP.
01v,17may04,tam  removed _WRS_KERNEL_TEXT_START_ADRS definition
01u,14may04,jmp  removed upsolete stuff.
01t,22apr04,jmp  added macros for getting frame and return PC from a jmp_buf.
01s,25feb04,aeg  moved _WRS_PAL_COPROC_LIB to archSimsolarisCommon.h
01r,22jan04,dbt  Added support for GNU compiler.
01q,02dec03,dbt  Updated power management support.
01p,14nov03,job  Moved blib stuff to archSimsolarisCommon.h
01o,13nov03,pes  Add include of common header file.
01n,23sep03,dbt  Use new B6 scheduler.
01m,18aug03,dbt  Added PAL spuport for VxSim.
01l,22jul03,kam  removed define of WV_INSTRUMENTATION
01k,19jun03,dbt  Added support for DCC.
01j,31may01,jmp  removed _WRS_NO_EXC_STACK_USED definition.
		 Added _WRS_ARCH_IS_SIMULATOR.
		 Added define for PAL-ified cache library.
01i,12feb01,hbh  Removed _WRS_NO_OVERLAPPING_MODEL and NO_SL_DATA_SUPPORT
                 flags definition (SPR 64073)
01h,26may00,rlp  added _WRS_NO_EXC_STACK_USED macro.
01g,19apr00,hbh  Added _WRS_NO_OVERLAPPING_MODEL macro.
01f,17apr00,cjj  defined NO_SL_DATA_SUPPORT
01e,13apr00,hbh  added WV_INSTRUMENTATION
01d,23dec99,cjj  removed WV_INSTRUMENTATION for T3 development.
01c,29jul98,ms   added WV_INSTRUMENTATION
01b,16dec96,yp   va_start now passes LASTARG to builtin_next_arg
01a,07jun95,ism  derived from simsparc
*/

#ifndef __INCarchSimsolarish
#define __INCarchSimsolarish

#ifdef __cplusplus
extern "C" {
#endif

#define	_ALLOC_ALIGN_SIZE	8		/* 8-byte alignment */
#define _STACK_ALIGN_SIZE	8		/* 8-byte alignment */

#define _DYNAMIC_BUS_SIZING	FALSE		/* no dynamic bus sizing */

#define _WRS_ARCH_IS_SIMULATOR	/* This is a simulator */

#define _ARCH_SUPPORTS_DCC	/* DCC compiler is supported on this arch. */
#define _ARCH_SUPPORTS_GCC	/* GCC compiler is supported on this arch. */

/* Used in usrDepend.c and usrKernelStack.c */

#define _ARCH_SUPPORTS_PROTECT_INTERRUPT_STACK

/* moved here from h/private/javaLibP.h */

#define JAVA_C_SYM_PREFIX	""

#ifndef _ASMLANGUAGE
/* macros for getting frame and return PC from a jmp_buf */

#define _WRS_FRAMEP_FROM_JMP_BUF(env)	((char *)  (env)[0].reg.reg_sp)
#define _WRS_RET_PC_FROM_JMP_BUF(env)	((INSTR *) (env)[0].reg.reg_pc)

#endif 	/* _ASMLANGUAGE */

/* from loadElfLib.c */

#define _WRS_USE_ELF_LOAD_FORMAT

/* from xdr_float.c */

#define FLOAT_NORM

/* from qPriBMapLib.c */

#define qPriBMapLib_PORTABLE

/* from ffsLib.c */

#define ffsLib_PORTABLE

/* from schedLib.c */

#define _WRS_VXLIB_SUPPORT

#define _WRS_BASE6_SCHEDULER
#define _WRS_ENABLE_IDLE_INTS

/* from spyLib.c */

#define _WRS_SPY_TASK_SIZE      10000

/* End PAL */

#include "arch/simsolaris/archSimsolarisCommon.h"

#ifdef __cplusplus
}
#endif

#endif /* __INCarchSimsolarish */
