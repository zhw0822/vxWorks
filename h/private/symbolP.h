/* symbolP.h - private symbol structure header */

/*
 * Copyright (c) 1984-2005 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use of this software
 * may be licensed only pursuant to the terms of an applicable Wind River
 * license agreement.
 */

/*
modification history
--------------------
01d,07mar05,v_r  Cleanups + code conventions compliancy work.
		 Moved SYMBOL structure back into public header as needed by
		 published APIs.
01c,18sep04,jn   Move toward shared code on host and target
01b,01apr04,jn   Fix display of symbol types
01b,17feb04,pad  Extended symbol's group field from 16 bits to 32 bits (to
		 support shared libraries).
01a,29oct01,jn   created from HIDDEN part of symbol.h
*/

#ifndef __INCsymbolPh
#define __INCsymbolPh

#ifdef __cplusplus
extern "C" {
#endif

#include "symbol.h"

#ifdef HOST
#include "dllLib.h"
#endif /* HOST */

typedef UINT32 SYM_EXT_TYPE;	    /* internal symbol type (SYM_TYPE is still
				     * an unsigned char) */

#define SYM_EXT_NO_TYPE        0x0  /* no internal type information */
#define SYM_EXT_WEAK       0x10000  /* weak symbol */            
#define SYM_EXT_SEC        0x20000  /* section descriptor */            
#define SYM_EXT_UPDATE    0x100000  /* symbol to update (symtbl sync.) */

/*
 * pre-defined symbol references:
 * The symRef field will usually hold a MODULE_ID, to identify the module that
 * the symbol belongs to. Therefore we can use any addresses between (sizeof
 * (MODULE) - 1) and zero to indicate the source of symbols that are not
 * associated with any module.
 */

#define SYMREF_NONE     NULL                /* no reference */
#define SYMREF_SHELL    (void *) 0xffffffff /* symbols created by the shell */
#define SYMREF_KERNEL   (void *) 0xfffffffe /* kernel symbols */
#define SYMREF_WTX      (void *) 0xfffffffd /* symbols created by WTX symAdd */

#ifdef HOST

/* Undefined symbols structure */

typedef struct
    {
    DL_NODE     undefSymNode;   /* double-linked list node information */
    char *      name;           /* name of the undefined symbol */
    } UNDEF_SYM;

typedef UNDEF_SYM * UNDEF_SYM_ID;

#endif	/* HOST */

#ifdef __cplusplus
}
#endif

#endif /* __INCsymbolPh */

