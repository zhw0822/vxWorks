/* mmanP.h - memory management library private header file */

/* Copyright 2004 Wind River Systems, Inc. */

/*
modification history
--------------------
01a,09apr04,tam  written
*/

#ifndef __INCmmanPh
#define __INCmmanPh

#ifdef __cplusplus
extern "C" {
#endif

/* includes */

#include "vxWorks.h"
#include "dllLib.h"

/* typedefs */

#ifndef _ASMLANGUAGE
typedef struct mm_node
    {
    DL_NODE node;
    void *  addr;		/* start address of the mmap'ed memory area */
    size_t  len;		/* size in bytes of the mmap'ed memory area */
    int     access;		/* access attributes of the whole memory area */
    int     minAccess;		/* minimal access to any page within the  */
    				/* mmap'ed memory area */
    } MM_NODE;

#endif /* _ASMLANGUAGE */

#ifdef __cplusplus
}
#endif

#endif /* __INCmmanPh */
