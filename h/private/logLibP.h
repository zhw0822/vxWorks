/* logLibP.h - private header for logLib logging facility */

/* Copyright 2004 Wind River Systems, Inc. */
 
/* modification history
--------------------
01a,17sep04,dlk	 written,
*/

#ifndef __INClogLibPh
#define __INClogLibPh

#include "vxWorks.h"
#include "semLibP.h"

/*
 * These two should arguably be in a public header, as
 * they refer to the public API.
 */

#define MAX_LOGARGS	6	/* max args to log message */
#define MAX_LOGFDS	5	/* max log fds */


#ifdef __cplusplus
extern "C" {
#endif

/*
 * The FD array and the semaphore which guards it are made
 * external since they are shared by nbioLogLib.
 */

extern SEMAPHORE logFdSem;

extern int logFd [MAX_LOGFDS];
extern int numLogFds;

#ifdef __cplusplus
    }
#endif

#endif /* __INClogLibPh */

