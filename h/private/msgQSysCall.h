/* msgQSysCall.h - Wind message queue system call definitions */

/* Copyright 2003-2004 Wind River Systems, Inc. */

/*
modification history
--------------------
01b,22apr04,dcc  added 'context' parameter to _msgQOpen().
01a,27nov03,aeg  written
*/

#ifndef __INCmsgQSysCallh
#define __INCmsgQSysCallh

#ifdef __cplusplus
extern "C" {
#endif

#include "vxWorks.h"

/* system call function prototypes */

extern MSG_Q_ID	_msgQOpen (const char * name, UINT maxMsgs, UINT maxMsgLength,
			   int options, int mode, void * context);

#ifdef __cplusplus
}
#endif

#endif /* __INCmsgQSysCallh */
