/* eventP.h - event log header */

/*
 * Copyright (c) 1994-2005 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */
 
/*
modification history
--------------------
03p,14jun05,tcr  add events for ioLib
03o,23feb05,tcr  update comments for apigen
03n,17sep04,tcr  add sdLib events
03m,03aug04,pch  Add PPC_WD_INT_ID for ppc4xx/ppc85xx hardware watchdog timer
03w,06oct04,pch  renumbered history
03v,17sep04,tcr  add sdLib events
03u,03aug04,pch  Add PPC_WD_INT_ID for ppc4xx/ppc85xx hardware watchdog timer
03t,04may04,ymz  modified SAL event ID to resolve clashes.
03r,28apr04,ymz  added SAL event ID and macro.
03r,26apr04,tcr  include code review comments for rtpSigLib.c
03q,15apr04,tcr  add macros and events for rtpLib
03p,18mar04,tcr  add RTP signal events
03o,25mar04,cpd  Re-add a State Change event
03n,04mar04,aim  renumbered EVENT_EDR_ERROR_INJECT to CLASS3(80)
03m,18feb04,tcr  add events for rtpLib
03l,24nov03,tcr  update EVENT_MEMREALLOC
03k,17nov03,tcr  add OBJ_VERIFY fail handling
03j,03nov03,tcr  add syscall events; add rtp to EVENT_TASKNAME;
                 validate objects in macros
03i,25sep03,tcr  always preserve tasknames
03h,16sep03,tcr  add events for isrLib
03g,09sep03,tcr  changes to event formats, change class instrumentation
03f,08sep03,tcr  change event formats
03e,05sep03,tcr  fix merge error
03d,05sep03,tcr  fix merge error
03c,02sep03,tcr  Base6 changes for obj instrumentation
03b,04dec03,aim  updated EVT_EDR
03a,04aug03,elg  Add support for new task status: define events related to new
                 status for instrumentation.
02z,19sep03,aim  added ED&R events
02y,13may03,pes  PAL conditional compilation cleanup. Phase 2.
02x,04jun03,bwa  Added macros for message channels and services.
02w,09may03,to   changed OBJ_ID to OBJ_CORE * in OBJ_EVT_RTN and TASK_EVT_RTN,
		 deleted condition code for OBJ_INST_RTN.
02v,09may02,tcr  Fix for SPR 74987 (add #ifdef __cplusplus)
02u,31oct01,tcr  add event range for RTI
02t,18oct01,tcr  add support for VxWorks Events, move to VxWorks 5.5
02s,27sep01,tcr  Fix SPR 24971 - instrument PPC interrupts
02r,13sep01,tcr  Fix SPR 29673 - macro for msgQSmLib events
02q,09sep98,cjtc reverting to byte ordering based on _BYTE_ORDER macro
02p,04sep98,cjtc porting for ARM architechture
02o,02sep98,nps  corrected WV2.0 -> T2 merge.
02n,24aug98,cjtc intEnt logging for PPC is now performed in a single step
		 instead of the two-stage approach which gave problems with
		 out-of-order timestamps in the event log (SPR 21868)
02m,17aug98,pr   replaced OBJ_VERIFY check with check on NULL pointer
02l,08may98,pr   added number of params for EVT_OBJ_SIG macro
02k,08may98,pr   added argument for wv logging in SIG and SM_MSGQ macros
02j,15apr98,cth  added event class WV_CLASS_TASKNAMES_PRESERVE/ON, and call to
		 _func_evtLogReserveTaskName
02i,08apr98,pr   reordered the definition of EVENT_SEMFLUSH and EVENT_SEMDELETE
02h,07apr98,cjtc added () to EVENT_INT_ENT to prevent problem with macro
		 expansion.
02g,22mar98,pr   modified EVT_CTX_BUF to accept TRG_USER_INDEX events.
		 modified definition of EVENT_SEMFLUSH and EVENT_SEMDELETE.
02f,04mar98,nps  modified xxx_CLASS_n_ON macros so they can be used by
                 inadequate assmblers.
02e,17feb98,pr   added number of params to EVT_OBJ macros.
02d,17feb98,dvs  added events for memory instrumentation. (pr)
02c,27jan98,cth  removed EVENT_BUFFER, CONFIG, BEGIN, END _SIZE definitions,
		 updated copyright
02b,22jan98,pr   made some macros readable also by the assembly.
		 added extra bit to evtAction.
		 added EVT_CTX_IDLE, EVT_CTX_NODISP macros.
02a,12jan98,dbs  added EVENT_NIL and EVENT_VALUE to pseudo-events.
01z,13dec97,pr   reduced the number of args passed to _func_trgCheck
                 moved some extern declaration in funcBindP.h
01y,20nov97,pr   added some event triggering defines and modified event
                 logging macros
01x,17nov97,cth  added EVENT_CONFIG_SIZE, _BEGIN_SIZE, _END_SIZE, _BUFFER_SIZE
01w,27oct97,pr   redefined levels as classes.
		 redesigned all the macros for managing classes.
01v,28aug97,pr   modified WV_OFF to reflect new levels.
01u,26aug97,pr   replaced WV_IS_ON with proper LEVEL
01t,18aug97,pr   added trigger calls.
01s,24jun97,pr   Added declarations for WindView 2.0 variables
		 modified macros to include new variable and triggers
01r,11Apr96,pr   changed if statement in EVT_OBJ_SM_MSGQ, SPR #6048, #6271
01q,22feb95,rdc  added EVENT_TASK_STATECHANGE.
01p,01feb95,rdc  added EVENT_TIMER_SYNC.
01o,27jan95,rdc  added processor number to EVENT_CONFIG.
01n,28nov94,rdc  changed 32 bit store macros for x86.
01m,02nov94,rdc  added protocol revision to EVENT_CONFIG.
		 added lockCnt param to EVT_CTX_TASKINFO.
01l,02nov94,rdc  added 960 macros.
01k,27may94,pad  added alignment macros. Corrected some parameter comments.
01j,14apr94,smb  optimised EVT_OBJ_ macros.
01i,04mar94,smb  added EVENT_INT_EXIT_K
		 changed parameters for EVENT_WIND_EXIT_NODISPATCH
		 removed EVENT_WIND_EXIT_IDLENOT
		 level 1 event optimisations
		 added macro for shared memory objects
01h,22feb94,smb  changed typedef EVENT_TYPE to event_t (SPR #3064)
01g,16feb94,smb  introduced new OBJ_ macros, SPR #2982
01f,26jan94,smb  redid pseudo events - generated host side
01e,24jan94,smb  added EVENT_WIND_EXIT_DISPATCH_PI and
		 EVENT_WIND_EXIT_NODISPATCH_PI
		 added event logging macros
01d,17dec93,smb  changed typedef of EVENT_TYPE
01c,10dec93,smb  modified MAX_LEVEL3_ID to include interrupt events
	  	 renamed EVENT_CLOCK_CONFIG to EVENT_CONFIG
01b,08dec93,kdl  added include of classLibP.h; made def of OBJ_INST_FUNC
	   +smb	 conditional on OBJ_INST_RTN; reformatted macro comments;
		 made includes and obj macros depend on CPU (i.e. not host).
01a,07dec93,smb  written.
*/

#ifndef __INCeventph
#define __INCeventph

#ifdef __cplusplus
extern "C" {
#endif

/* This file contains all windview event identifiers */

/* Triggering and Windview events definitions and macros.
 * They correspond to the old levels and are the only classes we
 * identify for the moment. They are also used by the architecture
 * dependent files, therefore are defined also for assembly languages.
 */

#ifdef	CPU				/* only for target, not host */

/* this is used by both WV and TRG */

#define CLASS_NONE    			0x00000000

#define WV_CLASS_1    			0x00000001   /* Context Switch */
#define WV_CLASS_2    			0x00000003   /* Task State Transition */
#define WV_CLASS_3    			0x00000007   /* Object and System Libraries */
#define WV_CLASS_TASKNAMES_PRESERVE	0x00001000
#define WV_ON           		0x10000000
#define WV_CLASS_1_ON  			0x10000001
#define WV_CLASS_2_ON 			0x10000003
#define WV_CLASS_3_ON  			0x10000007
#define WV_CLASS_TASKNAMES_PRESERVE_ON	0x10001000

#define TRG_CLASS_1     		0x00000001
#define TRG_CLASS_2     		0x00000010
#define TRG_CLASS_3     		0x00000100
#define TRG_CLASS_4     		0x00001000
#define TRG_CLASS_5     		0x00010000
#define TRG_CLASS_6     		0x01111111
#define TRG_ON          		0x10000000
#define TRG_CLASS_1_ON  		0x10000001
#define TRG_CLASS_2_ON  		0x10000010
#define TRG_CLASS_3_ON  		0x10000100
#define TRG_CLASS_4_ON  		0x10001000
#define TRG_CLASS_5_ON  		0x10010000
#define TRG_CLASS_6_ON  		0x11111111

#define TRG_CLASS1_INDEX	0
#define TRG_CLASS2_INDEX	1
#define TRG_CLASS3_INDEX	2
#define TRG_USER_INDEX		3
#define TRG_INT_ENT_INDEX	4
#define TRG_ANY_EVENT_INDEX	5
#define TRG_CONTROL_INDEX	6

#endif /* CPU */

/* types */

#ifndef _ASMLANGUAGE

typedef unsigned short event_t;

#ifdef	CPU				/* only for target, not host */

#include "private/funcBindP.h"
#include "private/classLibP.h"
#include "private/objLibP.h"


/*
 * All logging macros use this test to minimize overhead when logging is
 * disabled
 */
    
#define ACTION_IS_SET			(evtAction != 0)

#define TRG_ACTION_SET                  \
        {                               \
        evtAction |= 0x0001;            \
        TRG_EVTCLASS_SET(TRG_ON);       \
        }

#define TRG_ACTION_UNSET                \
        {                               \
        TRG_EVTCLASS_UNSET(TRG_ON);     \
        evtAction &= ~(0x0001);         \
        }

#define TRG_ACTION_IS_SET		( (evtAction&0x00ff) == 0x0001)

#define WV_ACTION_SET                   \
        {                               \
        evtAction |= 0x0100;            \
        WV_EVTCLASS_SET(WV_ON);         \
        }

#define WV_ACTION_UNSET                 \
        {                               \
        WV_EVTCLASS_UNSET(WV_ON);       \
        evtAction &= ~(0x0100);         \
        }

#define WV_ACTION_IS_SET		( (evtAction&0xff00) == 0x0100)

#define WV_EVTCLASS_IS_SET(class) 	( (wvEvtClass&(class)) == (class))
#define WV_EVTCLASS_SET(class) 		(wvEvtClass |= (class))
#define WV_EVTCLASS_UNSET(class)        (wvEvtClass &= ~(class))
#define WV_EVTCLASS_EMPTY	 	(wvEvtClass = CLASS_NONE)

#define TRG_EVTCLASS_IS_SET(class) 	((trgEvtClass&(class)) == (class))

#define TRG_EVTCLASS_SET(class) 	(trgEvtClass |= (class))
#define TRG_EVTCLASS_UNSET(class)	(trgEvtClass &= ~(class))
#define TRG_EVTCLASS_EMPTY	 	(trgEvtClass = CLASS_NONE)
#define TRG_EVTCLASS_IS_EMPTY	 	(trgEvtClass == CLASS_NONE)

/*
*
* OBJ_EVT_RTN - get instrumentation routine for an object
*
* RETURNS: instrumentation routine pointer from object class
*/

#define OBJ_EVT_RTN(objId)					\
    (((OBJ_CORE *)(objId))->pObjClass)->instRtn

#define TASK_EVT_RTN(tId)					\
    ((WIND_TCB *)(tId))->objCore.pObjClass->instRtn

#define OBJ_INSTRUMENTED(objId)					\
    ((((OBJ_CORE *)(objId))->handle.attributes) & WIND_OBJ_INSTRUMENTED)	

/* event logging macros */

/*
*
* OBJ_INST_FUNC - check if object is instrumented
*
* RETURNS: true if object is valid, and instrumented
*/

#define OBJ_INST_FUNC(objId, objClass)   			            \
        ((OBJ_VERIFY(objId,objClass) == OK) && (OBJ_INSTRUMENTED(objId)))

/*
*
* OBJ_VALID_WV_ASSERT - check if object is valid, and emit windview event if not
*
* This macro tests whether an object is instrumented. If it is, an OBJ_VERIFY()
* test is done, and a windview event emitted if it fails the test
*
* Action is like this:
*
* if (OBJ_INSTRUMENTED (objId))
*     {                         
*     if (OBJ_VERIFY (objId, classId) == OK)
*         return (1);   (instrumented and verified)
*     else
*         {             
*         eventLog (EVENT_OBJ_VERIFY_FAIL);
*         return (0);   (instrumented, not verified)
*         }
*     }
* else
*     return (0);       (not instrumented)
*
* RETURNS: true (non-zero) if obj is valid and instrumented, zero otherwise
*
*/

#define OBJ_VALID_WV_ASSERT(objId, objClass)                                  \
    (OBJ_INSTRUMENTED (objId) ?                                               \
        ((OBJ_VERIFY(objId,objClass) == OK) ?                                 \
            1 :                                                               \
            ((* _func_evtLogOIntLock)(EVENT_OBJ_VERIFY_FAIL,                  \
                                   2, objId, objClass),                       \
            0))                                                               \
        : 0)

/*
*
* TASK_INST_FUNC - check if task is instrumented
*
* RETURNS: instrumentation routine if instrumented, otherwise NULL
*/

#define TASK_INST_FUNC(tid)         OBJ_INST_FUNC(objId, taskClassId)


/*
*
* EVT_CTX_0 - context switch event logging with event id
*
*/

#define EVT_CTX_0(evtId)                                                    \
        if (ACTION_IS_SET)					            \
        {                                                                   \
            if ( WV_EVTCLASS_IS_SET(WV_CLASS_1|WV_ON) )                     \
                ( * _func_evtLogT0) (evtId);                                \
            if (TRG_EVTCLASS_IS_SET(TRG_CLASS_1|TRG_ON))		    \
		( * _func_trgCheck) (evtId, TRG_CLASS1_INDEX, NULL, NULL,   \
                                     NULL, NULL, NULL, NULL);               \
        }


/*
*
* EVT_CTX_1 - context switch event logging with one parameter
*
*/

#define EVT_CTX_1(evtId, arg)                                               \
    if ( ACTION_IS_SET )					            \
        {                                                                   \
        if ( WV_EVTCLASS_IS_SET(WV_CLASS_1|WV_ON) )                         \
            ( * _func_evtLogT1) (evtId, arg);                               \
        if ( TRG_EVTCLASS_IS_SET(TRG_CLASS_1|TRG_ON) )                      \
		( * _func_trgCheck) (evtId, TRG_CLASS1_INDEX, NULL, arg,    \
                                     NULL, NULL, NULL, NULL);               \
        }



/*
*
* EVT_CTX_IDLE - log EVENT_WIND_EXIT_IDLE event
*
* This macro stores information into the event buffer if the system is idle.
*
*/

#define EVT_CTX_IDLE(evtId, arg1)           	                            \
     if ( ACTION_IS_SET && (Q_FIRST (arg1) == NULL))		       	    \
        {                                                                   \
        if (WV_EVTCLASS_IS_SET(WV_CLASS_1|WV_ON))			    \
            (* _func_evtLogT0) (evtId);                                     \
        if ( TRG_EVTCLASS_IS_SET(TRG_CLASS_1|TRG_ON) )                      \
		( * _func_trgCheck) (evtId, TRG_CLASS1_INDEX, NULL, NULL,   \
                                     NULL, NULL, NULL, NULL);               \
        }


/*
*
* EVT_CTX_DISP - context switch event logging for reschedule
*
* This macro stores information into the event buffer about rescheduling tasks
* in the system. In order to determine the right event type, it first checks the
* two last args. It is used to log the DISPATCH event. Therefore the two last args
* are the current priority and the normal priority.
*
*/

#define EVT_CTX_DISP(evtId, arg1, arg2, arg3)           	            \
    if ( ACTION_IS_SET )					            \
        {                                                                   \
        if (arg3 > arg2)                                                    \
	    {                                                               \
            if (WV_EVTCLASS_IS_SET(WV_CLASS_1|WV_ON))		            \
                (* _func_evtLogTSched) (EVENT_WIND_EXIT_DISPATCH_PI, arg1,  \
                                        arg2);                              \
            if ( TRG_EVTCLASS_IS_SET(TRG_CLASS_1|TRG_ON) )                  \
		( * _func_trgCheck) (EVENT_WIND_EXIT_DISPATCH_PI,           \
                                     TRG_CLASS1_INDEX, NULL, arg1, arg2,    \
                                     NULL, NULL, NULL);                     \
	    }                                                               \
	    else                                                            \
	    {                                                               \
            if (WV_EVTCLASS_IS_SET(WV_CLASS_1|WV_ON))			    \
                (* _func_evtLogTSched) (EVENT_WIND_EXIT_DISPATCH, arg1,     \
                                        arg2);                              \
            if ( TRG_EVTCLASS_IS_SET(TRG_CLASS_1|TRG_ON) )                  \
		( * _func_trgCheck) (EVENT_WIND_EXIT_DISPATCH,              \
                                     TRG_CLASS1_INDEX, NULL, arg1, arg2,    \
                                     NULL, NULL, NULL);                     \
	    }                                                               \
        }

/*
*
* EVT_CTX_NODISP - context switch event logging for reschedule
*
* This macro stores information into the event buffer about rescheduling tasks
* in the system. In order to determine the right event type, it first checks the
* two last args. It is used to log the NODISPATCH event. Therefore the two last
* args are the current priority and the normal priority.
*
*/

#define EVT_CTX_NODISP(arg1, arg2, arg3)           	                    \
    if (ACTION_IS_SET)                                                      \
        {                                                                   \
        if (arg3 > arg2)                                                    \
	    {                                                               \
            if (WV_EVTCLASS_IS_SET(WV_CLASS_1|WV_ON))			    \
                (* _func_evtLogTSched) (EVENT_WIND_EXIT_NODISPATCH_PI,      \
                                        arg1, arg2);                        \
            if ( TRG_EVTCLASS_IS_SET(TRG_CLASS_1|TRG_ON) )	       	    \
		( * _func_trgCheck) (EVENT_WIND_EXIT_NODISPATCH_PI,         \
                                     TRG_CLASS1_INDEX, NULL, arg1, arg2,    \
                                     NULL, NULL, NULL);                     \
	    }                                                               \
	    else                                                            \
	    {                                                               \
            if (WV_EVTCLASS_IS_SET(WV_CLASS_1|WV_ON))			    \
               (* _func_evtLogTSched) (EVENT_WIND_EXIT_NODISPATCH, arg1,    \
                                       arg2);                               \
            if ( TRG_EVTCLASS_IS_SET(TRG_CLASS_1|TRG_ON) )	       	    \
		( * _func_trgCheck) (EVENT_WIND_EXIT_NODISPATCH,            \
                                     TRG_CLASS1_INDEX, NULL, arg1, arg2,    \
                                     NULL, NULL, NULL);                     \
	    }                                                               \
        }

/*
*
* EVT_CTX_TASKINFO - context switch event logging with task information
*
* This macro stores information into the event buffer about a task
* in the system.
*
*/

#define EVT_CTX_TASKINFO(evtId, state, pri, lockCnt, tid, entryPt, rtp, name) \
    if ( ACTION_IS_SET )						      \
        {                                                                     \
        if ( WV_EVTCLASS_IS_SET(WV_CLASS_1|WV_ON) )                           \
	    {						       	              \
            ( * _func_evtLogString) (evtId, 6, state, pri, lockCnt, tid,      \
                                     entryPt, rtp, name);                     \
									      \
	    if (_func_evtLogReserveTaskName != NULL)                          \
	     (* _func_evtLogReserveTaskName) (tid, evtId, 6,                  \
                                              state, pri, lockCnt, tid,       \
                                              entryPt, rtp, name);            \
	    }								      \
        if ( TRG_EVTCLASS_IS_SET(TRG_CLASS_1|TRG_ON) )                        \
		( * _func_trgCheck) (evtId, TRG_CLASS1_INDEX, NULL, state,    \
                                     pri, lockCnt, tid, name);                \
        }

    
    
/*
*
* EVT_CTX_RTPINFO - context switch event logging with RTPinformation
*
* This macro stores information into the event buffer about an RTP
* in the system.
*
*/

#define EVT_CTX_RTPINFO(evtId, options, ownerId, rtpId, name)           \
    if ( ACTION_IS_SET )						\
        {                                                       	\
        if ( WV_EVTCLASS_IS_SET(WV_CLASS_1|WV_ON) )                     \
	    {						       	        \
            ( * _func_evtLogString) (evtId, 3, options, ownerId,        \
                                     rtpId, 0, 0, 0, name);             \
									\
	    if (_func_evtLogReserveTaskName != NULL)                    \
	     (* _func_evtLogReserveTaskName) (rtpId, evtId, 3,          \
                                              options, ownerId,         \
                                              rtpId, 0, 0, 0, name);    \
	    }								\
        if ( TRG_EVTCLASS_IS_SET(TRG_CLASS_1|TRG_ON) )                  \
		( * _func_trgCheck) (evtId, TRG_CLASS1_INDEX, NULL,     \
                                     0, 0, 0, 0);                       \
        }
    
/*
*
* EVT_CTX_BUF - context switch event logging which logs a buffer,
*
* This macro stores eventpoint and user events into the event buffer.
*
*/

#define EVT_CTX_BUF(evtId, addr, bufSize, BufferAddr)		            \
    if ( ACTION_IS_SET )					            \
        {                                                                   \
        if ( WV_EVTCLASS_IS_SET(WV_CLASS_1|WV_ON) )                         \
             ( * _func_evtLogPoint) (evtId, addr, bufSize, BufferAddr);     \
        if ( TRG_EVTCLASS_IS_SET(TRG_CLASS_4|TRG_ON) )                      \
		( * _func_trgCheck) (evtId, TRG_USER_INDEX, NULL, addr,     \
                                     bufSize, BufferAddr, NULL, NULL);      \
        }

/*
*
* EVT_TASK_0 - task state transition event logging with event id.
*
*/

#define EVT_TASK_0(evtId)                                                   \
    if ( ACTION_IS_SET )					            \
        {                                                                   \
        if ( WV_EVTCLASS_IS_SET(WV_CLASS_2|WV_ON) )                         \
            (* _func_evtLogM0) (evtId);                                     \
        if ( TRG_EVTCLASS_IS_SET(TRG_CLASS_2|TRG_ON) )                      \
		( * _func_trgCheck) (evtId, TRG_CLASS2_INDEX, NULL, NULL,   \
                                     NULL, NULL, NULL, NULL);               \
        }

/*
*
* EVT_TASK_1 - task state transition event logging with one argument.
*
*/

#define EVT_TASK_1(evtId, arg)                                              \
  do                                                                        \
   {                                                                        \
    if ( ACTION_IS_SET )					            \
        {                                                                   \
        if ( WV_EVTCLASS_IS_SET(WV_CLASS_2|WV_ON) )                         \
            (* _func_evtLogM1) (evtId, arg);                                \
        if ( TRG_EVTCLASS_IS_SET(TRG_CLASS_2|TRG_ON) )                      \
		( * _func_trgCheck) (evtId, TRG_CLASS2_INDEX, NULL, arg,    \
                                     NULL, NULL, NULL, NULL);               \
        }                                                                   \
   } while (0)


/*
*
* EVT_TASK_2 - task state transition event logging with two arguments.
*
*/

#define EVT_TASK_2(evtId, arg1, arg2)                                       \
    if ( ACTION_IS_SET )					            \
        {                                                                   \
        if ( WV_EVTCLASS_IS_SET(WV_CLASS_2|WV_ON) )                         \
            (* _func_evtLogM2) (evtId, arg1, arg2);                         \
        if ( TRG_EVTCLASS_IS_SET(TRG_CLASS_2|TRG_ON) )                      \
		( * _func_trgCheck) (evtId, TRG_CLASS2_INDEX, NULL, arg1,   \
                                     arg2, NULL, NULL, NULL);               \
        }

/*
*
* EVT_TASK_3 - task state transition event logging with three arguments.
*
*/

#define EVT_TASK_3(evtId, arg1, arg2, arg3)                                 \
    if ( ACTION_IS_SET )					            \
        {                                                                   \
        if ( WV_EVTCLASS_IS_SET(WV_CLASS_2|WV_ON) )                         \
            (* _func_evtLogM3) (evtId, arg1, arg2, arg3);                   \
        if ( TRG_EVTCLASS_IS_SET(TRG_CLASS_2|TRG_ON) )                      \
		( * _func_trgCheck) (evtId, TRG_CLASS2_INDEX, NULL, arg1,   \
                                     arg2, arg3, NULL, NULL);               \
        }

/*
*
* EVT_SYSCALL_ENTRY - record syscall event with params
*
*/

#define EVT_SYSCALL_ENTRY(rtpId, nParams, pState)                           \
    if ( ACTION_IS_SET )			         		    \
        {                                                                   \
        if ( WV_EVTCLASS_IS_SET(WV_CLASS_3|WV_ON) )                         \
            {                                                               \
            if (OBJ_INST_FUNC(rtpId, rtpClassId))			    \
	       (* _func_evtLogSyscall)                                      \
	       (nParams, pState);                                           \
            }                                                               \
            /* TODO: Implement trigger test */                              \
        }

/*
*
* EVT_OBJ_6 - object status event logging with six arguments.
*
*/

#define EVT_OBJ_6(objId, classId, evtId, arg1, arg2, arg3,                  \
		     arg4, arg5, arg6)					    \
    if ( ACTION_IS_SET )			         		    \
        {                                                                   \
        if ( WV_EVTCLASS_IS_SET(WV_CLASS_3|WV_ON) )                         \
            {                                                               \
            if (OBJ_VALID_WV_ASSERT(objId, classId))   		            \
                if (OBJ_INST_FUNC(objId, classId))			    \
	            (* (OBJ_EVT_RTN(objId)))                                \
	                (evtId, 6, arg1, arg2, arg3, arg4, arg5, arg6);     \
            }                                                               \
        if ( TRG_EVTCLASS_IS_SET(TRG_CLASS_3|TRG_ON) )                      \
	    ( * _func_trgCheck)                                             \
	    (evtId, TRG_CLASS3_INDEX, objId, arg1, arg2, arg3, arg4, arg5); \
        }

/*
*
* EVT_OBJ_5 - object status event logging with five arguments.
*
*/

#define EVT_OBJ_5(objId, classId, evtId, arg1, arg2, arg3,                  \
		     arg4, arg5)					    \
   if ( ACTION_IS_SET )					                    \
        {                                                                   \
        if ( WV_EVTCLASS_IS_SET(WV_CLASS_3|WV_ON) )                         \
            {                                                               \
            if (OBJ_VALID_WV_ASSERT(objId, classId))                        \
                if (OBJ_INST_FUNC(objId, classId))			    \
	            (* (OBJ_EVT_RTN(objId)))                                \
	                 (evtId, 5, arg1, arg2, arg3, arg4, arg5);          \
            }                                                               \
        if ( TRG_EVTCLASS_IS_SET(TRG_CLASS_3|TRG_ON) )                      \
	    ( * _func_trgCheck)                                             \
	    (evtId, TRG_CLASS3_INDEX, objId, arg1, arg2, arg3, arg4, arg5); \
        }

/*
*
* EVT_OBJ_4 - object status event logging with four arguments.
*
*/

#define EVT_OBJ_4(objId, classId, evtId, arg1, arg2, arg3, arg4)             \
    if ( ACTION_IS_SET )					             \
        {                                                                    \
        if ( WV_EVTCLASS_IS_SET(WV_CLASS_3|WV_ON) )                          \
            {                                                                \
            if (OBJ_VALID_WV_ASSERT(objId, classId))                         \
                if (OBJ_INST_FUNC(objId, classId))			     \
	            (* (OBJ_EVT_RTN(objId)))                                 \
	               (evtId, 4, arg1, arg2, arg3, arg4);                   \
            }                                                                \
        if ( TRG_EVTCLASS_IS_SET(TRG_CLASS_3|TRG_ON) )                       \
	    ( * _func_trgCheck)                                              \
	    (evtId, TRG_CLASS3_INDEX, objId, arg1, arg2, arg3, arg4, NULL);  \
        }

/*
*
* EVT_OBJ_3 - object status event logging with three arguments.
*
*/

#define EVT_OBJ_3(objId, classId, evtId, arg1, arg2, arg3)                   \
    if ( ACTION_IS_SET )					             \
        {                                                                    \
        if ( WV_EVTCLASS_IS_SET(WV_CLASS_3|WV_ON) )                          \
            {                                                                \
            if (OBJ_VALID_WV_ASSERT(objId, classId))                         \
                if (OBJ_INST_FUNC(objId, classId))			     \
	            (* (OBJ_EVT_RTN(objId)))                                 \
        	       (evtId, 3, arg1, arg2, arg3);                         \
            }                                                                \
        if ( TRG_EVTCLASS_IS_SET(TRG_CLASS_3|TRG_ON) )                       \
	    ( * _func_trgCheck)                                              \
	    (evtId, TRG_CLASS3_INDEX, objId, arg1, arg2, arg3, NULL, NULL);  \
        }
/*
*
* EVT_OBJ_2 - object status event logging with two arguments.
*
*/

#define EVT_OBJ_2(objId, classId, evtId, arg1, arg2)	                    \
    if ( ACTION_IS_SET )					            \
        {                                                                   \
        if ( WV_EVTCLASS_IS_SET(WV_CLASS_3|WV_ON) )                         \
            {                                                               \
            if (OBJ_VALID_WV_ASSERT(objId, classId))                        \
                if (OBJ_INST_FUNC(objId, classId))			    \
	            (* (OBJ_EVT_RTN(objId))) (evtId, 2, arg1, arg2);        \
            }                                                               \
        if ( TRG_EVTCLASS_IS_SET(TRG_CLASS_3|TRG_ON) )                      \
	    ( * _func_trgCheck)                                             \
	    (evtId, TRG_CLASS3_INDEX, objId, arg1, arg2, NULL, NULL, NULL); \
        }

/*
*
* EVT_OBJ_1 - object status event logging with an argument.
*
*/

#define EVT_OBJ_1(objId, classId, evtId, arg1)		                    \
    if ( ACTION_IS_SET )					            \
        {                                                                   \
        if ( WV_EVTCLASS_IS_SET(WV_CLASS_3|WV_ON) )                         \
            {                                                               \
            if (OBJ_VALID_WV_ASSERT(objId, classId))                        \
                if (OBJ_INST_FUNC(objId, classId))			    \
	           (* (OBJ_EVT_RTN(objId)))(evtId, 1, arg1);                \
            }                                                               \
        if ( TRG_EVTCLASS_IS_SET(TRG_CLASS_3|TRG_ON) )                      \
	    ( * _func_trgCheck)                                             \
	    (evtId, TRG_CLASS3_INDEX, objId, arg1, NULL, NULL, NULL, NULL); \
        }


/*
*
* EVT_OBJ_STR_CLASS_N - object status event logging with an argument.
*
*/

#define EVT_STR_CLASS_N(classId, evtId, nArgs,                              \
                         arg1, arg2, arg3, arg4, arg5, arg6,                \
                         length, data)		                            \
    if ( ACTION_IS_SET )					            \
        {                                                                   \
        if ( WV_EVTCLASS_IS_SET(WV_CLASS_3|WV_ON) )                         \
            {                                                               \
            if (WV_OBJ_CLASS_IS_INSTRUMENTED(classId))                      \
                    ( * _func_evtLogOBinary) (evtId, nArgs,                 \
                                              arg1, arg2, arg3,             \
                                              arg4, arg5, arg6,             \
                                              length, data);                \
            }                                                               \
        if ( TRG_EVTCLASS_IS_SET(TRG_CLASS_3|TRG_ON) )                      \
	    ( * _func_trgCheck)                                             \
	    (evtId, TRG_CLASS3_INDEX, NULL, NULL, NULL, NULL, NULL, NULL);  \
        }

/*
*
* EVT_OBJ_STR_2 - object event logging with 2 params and a binary argument.
*
*/

#define EVT_OBJ_STR_2(objId, classId, evtId,                                \
                      arg1, arg2, length, string)                           \
    if ( ACTION_IS_SET )					            \
        {                                                                   \
        if ( WV_EVTCLASS_IS_SET(WV_CLASS_3|WV_ON) )                         \
            {                                                               \
            if (OBJ_VALID_WV_ASSERT(objId, classId))                        \
                if (OBJ_INST_FUNC(objId, classId))			    \
                    ( * _func_evtLogOBinary) (evtId, 2, arg1, arg2, 0,      \
                                              0, 0, 0,                      \
                                              length, string);              \
            }                                                               \
        if ( TRG_EVTCLASS_IS_SET(TRG_CLASS_3|TRG_ON) )                      \
	    ( * _func_trgCheck)                                             \
	    (evtId, TRG_CLASS3_INDEX, objId, arg1, NULL, NULL, NULL, NULL); \
        }

/*
*
* EVT_OBJ_STR_3 - object event logging with 3 params and a binary argument.
*
*/

#define EVT_OBJ_STR_3(objId, classId, evtId,                                \
                      arg1, arg2, arg3,length, string)                      \
    if ( ACTION_IS_SET )					            \
        {                                                                   \
        if ( WV_EVTCLASS_IS_SET(WV_CLASS_3|WV_ON) )                         \
            {                                                               \
            if (OBJ_VALID_WV_ASSERT(objId, classId))                        \
                if (OBJ_INST_FUNC(objId, classId))			    \
                    ( * _func_evtLogOBinary) (evtId, 3, arg1, arg2, arg3,   \
                                              0, 0, 0,                      \
                                              length, string);              \
            }                                                               \
        if ( TRG_EVTCLASS_IS_SET(TRG_CLASS_3|TRG_ON) )                      \
	    ( * _func_trgCheck)                                             \
	    (evtId, TRG_CLASS3_INDEX, objId, arg1, NULL, NULL, NULL, NULL); \
        }

/*
*
* EVT_OBJ_STR_5 - object event logging with 5 params and a binary argument.
*
*/

#define EVT_OBJ_STR_5(objId, classId, evtId,                                \
                      arg1, arg2, arg3, arg4, arg5, length, string)         \
    if ( ACTION_IS_SET )					            \
        {                                                                   \
        if ( WV_EVTCLASS_IS_SET(WV_CLASS_3|WV_ON) )                         \
            {                                                               \
            if (OBJ_VALID_WV_ASSERT(objId, classId))                        \
                if (OBJ_INST_FUNC(objId, classId))			    \
                    ( * _func_evtLogOBinary) (evtId, 5, arg1, arg2, arg3,   \
                                              arg4, arg5, 0,                \
                                              length, string);              \
            }                                                               \
        if ( TRG_EVTCLASS_IS_SET(TRG_CLASS_3|TRG_ON) )                      \
	    ( * _func_trgCheck)                                             \
	    (evtId, TRG_CLASS3_INDEX, objId, arg1, NULL, NULL, NULL, NULL); \
        }

/*
*
* EVT_OBJ_TASKSPAWN - object status event logging for a spawned task.
*
*/

#define EVT_OBJ_TASKSPAWN(evtId, arg1, arg2, arg3, arg4, arg5)              \
    if ( ACTION_IS_SET )					            \
        {                                                                   \
        if ( WV_EVTCLASS_IS_SET(WV_CLASS_3|WV_ON) )                         \
              ( * _func_evtLogOIntLock) (evtId, 5, arg1, arg2, arg3,        \
                                         arg4, arg5);                       \
        if ( TRG_EVTCLASS_IS_SET(TRG_CLASS_3|TRG_ON) )                      \
		( * _func_trgCheck) (evtId, TRG_CLASS3_INDEX, NULL, arg1,   \
                                     arg2, arg3, arg4, arg5);               \
        }

/*
*
* EVT_OBJ_SIG_0 - signal event logging with no parameter
*
*/

#define EVT_OBJ_SIG_0(evtId)			                            \
    if ( ACTION_IS_SET )					            \
        {                                                                   \
        if ( WV_EVTCLASS_IS_SET(WV_CLASS_3|WV_ON) )                         \
            if (sigEvtRtn != NULL)                                          \
                (* sigEvtRtn) (evtId, 0);                                   \
        if ( TRG_EVTCLASS_IS_SET(TRG_CLASS_3|TRG_ON) )                      \
		( * _func_trgCheck) (evtId, TRG_CLASS3_INDEX, NULL, NULL,   \
                                     NULL, NULL, NULL, NULL);               \
        }
/*
*
* EVT_OBJ_SIG_1 - signal event logging with 1 parameter
*
*/

#define EVT_OBJ_SIG_1(evtId, arg1)			                    \
    if ( ACTION_IS_SET )					            \
        {                                                                   \
        if ( WV_EVTCLASS_IS_SET(WV_CLASS_3|WV_ON) )                         \
            if (sigEvtRtn != NULL)                                          \
                (* sigEvtRtn) (evtId, 1, arg1);                             \
        if ( TRG_EVTCLASS_IS_SET(TRG_CLASS_3|TRG_ON) )                      \
		( * _func_trgCheck) (evtId, TRG_CLASS3_INDEX, NULL, arg1,   \
                                     NULL, NULL, NULL, NULL);               \
        }

/*
*
* EVT_OBJ_SIG - signal event logging
*
*/

#define EVT_OBJ_SIG(evtId, nParam, arg1, arg2)			            \
    if ( ACTION_IS_SET )					            \
        {                                                                   \
        if ( WV_EVTCLASS_IS_SET(WV_CLASS_3|WV_ON) )                         \
            if (sigEvtRtn != NULL)                                          \
                (* sigEvtRtn) (evtId, nParam, arg1, arg2);                  \
        if ( TRG_EVTCLASS_IS_SET(TRG_CLASS_3|TRG_ON) )                      \
		( * _func_trgCheck) (evtId, TRG_CLASS3_INDEX, NULL, arg1,   \
                                     arg2, NULL, NULL, NULL);               \
        }

/*
*
* EVT_OBJ_SIG_2 - signal event logging
*
*/

#define EVT_OBJ_SIG_2(evtId, arg1, arg2)			            \
    if ( ACTION_IS_SET )					            \
        {                                                                   \
        if ( WV_EVTCLASS_IS_SET(WV_CLASS_3|WV_ON) )                         \
            if (sigEvtRtn != NULL)                                          \
                (* sigEvtRtn) (evtId, 2, arg1, arg2);                       \
        if ( TRG_EVTCLASS_IS_SET(TRG_CLASS_3|TRG_ON) )                      \
		( * _func_trgCheck) (evtId, TRG_CLASS3_INDEX, NULL, arg1,   \
                                     arg2, NULL, NULL, NULL);               \
        }

/*
*
* EVT_OBJ_SIG_3 - signal event logging with 3 parameters
*
*/

#define EVT_OBJ_SIG_3(evtId, arg1, arg2, arg3)			            \
    if ( ACTION_IS_SET )					            \
        {                                                                   \
        if ( WV_EVTCLASS_IS_SET(WV_CLASS_3|WV_ON) )                         \
            if (sigEvtRtn != NULL)                                          \
                (* sigEvtRtn) (evtId, 3, arg1, arg2, arg3);                 \
        if ( TRG_EVTCLASS_IS_SET(TRG_CLASS_3|TRG_ON) )                      \
		( * _func_trgCheck) (evtId, TRG_CLASS3_INDEX, NULL, arg1,   \
                                     arg2, arg3, NULL, NULL);               \
        }

/*
*
* EVT_OBJ_SIG_4 - signal event logging with 4 parameters
*
*/

#define EVT_OBJ_SIG_4(evtId, arg1, arg2, arg3, arg4)			    \
    if ( ACTION_IS_SET )					            \
        {                                                                   \
        if ( WV_EVTCLASS_IS_SET(WV_CLASS_3|WV_ON) )                         \
            if (sigEvtRtn != NULL)                                          \
                (* sigEvtRtn) (evtId, 4, arg1, arg2, arg3, arg4);           \
        if ( TRG_EVTCLASS_IS_SET(TRG_CLASS_3|TRG_ON) )                      \
		( * _func_trgCheck) (evtId, TRG_CLASS3_INDEX, NULL, arg1,   \
                                     arg2, arg3, arg4, NULL);               \
        }

/*
*
* EVT_OBJ_SAL_1 - SAL event logging with 1 parameter
*
*/

#define EVT_OBJ_SAL_1(evtId, arg1)                                          \
    if ( ACTION_IS_SET )                                                    \
        {                                                                   \
        if ( WV_EVTCLASS_IS_SET(WV_CLASS_3|WV_ON) )                         \
            if (salEvtRtn != NULL)                                          \
                (* salEvtRtn) (evtId, 1, arg1);                             \
        if ( TRG_EVTCLASS_IS_SET(TRG_CLASS_3|TRG_ON) )                      \
                ( * _func_trgCheck) (evtId, TRG_CLASS3_INDEX, NULL, arg1,   \
                                     NULL, NULL, NULL, NULL);               \
        }

/*
*
* EVT_OBJ_SAL_2 - SAL event logging with 2 parameters
*
*/

#define EVT_OBJ_SAL_2(evtId, arg1, arg2)                                    \
    if ( ACTION_IS_SET )                                                    \
        {                                                                   \
        if ( WV_EVTCLASS_IS_SET(WV_CLASS_3|WV_ON) )                         \
            if (salEvtRtn != NULL)                                          \
                (* salEvtRtn) (evtId, 2, arg1, arg2);                       \
        if ( TRG_EVTCLASS_IS_SET(TRG_CLASS_3|TRG_ON) )                      \
                ( * _func_trgCheck) (evtId, TRG_CLASS3_INDEX, NULL, arg1,   \
                                     arg2, NULL, NULL, NULL);               \
        }

/*
*
* EVT_OBJ_SAL_STR_2 - SAL event logging with 2 parameters
*
*/

#define EVT_OBJ_SAL_STR_2(evtId, name, fd)                                  \
    if ( ACTION_IS_SET )                                                    \
        {                                                                   \
        if ( WV_EVTCLASS_IS_SET(WV_CLASS_3|WV_ON) )                         \
            if (salEvtRtn != NULL)                                          \
                ( * _func_evtLogOBinary) (evtId, 1, fd, 0, 0, 0, 0, 0,      \
                                          strlen (name), name);             \
        if ( TRG_EVTCLASS_IS_SET(TRG_CLASS_3|TRG_ON) )                      \
                ( * _func_trgCheck) (evtId, TRG_CLASS3_INDEX, NULL, name,   \
                                     fd, NULL, NULL, NULL);                 \
        }

/*
*
* EVT_OBJ_SAL_3 - SAL event logging with 3 parameters
*
*/

#define EVT_OBJ_SAL_3(evtId, arg1, arg2, arg3)                              \
    if ( ACTION_IS_SET )                                                    \
        {                                                                   \
        if ( WV_EVTCLASS_IS_SET(WV_CLASS_3|WV_ON) )                         \
            if (salEvtRtn != NULL)                                          \
                (* salEvtRtn) (evtId, 3, arg1, arg2, arg3);                 \
        if ( TRG_EVTCLASS_IS_SET(TRG_CLASS_3|TRG_ON) )                      \
                ( * _func_trgCheck) (evtId, TRG_CLASS3_INDEX, NULL, arg1,   \
                                     arg2, arg3, NULL, NULL);               \
        }

/*
*
* EVT_OBJ_EVENT - events (from eventLib) event logging
*
*/

#define EVT_OBJ_EVENT(evtId, nParam, arg1, arg2, arg3)	                    \
    if ( ACTION_IS_SET )					            \
        {                                                                   \
        if ( WV_EVTCLASS_IS_SET(WV_CLASS_3|WV_ON) )                         \
            if (eventEvtRtn != NULL)                                        \
                (* eventEvtRtn) (evtId, nParam, arg1, arg2, arg3);          \
        if ( TRG_EVTCLASS_IS_SET(TRG_CLASS_3|TRG_ON) )                      \
		( * _func_trgCheck) (evtId, TRG_CLASS3_INDEX, NULL,         \
                                     arg1, arg2, arg3, NULL, NULL);         \
        }

/*
*
* EVT_EDR_ERR_INJECT - events (from edrLib) event logging
*
*/

#define EVT_EDR_ERR_INJECT(evtId, arg1, arg2, arg3, arg4, arg5)	        \
    if ( ACTION_IS_SET )						\
        {								\
        if ( WV_EVTCLASS_IS_SET(WV_CLASS_3|WV_ON) )			\
            if (_func_edrEvt != NULL)					\
                (* _func_edrEvt) (evtId, arg1, arg2, arg3, arg4, arg5);	\
        if ( TRG_EVTCLASS_IS_SET(TRG_CLASS_3|TRG_ON) )			\
		( * _func_trgCheck) (evtId, TRG_CLASS3_INDEX, NULL,	\
                                     arg1, arg2, arg3, arg4, arg5);	\
        }

/*
*
* INST_SIG_INSTALL - install event logging routine for signals
*
*/

#define INST_SIG_INSTALL					\
    if ( ACTION_IS_SET )					\
        {                                                       \
        if ( WV_EVTCLASS_IS_SET(WV_CLASS_3|WV_ON) )             \
            {                                                   \
                sigEvtRtn = _func_evtLogOIntLock;               \
            else                                                \
                sigEvtRtn = NULL;                               \
            }                                                   \
        }

/*
*
* EVT_OBJ_SM_MSGQ - object status event logging for SMO message queues.
*
*/

#define EVT_OBJ_SM_MSGQ(evtId, arg1, arg2, arg3, arg4, arg5, arg6)          \
    if ( ACTION_IS_SET )					            \
        {                                                                   \
        if ( WV_EVTCLASS_IS_SET(WV_CLASS_3|WV_ON) )                         \
                (* _func_evtLogOIntLock) (evtId, arg6, arg1, arg2, arg3,    \
                                          arg4, arg5);                      \
        if ( TRG_EVTCLASS_IS_SET(TRG_CLASS_3|TRG_ON) )                      \
		( * _func_trgCheck) (evtId, TRG_CLASS3_INDEX, arg1, arg2,   \
                                     arg3, arg4, arg5);                     \
        }

    
/*
*
* EVT_STR_N - record an event with a timestamps, N-params and a string
*
*/

#define EVT_STR_N(evtId, nParams, arg1, arg2, arg3, arg4, arg5, arg6,       \
                  length, string)                                           \
    if ( ACTION_IS_SET )                                                    \
        {                                                                   \
        if ( WV_EVTCLASS_IS_SET(WV_CLASS_3|WV_ON) )                         \
            if (_func_evtLogOBinary != NULL)                                \
                ( * _func_evtLogOBinary) (evtId, nParams, arg1, arg2, arg3, \
                                          arg4, arg5, arg6, length, string);\
        if ( TRG_EVTCLASS_IS_SET(TRG_CLASS_3|TRG_ON) )                      \
                ( * _func_trgCheck) (evtId, TRG_CLASS3_INDEX, NULL, NULL,   \
                                     NULL, NULL, NULL, NULL);               \
        }

/******************************************************************************
*
* EVT_NOOBJ_STR_0 - event logging with strings 
*
* This macro stores object level string information for places where the objId
* is not available.  The string must be arg1.
*
* NOMANUAL
*/

#define EVT_NOOBJ_STR_0(evtId,length,string)                                \
    if ( ACTION_IS_SET )			         		    \
        {                                                                   \
        if ( WV_EVTCLASS_IS_SET(WV_CLASS_3|WV_ON) )                         \
	    ( * _func_evtLogOBinary) (evtId, 0, 0, 0, 0, 0, 0, 0,           \
                                      length, string);                      \
        if ( TRG_EVTCLASS_IS_SET(TRG_CLASS_3|TRG_ON) )                      \
	    ( * _func_trgCheck)                                             \
	    (evtId, TRG_CLASS3_INDEX, NULL, NULL, NULL, NULL, NULL, NULL);  \
        }


    
#endif	/* CPU */			/* end target-only section */
#endif  /* _ASMLANGUAGE */

#define WV_REV_ID_T2    0xb0b00000
#define WV_REV_ID_T3    0xcdcd0000
#define WV_REV_ID_BASE6 0xb1b10000      /* based on VxWorks 5 */
    
#define WV_EVT_PROTO_REV_1_0_FCS 1

#define WV_EVT_PROTO_REV_2_0_FCS 2
#define WV_EVT_PROTO_REV_3_0_FCS 3
#define WV_EVT_PROTO_REV_3_1_FCS 4
#define WV_EVT_PROTO_REV_2_2_FCS 5

/* Base6 parser revisions */
    
#define WV_EVT_PROTO_REV_1_EAR   1

    
#define WV_REV_ID_CURRENT               WV_REV_ID_BASE6
#define WV_EVT_PROTO_REV_CURRENT        WV_EVT_PROTO_REV_1_EAR

/* ranges for the different classes of events */

#define MIN_CONTROL_ID 	0
#define MAX_CONTROL_ID	49
#define MIN_CLASS1_ID	50
#define MAX_CLASS1_ID	599
#define MIN_INT_ID	102
#define PPC_WD_INT_ID	598
#define PPC_DECR_INT_ID	599
#define MAX_INT_ID	599
#define MIN_CLASS2_ID	600
#define MAX_CLASS2_ID	9999
#define MIN_CLASS3_ID	10000
#define MAX_CLASS3_ID	19999 
#define MIN_RESERVE_ID	20000

/*
 * Network event IDs can go from 20000 - 20292
 * These are present in 5.x and AE
 */
    
/*
 * Dual-stack network event IDs go from 20400 - 20475
 * though they go in steps of 15
 */
    
/* range for synthetic syscall event Ids */

#define MIN_SYSCALL_ID  21000
#define MAX_SYSCALL_ID  21999
    
#define MIN_RTI_RESERVE_ID      39000
#define MAX_RTI_RESERVE_ID      39999
#define MAX_RESERVE_ID	39999 
#define MIN_USER_ID	40000
#define MAX_USER_ID	65535

#define INT_LEVEL(eventNum) ((eventNum)-MIN_INT_ID)

#define IS_CLASS1_EVENT(event) \
    ((event >= MIN_CLASS1_ID) && (event <= MAX_CLASS1_ID))

#define IS_CLASS2_EVENT(event) \
    ((event >= MIN_CLASS2_ID) && (event <= MAX_CLASS2_ID))

#define IS_CLASS3_EVENT(event) \
    ((event >= MIN_CLASS3_ID) && (event <= MAX_CLASS3_ID))

#define IS_INT_ENT_EVENT(event) \
    ((event >= MIN_INT_ID) && (event <= MAX_INT_ID))

#define IS_CONTROL_EVENT(event) \
    ((event >= MIN_CONTROL_ID) && (event <= MAX_CONTROL_ID))

#define IS_USER_EVENT(event) \
    ((event >= MIN_USER_ID) && (event <= MAX_USER_ID))

/*
 * This test determines whether the parser should translate
 * the event into the correct syscall
 */
    
#define IS_SYSCALL_EVENT(event) \
    ((event >= EVENT_SYSCALL_0) && (event <= EVENT_SYSCALL_8))

#define CONTROL_EVENT(id) (MIN_CONTROL_ID + id)
#define CLASS1_EVENT(id)  (MIN_CLASS1_ID + id)
#define CLASS2_EVENT(id)  (MIN_CLASS2_ID + id)
#define CLASS3_EVENT(id)  (MIN_CLASS3_ID + id)
#define INT_EVENT(id)     (MIN_INT_ID + id)


/* EVENT Id's */

/* Interrupt events */

#define EVENT_INT_ENT(k) 		((k) + MIN_INT_ID)
    /* Param:
     *   short EVENT_INT_ENT(k), 
     *   int timeStamp
     */

#define EVENT_INT_EXIT 	 		(MIN_INT_ID - 1)
    /* Param:
     *   short EVENT_INT_EXIT, 
     *   int timeStamp
     */

#define EVENT_INT_EXIT_K                (MIN_INT_ID - 2)
    /* Param:
     *   short EVENT_INT_EXIT_K,
     *   int timeStamp
     */

/* Control events */

/*
 * The length includes a string buffer. The buffer should be padded to the next
 * half-word boundary
 */
    
#define EVENT_BEGIN_SIZE(stringBuffLen)                 \
   (sizeof(short) + (4 * sizeof(int)) + stringBuffLen)

#define EVENT_BEGIN                     CONTROL_EVENT(0)
    /* Param:
     *   short EVENT_BEGIN, 
     *   int CPU, 
     *   int bspSize, 
     *   char * bspName, 
     *   int taskIdCurrent, 
     *   int collectionMode
     */


#define EVENT_TIMER_ROLLOVER            CONTROL_EVENT(2)
    /* Param:
     *   short EVENT_TIMER_ROLLOVER
     *   int timeStamp, 
     */

 /*
  * The size of this event includes the string buffer, which must be padded to
  * the next half-word boundary
  */
    
#define EVENT_TASKNAME_SIZE(stringBuffLen)              \
  (sizeof(short) + 7 * sizeof(int) + stringBuffLen)

#define	EVENT_TASKNAME			CONTROL_EVENT(3)
    /* Param:
     *   short EVENT_TASKNAME, 
     *   int status, 
     *   int priority, 
     *   int taskLockCount
     *   int tid, 
     *   int entryPoint
     *   int rtp
     *   int namesize
     *   char * name
     */

#define EVENT_CONFIG_SIZE  (sizeof (short) + (1 * sizeof(int)))

#define EVENT_CONFIG              	CONTROL_EVENT(4)
    /* Param:
     *   int revision                   WV_REV_ID | WV_EVT_PROTO_REV
     */


/*
 * The size of this event includes the string buffer, which must be padded to
 * half-word boundary
 */
    
#define EVENT_COMMENT_SIZE(stringBuffLength)            \
 (sizeof(short) + sizeof(int) + stringBuffLength)

    
#define EVENT_LOGCOMMENT                CONTROL_EVENT(7)
     /* Param:
      *   int length
      *   string comment
      */

    
#define EVENT_TIMESTAMP_CONFIG_SIZE                     \
 (sizeof(short) + 4 * sizeof(int))

#define EVENT_TIMESTAMP_CONFIG          CONTROL_EVENT(8)
    
    /*
     *   short eventId
     *   int timestampFreq, 
     *   int timestampPeriod,
     *   int autoRollover, 
     *   int clkRate,
     */

#define EVENT_ISR_INFO_SIZE(stringBuffLength)           \
 (sizeof(short) + 3 * sizeof(int) + stringBuffLength)
    
#define EVENT_ISR_INFO                  CONTROL_EVENT(9)
    /* Param:
     *   short EVENT_ISR_INFO, 
     *   int objId
     *   int handler
     *   int nameLen
     *   char * name,
     */

#define	EVENT_RTPNAME			CONTROL_EVENT(16)
    /* Param:
     *   short EVENT_RTPNAME,
     *   UINT32 options
     *   UINT32 owner
     *   UINT32 rtpId
     *   UINT32 nameLength
     *   UINT32 name
     */
    

#define EVENT_ANY_EVENT			CONTROL_EVENT(48)
    /* This is added for triggering, when no particular event is required */

/* CLASS3 events */

#define	EVENT_TASKSPAWN		                CLASS3_EVENT(0)
    /* Param:
     *   short EVENT_TASKSPAWN, 
     *   int timeStamp, 
     *   int options, 
     *   int entryPt, 
     *   intstackSize, 
     *   int priority, 
     *   int pTcb
     */

#define	EVENT_TASKDESTROY			CLASS3_EVENT(1)
    /* Param:
     *    short EVENT_TASKDESTROY, 
     *    int timeStamp, 
     *    int safeCnt, 
     *    int pTcb
     */

#define	EVENT_TASKDELAY				CLASS3_EVENT(2)
    /* Param:
     *   short EVENT_TASKDELAY, 
     *   int timeStamp, 
     *   int ticks
     */

#define	EVENT_TASKPRIORITYSET			CLASS3_EVENT(3)
    /* Param:
     *   short EVENT_TASKPRIORITYSET, 
     *   int timeStamp, 
     *   int oldPri, 
     *   int newPri, 
     *   int pTcb
     */

#define	EVENT_TASKSUSPEND			CLASS3_EVENT(4)
    /* Param:
     *   short EVENT_TASKSUSPEND, 
     *   int timeStamp, 
     *   int pTcb
     */

#define	EVENT_TASKRESUME			CLASS3_EVENT(5)
    /* Param:
     *   short EVENT_TASKRESUME, 
     *   int timeStamp, 
     *   int priority, 
     *   int pTcb
     */

#define	EVENT_TASKSAFE				CLASS3_EVENT(6)
    /* Param:
     *   short EVENT_TASKSAFE, 
     *   int timeStamp, 
     *   int safeCnt, 
     *   int tid
     */

#define	EVENT_TASKUNSAFE			CLASS3_EVENT(7)
    /* Param:
     *   short EVENT_TASKUNSAFE, 
     *   int timeStamp, 
     *   int safeCnt, 
     *   int tid
     */

#define	EVENT_SEMBCREATE			CLASS3_EVENT(8)
    /* Param:
     *   short EVENT_SEMBCREATE, 
     *   int timeStamp, 
     *   int semOwner, 
     *   int options, 
     *   int semId
     */

#define	EVENT_SEMCCREATE			CLASS3_EVENT(9)
    /* Param:
     *   short EVENT_SEMCCREATE, 
     *   int timeStamp, 
     *   int initialCount, 
     *   int options, 
     *   int semId
     */

#define	EVENT_SEMDELETE				CLASS3_EVENT(10)
    /* Param:
     *   short EVENT_SEMDELETE, 
     *   int timeStamp, 
     *   int qHead
     *   int recurse
     *   int state
     *   int semId
     */

#define	EVENT_SEMFLUSH				CLASS3_EVENT(11)
    /* Param:
     *   short EVENT_SEMFLUSH, 
     *   int timeStamp, 
     *   int qHead
     *   int recurse
     *   int state
     *   int semId
     */

#define	EVENT_SEMGIVE				CLASS3_EVENT(12)
    /* Param:
     *   short EVENT_SEMGIVE, 
     *   int timeStamp, 
     *   int recurse, 
     *   int semOwner, 
     *   int semId
     */

#define	EVENT_SEMMCREATE			CLASS3_EVENT(13)
    /* Param:
     *   short EVENT_SEMMCREATE, 
     *   int timeStamp, 
     *   int semOwner, 
     *   int options, 
     *   int semId
     */

#define	EVENT_SEMMGIVEFORCE			CLASS3_EVENT(14)
    /* Param:
     *   short EVENT_SEMMGIVEFORCE, 
     *   int timeStamp, 
     *   int semOwner, 
     *   int options, 
     *   int semId
     */

#define	EVENT_SEMTAKE				CLASS3_EVENT(15)
    /* Param:
     *   short EVENT_SEMTAKE, 
     *   int timeStamp, 
     *   int recurse, 
     *   int semOwner, 
     *   int semId
     */

#define	EVENT_WDCREATE				CLASS3_EVENT(16)
    /* Param:
     *   short EVENT_WDCREATE, 
     *   int timeStamp, 
     *   int wdId
     */

#define	EVENT_WDDELETE				CLASS3_EVENT(17)
    /* Param:
     *   short EVENT_WDDELETE, 
     *   int timeStamp, 
     *   int wdId
     */

#define	EVENT_WDSTART				CLASS3_EVENT(18)
    /* Param:
     *   short EVENT_WDSTART, 
     *   int timeStamp, 
     *   int delay, 
     *   int wdId
     */

#define	EVENT_WDCANCEL				CLASS3_EVENT(19)
    /* Param:
     *   short EVENT_WDCANCEL, 
     *   int timeStamp, 
     *   int wdId
     */

#define	EVENT_MSGQCREATE			CLASS3_EVENT(20)
    /* Param:
     *   short EVENT_MSGQCREATE, 
     *   int timeStamp, 
     *   int options, 
     *   int maxMsgLen, 
     *   int maxMsg, 
     *   int msgId
     */

#define	EVENT_MSGQDELETE			CLASS3_EVENT(21)
    /* Param:
     *   short EVENT_MSGQDELETE, 
     *   int timeStamp, 
     *   int msgId
     */

#define	EVENT_MSGQRECEIVE			CLASS3_EVENT(22)
    /* Param:
     *   short EVENT_MSGQRECEIVE, 
     *   int timeStamp, 
     *   int timeout, 
     *   int bufSize, 
     *   int buffer, 
     *   int msgId
     */

#define	EVENT_MSGQSEND				CLASS3_EVENT(23)
    /* Param:
     *   short EVENT_MSGQSEND, 
     *   int timeStamp, 
     *   int priority, 
     *   int timeout, 
     *   int bufSize, 
     *   int buffer, 
     *   int msgId
     */

#define	EVENT_SIGNAL				CLASS3_EVENT(24)
    /* Param:
     *   short EVENT_SIGNAL, 
     *   int timeStamp, 
     *   int handler, 
     *   int signo
     */

#define	EVENT_SIGSUSPEND			CLASS3_EVENT(25)
    /* Param:
     *   short EVENT_SIGSUSPEND, 
     *   int timeStamp, 
     *   int pSet
     */

#define	EVENT_PAUSE				CLASS3_EVENT(26)
    /* Param:
     *   short EVENT_PAUSE, 
     *   int timeStamp, 
     *   int tid
     */

#define	EVENT_KILL				CLASS3_EVENT(27)
    /* Param:
     *   short EVENT_KILL, 
     *   int timeStamp, 
     *   int tid, 
     *   int signo
     */

#define EVENT_SAFE_PEND                         CLASS3_EVENT(28)
    /* Param:
     *   short EVENT_SAFE_PEND, 
     *   int tid
     */

#define EVENT_SIGWRAPPER                        CLASS3_EVENT(29)
    /* Param:
     *   short EVENT_SIGWRAPPER, 
     *   int signo, 
     *   int tid
     */


#define EVENT_MEMALLOC                          CLASS3_EVENT(30)
    /* Param:
     *   int timeStamp,
     *   unsigned nBytes,
     *   unsigned nBytesPlusHeaderAlign,
     *   int pBlock,
     *   int partId
     */

#define EVENT_MEMFREE                           CLASS3_EVENT(31)
    /* Param:
     *   int timeStamp,
     *   unsigned nBytesPlusHeaderAlign,
     *   int pBlock,
     *   int partId
     */

#define EVENT_MEMPARTCREATE                     CLASS3_EVENT(32)
    /* Param:
     *   int timeStamp,
     *   unsigned poolSize,
     *   int partId
     */

#define EVENT_MEMREALLOC                        CLASS3_EVENT(33)
    /* Param:
     *   int timeStamp,
     *   unsigned nBytes,
     *   int pBlock
     *   unsigned nBytesPlusHeaderAlign,
     *   int pNewBlock,
     *   int partId
     */

#define EVENT_MEMADDTOPOOL                      CLASS3_EVENT(34)
    /* Param:
     *   int timeStamp,
     *   unsigned poolSize,
     *   int partId
     */

#define EVENT_MSGCCREATE                        CLASS3_EVENT(35)
    /* Param:
     */

#define EVENT_MSGCSEND                          CLASS3_EVENT(36)
    /* Param:
     */

#define EVENT_MSGCREPLYGET                      CLASS3_EVENT(37)
    /* Param:
     */

#define EVENT_MSGCDELETE                        CLASS3_EVENT(38)
    /* Param:
     */

#define EVENT_MSGSCREATE                        CLASS3_EVENT(39)
    /* Param:
     */

#define EVENT_MSGSREPLY                         CLASS3_EVENT(40)
    /* Param:
     */

#define EVENT_MSGSRECEIVE                       CLASS3_EVENT(41)
    /* Param:
     */

#define EVENT_MSGSDELETE                        CLASS3_EVENT(42)
    /* Param:
     */

#define	EVENT_TASKSTOP				CLASS3_EVENT(43)
   /* Param:
     *   short EVENT_TASKSTOP, 
     *   int timeStamp, 
     *   int pTcb
     */

#define	EVENT_TASKCONT				CLASS3_EVENT(44)
    /* Param:
     *   short EVENT_TASKCONT, 
     *   int timeStamp, 
     *   int priority, 
     *   int pTcb
     */

#define EVENT_IOOPEN				CLASS3_EVENT(48)
    /* Param:
     *	 short EVENT_IOOPEN,
     *   int timeStamp,
     *   int flags,
     *   int owner,
     *	 int fd,
     *   int strSize,
     *   char * name
     */
 
#define EVENT_IOREAD				CLASS3_EVENT(49)
    /* Param:
     *	 short EVENT_IOREAD,
     *   int timeStamp,
     *   int buffer,
     *   int bytesRead,
     *	 int fd,
     */
 
#define EVENT_IOWRITE				CLASS3_EVENT(50)
    /* Param:
     *	 short EVENT_IOWRITE,
     *   int timeStamp,
     *   int buffer,
     *   int bytesWritten,
     *	 int fd,
     */
 
#define EVENT_IOIOCTL				CLASS3_EVENT(51)
    /* Param:
     *	 short EVENT_IOIOCTL,
     *   int timeStamp,
     *   int function,
     *   int arg,
     *	 int fd,
     */
 
#define EVENT_IOCLOSE				CLASS3_EVENT(52)
    /* Param:
     *	 short EVENT_IOCLOSE,
     *   int timeStamp,
     *	 int fd
     */
 
#define EVENT_IOCREAT				CLASS3_EVENT(53)
    /* Param:
     *	 short EVENT_IOCREAT,
     *   int timeStamp,
     *   int flag,
     *   int owner,
     *	 int fd,
     *   int strSize,
     *   char * name
     */

#define EVENT_IODELETE				CLASS3_EVENT(54)
    /* Param:
     *	 short EVENT_IODELETE,
     *   int timeStamp,
     *   int strSize
     *	 char * name
     */
    
#define EVENT_EVENTSEND				CLASS3_EVENT(57)
    /* Param:
     *   int taskId,
     *   UINT32 events
     */

#define EVENT_EVENTRECEIVE			CLASS3_EVENT(58)
    /* Param:
     *   UINT32 events,
     *   int timeout,
     *   UINT8 flags
     */

#define EVENT_OWNERSET				CLASS3_EVENT(59)
    /* Param:
     *   short EVENT_OWNERSET, 
     *   int timeStamp, 
     *   int ownerId, 
     *   int objId
     */

#define EVENT_ISR_DISPATCHER_INVOKE             CLASS3_EVENT(60)
    /* Param:
     *   short EVENT_ISR_DISPATCHER_INVOKE, 
     *   int timeStamp, 
     *   int objId
     */
    
#define EVENT_ISR_DISPATCHER_DONE               CLASS3_EVENT(61)
    /* Param:
     *   short EVENT_ISR_DISPATCHER_DONE, 
     *   int timeStamp, 
     *   int objId
     */

#define EVENT_ISR_CREATE                        CLASS3_EVENT(62)
    /* Param:
     *   short EVENT_ISR_CREATE,
     *   int timestamp
     *   int objId      
     *   tag,
     *   rtn,
     *   param,
     *   options
     */
    
    
#define EVENT_ISR_DESTROY                       CLASS3_EVENT(64)
    /* Param:
     *   short EVENT_ISR_DESTROY, 
     *   int timeStamp, 
     *   int objId
     */

#define EVENT_SYSCALL_0                         CLASS3_EVENT(65)
    /* Params:
     *   short EVENT_SYSCALL_0
     *   int timestamp
     *   short syscallNumber
     */
    
#define EVENT_SYSCALL_1                         CLASS3_EVENT(66)
    /* Params:
     *   short EVENT_SYSCALL_1
     *   int timestamp
     *   short syscallNumber
     *   int param
     */
    
#define EVENT_SYSCALL_2                         CLASS3_EVENT(67)
    /* Params:
     *   short EVENT_SYSCALL_2
     *   int timestamp
     *   short syscallNumber
     *   int param
     *   int param
     */
    
#define EVENT_SYSCALL_3                         CLASS3_EVENT(68)
    /* Params:
     *   short EVENT_SYSCALL_3
     *   int timestamp
     *   short syscallNumber
     *   int param
     *   int param
     *   int param
     */
    
#define EVENT_SYSCALL_4                         CLASS3_EVENT(69)
    /* Params:
     *   short EVENT_SYSCALL_4
     *   int timestamp
     *   short syscallNumber
     *   int param
     *   int param
     *   int param
     *   int param
     */
    
#define EVENT_SYSCALL_5                         CLASS3_EVENT(70)
    /* Params:
     *   short EVENT_SYSCALL_5
     *   int timestamp
     *   short syscallNumber
     *   int param
     *   int param
     *   int param
     *   int param
     *   int param
     */
    
#define EVENT_SYSCALL_6                         CLASS3_EVENT(71)
    /* Params:
     *   short EVENT_SYSCALL_6
     *   int timestamp
     *   short syscallNumber
     *   int param
     *   int param
     *   int param
     *   int param
     *   int param
     *   int param
     */
    
#define EVENT_SYSCALL_7                         CLASS3_EVENT(72)
    /* Params:
     *   short EVENT_SYSCALL_7
     *   int timestamp
     *   short syscallNumber
     *   int param
     *   int param
     *   int param
     *   int param
     *   int param
     *   int param
     *   int param
     */
    
#define EVENT_SYSCALL_8                         CLASS3_EVENT(73)
    /* Params:
     *   short EVENT_SYSCALL_8
     *   int timestamp
     *   short syscallNumber
     *   int param
     *   int param
     *   int param
     *   int param
     *   int param
     *   int param
     *   int param
     *   int param
     */
    
#define EVENT_SYSCALL_RETURN_OK                 CLASS3_EVENT(74)
    /* Params:
     *   short EVENT_SYSCALL_OK
     *   int timestamp
     *   int returnValue
     */
    
#define EVENT_SYSCALL_RETURN_ERR                CLASS3_EVENT(75)
    /* Params:
     *   short EVENT_SYSCALL_ERR
     *   int timestamp
     *   int errno
     */
    
#define EVENT_SYSCALL_BAD_ID                    CLASS3_EVENT(76)
    /* Params:
     *   short EVENT_SYSCALL_BAD_ID
     *   int timestamp
     *   int syscallGroup
     *   int syscallRoutine
     */
    
#define EVENT_OBJ_VERIFY_FAIL                   CLASS3_EVENT(77)
    /* Params:
     *   short EVENT_OBJ_VERIFY_FAIL
     *   int timestamp
     *   int objId
     *   int classId
     */

    
#define EVENT_RTP_SPAWN                         CLASS3_EVENT(78)
    /* Params:
     *   short EVENT_RTP_SPAWN
     *   int timestamp
     *   int rtpId
     *   int priority
     *   int stacksize
     *   int options
     */

#define EVENT_RTP_DELETE                        CLASS3_EVENT(79)
    /* Params:
     *   short EVENT_RTP_DELETE
     *   int timestamp
     *   int rtpId
     */

#define EVENT_RTP_INIT_TASK                     CLASS3_EVENT(80)
    /* Params:
     *   short EVENT_RTP_SPAWN
     *   int timestamp
     */

#define EVENT_RTP_LOAD                          CLASS3_EVENT(81)
    /* Params:
     *   short EVENT_RTP_SPAWN
     *   int timestamp
     */

#define EVENT_EDR_ERROR_INJECT                  CLASS3_EVENT(82)
    /* Param:
     *   UINT32 kind,
     *   int    len (fileName)
     *   char * fileName,
     *   UINT32 lineNumber,
     *   UINT32 address,
     *   int    len (textPayload)
     *   char * textPayload,
     */

#define EVENT_RTPKILL                           CLASS3_EVENT(83)
    /* Param:
     *  short   eventId
     *  int     timestamp
     *  int     rtpId
     *  int     signal
     */
    
#define EVENT_RTPSIGNAL                         CLASS3_EVENT(84)
    /* Param:
     *  short   eventId
     *  int     timestamp
     *  int     rtpId
     *  int     signal
     *  int     handler
     */
    
#define EVENT_RTPSUSPEND                        CLASS3_EVENT(85)
    /* Param:
     *  short   eventId
     *  int     timestamp
     *  int     signalSet
     */
    
#define EVENT_RTPPAUSE                          CLASS3_EVENT(86)
    /* Param:
     *  short   eventId
     *  int     timestamp
     */
    
#define EVENT_RTPTASKKILL                       CLASS3_EVENT(87)
    /* Param:
     *  short   eventId
     *  int     timestamp
     *  int     taskId
     *  int     signal
     */
    
#define EVENT_RTPSIGQUEUE                       CLASS3_EVENT(88)
    /* Param:
     *  short   eventId
     *  int     timestamp
     *  int     rtpId
     *  int     signal
     *  int     pValue
     */
    
#define EVENT_RTPTASKSIGQUEUE                   CLASS3_EVENT(89)
    /* Param:
     *  short   eventId
     *  int     timestamp
     *  int     taskId
     *  int     signal
     *  int     pValue
     */
    
#define EVENT_RTPCHILDWAIT                      CLASS3_EVENT(90)
    /* Param:
     *  short   eventId
     *  int     timestamp
     *  int     rtpI
     *  int     options
     */
    

#define EVENT_RTPSIGTIMEDWAIT                   CLASS3_EVENT(91)
    /* Param:
     *  short   eventId
     *  int     timestamp
     *  int     rtpId
     *  int     pSet
     *  int     pInfo
     *  int     wait
     */
    
#define EVENT_RTPSIGWRAPPER                     CLASS3_EVENT(92)
    /* Param:
     *  short   eventId
     *  int     timestamp
     *  int     rtpId
     *  int     type (SIG_DELIVERED_IN_SYSCALL
     *                SIG_DELIVERED_IN_WINDCTX
     *                SIG_DELIVERED_IN_EXCEPTION)
     */

#define EVENT_RTPSIGWRAPPER_DONE                CLASS3_EVENT(93)
    /* Param:
     *  short   eventId
     *  int     timestamp
     */

#define EVENT_RTPSIGPROCMASK                    CLASS3_EVENT(94)
    /* Params:
     *   short EVENT_RTP_SPAWN
     *   int timestamp
     *   int how
     *   int pSet
     *   int pOSet
     */

#define EVENT_RTPSIGPENDING                     CLASS3_EVENT(95)
    /* Params:
     *   short EVENT_RTP_SPAWN
     *   int timestamp
     *   int pSet
     */

#define EVENT_SALOPEN                           CLASS3_EVENT(96)
    /* Param:
     *   short  EVENT_SALOPEN
     *   char * name
     *   int    timestamp
     *   int    sockfd 
     */

#define EVENT_SALCALL_SENDREQ                   CLASS3_EVENT(97)
    /* Param:
     *   short  EVENT_SENDREQ
     *   int    timestamp
     *   int    sockfd
     *   int    sendBuf   
     *   int    len   
     */

#define EVENT_SALCALL_REPLY                     CLASS3_EVENT(98)
    /* Param:
     *   short  EVENT_REPLY
     *   int    timestamp
     *   int    sockfd
     *   int    recvBuf   
     *   int    len   
     */
    
#define EVENT_SALCREATE                         CLASS3_EVENT(99)
    /* Param:
     *   short  EVENT_SALCREATE
     *   int    timestamp
     *   char * name
     *   int    serverId
     */

#define EVENT_SALDELETE                         CLASS3_EVENT(100)
    /* Param:
     *   short  EVENT_SALDELETE
     *   int    timestamp
     *   int    serverId
     */

#define EVENT_SALRUN_WAIT                       CLASS3_EVENT(101)
    /* Param:
     *   short  EVENT_SALRUN_WAIT
     *   int    timestamp
     *   int    serverId
     */

#define EVENT_SALRUN_READREQ                    CLASS3_EVENT(102)
    /* Param:
     *   short  EVENT_SALRUN_READREQ
     *   int    timestamp
     *   int    serverId
     *   int    sockfd
     */

#define EVENT_SALRUN_SOCKCLOSE                  CLASS3_EVENT(103)
    /* Param:
     *   short  EVENT_SALRUN_SOCKCLOSE
     *   int    timestamp
     *   int    serverId
     *   int    sockfd
     */

#define EVENT_SALRUN_SOCKIGNORE                 CLASS3_EVENT(104)
    /* Param:
     *   short  EVENT_SALRUN_SOCKIGNORE
     *   int    timestamp
     *   int    serverId
     *   int    sockfd
     */

#define EVENT_SALRUN_ACCEPT                     CLASS3_EVENT(105)
    /* Param:
     *   short  EVENT_SALRUN_ACCEPT
     *   int    timestamp
     *   int    serverId
     *   int    sockfd
     */

#define EVENT_SALRUN_CONNECT                    CLASS3_EVENT(106)
    /* Param:
     * short    EVENT_SALRUN_CONNECT
     * int      serverId
     * int      sockfd
     */

#define EVENT_SALRUN_TERMINATE                  CLASS3_EVENT(107)
    /* Param:
     *   short  EVENT_SALRUN_TERMINATE
     *   int    timestamp
     *   int    serverId
     */

/*
 * USB Event Ids
 * These are CLASS3 events, from 120 - 149 for peripheral,
 * and 150 - 199 for host
 */

#define EVENT_SD_CREATE                         CLASS3_EVENT(200)
    /*
     *  short   EVENT_SD_CREATE
     *  int     timestamp
     *  int     sdId
     *  int     options
     *  int     size
     *  int     physical address,
     *  int     attr
     *  int     virtual address.
     *  string  name
     */
    
#define EVENT_SD_DELETE                         CLASS3_EVENT(201)
    /*
     *  short   EVENT_SD_DELETE
     *  int     timestamp
     *  int     sdId
     *  int     options
     */
    
#define EVENT_SD_MAP                            CLASS3_EVENT(202)
    /*
     *  short   EVENT_SD_MAP
     *  int     timestamp
     *  int     sdId,
     *  int     attr
     *  int     options
     */
    
#define EVENT_SD_UNMAP                          CLASS3_EVENT(203)
    /*
     *  short   EVENT_SD_UNMAP
     *  int     timestamp
     *  int     sdId,
     *  int     options
     */

/* CLASS2 events */

#define	EVENT_WINDSPAWN				CLASS2_EVENT(0)
    /* Param:
     *   short EVENT_WINDSPAWN, 
     *   int pTcb, 
     *   int priority
     */

#define	EVENT_WINDDELETE			CLASS2_EVENT(1)
    /* Param:
     *   short EVENT_WINDDELETE, 
     *   int pTcb
     */

#define	EVENT_WINDSUSPEND			CLASS2_EVENT(2)
    /* Param:
     *   short EVENT_WINDSUSPEND, 
     *   int pTcb
     */

#define	EVENT_WINDRESUME			CLASS2_EVENT(3)
    /* Param:
     *   short EVENT_WINDRESUME, 
     *   int pTcb
     */

#define	EVENT_WINDPRIORITYSETRAISE		CLASS2_EVENT(4)
    /* Param:
     *   short EVENT_WINDPRIORITYSETRAISE, 
     *   int pTcb, 
     *   int oldPriority, 
     *   int priority
     */


#define	EVENT_WINDPRIORITYSETLOWER		CLASS2_EVENT(5)
    /* Param:
     *   short EVENT_WINDPRIORITYSETLOWER, 
     *   int pTcb, 
     *   int oldPriority, 
     *   int priority
     */
    
#define	EVENT_WINDSEMDELETE			CLASS2_EVENT(6)
    /* Param:
     *   short EVENT_WINDSEMDELETE, 
     *   int semId
     */


#define	EVENT_WINDTICKANNOUNCETMRSLC		CLASS2_EVENT(7)
    /* Param:
     *   short EVENT_WINDTICKANNOUNCETMRSLC
     */

#define	EVENT_WINDTICKANNOUNCETMRWD		CLASS2_EVENT(8)
    /* Param:
     *   short EVENT_WINDTICKANNOUNCETMRWD, 
     *   int wdId
     */

#define	EVENT_WINDDELAY				CLASS2_EVENT(9)
    /* Param:
     *   short EVENT_WINDDELAY
     *   int ticks
     */

#define	EVENT_WINDUNDELAY			CLASS2_EVENT(10)
    /* Param:
     *   short EVENT_WINDUNDELAY, 
     *   int pTcb
     */
    
#define	EVENT_WINDWDSTART			CLASS2_EVENT(11)
    /* Param:
     *   short EVENT_WINDWDSTART, 
     *   int wdId
     */

#define	EVENT_WINDWDCANCEL			CLASS2_EVENT(12)
    /* Param:
     *   short EVENT_WINDWDCANCEL, 
     *   int wdId
     */

#define	EVENT_WINDPENDQGET			CLASS2_EVENT(13)
    /* Param:
     *   short EVENT_WINDPENDQGET, 
     *   int pTcb
     */

#define	EVENT_WINDPENDQFLUSH			CLASS2_EVENT(14)
    /* Param:
     *   short EVENT_WINDPENDQFLUSH, 
     *   int pTcb
     */

#define	EVENT_WINDPENDQPUT			CLASS2_EVENT(15)
    /* Param:
     *   short EVENT_WINDPENDQPUT
     */

#define	EVENT_WINDPENDQTERMINATE		CLASS2_EVENT(17)
    /* Param:
     *   short EVENT_WINDPENDQTERMINATE, 
     *   int pTcb
     */

#define	EVENT_WINDTICKUNDELAY			CLASS2_EVENT(18)
    /* Param:
     *   short EVENT_WINDTICKUNDELAY, 
     *   int pTcb
     */

#define EVENT_OBJ_TASK				CLASS2_EVENT(19)
    /* Param:
     *   short EVENT_OBJ_TASK, 
     *   int pTcb
     */

#define EVENT_OBJ_SEMGIVE			CLASS2_EVENT(20)
    /* Param:
     *   short EVENT_OBJ_SEMGIVE, 
     *   int semId
     */

#define EVENT_OBJ_SEMTAKE			CLASS2_EVENT(21)
    /* Param:
     *   short EVENT_OBJ_SEMTAKE, 
     *   int semId
     */

#define EVENT_OBJ_SEMFLUSH			CLASS2_EVENT(22)
    /* Param:
     *   short EVENT_OBJ_SEMFLUSH, 
     *   int semId
     */

#define EVENT_OBJ_MSGSEND			CLASS2_EVENT(23)
    /* Param:
     *   short EVENT_OBJ_MSGSEND, 
     *   int msgQId
     */

#define EVENT_OBJ_MSGRECEIVE			CLASS2_EVENT(24)
    /* Param:
     *   short EVENT_OBJ_MSGRECEIVE, 
     *   int msgQId
     */

#define EVENT_OBJ_MSGDELETE			CLASS2_EVENT(25)
    /* Param:
     *   short EVENT_OBJ_MSGDELETE, 
     *   int msgQId
     */

#define EVENT_OBJ_SIGPAUSE			CLASS2_EVENT(26)
    /* Param:
     *   short EVENT_OBJ_SIGPAUSE, 
     *   int qHead
     */

#define EVENT_OBJ_SIGSUSPEND			CLASS2_EVENT(27)
    /* Param:
     *   short EVENT_OBJ_SIGSUSPEND, 
     *   int sigset
     */

#define EVENT_OBJ_SIGKILL			CLASS2_EVENT(28)
    /* Param:
     *   short EVENT_OBJ_SIGKILL, 
     *   int tid
     */

#define	EVENT_WINDSTOP				CLASS2_EVENT(29)
    /* Param:
     *   short EVENT_WINDSTOP, 
     *   int pTcb
     */

#define	EVENT_WINDCONT				CLASS2_EVENT(30)
    /* Param:
     *   short EVENT_WINDCONT, 
     *   int pTcb
     */

#define EVENT_WINDTICKTIMEOUT                   CLASS2_EVENT(31)
    /* Param:
     *   short EVENT_WINDTICKTIMEOUT,
     *   int tId
     */

#define EVENT_OBJ_SIGWAIT                       CLASS2_EVENT(32)
    /* Param:
     *   short EVENT_OBJ_SIGWAIT, 
     *   int tid
     */

#define	EVENT_OBJ_EVENTSEND			CLASS2_EVENT(35)
    /* Param:
     *   short EVENT_OBJ_EVENTSEND 
     */

#define	EVENT_OBJ_EVENTRECEIVE			CLASS2_EVENT(36)
    /* Param:
     *   short EVENT_OBJ_EVENTRECEIVE
     */

#define EVENT_OBJ_MSGCDELETE                    CLASS2_EVENT(37)
    /* Param:
     *   short EVENT_OBJ_MSGCDELETE
     */

#define EVENT_OBJ_MSGCSEND                      CLASS2_EVENT(38)
    /* Param:
     *   short EVENT_OBJ_MSGCSEND
     */

#define EVENT_OBJ_MSGCREPLYGET                  CLASS2_EVENT(39)
    /* Param:
     *   short EVENT_OBJ_MSGCREPLYGET
     */

#define EVENT_OBJ_MSGCACKSEND                   CLASS2_EVENT(40)
    /* Param:
     *   short EVENT_OBJ_MSGCACKSEND
     */

#define EVENT_OBJ_MSGCREPLYSEND                 CLASS2_EVENT(41)
    /* Param:
     *   short EVENT_OBJ_MSGCREPLYSEND
     */

#define EVENT_OBJ_MSGSDELETE                    CLASS2_EVENT(42)
    /* Param:
     *   short EVENT_OBJ_MSGSDELETE
     */

#define EVENT_OBJ_MSGSRECEIVE                   CLASS2_EVENT(43)
    /* Param:
     *   short EVENT_OBJ_MSGSRECEIVE
     */

#define EVENT_OBJ_MSGSREPLY                     CLASS2_EVENT(44)
    /* Param:
     *   short EVENT_OBJ_MSGSREPLY
     */

#define EVENT_OBJ_MSGSSEND                      CLASS2_EVENT(45)
    /* Param:
     *   short EVENT_OBJ_MSGSSEND
     */


/* CLASS1 events */

#define EVENT_WIND_EXIT_DISPATCH        	CLASS1_EVENT(2)
    /* Param:
     *   short EVENT_WIND_EXIT_DISPATCH, 
     *   int timestamp,
     *   int tId,
     *   int priority
     */

#define EVENT_WIND_EXIT_NODISPATCH        	CLASS1_EVENT(3)
    /* Param:
     *   short EVENT_WIND_EXIT_NODISPATCH, 
     *   int timestamp,
     *   int priority
     */

#define EVENT_WIND_EXIT_DISPATCH_PI        	CLASS1_EVENT(4)
    /* Param:
     *   short EVENT_WIND_EXIT_DISPATCH_PI, 
     *   int timestamp,
     *   int tId,
     *   int priority
     */

#define EVENT_WIND_EXIT_NODISPATCH_PI        	CLASS1_EVENT(5)
    /* Param:
     *   short EVENT_WIND_EXIT_NODISPATCH_PI, 
     *   int timestamp,
     *   int priority
     */

    /*
      #define EVENT_DISPATCH_OFFSET   0xe
      #define EVENT_NODISPATCH_OFFSET   0xa
    */
    
    /*
     * This definition is needed for the logging of the above _PI events
     */

#define	EVENT_WIND_EXIT_IDLE			CLASS1_EVENT(6)
    /* Param:
     *   short EVENT_WIND_EXIT_IDLE, 
     *   int timestamp
     */

#define	EVENT_TASKLOCK				CLASS1_EVENT(7)
    /* Param:
     *   short EVENT_TASKLOCK, 
     *   int timeStamp
     */

#define	EVENT_TASKUNLOCK			CLASS1_EVENT(8)
    /* Param:
     *   short EVENT_TASKUNLOCK, 
     *   int timeStamp
     */

#define	EVENT_TICKANNOUNCE			CLASS1_EVENT(9)
    /* Param:
     *   short EVENT_TICKANNOUNCE, 
     *   int timeStamp
     */

#define	EVENT_EXCEPTION				CLASS1_EVENT(10)
    /* Param:
     *   short EVENT_EXCEPTION, 
     *   int timeStamp, 
     *   int exception
     */

#define EVENT_TASK_STATECHANGE                  CLASS1_EVENT(11)
    /* Param:
     *   short EVENT_STATECHANGE
     *   int timeStamp,
     *   int taskId,
     *   int oldState
     *   int newState
     */

/* pseudo events - generated host side */



#ifdef CPU   /* This is for target side build only */

#ifndef _BYTE_ORDER
#error _BYTE_ORDER must be defined
#endif /* _BYTE_ORDER */

#ifndef _BIG_ENDIAN
#error _BIG_ENDIAN must be defined
#endif /* _BIG_ENDIAN */

/*
 * Alignment macros used to store unaligned short (16 bits) and unaligned
 * int (32 bits).
 */

/*
 * If _WRS_BLIB_ALIGNMENT is defined as 1, arch can do non-aligned word writes,
 * otherwise we need to handle it ourselves.
 */
      
#if defined (_WRS_BLIB_ALIGNMENT)
    #if (_WRS_BLIB_ALIGNMENT == 1 || _WRS_BLIB_ALIGNMENT == 0)
        #define WV_UNALIGNED_SUPPORT
    #endif
#endif      /* defined (_WRS_BLIB_ALIGNMENT) */

#define EVT_STORE_UINT16(pBuf, event_id) \
    *pBuf++ = (event_id)

#if (_BYTE_ORDER==_BIG_ENDIAN)

#   if defined  WV_UNALIGNED_SUPPORT
    	/* unaligned access supported */
#   	define EVT_STORE_UINT32(pBuf, value)                    \
    		*pBuf++ = (value)
#   else
	/* unaligned access not supported */
#	define EVT_STORE_UINT32(pBuf, value)                    \
    		do { *((short *) pBuf) = (value) >> 16;         \
	 	     *(((short *) pBuf) + 1) = (value);         \
	 	     pBuf++; } while (0)
#   endif /* WV_UNALIGNED_SUPPORT */

#else	/* (_BYTE_ORDER==_BIG_ENDIAN) */

#   if defined  WV_UNALIGNED_SUPPORT
    	/* unaligned access supported */
#   	define EVT_STORE_UINT32(pBuf, value)                    \
    		*pBuf++ = (value)
#   else
	/* unaligned access not supported */
#       define EVT_STORE_UINT32(pBuf, value)                    \
    		do { *((short *) pBuf) = (value);               \
	 	     *(((short *) pBuf) + 1) = (value) >> 16;   \
	 	     pBuf++; } while (0)
#   endif /* WV_UNALIGNED_SUPPORT */

#endif	/* (_BYTE_ORDER==_BIG_ENDIAN) */

#endif /* CPU */

#ifdef __cplusplus
}
#endif

#endif /* __INCeventph*/



