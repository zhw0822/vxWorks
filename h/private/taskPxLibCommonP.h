/* taskPxLibCommonP.h - common header for POSIX support in VxWorks task */
 
/*
 * Copyright (c) 2005 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */
 
/*
modification history
--------------------
01b,09jul05,jln  removed VX_POSIX_INSERT_HEAD_CHECK
01a,26jun05,jln  created.
*/
 
#ifndef __INCtaskPxLibCommonPh
#define __INCtaskPxLibCommonPh

/* includes */

#if defined(__cplusplus)
extern "C" {
#endif	/* __cplusplus */

#include <vwModNum.h>

/* general status code */

#define S_taskPxLib_ILLEGAL_PRIORITY		(M_taskPxLib | 101)
#define S_taskPxLib_ILLEGAL_POLICY		(M_taskPxLib | 102)

/* POSIX "schedInfo" definition for VxWorks task */

#define VX_POSIX_SCHED_FIFO			0x0001
#define VX_POSIX_SCHED_RR			0x0002
#define VX_POSIX_SCHED_OTHER			0x0004

#define VX_POSIX_INSERT_HEAD                    0x80000000

#define VX_POSIX_SCHED_MASK			(VX_POSIX_SCHED_FIFO | \
						 VX_POSIX_SCHED_RR   | \
						 VX_POSIX_SCHED_OTHER)

/* flag: bit def for "flag" in TASK_PX_ATTR passed to VxWorks kernel */

#define VX_POSIX_FLAG_SETSCHEDPRIO	0x0001	/* pthread_setschedprio() */
#define VX_POSIX_FLAG_PRIORITY          0x0002  /* set/get priority       */
#define VX_POSIX_FLAG_POLICY            0x0004  /* set/get policy         */

/* typedef */

/* Don't change the order of this struct; only append new members if needed */
  
typedef struct task_px_attr 
     {
     UINT	flag;		/* bits to specify which attr to set/get */
     int        priority;	/* priority attribute */
     int        policy;		/* policy attribute */
     } TASK_PX_ATTR;

#if defined(__cplusplus)
}
#endif	/* __cplusplus */

#endif /* __INCtaskPxLibCommonPh */

