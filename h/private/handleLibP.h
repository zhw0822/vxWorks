/* handleLibP.h - handle management library header */

/* Copyright 1998-2003 Wind River Systems, Inc. */

/*
modification history
--------------------
01d,02mar04,dcc  modified attributes to be 16 bits wide.
01c,02nov03,dcc  modified HANDLE to comply with ANSI standards.
01b,02jul03,to   added offsets into HANDLE.
01a,23jun03,dcc  ported from AE1.1
*/

#ifndef __INChandleLibPh
#define __INChandleLibPh

#ifdef __cplusplus
extern "C" {
#endif

#include "vxWorks.h"

#ifndef _ASMLANGUAGE

/* typedefs */

enum handleContextType
    {
    handleContextTypeNone   = 0,
    handleContextTypeUser,
    handleContextTypeOms,
    handleContextTypeCOM,
    handleContextTypeI2O
    };
					 
typedef struct
    {
    void *    context;        /* 0x00: WRS defined context */
    UINT32    magic;	      /* 0x04: magic. Used in HANDLE_VERIFY() */
    UINT16    attributes;     /* 0x08: attribute bit set */
    INT8      type;           /* 0x0a: enum windObjClassType */
    UINT8     contextType;    /* 0x0b: enum handleContextType */
    } HANDLE;

typedef HANDLE * HANDLE_ID;		/* handle id */

/* function declarations */

extern STATUS   handleContextGet        (HANDLE_ID handleId, void **pContext,
					 enum handleContextType *pContextType);
extern void	handleContextGetBase	(HANDLE_ID handleId, void **pContext,
					 enum handleContextType *pContextType);
extern STATUS   handleContextSet        (HANDLE_ID handleId, void *context,
					 enum handleContextType contextType);
extern STATUS	handleInit	 	(HANDLE_ID handleId, enum handleType);
extern STATUS	handleVerify	 	(HANDLE_ID handleId, enum handleType);
extern STATUS	handleTerminate  	(HANDLE_ID handleId);
extern STATUS	handleShow	 	(HANDLE_ID handleId, int showType);
extern STATUS	handleShowConnect	(enum handleType, FUNCPTR showRtn);
extern enum handleType handleTypeGet	(HANDLE_ID handleId);

/* macro declarations */

#define HANDLE_VERIFY(handle,handleType)				\
    (									\
        (								\
	    (((HANDLE_ID) (handle))->magic == (unsigned)(handle))	\
	&&								\
	    (((HANDLE_ID) (handle))->type == (handleType))		\
	)								\
    ? 									\
	OK								\
    :									\
	ERROR								\
    )

#else /* _ASMLANGUAGE */

/* offsets into HANDLE */

#define HANDLE_MAGIC	0x4
#define HANDLE_TYPE	0xa

#endif /* _ASMLANGUAGE */

#ifdef __cplusplus
}
#endif

#endif /* __INChandleLibPh */
