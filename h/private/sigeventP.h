/* sigeventP.h - sigeventLib header files */

/* Copyright 2003 Wind River Systems, Inc. */

/*
modification history
--------------------
01a,25sep03,ans  written
*/

#ifndef __INCsigeventph
#define __INCsigeventph

#ifdef __cplusplus
extern "C" {
#endif

struct sigevent_t
    {
    int             notifierId;  /* task or RTP Id to notify */
    struct sigevent sigEvent;    /* sigevent descibing the notification */
    union 
        {
        struct sigpend _timerSigPend;
        } notifier;
#define timerSigPend notifier._timerSigPend
  
    };



#ifdef __cplusplus
}
#endif

#endif /* __INCsigeventph */
