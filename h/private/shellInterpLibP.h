/* shellInterpLibP.h - private header file for the interpreters management */

/* Copyright 2003-2005 Wind River Systems, Inc. */

/*
modification history
--------------------
01f,08feb05,bpn  Added shellInterpPromptDftSet() prototype, changed
                 shellInterpPromptSet() (SPR#106114).
01e,28jul04,bpn  Removed shellInterpNameFromCtxGet() and shellInterpCtxCreate()
                 prototypes.
01d,26feb04,bpn  Added shellInterpRestart() and shellInterpEventDispatch()
                 prototypes.
01c,21jan04,bpn  Added shellInterpDelete() prototype.
01b,25jun03,bpn  Added shellInterpHistorySave() prototype.
01a,17feb03,bpn  Written.
*/

#ifndef __INCshellInterpLibPh
#define __INCshellInterpLibPh

#ifdef __cplusplus
extern "C" {
#endif

/* Includes */

#include <shellLib.h>
#include <shellInterpLib.h>
#include <private/shellLibP.h>

/* Function declarations */

extern STATUS		shellInterpSet (SHELL_CTX * pShellCtx,
					const char * interpreterName);
extern const char *	shellInterpPromptGet (SHELL_INTERP_CTX * pInterpCtx);
extern STATUS		shellInterpPromptSet (SHELL_ID shellId,
					      const char * interp,
					      const char * promptFmt);
extern STATUS		shellInterpPromptDftSet (const char * interp,
						 const char * promptFmt);

extern STATUS		shellInterpSwitch (SHELL_CTX * pShellCtx);

extern void		shellInterpDelete (SHELL_CTX * pShellCtx);
extern void		shellInterpRestart (SHELL_CTX * pShellCtx);

#ifdef HOST
extern void		shellInterpHistorySave (SHELL_CTX * pShellCtx);
extern STATUS		shellInterpEventDispatch (const char * evt);
#endif

#ifdef __cplusplus
}
#endif

#endif /* __INCshellInterpLibPh */
