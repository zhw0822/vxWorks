/* wvLibP.h - WindView lib private header */

/* Copyright 1997-2003 Wind River Systems, Inc. */

/*
modification history
--------------------
01h,12mar04,tcr  postMortem features
01g,09oct03,tcr  tidy up
01f,17jul03,tcr  make hash table more generic
01e,20apr98,cth  changed WV_LOG_HEADER def, reverted uploadTaskPriority to 01b
01d,17apr98,cth  added type WV_LOG_HEADER, removed prototype of wvUpload
01c,15apr98,cth  removed wvUploadTaskPriority, added wvUploadTaskPriorityCont
		 and wvUploadPathPriorityOnce
01b,18dec97,cth  updated includes
01a,16nov97,cth  created, derived from wvLib.h
*/

#ifndef __INCwvlibph
#define __INCwvlibph

#include "wvLib.h"
#include "sllLib.h"

#include "private/wvUploadPathP.h"
#include "private/wvBufferP.h"

#ifdef __cplusplus
extern "C" {
#endif


/* globals */

extern int      wvUploadTaskPriority;	   /* upload task priority */
extern int      wvUploadTaskStackSize;     /* upload task stack size (bytes) */
extern int      wvUploadTaskOptions;       /* upload task options */

/* defines */
 
#define WV_LOG_LIST_MAGIC               0x57764f6b
    
/* types */

typedef struct wvHashNode
    {
    char *              pEvent;         /* event data */
    int                 eventSize;      /* size of event data */
    int                 key;            /* hash key */
    struct wvHashNode * next;           /* next node */
    } WV_HASH_NODE;
    



typedef struct tnHashTable
    {
    int                 size;
    WV_HASH_NODE **     tbl;		/* *tbl[] */
    } WV_HASH_TBL;

typedef struct wvIterKey
    {
    int 	   index;
    WV_HASH_NODE * pCur;
    } WV_ITER_KEY;



/*
 * This struct is the root of the WindView logs on the target
 */

typedef struct wvLogList
    {
    struct
        {
        SL_LIST     wvLogListHead;          /* List of wind View logs */
        PART_ID     memPart;                /* Partition logs are created in */
        int         numLogs;                /* Maximum number of logs in the list */
        UINT32      magic;                  /* Should be WV_LOG_LIST_MAGIC */
        } hdr;
    UINT32      checksum;
    } WV_LOG_LIST;


/*
 * This struct describes a single WindView log.
 */

typedef struct wvLog
    {
    SL_NODE             node;           /* list node */
    PART_ID             memPart;        /* mem partition log created in */
    BUFFER_ID           pEvtBuffer;     /* associated event buffer */
    char *              pConfigEvent;   /* config event, with log revision */
    int                 configEventSize;
    WV_HASH_TBL *       pHashTbl;       /* hast table tasknames, rtp etc. */
    } WV_LOG;
    

typedef struct wvUploadTaskDescriptor
    {
    int         uploadTaskId;           /* task id of this upload task */
    SEMAPHORE   uploadCompleteSem;      /* given iff buffers are emptied */
    BOOL        exitWhenEmpty;          /* if true task exits when buf empty */
    UPLOAD_ID	uploadPathId;		/* id of the upload path to host */
    WV_LOG *	pWvLog;		        /* id of the log to upload from */
    int		status;			/* indicate errors in up task */
    } WV_UPLOADTASK_DESC;


/* function declarations */

extern STATUS wvLogListValidate (WV_LOG_LIST * pLogList);

extern STATUS wvObjNameBuffAdd (int key, short eventId, int nArgs, int arg1,
                                int arg2, int arg3, int arg4, int arg5,
                                int arg6, char * name );
    
#ifdef __cplusplus
}
#endif

#endif /* __INCwvlibph*/
