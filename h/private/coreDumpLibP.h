/* coreDumpLibP.h - core dump private interface header */

/*
 * Copyright (c) 2005 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */
 
/*
modification history
--------------------
01m,13sep05,dbt  Fixed pMemWriteFilter() prototype.
01l,07sep05,jmp  Performed some updates required for the coreDumpLib.c split.
		 Added support for core dump alternate stack switch.
		 Provide the ability to install core dump filters before
		 usrCoreDumpInit() call.
01k,01sep05,dbt  Updated TGT_INFO structure.
		 Added coreDumpMemCksum() prototype.
01j,16aug05,dbt  Added device storage size in CORE_DUMP_DEV_INFO structure.
01i,11aug05,dbt  Added coreDumpGenerate() prototype.
01h,10aug05,dbt  Removed unused coreDumpMemWriteFilter() prototype.
01g,08aug05,dbt  Moved some definitions from coreDumpLib.h
01f,04aug05,jmp  updated coreDumpWrite().
01e,03aug05,dbt  Added coreDumpHookInit() & coreDumpCreateHooksRun()
                 prototypes.
01d,28jul05,jmp  moved CORE_DUMP_DEV_INFO from coreDumpLib.c.
		 added core dump memory region filtering.
01c,27jul05,jmp  added compression level to coreDumpCompressLibInit().
01b,18jul05,jmp  added compression support.
01a,01jul05,jmp  written.
*/

#ifndef __INCcoreDumpLibPh
#define __INCcoreDumpLibPh

#ifdef __cplusplus
extern "C" {
#endif

/* includes */

#ifndef HOST
#include <regs.h>
#include <excLib.h>
#endif
#include <coreDumpLib.h>

/* defines */

#define CORE_DUMP_MAGIC_NUMBER	0x45524f43	/* core dump magic number */
#define CORE_DUMP_VERSION	1		/* core dump version number */

#ifndef HOST
/* Core dump compression macros */

#define CORE_DUMP_COMPRESS_ENABLED	(coreDumpLibInfo.compressEnabled)

#define	CORE_DUMP_COMPRESS_INIT()					\
    ((coreDumpLibInfo.pCompressInitRtn == NULL) ?			\
    (OK) :								\
    ((*coreDumpLibInfo.pCompressInitRtn) ()))

#define CORE_DUMP_COMPRESS_WRITE(data, size)				\
    ((coreDumpLibInfo.pCompressWriteRtn == NULL) ?			\
    (OK) :								\
    ((*coreDumpLibInfo.pCompressWriteRtn) (data, size)))

#define CORE_DUMP_COMPRESS_TERMINATE()					\
    ((coreDumpLibInfo.pCompressTermRtn == NULL) ?			\
    (OK) :								\
    ((*coreDumpLibInfo.pCompressTermRtn) ()))

#define CORE_DUMP_COMPRESS_FLUSH(data, size)				\
    ((coreDumpLibInfo.pCacheWriteRtn == NULL) ?				\
    (coreDumpStorageWrite (data, size)) :				\
    ((*coreDumpLibInfo.pCacheWriteRtn) (data, size)))

/* Core dump cache macros */

#define	CORE_DUMP_CACHE_ENABLED		(coreDumpLibInfo.cacheEnabled)

#define CORE_DUMP_CACHE_INIT()						\
    ((coreDumpLibInfo.pCacheInitRtn == NULL) ?				\
    (OK) :								\
    ((*coreDumpLibInfo.pCacheInitRtn) ()))

#define CORE_DUMP_CACHE_WRITE(data, size)				\
    ((coreDumpLibInfo.pCacheWriteRtn == NULL) ?				\
    (OK) :								\
    ((*coreDumpLibInfo.pCacheWriteRtn) (data, size)))

#define CORE_DUMP_CACHE_FLUSH()						\
    ((coreDumpLibInfo.pCacheFlushRtn == NULL) ?				\
    (OK) :								\
    ((*coreDumpLibInfo.pCacheFlushRtn) ()))

#define DEVICE_DESC             coreDumpDevInfo.pCoreDumpDevDesc

#define CORE_DUMP_BLOCK_ALIGN                   \
        ((DEVICE_DESC->devWrtBlkSize != 0) ?    \
	DEVICE_DESC->devWrtBlkSize : 1)

#define BLOCK_ROUND_UP(x)                       \
        ROUND_UP (x, CORE_DUMP_BLOCK_ALIGN)

#define BLOCK_ROUND_DOWN(x)                     \
        ROUND_DOWN (x, CORE_DUMP_BLOCK_ALIGN)

#define CORE_DUMP_HDR_ALIGNED_SIZE		\
	BLOCK_ROUND_UP (sizeof (CORE_DUMP_HDR))

#define CORE_DUMP_STORAGE_HDR_ALIGNED_SIZE	\
	BLOCK_ROUND_UP (sizeof (CORE_DUMP_STORAGE_HDR))
#endif

/* Runtime information fields length */

#define	CORE_DUMP_RT_NAME_LEN		50
#define	CORE_DUMP_RT_VER_LEN		50
#define	CORE_DUMP_CPU_VARIANT_LEN	20
#define	CORE_DUMP_TOOL_NAME_LEN		20
#define	CORE_DUMP_BSP_SHORT_NAME_LEN	100
#define	CORE_DUMP_BSP_NAME_LEN		100
#define	CORE_DUMP_TYPE_LEN		30

/* typedefs */

typedef struct tgt_info_note	/* TGT_INFO_NOTE - target information note */
    {
    /* runtime information */

    char 	rtName[CORE_DUMP_RT_NAME_LEN];
    char 	rtVersion[CORE_DUMP_RT_VER_LEN];
    UINT32	cpuFamily;
    UINT32	cpuType;
    char 	cpuVariant[CORE_DUMP_CPU_VARIANT_LEN];
    char 	toolName[CORE_DUMP_TOOL_NAME_LEN];
    char 	bspShortName[CORE_DUMP_BSP_SHORT_NAME_LEN];
    UINT32	hasCoprocessor;
    BOOL	hasWriteProtect;
    UINT32	pageSize;
    UINT32	endian;
    char 	bspName [CORE_DUMP_BSP_NAME_LEN];
    UINT32	memBase;
    UINT32	memSize;
    UINT32	textScnCksum;

    /* exception information */

    UINT32	excValid;
    UINT32	excVecNum;
    UINT32	excCtxId;

    /* core dump information */

    char 	coreDumpType [CORE_DUMP_TYPE_LEN];

    /* registers information */

    REG_SET	regs;
    } TGT_INFO_NOTE;

#ifndef HOST
typedef struct core_dump_dev_desc	/* coredump device descriptor */
    {
    UINT32	devWrtBlkSize;		/* device write block size */
    char *	pDevWrtBlkCache;	/* device cache buffer */
    FUNCPTR	pDevEraseRtn;		/* device erase() routine */
    FUNCPTR	pDevOpenRtn;		/* device open() routine */
    FUNCPTR	pDevWriteRtn;		/* device write() routine */
    FUNCPTR	pDevReadRtn;		/* device read() routine */
    FUNCPTR	pDevFlushRtn;		/* device flush() routine */
    FUNCPTR	pDevCloseRtn;		/* device close() routine */
    } CORE_DUMP_DEV_DESC;

typedef struct core_dump_lib_info
    {
    /* general core dump information */

    BOOL	initialized;		/* core dump support initialized */

    FUNCPTR	pRtInfoGet;		/* Runtime information retrieval */
    UINT32	coreDumpMax;		/* maximun core dump on storage */
    char *	sysMemBaseAdrs;		/* LOCAL_MEM_LOCAL_ADRS */

    /* compression support information */

    BOOL	compressEnabled;	/* compression enabled ? */

    FUNCPTR	pCompressInitRtn;	/* compression init routine */
    FUNCPTR	pCompressTermRtn;	/* compression terminate routine */
    FUNCPTR	pCompressWriteRtn;	/* compression write routine */

    /* cache support information */

    BOOL	cacheEnabled;		/* cache enabled ? */

    FUNCPTR	pCacheInitRtn;		/* cache init routine */
    FUNCPTR	pCacheWriteRtn;		/* cache write routine */
    FUNCPTR	pCacheFlushRtn;		/* cache flush routine */

    /* core dump filtering information */

    STATUS 	(*pMemWriteFilter)	(
					VIRT_ADDR buffer,
					UINT32 size,
					VIRT_ADDR vaddr,
					PHYS_ADDR paddr,
					UINT32 state,
					UINT32 alignment
					);	/* memory region filter */
    } CORE_DUMP_LIB_INFO;

typedef struct core_dump_dev_info	/* information on core dump device */
    {
    BOOL	devInitialized;		/* Is core dump device initialized ? */
    UINT32	deviceId;		/* identifier of the core dump device */

    /* device routines */

    CORE_DUMP_DEV_DESC * pCoreDumpDevDesc; /* coredump device descriptor */

    /* information on the device */

    BOOL	formated;		/* Is device formated ? */
    size_t	size;			/* device size */
    UINT32	coreArrayOffset;	/* core dump array offset */
    UINT32	coreStartOffset;	/* core dump start offset */
    UINT32	coreWriteOffset;	/* core dump write offset */
    } CORE_DUMP_DEV_INFO;

typedef struct core_dump_storage_hdr	/* core dump storage header */
    {
    UINT32	coreDumpMagic;		/* core dump magic number */
    UINT32	version;		/* core dump version number */
    UINT32	coreDumpMax;		/* maximun core dump on storage */
    } CORE_DUMP_STORAGE_HDR;

typedef struct core_dump_hdr		/* coredump hdr for core dump storage */
    {
    UINT32	coreDumpMagic;		/* core dump magic number */
    UINT32	offset;			/* offset for this coredump */
    UINT32	nextOffset;		/* offset for next coredump */
    CORE_DUMP_INFO coreDumpInfo;	/* information on the core dump */
    } CORE_DUMP_HDR;

typedef struct core_dump_mem_filter
    {
    void *	addr;		/* address of memory region to filter */
    size_t	size;		/* size of memory region to filter */
    } CORE_DUMP_MEM_FILTER;

/* externals */

extern CORE_DUMP_LIB_INFO coreDumpLibInfo; /* core dump facility information */
extern CORE_DUMP_DEV_INFO coreDumpDevInfo; /* core dump device information */

extern char *	coreDumpMemPtr;		/* memory core dump base address */
extern UINT32	coreDumpMemSize;	/* memory core dump size */
extern UINT32   coreDumpCount;          /* core dump count on device */
extern CORE_DUMP_MEM_FILTER coreDumpMemFilter[];/* core dump mem filter array */
extern UINT32   coreDumpMemFilterMaxCount; /* Max # of core dump mem filter */

/* function declarations */

extern STATUS   coreDumpInit            (char * pStack,
					 UINT32 stackSize);
extern STATUS   coreDumpRawDevInit	(CORE_DUMP_RAW_DEV_DESC * pDesc);
extern STATUS   coreDumpWrite           (char * data, size_t size);
extern STATUS	coreDumpMemInit		(void);
extern UINT32	coreDumpMemRead		(UINT32 deviceId, size_t offset,
					 void * buffer, UINT32 maxbytes);
extern UINT32	coreDumpMemWrite	(UINT32 deviceId, size_t offset,
					 void * buffer, UINT32 nbytes);
extern STATUS	coreDumpCacheLibInit	(char * pCacheBuffer, UINT32 cacheSize);
extern STATUS	coreDumpCompressLibInit	(int level);
extern STATUS	coreDumpStorageWrite	(char * buffer, size_t size);
extern STATUS	coreDumpCreateHooksRun	(void);
extern void	coreDumpHookInit	(FUNCPTR * createTable,
					 UINT32 tableSize);
extern STATUS	coreDumpGenerate	(int vector, char * pEsf,
					 REG_SET * pRegs, EXC_INFO * pExcInfo,
					 CORE_DUMP_TYPE coreDumpType);
extern UINT32	coreDumpMemCksum	(char * addr, int len);
extern char *	coreDumpTypeStrGet	(char * typeStr,
					 CORE_DUMP_TYPE coreDumpType);
#endif	/* !HOST */

#ifdef __cplusplus
}
#endif

#endif /* __INCcoreDumpLibPh */
