/* adrSpaceLibP.h - Address Space Library header */

/* Copyright 1998-2004 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*
modification history
--------------------
01b,22sep04,zl   post-inspection fixes.
01a,31aug04,zl   created.
*/

#ifndef __INCadrSpaceLibPh
#define __INCadrSpaceLibPh

#ifdef __cplusplus
extern "C" {
#endif

/* includes */

#include "vxWorks.h"
#include "private/pgPoolLibP.h"
#include "private/vmLibP.h"


/* address space options */

#define	ADR_SPACE_OPT_KERNEL	0x00000001
#define	ADR_SPACE_OPT_RTP	0x00000002
#define	ADR_SPACE_OPT_SD	0x00000004

/* typedefs */

typedef struct adr_space_private_info
    {
    PAGE_POOL_ID	userPgPoolId;		/* user region free pool */
    PAGE_POOL_ID	userMstrPgPoolId;	/* user region master pool */
    PAGE_POOL_ID	kernelPgPoolId;		/* kernel region free pool */
    PAGE_POOL_ID	kernelMstrPgPoolId;	/* kernel region master pool */
    PAGE_POOL_ID	kernelVirtPgPoolId;	/* kernel virtual page pool */
    VIRT_ADDR		kernelMemTop;		/* top of kernel memory */
    VIRT_ADDR		localMemLocalAdrs;	/* LOCAL_MEM_LOCAL_ADRS */
    PHYS_MEM_DESC	*pPhysMemDesc;		/* mem.desc table */
    int			physMemDescNumEnt;	/* number of entries */
    
    } ADR_SPACE_PRIVATE_INFO;

typedef struct adrSpaceRgnCtl
    {
    UINT		allocUnit;		/* allocation unit size */
    PAGE_POOL_ID	freePgPoolId;		/* free page pool */
    PAGE_POOL_ID	masterPgPoolId;		/* master page pool */
    } ADR_SPACE_RGN_CTL;

typedef struct adrSpaceInitParams
    {
    PHYS_MEM_DESC *	pPhysMemDesc;		/* physMemDesc pointer */
    int 		physMemDescNumEnt;	/* number of elements */
    VIRT_ADDR   	localMemLocalAdrs;	/* where memory begins */
    VIRT_ADDR   	kernelMemTop;		/* top of kernel mem (heap) */
    VIRT_ADDR   	overlapBaseAdr;		/* overlapped rgn base adrs */
    UINT        	overlapMaxSize;		/* overlapped rgn max size */
    } ADR_SPACE_INIT_PARAMS;

/* globals */

extern PAGE_POOL_ID globalRAMPgPoolId;

/* function prototypes */

extern STATUS 	adrSpaceLibInit (VIRT_ADDR localMemLocalAdrs,
				 VIRT_ADDR kernelMemTop,
				 PHYS_MEM_DESC *pPhysMemDesc,
				 int physMemDescNumEnt,
				 VIRT_ADDR overlapBaseAdr,
				 UINT overlapMaxSize);
extern PAGE_POOL_ID adrSpaceCreate (UINT options);
extern PAGE_POOL_ID adrSpaceCreateAt (VIRT_ADDR virtAdr, UINT minBytes,
				      UINT options);
extern STATUS	adrSpaceDelete (PAGE_POOL_ID pgPoolId);
extern STATUS	adrSpaceAlloc (PAGE_POOL_ID poolId, UINT minBytes);
extern STATUS	adrSpaceAllocAt (PAGE_POOL_ID poolId, VIRT_ADDR virtAdrs,
				 UINT minBytes);
extern void	adrSpacePrivateInfoGet (ADR_SPACE_PRIVATE_INFO * pInfo);
extern STATUS 	adrSpaceShowInit ();

#ifdef __cplusplus
}
#endif

#endif /* __INCadrSpaceLibPh */
