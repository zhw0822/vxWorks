/* sdLibP.h - private shared data library header file */

/* 
 * Copyright (c) 2003-2005 Wind River Systems, Inc. 
 *
 * The right to copy, distribute, modify or otherwise make use 
 * of this software may be licensed only pursuant to the terms 
 * of an applicable Wind River license agreement. 
 */ 

/*
modification history
--------------------
01k,28mar05,zl   added sdRtpUnmap() prototype.
01j,08feb05,zl   added sdRtpMap() prototype.
01i,02oct04,gls  added sdLibInit() and sdShowInit() declarations
01h,24sep04,gls  removed MMU_ATTR_CACHE_MSK from SD_ATTR_MASK
01g,19jul04,gls  added hook routines (SPR #98361)
01f,04may04,gls  added currentAttr to SD_NODE
01e,29mar04,gls  added include of objLibP.h (SPR #95051)
01d,04mar04,gls  removed sdListNode from WIND_SD struct
01c,17feb04,gls  cleaned up
01b,11dec03,gls  added functionality
01a,04nov03,kk   written
*/

#ifndef __INCsdLibPh
#define __INCsdLibPh

#include "vxWorks.h"
#include "sdLib.h"
#include "private/objLibP.h"

#ifdef __cplusplus
extern "C" {
#endif

extern STATUS   sdLibInit  (const int hookTblSize);
extern void     sdShowInit (void);

/* defines */

#define SD_DEFAULT_ATTR (MMU_ATTR_SUP_RW | MMU_ATTR_USR_RW | \
			 MMU_ATTR_VALID | MMU_ATTR_CACHE_DEFAULT)

#define SD_ATTR_MASK    (MMU_ATTR_PROT_MSK | MMU_ATTR_VALID_MSK | \
			 MMU_ATTR_SPL_MSK) 

/* private options */

#define	SD_LIB_TEXT	0x00000003	/* SD is used for SL */
#define	SD_HOOK_IGNORE	0x00000004	/* internal call, skip hook routines */

/* typedef */

typedef struct sdNode		/* Node of a linked list. */
    {
    struct dlnode *next;	/* Points at the next node in the list */
    struct dlnode *previous;	/* Points at the previous node in the list */

    SD_ID    sdId;
    MMU_ATTR currentAttr;	/* Current MMU attributes of SD in the RTP */
    } SD_NODE;

typedef struct wind_sd
    {
    OBJ_CORE	 objCore;		/* object management */
    SEM_ID       semId;			/* SD semaphore */
    int		 options;		/* SD options */
    MMU_ATTR	 attr;			/* MMU attributes allowed on this SD */
    UINT	 size;			/* size of the SD */
    UINT	 clientCount;		/* current number of client RTPs */
    PAGE_POOL_ID virtPgPoolId;		/* shared data virtual page pool */
    PHYS_ADDR	 physAdrs;		/* start physical address of mapping */
    VIRT_ADDR	 virtAdrs;		/* start virtual address of mapping */
    } WIND_SD;


/* private function declarations */

extern VIRT_ADDR sdRtpMap (SD_ID sdId, MMU_ATTR attr, int options,
			   RTP_ID rtpId);
extern STATUS    sdRtpUnmap (SD_ID sdId, RTP_ID rtpId, int options);


#ifdef __cplusplus
}
#endif	/* __cplusplus */

#endif /* __INCsdLibPh */
