/* shellMemLibP.h - private header for the shell memory library */

/* Copyright 2004 Wind River Systems, Inc. */

/*
modification history
--------------------
01d,23sep04,bpn  Added definitions from shellMemLib.h.
01c,25aug04,bpn  Added shellMemStringIsAllocated() prototype.
01b,06apr04,bpn  Fixed scalability issue with memShow.o
01a,23feb04,bpn  Written.
*/

#ifndef __INCshellMemLibPh
#define __INCshellMemLibPh

#ifdef __cplusplus
extern "C" {
#endif

/* Includes */

#include <lstLib.h>

/* Structures */

typedef struct shell_alloc
    {
    NODE	node;
    void *	pMem;	/* memory area address allocated */
    INT32	flags;	/* flags associated to that memory */
    } SHELL_ALLOC;

/* Function definition */

#if !defined(HOST) && defined(DEBUG)
extern void	shellMemShow (int type);
#endif

extern void	shellMemRegAllFree (INT32 flags, INT32 mask, LIST * pList);
extern void	shellMemAllRegFree (INT32 flags, INT32 mask,
				    VOIDFUNCPTR freeRtn, LIST * pList);
extern BOOL	shellMemStringIsAllocated (SHELL_ID shellId,
					   const char * string);
/* XXX bpn - needed by usrLib.c */

extern char *	shellMemStringAdd (SHELL_ID shellId, const char * string);
extern STATUS	shellMemStringFree (SHELL_ID shellId, const char * string);
extern void	shellMemStringShow (SHELL_ID shellId);


#ifdef __cplusplus
}
#endif

#endif /* __INCshellMemLibPh */
