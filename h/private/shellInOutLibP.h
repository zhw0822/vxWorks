/* shellInOutLibP.h - private header file for the shell IO management module */

/* Copyright 2004 Wind River Systems, Inc. */

/*
modification history
--------------------
01e,22sep04,bpn  Added some definitions from shellInOutLib.h.
01d,30jun04,bpn  Fixed SPR#97937: removed shellInOutCompatible...() functions.
01c,15apr04,bpn  Added background structure definition.
01b,26mar04,bpn  Added structure definitions for shellInOutBlockingXxx()
                 functions.
01a,17feb04,bpn  Written.
*/

#ifndef __INCshellInOutLibPh
#define __INCshellInOutLibPh

#ifdef __cplusplus
extern "C" {
#endif

#include <lstLib.h>
#include <shellLib.h>

enum ioType
    {
    SHELL_IO_REDIR = 1,
    SHELL_IO_BLOCKING,
    SHELL_IO_BACKGROUND
    };

struct redirection
    {
    BOOL	isRedirected;	/* TRUE if redirected */
    int		curInFd;	/* input before redirection */
    int		curOutFd;	/* output before redirection */
    int		curErrFd;	/* error before redirection */
    int		inFd;		/* input for redirection */
    int		outFd;		/* output for redirection */
    int		errFd;		/* error for redirection */
#ifdef HOST
    int		haveTerminal;	/* for host shell */
#endif	
    };

struct blocking
    {
    char *  name;
    int	    masterFd;
    int	    slaveFd;
    BOOL    isBlocked;
    SEM_ID  stateMutexId;
    SEM_ID  structMutexId;
    UINT32  data;
    };

struct background
    {
    char *  name;
#ifdef HOST
    char *  masterFilename;
    char *  slaveFilename;
#endif
    int	    masterFd;
    int	    slaveFd;
    };

union ioData
    {
    struct blocking	blocking;
    struct redirection	redirection;
    struct background 	background;
    };

typedef struct shell_io_fd_node
    {
    NODE	    node;
    enum ioType	    type;
    union ioData    data;
    } SHELL_IO_FD_NODE;

extern BOOL	shellInOutInputUniqueCheck (int shellInputFd);
extern STATUS	shellInOutInputHookSet (SHELL_ID shellId);
extern STATUS	shellInOutInputHookUnset (SHELL_ID shellId);

/* XXX bpn - define this function here because they are used by edrStub.c */

extern int	shellPrint (SHELL_ID shellId, const char * fmt, ...);
extern int	shellPrintErr (SHELL_ID shellId, const char * fmt, ...);

/* XXX bpn - define this function here because they are used by external files */

extern void	shellInOutUse (SHELL_ID shellId);

#ifdef __cplusplus
}
#endif

#endif /* __INCshellInOutLibPh */
