/* semPxSysCall.h - POSIX semaphore system calls */

/* Copyright 2003 Wind River Systems, Inc. */

/*
modification history
--------------------
01b,24mar04,mcm  Including time.h instead of sys/time.h in user-space
01a,17nov03,m_s  written
*/

#ifndef __INCsemPxSysCallh
#define __INCsemPxSysCallh

#ifdef __cplusplus
extern "C" {
#endif

#include "objLib.h"
#include "pxObjSysCall.h"

#include "time.h"

/* typedefs */

/* structure for pxCtl system call */

struct semGetvalueArgs    /* PX_SEM_GETVALUE */
    {
    int * sval;
    };

/* system call function prototypes */

extern int pxSemWait   (OBJ_HANDLE handle, PX_WAIT_OPTION waitOption,
                        struct timespec * timeout);
extern int pxSemPost   (OBJ_HANDLE handle);

#ifdef __cplusplus
}
#endif

#endif /* __INCsemPxSysCallh */
