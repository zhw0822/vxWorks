/* windPwrLibP.h - VxWorks power library private header file */

/* Copyright 2002 - 2003 Wind River Systems, Inc. */

/*
modification history
--------------------
01b,01jul03,rec  code review comments
01a,29apr02,rec  Created
*/

#ifndef __INCwindPwrLibPh
#define __INCwindPwrLibPh

#ifdef __cplusplus
extern "C" {
#endif

#include "vxWorks.h"
#include "vwModNum.h"
#include "qLib.h"
#include "taskLib.h"
#include "semLib.h"
#include "private/wdLibP.h"

/* variable declarations */

extern BOOL     windPwrOffState;

#if defined(__STDC__) || defined(__cplusplus)

/* function declarations */

extern void 	windPwrDown (void);
extern void 	windPwrUp (void);

#else

extern void 	windPwrDown ();
extern void 	windPwrUp ();

#endif	/* __STDC__ */

#ifdef __cplusplus
}
#endif

#endif /* __INCwindPwrLibPh */
