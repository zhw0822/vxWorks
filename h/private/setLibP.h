/* setLibP.h - Private header file for Wind Object Sets */

/* Copyright 1984-2002 Wind River Systems, Inc. */

/*
modification history
--------------------
01d,14oct02,dat  5.X compatibility
01c,18apr01,cjj  mods after FHA 1.0 code inspection
01b,27nov00,cjj  added OBJ_SET definition which was in setLib.h
01a,16feb98,mas, original version.  TOR 1.0.1/VXW 5.3 compliant.
	    dat
*/

/*
DESCRIPTION
This file contains the Private configuration parameters for the Wind Object
Set library.

A set is an object containing a collection of objects with no implied order
although application-specific ordering may be enforced through other libraries.
A set may have any number of objects.  A set may contain other sets.  A set
with no objects is a null set.  Sets have no size restrictions other than
available memory.

Objects are referenced by a set, never included directly.  This allows objects
to be independent of set implementation and target architecture variations.
*/

#ifndef INCsetLibPh
#define INCsetLibPh

#ifdef __cplusplus
extern "C" {
#endif

/* Includes */

#include "vxWorks.h"
#include "private/objLibP.h"
#include "private/classLibP.h"
#include "setLib.h"
#include "lstLib.h"
#include "semLib.h"

/* defines */

#define SET_CLASS_OPTIONS WIND_CLASS_ALLOW_DUPLICATE_NAMES

/* Set mutex semaphore attributes */

#define SET_SEM_CREATE_ATTRIB \
    (SEM_Q_PRIORITY | SEM_INVERSION_SAFE | SEM_DELETE_SAFE)

/* Typedefs */

typedef struct setNode
    {
    NODE    node;       /* doubly-linked list pointers */
    SET_ITEM  itemId;      /* object ID */
    } SET_NODE;
typedef struct setNode * NODE_ID;

/* Object Set structure */
 
typedef struct setObj
    {
    OBJ_CORE objCore;
    LIST     setList;   /* Header of doubly-linked list (set) of objects */
    NODE_ID  curNode;   /* current node ID in set traversal */
    SEM_ID   mutex;     /* Mutex to use when servicing set */
    } SET_OBJ;

/* Global exports */

extern CLASS_ID setClassId;

#ifdef __cplusplus
}
#endif

#endif  /* INCsetLibPh */
