/* poolLibP.h - private header file for Wind Memory Pools */

/*
 * Copyright (c) 1998-2005 Wind River Systems, Inc.
 *
 * The right to copy, distribute or otherwise make use of this software
 * may be licensed only pursuant to the terms of an applicable Wind River
 * license agreement. 
 */

/*
modification history
--------------------
01f,12aug05,zl   exclude building in unsupported layers.
01e,08aug05,zl   added POOL_CREATE and POOL_ITEM_GET macros. Embedded semaphore
                 in pool structure.
01d,27jul05,mil  Added support for minimal kernel.
01c,18feb04,dat  POOL structure is private, not public
01b,20aug03,zl   added item alignment support.
01a,22feb98,dat, original version.  TOR 1.0.1/VXW 5.3 compatible.
	    mas,tm
*/

#ifndef INCpoolLibPh
#define INCpoolLibPh

#ifdef __cplusplus
extern "C" {
#endif

/* Includes */

#include <vxWorks.h>
#include <private/semLibP.h>
#include <poolLib.h>
#include <lstLib.h>


/* Private macros */

#define POOL_SEM_CREATE_ATTRIB \
    (SEM_Q_PRIORITY | SEM_INVERSION_SAFE | SEM_DELETE_SAFE)

#define POOL_SEM_TAKE_ATTRIB	WAIT_FOREVER

/* indicator of a block that is provided by user */

#define BLOCK_DO_NOT_FREE	0x00000001

#ifndef _ASMLANGUAGE


#ifdef _WRS_KERNEL
#define POOL_LIST_LOCK()	semMTake (&poolListMutex, POOL_SEM_TAKE_ATTRIB)
#define POOL_LIST_UNLOCK()	semMGive (&poolListMutex)
#define POOL_LOCK(poolId)	semMTake (&(poolId)->mutex, POOL_SEM_TAKE_ATTRIB)
#define POOL_UNLOCK(poolId)	semMGive (&(poolId)->mutex)
#else
#define POOL_LIST_LOCK()	semTake (&poolListMutex, POOL_SEM_TAKE_ATTRIB)
#define POOL_LIST_UNLOCK()	semGive (&poolListMutex)
#define POOL_LOCK(poolId)	semTake (&(poolId)->mutex, POOL_SEM_TAKE_ATTRIB)
#define POOL_UNLOCK(poolId)	semGive (&(poolId)->mutex)
#endif


#ifdef INCLUDE_MEM_ALLOT

/*
 * for layers that don't support poolLib (MKL), simulate the API via
 * memAllotLib. There is no POOL_ITEM_RETURN() in this case.
 */

#define POOL_CREATE(name, itmSize, align, initCnt, incrCnt, partId, options)   \
	    memAllotPoolCreate(itmSize, align)
#define POOL_ITEM_GET(poolId)						       \
	    memAllot(poolId->sizItem, poolId->alignment)

typedef struct pool	/* Pool structure */
    {
    ULONG		sizItem;	/* individual item size in bytes */
    ULONG		alignment;	/* individual item alignment in bytes */
    } POOL;

extern void *  memAllot (unsigned size, unsigned alignment);
extern POOL_ID memAllotPoolCreate (unsigned itmSize, unsigned alignment);

#else	/* INCLUDE_MEM_ALLOT */

#define POOL_CREATE(name, itmSize, align, initCnt, incrCnt, partId, options)   \
	    poolCreate(name, itmSize, align, initCnt, incrCnt, partId, options)
#define POOL_ITEM_GET(poolId)						       \
	    poolItemGet(poolId)
#define POOL_ITEM_RETURN(poolId,pItem)					       \
	    poolItemReturn(poolId, pItem)

/* build poolLib.c and poolShow.c only in this case */

#define _BUILD_POOL_LIB

/* Pool Block records */

typedef struct pool	/* Pool structure */
    {
    NODE		poolNode;	/* pool list for pond */
    const char *	poolName;	/* pool name */
    SEMAPHORE		mutex;		/* Mutex to use for thread-safe pools */
    ULONG		sizItem;	/* individual item size in bytes */
    ULONG		sizItmReal;	/* actual item size, due to alignment */
    ULONG		alignment;	/* individual item alignment in bytes */
    ULONG		numIncr;	/* nr. of incrementally allocated itm */
    ULONG		numTotl;	/* total number of items created */
    PART_ID		partId;		/* memory partition ID */
    ULONG		options;	/* pool options word */
    LIST		blockList;	/* allocated block list */
    LIST		freeItems;	/* free item list */
    } POOL;

typedef struct pool_block
    {
    NODE	blockNode;	/* block node for pool */
    void *	pItmBlk;	/* ptr to start of item pool block */
    ULONG	itemCnt;	/* count of items in this block */
    } POOL_BLOCK;

#endif	/* INCLUDE_MEM_ALLOT */


#endif /* _ASMLANGUAGE */

#ifdef __cplusplus
}
#endif

#endif  /* INCpoolLibPh */
