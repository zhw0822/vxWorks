/* isrLibP.h - ISR objects private header */

/* Copyright 2004 Wind River Systems, Inc. */

/*
modification history
--------------------
01e,02jun04,lei  added 4 UINT32 variables to WIND_ISR(SPR 97866)
01d,29apr04,cjj  added isrShowInit() prototype
01c,29oct03,kam  ISR Object code inspection mods
01b,04jun03,kam  updated structure name from windISR to wind_isr
01a,03jun03,kam  written.
*/

#ifndef __INCisrLibPh
#define __INCisrLibPh

#ifdef __cplusplus
extern "C" {
#endif

/* includes */

#include "private/objLibP.h"
    
/* typedefs */

typedef struct wind_isr
    {
    OBJ_CORE     objCore;          /* wind object management struct        */

    UINT         isrTag;           /* interrupt tag                        */
    UINT         count;            /* # of times this ISR has been invoked */
    UINT    	 serviceCount;	   /* # of times this ISR has returned OK  */
    UINT64	 cpuTime;	   /* cpu time spent in ISR 		   */
    int          options;          /* ISR object options                   */  
    
    FUNCPTR      handlerRtn;       /* pointer to handler routine           */
    int          arg;              /* parameter to be passed to routine    */

    UINT32	 reserved1;	   /* reserved 1 			   */
    UINT32	 reserved2;	   /* reserved 2 			   */
    UINT32	 spare1;	   /* possible user extension		   */
    UINT32	 spare2;	   /* possible user extension		   */  
    UINT32	 spare3;	   /* possible user extension		   */
    UINT32	 spare4;	   /* possible user extension		   */
    } WIND_ISR;

/* externs */

extern CLASS_ID isrClassId;

extern STATUS isrLibInit (void);
extern STATUS isrShowInit (void);
extern STATUS isrInit (ISR_ID isrId, char * name, UINT isrTag, 
                       FUNCPTR handlerRtn, int parameter, UINT options);
extern STATUS isrTerminate (ISR_ID isrId);
extern STATUS isrDestroy (WIND_ISR * isrId, BOOL dealloc);

extern STATUS isrHandlerSet (ISR_ID isrId, FUNCPTR routine, 
        int parameter);
extern STATUS isrCpuTimeGet (ISR_ID isrId, UINT64 * cpuTime);
extern STATUS isrCpuTimeSet (ISR_ID isrId, UINT64 cpuTime);

#ifdef __cplusplus
}
#endif

#endif /* __INCisrLibPh */
