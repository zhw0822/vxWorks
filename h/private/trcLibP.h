/* trcLibP.h - private trace library header file */

/* Copyright 1984-2004 Wind River Systems, Inc. */

/*
modification history
--------------------
01d,14may04,zl   reordered TRC_LIB_FUNCS fields.
01c,02apr04,jmp  added pc argument to TRC_LVL_ARGS_GET().
01b,19mar04,zl   updated for user build.
01a,18feb04,zl 	 created
*/

#ifndef __INCtrcLibPh
#define __INCtrcLibPh

#ifdef __cplusplus
extern "C" {
#endif


#include "vxWorks.h"
#ifdef _WRS_KERNEL
#include "regs.h"
#include "private/syscallLibP.h"
#endif

#ifndef	_ASMLANGUAGE

/* structure passed to the arch primitives */

typedef struct trc_os_ctx
    {
    char *		stackBase;	/* stack base */
    char *		stackEnd;	/* stack end */
    FUNCPTR		pcValidateRtn;	/* routine to validate PC */
#ifdef _WRS_KERNEL
    REG_SET *		pRegs;		/* register set */
    BOOL		isSyscall;	/* TRU if tracing a system call */
#endif
    } TRC_OS_CTX;

/* structure passed to OS trace routine */

typedef struct trc_ctx
    {
    FUNCPTR	 usrRtn;
    int		 taskId;
    BOOL	 isKernelAdrs;
    TRC_OS_CTX * pOsCtx;
    void *	 usrCtx;
    int		 nArgs;
    int	*	 pArgs;
    BOOL	 verbose;
    } TRC_CTX;


/* primitives provided by architecture specific trace libraries */

typedef STATUS (* TRC_LVL_INFO_GET) (char * fp, INSTR * pc, TRC_OS_CTX * pCtx,
				     char ** pPrevFp, INSTR ** pPrevPc, 
				     INSTR ** pFuncAddr);
typedef int (* TRC_LVL_ARGS_GET) (INSTR * pc, INSTR * funcAddr, char * fp, 
				  int pArgBuf[], int argMax, 
				  int ** ppArgs);

#ifdef _WRS_KERNEL
typedef STATUS (* TRC_TOP_INFO_GET) (TRC_OS_CTX * pCtx, char ** pFp, 
				     char ** pPrevFp, INSTR ** pPrevPc, 
				     INSTR ** pFuncAddr);

typedef void (* TRC_SYSCALL_CHECK) (SYSCALL_ENTRY_STATE * pScState,
				    TRC_OS_CTX * pCtx);
#endif

/* structure for trace library primitives */

typedef struct trc_lib_funcs
    {
    TRC_LVL_INFO_GET	lvlInfoGet;	/* routine to get frame info */
    TRC_LVL_ARGS_GET	lvlArgsGet;	/* routine to get arguments */
#ifdef _WRS_KERNEL
    TRC_TOP_INFO_GET	topInfoGet;	/* routine to get top frame info */
    TRC_SYSCALL_CHECK	syscallCheck;	/* routine check if syscall */
#endif
    } TRC_LIB_FUNCS;

/* private global variable declarations */

extern TRC_LIB_FUNCS trcLibFuncs;

/* private function declarations */

#ifdef _WRS_KERNEL
extern STATUS trcStackTrace (REG_SET * pRegs, TRC_CTX * pCtx);
#endif

#endif	/* _ASMLANGUAGE */

#ifdef __cplusplus
}
#endif

#endif /* __INCtrcLibPh */
