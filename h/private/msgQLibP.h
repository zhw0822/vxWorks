/* msgQLibP.h - private message queue library header file */

/* Copyright 1984-2004 Wind River Systems, Inc. */

/*
modification history
--------------------
01m,04oct04,aeg  added #ifndef _ASMLANGUAGE.
01l,31aug04,pcm  integrated qJobLib.h
01k,28sep04,fr   removed vMsgQCreate prototype (SPR 101349)
01j,22mar04,dcc  added vMsgQCreate prototype.
01i,26aug03,tcr  remove WindView InstClassId
01h,06sep01,bwa  Added VxWorks events support.
01g,19may98,drm  merged 3rd party code which added external declarations
                 of distributed msgQ routines.
                 - merged code was originally based on version 01d
01f,17apr98,rlp  canceled MSG_Q modification for backward compatibility.
01e,04nov97,rlp  modified MSG_Q structure for tracking messages sent.
01d,16jan94,c_s  added extern declaration for msgQInstClassId.
01c,22sep92,rrr  added support for c++
01b,19jul92,pme  added external declaration of shared msgQ show routine.
01a,04jul92,jcf  created.
*/

#ifndef __INCmsgQLibPh
#define __INCmsgQLibPh

#ifdef __cplusplus
extern "C" {
#endif

#ifndef _ASMLANGUAGE
#include "vxWorks.h"
#include "vwModNum.h"
#include "qLib.h"
#include "sllLib.h"
#include "msgQLib.h"
#include "classLib.h"

#include "private/objLibP.h"
#include "private/eventLibP.h"

typedef struct
    {
    SL_LIST	list;
    int		count;
    Q_HEAD	pendQ;
    } MSG_Q_HEAD;

typedef struct msg_q		/* MSG_Q */
    {
    OBJ_CORE		objCore;	/* object management */
    MSG_Q_HEAD		msgQ;		/* message queue head */
    MSG_Q_HEAD		freeQ;		/* free message queue head */
    UINT32		options;	/* message queue options */
    int			maxMsgs;	/* max number of messages in queue */
    int			maxMsgLength;	/* max length of message */
    int			sendTimeouts;	/* number of send timeouts */
    int			recvTimeouts;	/* number of receive timeouts */
    EVENTS_RSRC		events;		/* VxWorks events */
    } MSG_Q;

typedef struct			/* MSG_NODE */
    {
    SL_NODE		node;		/* queue node */
    int			msgLength;	/* number of bytes of data */
    MSG_Q *		pMsgQ;		/* node's message queue */
    } MSG_NODE;

typedef struct				/* This structure is embedded */
    {					/* into the WIND_TCB structure. */
    union				/* Beware of TCB offset changes */
	{				/* should its size change. */
	char *		buffer;
	MSG_NODE *	node;
	} msg;
    UINT		size;
    } MSGQ_TCB;

/* message priority hidden to public */

#define MSG_PRI_DONT_CARE       MSG_PRI_URGENT  /* head is faster */

#define	MSG_Q_RESTRICT_UNPEND	0x80000000
#define	MSG_Q_OPTIONS_MASK	(MSG_Q_TYPE_MASK |		\
				 MSG_Q_EVENTSEND_ERR_NOTIFY |	\
				 MSG_Q_INTERRUPTIBLE)

/* macros */

#define MSG_NODE_DATA(pNode)   (((char *) pNode) + sizeof (MSG_NODE))

/* variable definitions */

extern CLASS_ID msgQClassId;		/* message queue class id */

/* shared memory objects function pointers */

extern FUNCPTR  msgQSmSendRtn;
extern FUNCPTR  msgQSmReceiveRtn;
extern FUNCPTR  msgQSmNumMsgsRtn;
extern FUNCPTR  msgQSmShowRtn;

/* distributed objects function pointers */

extern FUNCPTR  msgQDistSendRtn;
extern FUNCPTR  msgQDistReceiveRtn;
extern FUNCPTR  msgQDistNumMsgsRtn;
extern FUNCPTR  msgQDistShowRtn;

/* function declarations */

extern STATUS	msgQTerminate (MSG_Q_ID msgQId);
extern STATUS	msgQInit (MSG_Q *pMsgQ, int maxMsgs, int maxMsgLength,
			  int options, void *pMsgPool);

#endif /* _ASMLANGUAGE */

#ifdef __cplusplus
}
#endif

#endif /* __INCmsgQLibPh */
