/* timerLibP.h - private POSIX 1003.4 Clocks & Timers header */

/* Copyright 2003-2004 Wind River Systems, Inc. */

/*
modification history
--------------------
03n,26oct04,fr   added timerOpenInit() routine (SPR 101491)
03m,28sep04,fr   removed vTimerCreate prototype (SPR 101349)
03l,23aug04,lei  added timer_destroy() prototype.
03k,31mar04,dcc  added vTimerCreate() prototype. Added timerClassId declaration.
03j,02dec03,ans  code inspection changes
03i,22oct03,ans  added extern declaration for timerLibInit.
03h,30sep03,ans  User-level timer support.
03g,12nov01,gls  Account for system clock rate (SPR #70255)
03f,27jun96,dbt  modified TV_CONVERT_TO_TICK to round up the value to
		 return (SPR #5647).
		 Updated copyright.
03e,29nov93,dvs  updated for POSIX draft 14
03d,23sep92,kdl  changed name to timerLibP.h
03c,22sep92,rrr  added support for c++
03b,31jul92,gae  added sigevent.h, time.h and timers.h includes.
03a,22jul92,gae  Draft 12 revision; removed object stuff; moved
		 some prototype definitions to timers.h.
02a,04jul92,jcf  cleaned up.
01d,26may92,rrr  the tree shuffle
01c,30apr92,rrr  some preparation for posix signals.
01a,16oct91,gae  written.
*/

/*
DESCRIPTION
This file provides header information for the
POSIX 1003.4 Clocks & Timers interface per Draft 12.
*/

#ifndef __INCtimerLibPh
#define __INCtimerLibPh

#ifdef __cplusplus
extern "C" {
#endif

#include "sys/types.h"
#include "time.h"
#include "signal.h"
#include "sigevent.h"
#include "timers.h"
#include "private/sigLibP.h"
#include "private/objLibP.h"
#include "wdLib.h"
#include "stdarg.h"


typedef struct __timer
    {
    OBJ_CORE            objCore;        /* object core */
    WDOG_ID		wdog;		/* expiry mechanism */
    int			active;		/* wdog is set */
    int			taskId;		/* "owner" of timer */
    clock_t		clock_id;	/* always CLOCK_REALTIME */
    SIGEVENT_ID         sigevId;        /* sigevent ID for async notification */
    struct sigevent	sevent;		/* user's signal event */
    struct itimerspec	exp;		/* time to go off plus interval */
    VOIDFUNCPTR		routine;	/* user's routine */
    int			arg;		/* argument to user's routine */
    struct timespec     timeStamp;	/* timestamp when timer is armed */
    } TIMER;

typedef struct clock
    {
    UINT64 tickBase;			/* vxTicks when time set */
    struct timespec timeBase;		/* time set */
    } CLOCK;

extern struct clock _clockRealtime;
extern CLASS_ID		timerClassId;		/* timer class id */

/* macros for "time value" manipulation */

#define BILLION         1000000000	/* 1000 million nanoseconds / second */

#define TV_EQ(a,b)	\
        ((a).tv_sec == (b).tv_sec && (a).tv_nsec == (b).tv_nsec)
#define TV_GT(a,b)	\
        ((a).tv_sec  > (b).tv_sec ||	\
        ((a).tv_sec == (b).tv_sec && (a).tv_nsec > (b).tv_nsec))
#define TV_LT(a,b)	\
        ((a).tv_sec  < (b).tv_sec ||	\
        ((a).tv_sec == (b).tv_sec && (a).tv_nsec < (b).tv_nsec))
#define TV_ISZERO(a)	\
        ((a).tv_sec == 0 && (a).tv_nsec == 0)
#define TV_SET(a,b)	\
        (a).tv_sec = (b).tv_sec; (a).tv_nsec = (b).tv_nsec
#define TV_ADD(a,b)	\
        (a).tv_sec += (b).tv_sec; (a).tv_nsec += (b).tv_nsec; TV_NORMALIZE(a)
#define TV_SUB(a,b)	\
        (a).tv_sec -= (b).tv_sec; (a).tv_nsec -= (b).tv_nsec; TV_NORMALIZE(a)
#define	TV_NORMALIZE(a)	\
	if ((a).tv_nsec >= BILLION)	\
	    { (a).tv_sec++; (a).tv_nsec -= BILLION; }	\
	else if ((a).tv_nsec < 0)	\
	    { (a).tv_sec--; (a).tv_nsec += BILLION; }
#define TV_VALID(a)	\
        ((a).tv_nsec >= 0 && (a).tv_nsec < BILLION)
#define TV_ZERO(a)	\
        (a).tv_sec = 0; (a).tv_nsec = 0

#define TV_CONVERT_TO_SEC(a,b)  \
        do { \
        register UINT32 hz = sysClkRateGet(); \
	(a).tv_sec  = (time_t)((b) / hz);   \
	(a).tv_nsec = (long)(((b) % hz) * (BILLION / hz)); \
        } while (0)

#define TV_CONVERT_TO_TICK(a,b) \
        do { \
        register UINT32 hz = sysClkRateGet(); \
        register UINT32 res = (BILLION / hz); \
        (a) = (UINT64) ((b).tv_sec) * hz + \
 	      (UINT64) ((b).tv_nsec) / res + \
              (((UINT64)((b).tv_nsec) % res) ? 1 : 0); \
        } while (0)

/* non-standard function declarations */

extern int 	clock_setres (clockid_t clock_id, struct timespec *res);

extern int 	timer_cancel (timer_t timerid);
extern int 	timer_connect (timer_t timerid, VOIDFUNCPTR routine, int arg);
extern int 	timer_show (timer_t timerid);
extern STATUS   timerLibInit (void);
extern void	timerOpenInit (void);
extern int      timer_destroy (timer_t timerid, BOOL dealloc);

#ifdef __cplusplus
}
#endif

#endif /* __INCtimerLibPh */
