/* eventSysCall.h - VxWorks user event system call definitions */

/* Copyright 2004 Wind River Systems, Inc. */

/*
modification history
--------------------
01a,09mar04,hya  created
*/
#ifndef __INCeventSysCallh
#define __INCeventSysCallh

#ifdef __cplusplus
extern "C" {
#endif

#include "vxWorks.h"

/* command codes for the eventCtl system call */
typedef enum
    {
    VX_EVT_CTL_SEM_START,
    VX_EVT_CTL_SEM_STOP,
    VX_EVT_CTL_MQ_START,
    VX_EVT_CTL_MQ_STOP,
    VX_EVT_CTL_EVENT_CLEAR
    } VX_EVT_CTL_CMD;

/* argument structure for the eventCtl() system call */

typedef struct vx_rsc_event_start_args
    {
    UINT32 registered;	/* 0x00: events registered for that resource	*/
    UINT32 options;	/* 0x04: user options				*/
    } VX_SRC_EVENT_START_ARGS;

/* system call function prototypes */
extern STATUS eventSend (int taskId, UINT32 events);
extern STATUS eventReceive (
			    UINT32 events,
			    UINT8 options,
			    int timeout,
			    UINT32 *eventsReceived
			   );
extern STATUS eventCtl (
			OBJ_HANDLE handle,
			VX_EVT_CTL_CMD command,
			void * pArg,
			UINT * pArgSize
			);


#ifdef __cplusplus
}
#endif

#endif /* __INCeventSysCallh */
