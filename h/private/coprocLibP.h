/* coprocLibP.h - coprocessor management library private header */

/* Copyright 1984-2003 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*
modification history
--------------------
01b,02oct04,yvp  Added new members to COPROC_DESC for mRegs support.
01a,21mar03,yvp	 written.
*/

#ifndef __INCcoprocLibPh
#define __INCcoprocLibPh

#ifdef __cplusplus
extern "C" {
#endif

#ifndef	_ASMLANGUAGE

#include "vxWorks.h"

/* Coprocessor Descriptor structure */

typedef struct coprocDesc 
    {
    struct coprocDesc *next;            /* next descriptor in list */
    UINT32      mask;                   /* bit mask position in task option */
    int         ctxSize;                /* context size in bytes */
    int         ctxAlign;               /* memory alignment for context */
    FUNCPTR     pCtxCreateRtn;          /* allocate & init a coprocessor ctx */
    FUNCPTR     pCtxDeleteRtn;          /* delete the coprocessor ctx */
    FUNCPTR     pEnableRtn;             /* enable the coprocessor */
    FUNCPTR     pDisableRtn;            /* disable the coprocessor */
    FUNCPTR     pCtxSaveRtn;            /* context save routine */
    FUNCPTR     pCtxRestoreRtn;         /* context restore routine */
    FUNCPTR     pCtxGetRtn;             /* get the coprocessor context */
    FUNCPTR     pCtxSetRtn;             /* set the coprocessor context */
    FUNCPTR     pDsmRtn;                /* disassembly routine */
    FUNCPTR     pExcHandlerRegisterRtn; /* register exc. handlers */
    FUNCPTR     pShowRtn;               /* show the coprocessor context */
    FUNCPTR     pAboutRtn;              /* displays a short description */
    FUNCPTR     pMRegsRtn;              /* interactively modify reg. values */
    FUNCPTR     pRegListShowRtn;        /* show list of available registers */
    void *      pLastTask;              /* last task to use this coproc. */
    BOOL        showOnDebug;            /* show registers on debug events */
    UINT32      spare1; 
    UINT32      spare2; 
    } COPROC_DESC;

/* Coprocessor Table Entry structure */

typedef struct coprocTblEntry
    {
    void *        pCtx;                 /* points to coprocessor REG_SET */
    COPROC_DESC * pDescriptor;          /* points to associated descriptor */
    } COPROC_TBL_ENTRY;

#endif	/* _ASMLANGUAGE */

#ifdef __cplusplus
}
#endif

#endif /* __INCcoprocLibPh */
