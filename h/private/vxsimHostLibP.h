/* vxsimHostLibP.h - private Host/VxSim interface header */

/*
 * Copyright (c) 2004-2005 Wind River Systems, Inc.
 *
 * The right to copy, distribute or otherwise make use of this software
 * may be licensed only pursuant to the terms of an applicable Wind River
 * license agreement. No license to Wind River intellectual property rights
 * is granted herein. All rights not licensed by Wind River are reserved
 * by Wind River.
 */

/*
modification history
--------------------
01e,29jun05,jmp  reworked vxsimHostModel() to no longer return a host side
                 address.
01d,29sep04,jmp  added vxsimBootStrategyGet().
01c,09sep04,jmp  updated vxsimHostRename() prototype (SPR #97903).
01b,03may04,dbt  Added vxsimHostIntLock() API.
01a,29may04,jeg  written.
*/

#ifndef __INCvxsimHostLibPh
#define __INCvxsimHostLibPh

extern STATUS	vxsimHostClose		(int fd);
extern STATUS	vxsimHostPassfsDirRead	(PASS_FILE_DESC * pPassFd,
					 char * pEntryName);
extern STATUS   vxsimHostPassfsDelete	(char * name);
extern int	vxsimHostErrnoGet	(void);
extern void	vxsimHostExit 		(void);
extern STATUS   vxsimHostPassfsClose	(PASS_FILE_DESC * pPassFd);
extern STATUS	vxsimHostPassfsStatGet	(PASS_FILE_DESC * pPassFd, 
					void * pVolumeDesc,
					struct statfs * pStatfs);
extern STATUS	vxsimHostFtruncate	(int fd, int length); 
extern void 	vxsimHostIfInit		(void * pHostInfo);
extern int 	vxsimHostIntLock	(void);
extern void 	vxsimHostIntUnlock	(void);
extern STATUS 	vxsimHostLseek		(int fd, int offset, int whence);
extern STATUS	vxsimHostPassfsMkdir	(char *path);
extern STATUS	vxsimHostPassfsOpen	(char *name, int flags, int mode);
extern STATUS	vxsimHostOpen		(char *name, int flags, int mode);
extern void *	vxsimHostOpendir	(char *dirname);
extern void	vxsimHostPowerDown	(void);
extern void 	vxsimHostPrintf		(const char * fmt, ...);
extern void 	vxsimHostProcNumSet	(int simLinuxDef, int smMemSize);
extern STATUS	vxsimHostRead		(int fd, char *buf, int len);
extern STATUS	vxsimHostRename		(PASS_FILE_DESC * pPassFd,
					 char *newPath);
extern STATUS 	vxsimHostReboot		(int startType);
extern STATUS	vxsimHostPassfsRmdir	(char *path);
extern STATUS 	vxsimHostPassfsFileStatGet	(PASS_FILE_DESC * pPassFd,
					void * pVolumeDesc,
					struct stat * pStat);
extern STATUS 	vxsimHostTimerInit 	(int sysVirtualTimeDef,
					int auxVirtualTimeDef);
extern void	vxsimHostTimerDisable	(int clk);
extern STATUS	vxsimHostTimerTimestampEnable	(int tsRollOverIntNum);
extern STATUS	vxsimHostTimerTimestampDisable	(void);
extern UINT32	vxsimHostTimerTimestampFreqGet	(void);
extern UINT32	vxsimHostTimerTimestampGet	(void);
extern STATUS	vxsimHostPassfsUnlink	(char *path);
extern int	vxsimHostPassfsCreate	(char * name, int flags);
extern STATUS	vxsimHostPassfsTimeSet	(PASS_FILE_DESC * pPassFd,
					 struct utimbuf * pTime);
extern STATUS	vxsimHostWrite		(int fd, char *buf, int len);
extern void	vxsimHostDbg		(char * format, ...);
extern STATUS	vxsimHostCurrentPathGet	(char * buf, int size);
extern char *	vxsimHostModel		(char * sysModelStr,
					 int sysModelStrMaxLen);
extern STATUS	vxsimHostStat		(char * path, struct stat * pStat);
extern UINT32	vxsimHostTimeGet	(void);
extern STATUS	vxsimHostBufInit	(void * pBufPool, int numBufs,
					 int bufSize);
extern STATUS	vxsimHostBufDelete	(void * pBufPool);
extern STATUS	vxsimHostSysNvRamGet	(char *	string, int strLen, int	offset,
					 int nvRamSize);
extern STATUS	vxsimHostSysNvRamSet	(char *	string, int strLen, int	offset,
					 int nvRamSize);
extern void *	vxsimHostShMemInit	(void * smMemAddr, int smMemSize,
					 int * smLevel);
extern STATUS	vxsimHostBusIntGen	(int smLevel, int smBusInt);
extern STATUS	vxsimMmuEnable		(BOOL enable,
					 MMU_TRANS_TBL * pKernelTransTbl);
extern STATUS	vxsimMmuL1DescUpdate	(BOOL mapIt, UINT32 l1Index,
					 MMU_LEVEL_1_DESC * pL1Desc);
extern STATUS	vxsimMmuL2DescUpdate	(void * effectiveAddr, PTE * pPte,
					 PTE pteVal);
extern STATUS	vxsimHostSimnetAttach	(char * subnetName, char * ipAddr,
					 int netType, void ** ppNetCfg,
					 int * pIntId);
extern void	vxsimHostSimnetDetach	(void ** ppNetCfg);
extern void	vxsimHostSimnetIntClear (int fd);
extern void	vxsimHostSimnetIntEnable (void * pNetCfg, int intId,
					  BOOL enable);
extern int	vxsimHostSimnetPhysAddrLenGet (void * ppNetCfg);
extern void	vxsimHostSimnetPhysAddrGet (void * ppNetCfg, UINT8 * enetAddr,
					    int len);
extern STATUS	vxsimHostSimnetPhysAddrSet (void * ppNetCfg, UINT8 * enetAddr,
					    int len);
extern void	vxsimHostSimnetBrdAddrGet (void * ppNetCfg, UINT8 * enetAddr,
					   int len);
extern int	vxsimHostSimnetPktReceive (void * ppNetCfg, SIMNET_PKT * pPkt);
extern SIMNET_PKT *	vxsimHostSimnetPktGet (void * ppNetCfg, int len);
extern STATUS	vxsimHostSimnetPktSend (void * ppNetCfg, SIMNET_PKT * pPkt);
extern STATUS	vxsimHostSimnetPromiscModeSet (void * pNetCfg, BOOL promisc);
extern STATUS	vxsimHostSimnetMCastModeSet (void * pNetCfg, BOOL mcast);
extern STATUS	vxsimHostSimnetParamGet (void * pNetCfg, int param, 
					 UCHAR * buf, int len);
extern STATUS	vxsimHostSimnetParamSet (void * pNetCfg, int param,
					 UCHAR * buf, int len);

extern STATUS	vxsimHostNetAddIfGet (int index, char ** ppDevice, int * pUnit,
				      char ** ppDevStr, char ** ppIpAddr,
				      char ** ppIpMask);
extern void	vxsimHostVirtualMemInfoGet	(void ** pVxsimVirtMemBaseAddr,
						 UINT32 * pVxsimVirtMemSize);

extern UINT32	vxsimBootStrategyGet	(void);
#endif	/* __INCvxsimHostLibPh */
