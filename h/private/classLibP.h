/* classLibP.h - private object class management library header file */

/*
 * Copyright (c) 2004-2005 Wind River Systems, Inc.
 *
 * The right to copy, distribute or otherwise make use of this software
 * may be licensed only pursuant to the terms of an applicable Wind River
 * license agreement. 
 */

/*
modification history
--------------------
01p,29aug05,kk   added classCreateConnect() (SPR# 106196)
01o,26oct04,fr   added classOpenConnect() routine (SPR 101491)
01n,28sep04,fr   added createRtn & openRtn and removed nameSetRtn & nameGetRtn
                 from wind_class and classInit() (SPR 101349)
01m,14may04,dcc  added CLASSID_INVALIDATE() and CLASSID_RESTORE() macros.
01l,12apr04,dcc  enabled use of the per-class mutex.
01k,11sep03,tcr  add WindView class instrumentation.
01j,26aug03,tcr  remove WindView functions no longer needed
01i,22oct03,dcc  added offsets in the OBJ_CLASS comments.
01h,21aug03,dcc  added support for public objects
01g,02jul03,to   added offset for objClassType.
01f,23jun03,dcc  removed classCreate() and classDestroy() prototypes.
01e,14mar03,dcc  ported from AE1.1 
01d,19may97,jpd  added _ASMLANGUAGE for offsets into OBJ_CLASS.
01c,10dec93,smb  added windview documentation
		 changed helpRtn to instRtn
01b,22sep92,rrr  added support for c++
01a,04jul92,jcf  created.
*/

#ifndef __INCclassLibPh
#define __INCclassLibPh

#ifdef __cplusplus
extern "C" {
#endif

/* offsets in the OBJ_CLASS structure */

#define WIND_CLASS_INST_RTN	0x1c    /* offset to instRtn */
#define WIND_CLASS_TYPE		0x38	/* offset to objClassType */

#ifndef _ASMLANGUAGE
#include <vxWorks.h>
#include <vwModNum.h>
#include <memLib.h>
#include <classLib.h>
#include <dllLib.h>
#include <private/handleLibP.h>

/* class options */

#define WIND_CLASS_ALLOW_DUPLICATE_NAMES	0x01
#define WIND_CLASS_RENAME_NOT_ALLOWED		0x02

#define WIND_CLASS_INSTRUMENTED		        0x04 

/*
 * These macros take a CLASS_ID and test/get/set the instrumentation state for
 * the class. Then any subsequent object creations will inherit the
 * instrumented state from the class.
 */
    
#define WV_OBJ_CLASS_IS_INSTRUMENTED(classId)                   \
            (classId->handle.attributes & WIND_CLASS_INSTRUMENTED)

#define WV_OBJ_CLASS_INSTRUMENTATION_ENABLE(classId)            \
            (classId->handle.attributes |= WIND_CLASS_INSTRUMENTED)
    
#define WV_OBJ_CLASS_INSTRUMENTATION_DISABLE(classId)           \
            (classId->handle.attributes &= ~WIND_CLASS_INSTRUMENTED)

/* macros to invalidate and restore the classId field */

#define CLASSID_INVALIDATE(classId)	((CLASS_ID) ((int)classId | 1))
#define CLASSID_RESTORE(classId)	((CLASS_ID) ((int)classId & ~1))
    

typedef struct wind_class	/* OBJ_CLASS */
    {
    HANDLE		handle;		/* 0x00: handle management */
    unsigned		objSize;	/* 0x0c: size of object */
    CLASS_ID		altClassId;     /* 0x10: alternate class ID */
    FUNCPTR		destroyRtn;	/* 0x14: object destroy routine */
    FUNCPTR		showRtn;	/* 0x18: object show routine */
    VOIDFUNCPTR		instRtn;	/* 0x1c: object inst routine */
    FUNCPTR   	        createRtn;  	/* 0x20: object create routine */
    FUNCPTR   		openRtn;  	/* 0x24: object open routine */
    FUNCPTR 		objMemAllocRtn; /* 0x28: object memory alloc routine */
    FUNCPTR 		objMemFreeRtn;  /* 0x2c: object memory free routine */
    FUNCPTR		initRtn;        /* 0x30: for backwards compatibility 
				           with windView instrumentation. */  
    struct mem_part*	objPartId;      /* 0x34: object memory pool ID */
    enum windObjClassType objClassType;	/* 0x38: enum value for class type */
    int			options;	/* 0x3c: options */
    DL_LIST		objPrivList;    /* 0x40: list of private objects */
    DL_LIST	        objPubList;     /* 0x48: list of public objects */
    SEM_ID		pClassSem;      /* 0x50: lists' mutual exclusion */
    } OBJ_CLASS;


/* variable declarations */

extern CLASS_ID	classIdTable [];	/* array of class IDs */
extern FUNCPTR	_func_classListLock;
extern FUNCPTR	_func_classListUnlock;

/* function declarations */

extern STATUS	classInit	(OBJ_CLASS *pObjClass, unsigned objectSize,
				 FUNCPTR destroyRtn, FUNCPTR createRtn,
				 FUNCPTR openRtn, FUNCPTR objMemAllocRtn,
				 FUNCPTR objMemFreeRtn, void *objMemPoolId,
				 enum windObjClassType objClassType,
				 int options);
extern STATUS	classCreateConnect(CLASS_ID classId, FUNCPTR createRtn);
extern STATUS	classOpenConnect(CLASS_ID classId, FUNCPTR openRtn);
extern STATUS	classShowConnect(CLASS_ID classId, FUNCPTR showRtn);

#endif  /* _ASMLANGUAGE */

#ifdef __cplusplus
}
#endif

#endif /* __INCclassLibPh */
