/* workQLibP.h - private kernel work queue header file */

/* Copyright 1984-2004 Wind River Systems, Inc. */

/*
modification history
--------------------
01e,03may04,cjj  Mods for code inspection of version 01d.
		 Removed useless WORK_JOB_BASE.
01d,21apr04,cjj  Mods for configurable workQ size
01c,18dec00,pes  Correct compiler warnings
01b,22sep92,rrr  added support for c++
01a,04jul92,jcf  extracted from v2i of wind.h.
*/

#ifndef __INCworkQLibPh
#define __INCworkQLibPh

#ifdef __cplusplus
extern "C" {
#endif

#ifndef	_ASMLANGUAGE
#include "vxWorks.h"
#include "vwModNum.h"

/* typedefs */

typedef struct		/* JOB */
    {
    FUNCPTR 	function;	/* 00: function to invoke */
    int		arg1;		/* 04: argument 1 */
    int 	arg2;		/* 08: argument 2 */
    UINT 	isrTag;		/* 12: tag of ISR that queued up work */
    } JOB;

/* 
 * Having the workQ indices in a struct increases the chances of them
 * being all in the same cache line hence improving kernel performance
 * at a critical time.  The bitmask member is always used whenever one
 * or the other index is used so it is located between them to ensure
 * it will at least be in the same cache line as one of them.
 */
typedef struct
    {
    volatile    UINT16 read;	/* workQ read index */
    UINT16 	bitmask;	/* workQ index bitmask */
    volatile    UINT16 write;   /* workQ write index */
    UINT16 unused;		/* padding */
    } WORK_Q_IX;

/* variable declarations */

extern WORK_Q_IX	workQIx;	/* workQ indices struct */
extern JOB 		pJobPool[];	/* pool of memory for jobs */
extern volatile BOOL    workQIsEmpty;	/* TRUE if work queue is empty */
extern FUNCPTR          workQPanicHook; /* undocumented hook for */
					/* testing/debugging */

/* function declarations */

extern void	workQInit (UINT queueSize);
extern void	workQDoWork (void);
extern void	workQPanic (void);
extern void	workQAdd0 (FUNCPTR func);
extern void	workQAdd1 (FUNCPTR func, int arg1);
extern void	workQAdd2 (FUNCPTR func, int arg1, int arg2);

#else	/* _ASMLANGUAGE */

#define JOB_FUNCPTR	0x0		/* routine to invoke */
#define JOB_ARG1	0x4		/* argument 1 */
#define JOB_ARG2	0x8		/* argument 2 */
#define JOB_ISR_TAG	0xc		/* tag of ISR that queued up work */

#define WORK_READ_IX	0x0		/* work queue read index */
#define WORK_BITMASK	0x2		/* work queue index bitmask */
#define WORK_WRITE_IX	0x4		/* work queue write index */

#endif	/* _ASMLANGUAGE */

#ifdef __cplusplus
}
#endif

#endif /* __INCworkQLibPh */
