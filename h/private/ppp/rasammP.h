/* 
 * rasammP.h - Private Remote Access address manager header file 
 */

/* 
 * Copyright 2003 Wind River Systems, Inc. 
 */

/*
modification history
--------------------
01a,15may03,ijm removed adv_net.h
*/


#ifndef _RAS_AMM_H
#define _RAS_AMM_H

#ifdef _cplusplus
extern "C" {
#endif

#include "string.h"

#ifdef _DEBUG_STANDALONE
#include <stdio.h>
#include <malloc.h>	  
#include <winsock.h>

typedef unsigned char UINT8;
typedef unsigned int  UINT;
typedef unsigned long UINT32;
typedef char          INT8;
typedef int           INT;
typedef long          INT32;
typedef int           STATUS;

#define ERROR         -1
#define OK            0

#define SEM_ID            UINT32
#define SEM_Q_PRIORITY
#define SEM_FULL
#define WAIT_FOREVER

#define FAST		  register
#define LOCAL             static 

#define intLock()	  1
#define intUnlock(x)

#define semBCreate(x,y)   1
#define semTake(x,y)
#define semGive(x)
#define errnoSet(x)
#define errnoGet()        -1

#else	/* end of _DEBUG */
#include "stdio.h"
#include "stdlib.h"
#include "semLib.h"
#include "errnoLib.h"
#include "inetLib.h"
#endif

#define RAS_ADDRM_FASTS_POOL  1   /* very fast lookup */
#define RAS_ADDRM_SMALS_POOL  2   /* very small in storage */

#define IPV4_ADDR_BITS        0xFFFFFFFF
#define IPV4_ADDR_LEN         4

#define ADDRPOOL_DEL_INPGRES  1

#define RASAMM_MODULE_ID      (130 << 16)
#define ERASAMMDELINPGRES     (RASAMM_MODULE_ID | 1)
#define ERASAMMINVALIDPARM    (RASAMM_MODULE_ID | 2)
#define ERASAMMNOMEMORY       (RASAMM_MODULE_ID | 3)
#define ERASAMMINVLDCOMMAND   (RASAMM_MODULE_ID | 4)
#define ERASAMMNOPMPLKUPTBL   (RASAMM_MODULE_ID | 5)


typedef struct ras_pmp_mux_entry
{
     struct ras_pmp_mux_entry *next;
     void *drv;
     UINT32 srcipAddr;
     UINT32 dstipAddr;
} RAS_PMP_MUX_ENTRY;

typedef struct addrpool_entry {
     struct addrpool_entry *next;
     UINT32 routerId;
     UINT32 firstIpAddr;
     UINT   prefixLen;
     UINT32 poolSz;
     UINT32 poolId;
     UINT   poolType;
     UINT32 poolState;
     UINT32 allocated;
     UINT32 firstFree;
     union {
         UINT32 *faddrPool;
         UINT8  *saddrPool;
     } addrPool;
#define faddrPool  addrPool.faddrPool
#define saddrPool  addrPool.saddrPool
     void *exportedDrv;
     UINT numDrvs;
     UINT supportPMP;
     UINT lkupTblSz;
     RAS_PMP_MUX_ENTRY **lkupTbl;
} ADDR_POOL_ENTRY;


/*
 * IOCTL commands
 */
#define RASIOCTLCOMMANDBASE        0  /* might need to change for conformity */
#define RASIOCGETMAXPOOLID         (RASIOCTLCOMMANDBASE + 1)
#define RASIOCGETTOTALNUMPOOLS	   (RASIOCTLCOMMANDBASE + 2)
#define RASIOCGETADDRPOOLSZ        (RASIOCTLCOMMANDBASE + 3)
#define RASIOCGETIPADDRPREFIX	   (RASIOCTLCOMMANDBASE + 4)
#define RASIOCGETROUTERID	   (RASIOCTLCOMMANDBASE + 5)
#define RASIOCGETFIRSTIPADDR	   (RASIOCTLCOMMANDBASE + 6)
#define RASIOCGETNUMFREEADDRS	   (RASIOCTLCOMMANDBASE + 7)
#define RASIOCGETGETFREEADDRS	   (RASIOCTLCOMMANDBASE + 8)
#define RASIOCGETPMPSUPPORT	   (RASIOCTLCOMMANDBASE + 9)
#define RASIOCGETEXPTDDRIVER	   (RASIOCTLCOMMANDBASE + 10)
#define RASIOCGETNUMDRIVERS	   (RASIOCTLCOMMANDBASE + 11)
#define RASIOCSETEXPTDDRIVER	   (RASIOCTLCOMMANDBASE + 12)

typedef struct _rasioctlarg
{
	UINT32 	poolId;
	union {
	    UINT32  poolSz;
	    UINT32  routerId;
	    UINT32  firstIp;
	    UINT32  maxPoolId;
	    UINT32  numPools;
	    UINT32  prefxLen;
	    UINT32  numFreeAddr;
	    UINT32  *freeAddrs;
            UINT    supportPMP;
            void    *exportedDrv;
            UINT    numDrvs;
	} info;
} RASAMM_IOCTL_ARG;

#ifdef _cplusplus
}
#endif

#endif
