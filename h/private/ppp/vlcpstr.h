/* vlcpstr.h - LCP structure definitions */

/* Copyright 1999 Wind River Systems, Inc. */

#include "copyright_wrs.h"

/*
modification history
--------------------
01e,28may02,rvr fixed the structure MAGIC_NUMBER_OPTION to remove extra
                _WRS_PACK_ALIGN(x)
01d,16dec01,mk  added macro _WRS_PACK_ALIGN(x) 
01c,07dec01,mk  added macros _WRS_PACK_ALIGN(x),to fix alignment
                problems for PPP for arm
01f,29sep00,sj  merging with TOR2_0-WINDNET_PPP-CUM_PATCH_2
01e,01aug00,adb  Merging with openstack view
01d,11jul00,md  added AUTHENTICATION_ATTRIBUTES structure
01c,10jan00,sj  changed magic in echo packets to a byte array(CSCO VPN WS find)
01b,10nov99,sj  include vpppstr.h
01a,28sep99,sj 	derived from routerware source base
*/
/*
*$Log:: /Rtrware/devdrvrs/ppp/vlcpstr $
 * 
 * 7     10/01/98 11:43a Alex
 * Updated the PPP source code to conform
 * to a single build. 
 * 
 * 6     4/30/98 3:03p Alex
 * Ppp v4.2.0 check in
 * 
 * 1     4/24/98 12:10a Release Engineer
 * code cleanup, code style changes,
 * linted, system level test
 * PPP v4.2.0
*/

#ifndef __INCvlcpstrh
#define __INCvlcpstrh

#ifdef __cplusplus
extern "C" {
#endif

#include "ppp/kstart.h"
#include "ppp/kppp.h"
#include "private/ppp/vpppstr.h"
#include "pfw/pfw.h"

typedef enum
    {
    RESERVED,
    USER_NAME_PASSWORD_EXCHANGE,
    PPP_CHAP,
    PPP_PAP,
    NO_AUTHENTICATION,
    MS_CHAP_VER_1,
    NUMBER_OF_PROXY_AUTHEN
    } PROXY_AUTHEN_TYPE;

typedef struct AUTHENTICATION_ATTRIBUTES
    {
    UINT8                           proxyAuthenID;
    BYTE_ENUM(PROXY_AUTHEN_TYPE)	proxyAuthenType;
    UINT8						    proxyAuthenNameLen;
    char						    proxyAuthenName[NAME_SIZE];
    UINT8						    proxyAuthenChallengeLen;
    char						    proxyAuthenChallenge[CHALLENGE_VALUE_SIZE];
    UINT8						    proxyAuthenResponseLen;
    char						    proxyAuthenResponse[RESPONSE_VALUE_SIZE];
    UINT8						    proxyAuthenPasswordLen;
    char						    proxyAuthenPassword[NAME_SIZE];
    } AUTHENTICATION_ATTRIBUTES;

typedef enum 
    {
    LCP_MAXIMUM_RECEIVE_UNIT                            = 0x01,
    LCP_ASYNC_CONTROL_CHARACTER_MAP                     = 0x02,
    LCP_AUTHENTICATION_PROTOCOL                         = 0x03,
    LCP_QUALITY_PROTOCOL                                = 0x04,
    LCP_MAGIC_NUMBER                                    = 0x05,
    LCP_PROTOCOL_FIELD_COMPRESSION                      = 0x07,
    LCP_ADDRESS_AND_CONTROL_FIELD_COMPRESSION           = 0x08,
    LCP_FCS_ALTERNATIVES                                = 0x09,
    LCP_SELF_DESCRIBING_PADDING                         = 0x0a,
    LCP_NUMBERED_MODE                                   = 0x0b,
    LCP_MULTILINK_PROCEDURE                             = 0x0c,
    LCP_CALLBACK                                        = 0x0d,
    LCP_CONNECT_TIME                                    = 0x0e,
    LCP_COMPOUND_FRAMES                                 = 0x0f,
    LCP_NOMINAL_DATA_ENCAPSULATION                      = 0x10,
    LCP_MULTILINK_MAXIMUM_RECEIVED_RECONSTRUCTED_UNIT   = 0x11,
    LCP_MULTILINK_SHORT_SEQUENCE_NUMBER_HEADER_FORMAT   = 0x12,
    LCP_ENDPOINT_DISCRIMINATOR                          = 0x13, 
#define BACP_LCP_LINK_DISCRIMINATOR_LENGTH 2 /* length of discriminator only */
    LCP_LINK_DISCRIMINATOR                              = 0x17, 
    NUMBER_OF_LCP_OPTIONS
    }LCP_OPTION_TYPE;

typedef	struct PROTOCOL_FIELD_COMPRESSION
    {
    BYTE_ENUM (LCP_OPTION_TYPE) type;
    USHORT                      length;
    } PROTOCOL_FIELD_COMPRESSION;

typedef	struct ADDRESS_AND_CONTROL_FIELD_COMPRESSION
    {
    BYTE_ENUM (LCP_OPTION_TYPE) type;
    USHORT                      length;
    } ADDRESS_AND_CONTROL_FIELD_COMPRESSION;

typedef	struct PPP_LCP_STATISTICS
    {
    ULONG number_of_tx_packets;
    ULONG number_of_tx_bytes;
    ULONG number_of_rx_packets;
    ULONG number_of_rx_bytes;
    ULONG number_of_packets_rxed_greater_than_maximum_size;
    ULONG number_of_packets_rxed_less_than_minimum_size;
    ULONG number_of_control_tx_packets[NUMBER_OF_PPP_CONTROL_CODES];
    ULONG number_of_control_rx_packets[NUMBER_OF_PPP_CONTROL_CODES];
    } PPP_LCP_STATISTICS;

typedef struct LINK_QUALITY_REPORT
    {
    PPP_HEADER header;
    ULONG      magic_number;
    ULONG      LastOutLQRs;
    ULONG      LastOutPackets;
    ULONG      LastOutOctets;
    ULONG      PeerInLQRs;
    ULONG      PeerInPackets;
    ULONG      PeerInDiscards;
    ULONG      PeerInErrors;
    ULONG      PeerInOctets;
    ULONG      PeerOutLQRs;
    ULONG      PeerOutPackets;
    ULONG      PeerOutOctets;
    ULONG      SaveInLQRs;
    ULONG      SaveInPackets;
    ULONG      SaveInDiscards;
    ULONG      SaveInErrors;
    ULONG      SaveInOctets;
    } _WRS_PACK_ALIGN(1)  LINK_QUALITY_REPORT;

typedef	struct PPP_LCP_MIBS
    {
    ULONG pppLinkStatusPhysicalIndex;
    ULONG pppLinkStatusBadAddresses;
    ULONG pppLinkStatusBadControls;
    ULONG pppLinkStatusPacketTooLongs;
    ULONG pppLinkStatusBadFCSs;
    ULONG pppLinkStatusLocalMRU;
    ULONG pppLinkStatusRemoteMRU;
    ULONG pppLinkStatusLocalToPeerACCMap;
    ULONG pppLinkStatusPeerToLocalACCMap;
    ULONG pppLinkStatusLocalToRemoteProtocolCompression;
    ULONG pppLinkStatusRemoteToLocalProtocolCompression;
    ULONG pppLinkStatusLocalToRemoteACCompression;
    ULONG pppLinkStatusRemoteToLocalACCompression;
    ULONG pppLinkStatusTransmitFcsSize;
    ULONG pppLinkStatusReceiveFcsSize;
    ULONG pppRemoteAuthenticationProtocol;
    ULONG pppLocalAuthenticationProtocol;
    ULONG pppLinkConfigInitialMRU;
    ULONG pppLinkConfigReceiveACCMap;
    ULONG pppLinkConfigTransmitACCMap;
    ULONG pppLinkConfigMagicNumber;
    ULONG pppLinkConfigFcsSize;
    ULONG pppLqrQuality;
    ULONG pppLqrInGoodOctets;
    ULONG pppLqrLocalPeriod;
    ULONG pppLqrRemotePeriod;
    ULONG pppLqrOutLQRs;
    ULONG pppLqrInLQRs;
    ULONG pppLqrConfigPeriod;
    ULONG pppLqrConfigStatus;

    LINK_QUALITY_REPORT pppLqrExtnsLastReceivedLqrPacket;
    } PPP_LCP_MIBS;

typedef	struct PROTOCOL_REJECT_HEADER
    {
    USHORT_ENUM (PPP_PROTOCOL_TYPE) protocol;
    BYTE                            information[MAXIMUM_PACKET_LENGTH];
    } _WRS_PACK_ALIGN(1)  PROTOCOL_REJECT_HEADER;

typedef struct MRU_OPTION
    {
    BYTE_ENUM (LCP_OPTION_TYPE) type;
    BYTE                        length;
    USHORT                      maximum_receive_unit;
    } _WRS_PACK_ALIGN(1)  MRU_OPTION;

typedef struct ASYNC_CONTROL_CHARACTER_MAP_OPTION
    {
    BYTE_ENUM (LCP_OPTION_TYPE) type;
    BYTE                        length;
    ULONG                       async_control_character_map;
    } _WRS_PACK_ALIGN(1)  ASYNC_CONTROL_CHARACTER_MAP_OPTION;

typedef struct AUTHENTICATION_OPTION
    {
    BYTE_ENUM (LCP_OPTION_TYPE)          type;
    BYTE                                 length;
    USHORT_ENUM (PPP_PROTOCOL_TYPE)      authentication_protocol;
    BYTE_ENUM (AUTHENTICATION_ALGORITHM) algorithm;
    } _WRS_PACK_ALIGN(1)  AUTHENTICATION_OPTION;

typedef struct ENCRYPTION_OPTION
    {
    BYTE_ENUM (LCP_OPTION_TYPE)     type;
    BYTE                            length;
    USHORT_ENUM (PPP_PROTOCOL_TYPE) encryption_protocol;
    } _WRS_PACK_ALIGN(1)  ENCRYPTION_OPTION;

typedef	struct MAGIC_NUMBER_OPTION
    {
    BYTE_ENUM (LCP_OPTION_TYPE)	type;
    BYTE                        length;
    ULONG                       magic_number;
    } _WRS_PACK_ALIGN(1)  MAGIC_NUMBER_OPTION;

typedef struct LINK_QUALITY_OPTION
    {
    BYTE_ENUM (LCP_OPTION_TYPE)     type;
    BYTE                            length;
    USHORT_ENUM (PPP_PROTOCOL_TYPE) quality_protocol;
    ULONG                           reporting_time;
    } _WRS_PACK_ALIGN(1)  LINK_QUALITY_OPTION;

typedef struct PROTOCOL_FIELD_COMPRESSION_OPTION
    {
    BYTE_ENUM (LCP_OPTION_TYPE) type;
    BYTE                        length;
    } _WRS_PACK_ALIGN(1)  PROTOCOL_FIELD_COMPRESSION_OPTION;

typedef struct AC_FIELD_COMPRESSION_OPTION
    {
    BYTE_ENUM (LCP_OPTION_TYPE)	type;
    BYTE                        length;
    } _WRS_PACK_ALIGN(1)  AC_FIELD_COMPRESSION_OPTION;

typedef	struct	LCP_OPTIONS_RECEIVED
    {
    MRU_OPTION MRU_option;
    ASYNC_CONTROL_CHARACTER_MAP_OPTION async_control_character_map_option;
    AUTHENTICATION_OPTION              authentication_option;
    ENCRYPTION_OPTION                  encryption_option;
    MAGIC_NUMBER_OPTION                magic_number_option;
    LINK_QUALITY_OPTION	               link_quality_option;
    PROTOCOL_FIELD_COMPRESSION_OPTION  protocol_field_compression_option;
    AC_FIELD_COMPRESSION_OPTION        address_and_control_field_compression_option;

    } LCP_OPTIONS_RECEIVED;

typedef	struct GENERIC_OPTION
    {
    BYTE type;
    BYTE length;

    BYTE data[VARIABLE_NUMBER_OF_BYTES];					
    } _WRS_PACK_ALIGN(1)  GENERIC_OPTION;

typedef	union UNION_LCP_OPTIONS
    {
    MRU_OPTION MRU_option;
    ASYNC_CONTROL_CHARACTER_MAP_OPTION async_control_character_map_option;
    AUTHENTICATION_OPTION              authentication_option;
    ENCRYPTION_OPTION                  encryption_option;
    MAGIC_NUMBER_OPTION                magic_number_option;
    LINK_QUALITY_OPTION                link_quality_option;
    PROTOCOL_FIELD_COMPRESSION_OPTION  protocol_field_compression_option;
    AC_FIELD_COMPRESSION_OPTION        address_and_control_field_compression_option;

    }_WRS_PACK_ALIGN(1) UNION_LCP_OPTIONS;

typedef	struct LCP_HEADER
    {
    BYTE_ENUM (PPP_CONTROL_CODE) code;
    BYTE                         id;
    USHORT                       length;
    } _WRS_PACK_ALIGN(1)  LCP_HEADER;

typedef	struct LCP_PACKET
    {
    PPP_HEADER        ppp_header;
    LCP_HEADER        lcp_header;
    /* UNION_LCP_OPTIONS options;*/
    GENERIC_OPTION    options;
    } _WRS_PACK_ALIGN(1)  LCP_PACKET;

typedef void (*FPTR_LCP_PACKET_RECEIVED)
		(
		PFW_PLUGIN_OBJ_STATE * pluginState,
		M_BLK_ID packet
		);

typedef	struct LCP_CONFIGURE_REQUEST
    {
    PPP_HEADER                         header;
    BYTE_ENUM (PPP_CONTROL_CODE)       code;
    BYTE                               id;
    USHORT                             length;
    MRU_OPTION                         MRU_option;
    ASYNC_CONTROL_CHARACTER_MAP_OPTION async_control_character_map_option;
    AUTHENTICATION_OPTION              authentication_option;
    ENCRYPTION_OPTION                  encryption_option;
    MAGIC_NUMBER_OPTION                magic_number_option;
    LINK_QUALITY_OPTION                link_quality_option;
    PROTOCOL_FIELD_COMPRESSION_OPTION  protocol_field_compression_option;
    AC_FIELD_COMPRESSION_OPTION        address_and_control_field_compression_option;

    } _WRS_PACK_ALIGN(1)  LCP_CONFIGURE_REQUEST;

typedef	struct LCP_CONFIGURE_REQUEST_PACKET
    {
    PPP_HEADER                   header;
    BYTE_ENUM (PPP_CONTROL_CODE) code;
    BYTE                         id;
    USHORT                       length;
    /* UNION_LCP_OPTIONS            options;*/
    GENERIC_OPTION               options;
    } _WRS_PACK_ALIGN(1)  LCP_CONFIGURE_REQUEST_PACKET;

typedef	struct CONFIGURE_ACK_PACKET
    {
    PPP_HEADER                   header;
    BYTE_ENUM (PPP_CONTROL_CODE) code;
    BYTE                         id;
    USHORT                       length;
    /* UNION_LCP_OPTIONS         union_lcp_options[VARIABLE_NUMBER_OF_BYTES];*/
    GENERIC_OPTION               options;
    } _WRS_PACK_ALIGN(1)  CONFIGURE_ACK_PACKET;

typedef	struct CONFIGURE_NAK_PACKET
    {
    PPP_HEADER                   header;
    BYTE_ENUM (PPP_CONTROL_CODE) code;
    BYTE                         id;
    USHORT                       length;
    /* UNION_LCP_OPTIONS            options;*/
    GENERIC_OPTION               options;
    } _WRS_PACK_ALIGN(1)  CONFIGURE_NAK_PACKET;

typedef	struct CONFIGURE_REJECT_PACKET
    {
    PPP_HEADER                   header;
    BYTE_ENUM (PPP_CONTROL_CODE) code;
    BYTE                         id;
    USHORT                       length;
    /* UNION_LCP_OPTIONS            options;*/
    GENERIC_OPTION               options;
    } _WRS_PACK_ALIGN(1)  CONFIGURE_REJECT_PACKET;

typedef	struct TERMINATE_REQUEST
    {
    PPP_HEADER                   header;
    BYTE_ENUM (PPP_CONTROL_CODE) code;
    BYTE                         id;
    USHORT                       length;
    } _WRS_PACK_ALIGN(1)  TERMINATE_REQUEST;

typedef	struct TERMINATE_ACK
    {
    PPP_HEADER                   header;
    BYTE_ENUM (PPP_CONTROL_CODE) code;
    BYTE                         id;
    USHORT                       length;
    } _WRS_PACK_ALIGN(1)  TERMINATE_ACK;

typedef	struct CODE_REJECT_PACKET
    {
    PPP_HEADER                   header;
    BYTE_ENUM (PPP_CONTROL_CODE) code;
    BYTE                         id;
    USHORT                       length;
    LCP_PACKET                   rejected_packet;
    } _WRS_PACK_ALIGN(1)  CODE_REJECT_PACKET;

typedef	struct PROTOCOL_REJECT_PACKET
    {
    PPP_HEADER                   header;
    BYTE_ENUM (PPP_CONTROL_CODE) code;
    BYTE                         id;
    USHORT                       length;
    PROTOCOL_REJECT_HEADER       rejected_protocol_and_information;
    } _WRS_PACK_ALIGN(1)  PROTOCOL_REJECT_PACKET;

typedef	struct ECHO_REQUEST_PACKET
    {
    PPP_HEADER                   header;
    BYTE_ENUM (PPP_CONTROL_CODE) code;
    BYTE                         id;
    USHORT                       length;
    /*ULONG                        magic_number;*/
    BYTE                         magic_number[4];
    } _WRS_PACK_ALIGN(1)  ECHO_REQUEST_PACKET;

typedef	struct	ECHO_REPLY_PACKET
    {
    PPP_HEADER                   header;
    BYTE_ENUM (PPP_CONTROL_CODE) code;
    BYTE                         id;
    USHORT                       length;
    /*ULONG                        magic_number;*/
    BYTE                         magic_number[4];
    } _WRS_PACK_ALIGN(1)  ECHO_REPLY_PACKET;

typedef	struct 	DISCARD_REQUEST_PACKET
    {
    PPP_HEADER                    header;
    BYTE_ENUM (PPP_CONTROL_CODE) code;
    BYTE                         id;
    USHORT                       length;
    ULONG                        magic_number;
    } _WRS_PACK_ALIGN(1)  DISCARD_REQUEST_PACKET;

typedef struct LINK_QUALITY_COUNTERS
    {
    ULONG OutLQRs;
    ULONG InLQRs;
    ULONG InGoodOctets;
    ULONG ifInOctets;
    ULONG ifInErrors;
    ULONG ifInDiscards;
    ULONG ifOutOctets;
    ULONG ifOutUniPackets;
    ULONG ifInUniPackets;
    } LINK_QUALITY_COUNTERS;

typedef struct IDENTIFICATION_PACKET
    {
    PPP_HEADER                   header;
    BYTE_ENUM (PPP_CONTROL_CODE) code;
    BYTE                         id;
    USHORT                       length;
    ULONG                        magic_number;
    char                         message[VARIABLE_NUMBER_OF_BYTES]; 
    } _WRS_PACK_ALIGN(1)  IDENTIFICATION_PACKET;

typedef struct TIME_REMAINING_PACKET
    {
    PPP_HEADER                   header;
    BYTE_ENUM (PPP_CONTROL_CODE) code;
    BYTE                         id;
    USHORT                       length;
    ULONG                        magic_number;
    ULONG                        seconds_remaining;
    char                         message[VARIABLE_NUMBER_OF_BYTES];
    } _WRS_PACK_ALIGN(1)  TIME_REMAINING_PACKET;

typedef	struct CALLBACK_PACKET
    {
    PPP_HEADER                     header;
    BYTE_ENUM (PPP_CONTROL_CODE)   code;
    BYTE                           id;
    USHORT                         length;
    BYTE_ENUM (CALLBACK_OPERATION) operation;
    char                           message[VARIABLE_NUMBER_OF_BYTES];
    } _WRS_PACK_ALIGN(1)  CALLBACK_PACKET;

#define LCP_PACKET_HEADER_SIZE (sizeof(PPP_HEADER) + sizeof(LCP_HEADER))

/* lcptimer.c */

extern STATUS retry_lcp_configure_request (PFW_PLUGIN_OBJ_STATE * ,int backOff);
extern STATUS retry_lcp_termination_request (PFW_PLUGIN_OBJ_STATE *,int arg);
extern STATUS periodic_lcp_echo_request (PFW_PLUGIN_OBJ_STATE * , int arg);

extern void send_lcp_time_remaining_packet (PFW_PLUGIN_OBJ_STATE * pluginState);

extern void send_lcp_link_quality_report (PFW_PLUGIN_OBJ_STATE * pluginState);
extern void send_lcp_id_packet (PFW_PLUGIN_OBJ_STATE * pluginState);

/* ppplcprx.c */

extern TEST lcp_packet_received (PFW_PLUGIN_OBJ_STATE * pluginState,
							    M_BLK_ID packet);

#ifdef __cplusplus
}
#endif

#endif /* __INCvlcpstrh */
