/* vnipstr.h - IPCP definitions file */

/* Copyright (C) 1999-2003 Wind River Systems, Inc. */

#include "copyright_wrs.h"

/*
modification history
--------------------
01d,22jul03,rp adding/updating copyright notice
01c,20dec01,mk added macros _WRS_PACK_ALIGN(x)
01b,07dec01,mk added macros _WRS_PACK_ALIGN(x),to fix alignment
                problems for PPP for arm
01a,12oct99,sgv derived from routerware source base
*/

#ifndef __INCvnipstrh
#define __INCvnipstrh

#ifdef __cplusplus
extern "C" {
#endif


/*
*$Log:: /Rtrware/devdrvrs/ppp/vnipstr $
 * 
 * 7     10/01/98 11:43a Alex
 * Updated the PPP source code to conform
 * to a single build. 
 * 
 * 6     4/30/98 3:03p Alex
 * Ppp v4.2.0 check in
 * 
 * 1     4/24/98 12:10a Release Engineer
 * code cleanup, code style changes,
 * linted, system level test
 * PPP v4.2.0
*/

typedef	struct	IP_OPTIONS_BOOLEAN
{
	BYTE_ENUM (BOOLEAN)	compression;
	BYTE_ENUM (BOOLEAN) 	address;
}	IP_OPTIONS_BOOLEAN;

typedef	struct	PPP_IP_CONFIGURATION
{
	IP_OPTIONS_BOOLEAN					enabled;

	IP_OPTIONS_BOOLEAN					remote_enabled;

	USHORT_ENUM (PPP_PROTOCOL_TYPE)	compression_protocol;

	BYTE										maximum_slot_id;	
	BYTE										compression_slot_id;

	USHORT_ENUM (PPP_PROTOCOL_TYPE)	remote_compression_protocol;

	ULONG				 						my_ip_address;
	ULONG				 						remote_ip_address;
} PPP_IP_CONFIGURATION;

typedef	struct	VAN_JACOBSON_HEADER_COMPRESSION_OPTION
{
	BYTE_ENUM (IPCP_OPTION_TYPE)			type;
	USHORT										length;

	USHORT_ENUM (PPP_PROTOCOL_TYPE)		ip_compression_protocol;
	BYTE											maximum_slot_id;
	BYTE											compressed_slot_id;
} _WRS_PACK_ALIGN(1)  VAN_JACOBSON_HEADER_COMPRESSION_OPTION;

typedef	struct	PPP_IPCP_MIBS
{
	ULONG	pppIpOperStatus;
	ULONG	pppIpLocalToRemoteCompressionProtocol;
	ULONG	pppIpRemoteToLocalCompressionProtocol;
	ULONG	pppIpRemoteMaxSlotId;
	ULONG	pppIpLocalMaxSlotId;

	ULONG	pppIpConfigAdminStatus;
	ULONG	pppIpConfigCompression;
} PPP_IPCP_MIBS;

typedef	struct IP_COMPRESSION_OPTION
{
	BYTE_ENUM (IPCP_OPTION_TYPE)	type;
	BYTE							length;
	USHORT_ENUM (PPP_PROTOCOL_TYPE)	ip_compression_protocol;
} _WRS_PACK_ALIGN(1)  IP_COMPRESSION_OPTION;

typedef	struct IP_ADDRESS_OPTION
{
	BYTE_ENUM (IPCP_OPTION_TYPE)	type;
	BYTE							length;
	ULONG							ip_address;
} _WRS_PACK_ALIGN(1)  IP_ADDRESS_OPTION;

typedef	struct	GENERIC_IPCP_OPTION
{
	BYTE_ENUM (IPCP_OPTION_TYPE)	type;
	BYTE							length;
} _WRS_PACK_ALIGN(1)  GENERIC_IPCP_OPTION;

typedef	union	UNION_IPCP_OPTIONS
{
	GENERIC_IPCP_OPTION							generic;
	IP_COMPRESSION_OPTION						ip_compression_option;
	IP_ADDRESS_OPTION								ip_address_option;
	VAN_JACOBSON_HEADER_COMPRESSION_OPTION	vj_compression_option;
}_WRS_PACK_ALIGN(1) UNION_IPCP_OPTIONS;

typedef	struct	IPCP_OPTIONS
{
	IP_COMPRESSION_OPTION	compression;
	IP_ADDRESS_OPTION			address;
} _WRS_PACK_ALIGN(1) 	IPCP_OPTIONS;

typedef	struct	IPCP_CONFIGURE_REQUEST
{
	PPP_HEADER							header;

	BYTE_ENUM (PPP_CONTROL_CODE)	code;
	BYTE									id;
	USHORT 								length;

	UNION_IPCP_OPTIONS				union_ipcp_options[VARIABLE_NUMBER];
} _WRS_PACK_ALIGN(1)  IPCP_CONFIGURE_REQUEST;

typedef	struct	IPCP_CONFIGURE_ACK
{
	PPP_HEADER							header;

	BYTE_ENUM (PPP_CONTROL_CODE)	code;
	BYTE									id;
	USHORT 								length;

	UNION_IPCP_OPTIONS				union_ipcp_options[VARIABLE_NUMBER];
} _WRS_PACK_ALIGN(1)  IPCP_CONFIGURE_ACK;

typedef	struct	IPCP_CONFIGURE_NAK
{
	PPP_HEADER							header;

	BYTE_ENUM (PPP_CONTROL_CODE)	code;
	BYTE									id;
	USHORT 								length;

	UNION_IPCP_OPTIONS				union_ipcp_options[VARIABLE_NUMBER];
} _WRS_PACK_ALIGN(1)  IPCP_CONFIGURE_NAK;

typedef	struct	IPCP_CONFIGURATION_REJECT
{
	PPP_HEADER							header;

	BYTE_ENUM (PPP_CONTROL_CODE)	code;
	BYTE									id;
	USHORT 								length;

	UNION_IPCP_OPTIONS				union_ipcp_options[VARIABLE_NUMBER];
} _WRS_PACK_ALIGN(1)  IPCP_CONFIGURATION_REJECT;

typedef	struct	IPCP_TERMINATE_REQUEST
{
	PPP_HEADER							header;

	BYTE_ENUM (PPP_CONTROL_CODE)	code;
	BYTE									id;
	USHORT 								length;
} _WRS_PACK_ALIGN(1)  IPCP_TERMINATE_REQUEST;

typedef	struct	IPCP_TERMINATE_ACK
{
	PPP_HEADER							header;

	BYTE_ENUM (PPP_CONTROL_CODE)	code;
	BYTE									id;
	USHORT 								length;
} _WRS_PACK_ALIGN(1)  IPCP_TERMINATE_ACK;

typedef	struct	IPCP_CODE_REJECT_PACKET
{
	PPP_HEADER							header;

	BYTE_ENUM (PPP_CONTROL_CODE)	code;
	BYTE									id;
	USHORT 								length;
	BYTE  								rejected_packet[VARIABLE_NUMBER_OF_BYTES];

} _WRS_PACK_ALIGN(1)  IPCP_CODE_REJECT_PACKET;

typedef	struct	IPCP_PACKET
{
	PPP_HEADER							header;

	BYTE_ENUM (PPP_CONTROL_CODE)	code;
	BYTE									id;
	USHORT 								length;

	UNION_IPCP_OPTIONS 				options;
} _WRS_PACK_ALIGN(1)  IPCP_PACKET;

typedef	union	UNION_IPCP_PACKET
{
	IPCP_PACKET						generic;
	IPCP_CONFIGURE_REQUEST		configure_request;
	IPCP_CONFIGURE_ACK			configuration_ack;
	IPCP_CONFIGURE_NAK			configuration_nak;
	IPCP_CONFIGURATION_REJECT	configuration_reject;
	IPCP_TERMINATE_REQUEST 		terminate_request;
	IPCP_TERMINATE_ACK			terminate_ack;
	IPCP_CODE_REJECT_PACKET		reject;
}_WRS_PACK_ALIGN(1) UNION_IPCP_PACKET;

void initialize_ip_ncp (PFW_PLUGIN_OBJ_STATE * pluginState);

#ifdef __cplusplus
}
#endif

#endif /* __INCvnipstrh */
