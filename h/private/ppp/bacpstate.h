/* bacpstate.h - state machine table of BACP */

/* Copyright 1999 Wind River Systems, Inc. */

#include "copyright_wrs.h"

/*
modification history
--------------------
01a,02feb01,sd 	created from routerware source base
*/

#ifndef __INCbacpstateh
#define __INCbacpstateh

#ifdef __cplusplus
extern "C" {
#endif

/* includes */

#include "ppp/kstart.h"
#include "ppp/kppp.h"
#include "private/ppp/vpppstr.h"
#include "pfw/pfw.h"
#include "netBufLib.h"

/*
*$Log:: /Rtrware/devdrvrs/ppp/vpppsta $
 * 
 * 7     10/01/98 11:43a Alex
 * Updated the PPP source code to conform
 * to a single build. 
 * 
 * 6     4/30/98 3:03p Alex
 * Ppp v4.2.0 check in
 * 
 * 1     4/24/98 12:10a Release Engineer
 * code cleanup, code style changes,
 * linted, system level test
 * PPP v4.2.0
*/

/***************************************************************************************************************
IRC  = initialize_counterss

SCA  = send_configuration_ack
SCJ  = send_configuration_reject ??
SCN  = send_configuration_nak
SCR  = send_configuration_request
SER  =
STA  = send_termination_ack
STR  = send_termination_request

TLS  = this_layer_start
TLF  = this_layer_finished
TLD  = this_layer_down
TLU  = this_layer_up

RCR+ = received_configuration_request good
RCR- = received_configuration_request bad
TO+  = Time interval to send next request
TO-  = Timeout has expired, prior to getting a response

Up   = layer below (MP) is up, start bringing up BACP protocol
Down = layer below (MP) is down, cleanup, and turn the timers off, wait for Up event
Open = initialize BACP
Close= release all resources and turn off timers, wait until next open


      | State																		 State
      |    0         1         2         3         4         5		    6         7         8           9
Events| Initial   Starting  Closed    Stopped   Closing   Stopping Req-Sent  Ack-Rcvd  Ack-Sent    Opened
------+----------------------------------------------------------------------------------------------------
 Up   |    -         -         -         -         -         -		    -         -         -           -
 Down |    -         -         0       tls/1       0         1		    1         1         1         tld/1
 Open |    -         -     irc,scr/6     3r        5r        5r	    6         7         8           9r
 Close|    -         -         2         2         4         4		irc,str/4 irc,str/4 irc,str/4 tld,irc,str/4
      |
  TO+ |    -         -         -         -       str/4     str/5	  scr/6     scr/6     scr/8         -
  TO- |    -         -         -         -       tlf/2     tlf/3	  tlf/3p    tlf/3p    tlf/3p        -
      |
 RCR+ |    -         -       sta/2 irc,scr,sca/8   4         5		  sca/8   sca,tlu/9   sca/8   tld,scr,sca/8
 RCR- |    -         -       sta/2 irc,scr,scn/6   4         5		  scn/6     scn/7     scn/6   tld,scr,scn/6
 RCA  |    -         -       sta/2     sta/3       4         5		  irc/7     scr/6x  irc,tlu/9   tld,scr/6x
 RCN  |    -         -       sta/2     sta/3       4         5		irc,scr/6   scr/6x  irc,scr/8   tld,scr/6x
      |
 RTR  |    -         -       sta/2     sta/3     sta/4     sta/5	  sta/6     sta/6     sta/6   tld,zrc,sta/5
 RTA  |    -         -         2         3       tlf/2     tlf/3	    6         6         8       tld,scr/6
      |
 RUC  |    -         -       scj/2     scj/3     scj/4     scj/5	  scj/6     scj/7     scj/8       scj/9
 RXJ+ |    -         -         2         3         4         5		    6         6         8           9
 RXJ- |    -         -       tlf/2     tlf/3     tlf/2     tlf/3	  tlf/3     tlf/3     tlf/3   tld,irc,str/5
      |
 RXR  |    -         -         2         3         4         5		    6         7         8         ser/9

***************************************************************************************************************/


typedef void (*STATE_ACTION_FUNCTION)(PFW_PLUGIN_OBJ_STATE * pluginObjState,
				    M_BLK_ID pMblk, PPP_STATE end_state);

typedef struct STATE_MACHINE_ACTION_TABLE
    {
    STATE_ACTION_FUNCTION set_ppp_state;
    STATE_ACTION_FUNCTION null_state;
    STATE_ACTION_FUNCTION this_layer_start;
    STATE_ACTION_FUNCTION this_layer_finished;
    STATE_ACTION_FUNCTION this_layer_up;
    STATE_ACTION_FUNCTION this_layer_down;
    STATE_ACTION_FUNCTION initialize_restart_counter;
    STATE_ACTION_FUNCTION zero_restart_counter;
    STATE_ACTION_FUNCTION send_configuration_request;
    STATE_ACTION_FUNCTION send_configuration_ack;
    STATE_ACTION_FUNCTION send_configuration_nak;
    STATE_ACTION_FUNCTION send_configuration_reject;
    STATE_ACTION_FUNCTION send_termination_request;
    STATE_ACTION_FUNCTION send_termination_ack;
    STATE_ACTION_FUNCTION send_code_reject;
    }STATE_MACHINE_ACTION_TABLE;

typedef struct BACP_CONTROL_PROTOCOL_STATE_DATA
    {
    BYTE_ENUM (PPP_STATE)         state;
    BYTE_ENUM (PPP_STATE)         old_state;

    STATE_MACHINE_ACTION_TABLE  * action_table;
    } BACP_CONTROL_PROTOCOL_STATE_DATA;

typedef	struct BACP_STATE_MACHINE_TABLE
    {
    STATE_ACTION_FUNCTION   fptr_state_function;
    BYTE_ENUM (PPP_STATE) end_state;

    STATE_ACTION_FUNCTION   fptr_state_function_1;
    BYTE_ENUM (PPP_STATE) end_state_1;

    STATE_ACTION_FUNCTION   fptr_state_function_2;
    BYTE_ENUM (PPP_STATE) end_state_2;
    } BACP_STATE_MACHINE_TABLE;

extern void bacpSetState (PFW_PLUGIN_OBJ_STATE * pluginObjState,
				    M_BLK_ID pMblk, PPP_STATE end_state);
extern void bacpNullState (PFW_PLUGIN_OBJ_STATE * pluginObjState,
				    M_BLK_ID pMblk, PPP_STATE end_state); 
extern void execute_bacp_state_machine (PFW_PLUGIN_OBJ_STATE * pluginState,
	                               PPP_EVENT ppp_event, M_BLK_ID pMblk);


#ifdef __cplusplus
}
#endif

#endif /* __INCbacpstateh */
