/* mpInterfaceLayerP.h - mpInterface layer component private header file */

/* Copyright 1999 Wind River Systems, Inc. */

#include "copyright_wrs.h"

/*
modification history
--------------------
01b,18dec01,ak support for dynamic assignment of MP Member Stack to Manager 
			   Stack

01a,19feb01,ak created
*/

#ifndef __INCmpInterfaceLayerPh
#define __INCmpInterfaceLayerPh

#ifdef __cplusplus
extern "C" {
#endif

#include "ppp/pppInterfaces.h" 

/*	MP_INTERFACE_LAYER component data structure */

typedef struct MP_INTERFACE_LAYER_COMPONENT
	{
	PFW_LAYER_OBJ	layer;
	} MP_INTERFACE_LAYER_COMPONENT;

/* MP_INTERFACE_LAYER stack data structure */

typedef struct mpInterfaceLayerStackData 
	{
	PFW_PLUGIN_OBJ_CALLBACKS 		*callbacks;
	PFW_STACK_OBJ					*pManagerStackObj;
	PFW_INTERFACE_STATE_PAIR		lcpPacketSendInterfaceStatePair; 
	PFW_INTERFACE_STATE_PAIR		mpControlLayerInterfaceStatePair;
	PFW_INTERFACE_STATE_PAIR		lcpNegotiatedAuthProtocolInterfaceStatePair; 
	PFW_INTERFACE_STATE_PAIR  		mpBundleManagementInterfaceStatePair; 
	PFW_INTERFACE_STATE_PAIR		mpPacketRcvInterfaceStatePair;
	UINT 							authMpInterfaceId;
	MP_UPCALL_FUNCTIONS				*mpUpCalls; 
	void							*userHandler; 
	} MP_INTERFACE_LAYER_STACK_DATA;

/*	MP_INTERFACE_LAYER profile data structure */

typedef struct mpInterfaceLayerProfileData
	{
	PFW_STACK_OBJ	*mpInterface_managerStackId;
	ULONG			mpInterface_memberLinkSpeed;
	} MP_INTERFACE_LAYER_PROFILE_DATA;

#ifdef __cplusplus
}
#endif

#endif /* __INCmpInterfaceLayerPh */

