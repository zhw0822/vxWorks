/* pppLcpComponentP.h - LCP component private header file */

/* Copyright 1999 - 2005 Wind River Systems, Inc. */

#include "copyright_wrs.h"

/*
modification history
--------------------
01r,28feb05,ijm added lcpEchoesFailed, SPR#70584
01q,21nov03,rp  updated path for random.h
01p,07aug03,rp  updating include path for random.h
01o,17feb02,ak  support for dynamic assignment of MP Member Stack to Manager
                stack
01n,04feb01,ak  redefined config option structures as per the memory reduction 
				recommendations
01m,30jun01,nts removed conditional compilation PPP_MP
01l,30jun01,ak  merging with the ML-PPP code
01k,29sep00,sj  merging with TOR2_0-WINDNET_PPP-CUM_PATCH_2
01j,09aug00,adb  support opening connections in passive mode
01i,01aug00,adb  Merging with openstack view
01h,13jul00,sj  merged from the PPP radius-ras branch
01g,26jun00,sj  adding stackData pointer to profileData
01f,11jul00,md  added initialReceived, lastReceived and lastSent to
                 lcpStackData structure
01e,01mar00,sj  split alternateString List into LocalAlternate and
                 RemoteAlternate
01d,09feb00,sj  add LCP_EXTENSIONS and PPP_SECURITY_CONFIG interfaces
01c,16dec99,sj  optionArray needs to be of size NUMBER_OF_LCP_OPTIONS + 1
01b,11nov99,sj  using remoteString
01a,04nov99,sj 	cut out from target/h/ppp/pppLcpComponent.h
*/

#ifndef __INCpppLcpComponentPh
#define __INCpppLcpComponentPh

#ifdef __cplusplus
extern "C" {
#endif


#include "vxWorks.h"
#include "tickLib.h"
#include "logLib.h"
#include "errnoLib.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "ctype.h"
#include "memLib.h"
#include "netBufLib.h"
#include "pfw/pfw.h"
#include "pfw/pfwStack.h"
#include "pfw/pfwProfile.h"
#include "pfw/pfwComponent.h"
#include "pfw/pfwLayer.h"
#include "pfw/pfwEvent.h"
#include "pfw/pfwTimer.h"
#include "pfw/pfwMemory.h"
#include "ppp/kstart.h"
#include "ppp/kppp.h"
#include "private/ppp/vpppstr.h"
#include "private/ppp/vlcpstr.h"
#include "private/ppp/pppoptn.h"
#include "private/ppp/pppstate.h"
#include "ppp/magic.h"
#include "ppp/interfaces/pppControlLayerInterface.h"
#include "ppp/interfaces/pppControlProtocolInterface.h"
#include "ppp/interfaces/lcpInterfaces.h"
#ifdef _WRS_KERNEL
#include "random.h"
#else
#include "wrn/util/random.h"
#endif

#define REMOTE_STRING

typedef struct lcpOption
    {
    char                       name[PFW_MAX_NAME_LENGTH];
    BYTE_ENUM(LCP_OPTION_TYPE) optionType;
    char                       optionTypeString[16];
    }LCP_CONFIG_OPTION; 

/* Component data per instance; one per pfw */

typedef struct pppLcpComponent
    {
    PFW_COMPONENT_OBJ                               component;
    FPTR_LCP_PACKET_RECEIVED *                      fptr_lcp_packet_received;
    CONTROL_PROTOCOL_INTERFACE                      lcpControlInterface;
    PPP_LINK_STATUS_ENTRY_INTERFACE                 lcpLinkStatusEntry;
    PPP_LINK_CONFIG_ENTRY_INTERFACE                 lcpLinkConfigEntry;
    PPP_LINK_STATUS_COUNTER_INCREMENT_INTERFACE     lcpLinkCounters;
    LCP_NEGOTIATED_AUTH_PROTOCOL_INTERFACE          lcpNegotiatedAuthProtocols;
    LCP_EXTENDED_OPTIONS_INTERFACE                  lcpExtendedOptions;
    LCP_CONFIG_REQS_INTERFACE                       lcpConfReqGet;
    LCP_PACKET_SEND_INTERFACE                       lcpPacketSend;
    PPP_SECURITY_CONFIG_ENTRY_INTERFACE             pppSecurityConfigEntry;
    LCP_CONFIG_OPTION          	                    option[NUMBER_OF_LCP_OPTIONS+1]; 

/* WindNet Multilink - Start */

	LCP_PROXYLCP_INTERFACE							lcpProxyLcp;
	LCP_BUNDLE_OPTIONS_INTERFACE					lcpBundleOptions;
#if 0 
	LCP_MP_TIME_INTERFACE							lcpMpTimeInterface;  /* NOT SUPPORTED IN THIS MP RELEASE */
#endif 

/* WindNet Multilnik - End */

    } PPP_LCP_COMPONENT;

/* LCP configuration option types */

typedef struct alternateStrings
    {
    struct alternateStrings * next;
    UINT32 preference;	/* key for this ordered list */
    char configString[1];
    }ALTERNATE_OPTION_STRING;

typedef struct lcpConfigStrings
    {
    char                       *configString;
    ALTERNATE_OPTION_STRING    *localAlternateConfigString;
    ALTERNATE_OPTION_STRING    *remoteAlternateConfigString;
    char                       *remoteConfigString;
    } LCP_CONFIG_STRINGS; 

typedef struct LCP_EXTENSIONS
    {
    BOOL pppLocalToPeerCompoundFrames;
    BOOL pppPeerToLocalCompoundFrames;
    BYTE pppLocalToPeerMaxSelfDescribingPad;
    BYTE pppPeerToLocalMaxSelfDescribingPad;
    BOOL pppLocalToPeerCallbackOperation;
    BOOL pppPeerToLocalCallbackOperation;
    char * pppPeerToLocalCallbackMsg;
    } LCP_EXTENSIONS;

typedef struct lcpProfileData
    {
    LCP_CONFIG_STRINGS *        lcpConfigStrings; 
    BYTE_ENUM (BOOLEAN)	        send_id_enabled;
    char *                      time_remaining_message;
    ULONG                       time_remaining_timeout;
    char *                      tx_identification_message;
    ULONG                       maximum_number_of_configuration_requests;
    ULONG                       maximum_number_of_configuration_failures;
    ULONG                       maximum_number_of_termination_requests;
    ULONG                       maximum_number_of_unacknowledged_echo_requests;
    ULONG                       maximum_configuration_request_send_interval;
    ULONG                       maximum_termination_request_send_interval;
    ULONG                       maximum_echo_request_send_interval;
    ULONG                       maximum_configuration_request_backoff_interval;
    BYTE                        size_of_time_remaining_message;
    BYTE_ENUM (CALLBACK_OPERATION)  callback_operation;
    char                        *callback_message;
#if 0 /* NOT SUPPORTED IN THIS RELEASE */
    ULONG                       link_quality_reporting_interval;
#endif /* NOT SUPPORTED IN THIS RELEASE */
    BOOL                        passiveMode;                   
    struct lcpStackData *       stackData;
    } LCP_PROFILE_DATA;

typedef	struct lcpStackData
    {
    PPP_CONTROL_PROTOCOL_STATE_DATA    stateData;
    NET_POOL_ID                   netPoolId;
    OPTION_LISTS                  option_lists;
    PFW_TIMER_OBJ *               lcp_timer;
    PFW_TIMER_OBJ *               time_remaining_timer;
    PFW_EVENT_OBJ  *              lcpUpEventObj;
    PPP_CONTROL_LAYER_INTERFACE * controlLayerInterface;
    LINK_QUALITY_REPORT           most_recently_received_LQR;
    LINK_QUALITY_COUNTERS         link_quality_counters;
#if 0
    BYTE_ENUM (BOOLEAN)           link_quality_reporting_interval;
    USHORT                        time_to_send_LQR;
#endif
    BYTE                          last_id_of_lcp_packet_sent;
    USHORT                        number_of_configuration_requests;
    USHORT                        number_of_configuration_naks;
    USHORT                        number_of_echo_requests;
    USHORT                        number_of_lcp_termination_requests;
    PPP_LCP_STATISTICS            lcp_statistics;
    PPP_LCP_MIBS                  lcp_mibs;
    LCP_EXTENSIONS                lcpExtensions;
    M_BLK_ID                      last_txed_echo_request_packet;
    M_BLK_ID                      last_txed_lcp_configuration_request_packet;
    M_BLK_ID                      last_rxed_lcp_configuration_request_packet;
    M_BLK_ID                      last_rxed_lcp_termination_request_packet;
    M_BLK_ID                      last_rxed_lcp_echo_request_packet;
    USHORT                        number_of_bytes_rejected;
    ULONG                         time_remaining_in_seconds;
    ULONG                         client_time_remaining;
    BYTE                          id_sequence_number;
    char                          rx_identification_message[NAME_SIZE];
    BOOLEAN                       configuration_request_backoff_period_started;
    BOOLEAN                       lcpEchoesFailed;
    PFW_PLUGIN_OBJ_CALLBACKS    * callbacks;
    M_BLK_ID                      copy_of_initial_rxed_lcp_configuration_request_packet;
    M_BLK_ID                      copy_of_last_rxed_lcp_configuration_request_packet;
    M_BLK_ID                      copy_of_last_txed_lcp_configuration_request_packet;
/* WindNet Multilink */
    BOOL                          isMpManagerStack;
#if 0 /* NOT SUPPORTED IN THIS RELEASE OF MP */
	BOOL						  is_mp_timing_the_link; 
    PFW_EVENT_OBJ  *              pEchoReplyReceivedEventObj;
#endif
/* WindNet Multilnik */
    } LCP_STACK_DATA;

/* ppplcprx.c */

void lcpSetReceiveFunctions ( PPP_LCP_COMPONENT * lcpComponent);

/* ppplcptx.c */

void lcpSetSendActionFunctions ( STATE_MACHINE_ACTION_TABLE * actionTable);

#ifdef __cplusplus
}
#endif

#endif /* __INCpppLcpComponentPh */
