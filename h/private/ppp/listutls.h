/* listutls.h - option list manager */

/* Copyright 1999 Wind River Systems, Inc. */

#include "copyright_wrs.h"

/*
modification history
--------------------
01a,04oct99,sj 	derived from Routerware source base
*/

#ifndef __INClistutlsh
#define __INClistutlsh

#ifdef __cplusplus
extern "C" {
#endif

extern void add_entry_to_list ( LINK *sptr_link, LINK *sptr_link_to_add);
extern void *get_entry_from_list ( LINK *sptr_link);
extern void delete_entry_from_list ( LINK *sptr_list_link,
						    LINK *sptr_link_to_delete);

extern void add_entry_to_front_of_list (LINK *sptr_link,LINK *sptr_link_to_add);
extern void insert_entry_in_list ( LINK *sptr_link, LINK *sptr_link_to_add,
					    LINK *sptr_link_after_which_to_add);

extern void *get_pointer_to_first_entry_in_list ( LINK *sptr_link);
extern void *get_pointer_to_next_entry_in_list ( LINK *sptr_current_entry);
extern void *get_pointer_to_previous_entry_in_list ( LINK *sptr_current_entry);

extern void free_list ( LINK *sptr_list, 
				void (*fptr_free) (void *vptr_data_to_free));

extern void *get_entry_from_list_using_index (LINK *sptr_list,
						USHORT element_index_to_find);

#ifdef __cplusplus
}
#endif

#endif /* __INClistutlsh */
