/* bacputil.h - BACP utility function header file */

/* Copyright 1999 Wind River Systems, Inc. */

#include "copyright_wrs.h"

/*
modification history
--------------------
02b,08oct01,as  added copyPortInfoToBapBuffer for multiple phone delta option support
01a,01feb01,as 	created
*/

#ifndef __INCbacputilh
#define __INCbacputilh

#ifdef __cplusplus
extern "C" {
#endif

#include "private/ppp/vbapstr.h"

/* baputil.c */


extern M_BLK_ID bacpDupPkt (M_BLK_ID pPacket); 

extern BAP_BUFFER * bap_match_received_response_to_outstanding_request 
							(LINK * ptr_linked_list, BYTE bap_id);

extern BAP_BUFFER * bap_find_unacknowledged_outstanding_request 
							(LINK * ptr_linked_list);

extern BAP_ACTIVE_PORT * bap_find_active_port_with_this_bandwidth 
							(LINK * ptr_linked_list, USHORT bandwidth);		

extern PFW_STACK_OBJ * bap_find_stackobj_with_this_discriminators 
(LINK * ptr_linked_list, USHORT localDiscriminator, USHORT remoteDiscriminator);

extern BAP_BUFFER * bap_find_acknowledged_linkdrop_request_with_discriminator 
   				(LINK * ptr_linked_list, USHORT localDiscriminator);

extern PFW_STACK_OBJ * bap_find_stackobj_for_the_remote_discriminator 
				(LINK * ptr_linked_list, USHORT remoteDiscriminator);

extern BAP_ACTIVE_PORT * bap_find_active_port_with_this_stackobj
							(LINK * ptr_linked_list, PFW_STACK_OBJ * stackObj);

extern BAP_ACTIVE_PORT * bap_find_active_port_with_this_portnumber
   	(LINK * ptr_linked_list, USHORT portnumber);

extern USHORT bap_number_of_active_port_entries
   	(LINK * ptr_linked_list);

extern BAP_BUFFER * bap_find_acknowledged_linkdrop_request 
							(LINK * ptr_linked_list);

extern BAP_BUFFER* bap_find_outstanding_request_for_port
    						(LINK * ptr_linked_list, USHORT port_number);

extern BAP_ACTIVE_PORT * bap_match_discriminator_and_active_port 
	(LINK * ptr_linked_list, USHORT localDiscriminator);

extern STATUS copyPortInfoToBapBuffer (PFW_OBJ * pfwObj, 
				SL_LIST * destinationPortList, SL_LIST * sourcePortList);

extern void bapPrintOptionsInPacket (M_BLK_ID packet);


#ifdef __cplusplus
}
#endif

#endif /* __INCbacputilh */
