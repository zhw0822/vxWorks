/* vbapstr.h - BAP structure definitions */

/* Copyright 1999 Wind River Systems, Inc. */

#include "copyright_wrs.h"

/*
modification history
--------------------
01b,08oct01,as	remove port manager related function definitions
01a,30jan01,as 	derived from routerware source base
*/

/*
*$Log:: /Rtrware/devdrvrs/ppp/vlcpstr $
 * 
 * 7     10/01/98 11:43a Alex
 * Updated the PPP source code to conform
 * to a single build. 
 * 
 * 6     4/30/98 3:03p Alex
 * Ppp v4.2.0 check in
 * 
 * 1     4/24/98 12:10a Release Engineer
 * code cleanup, code style changes,
 * linted, system level test
 * PPP v4.2.0
*/

#ifndef __INCvbapstrh
#define __INCvbapstrh

#ifdef __cplusplus
extern "C" {
#endif

#include "ppp/kstart.h"
#include "ppp/kppp.h"
#include "private/ppp/vpppstr.h"
#include "ppp/kbacpif.h"
#include "sllLib.h"

enum BAP_PACKET_TYPE
	{
	BAP_CALL_REQUEST		      = (BYTE) 0x01,
	BAP_CALL_RESPONSE			  = (BYTE) 0x02,
	BAP_CALLBACK_REQUEST		  = (BYTE) 0x03,
	BAP_CALLBACK_RESPONSE		  = (BYTE) 0x04,
	BAP_LINK_DROP_QUERY_REQUEST	  = (BYTE) 0x05,
	BAP_LINK_DROP_QUERY_RESPONSE  = (BYTE) 0x06,
	BAP_CALL_STATUS_INDICATION	  = (BYTE) 0x07,
	BAP_CALL_STATUS_RESPONSE	  = (BYTE) 0x08,
	NUMBER_OF_BAP_PACKET_TYPES
	};


enum BAP_RESPONSE_CODE
	{
	BAP_RESPONSE_ACK		  	= (BYTE) 0x00,
	BAP_RESPONSE_NAK		  	= (BYTE) 0x01,
	BAP_RESPONSE_REJ		  	= (BYTE) 0x02,
	BAP_RESPONSE_FULL_NAK	  	= (BYTE) 0x03,

	BAP_SECURITY_VIOLATION         = (BYTE) 0xfd,   /* routerware added */
	BAP_FAVORED_PEER_PRIORITY_DROP = (BYTE) 0xfe,	/* routerware added */
	BAP_RESPONSE_TIMEOUT		   = (BYTE) 0xff	/* routerware added */
	};


/**************************************/
/* Option #1 = Link Type Option       */
/**************************************/
typedef struct BAP_LINK_TYPE_OPTION
	{								
	BYTE	type;		 /* = 0x01  */
	BYTE	length;		 /* = 0x05  */
	USHORT	link_speed;	 /* in Kb/s */
	BYTE_ENUM(BAP_LINK_TYPES) link_type;
	}__attribute__((__packed__))
	BAP_LINK_TYPE_OPTION;


/**************************************/
/* Option #2= Phone Delta Option      */
/**************************************/
enum BAP_PHONE_DELTA_TYPES
	{
	UNIQUE_DIGITS_TYPE			 = (BYTE) 0x01,
	SUBSCRIBER_NUMBER_TYPE		 = (BYTE) 0x02,
	PHONE_NUMBER_SUBADDRESS_TYPE = (BYTE) 0x03,
	SUBOPTION_TYPE_NONE          = (BYTE) 0xff

	} BAP_PHONE_DELTA_TYPES;

typedef struct BAP_PHONE_DELTA_UNIQUE_DIGITS
	{
	BYTE_ENUM(BAP_PHONE_DELTA_TYPES)	type;
	BYTE							length;
	BYTE							number_of_digits;
	BYTE							digits[VARIABLE_NUMBER_OF_BYTES];

	} __attribute__((__packed__))
	BAP_PHONE_DELTA_UNIQUE_DIGITS;

typedef struct BAP_PHONE_DELTA_SUBSCRIBER_NUMBER
	{
	BYTE_ENUM(BAP_PHONE_DELTA_TYPES)	type;
	BYTE							length;
	BYTE							digits[VARIABLE_NUMBER_OF_BYTES];

	} __attribute__((__packed__))
	BAP_PHONE_DELTA_SUBSCRIBER_NUMBER;

typedef struct BAP_PHONE_DELTA_PHONE_NUMBER_SUB_ADDRESS
	{
	BYTE_ENUM(BAP_PHONE_DELTA_TYPES)	type;
	BYTE							length;
	BYTE							digits[VARIABLE_NUMBER_OF_BYTES];

	}__attribute__((__packed__))
    BAP_PHONE_DELTA_PHONE_NUMBER_SUB_ADDRESS;

typedef union BAP_PHONE_DELTA_SUBOPTION
	{
	BAP_PHONE_DELTA_UNIQUE_DIGITS				unique_digits;
	BAP_PHONE_DELTA_SUBSCRIBER_NUMBER			subscriber_number;
	BAP_PHONE_DELTA_PHONE_NUMBER_SUB_ADDRESS	phone_number_sub_address;

	}__attribute__((__packed__))
	BAP_PHONE_DELTA_SUBOPTION;

typedef struct BAP_PHONE_DELTA_OPTION
	{								
	BYTE	type;		 /* = 0x02  */
	BYTE	length;		 
	BYTE	data[VARIABLE_NUMBER_OF_BYTES];
/* Note that there may be more than one suboption within 
												the phone delta option */
	}__attribute__((__packed__))
	BAP_PHONE_DELTA_OPTION;


/**************************************/
/* Option #3 = No Phone Number Option */
/**************************************/
typedef struct BAP_NO_PHONE_NUMBER_NEEDED_OPTION
	{								
	BYTE	type;		 /* = 0x03  */
	BYTE	length;		 /* = 0x02  */

	}__attribute__((__packed__))
    BAP_NO_PHONE_NUMBER_NEEDED_OPTION;

/**************************************/
/* Option #4 = Reason Option          */
/**************************************/
typedef struct BAP_REASON_OPTION
	{								
	BYTE	type;		 /* = 0x04  */
	BYTE	length;		 
	BYTE	reason[VARIABLE_NUMBER_OF_BYTES]; 

	}__attribute__((__packed__))
	BAP_REASON_OPTION;

/*****************************************/
/* Option #5 = Link Discriminator Option */
/*****************************************/
typedef struct BAP_LINK_DISCRIMINATOR_OPTION
	{								
	BYTE	type;		 /* = 0x05  */
	BYTE	length;		 /* = 0x04  */
	USHORT  link_discriminator;

	}__attribute__((__packed__))
	BAP_LINK_DISCRIMINATOR_OPTION;

/**************************************/
/* Option #6 = Link Status Option     */
/**************************************/
typedef struct BAP_CALL_STATUS_OPTION
	{								
	BYTE	type;		 /* = 0x06  */
	BYTE	length;		 /* = 0x04  */
	BYTE_ENUM(CALL_STATUS)    status;
	BYTE_ENUM(CALL_ACTION)    action;

	}__attribute__((__packed__))
	BAP_CALL_STATUS_OPTION;


typedef struct BAP_TLV_OPTION
	{
	BYTE	type;
	BYTE	length;
	BYTE	value[VARIABLE_NUMBER_OF_BYTES];

	}__attribute__((__packed__))
	BAP_TLV_OPTION;


enum BAP_OPTION_TYPE
	{
	BAP_LINK_TYPE				= (BYTE) 0x01,
	BAP_PHONE_DELTA				= (BYTE) 0x02,
	BAP_NO_PHONE_NUMBER_NEEDED	= (BYTE) 0x03,
	BAP_REASON					= (BYTE) 0x04,
	BAP_LINK_DISCRIMINATOR		= (BYTE) 0x05,
	BAP_CALL_STATUS				= (BYTE) 0x06
	};
	
typedef TEST (*FPTR_BAP_PACKET_RECEIVED)
		(
		PFW_PLUGIN_OBJ_STATE * pluginState,
		M_BLK_ID packet
		);

/**************************************/
/* BAP Packet Types                   */
/**************************************/
typedef struct BAP_REQUEST_HEADER
	{
	BYTE_ENUM (BAP_PACKET_TYPE)		bap_type;
	BYTE							bap_id;
	USHORT							bap_length;
	}__attribute__((__packed__))
  	BAP_REQUEST_HEADER;


typedef struct BAP_REQUEST_PACKET
	{
	PPP_HEADER						ppp_header;
	BYTE_ENUM (BAP_PACKET_TYPE)		bap_type;
	BYTE							bap_id;
	USHORT							bap_length;
	BYTE							bap_data[VARIABLE_NUMBER_OF_BYTES];
	}__attribute__((__packed__))
	BAP_REQUEST_PACKET;

typedef struct BAP_INDICATION_PACKET
	{
	PPP_HEADER						ppp_header;
	BYTE_ENUM (BAP_PACKET_TYPE)		bap_type;
	BYTE							bap_id;
	USHORT							bap_length;
	BYTE							bap_data[VARIABLE_NUMBER_OF_BYTES];
	}__attribute__((__packed__))
	BAP_INDICATION_PACKET;

typedef struct BAP_RESPONSE_HEADER
	{
	BYTE_ENUM (BAP_PACKET_TYPE)		bap_type;
	BYTE							bap_id;
	USHORT							bap_length;
	BYTE_ENUM(BAP_RESPONSE_CODE)    bap_response_code;
	}__attribute__((__packed__))
	BAP_RESPONSE_HEADER;


typedef struct BAP_RESPONSE_PACKET
	{
	PPP_HEADER						ppp_header;
	BYTE_ENUM (BAP_PACKET_TYPE)		bap_type;
	BYTE							bap_id;
	USHORT							bap_length;
	BYTE_ENUM(BAP_RESPONSE_CODE)    bap_response_code;
	BYTE							bap_data[VARIABLE_NUMBER_OF_BYTES];
	}__attribute__((__packed__))
	BAP_RESPONSE_PACKET;

typedef	struct	BAP_CALL_REQUEST_PACKET 
	{
	PPP_HEADER						ppp_header; 
	BYTE_ENUM (BAP_PACKET_TYPE)		bap_type; 
	BYTE							bap_id; 
	USHORT							bap_length; 
	BAP_LINK_TYPE_OPTION			link_type_option; 
	/* OPTION: No Phone Number Option */
	}__attribute__((__packed__))
    BAP_CALL_REQUEST_PACKET;

typedef	struct	BAP_CALL_RESPONSE_PACKET
	{
	PPP_HEADER						ppp_header;
	BYTE_ENUM (BAP_PACKET_TYPE)		bap_type;
	BYTE							bap_id;
	USHORT							bap_length;
	BYTE_ENUM(BAP_RESPONSE_CODE)    bap_response_code;
	/* OPTION:  Phone Delta Options */
	/* OPTION: 	Link Type   Options */
	}__attribute__((__packed__))
	BAP_CALL_RESPONSE_PACKET;

typedef	struct	BAP_CALLBACK_REQUEST_PACKET
	{
	PPP_HEADER						ppp_header;
	BYTE_ENUM (BAP_PACKET_TYPE)		bap_type;
	BYTE							bap_id;
	USHORT							bap_length;
	BAP_LINK_TYPE_OPTION			link_type_option;
	/* BAP_PHONE_DELTA_SUBOPTION       bap_phone_delta_option; */
	/* Note that above option has multiple suboptions and variable length */
	}__attribute__((__packed__))
	BAP_CALLBACK_REQUEST_PACKET;

typedef struct BAP_CALLBACK_RESPONSE_PACKET
	{
	PPP_HEADER					ppp_header;
	BYTE_ENUM (BAP_PACKET_TYPE)		bap_type;
	BYTE						bap_id;
	USHORT					bap_length;
	BYTE_ENUM(BAP_RESPONSE_CODE)    bap_response_code;
	}__attribute__((__packed__))
    BAP_CALLBACK_RESPONSE_PACKET;

typedef struct BAP_LINK_DROP_QUERY_REQUEST_PACKET
	{
	PPP_HEADER						ppp_header;
	BYTE_ENUM (BAP_PACKET_TYPE)		bap_type;
	BYTE							bap_id;
	USHORT							bap_length;
	BAP_LINK_DISCRIMINATOR_OPTION	link_discriminator_option;
	} __attribute__((__packed__))
	BAP_LINK_DROP_QUERY_REQUEST_PACKET;

typedef	struct	BAP_LINK_DROP_QUERY_RESPONSE_PACKET
	{
	PPP_HEADER						ppp_header;
	BYTE_ENUM (BAP_PACKET_TYPE)		bap_type;
	BYTE							bap_id;
	USHORT							bap_length;
	BYTE_ENUM(BAP_RESPONSE_CODE)    bap_response_code;
	}__attribute__((__packed__))
    BAP_LINK_DROP_QUERY_RESPONSE_PACKET;

typedef	struct	BAP_CALL_STATUS_INDICATION_PACKET
	{
	PPP_HEADER						ppp_header;
	BYTE_ENUM (BAP_PACKET_TYPE)		bap_type;
	BYTE							bap_id;
	USHORT							bap_length;
	BAP_CALL_STATUS_OPTION			bap_call_status_option;
	/* OPTION:  Phone Delta Options */
	}__attribute__((__packed__))
	BAP_CALL_STATUS_INDICATION_PACKET;

typedef	struct	BAP_CALL_STATUS_RESPONSE_PACKET
	{
	PPP_HEADER						ppp_header;
	BYTE_ENUM (BAP_PACKET_TYPE)		bap_type;
	BYTE							bap_id;
	USHORT							bap_length;
	BYTE_ENUM(BAP_RESPONSE_CODE)    bap_response_code;
	}__attribute__((__packed__))
	BAP_CALL_STATUS_RESPONSE_PACKET;


typedef	struct BAP_HEADER
	{
	BYTE_ENUM (BAP_PACKET_TYPE)		code;
	BYTE							id;
	USHORT							length;		
	}__attribute__((__packed__))
	BAP_HEADER;

typedef	struct BAP_PACKET
	{
	PPP_HEADER						ppp_header;
	BAP_HEADER						bap_header;
	BAP_TLV_OPTION					option;
	}__attribute__((__packed__))
	BAP_PACKET;


typedef	struct	PPP_BAP_STATISTICS
	{
	ULONG	number_of_tx_packets;
	ULONG	number_of_tx_bytes;
	ULONG	number_of_rx_packets;
	ULONG	number_of_rx_bytes;
	ULONG	number_of_packets_rxed_greater_than_maximum_size;
	ULONG	number_of_packets_rxed_less_than_minimum_size;
	ULONG	number_of_control_tx_packets[NUMBER_OF_BAP_PACKET_TYPES];
	ULONG	number_of_control_rx_packets[NUMBER_OF_BAP_PACKET_TYPES];
	} PPP_BAP_STATISTICS;



/****************************************************************/
/* BAP and PORT MANAGER RELATED DEFINES AND STRUCTURES  ***/
/****************************************************************/

enum BAP_TEST
	{
	BAP_PASS										=  0x00,
	BAP_FAIL										=  0x01,
	BAP_NO_LINK_AVAILABLE							=  0x02,
	BAP_CALL_REQUEST_ALREADY_IN_PROGRESS			=  0x03,
	BAP_CALLBACK_REQUEST_ALREADY_IN_PROGRESS		=  0x04,
	BAP_CALL_STATUS_INDICATION_ALREADY_IN_PROGRESS	=  0x05,
	BAP_LINK_DROP_ALREADY_IN_PROGRESS				=  0x06,
	BAP_INVALID_REQUEST								=  0x07,
	BAP_NO_MEMORY									=  0x08,
	BAP_PROTOCOL_NOT_UP								=  0x09,
	BAP_CALL_REQUEST_ACCEPTED						=  0x0a,
	BAP_CALL_REQUEST_IN_PROGRESS					=  0x0b,
	BAP_FAIL2										=  0x0f,
	BAP_LINK_DROP_ACCEPTED							=  0x10
	} BAP_TEST;


typedef	struct	BAP_BUFFER_LINK
	{
	struct BAP_BUFFER 		*sptr_forward_link;
	struct BAP_BUFFER 		*sptr_backward_link;
	} BAP_BUFFER_LINK;

typedef	struct	BAP_ACTIVE_PORT_LINK
	{
	struct BAP_ACTIVE_PORT 		*sptr_forward_link;
	struct BAP_ACTIVE_PORT 		*sptr_backward_link;
	} BAP_ACTIVE_PORT_LINK;


typedef struct BAP_BUFFER
	{
	BAP_BUFFER_LINK			links;
	enum BAP_TEST			status;
	SL_LIST 				port_list;
	USHORT					packet_length;
	M_BLK_ID				bap;
	} BAP_BUFFER;


typedef struct BAP_ACTIVE_PORT
	{
	BAP_ACTIVE_PORT_LINK		links;
	USHORT						port_number;
	BYTE_ENUM(BAP_LINK_TYPES)	port_bap_type;
	ULONG						port_speed;
	USHORT						port_bacp_local_discr;
	USHORT						port_bacp_remote_discr;
	BOOLEAN						bacpAdded;
	PFW_STACK_OBJ				* linkStackObj; 
   } BAP_ACTIVE_PORT;


/* baptimer.c */

extern STATUS retry_bap_call_request (PFW_PLUGIN_OBJ_STATE *, int id);
extern STATUS retry_bap_callback_request (PFW_PLUGIN_OBJ_STATE *, int id);
extern STATUS retry_bap_link_drop_request (PFW_PLUGIN_OBJ_STATE *, int id);
extern STATUS retry_bap_status_indication (PFW_PLUGIN_OBJ_STATE *, int id); 

extern void free_active_port (LINK * ptr_linked_list,
								BAP_ACTIVE_PORT *active_port);

extern void free_bap_buffer (LINK * ptr_linked_list, BAP_BUFFER * bap_buffer);



/* pppbaprx.c */

extern TEST bap_packet_received (PFW_PLUGIN_OBJ_STATE * pluginState,
							    M_BLK_ID packet);
#ifdef __cplusplus
}
#endif

#endif /* __INCvbapstrh */
