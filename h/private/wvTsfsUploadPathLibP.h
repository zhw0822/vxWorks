/* wvTsfsUploadPathLibP.h - tsfs event-upload mechanism library header */

/* Copyright 1997 Wind River Systems, Inc. */

/*
modification history
--------------------
01f,04sep03,tcr  rename init function
01e,27jan98,cth  removed tsfsUploadPathError prototypes, SOSENDBUFSIZE
01d,18dec97,cth  renamed again to wvTsfsUploadPathLibP.h from, updated include,
		 wvTsfsUploadPathP.h, added tsfsUploadPathLibInit prototype 
01c,16nov97,cth  renamed again to wvTsfsUploadPathP.h from tsfsUploadPathP.h
01b,16nov97,cth  changed prototypes to match new WV2.0 upload-path model
                 renamed to tsfsUploadPathP.h from evtTsfsSockLibP.h
01a,21aug97,cth  created, modified from evtSockLibP.h
*/


#ifndef __INCwvtsfsuploadpathlibph
#define __INCwvtsfsuploadpathlibph

#ifdef __cplusplus
extern "C" {
#endif


#include "private/wvUploadPathP.h"


extern STATUS	    wvTsfsUploadPathLibInit (void);
extern UPLOAD_ID    wvTsfsUploadPathCreate (char *ipAddress, short port);
extern void         wvTsfsUploadPathClose (UPLOAD_ID upId);
extern int          wvTsfsUploadPathWrite (UPLOAD_ID upId, char * buffer,
                                           size_t bufSize);


#ifdef __cplusplus
}
#endif

#endif /* __INCwvtsfsuploadpathlibph*/

