/* pfwP.h - Protocol framework private header file */

/* Copyright 1999 Wind River Systems, Inc. */

/*
modification history
--------------------
01d,17oct99,koz  Added interface table to pfwObj
01c,20sep99,koz  Changed for allowing to add control jobs in interrupt context
01b,30aug99,koz  Added memory manager.
01a,02aug99,koz  written
*/

#ifndef __INCpfwPh
#define __INCpfwPh

#ifdef __cplusplus
extern "C" {
#endif

#include "pfw/pfw.h"
#include "private/pfw/pfwTimerP.h"
#include "rngLib.h"
#include "pfw/pfwTable.h"
#include "private/pfw/pfwUtilP.h"
#include "private/pfw/pfwMemoryP.h"
#include "private/pfw/pfwEventP.h"

struct pfwObj
    {
    char name[PFW_MAX_NAME_LENGTH];    /* framework name */        
    unsigned int id;                   /* framework id */
    int controlTaskId;                 /* framework control task id */
    int controlTaskPriority;           /* framework control task priority */
    SEM_ID controlSem;                 /* semaphore to signal the control task*/
    unsigned int controlJobQueueSize;  /* size of controlJobRing */
    RING_ID controlJobRing;            /* ring of submitted control path jobs */
    int dataTaskId;                    /* framework control task id */
    int dataTaskPriority;              /* framework data task priority */
    SEM_ID dataSem;                    /* semaphore to signal the data task */
    unsigned int dataJobQueueSize;     /* size of dataJobRing */
    RING_ID dataJobRing;               /* ring of submitted data path jobs */
    NET_POOL_ID netPoolId;             /* net pool id used by framework objs */ 
    PFW_TIMER_MANAGER *timerManager;   /* pointer to framework timer manager */
    PFW_MEMORY_MANAGER *memoryManager; /* pointer to framework memory manager*/
    PFW_TABLE_OBJ *interfaceTable;     /* table of interfaces */ 
    PFW_TABLE_OBJ *eventTable;         /* table of events */ 
    PFW_TABLE_OBJ                      /* one table for each layered plane */ 
        *planeTable[PFW_LAYERED_PLANE_NUMBER];
    PFW_TABLE_OBJ *layerTable;         /* table of layers */
    PFW_TABLE_OBJ *componentTable;     /* table of components */ 
    PFW_TABLE_OBJ *stackTable;         /* table of stacks */
    PFW_TABLE_OBJ *profileTable;       /* table of profiles */
    PFW_TABLE_OBJ *parameterTable;     /* table of parameters */
    };

struct pfwPrivateData
    {
    unsigned int id;                /* id */
    PFW_PLUGIN_OBJ_TYPE objType;    /* type, component or layer */
    PFW_EVENT_SUBSCRIBER_ITEM       /* head of the event subscriber item list */
	*eventSubscriberItemHead;
    SEM_ID eventMutex;              /* mutex for accessing the event list */
    SL_LIST interfaceItemList;      /* list of interface items */
    unsigned int referenceCount;    /* reference count */
    };

STATUS pfwPluginObjShow (PFW_PLUGIN_OBJ *pluginObj);

#ifdef __cplusplus
}
#endif

#endif /* __INCpfwPh */
