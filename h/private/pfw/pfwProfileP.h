/* pfwProfile.h - private Protocol FrameWork profile header */

/* Copyright 1999 Wind River Systems, Inc. */

/* 
modification history
--------------------
01f,23feb00,sj  remove extra comma after last enum in PFW_PARAMETER_TYPE
01e,22nov99,koz  Added pfwProfileTableCreate and pfwParameterTableCreate
                 rotines
01d,12oct99,koz  Added static and dynamic plugin object profile parameters
01c,15sep99,sj   changing field name "key" to "id" in profile object
01b,18Aug99,sj 	 modified for generic framework 
01a,xxXXXxx,koz	 created 
*/

#ifndef __INCpfwProfilePh
#define __INCpfwProfilePh

#ifdef __cplusplus
extern "C" {
#endif

#include "pfw/pfwComponent.h"
#include "pfw/pfwLayer.h"
#include "pfw/pfwProfile.h"
#include "private/pfw/pfwP.h"
#include "semLib.h"

/* typedefs */

typedef enum pfwParameterType
    {
    _REGULAR_PARAMETER,   /* a parameter created by a plugin object */
    _REMOVE_FROM_PROFILE, /* a parameter created by the framework to remove */
			  /* from a connection profile */
    _STATIC_PROFILE_ITEM, /* a parameter created by the framework to add a */
			  /* plugin object to stack statically */
    _DYNAMIC_PROFILE_ITEM /* a parameter created by the framework to add a */
			  /* plugin object to stack dynamically */
    } PFW_PARAMETER_TYPE;

typedef enum pfwProfileDataStatus
    {
    PFW_DATA_INHERITED,
    PFW_DATA_OWNED
    } PFW_PROFILE_DATA_STATUS;

typedef enum pfwProfileItemStatus
    {
    PFW_STATIC_PROFILE_ITEM,
    PFW_DYNAMIC_PROFILE_ITEM
    } PFW_PROFILE_ITEM_STATUS;

/*
 * Plugin objects (components and layers) can be configured differently from
 * one stack instance to another. Components and layers are configured using 
 * configuration parameters. Each configuration parameter is represented by a 
 * parameter object in the framework. The following data structure is defined 
 * for a parameter object.                                                    
 */

typedef struct pfwParameterObj
    {
    char name[PFW_MAX_NAME_LENGTH]; /* parameter name */
    PFW_PARAMETER_TYPE type;        /* partameter type */ 
    PFW_PLUGIN_OBJ *owner;          /* pointer to the owner of the parameter */
    PFW_PARAMETER_HANDLER handlerFunc; /* function to process the parameter */
    } PFW_PARAMETER_OBJ;


/*
 * Each stack instance is configured from a stack profile. A stack 
 * profile is implemented by a stack profile object. A stack profile
 * object is a list of lists. The elements of the outer list are for layer
 * object configuration information and the elements of the inner list are 
 * for component object information.
 */


typedef struct pfwComponentProfileItem PFW_COMPONENT_PROFILE_ITEM;
struct pfwComponentProfileItem
    {
    PFW_COMPONENT_OBJ *obj;       /* pointer to the component object */
    void *profileData;            /* pointer to the component profile data */
    unsigned int ownedOrInherited : 1;/* if profile data is inherited or owned*/
    unsigned int staticOrDynamic : 1; /* if profile item is static or dynamic */
    PFW_COMPONENT_PROFILE_ITEM *next; /* pointer to the next component profile*/
    };

typedef struct pfwLayerProfileItem PFW_LAYER_PROFILE_ITEM;

struct pfwLayerProfileItem
    {
    PFW_LAYER_OBJ *obj;           /* pointer to the component object */
    void *profileData;            /* pointer to the layer profile data */
    unsigned int ownedOrInherited : 1;/* if profile data is inherited or owned*/
    unsigned int staticOrDynamic : 1; /* if profile item is static or dynamic */
    PFW_LAYER_PROFILE_ITEM * next;/* pointer to the next layer profile */
    PFW_COMPONENT_PROFILE_ITEM *firstComponent;/* pointer to the profile item */
                                            /* for the first component of the */
					    /* layer */ 
    };

struct pfwProfileObj
    {
    unsigned int id;                /* position in the profile table */
    SEM_ID profileMutex;            /* profile object protection */
    char name[PFW_MAX_NAME_LENGTH];
    PFW_OBJ *pfwObj;                /* framework this profile belongs to */
    PFW_LAYER_PROFILE_ITEM *firstLayer[PFW_LAYERED_PLANE_NUMBER];
    };

extern STATUS  _pfwParameterAdd (PFW_PARAMETER_OBJ * parameterObj);
extern STATUS  _pfwProfileDelete (PFW_OBJ *, 
				  PFW_LAYER_PROFILE_ITEM *firstLayer[]);
extern STATUS  _pfwObjProfileDataShow (PFW_PLUGIN_OBJ * , void *profileData);
extern PFW_LAYER_PROFILE_ITEM 
	      *pfwNewLayerProfileItemAdd (PFW_LAYER_OBJ *layerObj, 
					  unsigned int planeId,
		                          PFW_LAYER_PROFILE_ITEM **firstLayer);
extern PFW_COMPONENT_PROFILE_ITEM
	      *pfwNewComponentProfileItemAdd (PFW_COMPONENT_OBJ *componentObj,
		                              PFW_LAYER_PROFILE_ITEM *layer);

extern STATUS  pfwProfileTreeCopy (PFW_LAYER_PROFILE_ITEM **baseFirstLayer,
				   PFW_LAYER_PROFILE_ITEM **newFirstLayer,
				   PFW_PROFILE_DATA_STATUS dataStatus,
				   BOOL check);
extern void    pfwProfileTableCreate (PFW_OBJ *pfwObj, unsigned int initialSize,
				      int maxSize);
extern void    pfwProfileTableDelete (PFW_OBJ *pfwObj);
extern void    pfwParameterTableCreate (PFW_OBJ *pfwObj, 
					unsigned int initialSize, int maxSize);
extern void    pfwParameterTableDelete (PFW_OBJ *pfwObj);

#ifdef __cplusplus
}
#endif

#endif /* __INCpfwProfilePh */

