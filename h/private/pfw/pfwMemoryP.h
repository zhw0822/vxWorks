/* memoryP.h - private memory header file */

/* Copyright 1999 Wind River Systems, Inc. */

/*
modification history
--------------------
01a,03oct99,koz  Changed for memory partition based memory allocations
01a,xxxxxxx,koz  written
*/

#ifndef __INCpfwMemoryPh
#define __INCpfwMemoryPh

#ifdef __cplusplus
extern "C" {
#endif

#include "semLib.h"

#define FIRST_INTERVAL_UNIT_SIZE_GRANULARITY 4
#define LOG2_FIRST_INTERVAL_UNIT_SIZE_GRANULARITY 2 
#define FIRST_INTERVAL_UNIT_POOL_NUM 32 
#define NON_FIRST_INTERVAL_UNIT_POOL_NUM 16 
#define LOG2_FIRST_INTERVAL_MAX_UNIT_SIZE 7  
#define LOG2_FIRST_CHUNK_SIZE 10 

typedef struct pfwMemoryUnit
    {
    void *chunkOrNext;
    } PFW_MEMORY_UNIT;

typedef struct pfwLargeMemoryUnit
    {
    PFW_MEMORY_UNIT memoryUnit;
    PART_ID partId;
    } PFW_LARGE_MEMORY_UNIT;

typedef struct pfwMemoryUnitPool PFW_MEMORY_UNIT_POOL;
typedef struct pfwMemoryChunkPool PFW_MEMORY_CHUNK_POOL;
typedef struct pfwMemoryManager PFW_MEMORY_MANAGER;
typedef struct pfwMemoryChunk PFW_MEMORY_CHUNK;

struct pfwMemoryChunk
    {
    DL_NODE node;                   /* node in a double link list chunks */   
    PFW_MEMORY_UNIT_POOL *unitPool; /* pointer to the unit pool using it */
    unsigned int id;                /* id of the chunk */
    unsigned int freeUnitNum;
    PFW_MEMORY_UNIT *freeUnitHead;  /* head of free memory units in the chunk */
    char *memory;                   /* pointer to the memory area of the chunk*/
    };


struct pfwMemoryUnitPool
    {
    unsigned int unitSize;
    unsigned int unitNumPerChunk;
    DL_LIST chunkList;
    unsigned int chunkPoolIndex;
    PFW_MEMORY_MANAGER *manager;
    };

struct pfwMemoryChunkPool
    {
    unsigned int chunkSize;
    DL_LIST chunkList;
    };

struct pfwMemoryManager
    { 
    PART_ID partId;                        /* memory partion id */
    unsigned int maxUnitSize;              /* max memory unit size */
    unsigned int unitPoolNum;              /* number of unit pools */
    PFW_MEMORY_UNIT_POOL *unitPool;        /* array unit pools */
    unsigned int chunkPoolNum;             /* number of chunk pools */
    PFW_MEMORY_CHUNK_POOL *chunkPool;      /* array of chunk pools */
    SEM_ID mutex;                          /* mutex for exclusive access */ 
    };

extern STATUS pfwMemoryManagerCreate (PFW_OBJ *pfwObj, unsigned int maxUnitSize,
				      PART_ID partId);
extern STATUS pfwMemoryManagerDelete (PFW_OBJ *pfwObj);

#ifdef __cplusplus
}
#endif

#endif /* __INCpfwMemoryPh */


