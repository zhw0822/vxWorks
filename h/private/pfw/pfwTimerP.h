/* timerP.h - private timer header file */

/* Copyright 1999 Wind River Systems, Inc. */

/*
modification history
--------------------
01e,15oct99,koz  Added sysClkTicStart attribute to PFW_TIMER_UNIT_MANAGER
01d,19sep99,koz  Changed timer type to timer unit
01c,01aug99,koz  Added multiple timer manager functionality
01b,31jul99,koz  Cleaned up.
01a,23jul99,koz  written.
*/

#ifndef __INCpfwTimerPh
#define __INCpfwTimerPh

#ifdef __cplusplus
extern "C" {
#endif

#include "dllLib.h"
#include "semLib.h"
#include "wdLib.h"
#include "pfw/pfwTimer.h"

/* typedefs */
typedef struct pfwTimerManager PFW_TIMER_MANAGER;
typedef struct pfwTimeUnitManger PFW_TIME_UNIT_MANAGER;
typedef struct pfwTimerGroup PFW_TIMER_GROUP;

struct pfwTimerObj
    {
    DL_NODE node;                /* node of a double link list */
    PFW_TIMER_HANDLER handler;   /* timer handler */
    PFW_PLUGIN_OBJ_STATE *state; /* owner state, an arg to the timer handler */
    unsigned int arg;            /* second arg to the timer handler function */
    unsigned int unit;           /* timer unit (e.g., second and minute) */
    PFW_TIMER_GROUP *group;      /* pointer to the group of timers with the */
                                 /* same deadline*/
    };

struct pfwTimerGroup
    {
    DL_NODE node;               /* node of a double link list */
    DL_LIST timerObjList;       /* list of timer objects */
    unsigned int relativeTime;  /* timeout value relative to the previous */
                                /* timer group */
    BOOL expired;               /* if this group has been expired */
    PFW_TIME_UNIT_MANAGER       /* pointer to the timer manager */
        *manager;
    };

/* Unit specific timer manager */
struct pfwTimeUnitManger
    {
    DL_LIST timerGroupList;    /* list of timer groups */
    unsigned int ticks;        /* number of elapsed ticks of the watch */
                               /* dog timer */
    ULONG sysClkTickStart;     /* system clock tick value when watchdog is */
			       /* started */
    unsigned int deadline;     /* deadline for the first timer group in */
                               /* the timer group list */
    unsigned int sysClkTicks;  /* number of system clock ticks at which */
                               /* the watch dog timer expires */
    WDOG_ID watchDogId;        /* watchDog timer id */
    SEM_ID timerMutex;         /* Mutex to protect timer resources */
    SEM_ID semId;              /* semaphore to be given when a timer group */
                               /* is expired */
    };

struct pfwTimerManager
    {
    PFW_TIME_UNIT_MANAGER *timeUnitManager[PFW_TIME_UNIT_NUM];
    };

extern STATUS         pfwTimerManagerCreate (PFW_OBJ *pfwObj,
                                             unsigned int sysClkTickCounts[],
                                             SEM_ID semId);
extern STATUS         pfwTimerManagerDelete (PFW_OBJ *pfwObj);
extern PFW_TIMER_OBJ *pfwTimerExpire (PFW_OBJ *pfwObj);
extern STATUS         pfwTimerShow (PFW_OBJ *pfwObj);

#ifdef __cplusplus
}
#endif

#endif /* __INCpfwTimerPh */


