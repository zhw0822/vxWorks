/* pfwEventP.h - private protocol framework event manager header file  */

/* Copyright 1999 Wind River Systems, Inc. */

/*
modification history
--------------------
01d,17oct99,koz  Added event manager create and delete routines
01c,19sep99,koz  Changed the pfwEventItemAdd interface
01b,08sep99,koz  Modified event structures to associate events with stack
                 objects
01a,17aug99,koz  witten.
*/

#ifndef __INCpfwEventPh
#define __INCpfwEventPh

#ifdef __cplusplus
extern "C" {
#endif

#include "pfw/pfw.h"
#include "pfw/pfwEvent.h"

/* typedefs */

typedef struct pfwEventHandlerItem PFW_EVENT_HANDLER_ITEM;

struct pfwEventHandlerItem
    {
    PFW_EVENT_HANDLER_ITEM *next;     /* pointer to the next event handler */
				      /* item in the list */
    PFW_PLUGIN_OBJ_STATE *state;      /* pointer to a pfw plugin object */ 
				      /* registered for this event */ 
    PFW_EVENT_HANDLER eventHandler;   /* function pointer to an event handler */
    };

typedef struct pfwEventSubscriberItem PFW_EVENT_SUBSCRIBER_ITEM;

struct pfwEventSubscriberItem
    {
    PFW_EVENT_SUBSCRIBER_ITEM *next;  /* pointer to the next event handler */
				      /* item in the list */
    PFW_EVENT_OBJ *eventObj;          /* pointer to the event object */ 
    PFW_EVENT_HANDLER eventHandler;   /* function pointer to an event handler */
    };

typedef struct pfwEventItem PFW_EVENT_ITEM;

struct pfwEventItem
    {
    PFW_EVENT_ITEM *next;             /* pointer to the next event handler */
				      /* item in the list */
    PFW_EVENT_OBJ *eventObj;          /* pointer to the event object */ 
    PFW_EVENT_HANDLER_ITEM            /* head of the event handler item list */ 
	*eventHandlerItemHead;  
    };

struct pfwEventObj 
    {
    char name[PFW_MAX_NAME_LENGTH];    /* pfw event name */
    unsigned int id;                   /* pfw event id */
    PFW_OBJ* pfwObj;                   /* pointer to the framework object */
    };

extern STATUS pfwEventManagerCreate (PFW_OBJ *pfwObj);
extern STATUS pfwEventManagerDelete (PFW_OBJ *pfwObj);
extern STATUS pfwEventPluginObjUnsubscribe (PFW_PLUGIN_OBJ *pluginObj);
extern STATUS pfwEventItemAdd (PFW_PLUGIN_OBJ_STATE *state);
extern STATUS pfwEventItemDelete (PFW_PLUGIN_OBJ_STATE *state);
extern STATUS pfwEventItemListDelete (PFW_STACK_OBJ *state);

#ifdef __cplusplus
}
#endif

#endif /* __INCpfwEventPh */


