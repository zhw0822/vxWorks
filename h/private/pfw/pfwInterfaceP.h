/* pfwInterfaceP.h - private protocol framework interface manager header file  */

/* Copyright 1999 Wind River Systems, Inc. */

/*
modification history
--------------------
*/

#ifndef __INCpfwInterfacePh
#define __INCpfwInterfacePh

#ifdef __cplusplus
extern "C" {
#endif

#include "pfw/pfw.h"
#include "private/pfw/pfwP.h"
#include "private/pfw/pfwUtilP.h"
#include "pfw/pfwInterface.h"

/* typedefs */

typedef struct pfwPluginObjInterfaceItem
    {
    SL_NODE node;
    PFW_INTERFACE_OBJ *interfaceObj;
    } PFW_PLUGIN_OBJ_INTERFACE_ITEM;

typedef struct pfwInterfaceHead
    {
    DL_LIST interfaceObjList;
    char name[PFW_MAX_INTERFACE_NAME_LENGTH];    /* interface name */
    } PFW_INTERFACE_HEAD;

extern STATUS pfwInterfaceManagerCreate (PFW_OBJ *pfwObj);
extern STATUS pfwInterfaceManagerDelete (PFW_OBJ *pfwObj);
extern STATUS pfwInterfaceUnbind (PFW_PLUGIN_OBJ *pluginObj);

#ifdef __cplusplus
}
#endif

#endif /* __INCpfwInterfacePh */


