/* pgPoolVirtLibP.h - virtual page pool object library header file */

/* Copyright 1998-2004 Wind River Systems, Inc. */

/*
modification history
--------------------
01b,13sep04,zl   converted to private header file.
01a,08jul03,yp   written.
*/

#ifndef __INCpgPoolVirtLibPh
#define __INCpgPoolVirtLibPh

#ifdef __cplusplus
extern "C" {
#endif

/* includes */
#include "private/pgPoolLibP.h"

/* defines */

/* typedefs */

/* function declarations */

extern STATUS		pgPoolVirtLibInit (void);
extern PAGE_POOL_ID	pgPoolVirtCreate (VIRT_ADDR, UINT, UINT, PG_POOL_OPT);
extern STATUS		pgPoolVirtAddToPool (PAGE_POOL_ID , VIRT_ADDR, UINT);
extern STATUS		pgPoolVirtDelete (PAGE_POOL_ID);
extern BOOL		pgPoolVirtPageIsFree (PAGE_POOL_ID, VIRT_ADDR, UINT);
extern BOOL		pgPoolVirtPageIsAllocated (PAGE_POOL_ID, VIRT_ADDR,
						   UINT);
extern VIRT_ADDR	pgPoolVirtPageGet (PAGE_POOL_ID, UINT);
extern VIRT_ADDR	pgPoolVirtAlignedPageGet (PAGE_POOL_ID, UINT, UINT);
extern VIRT_ADDR	pgPoolVirtPageGetAt (PAGE_POOL_ID, VIRT_ADDR, UINT);
extern STATUS		pgPoolVirtPageRelease (PAGE_POOL_ID, VIRT_ADDR, UINT);
extern BOOL		pgPoolVirtPageRangeVerify (PAGE_POOL_ID , VIRT_ADDR , 
						   UINT);
extern UINT		pgPoolVirtPageSize (PAGE_POOL_ID);
extern STATUS		pgPoolVirtOptionsSet (PAGE_POOL_ID, PG_POOL_OPT, 
					  PG_POOL_OPT);
extern STATUS		pgPoolVirtOptionsGet (PAGE_POOL_ID, PG_POOL_OPT *);
extern STATUS		pgPoolVirtInfoGet (PAGE_POOL_ID, PAGE_POOL_INFO *);

#ifdef __cplusplus
}
#endif

#endif /* __INCpgPoolVirtLibPh */
