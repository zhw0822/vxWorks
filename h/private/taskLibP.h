/* taskLibP.h - private task library interface header */

/*
 * Copyright (c) 1984-2005 Wind River Systems, Inc.
 *
 * The right to copy, distribute or otherwise make use of this software
 * may be licensed only pursuant to the terms of an applicable Wind River
 * license agreement. 
 */

/*
modification history
--------------------
03g,30jul05,pad  Removed _exit() prototype (SPR 110038).
03g,15aug05,yvp  Moved KERNEL_TASK_EXC_STACK_SIZE to sys/taskdefs.h
03f,09aug05,mmi  Update WIND_TCB_REGS offset
03e,15jul05,yvp  Added KERNEL_TASK_EXC_STACK_SIZE.
03d,07jun05,yvp  Updated copyright. #include now with angle-brackets.
03c,23mar05,yvp  Added extern declarations for task(Ker/Usr)ExcStackSize.
03b,18mar05,yvp  Added prototype for taskPriInheritDataInit.
03a,14mar05,yvp  Added task(Under/Over)flowSize
02z,09mar05,kk   updated the offsets due to addition in WIND_TCB
02y,25sep04,dcc  updated TCB offsets.
02x,28sep04,fr   removed vTaskOpen prototype (SPR 101349)
02w,13sep04,md   added taskStopForce()
02v,16sep04,dbt  Added taskDbgUnlock() prototype (SPR #101075).
02u,30apr04,dcc  added WIND_TCB_UTCB offset.
02t,30apr04,yvp  Added WIND_TCB_P_K_STK_END.
02s,20apr04,hya  added taskReset prototype.
02r,22mar04,dcc  added vTaskOpen() prototype.
02q,23feb04,dcc  adjusted WIND_TCB offsets.
02p,18feb04,aeg  deprecated taskCreat().
02o,27nov03,aeg  added vTaskOpen() function prototype.
02n,19nov03,kk   added taskInitialize
02m,31oct03,dcc  adjusted WIND_TCB offsets as a result of OBJ_CORE expansion.
02l,30pcm03,pcm  implemented changes from code review
02k,20oct03,pcm  cleaned up WIND_TCB_xxx macros
02j,17sep03,kk   renamed WIND_TCB_P_K_STK_PTR to WIND_TCB_P_EXC_STK_BASE
02i,10sep03,dcc  moved taskCreate() prototype to taskLibCommon.h
02h,04aug03,elg  Add support for new task status: define WIND_STOP.
02g,24jul03,kam  updated WIND_TCB_REGS for SIMLINUX and SIMNT
02f,23jul03,kam  updated WIND_TCB_REGS for SIMSPARCSOLARIS
02e,07may03,pes  PAL conditional compilation cleanup, phase 2. Moved
		 ARM-specific #defines to arch/arm/regsArm.h.
02d,22jul03,pad  Moved _exit declaration here to solve coupling issues.
02c,18jul03,kk   change WIND_TCB_P_K_STK_PTR macro to point pKStackBase & 
		 pKStackPtr
02b,10jul03,kk   added WIND_TCB_P_K_STK_PTR & WIND_TCB_RTP_ID macro
02a,17apr03,dbt  Added WIND_TCB_REGS definition for Linux & Windows VxSim.
01z,25mar03,dbt  Added WIND_TCB_REGS definition for Solaris VxSim.
01y,10jul03,ymz  fixed WIND_TCB_ERRNO comment.
01x,01jul03,ymz  updated MIPS register offset for 8-byte alignement.
01w,01jul03,rec  code review comments.
01v,02jun03,rec  Power Management - added ARM WIND_TCB_R5 WIND_TCB_SP.
01u,28mar03,ymz  does not assume qNode is the first element in WIND_TCB, 
		 added WIND_TCB_QNODE.
01t,16jan02,to   added WIND_TCB_FPSTATUS for ARM.
01s,09nov01,dee  added coldfire portions from T2.1.0
01r,04oct01,yvp  fixed SPR69306: changed WIND_TCB_REGS to 0x130 for PPC.
01q,19sep01,aeg  updated WIND_TCB* macros to match taskLib.h v04j changes;
		 deleted Am29K family support.
01p,04sep01,hdn  moved I80X86 macros to regsI86.h.
01o,28feb00,frf  added SH support for T2.
01n,23jul98,mem  added pTaskLastDspTcb.
01m,23apr97,hk   added SH support.
01l,28nov96,cdp  added ARM support.
		 added WIND_TCB_PSTACKBASE.
01k,19mar95,dvs  removed tron references.
01j,26may94,yao  added PPC support.
01i,28jul94,dvs  added extern of pTaskLastFpTcb. (SPR #3033).
01h,12nov93,hdn  added support for I80X86.
01g,02dec93,pme  added Am29K family support.
01f,03aug93,jwt  abstracted common WIND_TCB_XXX definitions; copyright.
01e,22sep92,rrr  added support for c++.
01d,23aug92,jcf  added extern of taskBpHook.
01c,28jul92,jcf  changed offsets to match WIND_TCB.
01b,07jul92,ajm  removed external defines of taskSwapMaskClear/taskSwapMaskSet
01a,04jul92,jcf  written by extracting from taskLib.h v02x.
*/

#ifndef __INCtaskLibPh
#define __INCtaskLibPh

#ifdef __cplusplus
extern "C" {
#endif

/* task status values */

#define WIND_READY		0x00	/* ready to run */
#define WIND_SUSPEND		0x01	/* explicitly suspended */
#define WIND_PEND		0x02	/* pending on semaphore */
#define WIND_DELAY		0x04	/* task delay (or timeout) */
#define WIND_DEAD		0x08	/* dead task */
#define	WIND_STOP		0x10	/* stopped by the debugger */

#ifndef	_ASMLANGUAGE
#include <vxWorks.h>
#include <taskLib.h>
#include <classLib.h>
#include <objLib.h>
#include <stdarg.h>

/* variable declarations */

extern REG_INDEX taskRegName [];
extern FUNCPTR	taskBpHook;
extern FUNCPTR	taskCreateTable [];
extern FUNCPTR	taskSwitchTable [];
extern FUNCPTR 	taskDeleteTable [];
extern FUNCPTR 	taskSwapTable [];
extern int	taskSwapReference [];
extern WIND_TCB * pTaskLastFpTcb;
extern WIND_TCB * pTaskLastDspTcb;
extern int	taskOverflowSize;	/* overflow protection size */
extern int	taskUnderflowSize;	/* underflow protection size */
extern int	taskUsrExcStackSize;	/* default exception stack size */
extern int	taskKerExcStackSize;	/* kernel task exc stack size */

/* function declarations */

extern STATUS 	taskInitialize (WIND_TCB * pTcb, char * name, int priority, 
                                int options, char * pStackBase, int stackSize,
                                char * pExcStackBase, int excStackSize, 
			        FUNCPTR entry, int arg1, int arg2, int arg3, 
			        int arg4, int arg5, int arg6, int arg7, 
				int arg8, int arg9, int arg10);

extern int	taskCreat (char *name, int priority, int options, int stackSize,
		      	   FUNCPTR entryPt, int arg1, int arg2, int arg3,
		      	   int arg4, int arg5, int arg6, int arg7,
		      	   int arg8, int arg9, int arg10) 
_WRS_DEPRECATED ("please use taskCreate instead");

extern STATUS	taskTerminate (int tid);
extern STATUS	taskDestroy (int tid, BOOL dealloc, int timeout,
			     BOOL forceDestroy);
extern STATUS	taskUndelay (int tid);
extern void	taskBpHookSet (FUNCPTR bpHook);
extern void	taskIdListSort (int idList[], int nTasks);
extern STATUS	taskStop (int tid);
extern STATUS	taskStopForce (int tid);
extern STATUS	taskCont (int tid);
extern BOOL	taskIsStopped (int tid);
/*extern STATUS   _exit (int status);*/
extern STATUS   taskReset (int tid);
extern STATUS	taskDbgUnlock (void);
extern STATUS	taskPriInheritDataInit (int tid);

#else	/* _ASMLANGUAGE */

/* PRI_INHERIT_DATA offsets */

#define	PRI_INHERIT_PRI_MUTEX_CNT	(0x00)
#define PRI_INHERIT_PRI_INHERIT_CNT	(0x04)
#define PRI_INHERIT_PRI_NORMAL		(0x08)
#define PRI_INHERIT_PRI_DROP		(0x0c)

/* architecture-independent WIND_TCB defines */

#include "objLibP.h"

#define WIND_TCB_QNODE          (WIND_OBJ_CORE_SIZE)        /* 0x40 */
#define WIND_TCB_P_K_STK_BASE   (0x20+WIND_OBJ_CORE_SIZE)   /* 0x60 */
#define WIND_TCB_P_K_STK_END    (0x24+WIND_OBJ_CORE_SIZE)   /* 0x64 */
#define	WIND_TCB_PEXC_STK_START (0x28+WIND_OBJ_CORE_SIZE)   /* 0x68 */
#define WIND_TCB_EXC_CNT	(0x2c+WIND_OBJ_CORE_SIZE)   /* 0x6c */
#define	WIND_TCB_STATUS		(0x30+WIND_OBJ_CORE_SIZE)   /* 0x70 */
#define	WIND_TCB_PRIORITY	(0x34+WIND_OBJ_CORE_SIZE)   /* 0x74 */
#define WIND_TCB_PRI_INHERIT	(0x38+WIND_OBJ_CORE_SIZE)   /* 0x78 */
#define	WIND_TCB_LOCK_CNT	(0x3c+WIND_OBJ_CORE_SIZE)   /* 0x7c */
#define WIND_TCB_RTP_ID         (0x44+WIND_OBJ_CORE_SIZE)   /* 0x84 */
#define WIND_TCB_OPTIONS	(0x48+WIND_OBJ_CORE_SIZE)   /* 0x88 */
#define WIND_TCB_UTCB 		(0x4c+WIND_OBJ_CORE_SIZE)   /* 0x8c */
#define	WIND_TCB_SWAP_IN	(0x54+WIND_OBJ_CORE_SIZE)   /* 0x94 */
#define	WIND_TCB_SWAP_OUT	(0x56+WIND_OBJ_CORE_SIZE)   /* 0x96 */
#define	WIND_TCB_SAFE_CNT	(0x5c+WIND_OBJ_CORE_SIZE)   /* 0x9c */
#define	WIND_TCB_SAFETY_Q_HEAD	(0x60+WIND_OBJ_CORE_SIZE)   /* 0xa0 */
#define	WIND_TCB_ENTRY		(0x70+WIND_OBJ_CORE_SIZE)   /* 0xb0 */
#define WIND_TCB_PSTACKBASE     (0x74+WIND_OBJ_CORE_SIZE)   /* 0xb4 */
#define WIND_TCB_PSTACKEND	(0x78+WIND_OBJ_CORE_SIZE)   /* 0xb8 */
#define WIND_TCB_ERRNO		(0x80+WIND_OBJ_CORE_SIZE)   /* 0xc0 */
#define WIND_TCB_PFPCONTEXT	(0xa0+WIND_OBJ_CORE_SIZE)   /* 0xe0 */


/* architecture-dependent WIND_TCB defines */

#if CPU_FAMILY==MC680X0
#define	WIND_TCB_REGS		0x188
#define	WIND_TCB_DREGS	        WIND_TCB_REGS	0x188
#define	WIND_TCB_DREGS4		WIND_TCB_REGS + 0x00
#define	WIND_TCB_DREGS8		WIND_TCB_REGS + 0x04
#define	WIND_TCB_AREGS		WIND_TCB_REGS + 0x08
#define	WIND_TCB_AREGS4		WIND_TCB_REGS + 0x20
#define	WIND_TCB_AREGS8		WIND_TCB_REGS + 0x24
#define	WIND_TCB_SSP		WIND_TCB_REGS + 0x28
#define	WIND_TCB_FRAME1		WIND_TCB_REGS + 0x42
#define	WIND_TCB_SR		WIND_TCB_REGS + 0x42
#define	WIND_TCB_PC		WIND_TCB_REGS + 0x44
#define	WIND_TCB_FRAME2		WIND_TCB_REGS + 0x46
#define	WIND_TCB_FOROFF		WIND_TCB_REGS + 0x48
#endif	/* CPU_FAMILY==MC680X0 */

#if CPU_FAMILY==I960
#define	WIND_TCB_REGS		0x1a0

#define	TCB_REG_SET_BASE 	0x1a0
#define	TCB_REG_SET_SIZE 	0x8c
#define	WIND_TCB_R0   		TCB_REG_SET_BASE + 0x00
#define	WIND_TCB_PFP  		TCB_REG_SET_BASE + 0x00
#define	WIND_TCB_SP   		TCB_REG_SET_BASE + 0x04
#define	WIND_TCB_RIP  		TCB_REG_SET_BASE + 0x08
#define	WIND_TCB_R3   		TCB_REG_SET_BASE + 0x0c
#define	WIND_TCB_R4   		TCB_REG_SET_BASE + 0x10
#define	WIND_TCB_R8   		TCB_REG_SET_BASE + 0x20
#define	WIND_TCB_R12  		TCB_REG_SET_BASE + 0x30
#define	WIND_TCB_G0   		TCB_REG_SET_BASE + 0x40
#define	WIND_TCB_G4   		TCB_REG_SET_BASE + 0x50
#define	WIND_TCB_G8   		TCB_REG_SET_BASE + 0x60
#define	WIND_TCB_G12  		TCB_REG_SET_BASE + 0x70
#define	WIND_TCB_FP   		TCB_REG_SET_BASE + 0x7c
#define	WIND_TCB_PCW  		TCB_REG_SET_BASE + 0x80
#define	WIND_TCB_ACW  		TCB_REG_SET_BASE + 0x84
#define	WIND_TCB_TCW  		TCB_REG_SET_BASE + 0x88

#define WIND_TCB_DBG_STATE_PTR	TCB_REG_SET_BASE + 0x8c
#define WIND_TCB_RESUMPTION_RECORD	TCB_REG_SET_BASE + 0x90
#endif /* CPU_FAMILY==I960 */

#if     CPU_FAMILY==MIPS
#define WIND_TCB_REGS           0x198
#include "arch/mips/taskMipsLib.h"
#endif	/* MIPS */

#if	(CPU_FAMILY == PPC)
#define WIND_TCB_REGS		0x1a0 
#include "arch/ppc/regsPpc.h"
#endif	/* (CPU_FAMILY == PPC) */

#if	(CPU_FAMILY == SPARC)
#define	WIND_TCB_REGS		0x188 
#include "arch/sparc/regsSparc.h"
#endif	/* (CPU_FAMILY == SPARC) */

#if     CPU_FAMILY==I80X86
#define WIND_TCB_REGS           0x1d0 
#include "arch/i86/regsI86.h"
#endif  /* CPU_FAMILY==I80X86 */

#if	(CPU_FAMILY == SH)
#define WIND_TCB_REGS 		0x188 
#include "arch/sh/regsSh.h"
#endif	/* (CPU_FAMILY == SH) */

#if     CPU_FAMILY==ARM
#define WIND_TCB_FPSTATUS       0x184 
#define WIND_TCB_REGS   	0x190
#include "arch/arm/regsArm.h"
#endif  /* CPU_FAMILY==ARM */

#if CPU_FAMILY==COLDFIRE
#include "regs.h"
#define	WIND_TCB_REGS		0x188
#define	WIND_TCB_DREGS		(WIND_TCB_REGS+OFF_REG_D0)
#define	WIND_TCB_DREGS4		(WIND_TCB_REGS+OFF_REG_D1)
#define	WIND_TCB_DREGS8		(WIND_TCB_REGS+OFF_REG_D2)
#define	WIND_TCB_AREGS		(WIND_TCB_REGS+OFF_REG_A0)
#define	WIND_TCB_AREGS4		(WIND_TCB_REGS+OFF_REG_A1)
#define	WIND_TCB_AREGS8		(WIND_TCB_REGS+OFF_REG_A2)
#define	WIND_TCB_SSP		(WIND_TCB_REGS+OFF_REG_SP)
#define WIND_TCB_MAC		(WIND_TCB_REGS+OFF_REG_MAC)
#define WIND_TCB_MACSR		(WIND_TCB_REGS+OFF_REG_MACSR)
#define WIND_TCB_MASK		(WIND_TCB_REGS+OFF_REG_MASK)
#define WIND_TCB_HASMAC		(WIND_TCB_REGS+OFF_REG_HASMAC)
#define	WIND_TCB_SR		(WIND_TCB_REGS+OFF_REG_SR)
#define	WIND_TCB_PC		(WIND_TCB_REGS+OFF_REG_PC)
#endif	/* CPU_FAMILY==COLDFIRE */

#if	CPU_FAMILY==SIMLINUX
#define	WIND_TCB_REGS		0x188
#include "arch/simlinux/regsSimlinux.h"
#endif	/* CPU_FAMILY==SIMLINUX */

#if	CPU_FAMILY==SIMNT
#define	WIND_TCB_REGS		0x188
#include "arch/simnt/regsSimnt.h"
#endif	/* CPU_FAMILY==SIMLINUX */

#if	CPU_FAMILY==SIMSPARCSOLARIS
#define	WIND_TCB_REGS		0x198
#include "arch/simsolaris/regsSimsolaris.h"
#endif	/* CPU_FAMILY==SIMSPARCSOLARIS */

#endif	/* _ASMLANGUAGE */

#ifdef __cplusplus
}
#endif

#endif /* __INCtaskLibPh */
