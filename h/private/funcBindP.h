/* funcBindP.h - private function binding header */

/*
 * Copyright (c) 1992-1998, 2000-2005 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
04w,29sep05,kk   removed deprecated message for excJobAdd (SPR# 111818)
04v,27sep05,kk   added _func_fioFltFormatRtn and _func_fioFltScanRtn for
                 fioLib
04u,16aug05,gls  added objOwnerLibfunction pointers
04t,12aug05,pes  Add _func_dspProbe (conditional if _WRS_DSP_SUPPORT defined)
04s,08jul05,jln  added _func_windTickAnnounceHook
04r,25jun05,jln  Added _func_taskPxAttrSet/Get
04q,08jun05,yvp  Reverted excExcepHook.
04p,07jun05,yvp  Updated copyright. #include now with angle-brackets.
04o,07jun05,yvp  Renamed _func_jobExcAdd to _func_jobTaskWorkAdd.
04n,27apr05,yvp  Added _func_classListUnlock and _func_classListLock.
04m,26apr05,yvp  Removed _func_eventInit and _func_eventRsrcShow.
04l,06apr05,yvp  Added _func_event_* - decouple eventLib from sem's & msgQ's.
04k,18mar05,yvp  Added _func_excExcepHook and _func_exit.
04j,05feb05,dlk  Added _func_scMemValidate.
04i,10jan05,bpn  Added _func_dbgRtpStop, _func_dbgRtpCont entries.
04h,16dec04,yvp  Added _func_ioGlobalStdGet, _func_consoleOut & _func_event_*.
                 Added _func_isrCreate, _func_isrDelete.
		 Moved _func_isrDispatcher here from isrLib.
04g,23nov04,gls  added _func_rtpSig entries
04f,03nov04,hya  added _func_sigfillset
04e,06oct04,aeg  added _func_jobExcAdd
04d,30sep04,lei  added jobLibTaskInstalled
04c,29sep04,kk   added _func_symTblShutdown & _func_symRegister
04b,27sep04,gls  added _func_sdUnmapAll
04a,21sep04,zl   added __rta_longjmp
03z,18sep04,jn   Move toward shared code on host and target
03y,04sep04,dbt  Fixed _func_excJobAdd declaration.
03x,02sep04,ans  Added _func_jobAdd and deprecated _func_excJobAdd
03w,30aug04,dcc  Added declaration of _func_objHandleToObjId.
03v,26aug04,ymz  Added declaration of _func_rtpHandleIdGet.
03v,30aug04,jn   Add function bindings for unldLib
03w,23sep04,ann  Merge networking: Removed fastUdpInitialized var.
03v,23aug04,bpn  Added _func_symEach, _func_symFindByName (SPR#100715).
03u,04aug04,sru  Added declaration of _func_aimMmuLockRegionsImport.
03t,11jun04,pad  Added declaration of _func_rtpPthreadSetCancelType.
03s,08jun04,md   added loader function bindings
03r,17may04,pad  Added declaration of _func_loadRtpSymsPolicyOverride.
03q,14may04,mil  Added _func_mmuCacheFlushLoadAdrsGet.
03p,07may04,pad  Added declarations of func_rtpShlSymbolsRegister and
                 _func_rtpShlSymbolsUnregister.
03p,30apr04,pad  Added declaration of _func_shlRtpInfoGet.
03o,21apr04,cjj  Added _func_workQDbgTextDump and cleaned up file.
03n,16apr04,kk   added _func_shlRtpDeleteAll
03m,16apr04,yvp  Added FUNCPTRS for rtpHookLib functions.
03l,09apr04,ans  added _func_rtpTaskSigCheck
03k,07apr04,bpn  Addded _func_shellExcPrint.
03j,29mar04,jn   Add func pointer to allow use of kernel .o loader 
                 without rtp loader
03i,17mar04,pad  Added external declaration for _func_envGet.
03h,19feb04,yp   adding _func_virtExtraMapInfoGet
03g,16dec03,syy  Added func ptr _func_evtLogOBinary
03f,26nov03,jn   Add _func_rtpIdVerify
03e,19nov03,kk   remove rtpOrEdrInstalled
03d,13nov03,yp   added declaration for _func_virtSegInfoGet
03c,10nov03,tam  added _func_pgMgrPageAlloc _func_pgMgrPageMap
                 _func_pgMgrPageFree
03b,19sep03,aim  added _func_edrEvt
03a,10sep03,dbs  remove edrErrorInject funcptr
02z,06nov03,nrj  added signal block func for RTP
02y,21oct03,nrj  added RTP task exception signal functions
02x,16oct03,nrj  added missing RTP signal funcs
02w,16oct03,nrj  added support for RTP signal functions
02k,03nov03,tcr  add WindView syscall logging rtn
02j,12sep03,tcr  remove wvObjIsEnabled
02i,02sep03,tcr  add funcptr for WindView
02h,22aug03,dbs  ED&R merge to baseline
02g,18aug03,dbs  add _func_edrErrorInject
02f,29jul03,dbs  add _func_regsShow
02v,19sep03,pad  Added declaration for _func_rtpSpawn
02u,19sep03,kk   update comment on rtpOrEdrInstalled
02t,12sep03,kk   added variable rtpOrEdrInstalled for scalability
02s,22aug03,dbs  ED&R merge to baseline
02r,18aug03,dbs  add _func_edrErrorInject
02q,29jul03,dbs  add _func_regsShow
02p,01aug03,pch  support coprocessor on PPC604/AltiVec for PPC32
02o,10apr03,pes  PAL Coprocessor Abstraction
02n,22jul03,pad  Added external declaration for _func_rtpDelete.
02m,20mar03,wap  added m2If64BitCounters (SPR #86776)
02l,28feb03,bpn  Changed _func_bdall to _func_dbgTaskBpRemove.
02k,13jan03,rae  Merged from velocecp branch
02j,21nov02,pch  SPR 84107: add _func_altivecTaskRegsGet and _func_altivecProbe
02g,09jun03,dtr  CP1 Merge.
02f,03sep02,dtr  ADDED _func_speTaskRegsShow.
02i,26mar02,pai  added _func_sseTaskRegsShow (SPR 74103).
02h,09nov01,jn   added internal API for symLib
02g,29oct01,gls  added pthread support
02f,26oct01,brk  added _func_selPtyAdd & _func_selPtyDelete (SPR 65498)
02e,21sep01,aeg  added _func_selWakeupListTerm.
02d,16mar01,pcs  ADDED _func_altivecTaskRegsShow
02c,28feb00,frf  Add SH support for T2
02b,08aug98,kab  added _func_dspRegsListHook, _func_dspMregsHook.
02a,23jul98,mem  added _func_dspTaskRegsShow
01z,10aug98,pr   added WindView function pointers for i960
01y,15apr98,cth  added definition of _func_evtLogReserveTaskName
01x,08apr98,pr   added _func_evtLogT0_noInt. Set evtAction as UINT32
01w,13dec97,pr   moved some variables from eventP.h
		 removed some windview 1.0 variables
01v,13nov97,cth  removed evtBuf and scrPad references for WV2.0
01u,24jun97,pr   added evtInstMode
01t,09oct97,ms   added _func_ioTaskStdSet
01s,21feb97,tam  added _dbgDsmInstRtn
01r,08jul96,pr   added _func_evtLogT1_noTS
01q,12may95,p_m  added _func_printErr, _func_symFindByValue, _func_spy*
                       _func_taskCreateHookAdd and _func_taskDeleteHookAdd.
01p,24jan94,smb  added function pointers for windview portable kernel.
01o,10dec93,smb  added function pointers for windview.
01n,05sep93,jcf  added _remCurId[SG]et.
01m,20aug93,jmm  added _bdall
01l,22jul93,jmm  added _netLsByName
01k,13feb93,kdl  added _procNumWasSet.
01j,13nov92,jcf  added _func_logMsg.
01i,22sep92,rrr  added support for c++
01h,20sep92,kdl  added _func_ftpLs, ftpErrorSuppress.
01g,31aug92,rrr  added _func_sigprocmask
01f,23aug92,jcf  added _func_sel*, _func_excJobAdd,_func_memalign,_func_valloc
01e,02aug92,jcf  added/changed _exc*.
01d,29jul92,jcf  added _func_fclose
01c,29jul92,rrr  added _func_sigExcKill, _func_sigTimeoutRecalc,
                 _func_excEsfCrack and _func_excSuspend.
01b,19jul92,pme  added _func_smObjObjShow.
01a,04jul92,jcf  written
*/

#ifndef __INCfuncBindPh
#define __INCfuncBindPh

#ifdef __cplusplus
extern "C" {
#endif

#include <vxWorks.h>
#include <semLib.h>

/* variable declarations */


extern FUNCPTR     _func_aimMmuLockRegionsImport;
extern FUNCPTR     _func_classListUnlock;
extern FUNCPTR     _func_classListLock;
extern FUNCPTR     _func_consoleOut;
extern VOIDFUNCPTR _func_edrEvt;
extern FUNCPTR     _func_eventReceive;
extern FUNCPTR     _func_eventRsrcSend;
extern FUNCPTR     _func_eventSend;
extern VOIDFUNCPTR _func_eventStart;
extern VOIDFUNCPTR _func_eventTerminate;
extern VOIDFUNCPTR _func_evtLogM0;
extern VOIDFUNCPTR _func_evtLogM1;
extern VOIDFUNCPTR _func_evtLogM2;
extern VOIDFUNCPTR _func_evtLogM3;
extern VOIDFUNCPTR _func_evtLogO;
extern VOIDFUNCPTR _func_evtLogOBinary;
extern VOIDFUNCPTR _func_evtLogOIntLock;
extern FUNCPTR     _func_evtLogPoint;
extern FUNCPTR	   _func_evtLogReserveTaskName;
extern VOIDFUNCPTR _func_evtLogString;
extern VOIDFUNCPTR _func_evtLogSyscall;
extern VOIDFUNCPTR _func_evtLogT0;
extern VOIDFUNCPTR _func_evtLogT0_noInt;
extern VOIDFUNCPTR _func_evtLogT1;
extern VOIDFUNCPTR _func_evtLogT1_noTS;
extern VOIDFUNCPTR _func_evtLogTSched;
extern FUNCPTR     _func_excBaseHook;
extern FUNCPTR     _func_excInfoShow;
extern FUNCPTR     _func_excIntHook;
extern FUNCPTR     _func_excJobAdd;
extern FUNCPTR     _func_jobExcAdd;
extern FUNCPTR     _func_excPanicHook;
extern VOIDFUNCPTR _func_exit;
extern FUNCPTR     _func_fastUdpErrorNotify;
extern FUNCPTR     _func_fastUdpInput;
extern FUNCPTR     _func_fastUdpPortTest;
extern FUNCPTR	   _func_fioFltFormatRtn;
extern FUNCPTR	   _func_fioFltScanRtn;
extern FUNCPTR     _func_fclose;
extern FUNCPTR     _func_ftpLs;
extern FUNCPTR     _func_ioGlobalStdGet;
extern FUNCPTR     _func_ioTaskStdSet;
extern FUNCPTR     _func_isrCreate;
extern FUNCPTR     _func_isrDelete;
extern FUNCPTR     _func_isrDispatcher;
extern FUNCPTR     _func_jobAdd;
extern FUNCPTR     _func_jobTaskWorkAdd;
extern FUNCPTR     _func_logMsg;
extern FUNCPTR     _func_memalign;
extern FUNCPTR     _func_netLsByName;
extern FUNCPTR     _func_objHandleToObjId;
extern FUNCPTR     _func_objOwnerListAdd;
extern FUNCPTR     _func_objOwnerListRemove;
extern FUNCPTR     _func_objOwnerReclaim;
extern FUNCPTR     _func_objOwnerSetBaseInternal;
extern FUNCPTR     _func_objOwnerSetInternal;
extern FUNCPTR     _func_pgMgrPageAlloc;
extern FUNCPTR     _func_pgMgrPageMap;
extern FUNCPTR     _func_pgMgrPageFree;
extern FUNCPTR     _func_printErr;
extern FUNCPTR     _func_loadRtpDeltaBaseAddrApply;
extern FUNCPTR     _func_loadRtpSymsPolicyOverride;
extern FUNCPTR     _func_pthread_setcanceltype;
extern FUNCPTR     _func_regsShow;
extern FUNCPTR     _func_remCurIdGet;
extern FUNCPTR     _func_remCurIdSet;
extern FUNCPTR     _func_rtpDelete;
extern FUNCPTR     _func_rtpIdVerify;
extern FUNCPTR     _func_rtpPause;	        /* RTP pause function pointer */
extern FUNCPTR     _func_rtpSigaction;		/* RTP sigaction function pointer */
extern VOIDFUNCPTR _func_rtpSigExcKill; 	/* RTP task signal exception processing */
extern FUNCPTR     _func_rtpSigPendDestroy;	/* RTP Signal pend dest function pointer */
extern FUNCPTR     _func_rtpSigpending;	        /* RTP sigpending function pointer */
extern FUNCPTR     _func_rtpSigPendInit;	/* RTP Signal pend init function pointer */
extern FUNCPTR     _func_rtpSigPendKill;	/* RTP Signal RTP Kill function pointer */
extern FUNCPTR     _func_rtpSigprocmask;	/* RTP sigprocmask function pointer */
extern FUNCPTR     _func_rtpSigqueue;		/* RTP sigqueue function pointer */
extern FUNCPTR     _func_rtpSigsuspend;	        /* RTP sigsuspend function pointer */
extern FUNCPTR     _func_rtpSigtimedwait;	/* RTP sigtimedwait function pointer */
extern FUNCPTR     _func_rtpSpawn;
extern FUNCPTR     _func_rtpTaskKill;
extern FUNCPTR     _func_rtpTaskSigPendKill;	/* RTP Signal RTP Task Kill function pointer */
extern FUNCPTR     _func_rtpTaskSigqueue;	/* RTP Signal RTP Task queue function pointer */
extern VOIDFUNCPTR _func_rtpTaskSigBlock;
extern FUNCPTR     _func_rtpPreCreateHookAdd;
extern FUNCPTR     _func_rtpPreCreateHookDelete;
extern FUNCPTR     _func_rtpPostCreateHookAdd;
extern FUNCPTR     _func_rtpPostCreateHookDelete;
extern FUNCPTR     _func_rtpInitCompleteHookAdd;
extern FUNCPTR     _func_rtpInitCompleteHookDelete;
extern FUNCPTR     _func_rtpDeleteHookAdd;
extern FUNCPTR     _func_rtpDeleteHookDelete;
extern FUNCPTR     _func_rtpHandleIdGet;
extern FUNCPTR     _func_rtpShow;
extern FUNCPTR     _func_rtpShlSymbolsRegister;
extern FUNCPTR     _func_rtpShlSymbolsUnregister;
extern FUNCPTR     _func_rtpPthreadSetCancelType;
extern FUNCPTR	   _func_scMemValidate;
extern FUNCPTR     _func_sdUnmapAll;
extern FUNCPTR     _func_selPtyAdd;
extern FUNCPTR     _func_selPtyDelete;
extern FUNCPTR     _func_selTyAdd;
extern FUNCPTR     _func_selTyDelete;
extern FUNCPTR     _func_selWakeupAll;
extern FUNCPTR     _func_selWakeupListInit;
extern FUNCPTR     _func_selWakeupListTerm;
extern VOIDFUNCPTR _func_shlRtpDeleteAll;
extern FUNCPTR     _func_shlRtpInfoGet;
extern VOIDFUNCPTR _func_sigExcKill;
extern FUNCPTR     _func_sigprocmask;
extern FUNCPTR     _func_sigfillset;
extern FUNCPTR     _func_sigTimeoutRecalc;
extern FUNCPTR     _func_smObjObjShow;
extern FUNCPTR     _func_spy;
extern FUNCPTR     _func_spyStop;
extern FUNCPTR     _func_spyClkStart;
extern FUNCPTR     _func_spyClkStop;
extern FUNCPTR     _func_spyReport;
extern FUNCPTR     _func_spyTask;
extern FUNCPTR     _func_symEach;
extern FUNCPTR     _func_symFindByName;
extern FUNCPTR     _func_symFindByValueAndType;   /* obsolete - do not use. */
extern FUNCPTR     _func_symFindByValue;          /* obsolete - do not use. */
extern FUNCPTR     _func_symFindSymbol;
extern FUNCPTR     _func_symNameGet;
extern FUNCPTR     _func_symRegister;
extern FUNCPTR     _func_symTblShutdown;
extern FUNCPTR     _func_symValueGet;
extern FUNCPTR     _func_symTypeGet;
extern FUNCPTR     _func_taskCreateHookAdd;
extern FUNCPTR     _func_taskDeleteHookAdd;
extern FUNCPTR	   _func_taskMemCtxSwitch; 	/* switch task memory context */
extern FUNCPTR     _func_taskStackAlloc;
extern FUNCPTR     _func_taskStackFree;
extern FUNCPTR     _func_tmrStamp;
extern FUNCPTR     _func_tmrStampLock;
extern FUNCPTR     _func_tmrFreq;
extern FUNCPTR     _func_tmrPeriod;
extern FUNCPTR     _func_tmrConnect;
extern FUNCPTR     _func_tmrEnable;
extern FUNCPTR     _func_tmrDisable;
extern VOIDFUNCPTR _func_trgCheck;
extern FUNCPTR     _func_valloc;
extern FUNCPTR     _func_virtSegInfoGet;
extern FUNCPTR     _func_virtExtraMapInfoGet;
extern FUNCPTR     _func_workQDbgTextDump;
extern FUNCPTR     _func_moduleSegInfoGet;
extern FUNCPTR     _func_kernelModuleListIdGet;
extern FUNCPTR     _func_moduleIdFigure;
extern FUNCPTR     _func_unldByModuleId;
extern FUNCPTR     _func_taskPxAttrSet;
extern FUNCPTR     _func_taskPxAttrGet;
extern FUNCPTR     _func_windTickAnnounceHook;

/* cache/MMU communication */
extern FUNCPTR     _func_mmuCacheFlushLoadAdrsGet;
    
#if defined (_WRS_PAL_COPROC_LIB)
extern FUNCPTR     _func_coprocShow;   
extern FUNCPTR     _func_coprocTaskRegsShow;   
#if defined (_WRS_DSP_SUPPORT)
extern FUNCPTR     _func_dspProbe;
#endif /* _WRS_DSP_SUPPORT */
#else /* !_WRS_PAL_COPROC_LIB */
IMPORT FUNCPTR	   _func_dspMregsHook;		/* arch dependent mRegs() hook */
IMPORT VOIDFUNCPTR _func_dspRegsListHook;	/* arch dependent DSP regs list */
extern FUNCPTR     _func_dspTaskRegsShow;
extern FUNCPTR     _func_fppTaskRegsShow;
extern FUNCPTR     _func_sseTaskRegsShow;
#ifdef _WRS_ALTIVEC_SUPPORT
extern FUNCPTR     _func_altivecTaskRegsGet;
extern FUNCPTR     _func_altivecTaskRegsShow;
#endif /* _WRS_ALTIVEC_SUPPORT */
#ifdef _WRS_SPE_SUPPORT
extern FUNCPTR     _func_speTaskRegsShow;
#endif /* _WRS_SPE_SUPPORT */
#endif /* _WRS_PAL_COPROC_LIB */

#ifdef _WRS_ALTIVEC_SUPPORT
extern FUNCPTR     _func_altivecProbe;
#endif /* _WRS_ALTIVEC_SUPPORT */

extern FUNCPTR     excExcepHook;

extern FUNCPTR	   _dbgDsmInstRtn;

/* Memory partition/class Id's */

extern CLASS_ID memPartClassId;	/* partition class id */
extern PART_ID  memSysPartId;		/* sys partition id */

extern BOOL	   ftpErrorSuppress;
extern BOOL	   _procNumWasSet;
extern int	   m2If64BitCounters;

extern UINT32 evtAction;
extern UINT32 wvEvtClass;
extern UINT32 trgEvtClass;
extern FUNCPTR	_func_rtpTaskSigCheck;		/* RTP task signal check */

/* envLib */

extern FUNCPTR _func_envGet;

/* Shell routines */

extern VOIDFUNCPTR _func_shellExcPrint;

/* hook for RTC support */

extern VOIDFUNCPTR __rta_longjmp;

/* check whether job task exists */

extern BOOL jobLibTaskInstalled;

/* Debug routines */

extern FUNCPTR	_func_dbgRtpStop;               /* stop an RTP */
extern FUNCPTR	_func_dbgRtpCont;               /* continue an RTP */
extern FUNCPTR  _func_dbgTaskBpRemove;

#ifdef __cplusplus
}
#endif

#endif /* __INCfuncBindPh */
