/* rtpLibP.h - private real time process library header */

/*
 * Copyright (c) 2003-2005 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
02i,11may05,gls  added zombieHandleId to ZOMBIE_RTP (SPR #109170)
02h,08mar05,kk   added cpuTimeInfo[2] and pSchedInfo to WIND_RTP
02g,25feb05,pcs  Updated the prototype of fn. rtpLibInit to handle different
                 overflow and underflow sizes for the execution and exception
                 stacks.
02f,18oct04,kk   clean up comments
02e,06oct04,dcc  added objMgtSemId to WIND_RTP
02d,27sep04,ans  Added reserved, spare fields to WIND_RTP.
02c,22sep04,ans  added rtpLibInit() rtpShowInit() rtpShellCmdInit()
02b,10sep04,kk   renamed rtpBaseLib to kernelBaseLib
02a,18aug04,pad  Added pUsrPthreadInfo field in wind_rtp structure.
01z,02sep04,yvp  rtpHookLibInit() prototype change.
01y,17aug04,yvp  Added exitCode parameter to RTP delete hooks.
01x,05jul04,dcc  added handle table growing support.
01w,05may04,yvp  Added extra parameter to RTP Pre-create hooks
01v,26apr04,ans  added macros to get state definition
01u,15apr04,kk   added rtpHandleId
01t,14apr04,kk   added initTaskHandle for initTaskId
01s,08apr04,yvp  Added RTP Init-Complete hooks.
01r,31mar04,jn   Rename rtpLoadLib -> loadRtpLib
01q,11mar04,job  Moved ACCESS_DENIED error value to public header file.
01s,30mar04,dms  Add rtpShlNodeList and rtpShlNodeListMutex fields to the
                 wind_rtp structure.
01q,02mar04,kk  added rtpVar to RTP data structure
01p,19feb04,dat  chg tableSize from int to size_t
01o,05nov03,md   added lockTaskId for tracking task which has rtp locked
01n,02dec03,dcc  added pHandleTable member to the WIND_RTP structure.
01m,26nov03,jn   Add declaration for rtpIdVerify
01l,01dec03,nrj  struct names changed as per coding std
01k,12nov03,pmr  added default path to RTP struct.
01j,25nov03,nrj  code review mods
01i,23oct03,gls  added MEM_INFO to KERNEL_RTP_CMN
		 removed adrsSpaceId from KERNEL_RTP_CMN 
                 removed mmList from WIND_RTP
01h,13nov03,yvp  Added RTP hook typedefs.
01g,24oct03,nrj  added support for parent-child RTP
01f,21oct03,nrj  moved kernel specific macros to rtpBaseLib.h
01e,15oct03,nrj  sig lib includes the RTP and not the other way
01d,12oct03,nrj  added signal feature
01c,25aug03,x_p  Took from base6_itn2_rtp-dev.al
01g,11sep03,zl   added mmList to WIND_RTP.
01f,07sep03,kk   update NODE_TO_RTP to reflect KERNEL_RTP_CMN addition
01e,04sep03,nrj  rationalized kernel RTP
01d,29aug03,pad  Updated binaryInfo field RTP_IMAGE_INFO type.
01c,14aug03,job  Added some useful macros
01b,15jul03,pad  Added fields for segment info in WIND_RTP structure.
01a,26jun03,nrj  created.
*/

#ifndef	__INCrtpLibPh
#define	__INCrtpLibPh

#ifdef	_ASMLANGUAGE
#else	/* _ASMLANGUAGE */

#ifdef __cplusplus
extern "C" {
#endif

/* includes */

#include "vxWorks.h"
#include "rtpLib.h"
#include "classLib.h"
#include "dllLib.h"
#include "lstLib.h"
#include "envLib.h"
#include "semLib.h"
#include "private/objLibP.h"
#include "private/loadRtpLibP.h"
#include "private/rtpSigLibP.h"
#include "private/memInfoP.h"
#include "private/rtpPthreadLibP.h"

/* externs */

extern CLASS_ID	rtpClassId;
extern DL_LIST	rtpList;

/* macros */

#define NUM_RTP_HOOK_TBLS	4

/* 
 * Macros to update the RTP state and status field.
 * Using the XX_PUT_XX macros, one can write multiple state/status value(s).
 * Using the XX_SET_XX / XX_UNSET_XX macros, one can set or unset a specific 
 * state/status value.
 * The XX_GET_XX macros are defined in rtpLibCommon.h, so that user code
 * can inspect the state/status of an RTP after the rtpInfoGet() system call.
 */

#define RTP_STATE_PUT(pValue, value) (*((UINT32 *)pValue) = \
         (((value) & RTP_STATE_MASK) | (*((UINT32 *)pValue) & RTP_STATUS_MASK)))
#define RTP_STATE_SET(pValue, value) (*((UINT32 *)pValue) |= \
                                      ((value) & RTP_STATE_MASK))
#define RTP_STATE_UNSET(pValue, value) (*((UINT32 *)pValue) = \
          (*((UINT32 *)pValue) & ((~(value)) & RTP_STATE_MASK)) | \
          (*((UINT32 *)pValue) & RTP_STATUS_MASK))

#define RTP_STATUS_PUT(pValue, value) (*((UINT32 *)pValue) = \
         (((value) & RTP_STATUS_MASK) | (*((UINT32 *)pValue) & RTP_STATE_MASK)))
#define RTP_STATUS_SET(pValue, value) (*((UINT32 *)pValue) |= \
                                      ((value) & RTP_STATUS_MASK))
#define RTP_STATUS_UNSET(pValue, value) (*((UINT32 *)pValue) = \
          (*((UINT32 *)pValue) & ((~(value)) & RTP_STATUS_MASK)) |  \
          (*((UINT32 *)pValue) & RTP_STATE_MASK))


/* Macro to get to the RTP_ID using the rtpNode pointer */

#define NODE_TO_RTP(pNode)	((RTP_ID)((int) pNode - sizeof(OBJ_CORE) - \
				 sizeof (KERNEL_RTP_CMN)))

/* RTP_LOCK locks the RTP semaphore without verifiying its object and state */

#define RTP_LOCK(rtpId)		(semTake ((rtpId)->semId, WAIT_FOREVER))

/* RTP_UNLOCK releases the semaphore of an RTP */

#define RTP_UNLOCK(rtpId)	(semGive ((rtpId)->semId))

/* RTP_SIG_RCB_GET obtains the signal data pointer for an RTP */

#define RTP_SIG_RCB_GET( rtpId ) (&((rtpId)->signalInfo))

/* RTP_VERIFIED_LOCK locks the RTP sem after verifiying the object and state */

#define RTP_VERIFIED_LOCK(rtpId)	rtpVerifyAndLock ((rtpId))

/*
 * RTP_VERIFIED_LOCK_TASKID locks the owner RTP semaphore  of the given task
 * after verifiying the task object,  the RTP object and the RTP state.
 */

#define RTP_VERIFIED_LOCK_TASKID(tid) rtpVerifyTaskIdAndLock ((tid))

#define RTP_ID_VERIFY(rtpId)	(OBJ_VERIFY ((rtpId), (rtpClassId)))

/* typedefs */

/*
 * KERNEL_RTP_CMN: structure containing a collection of common elements 
 * 		   between the kernelId and user RTPs.
 * ctxCnt: number of tasks using the vm context of RTP/kernel, can't 
 *         delete if non zero.
 * memInfo: RTP/Kernel virtual memory info. 
 * sharedDataList: list of shared data regions mapped.
 * pWdbInfo: pointer to WDB info.
 */

typedef struct kernelRtpCommon
    {
    INT32		ctxCnt;		/* number of tasks in this context */
    MEM_INFO *		memInfo;	/* pointer to memory structure */
    DL_LIST		sharedDataList;	/* List of the attached SD rgns */
    void *		pWdbInfo;	/* pointer to debugger specific info */
    } KERNEL_RTP_CMN;

/* 
 * objCore: includes RTP name, ID 
 * kernelRtpCmn: common between RTP and Kernel object
 * rtpNode: the rtp is linked to its parent when it becomes zombie via this node
 * semId:  semaphore to protect the RTP structure.
 * rtpStat (status): placeholder to represent the state and status of the RTP.
 * pPathName: Pointer to the executable file pathname string.
 * pArgv:Pointer to argv[] as supplied during creation.
 * pEnv: Pointer to envp[] as supplied during creation.
 * options: the options that are supplied during creation.
 * initTaskId: the task ID of the intial task
 * initTaskHandle: the initial task user handle ID, for user space.
 * memInfo: The memory information structure pointer,
 *          e.g. page pool lists or heap partition ID. 
 * symTabId: RTP's  symbol table.
 * binaryInfo: information about RTP code.
 * taskCnt: Number of tasks in the RTP.
 * fdTable: Pointer to shared FD table.
 * fdTableSize: number of entries in the FD table.
 * taskExitRtn: pointer to taskExit() in the RTP address space.
 * sigReturnRtn: Pointer to signal return routine in the RTP address space.
 * waitQHead: q of tasks waiting for RTP to die - to support waitpid().
 * signalInfo: RTP global Signal control block.
 * pSigQFreeHead: pointer to free pool of sigpend nodes (sigqueue).
 * sigwaitQ: list of tasks waiting for signals.
 * rtpChildList: list of RTP child and zombie RTPs attached to this RTP.
 * parentRtpId: ID of the RTP which created this RTP.
 * objMgtSemId: object management semaphore lock.
 * mstrHandleTbl: per RTP master handle table.
 * lockTaskId: ID of the task which locked the RTP.
 * rtpHandleId: handle ID of this RTP, for abstraction in user space.
 * pRtpVar: RTP specific variables, for SL support.
 * rtpShlNodeList: list of shared library references.
 * rtpShlNodeListMutex: mutex semaphore protecting the rtpShlNodeList list.
 * pUsrPthreadInfo: pointer to user-side pthreadLib information.
 */

typedef struct wind_rtp
    {
    OBJ_CORE		objCore;	/* object management */
    KERNEL_RTP_CMN	kernelRtpCmn;	/* common between RTP and Kernel obj */
    DL_NODE		rtpNode;	/* link of this zombie to its parent */
    SEM_ID		semId;		/* semaphore to protect this struct */
    UINT32		status;	        /* state and status of the RTP */
    char *		pPathName;	/* pointer to pathname of RTP */
    char **		pArgv;		/* pointer to arguments */
    char **		pEnv;		/* pointer to environment */
    UINT32		options;	/* option bits, e.g. debug, symtable */
    void *		entrAddr;	/* entry point of ELF file */
    int 		initTaskId;	/* the initial task ID */
    OBJ_HANDLE 		initTaskHandle;	/* the initial task user handle ID */
    int			symTabId;	/* symbol table  */
    RTP_IMAGE_INFO	binaryInfo;	/* information about RTP code */
    UINT		taskCnt;	/* number of tasks in the RTP */
    Q_HEAD		deleteQHead;	/* q of tasks waiting the RTP delete */
    void  *		fdTable;	/* fd table */
    size_t		fdTableSize;	/* number of entries in fd table */
    char *              defPath;        /* default path for I/O */
    size_t              defPathLen;     /* length of default path buffer */
    VOIDFUNCPTR         taskExitRtn;	/* Pointer to task exit routine */
    VOIDFUNCPTR		sigReturnRtn;	/* Pointer to signal return routine */
    RTP_SIG_CB		signalInfo;	/* Signal control block */
    struct sigpend *	pSigQFreeHead;	/* ptr to free pool of sigpends */
    DL_LIST		sigwaitQ;	/* tasks waiting for signals */
    DL_LIST		rtpChildList;	/* list of Child RTP zombies */
    struct wind_rtp *	parentRtpId;	/* RTP ID of this RTP's parent */
    SEM_ID		objMgtSemId;    /* object management lock */ 
    MASTER_HANDLE_TABLE mstrHandleTbl;  /* per RTP master handle table */
    int 		lockTaskId;	/* the task which has the RTP locked */
    OBJ_HANDLE		rtpHandleId;	/* handle ID of this RTP */
    struct rtp_var *	pRtpVar;	/* RTP specific variables */
    DL_LIST		rtpShlNodeList;	/* list of SHL references */
    SEM_ID		rtpShlNodeListMutex; /* lock for rtpShlNodeList */
    PTHREAD_INFO *	pUsrPthreadInfo;/* user pthreadLib info */

    /* extensions for plug-in schedulers */

    UINT64              cpuTimeInfo[2]; /* sched extension - CPU time info */
    void *              pSchedInfo;     /* ptr to scheduler info */

    int                 reserved1;      /* possible WRS extension */
    int                 reserved2;      /* possible WRS extension */
    int                 spare1;         /* possible user extension */
    int                 spare2;         /* possible user extension */
    int                 spare3;         /* possible user extension */
    int                 spare4;         /* possible user extension */
    } WIND_RTP;

/* small footprint zombie data struct for dead child RTP, chained to parent */

typedef struct zombieRtp
    {
    DL_NODE		rtpNode;	/* link of this zombie to its parent */
    WIND_RTP	 	* zombieId;	/* RTP ID of this RTP's parent */
    OBJ_HANDLE	 	zombieHandleId;	/* handle ID of this RTP's parent */
    int			exitStatus;	/* exit/termination status */
    } ZOMBIE_RTP;

/* typedef's for RTP create/delete hook functions */

typedef STATUS (* RTP_PRE_CREATE_HOOK) (const char *, const char *[], 
					const char *[], int, int, int, int);

typedef STATUS (* RTP_POST_CREATE_HOOK) (const RTP_ID);

typedef void   (* RTP_INIT_COMPLETE_HOOK) (const RTP_ID);

typedef void   (* RTP_DELETE_HOOK) (const RTP_ID, const int exitCode);

/* externals */

extern int		        rtpHookTblSize;
extern RTP_PRE_CREATE_HOOK *    pRtpPreCreateHookTbl;
extern RTP_POST_CREATE_HOOK *   pRtpPostCreateHookTbl;
extern RTP_INIT_COMPLETE_HOOK * pRtpInitCompleteHookTbl;
extern RTP_DELETE_HOOK *        pRtpDeleteHookTbl;

/* function declarations */

extern STATUS rtpVerifyAndLock        (WIND_RTP *pRtpId);
extern STATUS rtpVerifyTaskIdAndLock  (WIND_TCB *tid);
extern STATUS rtpIdVerify             (RTP_ID rtpId);
extern STATUS rtpHookLibInit          (int tblSize);
extern STATUS rtpPreCreateHookAdd     (RTP_PRE_CREATE_HOOK, BOOL);
extern STATUS rtpPreCreateHookDelete  (RTP_PRE_CREATE_HOOK);
extern STATUS rtpPostCreateHookAdd    (RTP_POST_CREATE_HOOK, BOOL);
extern STATUS rtpPostCreateHookDelete (RTP_POST_CREATE_HOOK);
extern STATUS rtpInitCompleteHookAdd  (RTP_INIT_COMPLETE_HOOK, BOOL);
extern STATUS rtpInitCompleteHookDelete (RTP_INIT_COMPLETE_HOOK);
extern STATUS rtpDeleteHookAdd        (RTP_DELETE_HOOK, BOOL);
extern STATUS rtpDeleteHookDelete     (RTP_DELETE_HOOK);
extern OBJ_HANDLE rtpHandleIdGet      (RTP_ID rtpId);
extern STATUS rtpLibInit 	      (int rtpHookTblSize,
                                       int syscallHookTblSize, 
                                       int rtpSignalQueueSize, 
                                       int defExcStkOverflowSize, 
                                       int defExecStkOverflowSize, 
                                       int defExecStkUnderflowSize);
extern void   rtpShowInit 	      (void);
extern STATUS rtpShellCmdInit         (void);

#ifdef __cplusplus
}
#endif

#endif	/* _ASMLANGUAGE */

#endif	/* __INCrtpLibPh */
