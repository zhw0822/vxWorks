/* pgPoolPhysLibP.h - physical page pool object library header file */

/* Copyright 1998-2004 Wind River Systems, Inc. */

/*
modification history
--------------------
01c,13sep04,zl   converted to private header file
01b,30nov03,yp   adding error code for address out of range
01a,08jul03,yp   written.
*/

#ifndef __INCpgPoolPhysLibPh
#define __INCpgPoolPhysLibPh

#ifdef __cplusplus
extern "C" {
#endif

/* includes */
#include "private/pgPoolLibP.h"

/* defines */

/* typedefs */

/* function declarations */

extern STATUS		pgPoolPhysLibInit (void);
extern PAGE_POOL_ID	pgPoolPhysCreate (PHYS_ADDR, UINT, UINT, PG_POOL_OPT);
extern STATUS		pgPoolPhysAddToPool (PAGE_POOL_ID , PHYS_ADDR, UINT);
extern STATUS		pgPoolPhysDelete (PAGE_POOL_ID);
extern BOOL		pgPoolPhysPageIsFree (PAGE_POOL_ID, PHYS_ADDR, UINT);
extern BOOL		pgPoolPhysPageIsAllocated (PAGE_POOL_ID, PHYS_ADDR, 
						    UINT);
extern PHYS_ADDR	pgPoolPhysPageGet (PAGE_POOL_ID, UINT);
extern PHYS_ADDR	pgPoolPhysAlignedPageGet (PAGE_POOL_ID, UINT, UINT);
extern PHYS_ADDR	pgPoolPhysPageGetAt (PAGE_POOL_ID, PHYS_ADDR, UINT);
extern STATUS		pgPoolPhysPageRelease (PAGE_POOL_ID, PHYS_ADDR, UINT);
extern BOOL		pgPoolPhysPageRangeVerify (PAGE_POOL_ID , PHYS_ADDR , 
						    UINT);
extern UINT		pgPoolPhysPageSize (PAGE_POOL_ID);
extern STATUS		pgPoolPhysOptionsSet (PAGE_POOL_ID, PG_POOL_OPT, 
					  PG_POOL_OPT);
extern STATUS		pgPoolPhysOptionsGet (PAGE_POOL_ID, PG_POOL_OPT *);
extern STATUS		pgPoolPhysInfoGet (PAGE_POOL_ID, PAGE_POOL_INFO *);

#ifdef __cplusplus
}
#endif

#endif /* __INCpgPoolPhysLibPh */
