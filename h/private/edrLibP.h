/* edrLibP.h - private ED&R library header file */

/* Copyright 2003-2004 Wind River Systems, Inc. */

/*
modification history
--------------------
01d,17sep04,md 	 added DSM hook
01c,20apr04,md 	 moved private info from edrLib.h
01b,12mar04,zl   renamed EDR_TASK_INFO to EDR_TCB_INFO.
01a,10feb04,zl 	 created
*/

#ifndef __INCedrLibPh
#define __INCedrLibPh

#ifdef __cplusplus
extern "C" {
#endif

#include "vxWorks.h"
#include "edrLib.h"
#include "avlUintLib.h"

/* definitions */

/* number of bytes to save for a code snapshot */

#define EDR_CODE_FRAG_SIZE  16

/* typedefs */

#ifndef	_ASMLANGUAGE

/* hook-function type for private DSM error injection hook. */

typedef void (*EDR_ERRINJ_DSM_HOOK_FUNCPTR) (
		EDR_ERROR_RECORD *pER,	      /* pointer to error record    */
		int		  kind,	      /* severity | facility	    */
		const char	 *fileName,   /* name of source file	    */
		int		  lineNumber, /* line number of source code */
		void		 *address,    /* faulting address	    */
		const char	 *msg	      /* additional text string	    */
		);

/* structure for task-local data, accessed with the pEdrInfo field of the TCB */

typedef struct edr_tcb_info
    {
    AVLU_TREE 	memEdrLocals;		/* local variables */
    UINT     	memEdrCtrl;		/* control of memEdrLib features */
    } EDR_TCB_INFO;


/* buffer structure used by printToEDRbuf() for oprintf output */

typedef struct {
    char *buf;		/* pointer to buffer */
    int  size;		/* size of buffer */
} EDR_PRINTF_BUFFER;


/* buffered output routines */

extern STATUS printToEDRbuf (char *, int , EDR_PRINTF_BUFFER *);

/* DSM Hook */

extern EDR_ERRINJ_DSM_HOOK_FUNCPTR edrErrorInjectDsmHook;

#endif	/* _ASMLANGUAGE */

#ifdef __cplusplus
}
#endif

#endif /* __INCedrLibPh */
