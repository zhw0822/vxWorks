/* cpuPwrLibP.h - CPU power management framework library private header file */

/*
 * Copyright (c) 2005 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------

01c,09sep05,mmi  mods for coding standard compliance 
01b,27aug05,mmi  adapt the file for all flavours of IA32 cpu's
01a,27Jul05,mmi  created
*/

#ifndef __INCcpuPwrLibPh
#define __INCcpuPwrLibPh

#ifdef __cplusplus
extern "C" {
#endif

#include <vxWorks.h>
#include <vxLib.h>
#include <taskLib.h>
#include <taskHookLib.h> 
#include <private/wdLibP.h>
#include <private/windLibP.h>

/* defines */

/* typedef's */

/* CPU power framework scheduler hook type  */

typedef void (*CPU_PWR_SCHED_HOOK) (void);

/* C-States Hook */

typedef STATUS (*CPU_PWR_C_STATE_HOOK) (CPU_PWR_C_STATE);

typedef UINT32 (* UTIL_GET_RTN) (void);
/* Strucure populated by BSP's */ 
 
typedef  struct cpuPwrBspInit
    {
    int                    themVectNum;   /* Thermal interrupt vector  */
    FUNCPTR                acPwrSrcCheck; /* Pwr source AC/DC ?  */
    CPU_PWR_C_STATE_HOOK   CStateHook;    /* get CPU_PWR_P_STATE */
    } CPU_PWR_BSP_INIT; 

/* Denifitions for the interface between the generic and the architecture 
 * specific layers of the power management framework.
 */
 
typedef  struct cpuPwrPrimitives
    {
    FUNCPTR       CStateSet;     /* Set CPU_PWR_C_STATE  */
    FUNCPTR       PStateSet;     /* Set CPU_PWR_P_STATE */
    FUNCPTR       PStateGet;     /* get CPU_PWR_P_STATE */
    FUNCPTR       pwrMgmtEnable;        /* Enable H/W pwr. mgmt. */ 
    FUNCPTR       tempThreshSet; /* Set temp. trip point */
    FUNCPTR       tempThreshGet; /* Get temp. trip point */
    FUNCPTR       checkHot;      /* Check if CPU hot */ 
    CPU_PWR_C_STATE  CStateMax;     /* max possible Cstates */
    CPU_PWR_P_STATE  PStateMax;     /* max possible Pstates */ 
    UTIL_GET_RTN     utilGet; /* Fcn to get utilization */
    FUNCPTR          thermIntEnable;/* Enable thermal int */
    FUNCPTR          tempGet;     /* Get CPU temperature */
    FUNCPTR          idleExitStamp; /* Idle exit time stamp */
    FUNCPTR          idleEntStamp;  /* Idle ent time stamp */
    } CPU_PWR_PRIMITIVES, * CPU_PWR_PRIMITIVES_PTR; 

typedef struct cpuPwrFrameWork /* holds Power management framework elements */
    {
    CPU_PWR_PRIMITIVES_PTR    pPrimitives;  /* pointer for low-level info */    
    UINT                      lowThreshold;  /* min temp threshold */
    UINT                      highThreshold; /* max temp threshold */
    UINT                      utilPeriod1;  /* time interval to 
                                          read utilization to decide if 
                                          power boots is necessary */ 
    UINT                      utilPeriod2;/* time interval to read 
                                          utilization to decide if 
                                          power reduction is necessary */
    CPU_PWR_P_STATE           taskPState; /* Record the default task P-State */
    CPU_PWR_P_STATE           isrPState;   /* Record the default ISR P-State */ 
    FUNCPTR                   pwrIntHandler; /* Thermal interrupt handler */
    CPU_PWR_P_STATE           currentPState; /* Current task P State */
    
    } CPU_PWR_FRAME_WORK, * CPU_PWR_FRAME_WORK_PTR; 

/* function declarations */

extern STATUS  cpuPwrArchLibInit (CPU_PWR_FRAME_WORK_PTR);
extern void    cpuPwrThermIntConnect (VOIDFUNCPTR * pVectNum);
extern STATUS  cpuPwrFrameWorkInit ( UINT32, UINT32, UINT32, UINT32, UINT32,
			                       UINT32); 
void    cpuPwrIdleEnter (void);
void    cpuPwrIntExit (void);
void    cpuPwrIntEnter (void);
void    cpuPwrWdUpRtn (void);
void    cpuPwrWdDownRtn (void);
void    cpuPwrTaskCreateRtn (WIND_TCB *);
void    cpuPwrTaskSwapRtn (WIND_TCB *, WIND_TCB *);
void    cpuPwrThermIntHandler (void);

#ifdef __cplusplus
}
#endif

#endif /* __INCcpuPwrLibP */
