/* selectLibP.h - private select library header */

/* Copyright 2001-2004 Wind River Systems, Inc. */

/*
modification history
--------------------
01d,03jun04,dat  Track maxFd for saved fd_set's, 97881
01c,18dec03,dat  replace memCtx with ready and returnSet.
01b,09dec03,dat  chg taskId to be a real WIND_TCB ptr, added ctx sw flag,
		 added exception fd_set.
01a,18sep01,aeg  extracted from version 02c of selectLib.h; added selContext.
*/

#ifndef __INCselectLibP
#define __INCselectLibP

#ifdef __cplusplus
extern "C" {
#endif

#ifndef _ASMLANGUAGE
#include "taskLib.h"
#include "lstLib.h"
#include "private/semLibP.h"

/* HELP: the public selectLib.h includes private/selectLibP.h */

typedef struct selWkNode
    {
    NODE		linkedListHooks;/* hooks for wakeup list */
    BOOL		dontFree;	/* first in free list isn't malloc'ed */
    WIND_TCB *		taskId;		/* taskId of select'ed task */
    USHORT		fd;		/* fd to set in fd_set on activity */
    USHORT		ready;		/* device is ready flag */
    SELECT_TYPE		type;		/* activity task is interested in */
    fd_set *		returnSet;	/* return fd set */
    } SEL_WAKEUP_NODE;

typedef struct
    {
    SEMAPHORE		listMutex;	/* mutex semaphore for list */
    SEL_WAKEUP_NODE	firstNode;	/* usually one deep, stash first one */
    LIST		wakeupList;	/* list of SEL_WAKEUP_NODE's */
    } SEL_WAKEUP_LIST;

typedef struct selContext
    {
    SEMAPHORE	wakeupSem;		/* wakeup semaphore */
    BOOL     	pendedOnSelect;		/* task pended on select? */

    fd_set     *pReadFds;		/* select'ed task's read fd_set */
    fd_set     *pWriteFds;		/* select'ed task's write fd_set */
    fd_set     *pExcFds;		/* select'ed task's exc fd_set */

    /* the following are needed for safe task deletion */

    fd_set     *pOrigReadFds;		/* task's original read fd_set */
    fd_set     *pOrigWriteFds;		/* task's original write fd_set */
    fd_set     *pOrigExcFds;		/* task's original exc fd_set */
    int		maxFd;			/* maxFd for saved sets above */
    int		width;			/* width parm passed to select() */

    /* 
     * The remaining memory in the SEL_CONTEXT dynamically allocated by
     * the select task create hook is used to store the various fd_set's
     * referenced in the above fd_set pointers.  Note that these structures
     * are not really fd_set's since their size is based on maxFiles (iosLib) 
     * instead of FD_SETSIZE.
     *
     * fd_set  origReadFds;	/@ memory for task's original read fd_set @/
     * fd_set  origWriteFds;	/@ memory for task's original write fd_set @/
     * fd_set  origExcFds;	/@ memory for task's original write fd_set @/
     */

    } SEL_CONTEXT;


/* function declarations */

IMPORT void	selectInit		(int numFiles);
IMPORT void	selTaskDeleteHookAdd 	(void);

#endif /* _ASMLANGUAGE */

#ifdef __cplusplus
}
#endif

#endif /* __INCselectLibP */
