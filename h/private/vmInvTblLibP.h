/* vmInvTblLibP.h - private header for vmInvTblLib.c */

/* Copyright 1984-2004  Wind River Systems, Inc. */

/*
modification history
--------------------
01a,15apr04,gls	 written.
*/

#ifndef __INCvmInvTblLibPh
#define __INCvmInvTblLibPh

#ifndef _ASMLANGUAGE

#ifdef __cplusplus
extern "C" {
#endif

IMPORT	STATUS	vmInvTblLibInit (void);
IMPORT	STATUS	vmInvPageMap (PHYS_ADDR physicalAddr, UINT len, BOOL noAlias);
IMPORT	STATUS	vmInvUnMap   (PHYS_ADDR physicalAddr, UINT len);

#ifdef __cplusplus
}
#endif

#endif

#endif /* __INCvmInvTblLibPh */
