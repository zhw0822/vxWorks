/* mqPxLibP.h - private POSIX message queue library header */

/* Copyright 1984-2004 Wind River Systems, Inc. */

/*
modification history
--------------------
01i,28sep04,fr   removed mqPxCreateRtn prototype (SPR 101349)
01h,30mar04,dcc  updated to use posixated objLib.
01g,05dec03,m_s  code-inspection changes
01f,31oct03,m_s  removed symLib and added extern for class id
                 moved mq_des definition to public header file for Base 6
01e,20jun03,ymz  moved OBJ_CORE from mq_des to msg_que.
01d,24mar99,elg  mqLibInit() must be in mqueue.h (SPR 20532).
01c,03feb94,kdl  moved structure definitions from mqPxLib.c.
01b,12jan94,kdl  changed mqLibInit() to mqPxLibInit(); added defines for
		 default queue and message size.
01a,02dec93,dvs  written
*/

#ifndef __INCmqPxLibPh
#define __INCmqPxLibPh

#ifdef __cplusplus
extern "C" {
#endif

#include "stdarg.h"
#include "mqueue.h"
#include "qLib.h"
#include "private/sigLibP.h"
#include "private/objLibP.h"

extern CLASS_ID mqClassId;             /* POSIX message queue class ID */

/* defines */

/* message queue object */

struct msg_que
    {
    OBJ_CORE		objCore;
    Q_HEAD		msgq_cond_read;
    Q_HEAD		msgq_cond_data;
    int			msgq_sigTask;
    struct sigpend	msgq_sigPend;
    unsigned long	msgq_bmap;
    struct sll_node	*msgq_data_list[MQ_PRIO_MAX];
    struct sll_node	*msgq_free_list;
    struct mq_attr	msgq_attr;
    };

/* message queue descriptor for kernel applications */

struct mq_des
    {
    int              f_flag;
    struct msg_que * f_data;
    };

/* a node to manage message buffer lists */

struct sll_node
    {
    struct sll_node *sll_next;
    size_t sll_size;
    };


/* function declarations */

extern int              mqPxLibInit (int);

extern int              _mq_close (struct msg_que * pMq);
extern ssize_t          _mq_receive (struct msg_que  * pMq,
                                     void            * pMsg,
                                     size_t            msgLen,
                                     unsigned int    * pMsgPrio,
                                     BOOL              nonBlock);
extern int              _mq_send (struct msg_que  * pMq,
                                  const void      * pMsg,
                                  size_t            msgLen,
                                  unsigned int      msgPrio,
                                  BOOL              nonBlock);
extern int              _mq_notify (struct msg_que        * pMq,
                                    const struct sigevent * pNotification);

#ifdef __cplusplus
}
#endif

#endif /* __INCmqPxLibPh */
