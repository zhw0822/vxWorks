/* setLib.h - header file for Wind Object Sets */

/* Copyright 1984-2002 Wind River Systems, Inc. */

/*
modification history
--------------------
01f,12dec02,dat  node is a tuple, not a single
01e,14oct02,dat  5.X compatibility
01d,18apr01,cjj  mods for FHA 1.0 code inspection
01c,27nov00,cjj  moved OBJ_SET definition to setLibP.h
01b,14jul99,mas  added define of SET_BAD for DMS.
01a,04aug98,mas  original version.  TOR 1.0.1/VXW 5.3 compliant.
*/

/*
DESCRIPTION
This file contains the public configuration parameters and macros for the Wind
Object Set library.

A set is an object containing a collection of objects with no implied order
although application-specific ordering may be enforced through other libraries.
A set may have any number of objects.  A set may contain other sets.  A set
with no objects is a null set.  Sets have no size restrictions other than
available memory.

Objects are referenced by a set, never included directly.  This allows objects
to be independent of set implementation and target architecture variations.
*/

#ifndef INCsetLibh
#define INCsetLibh

#ifdef __cplusplus
extern "C" {
#endif

/* Includes */

#include "vxWorks.h"
#include "vwModNum.h"
#include "objLib.h"	/* The set itself is an object */
#include "semLib.h"
#include "lstLib.h"

/* defines */

/* status codes */

#define S_setLib_LIB_INIT	(M_setLib | 1) /* set lib already initialized*/
#define S_setLib_LIB_NOT_INIT	(M_setLib | 2) /* set lib not initialized */
#define S_setLib_ARG_NOT_VALID	(M_setLib | 3) /* invalid input arg(s) */
#define S_setLib_OBJ_NOT_IN_SET (M_setLib | 4) /* obj not from given set */

/* Typedefs */

typedef struct setObj * SET_ID;
typedef void * SET_ITEM;


/* Function Prototypes */

STATUS setLibInit   (void);
SET_ID setCreate    (void);

STATUS setInit      (SET_ID setId);
STATUS setDelete    (SET_ID setId);
STATUS setDestroy   (SET_ID setId, BOOL dealloc);
SET_ID setDuplicate (SET_ID setId);
STATUS setTerminate  (SET_ID setId);
STATUS setItemAdd    (SET_ID setId, SET_ITEM itemId);
STATUS setItemDelete (SET_ID setId, SET_ITEM itemId);
SET_ITEM setItemFirst  (SET_ID setId);
SET_ITEM setItemLast   (SET_ID setId);
SET_ITEM setItemNext   (SET_ID setId);
SET_ITEM setItemPrev   (SET_ID setId);
STATUS setItemEach   (SET_ID setId, STATUS (*pFunc)(SET_ITEM,int), int arg);
STATUS setLock   (SET_ID setId, int semWait);
STATUS setUnlock (SET_ID setId);
ULONG  setSize   (SET_ID setId);
BOOL   setIsNull (SET_ID setId);

#ifdef __cplusplus
}
#endif

#endif  /* INCsetLibh */
