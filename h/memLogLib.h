/* memLogLib - memLogLib header file */

/* Copyright (c) 2003 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*
modification history
--------------------
01d,09sep03,aim  added iterators; reworked memLogWalk
01c,29aug03,dbs  update after review comments
01b,14aug03,aim  code cleanup; documentation tweaks
01a,16jun03,aim  created
*/

#ifndef __INCmemLogLib_h
#define __INCmemLogLib_h

#ifdef __cplusplus
extern "C" {
#endif

#include "vxWorks.h"

/*
 * Traversal directions for memLogWalk().
 */

typedef enum mem_log_walk_type
    {
    MEM_LOG_WALK_OLDEST_YOUNGEST = 1,
    MEM_LOG_WALK_YOUNGEST_OLDEST
    } MEM_LOG_WALK_TYPE;

typedef struct mem_log_node_marker
    {
    UINT32	id [2];		/* inter node marker                 */
    } MEM_LOG_NODE_MARKER;

typedef struct mem_log_node_header
    {
    int		offset;                 /* offset of this node               */
    int		size;		        /* total size of this node           */
    int		next;                   /* offset of next node               */
    int		prev;                   /* offset of prev node               */
    } MEM_LOG_NODE_HEADER;

typedef struct mem_log_node_payload
    {
    int		length;                 /* length of data                    */
    char	data [1];               /* data block                        */
    } MEM_LOG_NODE_PAYLOAD;

typedef struct mem_log_node             /* Node of a linked list.            */
    {
    MEM_LOG_NODE_MARKER  marker;        /* node marker                       */
    MEM_LOG_NODE_HEADER	 header;	/* node header                       */
    UINT32		 checksum;      /* checksum for node header          */
    MEM_LOG_NODE_PAYLOAD payload;	/* payload; must be the last entry   */
    } MEM_LOG_NODE;

typedef struct mem_log_node_list        /* Header for a linked list.         */
    {
    int                 count;          /* node list count                   */
    MEM_LOG_NODE        sentinel;       /* sentinel node                     */
    } MEM_LOG_NODE_LIST;

typedef struct mem_log_hdr		/* Header for a memory log.          */
    {
    int			endianness;     /* endianness identifier             */
    int			magic;          /* mem log identifier                */
    int			version;        /* mem log version number            */
    int			size;           /* mem log size (in bytes)           */
    UINT32		reserved [4];   /* reserved for future use           */
    } MEM_LOG_HEADER;

typedef struct mem_log                  /* A memory log.                     */
    {
    MEM_LOG_HEADER      header;         /* memory log meta-data              */
    UINT32		checksum;       /* header checksum                   */
    UINT32		reserved [4];   /* reserved for future use           */
    MEM_LOG_NODE_LIST   node_list;      /* the list of nodes                 */
    MEM_LOG_NODE	node_space [1]; /* node_list arena                   */
    } MEM_LOG;

typedef struct mem_log_iter	        /* An iterator.                      */
    {
    MEM_LOG *		pLog;	        /* pointer to log to iterate         */
    MEM_LOG_WALK_TYPE	direction;      /* direction to iterate              */
    MEM_LOG_NODE *	pNil;	        /* pointer to list head              */
    MEM_LOG_NODE *	pNode;	        /* pointer to current node           */
    int			nodeCount;      /* total number of nodes in log      */
    int			nodeNumber;     /* current node number; 1/nodeCount  */
    } MEM_LOG_ITER;


/* function prototypes */

extern int memLogWalk (MEM_LOG * pLog, FUNCPTR callback,
		       MEM_LOG_WALK_TYPE direction, void * callback_arg);
extern STATUS memLogAdd (MEM_LOG * pLog, const void * pData, int len);
extern STATUS memLogDelete (MEM_LOG * pLog);
extern MEM_LOG * memLogAttach (void * pAddr, int size);
extern MEM_LOG * memLogCreate (void * pAddr, int size);
extern int memLogNodeCount (MEM_LOG * pLog);
extern int memLogSize (MEM_LOG * pLog);
extern int memLogMinimumSize (void);
extern STATUS memLogDeleteAll (MEM_LOG * pLog);
extern BOOL memLogValidate (MEM_LOG * pLog);
extern BOOL memLogIterNext (MEM_LOG_ITER * pIter,
			    MEM_LOG_NODE_PAYLOAD ** ppPayload);
extern BOOL memLogIterCreate (MEM_LOG * pLog, MEM_LOG_ITER * pIter,
			      MEM_LOG_WALK_TYPE direction);

#ifdef  __cplusplus
}
#endif /* __cplusplus */

#endif /* __INCmemLogLib_h */
