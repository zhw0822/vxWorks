/* mman.h - memory management library header file */

/* Copyright 1984-2005 Wind River Systems, Inc. */

/*
modification history
--------------------
01f,06jan05,zl   added _mctrl support. reversed previous change.
01e,01oct04,gls  made <off> type off_t64
01d,14jun04,gls  added MAP_ANON_FD and MAP_ANONYMOUS
01c,11jun04,gls  added MAP_ANON
01b,09sep03,zl   added mmap(), mmunmap(), mprotect()
01a,05nov93,dvs  written
*/

#ifndef __INCmmanh
#define __INCmmanh

#ifdef __cplusplus
extern "C" {
#endif

/* includes */

#include "vxWorks.h"

/* memory locking flags */

#define MCL_CURRENT 	0x1		/* currently not used */
#define MCL_FUTURE	0x2		/* currently not used */

/* mmap protection, flag and return value */

#define PROT_NONE	0x0000		/* Page cannot be accessed */
#define PROT_READ	0x0001		/* Page can be read */
#define PROT_WRITE	0x0002		/* Page can be written */
#define PROT_EXEC	0x0004		/* Page can be executed */

#define MAP_SHARED	0x0001		/* Share changes */
#define MAP_PRIVATE	0x0002		/* Changes are private */
#define MAP_ANON	0x0004		/* Use anonymous memory */
#define MAP_FIXED	0x0010		/* Interpret addr exactly */

#define MAP_FAILED	((void *) -1)	/* mmap() error return value */

#define MAP_ANON_FD     -1              /* required fd value for mmap */

/* BSD uses MAP_ANON and Linux uses MAP_ANONYMOUS.  We'll provide both */

#define MAP_ANONYMOUS	MAP_ANON 

/* 
 * function definitions for _mctrl(); these should not be used directly, 
 * but via the wrapper functions, such as cacheLib.
 */

#define MCTL_CACHE_FLUSH	1	/* cache flush */
#define MCTL_CACHE_INVALIDATE	2	/* cache invalidate */
#define MCTL_CACHE_CLEAR	3	/* cache clear*/
#define MCTL_CACHE_TEXT_UPDATE	4	/* cache text update */


#ifndef _ASMLANGUAGE

/* Function declarations */

extern int	mlockall (int flags);
extern int	munlockall (void);
extern int	mlock (const void * addr, size_t len);
extern int	munlock (const void * addr, size_t len);
extern void *	mmap (void *addr, size_t len, int prot, int flags, int fildes,
		      off_t off);
extern int	mprotect (void *addr, size_t len, int prot);
extern int 	munmap (void * addr, size_t len);

#ifdef _WRS_KERNEL
extern void *	mmap64 (void *addr, size_t len, int prot, int flags, int fildes,
			off_t64 off);
#else
extern int	_mctl (void *addr, size_t len, int function, int arg);
#endif

#endif /* _ASMLANGUAGE */

#ifdef __cplusplus
}
#endif

#endif /* __INCmmanh */
