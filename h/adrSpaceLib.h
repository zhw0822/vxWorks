/* adrSpaceLib.h - Address Space Library header */

/* Copyright 1998-2004 Wind River Systems, Inc. */
#include "copyright_wrs.h"

/*
modification history
--------------------
01s,22sep04,zl   more comments
01r,31aug04,zl   moved private APIs to private header file
01q,07jun04,tam  added definition for option ADR_SPACE_OPT_RTP
01p,15apr04,yp   Adding sysPhysMemDesc and numElems to ADR_SPACE_INFO for
                 testing
		 Added prototype for adrSpaceRAMAddToPool()
01o,05apr04,yp   reworking adrSpaceLibInit call
01n,18mar04,yp   removing unused prototype adrSpaceReserve
		 adding the kernel virtual page pool ID to ADR_SPACE_INFO
		 adding adrSpaceLibInit params to ADR_SPACE_INFO
01m,25nov03,yp   using reserved field in page pool options for kernel space
01m,30nov03,yp   adding error ADDRESS_OUT_OF_RANGE and removing PD related
                 errors; fixing prototype for adrSpaceLibInit;
01l,11nov03,yp   adjusting ADR_SPACE_INFO structure for base 6
01k,16oct03,yp   proted to base 6
01j,31aug00,pfl  added S_adrSpaceLib_ALLOC_UNIT_IS_INVALID
01i,17aug00,tam  added ADR_SPACE_OPT_KERNEL_PD
01h,13jun99,map  added master page pools to ADR_SPACE_INFO struct.
01g,27mar00,map  support for shared library and IO address space.
01f,05nov99,map  added support for virtual regions.
01e,30apr99,map  added support for appl PD region. removed sub-region support.
01d,28apr99,wkn  added #include pgPoolLibP.h
01c,06apr99,map  added adrSpace options, fixed prototypes.
01b,08feb99,tam  added adrSpaceBaseGet().
01a,16sep98,map  written.
*/

#ifndef __INCadrSpaceLibh
#define __INCadrSpaceLibh

#ifdef __cplusplus
extern "C" {
#endif

/* includes */

#include "vxWorks.h"

/* errnos */

#define	S_adrSpaceLib_LIB_NOT_INITIALIZED	(M_adrSpaceLib | 1)
#define	S_adrSpaceLib_CANNOT_REINITIALIZE	(M_adrSpaceLib | 2)
#define	S_adrSpaceLib_PARAMETER_NOT_ALIGNED	(M_adrSpaceLib | 3)
#define	S_adrSpaceLib_REGION_NOT_AVAILABLE	(M_adrSpaceLib | 4)
#define	S_adrSpaceLib_VIRT_ADRS_NOT_ALIGNED	(M_adrSpaceLib | 5)
#define	S_adrSpaceLib_SIZE_IS_INVALID        	(M_adrSpaceLib | 6)
#define	S_adrSpaceLib_INVALID_PARAMETER		(M_adrSpaceLib | 7)
#define	S_adrSpaceLib_VIRTUAL_OVERLAP		(M_adrSpaceLib | 8) 
#define S_adrSpaceLib_PHYSICAL_OVERLAP		(M_adrSpaceLib | 9)
#define S_adrSpaceLib_ADDRESS_OUT_OF_RANGE	(M_adrSpaceLib |10)
#define S_adrSpaceLib_INSUFFICIENT_SPACE_IN_RGN	(M_adrSpaceLib |11)
#define S_adrSpaceLib_REGION_CONFLICT		(M_adrSpaceLib |12)

/* typedefs */

typedef struct adr_space_info
    {
    UINT	physAllocUnit;		/* physical space allocation unit */
    UINT	physTotalPages;		/* total system RAM physical pages*/
    UINT	physFreePages;		/* unmapped system RAM physical pages */
    UINT	physMaxSize;		/* largest unmapped system RAM block */
    UINT	kernelAllocUnit;	/* kernel region allocation unit */
    UINT	kernelTotalPages;	/* kernel region total pages */
    UINT	kernelFreePages;	/* kernel region unmapped pages */
    UINT	kernelMaxSize;		/* largest unmapped blk in kernel rgn */
    UINT	userAllocUnit;		/* user region allocation unit */
    UINT	userTotalPages;		/* user region total pages */
    UINT	userFreePages;		/* user region unmapped pages */
    UINT	userMaxSize;		/* largest unmapped blk in user rgn */
    } ADR_SPACE_INFO;

/* function prototypes */

extern STATUS	adrSpaceInfoGet (ADR_SPACE_INFO *pInfo);
extern STATUS 	adrSpaceRAMAddToPool (PHYS_ADDR startAddr, UINT size);

#ifdef __cplusplus
}
#endif

#endif /* __INCadrSpaceLibh */
